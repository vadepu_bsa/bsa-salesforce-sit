({
    // @FSL3-241
    validateDate : function(cmp, event, helper) {
        var dueDateFromCmp = cmp.find('dueDateFrom');
        var dueDateToCmp = cmp.find('dueDateTo');

        var dueDateFrom = dueDateFromCmp.get("v.value");
        var dueDateTo = dueDateToCmp.get("v.value");

        var initiatedInput = event.getSource().getLocalId();

        dueDateFromCmp.setCustomValidity("");
        dueDateToCmp.setCustomValidity("");

        if(dueDateFrom > dueDateTo) {
            cmp.find(initiatedInput).setCustomValidity("Due Date From date cannot be later than due date to");
        }

        var today = new Date();
        // (today.getMonth() + 1)

        var nextmonth = new Date(today.setMonth(today.getMonth()+1)).toISOString().substring(0, 10);

        // var nextmonth = today.add(1).month();
        if(dueDateFrom > nextmonth) { 
            // dueDateFromCmp.setCustomValidity("Are you sure ?");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'sticky',
                type: "Warning",
                title: "Warning!",
                message: "Are you sure you want to generate work order(s) beyond 1 month?"
            });
            toastEvent.fire();
        }


        dueDateFromCmp.reportValidity(); // Tells lightning:input to show the error right away without needing interaction
        dueDateToCmp.reportValidity(); // Tells lightning:input to show the error right away without needing interaction

    }
})