({
    handleRowSelection : function(cmp, selectedRow) {
        var selectedChecked = selectedRow.get("v.checked");
        var selectedRowIndex = selectedRow.get("v.value");

        var frequencies = cmp.get("v.servicePeriods");
        var selectedServicePeriods = cmp.get("v.selectedServicePeriods");
        var selectedFrequency = frequencies[selectedRowIndex].label;
        if(selectedChecked) {
            var index = selectedServicePeriods.indexOf(selectedFrequency);
            if(index == -1) {
                selectedServicePeriods.push(selectedFrequency);
            }
        }
        else {
            selectedServicePeriods.splice(selectedServicePeriods.indexOf(selectedFrequency), 1);
        }
        cmp.set("v.selectedServicePeriods", selectedServicePeriods);
    },
    updateServicePeriods : function(cmp) {
        var selectedServicePeriods = cmp.get('v.selectedServicePeriods');
        let selectedServicePeriodsSet = new Set(selectedServicePeriods);
        var rows = cmp.get('v.servicePeriods');            
        rows.forEach(function(row){
            if(selectedServicePeriodsSet && selectedServicePeriodsSet.has(row.value)) {
                row.isChecked = true;
            }
            else {
                row.isChecked = false;
            }
        });
        cmp.set('v.servicePeriods', rows);  
    },
    fetchServicePeriods: function(cmp, event) {
        this.showToggleSpinner(true);

        var action = cmp.get("c.getPicklistOptions");
        action.setParams({ 
            "objectApiName" : "MaintenanceAsset",
            "fieldApiName": "Maintenance_Routine_Global__c"
        });
        action.setCallback(this, function(response) {
            this.showToggleSpinner(false);
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.servicePeriods", response.getReturnValue());
                this.updateServicePeriods(cmp);
            }
            else if (state === "ERROR") {
				var errors = response.getError();
				message.showErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    showToggleSpinner: function (showSpinner) {
		var toggleSpinner = $A.get("e.c:APS_ToggleLoadingSpinner");
		toggleSpinner.setParams({
			"showSpinner": showSpinner
		});

		toggleSpinner.fire();
	},
})