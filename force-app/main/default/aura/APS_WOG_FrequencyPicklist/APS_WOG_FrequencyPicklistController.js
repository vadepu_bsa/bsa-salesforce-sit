({
    doInit : function(cmp, event, helper){
        helper.fetchServicePeriods(cmp, event);
        helper.updateServicePeriods(cmp);
    },
    refreshServicePeriods: function(cmp, event, helper){
        helper.updateServicePeriods(cmp);
    },
    selectAll : function(cmp, event, helper){
        var selectedRow = event.getSource();
        var selectedChecked = selectedRow.get("v.checked");

        var checkboxes = cmp.find("row-checkbox");

        checkboxes.forEach(checkbox => {
            checkbox.set("v.checked", selectedChecked);
            helper.handleRowSelection(cmp, checkbox);
        });
    },
    onFrequencySelect : function(cmp, event, helper){
        var selectedRow = event.getSource();
        helper.handleRowSelection(cmp, selectedRow);
    }
})