({
    loadData: function (cmp, evt, help) {
        var selectedServiceTerritories = cmp.get('v.selectedServiceTerritories');        
        var selectedServiceTerritoriesJSON;
        if(selectedServiceTerritories) {
            selectedServiceTerritoriesJSON = JSON.stringify(selectedServiceTerritories);
        }

        var selectedCustomers = cmp.get('v.selectedCustomers');        
        var selectedCustomersJSON;
        if(selectedCustomers) {
            selectedCustomersJSON = JSON.stringify(selectedCustomers);
        }

        var selectedSites = cmp.get('v.selectedSites');        
        var selectedSitesJSON;
        if(selectedSites) {
            selectedSitesJSON = JSON.stringify(selectedSites);
        }
        
        var selectedServicePeriods = cmp.get('v.selectedServicePeriods');        
        var selectedServicePeriodsJSON;
        if(selectedServicePeriods) {
            selectedServicePeriodsJSON = JSON.stringify(selectedServicePeriods);
        }

        var selectedEquipmentTypes = cmp.get('v.selectedEquipmentTypes');        
        var selectedEquipmentTypesJSON;
        if(selectedEquipmentTypes) {
            selectedEquipmentTypesJSON = JSON.stringify(selectedEquipmentTypes);
        }

        console.log(cmp.get('v.stepName'), cmp.get('v.sObjectName'));

        // Declare all varaiables.
        let methodName = 'c.fetchRecords',
            params = {
                'sObjectName' : cmp.get('v.sObjectName'),
                'fieldsetName' : cmp.get('v.fieldSetName'), 
                'searchField' : cmp.get('v.searchField'), 
                'searchKeyWord' : cmp.get('v.searchKeyWord'), 
                'serviceTerritoriesJSON' : selectedServiceTerritoriesJSON,
                'startDueDate' : cmp.get('v.dueDateFrom'), 
                'endDueDate' : cmp.get('v.dueDateTo'), 
                'contractedChk' : cmp.get('v.contractedChk'),
                'nonContractedChk' : cmp.get('v.nonContractedChk'),
                'servicePeriodsJSON' : selectedServicePeriodsJSON,
                'customersJSON' : selectedCustomersJSON, 
                'sitesJSON' : selectedSitesJSON,
                'equipmentTypesJSON' : selectedEquipmentTypesJSON,
                'stepName' : cmp.get('v.stepName')
            },
            // This is callback function for handling Response
            callback = (response) => {
                if (response) {
                    this.createHeader(cmp, response['lstFields'])
                    this.createRows(cmp, response['lstFields'], response['lstRows'])
                }
            }

        this.showToggleSpinner(true);
            
        //Call the apex method 
        this.callApex(cmp, methodName, params, callback);
    },
    filterData: function (cmp, evt, help) {        
        var searchKey = cmp.get('v.searchKeyWord');
        var allRowData = cmp.get('v.allRowData');
        var searchField = cmp.get('v.serachField');
        var filteredRowData = [];
        for (var i = 0; i < allRowData.length; i++) {
            var row = allRowData[i];
            if(row.record[searchField].toLowerCase().includes(searchKey.toLowerCase())){
                filteredRowData.push(row);
            }
        }
        cmp.set('v.filteredRowData', filteredRowData);
        cmp.set('v.rowData', filteredRowData.splice(0, cmp.get('v.prev')));
    },
    createHeader: function (cmp, field) {

        let orderData = field.map(e => {
            var obj = {};
            obj['label'] = e['label']
            obj['fieldPath'] = e['fieldPath']
            obj['isSortUp'] = true
            obj['isSortDown'] = false
            obj['byDefaultSort'] = e['label'] == 'Name' ? true : false
            return obj;
        })
        cmp.set('v.colData', orderData);

    },

    // TODO: need to re-visit this function to verify how to reset already selected rows previously but not in the current list currently
    // probably selected row caching issue
    createRows: function (cmp, fields, rows) {
        // Need to check if checkbox is checked on the page when loading the table
        let selectedRowIds = new Set(cmp.get('v.selectedRows'));
        var sObjectName = cmp.get('v.sObjectName');
        
        console.log('@rows: ' + JSON.stringify(rows));

        // @FSL3-217
        rows.forEach(function(row) {
            if(sObjectName != 'WorkOrder' && selectedRowIds && 
                ((sObjectName == 'Asset' && selectedRowIds.has(row.record.Equipment_Type__c)) || selectedRowIds.has(row.record.Id))
              ) {
                row.isChecked = true;
            }
        });

        let filedPath = fields.map(e => {
            return e['fieldPath']
        })
        cmp.set('v.fieldPath', filedPath);
        cmp.set('v.allRowData', rows);
        cmp.set('v.filteredRowData', rows);
        cmp.set('v.rowData', rows.splice(0, cmp.get('v.prev')));
    },

    toast: function (title, message, type) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type,
        });
        toastEvent.fire();
    },

    //This method for call the apex method.
    callApex: function (cmp, methodName, params, callback) {
        let state, action;
        action = cmp.get(methodName);
        action.setParams(params);
        action.setCallback(this, function (response) {
            state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
                this.showToggleSpinner(false);
            }
            else {
                var errors = response.getError();
				message.showErrors(errors);
                this.showToggleSpinner(false);
            }
        });
        $A.enqueueAction(action);
    },
    convertSelectedRowToPill : function(selectedRowValue){
        var pill = {};
        pill.type = 'icon';
        pill.href = '';
        pill.label = selectedRowValue.EG_Equipment_Type__c;
        pill.iconName = 'standard:product';
        pill.name = selectedRowValue.Equipment_Type__c;
        pill.alternativeText = selectedRowValue.Name;
        return pill;
    },
    pushPillToPillsContainer : function(pills, selectedRow, helper){
        var alreadyAdded = false;
        for (var i = 0; i < pills.length; i++) {
            var pill = pills[i];
            if(pill.name == selectedRow.Id || pill.name == selectedRow.Equipment_Type__c){
                alreadyAdded = true;
            }
        }
        if(!alreadyAdded) {
            pills.push(helper.convertSelectedRowToPill(selectedRow));
        }
        return pills;
    },    
    removePillFromPillsContainer : function(pills, selectedRow){
        for (var i = 0; i < pills.length; i++) {
            var pill = pills[i];
            if(pill.name == selectedRow.Id || pill.name == selectedRow.Equipment_Type__c){
                pills.splice(i, 1);
                break;
            }
        }
        return pills;
    },
    handleRowSelection : function(cmp, selectedRow, helper) {
        var selectedRowIndex = selectedRow.get("v.value");
        var selectedChecked = selectedRow.get("v.checked");
        var rows = cmp.get("v.rowData");

        var pills = cmp.get("v.pills");
        var selectedRows = cmp.get("v.selectedRows");

        var selectedRowValue;
        var selectedRowId;
        
        var objectName = cmp.get('v.sObjectName');

        if(objectName == 'WorkOrder') {
            selectedRowValue = rows[selectedRowIndex].workOrder;
            selectedRowId = selectedRowValue.Row_Number__c;
        }
        else if(objectName == 'Asset') {
            selectedRowValue = rows[selectedRowIndex].record;
            selectedRowId = selectedRowValue.Equipment_Type__c;
        }
        else {
            selectedRowValue = rows[selectedRowIndex].record;
            selectedRowId = selectedRowValue.Id;
        }
        if(selectedChecked) {
            pills = helper.pushPillToPillsContainer(pills, selectedRowValue, helper);
            var index = selectedRows.indexOf(selectedRowId);
            if(index == -1) {
                selectedRows.push(selectedRowId);
            }
        }else{
            pills = helper.removePillFromPillsContainer(pills, selectedRowValue);
            selectedRows.splice(selectedRows.indexOf(selectedRowId), 1);
        }
    
        cmp.set("v.pills", pills);   

        cmp.set("v.selectedRows", selectedRows);
    },
    handleSelectAll : function(cmp, selectedRow, helper, isChecked) {
        var objectName = cmp.get('v.sObjectName');
        var pills = cmp.get("v.pills");
        var selectedRows = cmp.get("v.selectedRows");
        var selectedRowValue;
        var selectedRowId;

        if(objectName == 'WorkOrder') {
            selectedRowValue = selectedRow.workOrder;
            selectedRowId = selectedRowValue.Row_Number__c;
        }
        else if(objectName == 'Asset') {
            selectedRowValue = selectedRow.record;
            selectedRowId = selectedRowValue.Equipment_Type__c;
        }
        else {
            selectedRowValue = selectedRow.record;
            selectedRowId = selectedRowValue.Id;
        }
        if(isChecked) {
            pills = helper.pushPillToPillsContainer(pills, selectedRowValue, helper);
            var index = selectedRows.indexOf(selectedRowId);
            if(index == -1) {
                selectedRows.push(selectedRowId);
            }
        }else{
            pills = helper.removePillFromPillsContainer(pills, selectedRowValue);
            selectedRows.splice(selectedRows.indexOf(selectedRowId), 1);
        }
    
        cmp.set("v.pills", pills);   
        cmp.set("v.selectedRows", selectedRows);
    },
    showToggleSpinner: function (showSpinner) {
		var toggleSpinner = $A.get("e.c:APS_ToggleLoadingSpinner");
		toggleSpinner.setParams({
			"showSpinner": showSpinner
		});

		toggleSpinner.fire();
	}
})