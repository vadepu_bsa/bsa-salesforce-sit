({
    loadData: function(cmp, evt, help) {
        help.loadData(cmp, evt, help)
    },
    filterData: function(cmp, evt, help) {
        help.filterData(cmp, evt, help)
    },
    next: function(cmp, evt, help) {
        let data, next, prev, orderData, tempdata = [];
        data = cmp.get('v.allRowData');
        data.forEach(val => {
            tempdata.push(val)
        })
        next = cmp.get('v.next');
        prev = cmp.get('v.prev');
        orderData = tempdata.splice(next + prev, prev);
        cmp.set('v.next', next + prev)
        cmp.set('v.rowData', orderData);
        cmp.set('v.allRowData', data)
    },
    prev: function(cmp, evt, help) {
        let data, next, prev, orderData, tempdata = [];
        data = cmp.get('v.allRowData');
        data.forEach(val => {
            tempdata.push(val)
        })
        next = cmp.get('v.next');
        prev = cmp.get('v.prev');
        orderData = tempdata.splice(next - prev, prev);
        cmp.set('v.next', next - prev)
        cmp.set('v.rowData', orderData);
        cmp.set('v.allRowData', data)
    },
    handleRowSelection : function(cmp, event, helper){
        var selectedRow = event.getSource();
        helper.handleRowSelection(cmp, selectedRow, helper);
    },
    updateSelectedRows : function(cmp, event, helper){
        var removedPillId = cmp.get("v.removedPillId");
        var selectedRows = cmp.get("v.selectedRows");

        var rows = cmp.get("v.rowData");
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i].record;
            if(row.Equipment_Type__c == removedPillId){
                console.log('find matched row');
                rows[i].isChecked = false;
                selectedRows.splice(selectedRows.indexOf(row.Equipment_Type__c), 1);
                break;
            }
        }
        cmp.set("v.rowData", rows);
        cmp.set("v.selectedRows", selectedRows);
        cmp.set("v.removedPillId", null);
    },
    selectAll : function(cmp, event, helper){
        var selectedRow = event.getSource();
        var selectedChecked = selectedRow.get("v.checked");

        var checkboxes = cmp.find("row-checkbox");

        if(checkboxes) {
            if(checkboxes.length) {
                checkboxes.forEach(checkbox => {
                    checkbox.set("v.checked", selectedChecked);
                    // helper.handleRowSelection(cmp, checkbox, helper);
                });
            }
            else {
                checkboxes.set("v.checked", selectedChecked);
                // helper.handleRowSelection(cmp, checkboxes, helper);
            }
        }

        var data = cmp.get("v.allRowData");
        // if(data) {
        //     if(data.length) {
                data.forEach(row => {
                    row.isChecked = selectedChecked;
                    helper.handleSelectAll(cmp, row, helper, selectedChecked);
                })
        //     }
        //     else {
        //         row.isChecked = selectedChecked;
        //     }
        // }
    }
})