({
    doInit : function(cmp, event, helper) {

    },
    convertToWorkOrders: function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        helper.convertToWorkOrders(cmp, event, recordId);    
    },
    closeModal: function(cmp, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    recordUpdated: function(cmp, event, helper) {
        var status = cmp.get("v.Order.Status");
        console.log('status: ' + status);
        if(status != 'Accepted') {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({                    
                "title": "Error!",
                "type": "error",
                "duration" : "10000",
                "message": "Can only convert 'Accepted' order to work order"
            });
                
            toastEvent.fire();   
            $A.get("e.force:closeQuickAction").fire(); 
        }
    }
})