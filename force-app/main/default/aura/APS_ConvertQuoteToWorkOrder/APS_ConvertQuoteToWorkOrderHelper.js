({
    convertToWorkOrders: function(cmp, event, recordId){
        this.showAndHideSpinner(cmp, 'show');
        console.log('recordId: ' + recordId);

        var action = cmp.get("c.generateWorkOrders");
        var order = cmp.get("v.Order");

        action.setParams({ 
            orderId : recordId,
            orderRecordType : order.RecordType.DeveloperName
        });
        action.setCallback(this, function(response) {
            this.showAndHideSpinner(cmp, 'hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                var workOrderIds = response.getReturnValue();
                
                console.log('workOrderIds: ' + workOrderIds);
                
                if(!workOrderIds || workOrderIds == null || workOrderIds.length == 0) {
                    var toastEvent = $A.get("e.force:showToast");             
                    toastEvent.setParams({                    
                        "title": "Error!",
                        "type": "error",
                        "duration" : "20000",
                        "message": "Couldn't convert to work order(s) due to unknown reason"
                    });
                    
                    toastEvent.fire(); 
                }
                else {
                    var toastEvent = $A.get("e.force:showToast");             
                    toastEvent.setParams({                    
                        "title": "Success!",
                        "type": "success",
                        "duration" : "20000",
                        "message": "Work order(s) and line items have been generated successfully. \n Related tasks are being generated separately. You will receive an email notification in case of failure."
                    });
                    
                    toastEvent.fire();   

                    var pageReference = {
                        type: 'standard__objectPage',
                        attributes: {
                            objectApiName: 'WorkOrder',
                            actionName: 'list'
                        },
                        state: {
                            filterName: 'Recent'
                        }
                    };

                    var navService = cmp.find("navService");
                    event.preventDefault();
                    navService.navigate(pageReference);
                }

                $A.get("e.force:closeQuickAction").fire();
            }
            else if (state === "ERROR") {
				var errors = response.getError();
				message.showErrors(errors);
            }
        });
        $A.enqueueAction(action);
    },
    showAndHideSpinner : function(cmp, action) {
        var spinner = cmp.find("spinner");
        if (action == 'show') {
            $A.util.removeClass(spinner, "slds-hide");
        }
        else if (action == 'hide') {
            $A.util.addClass(spinner, "slds-hide");
        }
    }
})