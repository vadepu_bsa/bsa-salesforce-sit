({
    generateDefectQuote: function(cmp, event, defectIds){
        
        var action = cmp.get("c.createDefectQuote");
        
        
        console.log('defectIds: ' + defectIds);
        
        action.setParams({ defectIdsStr : defectIds});
        
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            //helper.showToggleSpinner(false);
            var state = response.getState();
            if (state === "SUCCESS") {
                var quoteId = response.getReturnValue().split(',');
                cmp.set('v.quoteName',quoteId[1])
                cmp.set('v.quoteId', quoteId[0]);
                
                // if(quoteId) {
                
                var workspaceAPI = cmp.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    
                    
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                    
                    
                    // Sets the route to /lightning/o/Account/home
                    var pageReference = {
                        type: 'standard__recordPage',
                        attributes: {
                            "recordId": quoteId,
                            "objectApiName" : "Order",
                            "actionName" : "view"
                        }
                    };
                    
                    var navService = cmp.find("navService");
                    // event.preventDefault();
                    navService.navigate(pageReference);    
                })
                .catch(function(error) {
                    console.log(error);
                });   
                
                // }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                //message.showErrors(errors);
                var message=errors[0].message;
                cmp.set('v.quoteId', 'Error While Generating Work Order, Please check selected defects are correct !');
            }
            
            
        });
        $A.enqueueAction(action);
    }
})