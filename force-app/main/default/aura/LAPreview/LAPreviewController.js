({
    // common reusable function for toggle sections
    toggleSection : function(component, event, helper) {
        // dynamically get aura:id name from 'data-auraId' attribute
        var sectionAuraId = event.target.getAttribute("data-auraId");
        // get section Div element using aura:id
        //var sectionDiv = component.find(sectionAuraId).getElement();
        var sectionDiv = document.getElementById(sectionAuraId);
        /* The search() method searches for 'slds-is-open' class, and returns the position of the match.
         * This method returns -1 if no match is found.
        */
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        // -1 if 'slds-is-open' class is missing...then set 'slds-is-open' class else set slds-is-close class to element
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }
    },
    doInit : function(component, event, helper) {
        var action = component.get("c.fetchFiles");
        
        action.setParams({
            "linkedRecId" : component.get("v.recordId")
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
				var fileList = [];

                for ( var key in result ) {
                     fileList.push({wonumber:key, value:result[key]});
                     }
                
                component.set("v.filesList",fileList);
            }
        });
        $A.enqueueAction(action);
    }
})