({
	callApex : function(action) {
        var p = new Promise($A.getCallback(function(resolve,reject) {
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    console.log('Success');
                    resolve(response.getReturnValue());
                }
                else if (state === "ERROR") {
                    console.log("Error: ", response.getError());
                    reject(response.getError());
                }
            });
            
            $A.enqueueAction(action);
        }));
        
        return p;
    },
    searchIcon : function (component) {
        var action = component.get("c.getIconNameStr");
        action.setParams({ sObjectName : component.get("v.sObjectName") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var responseValue = response.getReturnValue();
                component.set("v.iconName", responseValue );
                console.log("responseIcon ", responseValue);
            }
        });

        $A.enqueueAction(action);
    }            
})