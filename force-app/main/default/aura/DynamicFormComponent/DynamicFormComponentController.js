({
	doInit : function(component, event, helper) {
        component.set("v.fields", []);
        console.log("firing sub product display init");
        component.set("v.activeSections", component.get("v.sObjectName"));

        var getFieldsToDisplay = component.get("c.getFieldsToDisplay");
        getFieldsToDisplay.setParams({
            recordId : component.get("v.recordId"), 
            sObjectName: component.get("v.sObjectName")
        });
        helper.callApex(getFieldsToDisplay)
        .then(function(result) {
            component.set("v.fields", result);
            console.log("field list:",result);
        });

        helper.searchIcon(component);

    }
})