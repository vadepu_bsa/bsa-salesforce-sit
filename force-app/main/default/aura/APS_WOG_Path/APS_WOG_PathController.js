({
    handleCurrentStep : function (cmp, evt, helper) {
        console.log("handleCurrentStep");
        var stepIndex = evt.getParam('index');       
        var steps = cmp.get("v.steps");
        var currentStep = steps[stepIndex].value;
        
        var disabledStep = cmp.get("v.disableStep");

        console.log(disabledStep);
        if (disabledStep){          

            if (stepIndex > 0){
                //currentStep = steps[stepIndex-1].value;
            }
            var toastEvent = $A.get("e.force:showToast");

            toastEvent.setParams({
                mode: 'error',
                type: "error",
                title: "Incorrect entry!",
                message: "Cannot move off this page while there is an incorrect entry."
            });
            toastEvent.fire();

            return;
        }

        if(currentStep == 'Review') {
            // TODO: may not necessary
        }

        cmp.set("v.currentStep", currentStep);
    }
})