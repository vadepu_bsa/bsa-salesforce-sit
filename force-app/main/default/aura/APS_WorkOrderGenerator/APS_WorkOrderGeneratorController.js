({
    doInit : function(cmp, event, helper) {
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        cmp.set('v.dueDateFrom', today);
        cmp.set('v.dueDateTo', today);
        helper.getUrlParams(cmp);
    },
    handlePillRemove : function(cmp, event, helper){
        var selectedPill = event.getParam("item");
        var pills = cmp.get("v.selectedEquipmentTypePills");        
        
        for (var i = 0; i < pills.length; i++) {
            var pill = pills[i];
            if(pill.name == selectedPill.name){
                pills.splice(i, 1);
            }
        }
        
        cmp.set("v.selectedEquipmentTypePills", pills);
        cmp.set("v.removedPillId", selectedPill.name);

    },
    updateSearchKey : function(cmp, event, helper){
        var filter = cmp.get("v.equipmentTypeSearchKeyWord");
        // filter = filter.toUpperCase();
        cmp.set("v.equipmentTypeSearchKeyWord", filter);
    },
    handleToggleSpinner : function(cmp, event, helper) {
        var spinner = cmp.find("spinner");
        var showSpinner = event.getParam("showSpinner");
        if (showSpinner == true) {
            $A.util.removeClass(spinner, "slds-hide");
        }
        else if (!$A.util.hasClass(spinner, "slds-hide")) {
            $A.util.addClass(spinner, "slds-hide");
        }
    },
    approveWorkOrders: function(cmp, event, helper){
        helper.approveWorkOrdersOrexportToCsv(cmp, event, 'approveWorkOrders');
    },
    exportToCsv: function(cmp, event, helper){
        if (confirm("Are you sure you want to export to Excel?")) {         
            helper.approveWorkOrdersOrexportToCsv(cmp, event, 'exportToCsv');
        }
    },
    clearServiceTerritoryRelatedCache: function(cmp, event, helper){
        console.log('clearServiceTerritoryRelatedCache', cmp.get("v.selectedServiceTerritories"));
        helper.clearSelectedCustomers(cmp);
        helper.clearSelectedSites(cmp);
        helper.clearSelectedEquipmentTypes(cmp);
        helper.clearSelectedWorkOrders(cmp);
    },
    clearCustomerRelatedCache: function(cmp, event, helper){
        helper.clearSelectedSites(cmp);
        helper.clearSelectedEquipmentTypes(cmp);
        helper.clearSelectedWorkOrders(cmp);
    },
    clearSiteRelatedCache: function(cmp, event, helper){
        helper.clearSelectedEquipmentTypes(cmp);
        helper.clearSelectedWorkOrders(cmp);
    },
    clearWorkOrdeRelatedCache: function(cmp, event, helper){
        helper.clearSelectedWorkOrders(cmp);
    },
    contractedToggle: function(cmp, event, helper){
        
        var boxChkContracted = cmp.find("chkContracted");
        var boxChkNonContracted = cmp.find("chkNoncontracted");

        var chkContracted = boxChkContracted.get("v.checked");
        var chkNonContracted = boxChkNonContracted.get("v.checked");

        boxChkContracted.setCustomValidity("");
        boxChkNonContracted.setCustomValidity("");

        if (!chkContracted && !chkNonContracted){
            boxChkContracted.setCustomValidity(" ");
            boxChkNonContracted.setCustomValidity("One checkbox must be selected before continuing.");

            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'pester',
                type: "warning",
                title: "Warning!",
                message: "One checkbox must be selected before continuing."
            });
            toastEvent.fire();

            cmp.set("v.disableStep", true);

        } else {    
            cmp.set("v.contracted", boxChkContracted.get("v.checked"));
            cmp.set("v.noncontracted", boxChkNonContracted.get("v.checked"));
            cmp.set("v.disableStep", false);
        }
        boxChkContracted.reportValidity(); // Tells lightning:input to show the error right away without needing interaction
        boxChkNonContracted.reportValidity(); // Tells lightning:input to show the error right away without needing interaction
        /*
        var chkContracted = cmp.find("chkContracted");
        var chkNoncontracted = cmp.find("chkNoncontracted");
        cmp.set("v.contracted", chkContracted.get("v.checked"));
        cmp.set("v.noncontracted", chkNoncontracted.get("v.checked"));
        */
        
    },
    startOver: function(cmp, event, helper){
        helper.resetWorkOrderGenerator(cmp);
    }
})