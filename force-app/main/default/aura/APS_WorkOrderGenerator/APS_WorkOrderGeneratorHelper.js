({
    showToggleSpinner: function (showSpinner) {
		var toggleSpinner = $A.get("e.c:APS_ToggleLoadingSpinner");
		toggleSpinner.setParams({
			"showSpinner": showSpinner
		});

		toggleSpinner.fire();
	},
	clearSelectedCustomers: function(cmp) {
		cmp.set("v.selectedCustomers", []);
	},
	clearSelectedSites: function(cmp) {
		cmp.set("v.selectedSites", []);
	},
	clearSelectedEquipmentTypes: function(cmp) {
		cmp.set("v.selectedEquipmentTypes", []);
		cmp.set("v.selectedEquipmentTypePills", []);
	},
	clearSelectedWorkOrders :function(cmp) {
		cmp.set("v.selectedWorkOrders", []);
	},
	approveWorkOrdersOrexportToCsv : function(cmp, event, actionType) {
		var selectedWorkOrderIndices = cmp.get("v.selectedWorkOrders");
        var selectedServiceTerritories = cmp.get("v.selectedServiceTerritories");
        var dueDateFrom = cmp.get("v.dueDateFrom");
        var dueDateTo = cmp.get("v.dueDateTo");
        var contracted = cmp.get("v.contracted");
        var noncontracted = cmp.get("v.noncontracted");
        var selectedServicePeriods = cmp.get("v.selectedServicePeriods");
        var selectedCustomers = cmp.get("v.selectedCustomers");
        var selectedSites = cmp.get("v.selectedSites");
        var selectedEquipmentTypes = cmp.get("v.selectedEquipmentTypes");

        var selectedWorkOrders = [];
		var workOrders = cmp.get("v.workOrders");
		
		if(actionType == 'approveWorkOrders') {
			selectedWorkOrderIndices.forEach(rowIndex => {
				selectedWorkOrders.push(workOrders[rowIndex]);
			});
		}
		else {
			selectedWorkOrders = workOrders;
		}

        var action = cmp.get("c.approveOrDownloadWorkOrders");

        action.setParams({ 
			guid : cmp.get("v.guid"),
            workOrdersJSON : JSON.stringify(selectedWorkOrders),            
            selectedServiceTerritories : JSON.stringify(selectedServiceTerritories),
            dueDateFrom : JSON.stringify(dueDateFrom),
            dueDateTo : JSON.stringify(dueDateTo),
            contracted : JSON.stringify(contracted),
            noncontracted : JSON.stringify(noncontracted),
            selectedServicePeriods : JSON.stringify(selectedServicePeriods),
            selectedCustomers : JSON.stringify(selectedCustomers),
            selectedSites : JSON.stringify(selectedSites),
            selectedEquipmentTypes : JSON.stringify(selectedEquipmentTypes),
            actionType : actionType
        });

        action.setCallback(this, function(response) {
			console.log('?!?');
			//allanah uncommented to get this to run
			//helper.showToggleSpinner(false);
			console.log('???');
			var state = response.getState();
			console.log('state',response.getState());
            if (state === "SUCCESS") {
				console.log('!!!');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({                    
                    "title": "Success!",
                    "type": "success",
                    "duration" : "30000",
                    "message": "Request has been submitted successfully. Work orders and line items are being processed now. You will receive an email notification regarding the process outcome."
                });

				//reset data
				this.resetWorkOrderGenerator(cmp);
                toastEvent.fire();
            }
            else if (state === "ERROR") {
				var errors = response.getError();
				message.showErrors(errors);
            }
        });
        $A.enqueueAction(action);
	},
	getUrlParams : function(cmp) {
		let myPageRef = cmp.get("v.pageReference");
		console.log(myPageRef.state.c__guid);
		let guid = myPageRef.state.c__guid;
		cmp.set("v.guid", guid);
		//get draft workorder
		if(guid) {
			this.getDraftJson(cmp, guid);
		}
	},
	getDraftJson : function(cmp, guid) {
		var action = cmp.get("c.retrieveDraftJson");
		action.setParams({
			'guid' : guid
		});
		action.setCallback(this, function(response) {
            //this.showToggleSpinner(false);
            var state = response.getState();
            if (state === "SUCCESS") {
				console.log('draft: ', response.getReturnValue());
				let draft = JSON.parse(response.getReturnValue());
				if (draft != null) {
					cmp.set("v.draftWorkOrder", draft);
					//populate component
					this.prepopulateWorkOrderGenerator(cmp);
				}
            }
            else if (state === "ERROR") {
				var errors = response.getError();
				message.showErrors(errors);
            }
        });

        $A.enqueueAction(action);
	},
	prepopulateWorkOrderGenerator : function(cmp) {
		let draftWorkOrder = cmp.get("v.draftWorkOrder");
		
		//populate data
		cmp.set("v.selectedServiceTerritories", draftWorkOrder.selectedServiceTerritories);
		cmp.set("v.selectedServicePeriods", draftWorkOrder.selectedServicePeriods);
		cmp.set("v.selectedCustomers", draftWorkOrder.selectedCustomers);
		cmp.set("v.selectedSites", draftWorkOrder.selectedSites);
		cmp.set("v.selectedEquipmentTypes", draftWorkOrder.selectedEquipmentTypes);
		cmp.set("v.noncontracted", draftWorkOrder.noncontracted);
		cmp.set("v.contracted", draftWorkOrder.contracted);
		cmp.set("v.dueDateTo", draftWorkOrder.dueDateTo);
		cmp.set("v.dueDateFrom", draftWorkOrder.dueDateFrom);

		//jump to the review screen
		cmp.set("v.currentStep", "Review");
	},
	resetWorkOrderGenerator : function(cmp) {
		//reset data
		cmp.set("v.selectedServiceTerritories", []);
		cmp.set("v.selectedServicePeriods", []);
		var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
		cmp.set('v.dueDateFrom', today);
		cmp.set('v.dueDateTo', today);
		cmp.set("v.contracted", true);
		cmp.set("v.noncontracted", false);

		// restart from the first step
		cmp.set("v.currentStep", "Service Territory");
		var pageReference = {
			type: "standard__navItemPage",
			attributes: {
				apiName: "Work_Order_Generator"    
			}
		};

		var navService = cmp.find("navService");
		// event.preventDefault();
		navService.navigate(pageReference);
	}
})