({  
    doInit: function(component, event, helper) {
       var recordid = component.get("v.recordId");
       console.log("recordid " +component.get("v.recordId") );
       var getHasEditAccess = component.get("c.entitlementAccess");
       getHasEditAccess.setParams({
			workorderId : recordid
	   }); 
       helper.callApex(getHasEditAccess)
		.then(function(result) {
          
                console.log("hasEditAccess " +result );
                component.set("v.hasEditAccess", result);	
          
            $A.util.addClass(component.find("Spinner"), "slds-hide");  
        })
        .catch(function(errors) {
            $A.util.addClass(component.find("Spinner"), "slds-hide");
 			
            var errorMessage = '';
            for (var i=0; i<errors.length; i++) {
                errorMessage += errors[i].message;
            }
            
            console.log("errorMessage =" + errorMessage);

            helper.showToast(errorMessage, "Error", "error");
        });
       var getEntitlement = component.get("c.fetchEntitlmentName");
       getEntitlement.setParams({
			workorderId : component.get("v.recordId")
	   }); 
       helper.callApex(getEntitlement)
		.then(function(result) {
            if (result) {
                console.log("currentEntitlement " +result );
                component.set("v.currentEntitlement", result);	
            }
            
            $A.util.addClass(component.find("Spinner"), "slds-hide");  
            
        })
        .catch(function(errors) {
            $A.util.addClass(component.find("Spinner"), "slds-hide");
 			
            var errorMessage = '';
            for (var i=0; i<errors.length; i++) {
                errorMessage += errors[i].message;
            }
            
            console.log("errorMessage =" + errorMessage);

            helper.showToast(errorMessage, "Error", "error");
        });
       var getEntitlementList = component.get("c.FetchEntitlements");
	   getEntitlementList.setParams({
			workorderId : component.get("v.recordId")
	   }); 
        helper.callApex(getEntitlementList)
		.then(function(result) {
           
             console.log("searchResult " +result );
             console.log("Length " +result.length );
             component.set('v.searchResult', result);	
           
             $A.util.addClass(component.find("Spinner"), "slds-hide");  
            
        })
        .catch(function(errors) {
            $A.util.addClass(component.find("Spinner"), "slds-hide");

            var errorMessage = '';
            for (var i=0; i<errors.length; i++) {
                 console.log('errorMessage', errors[i].message);
                errorMessage += errors[i].message;
            }
            helper.showToast(errorMessage, "Error", "error");
        });
        
    },
    Search: function(component, event, helper) {
        var searchField = component.find('searchField');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        // if value is missing show error message and focus on field
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
          // else call helper function 
            helper.SearchHelper(component, event);
        }
    },
    openModal: function(component, event, helper) {
        component.set("v.isModalOpen",true);
    },
    onClickCloseModal: function(component, event, helper) {
        component.set("v.isModalOpen",false);
    },
    selectEntitlement: function(component, event, helper) {
         var userId = event.getSource().get("v.value");
         var recordId = component.get("v.recordId");
         console.log("userId", userId);
          console.log("recordid " + recordId );
         var getEntitlement = component.get("c.updateEntitlmentName");
       
	   getEntitlement.setParams({
           entitlementId : userId, 
		   workorderId : recordId
	   }); 
       helper.callApex(getEntitlement)
		.then(function(result) {
            if (result) {
                component.set("v.currentEntitlement", result);	
                component.set("v.isModalOpen",false);
                $A.get('e.force:refreshView').fire();
            }
            
        })
        .catch(function(errors) {
            $A.util.addClass(component.find("Spinner"), "slds-hide");
 			
            var errorMessage = '';
            for (var i=0; i<errors.length; i++) {
                errorMessage += errors[i].message;
            }
            
            console.log("errorMessage =" + errorMessage);

            helper.showToast(errorMessage, "Error", "error");
        });
    }
})