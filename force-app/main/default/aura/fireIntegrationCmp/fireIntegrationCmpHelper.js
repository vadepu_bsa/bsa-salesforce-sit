({
	 callApex : function(action) {
        var p = new Promise($A.getCallback(function(resolve,reject) {
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                
                if (state === "SUCCESS") {
                    resolve(response.getReturnValue());
                }
                else if (state === "ERROR") {
                    console.log("Error: ", response.getError());
                    reject(response.getError());
                }
            });
            
            $A.enqueueAction(action);
        }));
        
        return p;
	},
    showToast: function(message, title, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    }
})