({
	 sendToPronto: function(component, event, helper) {
       var recordid = component.get("v.recordId");
       console.log("recordid " +recordid );
       var objectName = component.get("v.sObjectName");
       console.log("objectName " +objectName );
       var getHasEditAccess = component.get("c.execute");
       getHasEditAccess.setParams({
		   recId   : recordid,
           ObjName : objectName
	   }); 
       helper.callApex(getHasEditAccess)
		.then(function(result) {
          
            console.log("result " +result );
            
            $A.util.addClass(component.find("Spinner"), "slds-hide");  
            helper.showToast('Record has been sent to Pronto', "Info", "info");
         
        })
        .catch(function(errors) {
            $A.util.addClass(component.find("Spinner"), "slds-hide");
 			
            var errorMessage = '';
            for (var i=0; i<errors.length; i++) {
                errorMessage += errors[i].message;
            }
            
            console.log("errorMessage =" + errorMessage);

            helper.showToast(errorMessage, "Error", "error");
          
        });
     }
})