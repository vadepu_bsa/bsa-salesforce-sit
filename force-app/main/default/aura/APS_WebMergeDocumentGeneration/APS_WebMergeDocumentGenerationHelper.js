({
  doInit : function(component, event, helper) {

  },
  generatePDF : function(component,blobfile) {
      var pdfData = blobfile;
      $A.createComponent(
        "c:APS_pdfViewer",
          {
              "pdfData": pdfData
          },
          function(pdfViewer, status, errorMessage){
              if (status === "SUCCESS") {
                  var pdfContainer = component.get("v.pdfContainer");
                  pdfContainer.push(pdfViewer);
                  component.set("v.pdfContainer", pdfContainer);
              }
              else if (status === "INCOMPLETE") {
                  console.log("No response from server or client is offline.")
              }
              else if (status === "ERROR") {
                helper.showError(component, event, helper,'Error in generating preview of PDF');
                console.log("Error: " + errorMessage);
              }
        }
      );
  },
  generateDocument : function(component, event, helper) {
    component.set("v.spinner", true);
    component.set("v.showPreview",false);
    var action = component.get("c.WebMergeDocumentGenerationAura");
    action.setParams({	            
      "sObjectId" : component.get("v.recordId"), 
      "sObjectName" : component.get("v.sObjectName") ,
    });
    action.setCallback(this, function(response) {
        var state = response.getState();
        console.log('state==='+state);
        if (state === "SUCCESS") {
            console.log('response===' + JSON.stringify(response.getReturnValue()));
            if(response.getReturnValue().StatusCode == 201){
              var blobFile = response.getReturnValue().blobFile;
              if(!$A.util.isEmpty(blobFile))
              component.set("v.showPreview",true);
              helper.showSuccess(component, event, helper,'Document generated successfully in WebMerge');
              helper.generatePDF(component, blobFile);
            }
            else if(response.getReturnValue().StatusCode == 200){
              helper.showSuccess(component, event, helper,response.getReturnValue().Status);
            }
            else
            helper.showError(component, event, helper,'Error in generating document in WebMerge');
            component.set("v.spinner", false);
        }
        else if(state === "ERROR"){
          var errors = action.getError();
          if (errors) {
              if (errors[0] && errors[0].message) {
                helper.showError(component, event, helper, errors[0].message);
                component.set("v.spinner", false);
              }
          }
        }else if (status === "INCOMPLETE") {
          helper.showError(component, event, helper,'No response from server or client is offline.');
          component.set("v.spinner", false);
        }
    });
    $A.enqueueAction(action);
  } ,
  showSuccess : function(component, event, helper,text) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        title : 'Success',
        message: text,
        duration:' 5000',
        key: 'info_alt',
        type: 'success',
        mode: 'pester'
    });
    toastEvent.fire();
},
showError : function(component, event, helper,text) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        title : 'Error',
        message: text,
        duration:' 5000',
        key: 'info_alt',
        type: 'error',
        mode: 'pester'
    });
    toastEvent.fire();
},
})