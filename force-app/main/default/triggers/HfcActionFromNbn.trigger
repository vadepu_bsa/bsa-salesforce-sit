trigger HfcActionFromNbn on Action_From_NBN_To_BSA__e (after insert) {
    
    Integer counter = 0;
    for(Action_From_NBN_To_BSA__e event : Trigger.new){
        if(counter > 1){break;}
        ApplicationLogUtility.addException(null, JSON.serializePretty(event), event.Correlation_ID__c, 'Info', BSA_ConstantsUtility.SUCCESS_EVENT_NAME);    

        try{
            ApplicationLogUtility.saveExceptionLog();        
         } catch(Exception excp){System.debug('Error occured while saving logs'+excp.getMessage()+excp.getStackTraceString());}

         EventBus.TriggerContext.currentContext().setResumeCheckpoint(event.ReplayId);
         List<Action_From_NBN_To_BSA__e> eventList = new List<Action_From_NBN_To_BSA__e>();
         eventList.add(event);
         
         HFCNbnToBsaAutomation.syncWorkOrdersWithSF(eventList);   
    }    
    
    
}