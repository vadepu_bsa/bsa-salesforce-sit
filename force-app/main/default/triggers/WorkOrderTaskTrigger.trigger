trigger WorkOrderTaskTrigger on Work_Order_Task__c (before insert, before update, after update, after insert) {
    TriggerDispatcher.Run(new WorkOrderTaskTriggerHandler());

}