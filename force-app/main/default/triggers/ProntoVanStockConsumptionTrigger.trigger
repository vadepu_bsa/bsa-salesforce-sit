trigger ProntoVanStockConsumptionTrigger on Pronto_Van_Stock_Consumption_Update__e (after insert) {

    // Iterate through each notification.
    for(Pronto_Van_Stock_Consumption_Update__e event : Trigger.new) {
        System.debug('Pronto_Van_Stock_Consumption_Update__e'+event);
        System.debug('Serial Number: ' + event.Serial_Number__c);
        System.debug('Sales order #'+event.Sales_Order_Number__c);
        System.debug('Delta Consumtion #'+event.Delta_consumption__c);
    }

}