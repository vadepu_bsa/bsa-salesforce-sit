/**
 * @author          David A (IBM)
 * @date            06/Aug/2020 
 * @description     Trigger for Order Item
 *
 * Change Date    Modified by           Description  
 * 06/Aug/2020    David A               Trigger on Order Item object
 **/

trigger OrderItemTrigger on OrderItem (before insert, before update, before delete, after update, after insert) {
    TriggerDispatcher.Run(new OrderItemTriggerHandler());
}