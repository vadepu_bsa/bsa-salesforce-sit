trigger SuccessEventTrigger on SF_FSL_SUCCESS_PROCESSING_EVENT__e (after insert) {
    
    Map<String,String> responseMap = new Map<String,String>();
    Map<String,String> productConsumptionMap = new Map<String,String>();
    List<Application_Log__c> lstException = new List<Application_Log__c>();
    
    
    // Iterate through each notification.
    for(SF_FSL_SUCCESS_PROCESSING_EVENT__e event : Trigger.new) {
    
        System.debug('inside for loop');
        System.debug('NBN_Parent_Work_Order_Id__c: ' + event.NBN_Parent_Work_Order_Id__c);
        System.debug('event.Response__c: ' + event.Response__c);
        
        if(event.Product_Consumed__c == null && event.Product_Transfer__c == null && event.NBN_Parent_Work_Order_Id__c == null && String.isBlank(event.Source__c)) {
            /*ApplicationLogUtility.addException(null, '', event.Response__c, 'Info', 'ProductItem');
            ApplicationLogUtility.saveExceptionLog();*/
        }
        else if(event.Product_Consumed__c != null) {
            System.debug('Sales_Order_Number__c: ' + event.Sales_Order_Number__c);
            productConsumptionMap.put(event.Product_Consumed__c,event.Sales_Order_Number__c);
        }
        
        /*if(event.NBN_Field_Work_ID__c != null && event.Response__c != null && event.NBN_Activity_State_Id__c == 'ACKNOWLEDGED:Step_SDP_Tech_On_Site') {
            responseMap.put(event.NBN_Field_Work_ID__c,event.Response__c);
        }*/
        
        // When a Success Event is invoked by MULE, it will be sending Source__c = 'API'
        if(String.isNotBlank(event.Source__c) && event.Source__c.equalsIgnoreCase(BSA_ConstantsUtility.SOURCE_API)) {
            
            ApplicationLogUtility.addException(null, event.Response__c, event.CorrelationID__c, 'Info', BSA_ConstantsUtility.SUCCESS_EVENT_NAME);
        }
        
    }
    ApplicationLogUtility.saveExceptionLog();
    
    if(!productConsumptionMap.isEmpty()) {
        ApplicationLogUtility.popupalteSalesOrderNumber(productConsumptionMap);
    }
    
    //Capturing logs of Action_NBN_Accept_ToBSA event in SF
    /*if(!responseMap.isEmpty()) {
        List<Application_Log__c> lstException = new List<Application_Log__c>();
        
        for(WorkOrder obj : [SELECT Id, NBN_Parent_Work_Order_Id__c FROM WorkOrder WHERE NBN_Parent_Work_Order_Id__c IN : responseMap.keyset()]) {
            Application_Log__c log = new Application_Log__c();
            log.Log_Level__c = 'Info';
            log.Message__c = 'Action_NBN_Accept_ToBSA : ACKNOWLEDGED';
            log.NBN_NA_JSON__c = responseMap.get(obj.NBN_Parent_Work_Order_Id__c);
            log.Work_Order__c = obj.Id;
            log.Object__c = 'WorkOrder';
            
            lstException.add(log);
        }
        
        if(!lstException.isEmpty()) {
            insert lstException;
        }
    }*/

}