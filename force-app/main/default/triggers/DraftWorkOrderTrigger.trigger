/**
 * @author          IBM - Bohao Chen
 * @date            20/10/2020
 * @description     Trigger for Draft Work Order Object
 *
 * Change Date    Modified by         Description  
 **/
trigger DraftWorkOrderTrigger on Draft_Work_Order__c (before insert, before update, after update, after insert) {    
   
    TriggerDispatcher.Run(new DraftWorkOrderTriggerHandler());
}