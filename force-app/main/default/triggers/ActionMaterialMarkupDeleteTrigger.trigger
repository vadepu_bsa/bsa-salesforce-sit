trigger ActionMaterialMarkupDeleteTrigger on Material_Markup__c (after update, after insert) {

    List<Material_Markup__c> tbdMM = [Select ID, Markup_Code__c FROM Material_Markup__c WHERE ID IN: trigger.new AND Operations__c = 'Delete'];
    
    Set<Id> markupID = new Set<Id>();
    for(Material_Markup__c mp:tbdMM ){
        markupID.add(mp.Markup_Code__c);
    }
    
    List<Markup_Code__c> tbdMC = [Select ID FROM Markup_Code__c WHERE ID IN: markupID];
    
    if(tbdMM.size() > 0)
        delete tbdMM;

    if(tbdMC.size() > 0)
        delete tbdMC;

}