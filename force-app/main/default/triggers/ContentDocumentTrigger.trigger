/**
 * @author          Eliana G
 * @date            12/Jan/2021 
 * @description     Trigger for ContentDocument Object
 *
 * Change Date    Modified by         Description  
 *  
 **/
trigger ContentDocumentTrigger on ContentDocument (before delete) {
  
    TriggerDispatcher.Run(new ContentDocumentTriggerHandler());
 
}