trigger Update_Response_Detail_Link on Response_Details__c (before insert) {
    
    Map<String,Id> responseLink = new Map<String,ID>();
    List<String> tempResponseHeaderId = New List<String>();
    
    for(Response_Details__c rd : Trigger.new){
        //Create a list of Response Link ID
        iF(Rd.Temp_Response_Header_Link__c != null){
            tempResponseHeaderId.add(rd.Temp_Response_Header_Link__c);   
        }
    }
    
    //system.debug('tempResponseHeaderId ---->'+ tempResponseHeaderId);
    
    List<Response_Header__c> ResponseHeaderList = [SELECT Temp_Response_Header_Id__c,ID 
                                                   from Response_Header__c
                                                   where Temp_Response_Header_Id__c in :tempResponseHeaderId];
    IF(ResponseHeaderList!= Null){
        
        For(Response_Header__c rh : ResponseHeaderList){  
            responseLink.put(rh.Temp_Response_Header_Id__c,rh.id);	       
        }
        
        //system.debug('responseLink---->' + ResponseLink);
        
        For(Response_Details__c rd : Trigger.new){ 
            // update Response header link
            ID ResponseHeaderId = responseLink.get(rd.Temp_Response_Header_Link__c);
            
            if(ResponseHeaderId != null){
                rd.Response__c = ResponseHeaderId;
            }
        }
    }
}