/* Author: Abhijeet Anand
 * Date: 17 October, 2019
 * Apex Trigger to perform relevant backend automation on every DML event on the FSL 'ServiceResource' SObject
 */
 trigger ServiceResourceTrigger on ServiceResource (before delete, before insert, before update,
                                    after delete, after insert, after update, after undelete) {

    TriggerDispatcher.Run(new ServiceResourceHandler());
    
}