/* Author: Abhijeet Anand
 * Date: 13 September, 2019
 * Apex Trigger to perform relevant backend automation on every DML event on the FSL Location sObject
 */
 trigger LocationTrigger on Location (before delete, before insert, before update,
                                    after delete, after insert, after update, after undelete) {

    TriggerDispatcher.Run(new LocationHandler());
    
}