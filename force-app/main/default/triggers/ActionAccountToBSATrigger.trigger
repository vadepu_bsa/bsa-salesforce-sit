trigger ActionAccountToBSATrigger on Action_Account_To_BSA__e (after insert) {

  System.debug('Start Time'+System.now());
    //counter to make sure that we process one event at a time
    Integer counter = 0; 
    for(Action_Account_To_BSA__e event : Trigger.new) { 
        counter++;
        if(counter > 1){break;}
        //Ensure one event is fired at a time
        EventBus.TriggerContext.currentContext().setResumeCheckpoint(event.ReplayId);
        IPlatformEventSubscription subscriptionHandler = new PlatformEventSubscriptionFactory(Action_Account_To_BSA__e.sObjectType,event.Action__c).gethandlerName();
        List<Action_Account_To_BSA__e> newlist = new list<Action_Account_To_BSA__e>();
        newlist.add(event);
        subscriptionHandler.process(Action_Account_To_BSA__e.sObjectType,newlist,Account.sObjectType); 
    }
    System.debug('End Time '+System.now());
    

}