/* Author: Abhijeet Anand
 * Date: 05 September, 2019
 * Apex Trigger to perform relevant backend automation on every DML event on Application Log sObject
 */
 trigger ApplicationLogTrigger on Application_Log__c (before delete, before insert, before update,
                                    after delete, after insert, after update, after undelete) {

    TriggerDispatcher.Run(new ApplicationLogHandler());
    
}