/**
 * @author          IBM - David A
 * @date            17/08/2020
 * @description     Trigger for Asset Object
 *
 * Change Date    Modified by         Description  
 * 17/Aug/2020    IBM - David A       Trigger on the Asset Object
 **/
trigger AssetTrigger on Asset (before insert, before update, after update, after insert, after delete) {    
   
    TriggerDispatcher.Run(new AssetTriggerHandler());
}