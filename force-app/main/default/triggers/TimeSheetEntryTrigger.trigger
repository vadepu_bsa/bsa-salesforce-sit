/**
 * @File Name          : TimeSheetTrigger.trigger
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 25/08/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    25/08/2020         Tom Henson             Initial Version
**/
trigger TimeSheetEntryTrigger on TimeSheetEntry (after update, after insert,before insert, before update, before delete) {
          if(trigger.isDelete) {
            String profileName = [select Name from profile where id = :UserInfo.getProfileId()].Name;
            for (TimesheetEntry GettingRecord: trigger.old) {
                    //checking if the current Status is Submitted or not, if it is, stop the tech to delete.
                    if (trigger.oldMap.get(GettingRecord.Id).status == 'Submitted' && profileName == 'APS Field Technician') {
                        GettingRecord.addError('Apex Trigger Error: Sorry, you cannot delete Submitted Timesheet Entries!');
                    }
             }
            }
else {
      TriggerDispatcher.Run(new TimeSheetEntryTriggerHandler());
    }
}