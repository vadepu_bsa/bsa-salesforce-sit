trigger CustomAttributeTrigger on Custom_Attribute__c (before insert, before update, after update, after insert) {
    TriggerDispatcher.Run(new CustomAttributeTriggerHandler());

}