trigger BSA_ShiftTrigger on Shift (before insert, before update, before delete, after insert, after update, after delete) {
    
    ASPR_TriggerFactory.createHandler(Shift.sObjectType);
    
}