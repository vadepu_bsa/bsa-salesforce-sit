/* Author: Abhijeet Anand
* Date: 16 September, 2019
* Apex Trigger for catching Pronto.Van.Warehouse.Replenished.Updated events getting fired from Pronto via Mulesoft
*/
trigger ProntoStockReplenishment on Pronto_Van_Warehouse_Replenished_Updated__e (after insert) {
    
    //Map<String,Pronto_Van_Warehouse_Replenished_Updated__e> stockMap = new Map<String,Pronto_Van_Warehouse_Replenished_Updated__e>();
    Map<String,List<Pronto_Van_Warehouse_Replenished_Updated__e>> stockMap = new Map<String,List<Pronto_Van_Warehouse_Replenished_Updated__e>>();
    Integer counter = 0;
    for(Pronto_Van_Warehouse_Replenished_Updated__e obj : Trigger.new) {
        counter++;
        if(counter > 1){break;}
        
        ApplicationLogUtility.addException(null, JSON.serializePretty(obj), obj.Correlation_ID__c, 'Info', BSA_ConstantsUtility.SUCCESS_EVENT_NAME); 
        try{
            
            ApplicationLogUtility.saveExceptionLog();      
        } catch(Exception excp){System.debug('Error occured while saving logs'+excp.getMessage()+excp.getStackTraceString());}
        if(obj.Stock_Code__c != null) {
            //stockMap.put(obj.Stock_Code__c,obj);
            if(stockMap.containsKey(obj.Stock_Code__c)) {
                List<Pronto_Van_Warehouse_Replenished_Updated__e> peList = stockMap.get(obj.Stock_Code__c);
                peList.add(obj);
                stockMap.put(obj.Stock_Code__c,peList);
            } else {
                stockMap.put(obj.Stock_Code__c, new List<Pronto_Van_Warehouse_Replenished_Updated__e> { obj });
            }
        }
        
        if(!stockMap.isEmpty()) {
            
            System.debug('Replanish stock'+stockMap);
            System.debug('Replanish Trigger.new'+obj);
            EventBus.TriggerContext.currentContext().setResumeCheckpoint(obj.ReplayId);
            List<Pronto_Van_Warehouse_Replenished_Updated__e> eventList = new List<Pronto_Van_Warehouse_Replenished_Updated__e>();
            eventList.add(obj);
            ProntoStockReplenishmentAutomation.syncVanStockWithSF(eventList,stockMap);
        } 
    }
}