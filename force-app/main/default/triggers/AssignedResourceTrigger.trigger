/**
 * @author          Sunila.M
 * @date            18/May/2019 
 * @description     Trigger for Assigned Resource Object
 *
 * Change Date    Modified by         Description  
 * 18/May/2019      Sunila.M        Created Trigger on Assigned Resource object to remove WO share on Service repurce update or Assigned resource delete.
 **/
trigger AssignedResourceTrigger on AssignedResource (before update, before delete, after update) {
    
    TriggerDispatcher.Run(new AssignedResourceTriggerHandler());
    
}