/* Author: Abhijeet Anand
 * Date: 08 October, 2019
 * Apex Trigger to perform relevant backend automation on every DML event on the ProductTransfer sObject
 */
 
 trigger ProductTransferTrigger on ProductTransfer (before delete, before insert, before update,
                                    after delete, after insert, after update, after undelete) {

    TriggerDispatcher.Run(new ProductTransferHandler());

}