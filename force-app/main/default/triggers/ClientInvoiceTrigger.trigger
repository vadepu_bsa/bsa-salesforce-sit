/**
* @author          Sunila.M
* @date            09/April/2019 
* @description     
*
* Change Date   	 Modified by         Description  
* 09/April/2019   	 Sunila.M        	Created the Client Invoice trigger to avoid more than 1 Client Invoice
**/
trigger ClientInvoiceTrigger on Client_Invoice__c (before insert) {

    TriggerDispatcher.Run(new ClientInvoiceTriggerHandler());
}