/* Author: Abhijeet Anand
 * Date: 02 December, 2019
 * Apex Trigger to perform relevant backend automation on every DML event on the NBN Log Entry sObject
 */
 
 trigger LogEntryTrigger on NBN_Log_Entry__c (before delete, before insert, before update,
                                    after delete, after insert, after update, after undelete) {

    TriggerDispatcher.Run(new LogEntryHandler());

}