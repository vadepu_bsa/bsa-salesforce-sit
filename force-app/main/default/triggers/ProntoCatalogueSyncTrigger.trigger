/* Author: Abhijeet Anand
* Date: 09 September, 2019
* Apex Trigger for catching Pronto.Catalogue.Product events getting fired from Pronto via Mulesoft
*/
trigger ProntoCatalogueSyncTrigger on Pronto_Catalogue_Product_Created_Updated__e (after insert) {    
   
    for(Pronto_Catalogue_Product_Created_Updated__e event : Trigger.new){
        ApplicationLogUtility.addException(null, JSON.serializePretty(event), event.Correlation_ID__c, 'Info', BSA_ConstantsUtility.SUCCESS_EVENT_NAME);        
    } 
    
    try{
        ApplicationLogUtility.saveExceptionLog();        
    } catch(Exception excp){System.debug('Error occured while saving logs'+excp.getMessage()+excp.getStackTraceString());}
    
    ProntoCatalogueSyncAutomation.syncProntoProductsWithSF(Trigger.new);
}