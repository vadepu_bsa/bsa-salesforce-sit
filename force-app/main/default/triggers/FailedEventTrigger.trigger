trigger FailedEventTrigger on SF_FSL_FAILED_PROCESSING_EVENT__e (after insert) {
    
    Map<String,String> productConsumptionMap = new Map<String,String>();
    Map<String,String> productTransferMap = new Map<String,String>();
    List<Application_Log__c> lstException = new List<Application_Log__c>();
    Boolean isTriggerByAPI = false;
    
    for(SF_FSL_FAILED_PROCESSING_EVENT__e event : Trigger.new) {
        
        // When a Success Event is invoked by MULE, it will be sending Source__c = 'API' 
        if(String.isNotBlank(event.Source__c) && event.Source__c.equalsIgnoreCase(BSA_ConstantsUtility.SOURCE_API)) {
            
            ApplicationLogUtility.addException(null, event.Response__c, event.Correlation_ID__c, 'Error', BSA_ConstantsUtility.FAILURE_EVENT_NAME);
            isTriggerByAPI = true;
        }
        System.debug('Product_Consumed__c: ' + event.Product_Consumed__c);
        System.debug('Product_Transfer__c: ' + event.Product_Transfer__c);
        if(event.Product_Consumed__c == null && event.Product_Transfer__c == null && String.isBlank(event.Source__c)) {
            ApplicationLogUtility.addException(null, '', event.Response__c, 'Error', 'ProductItem');
            ApplicationLogUtility.saveExceptionLog();
        }
        else if(event.Product_Consumed__c != null) {
            productConsumptionMap.put(event.Product_Consumed__c,event.Response__c);
        }
        else if(event.Product_Transfer__c != null) {
            System.debug('Product_Transfer__c: ' + event.Product_Transfer__c);
            productTransferMap.put(event.Product_Consumed__c,event.Response__c);
        }
    }
    
    if(!productConsumptionMap.isEmpty()) {
        for(ProductConsumed obj : [SELECT Id, ProductConsumedNumber FROM ProductConsumed WHERE ProductConsumedNumber IN : productConsumptionMap.keyset()]) {
            String msg = productConsumptionMap.get(obj.ProductConsumedNumber);
            ApplicationLogUtility.addException(obj.Id, '', msg, 'Error', 'ProductConsumed');
        }
        
        ApplicationLogUtility.saveExceptionLog();
        
    }
    
    if(!productTransferMap.isEmpty()) {
        for(ProductTransfer obj : [SELECT Id, ProductTransferNumber FROM ProductTransfer WHERE ProductTransferNumber IN : productTransferMap.keyset()]) {
            String msg = productTransferMap.get(obj.ProductTransferNumber);
            ApplicationLogUtility.addException(obj.Id, '', msg, 'Error', 'ProductTransfer');
        }
        
        ApplicationLogUtility.saveExceptionLog();
        
    }
    
    if(isTriggerByAPI){
        
         ApplicationLogUtility.saveExceptionLog();
    }

}