/**
 * @author          Sunila.M
 * @date            07/March/2019 
 * @description     Trigger for ContentDocumentLink Object to make the files related to a Work Order visible to 
 *                  technician's in the Community.
 *
 * Change Date    Modified by       Description  
 * 07/March/2019    Sunila.M        Trigger on ContentDocumentLink object
 * 11/Jan/2021      Eliana G        before delete added  
 **/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, after insert, before delete) {

    System.debug(' before call ContentDocumentLinkShareHandler');
    TriggerDispatcher.Run(new ContentDocumentLinkShareHandler());
}