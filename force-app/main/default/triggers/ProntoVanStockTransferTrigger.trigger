trigger ProntoVanStockTransferTrigger on Pronto_Van_Stock_Transfer_Update__e (after insert) {

    // Iterate through each notification.
    for(Pronto_Van_Stock_Transfer_Update__e event : Trigger.new) {
        System.debug('Product Transfer Number: ' + event.Product_Transfer_Number__c);
        System.debug('Source Product Code: ' + event.Source_Product_Code__c);
        System.debug('Van ID (Source) : ' + event.Van_ID_Source__c);
        System.debug('Is Received : ' + event.Is_Received__c);
        System.debug('Van ID (Destination) : ' + event.Van_ID_Destination__c);
        System.debug('Serial Number : ' + event.Serial_Number__c);
    }

}