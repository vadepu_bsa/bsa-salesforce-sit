/**
 * @author          Dildar Hussain
 * @date            06/March/2020
 * @description     Trigger for Invoice Custom Object
 *
 **/
trigger InvoiceTrigger on Invoice__c (after update) {    
   
    TriggerDispatcher.Run(new InvoiceTriggerHandler());
}