/**
 * @File Name          : OrderTrigger.trigger
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 25/08/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/08/2020         Tom Henson             Initial Version
**/
trigger OrderTrigger on Order (before insert, after update,before update) {
    TriggerDispatcher.Run(new OrderTriggerHandler());
}