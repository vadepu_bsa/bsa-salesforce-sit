/**
 * @author          Dildar Hussain
 * @date            26/Nov/2019 
 * @description     Trigger for Work Order Line Item Object
 *
 * Change Date    Modified by         Description  
 * 29/Jun/2020    Bohao Chen @IBM     JIRA FSL3-11 create equipment task work order line items
 **/
trigger WorkOrderLineItemTrigger on WorkOrderLineItem (after update, after insert) {
    //TODO - enable
    TriggerDispatcher.Run(new WorkOrderLineItemTriggerHandler());
}