/**
 * @author          Sunila.M
 * @date            08/Feb/2019 
 * @description     Trigger for Work Order Object
 *
 * Change Date    Modified by         Description  
 * O8/Feb/2019      Sunila.M        Trigger on Work Order object
 **/
trigger WorkOrderTrigger on WorkOrder (before insert, before update, after update, after insert,before delete) {    
   
    TriggerDispatcher.Run(new WorkOrderTriggerHandler());
}