/* Author: Abhijeet Anand
 * Date: 26 September, 2019
 * Apex Trigger to perform relevant backend automation on every DML event on the ProductItem sObject
 */
 
trigger ProductItemTrigger on ProductItem (before delete, before insert, before update,
                                    after delete, after insert, after update, after undelete) {
                                    
    TriggerDispatcher.Run(new ProductItemHandler());

}