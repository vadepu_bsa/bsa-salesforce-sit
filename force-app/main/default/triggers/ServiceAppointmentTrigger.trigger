/**
* @author          Sunila.M
* @date            23/April/2019 
* @description     
*
* Change Date        Modified by         Description  
* 23/April/2019      Sunila.M           Created Trigger to restrict more than one Service Appointment for same Work Order
**/
trigger ServiceAppointmentTrigger on ServiceAppointment (before insert, after insert, after update, before delete) {
    
    TriggerDispatcher.Run(new ServiceAppointmentTriggerHandler());
    
}