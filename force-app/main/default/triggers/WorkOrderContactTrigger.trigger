trigger WorkOrderContactTrigger on Work_Order_Contact__c (after insert, before insert) {
	TriggerDispatcher.Run(new WorkOrderContactTriggerHandler());
}