/**
 * @author          Sunila.M
 * @date            11/Feb/2019 
 * @description     Trigger for Product Consumed Object
 *
 * Change Date    Modified by         Description  
 * 11/Feb/2019      Sunila.M        Trigger on Product Consumed object
 **/
trigger ProductConsumedTrigger on ProductConsumed (before insert, before update, before delete, after insert, after update) {

    TriggerDispatcher.Run(new ProductConsumedTriggerHandler());
}