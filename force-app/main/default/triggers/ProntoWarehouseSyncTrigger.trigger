/* Author: Abhijeet Anand
* Date: 11 September, 2019
* Apex Trigger for catching Pronto.Van.Warehouse events getting fired from Pronto via Mulesoft
*/
trigger ProntoWarehouseSyncTrigger on Pronto_Van_Warehouse_Created_Updated__e (after insert) {    
    
    for(Pronto_Van_Warehouse_Created_Updated__e event : Trigger.new){
        ApplicationLogUtility.addException(null, JSON.serializePretty(event), event.Correlation_ID__c, 'Info', BSA_ConstantsUtility.SUCCESS_EVENT_NAME);        
    } 
    
    try{
        ApplicationLogUtility.saveExceptionLog();        
    } catch(Exception excp){System.debug('Error occured while saving logs'+excp.getMessage()+excp.getStackTraceString());}
    ProntoWarehouseSyncAutomation.syncProntoWarehouseWithSF(Trigger.new);
}