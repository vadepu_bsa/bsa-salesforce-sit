trigger NBNLogEntryTrigger on NBN_Log_Entry__c (before insert, before update, after update, after insert) {
    TriggerDispatcher.Run(new NBNLogEntryTriggerHandler());

}