trigger ActionLabourRateDeleteTrigger on Labour_Rate__c (after update, after insert) {

    List<Labour_Rate__c> tbdLR = [Select ID, Operations__c FROM Labour_Rate__c WHERE ID IN: trigger.new AND Operations__c = 'Delete'];
	if(tbdLR.size() > 0)
        delete tbdLR;
}