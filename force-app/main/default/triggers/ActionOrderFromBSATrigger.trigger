trigger ActionOrderFromBSATrigger on Action_Order_From_BSA__e (after insert) {
       
    System.debug('Start Time'+System.now());
    //counter to make sure that we process one event at a time
    Integer counter = 0; 
    for(Action_Order_From_BSA__e event : Trigger.new) { 
        counter++;
        if(counter > 1){break;}
        //Ensure one event is fired at a time
        EventBus.TriggerContext.currentContext().setResumeCheckpoint(event.ReplayId);
        //IPlatformEventSubscription subscriptionHandler = new PlatformEventSubscriptionFactory(Action_B2B_From_BSA__e.sObjectType,event.ServiceContractNumber__c).gethandlerName();
        //subscriptionHandler.process(Action_B2B_From_BSA__e.sObjectType,trigger.new,WorkOrder.sObjectType); 
        ActionOrderFromBSAHandler.captureB2BActionLog(event);
      
        system.debug('Mahmood !!! ReplayId =  '+ event.ReplayId);
        system.debug('Mahmood !!! CorrelationId__c =  '+ event.CorrelationId__c);
        system.debug('Mahmood !!! Action__c =  '+ event.Action__c);
     
        system.debug('Mahmood !!! EventDateTime__c =  '+ event.EventTimeStamp__c);
    }
    ApplicationLogUtility.saveExceptionLog();

}