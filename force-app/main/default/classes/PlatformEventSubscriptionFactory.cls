/**
 * @File Name          : PlatformEventSubscriptionFactory.cls
 * @Description        : Factory class to route inbound Platform Event to correct handler
 * @Author             : Karan Shekhar
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 02/06/2020, 4:52:04 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/05/2020   Karan Shekhar     Initial Version
**/
public class PlatformEventSubscriptionFactory {
    private sObjectType mPESobjectType ;  
    private String mImplExtIdentifier ;  
    
    //Private constructor to avoid accidental initialization of Factory
    private  PlatformEventSubscriptionFactory(){}
    
    public PlatformEventSubscriptionFactory(sObjectType peSobjectType, String implExtIdentifier){ 
        
        mPESobjectType = peSobjectType;
        mImplExtIdentifier = implExtIdentifier;
    }
    
    /**
    * @description : Method to get specific handler name. Identifying handler follows a naming pattern.
                    e.g. If PE API name id Action_B2B_To_BSA then handler name should be ActionB2BTtoBSAHandler.
                    Contract specific implementation should have contract name in front of it. e.g. NBNOMMAActionB2BTtoBSAHandler
    * @author Karan Shekhar | 04/05/2020 
    * @return IPlatformEventSubscription 
    **/
    public IPlatformEventSubscription gethandlerName(){ 
        System.debug('mPESobjectType'+mPESobjectType);
        String defaultImplClassName = String.valueOf(mPESobjectType).contains('__e') ?  (String.valueOf(mPESobjectType).replace('__e','Handler').remove('_')) : '';
        String specificImplClassName;
        List<PE_Subscription_Object_Action_Mapping__mdt> setting = [SELECT Service_Implementation_Int_Identifier__c FROM PE_Subscription_Object_Action_Mapping__mdt WHERE Service_Implementation_Ext_Identifier__c = :mImplExtIdentifier LIMIT 1];
        SYstem.debug('class settings'+setting);
        if(setting.size() > 0 && setting[0] !=null && String.isNotBlank(setting[0].Service_Implementation_Int_Identifier__c)){
            specificImplClassName = String.valueOf(mPESobjectType).contains('__e') ? setting[0].Service_Implementation_Int_Identifier__c + (String.valueOf(mPESobjectType).replace('__e','Handler').remove('_')) : '';

        }
          
        System.debug('defaultImplClassName'+defaultImplClassName);
        System.debug('specificImplClassName'+specificImplClassName);
        specificImplClassName = specificImplClassName == null ? defaultImplClassName: specificImplClassName;
        Type customType = Type.forName(specificImplClassName);  
        
        if(customType == null){
            customType = Type.forName(defaultImplClassName);  
        }
        return (IPlatformEventSubscription)customType.newInstance();        
        
    } 
    
}