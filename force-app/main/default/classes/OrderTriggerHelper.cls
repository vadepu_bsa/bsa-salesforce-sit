/**
* @author          David Azzi (IBM)
* @date            06/Aug/2020
* @description     Trigger Helper added for the Order trigger
* 
* Change Date        Modified by         Description  
* 10/May/2021        Ben Lee            Added condition to only update when there is a Pronto ID
*/


public with sharing class OrderTriggerHelper {


    public static void updateAcceptedOrderLineItems(Map<Id, SObject> newOrders, Map<Id, SObject> oldOrders){
        Id DefectQuoteRecordTypeId = Schema.Sobjecttype.Order.getRecordTypeInfosByDeveloperName().get('Defect_Quote').getRecordTypeId();
        List<Id> acceptedOrderIds = new List<Id>();
        for(Order order : (List<Order>)newOrders.Values())
        {
            Order oo = (Order) oldOrders.get(order.Id);
            if(order.Status == 'Accepted' && (order.Status != oo.Status && order.RecordTypeId == DefectQuoteRecordTypeId))
            {
                acceptedOrderIds.add(order.id);
            }
        }

        List<OrderItem> orderItemsToUpdate = new List<OrderItem>();
        for(OrderItem oi : [SELECT Status__c From OrderItem WHERE Status__c != 'Rejected' AND OrderId in :acceptedOrderIds FOR UPDATE])
        {
            oi.Status__c = 'Approved';
            orderItemsToUpdate.add(oi);
        }

        if(!orderItemsToUpdate.isEmpty())
        {
            update orderItemsToUpdate;
        }

    }

    public static void sentToPronto(List<Order> newOrderList, Map<Id, SObject> oldItems){
        Set<Id> OrdersToSendEvent = new Set<Id>();
        for (integer i = 0; i < newOrderList.size() ; i++) {
            Order oldPo = (Order)oldItems.get(newOrderList[i].Id);
            if(oldPo.Status != 'Accepted' && (newOrderList[i].Status == 'Accepted' || newOrderList[i].Status == 'Submitted') &&  newOrderList[i].Originator_Type__c == 'Technician'){
                Order GetData = [SELECT Id, Work_Order__r.Pronto_WO_ID__c FROM Order WHERE Id =: newOrderList[i].Id LIMIT 1];
                if(GetData != null && GetData.Work_Order__r.Pronto_WO_ID__c != null) {
                    OrdersToSendEvent.add(newOrderList[i].Id);
                }
            } else if ((newOrderList[i].Status == 'Accepted' || newOrderList[i].Status == 'Submitted')  && newOrderList[i].Originator_Type__c == 'Admin'){ 
                Order GetData = [SELECT Id, Work_Order__r.Pronto_WO_ID__c FROM Order WHERE Id =: newOrderList[i].Id LIMIT 1];
                if(GetData != null && GetData.Work_Order__r.Pronto_WO_ID__c != null) {
                    OrdersToSendEvent.add(newOrderList[i].Id);
                }
            }
        }
        if (OrdersToSendEvent.size() > 0){
            ProntoPurchaseOrderSerialize.generateProntoPurchaseOrderPlatformEvent(OrdersToSendEvent,null);
        }
    }
    public static void updateDefectStatusOnOrderSubmit(Map<Id, SObject> newOrders, Map<Id, SObject> oldItems){
        
         Id DefectQuoteRecordTypeId = Schema.Sobjecttype.Order.getRecordTypeInfosByDeveloperName().get('Defect_Quote').getRecordTypeId();
        List<Id> changedStatusOrder=new List<Id>();
        for(Order newOrd:(List<Order>)newOrders.values()){
            Order oldOrd=(Order)oldItems.get(newOrd.id);
            if((newOrd.Status != oldOrd.Status) && newOrd.RecordTypeId == DefectQuoteRecordTypeId){
                changedStatusOrder.add(newOrd.id);
            }
        }
        //Retriving Order Product
        system.debug('Printing changedStatusOrder::'+changedStatusOrder);
        List<OrderItem> orderItemList= new List<OrderItem>();
        List<Id> defectList=new List<Id>();
        List<Defect__c> defects=new List<Defect__c>();
        orderItemList=[select id,Defect__c,OrderId from OrderItem where OrderId IN: changedStatusOrder AND Status__c != 'Rejected'];
        Map<Id,id> defectIdVsOrderId=new Map<Id,id>();
        for(OrderItem ordItem:orderItemList){
                defectIdVsOrderId.put(ordItem.Defect__c,ordItem.OrderId);
        }
        system.debug('Printing defectList::'+defectList);
        //Retriving defects
        defects=[select id,Defect_Status__c from Defect__c where id IN:defectIdVsOrderId.keySet()];
        List<Defect__c> listofDefectsToUpdate=new List<Defect__c>();
        for(Defect__c def:defects){
            //def.Defect_Status__c='Quoted';
            Order ordr=(Order)newOrders.get(defectIdVsOrderId.get(def.id));
            if(ordr.status=='Submitted'){
                def.Defect_Status__c='Quoted';
            }else if(ordr.status=='Draft'){
                def.Defect_Status__c='Open';
            }else if(ordr.status=='Accepted'){
                def.Defect_Status__c='Approved';
            }else if(ordr.status=='Rejected'){
                def.Defect_Status__c='Rejected';
            }else if(ordr.status=='Rectified'){
                def.Defect_Status__c='Completed';
            }
            listofDefectsToUpdate.add(def);
        }
        system.debug('Printing listofDefectsToUpdate::'+listofDefectsToUpdate);
        if(listofDefectsToUpdate.size() > 0)
            update listofDefectsToUpdate;
        
    }
    public static void validateStatus(Map<Id, sObject> newOrders, Map<Id, sObject> oldItems){
        
        Boolean isError;
        String oldStatus;
        String newStatus;
        for(Order newOrd:(List<Order>)newOrders.values()){
            Order oldOrd=(Order)oldItems.get(newOrd.id);
            if(newOrd.status != oldOrd.Status){
                if((oldOrd.Status == 'Draft' && newOrd.Status != 'In Approval Process' && newOrd.Status != 'Submitted' && newOrd.Status != 'Cancelled') ||
                   (oldOrd.Status == 'Submitted' && newOrd.Status != 'Draft' && newOrd.Status != 'Accepted' && newOrd.Status != 'Rejected') ||
                   (oldOrd.Status == 'In Approval Process' && newOrd.Status != 'Draft' && newOrd.Status != 'Submitted') ||
                   (oldOrd.Status == 'Rejected' && newOrd.Status != 'Rejected') ||
                   (oldOrd.Status == 'Cancelled' && newOrd.Status != 'Cancelled') ||
                   (oldOrd.Status == 'Accepted' && newOrd.Status != 'Accepted')){
                       oldStatus=oldOrd.Status;newStatus=newOrd.Status;
                       newOrd.Status.addError('You cannot change the Status from '+oldStatus+' to '+newStatus+'.');
                       break;
                   }
            }
        }
        
        
    }
     public static void updateDefectFieldOnOrderCancelled(Map<Id, sObject> newOrders, Map<Id, sObject> oldItems){
        
        List<Order> cancelledOrder=new List<Order>();
        for(Order newOrd:(List<Order>)newOrders.values()){
            Order oldOrd=(Order)oldItems.get(newOrd.id);
            if(newOrd.status != oldOrd.Status){
                if(newOrd.status == 'Cancelled'){
                    cancelledOrder.add(newOrd);
                }
            }
        }
        List<Defect__c> defectList=new List<Defect__c>();
        if(cancelledOrder.size() >0 ){
            defectList=[select id,Last_Order_Number__c from Defect__c where Last_Order_Number__c IN: cancelledOrder];
            for(Defect__c d:defectList){
                d.Last_Order_Number__c=null;
            }
            update defectList;
        }

    }

}