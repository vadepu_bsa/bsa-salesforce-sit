@isTest
public class CreateMediaResourceBatchTest {
     @testSetup static void setup()
    {
        
        Account acc = NBNTestDataFactory.createTestAccounts();
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
        
    }
    public TestMethod static void testBatch(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseForMediaResource());
        WorkOrder wo = [select id from workorder where parentWorkOrderId=null limit 1];
        ContentDocumentLinkShareHandler.TriggerDisabled=true;
        ContentVersion cv = new ContentVersion();
            cv.Media_Id__c = 'abcd';
            //cv.ContentLocation = 'S';
            //cv.ContentDocumentId = contentDocumentId;
            cv.Title = 'Blank Image';
            cv.PathOnClient = 'BlankImage'+workorder.id+'.png';
            cv.VersionData = Blob.valueOf('iVBORw0KGgoAAAANSUhEUgAAAd8AAABtCAIAAABvF/zFAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGISURBVHhe7dQxAQAADMOg+Tfd2cgBIrgB0GNngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgA92wPLx5jXqhb7swAAAABJRU5ErkJggg==');
            cv.IsMajorVersion = false;
            cv.WOMediaId__c = wo.id;
        	
            insert cv;
        Map<Id,Id> ContentDocToWOMapToUpload = new Map<Id,Id>();
        ContentVersion cv1=[select Id,ContentDocumentId from ContentVersion where Id=:cv.Id];
        System.debug('SAYALI LOG cv1-->'+cv1);
        ContentDocToWOMapToUpload.put(cv1.ContentDocumentId,wo.Id);
        Test.startTest();
        CreateMediaResourceBatch batchId = new CreateMediaResourceBatch(ContentDocToWOMapToUpload);
              database.executeBatch(batchId,1 ); 
        /*NBNFileUploadDownloadHandler nbnHandler = New NBNFileUploadDownloadHandler(wo.Id,'',cv);
            nbnHandler.process(Label.Get_Media_Resource_Process);*/
        Test.stopTest();
        
        
        
    }

}