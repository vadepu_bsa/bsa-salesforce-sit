/* Author: Abhijeet Anand
 * Date: 09 October, 2019
 * Trigger Handler added for ProductTransferTrigger
 */
 
 public class ProductTransferHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //Before insert method 
    public void BeforeInsert(List<SObject> newItems) { 

        //Pass ProductTransfer List
        if(!newItems.isEmpty()) {
            ProductTransferAutomationBusinessLogic.populateCorrelationID(newItems);
        }

    }
    
    //After update method 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) { 
        
        List<SObject> productTransferList = new List<SObject>();

        //Pass ProductTRansfer List
        if(!newItems.isEmpty()) {
            productTransferList.addAll(newItems.values());
            ProductTransferAutomationBusinessLogic.populateProductItem (productTransferList,oldItems);
        }

    }
    
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {} public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}  public void AfterInsert(Map<Id, SObject> newItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}

}