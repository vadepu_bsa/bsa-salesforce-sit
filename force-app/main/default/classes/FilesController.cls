public class FilesController {
	
    /*@AuraEnabled
    public static List<ContentDocumentLink> fetchFiles(String linkedRecId){
        
		List<ContentDocumentLink> cdlList = [SELECT ContentDocumentId,ContentDocument.title FROM ContentDocumentLink WHERE LinkedEntityId =: linkedRecId];
        return cdlList;
        
        Map<String, CustomObject__c> myMap = new Map<String, CustomObject__c>(); 
for(CustomObject__c objCS : [Select z.Name, z.Id From CustomObject__c z])
        myMap.put(objCS.Name, objCS);
    }*/
    
    @AuraEnabled
    public static Map<String,List<ContentDocumentLink>> fetchFiles(String linkedRecId){
        
        Map<String,List<ContentDocumentLink>> woAttachmentsMap = new Map<String,List<ContentDocumentLink>>();
        List<WorkOrder> woList = [Select Id, WorkOrderNumber from WorkOrder 
                                  where ParentWorkOrderId =: linkedRecId ORDER BY WorkOrderNumber ASC];
        
        List<ContentDocumentLink> cdlList = [SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, 
                                             SystemModstamp,ShareType,Visibility FROM ContentDocumentLink 
                                             where LinkedEntityId IN (Select Id from WorkOrder 
                                                                      where ParentWorkOrderId =: linkedRecId) 
                                             								ORDER BY LinkedEntityId ASC];
        for(WorkOrder woObject: woList)
        {
            List<ContentDocumentLink> lstContentDocLink = new List<ContentDocumentLink>();
            for(ContentDocumentLink cdl: cdlList)
            {
                if(cdl.LinkedEntityId == woObject.Id)
                    lstContentDocLink.Add(cdl);
            }
            if(lstContentDocLink.size() > 0)
            {
            	woAttachmentsMap.put(woObject.WorkOrderNumber,lstContentDocLink);
            }
        }
        return woAttachmentsMap;
    }
    
}