global class BatchUpsertSimplexWO Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
	global String strQuery;
    public List<String> batchException = new List<String>();
    //public Id woId;
    public SObjectType locationType = ((SObject)(Type.forName('Schema.Location').newInstance())).getSObjectType();
    public SObjectType serviceResourceType = ((SObject)(Type.forName('Schema.ServiceResource').newInstance())).getSObjectType();
	//public SObjectField woRefId = getSObjectFieldName('WorkOrder', woExternalField);
	public String locationRelationshipName = WorkOrder.LocationId.getDescribe().getRelationshipName();
    public String srRelationshipName = WorkOrder.Service_Resource__c.getDescribe().getRelationshipName();
    //Initialize the query string
    global BatchUpsertSimplexWO(){
        Map<String,Schema.DisplayType> mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Simplex_Work_Order_Staging__c');
        String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        strQuery = 'SELECT '+fieldsForSOQL+' from Simplex_Work_Order_Staging__c where IsProcessed__c=false';
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Simplex_Work_Order_Staging__c> listStagings){
        Id simplexRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();
        List<WorkOrder> lstWorkOrdersToUpsert = new List<WorkOrder>();
        Map<String,WorkOrder> mExtIdWO = new Map<String,WorkOrder>();
        List<PE_Subscription_Field_DTO_Mapping__mdt> lstMapping =[select id,Payload_Key_Name__c,Mapped_Object_Field_API_Name__c from PE_Subscription_Field_DTO_Mapping__mdt where PE_Subscription_Object_Action_Mapping__r.DeveloperName='Setup_Simplex_WO' and Active__c=true and PE_Field_API_Name__c=''];
        List<PE_Subscription_Field_DTO_Mapping__mdt> lstMappingRelated =[select id,Payload_Key_Name__c,Mapped_Object_Field_API_Name__c from PE_Subscription_Field_DTO_Mapping__mdt where PE_Subscription_Object_Action_Mapping__r.DeveloperName='Setup_Simplex_WO' and Active__c=true and PE_Field_API_Name__c='FromRelated'];
        Map<String,Id> mSamIdToST = new Map<String,Id>();
        Map<String,Worktype> mNameToWT = new Map<String,Worktype>();
        Map<Id,Id> mStToPB = new Map<Id,Id>();
        Map<String,String> mKeyToGLCode = new Map<String,String>();
        Map<String,String> mKeyToPTCode = new Map<String,String>();
        List<Work_Order_Automation_Rule__c> lstWoars =[select id,recordtype.developername,record_Type_name__c,Pronto_Territory_Id__c, Service_Area_Module__c,Service_Territory__c,Price_Book__c,GL_Code__c,State__c,Primary_Access_Technology__c,Service_Contract__c from Work_Order_Automation_Rule__c where Service_Contract__c=:Label.Unify_Services_Simplex_Contract_Id and Active__c=true and (recordtype.developername ='Price_Book_Automation' or recordtype.developername ='Service_Territory_Automation' or recordtype.developername ='GL_Code_Automation' or recordtype.developername ='Pronto_Territory_Automation')];
        List<WorkType> lstWtType = [Select Id, Name from WorkType where CUI__c =true];
        for(WorkType wtObj:lstWtType){
            mNameToWT.put(wtObj.Name,wtObj);
        }
        for(Work_Order_Automation_Rule__c objWoar:lstWoars){
            if(objWoar.recordtype.developername =='Service_Territory_Automation'){
            	mSamIdToST.put(objWoar.Service_Area_Module__c,objWoar.Service_Territory__c);
            }
            if(objWoar.recordtype.developername =='Price_Book_Automation' && objWoar.record_Type_name__c =='Simplex_CUI_Work_Order'){
            	mStToPB.put(objWoar.Service_Territory__c,objWoar.Price_Book__c);
            }
            if(objWoar.recordtype.developername =='GL_Code_Automation'){
            	String key = objWoar.State__c + objWoar.Primary_Access_Technology__c;
            	mKeyToGLCode.put(key,objWoar.GL_CODE__c);
            }
            if(objWoar.recordtype.developername =='GL_Code_Automation' && objWoar.record_Type_name__c =='Simplex_CUI_Work_Order'){
            	String key = objWoar.State__c + objWoar.Primary_Access_Technology__c;
            	mKeyToGLCode.put(key,objWoar.GL_CODE__c);
            }
            if(objWoar.recordtype.developername =='Pronto_Territory_Automation' && objWoar.record_Type_name__c =='Simplex_CUI_Work_Order'){
            	String key = objWoar.State__c + objWoar.Primary_Access_Technology__c;
            	mKeyToPTCode.put(key,objWoar.Pronto_Territory_Id__c);
            }
            System.debug('mKeyToPTCode=='+mKeyToPTCode);
            
        }
        for(Simplex_Work_Order_Staging__c woStaging:listStagings){
            Workorder woObj = new WorkOrder();
            woObj.RecordTypeId = simplexRecordTypeId;
            for(PE_Subscription_Field_DTO_Mapping__mdt objMapping : lstMapping){
                
                if(objMapping.Mapped_Object_Field_API_Name__c =='Status' && woStaging.WO_Status__c =='Complete WO'){
                    woObj.Status='Completed';
                }
                else{
                	woObj.put(objMapping.Mapped_Object_Field_API_Name__c,woStaging.get(objMapping.Payload_Key_Name__c));
                }
            }
            String pcrGroup ='';
            if(woStaging.Problem_Code__c !=null && woStaging.Problem_Code__c!=''){
                pcrGroup =woStaging.Problem_Code__c;
            }
            if(woStaging.Cause_Code__c !=null && woStaging.Cause_Code__c!=''){
                pcrGroup =pcrGroup + ' / '+woStaging.Cause_Code__c;
            }
            if(woStaging.Resolution_Code__c !=null && woStaging.Resolution_Code__c!=''){
                pcrGroup =pcrGroup + ' / '+woStaging.Resolution_Code__c;
            }
            woObj.PCR_Group__c = pcrGroup;
            
            //woObj.Sub_Status__c ='Not Validated';
            ServiceResource serviceResObj = new ServiceResource(
            Enable_Id__c=woStaging.Location_Enable_Id__c);                
         	woObj.Service_Resource__r = serviceResObj;
            
            //Populate Service Territory & PriceBook
            if(woObj.SAM_ID__c!=null && woObj.SAM_ID__c!='' && mSamIdToST!=null){
            	woObj.ServiceTerritoryId = mSamIdToST.get(woObj.SAM_ID__c);
                if(mStToPB!=null){
                   woObj.Pricebook2Id =  mStToPB.get(woObj.ServiceTerritoryId);
                }
            }
            if(mNameToWT!=null && mNameToWT.get(woObj.NBN_Field_Work_Specified_By_Id__c)!=null){
                woObj.WorkTypeId = mNameToWT.get(woObj.NBN_Field_Work_Specified_By_Id__c).Id;
            }
            
            //Populate GL Code & PT ID
            if(woObj.State!='' && woObj.Primary_Access_Technology__c!=null){
                String key =woObj.State+woObj.Primary_Access_Technology__c;
                System.debug('SAYALI Key=='+key);
                if(mKeyToGLCode!=null && mKeyToGLCode.get(key)!=null ){
                    woObj.GL_CODE__c = mKeyToGLCode.get(key);
                }
                if(mKeyToPTCode!=null && mKeyToPTCode.get(key)!=null ){
                    woObj.Pronto_Territory_Id__c = mKeyToPTCode.get(key);
                }
                
            }
            woObj.ServiceContractId = Label.Unify_Services_Simplex_Contract_Id;
            woObj.NBN_Location_ID__c = woStaging.Location_Name__c;
            mExtIdWO.put(woObj.Client_Work_Order_Number__c,woObj);
            
        }
        
        //Copy Data from Related Staging
         Map<String,Schema.DisplayType> mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Simplex_Activity_Staging__c');
        String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        Set<String> idExt = mExtIdWO.keySet();
        String strQueryActivity = 'SELECT '+fieldsForSOQL+' from Simplex_Activity_Staging__c where IsProcessed__c=false and Work_Order_Id__c IN :idExt';
        List<Simplex_Activity_Staging__c> lstActivityStaging = Database.query(strQueryActivity);
        for(Simplex_Activity_Staging__c activityStaging:lstActivityStaging){
            Workorder woObj = mExtIdWO.get(activityStaging.Work_Order_Id__c);
            woObj.RecordTypeId = simplexRecordTypeId;
            for(PE_Subscription_Field_DTO_Mapping__mdt objMapping : lstMappingRelated){
                woObj.put(objMapping.Mapped_Object_Field_API_Name__c,activityStaging.get(objMapping.Payload_Key_Name__c));
            }
            if(activityStaging.Documentation_Complete__c =='Y' || activityStaging.Documentation_Complete__c =='y'){
                woObj.Is_Doc_Complete_Done__c =True;
            }
            /*if(activityStaging.Created_Date_Time__c !='' || activityStaging.Created_Date_Time__c!=null){
                woObj.Created_Date_Time__c =DateTime.parse(activityStaging.Created_Date_Time__c);//Datetime.format(activityStaging.Created_Date_Time__c);
            }*/
            mExtIdWO.put(woObj.Client_Work_Order_Number__c,woObj);
            
        }
        lstWorkOrdersToUpsert = mExtIdWO.values();
        
        //Save the Records
        Schema.SObjectField extField = WorkOrder.Fields.Client_Work_Order_Number__c;
        Database.upsertResult[] SaveResultList = Database.upsert(lstWorkOrdersToUpsert,extField,false); 
        List<Simplex_Work_Order_Staging__c> simplexWoStagingToUpdate = new List<Simplex_Work_Order_Staging__c>();
        for(integer i =0; i<lstWorkOrdersToUpsert.size();i++){
            String msg='';
            WorkOrder woObj = lstWorkOrdersToUpsert[i];
            If(!SaveResultList[i].isSuccess()){
                msg +='Error: "';        
                for(Database.Error err: SaveResultList[i].getErrors()){  
                    msg += err.getmessage()+'"\n\n';
                } 
                Simplex_Work_Order_Staging__c objWoStaging = new Simplex_Work_Order_Staging__c(Id =woObj.Simplex_Staging_WO_Id__c);
                objWoStaging.Error_Message__c = msg;
                ObjWoStaging.IsProcessed__c = true;
                simplexWoStagingToUpdate.add(ObjWoStaging);
                
            }
            else{
                Simplex_Work_Order_Staging__c objWoStaging = new Simplex_Work_Order_Staging__c(Id =woObj.Simplex_Staging_WO_Id__c);
                //objWoStaging.Error_Message__c = msg;
                ObjWoStaging.IsSuccess__c = true;
                ObjWoStaging.IsProcessed__c = true;
                simplexWoStagingToUpdate.add(ObjWoStaging);
                
            }
        
     	} 
        if(simplexWoStagingToUpdate!=null && simplexWoStagingToUpdate.size()>0){
            Update simplexWoStagingToUpdate;
        }
                
    }
     global void finish(Database.BatchableContext bcMain){
        BatchUpsertSimplexSOR bt = new BatchUpsertSimplexSOR();
        Database.executeBatch(bt, 200);
     }
}