/**
 * @File Name          : SydUniDTO.cls
 * @Description        : 
 * @Author             : Mahmood Zubair 
 * @Group              : 
 * @Last Modified By   : Mahmoood Zubair 
 * @Last Modified On   : 11/08/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/08/2020   Mahmood Zubair     Initial Version
**/
public class SydUniDTO  implements IDTO{
    
    
    public workOrderDetails workorderDetails {get;set;}
    public SydUniDTO(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'workOrderDetails') {
						workOrderDetails = new workOrderDetails(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'SydUniDTO Integration consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class workOrderDetails {
		public String wrId {get;set;} 
		public String blId {get;set;} 
		public String blname {get;set;} 
		public String eqid {get;set;} 
		public String trid {get;set;} 
		public String priority {get;set;} 
		public String causecode {get;set;} 
		public String quotationrequired {get;set;} 
		public String dateassgined {get;set;} 
		public String timeassgined {get;set;} 
		public String dateinitialresponse {get;set;} 
		public String timeinitialresponse {get;set;} 
		public String datecontrolresponse {get;set;} 
		public String timecontrolresponse {get;set;} 
		public String requestor {get;set;} 
		public String email {get;set;} 
		public String phone {get;set;} 
		public String flId {get;set;} 
		public String rmId {get;set;} 
		public String location {get;set;} 
		public String description {get;set;} 
        public String notes {get;set;}
		
		public WorkOrderDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'Wr_Id') {
							wrId = parser.getText();
						} else if (text == 'bl_Id') {
							blId = parser.getText();
						} else if (text == 'bl-name') {
							blName = parser.getText();
						} else if (text == 'eq_id') {
							eqId = parser.getText();
						} else if (text == 'tr_id') {
							trId = parser.getText();
						} else if (text == 'priority') {
							priority = parser.getText();
						} else if (text == 'cause_code') {
							causeCode = parser.getText();
						} else if (text == 'quotation_required') {
							quotationRequired = parser.getText();
						} else if (text == 'date_assgined') {
							Dateassgined = parser.getText();
						} else if (text == 'time_assgined') {
							timeassgined = parser.getText();
						} else if (text == 'date_initial_response') {
							dateInitialResponse = parser.getText();
						} else if (text == 'time_initial_response') {
							timeInitialResponse = parser.getText();
						} else if (text == 'date_control_response') {
							dateControlResponse = parser.getText();
						} else if (text == 'time_control_response') {
							timeControlResponse = parser.getText();
						} else if (text == 'requestor') {
							requestor = parser.getText();
						} else if (text == 'email') {
							email = parser.getText();
						} else if (text == 'phone') {
							phone = parser.getText();
						} else if (text == 'fl_id') {
							flId = parser.getText();
						} else if (text == 'rm_id') {
							rmId = parser.getText();
						} else if (text == 'location') {
							location = parser.getText();
						} else if (text == 'description') {
							description = parser.getText();
						}  else {
							System.debug(LoggingLevel.WARN, 'WorkOrderDetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
                notes = '';
                notes += (blname != null) ? 'Building Name = ' + blname + '\n' : '';
                notes += (flId != null) ? 'Floor ID = ' + flId + '\n' : '';
                notes += (rmId != null) ? 'Room  ID = ' + rmId + '\n' : '';
                notes += (location != null) ? 'location = ' + location + '\n' : '';
                notes += (eqId != null) ? 'Equipment ID = ' + eqId + '\n' : '';
                notes += (causeCode != null) ? 'Cause Code = ' + causeCode + '\n' : '';
                notes += (description != null) ? 'Description = ' + description + '\n' : '';
                notes += (requestor != null) ? 'Requestor Name  = ' + requestor + '\n' : '';
                notes += (email != null) ? 'Requestor Email  = ' + email + '\n' : '';
                notes += (phone != null) ? 'Requestor Phone  = ' + phone + '\n' : '';
			}
		}
	}
	

    public  SydUniDTO() {
	}
	
	public  SydUniDTO parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new SydUniDTO(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	

    public String getSpecificDTOClassName(){

        return 'SydUniDTO';
    }


}