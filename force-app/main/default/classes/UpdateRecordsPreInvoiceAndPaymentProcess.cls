/**
 * @author          Karan Shekhar
 * @date            09/Dec/2019 
 * @description     Class to update Paymnent & Invoice references on Line Items. This standalone method was created because flow couldn't support complex data structures.
                    
 *
 * Change Date    Modified by         Description  
 **/

global class UpdateRecordsPreInvoiceAndPaymentProcess {
    
    @InvocableMethod
    public static void updateLineItemsPaymentsAndInvoices(List<Workorder> parentWorkOrderList) {
        
        
        List<Payment__c> paymentsToUpdateList = new List<Payment__c>();
        List<Line_Item__c> lineItemListToUpdate = new List<Line_Item__c>();
        List<Invoice__c> invoiceListToUpdate = new List<Invoice__c>();
        Set<String> lineItemsIds = new Set<String>();        
        Map<Id,Id> lineItemIdToPaymentIdMap = new Map<Id,Id>();
        Map<Id,Id> lineItemIdToInvoiceIdMap = new Map<Id,Id>();
        Id parentWorkOrderID;
        
        try{
            
            parentWorkOrderID = parentWorkOrderList[0].id;
            if(parentWorkOrderList[0].ParentWorkOrderId != null){
                
                parentWorkOrderID = parentWorkOrderList[0].ParentWorkOrderId;
            }
            
                        
            for(Payment__c paymentObj: [SELECT ID,Status__c,Sync_Status__c,Line_Item_Ids__c,Service_Resource__r.Account__r.Name,Service_Resource__r.Contact__r.Account.Name FROM Payment__c WHERE Work_Order__r.ParentWorkOrderId = :parentWorkOrderID AND Status__c = 'Pending' AND Sync_Status__c = null] ){
                
                for(String str: paymentObj.Line_Item_Ids__c.removeStart(';').split(';')){
                    System.debug(str);
                    if(String.isNotBlank(str)){
                        
                        lineItemIdToPaymentIdMap.put((Id)str,paymentObj.id);
                    }                    
                    
                }
                if(!paymentObj.Service_Resource__r.Contact__r.Account.Name.equalsIgnoreCase('Employee') && !paymentObj.Service_Resource__r.Contact__r.Account.Name.equalsIgnoreCase('BSA Limited')){
                    
                    paymentObj.Sync_Status__c = 'Ready For Sync';
                    paymentsToUpdateList.add(paymentObj);
                }
                
                
            }
            
            for(Invoice__c invoice : [SELECT ID,Status__c,Sync_Status__c,Line_Item_Ids__c FROM Invoice__c WHERE Status__c = 'Pending' AND Work_Order__c =:parentWorkOrderID AND Sync_Status__c = null]){
                
                for(String str: invoice.Line_Item_Ids__c.removeStart(';').split(';')){
                    System.debug(str);
                    if(String.isNotBlank(str)){
                        lineItemIdToInvoiceIdMap.put((Id)str,invoice.id);
                    }                                        
                }
                invoice.Sync_Status__c='Ready For Sync';
                invoiceListToUpdate.add(invoice);                                
            }
            
            System.debug('lineItemIdToPaymentIdMap'+lineItemIdToPaymentIdMap);        
            
            for(Line_Item__c lineItemObj : [Select ID, Invoice__c,Payment__c,Status__c FROM Line_Item__c WHERE ID IN :lineItemIdToPaymentIdMap.keySet()]){
                
                lineItemObj.Invoice__c = lineItemIdToInvoiceIdMap.get(lineItemObj.id);
                lineItemObj.Payment__c = lineItemIdToPaymentIdMap.get(lineItemObj.id);
                lineItemObj.Status__c = 'Committed';
                lineItemListToUpdate.add(lineItemObj);
            }
                        
            if(!lineItemListToUpdate.isEmpty()){
                UPDATE lineItemListToUpdate;
            }     
            if(!invoiceListToUpdate.isEmpty()){
                UPDATE invoiceListToUpdate;
            }  
            if(!paymentsToUpdateList.isEmpty()){
                UPDATE paymentsToUpdateList;  
            }  
            
        } catch(Exception excp) {
            
            System.debug('Exception occured'+excp.getMessage()+excp.getStackTraceString());
        }
        
        
    }
}