/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-08-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-08-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class GetPicklistLabelGenericTest {
    
    @isTest
    static void TestGetPicklistNameValue () {  
        List<GetPicklistLabelGeneric.getPicklistValueRequest> req = new List<GetPicklistLabelGeneric.getPicklistValueRequest>();
        GetPicklistLabelGeneric.getPicklistValueRequest gTest = new GetPicklistLabelGeneric.getPicklistValueRequest();
        gTest.sPicklistAPIName = 'Advertisement';
        gTest.objectName = 'Account';
        gTest.fieldName = 'AccountSource';
        req.add(gTest);
        
        Test.startTest();
        List<String> stList = GetPicklistLabelGeneric.getPicklistNameValue (req);
        Test.stopTest();
    }

}