global class BatchProcessSimplexInvoiceAndPayment Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
	global String strQuery;
    public List<String> batchException = new List<String>();
   	Set<Id> successWOSetToBeUpdated = new Set<Id>();    
    
    //Initialize the query string
    global BatchProcessSimplexInvoiceAndPayment(Set<Id> successWOIdSet){
        System.debug('Sayali Log: successWOIdSet- '+successWOIdSet);
        successWOSetToBeUpdated = successWOIdSet;
        String queryFields = 'Id,'+
            'Service_Resource__c,'+
            'Total_Summary_Cost_BSA__c,'+
            'Total_Summary_Cost_Tech__c,'+
            'servicecontract.AccountId,'+
            'Client_Work_Order_Number__c';
        strQuery = 'SELECT '+queryFields+' from WorkOrder where Id IN: successWOSetToBeUpdated';        
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<WorkOrder> workOrderList){
        //Store all success and failure on staging
        Map<String, Simplex_Activity_Staging__c> activityStagingMap = new Map<String, Simplex_Activity_Staging__c>();
        
        Set<Id> woIds = new Set<Id>();
        Map<String, WorkOrder> woMap = new Map<String, WorkOrder>();
        for(WorkOrder wo: workOrderList){
            woMap.put(wo.Client_Work_Order_Number__c, wo);
            woIds.add(wo.Id);
        }
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        List<Payment__c> paymentList = new List<Payment__c>();
        Map<String, List<Line_Item__c>> newWOLIMap = new Map<String, List<Line_Item__c>>();        
        for(Line_Item__c li: [Select Id, BSA_Cost__c, Technician_Cost__c, Work_Order__r.Client_Work_Order_Number__c From Line_Item__c Where Work_Order__c IN:woIds]){
			if(newWOLIMap.containsKey(li.Work_Order__r.Client_Work_Order_Number__c)){
                newWOLIMap.get(li.Work_Order__r.Client_Work_Order_Number__c).add(li);
            }else{
                newWOLIMap.put(li.Work_Order__r.Client_Work_Order_Number__c, new List<Line_Item__c> {li});
            }
        }
                
        List<Line_Item__c> liListToUpdate = new List<Line_Item__c>();
        if(newWOLIMap.size() > 0){
            for(String newLIWOKey: newWOLIMap.keySet()){
                System.debug('DildarLog: newLIWOKey - ' + newLIWOKey);
                System.debug('DildarLog: newLIWOKey Size - ' + (newWOLIMap.get(newLIWOKey)).size());    
                Double totalBSACost = 0;
                Double totalTechCost = 0;
                Invoice__c inv = new Invoice__c();
                Payment__c pay = new Payment__c();
                for(Line_Item__c li: newWOLIMap.get(newLIWOKey)){
                    /*if(li.BSA_Cost__c != null)
                        totalBSACost = totalBSACost + li.BSA_Cost__c;
                    
                    if(li.Technician_Cost__c != null)
                        totalTechCost = totalTechCost + li.Technician_Cost__c;*/
                    
                    li.Payment__r = new Payment__c(Payment_External_Id__c = newLIWOKey);
                    li.Invoice__r = new Invoice__c(Invoice_External_Id__c = newLIWOKey);
                    liListToUpdate.add(li);                    
                }
                
                inv.Work_Order__r = new WorkOrder(Client_Work_Order_Number__c=newLIWOKey);
                inv.BSA_Total__c = woMap.get(newLIWOKey).Total_Summary_Cost_BSA__c;
                inv.Invoice_External_Id__c = newLIWOKey;
                inv.Account__c = woMap.get(newLIWOKey).servicecontract.AccountId;
                invoiceList.add(inv);
                
                pay.Work_Order__r = new WorkOrder(Client_Work_Order_Number__c=newLIWOKey);
                pay.Amount__c = woMap.get(newLIWOKey).Total_Summary_Cost_Tech__c;
                pay.Payment_External_Id__c = newLIWOKey;
                pay.Service_Resource__c = (woMap.get(newLIWOKey)).Service_Resource__c;
                pay.Status__c = 'Pending';
                paymentList.add(pay);
            }
        }
        System.debug('DildarLog: invoiceList - ' + invoiceList);
        System.debug('DildarLog: paymentList - ' + paymentList);
        
        //Upsert lineitem/sor data for all simplex workorders
        if(invoiceList.size() > 0){
        	Database.UpsertResult[] upsertINVResult = Database.upsert(invoiceList, Invoice__c.Fields.Invoice_External_Id__c, false);
        	System.debug('DildarLog: upsertINVResult - ' + upsertINVResult);
            
            //TODO
            /*Map<String, String> errorInvStagingMap = new Map<String, String>();
            
            for(integer i =0; i<invoiceList.size();i++){
                String msg='';
                Invoice__c invObj = invoiceList[i];
                If(!upsertINVResult[i].isSuccess()){                
                    msg +='Error: "';        
                    for(Database.Error err: upsertINVResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    errorInvStagingMap.put(invObj.Invoice_External_Id__c, msg);
                }             
            }
            
            List<Simplex_Activity_Staging__c> objStagingList = new List<Simplex_Activity_Staging__c>();
            
            for(Simplex_Activity_Staging__c sas: [Select Id, Work_Order_Id__c, Invoice_Error_Message__c From Simplex_Activity_Staging__c Where Work_Order_Id__c IN: errorInvStagingMap.keySet()]){
                Simplex_Activity_Staging__c newSAS = new Simplex_Activity_Staging__c();
                newSAS.Id = sas.Id;
                newSAS.Invoice_Error_Message__c = errorInvStagingMap.get(sas.Work_Order_Id__c);
                objStagingList.add(newSAS);
            }
            if(objStagingList.size() > 0)
            	Update objStagingList;*/
        }
        
        //Upsert payment data for all simplex workorders
        if(paymentList.size() > 0){
        	Database.UpsertResult[] upsertPAYResult = Database.upsert(paymentList, Payment__c.Fields.Payment_External_Id__c, false);
        	System.debug('DildarLog: upsertPAYResult - ' + upsertPAYResult);
        }
        
        if(liListToUpdate.size() > 0){
            //Upsert lineitem/sor data for all simplex workorders with payment and invoice lookup
            Database.SaveResult[] updateLIResultWithINVPAY = Database.update(liListToUpdate, false);
            System.debug('DildarLog: updateLIResultWithINVPAY - ' + updateLIResultWithINVPAY);
        }
        
        //TODO
        /*if(activityStagingMap.size() > 0 ){
            Update activityStagingMap.values();
        }*/
    }
	global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exception list
        if(batchException.size() > 0){
            System.debug('Dildar Debug: Batch - BatchProcessSimplexInvoiceAndPayment - Finish - Exceptions - ' + batchException);
        } 
	}
}