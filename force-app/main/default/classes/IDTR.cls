public interface IDTR {
	    
    IDTR transform(Object obj,Id refId,String processId);
    IDTR transformUpload(Object obj,string refId,String processId,ContentVersion cv); 
}