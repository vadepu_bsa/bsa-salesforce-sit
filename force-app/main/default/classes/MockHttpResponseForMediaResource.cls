@isTest
global class MockHttpResponseForMediaResource implements System.HttpCalloutMock {
    global MockHttpResponseForMediaResource() {

    }
    global System.HttpResponse respond(System.HttpRequest req) {
       HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('[{'+
  '"id": "21351gty35-c243-11e9-88bc-098787f764621",'+
  '"relatedEntity":  { '+
    '"id": "WOR200001090638",'+
    '"@type": "WorkOrderRef"'+
    '},'+
    '"metadata": {'+
    '"objectType":"MetaDataWorkOrder",'+
    '"id": "21351gty35-c243-11e9-88bc-098787f764621",'+
    '"source":"TEST",'+
    '"notes": "manual for equipment",'+
    '"fileName": "manual.pdf",'+
    '"contentType": "application/pdf" },'+
  '"content": {'+
    '"presignedUrl": "https://testurl.com" '+
    '}'+
  '}]');
        res.setStatusCode(200);
        return res;
    }
}