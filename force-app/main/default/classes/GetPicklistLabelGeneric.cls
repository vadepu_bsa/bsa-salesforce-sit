public class GetPicklistLabelGeneric {
  	@InvocableMethod(label='Get Picklist Label Generic')
    public static List<String> getPicklistNameValue (List<getPicklistValueRequest> requestList )
    {
        List<String> pickListName = new List<String>();
        
        //Set Variables using Invocable Variables from Class getPicklistValueRequest
        getPicklistValueRequest objInput = requestList.get(0);
        String objectName = objInput.objectName;
        String fieldName = objInput.fieldName;
        
        //Retrieve Picklist values using Object Name & Field Name
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for (Schema.PicklistEntry f : ple)
        {
        	If(f.getValue() == objInput.sPicklistAPIName)
         	{
         		pickListName.add(f.getLabel());
        	}
        }
        return pickListName;
	}
    
	public class getPicklistValueRequest{
        @InvocableVariable
        public String sPicklistAPIName;
        
        @InvocableVariable
        public String objectName;
        
        @InvocableVariable
        public String fieldName;
	}
}