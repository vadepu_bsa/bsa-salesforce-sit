/**
 * @File Name          : IDTO.cls
 * @Description        : 
 * @Author             : Karan Shekhar
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 16/05/2020, 12:46:47 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/05/2020   Karan Shekhar     Initial Version
**/
public interface  IDTO {
    
    IDTO parse(String json);
    String getSpecificDTOClassName();
}