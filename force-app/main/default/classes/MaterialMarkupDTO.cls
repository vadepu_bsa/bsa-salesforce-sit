/**
 * @File Name          : MaterialMarkupDTO.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 03/03/2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    31/07/2020         Tom Henson             Initial Version
 * 1.1    03/03/2021         Ben Lee                Added Operations field
**/

public class MaterialMarkupDTO implements IDTO{
    
    public List<materialMarkupDetails> materialMarkupDetails {get;set;} 
    public MaterialMarkupDTO(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'materialDetails') {
                        materialMarkupDetails = arrayOfMaterialMarkupDetails(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'BaseDTO consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    private static List<MaterialMarkupDetails> arrayOfMaterialMarkupDetails(System.JSONParser p) {
        List<MaterialMarkupDetails> res = new List<MaterialMarkupDetails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new MaterialMarkupDetails(p));
        }
        return res;
    }
    
    public class materialMarkupDetails {
        public String contactcode {get;set;} 
        public String accountcode {get;set;} // in json: type
        public String markupkey {get;set;} 
        public String markupname {get;set;} 
        public String markupstartvalue {get;set;} 
        public String markupendvalue {get;set;} 
        public String markuppercentage {get;set;} 
        public String markupid {get;set;} 
        public String operations {get;set;}     //Ben Lee - 03/03/2021

        public MaterialMarkupDetails(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'contact-code') {
                            contactcode = parser.getText();
                        } else if (text == 'account-code') {
                            accountcode = parser.getText();
                        } else if (text == 'markup-key') {
                            markupkey = parser.getText();
                        } else if (text == 'markup-name') {
                            markupname = parser.getText();
                        } else if (text == 'markup-start-value') {
                            markupstartvalue = parser.getText();
                        } else if (text == 'markup-end-value') {
                            markupendvalue = parser.getText();
                        } else if (text == 'markup-percentage') {
                            markuppercentage = parser.getText();
                        } else if (text == 'markup-id') {
                            markupid = parser.getText();
                        } else if (text == 'Operations') {        //Ben Lee - 03/03/2021
                            operations = parser.getText();        //Ben Lee - 03/03/2021
                        } else {
                            System.debug(LoggingLevel.WARN, 'Markupcode consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }   
    }
    
    public  MaterialMarkupDTO() {
    }
    
    public  MaterialMarkupDTO parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new MaterialMarkupDTO(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
  
    public String getSpecificDTOClassName(){

        return 'MaterialMarkupDTO';
    }
}