/**
* @File Name          : WorkOrderServiceBaseImplementation.cls
* @Description        : 
* @Author             : Karan Shekhar
* @Group              : 
* @Last Modified By   : Karan Shekhar
* @Last Modified On   : 11/05/2020, 4:54:23 pm
* @Modification Log   : 
* Ver       Date            Author                  Modification
* 1.0    09/05/2020   Karan Shekhar     Initial Version
**/
public abstract class WorkOrderServiceBaseImplementation  implements IWorkOrderService{
    public static boolean isWOUpdated= false;
    private List<WorkOrder> mWOListToProcess;
    private List<Application_Log__c>  logList = new List<Application_Log__c>();
    private Map<id,SObject> mOldWoList;
    public Map<String,List<WorkOrder>> mServiceContractNumberToWOListMap;
    private Map<String,List<Work_Order_Automation_Rule__c>> mServiceContractNumberToWOARListMap;  
    public Map<String,ServiceContract> mServiceContractNumberToSRObjMap; 
    public Map<String,List<PE_Subscription_Object_Action_Mapping__mdt>> mServiceContractNumberToExtRednMethod; 
    private Map<String,Schema.DisplayType> mApiNameToSchemaType;
    private List<WorkOrder> childWorkOrderToInsert = new List<WorkOrder>();
    private Boolean mISDMLRequired; 
    private Map<String,Map<String,String>>  mWOARNameToFieldAPIMappings;
    private Map<Id, List<WorkOrder>> mParentWOIdToChildWOLst;
    //private Map<String,String> mworkOrderFieldAPINameSTToWOARFieldAPINameMapST;
    //private Map<String,String> mworkOrderFieldAPINamePBToWOARFieldAPINameMapPB;
    private static final String WT_AUTOMATION = 'Work_Type_Automation';
    private static final String ST_AUTOMATION = 'Service_Territory_Automation';
    private static final String PB_AUTOMATION = 'Price_Book_Automation';
    private static final String WOLI_AUTOMATION = 'WOLI_Automation';
    private static final String SM_AUTOMATION = 'Status_Mapping_Automation';
    private static final String SV_AUTOMATION = 'State_Validation_Automation';
    private static final String GL_AUTOMATION = 'GL_Code_Automation';
    private static final String PT_AUTOMATION = 'Pronto_Territory_Automation';
    
    private List<WorkOrderLineItem> woliToCreateList = new List<WorkOrderLineItem>();
    private static Blob cryptoKey = Blob.valueOf(Label.CryptoKey);

    public enum AutomationRuleSource {WORK_ORDER_AUTO_RULE}


    public WorkOrderServiceBaseImplementation() {
        
    }
    
    public virtual void setupWorkOrder(List<WorkOrder> woListToProcess){
        mWOListToProcess = woListToProcess;   
        getWOForServiceContracts();
        getServiceContractRecords();
        getWOARRecords();
        mapWorkOrders();


    }
     public void setupOldWOMap(Map<Id,WorkOrder> moldItems){
         mOldWoList = moldItems;
     }
    public virtual void setupWorkOrderAfterUpdate(List<WorkOrder> woListToProcess,Map<Id,SObject> moldItems){
        mWOListToProcess = woListToProcess;
        mOldWoList = moldItems;
        getWOForServiceContracts();
        getServiceContractRecords();
        getWOARRecords();
        //mapWorkOrders();


    }
    private void getParentToChildWO(){
        mParentWOIdToChildWOLst = new Map<Id,List<WorkOrder>>();
        for(WorkOrder objChild:childWorkOrderToInsert){
            if(mParentWOIdToChildWOLst.get(objChild.ParentWorkOrderId)!=null){
                List<WorkOrder> lstWO =mParentWOIdToChildWOLst.get(objChild.ParentWorkOrderId);
                lstWO.add(objChild);
                mParentWOIdToChildWOLst.put(objChild.ParentWorkOrderId,lstWO);
            }
            else{
                List<WorkOrder> lstWO =new List<WorkOrder>();
                lstWO.add(objChild);
                mParentWOIdToChildWOLst.put(objChild.ParentWorkOrderId,lstWO);
                
                
            }
        }
        System.debug('SAYALI WOLI mParentWOIdToChildWOLst:'+mParentWOIdToChildWOLst);
    }
    private void mapWorkOrders(){

        for(WorkOrder wobj : mWOListToProcess){
            if(wobj.RecordTypeId != Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.WO_CHILD).getRecordTypeId()){
                setServiceContract(wobj);
                System.debug('SAYALI: mServiceContractNumberToWOListMap--> '+mServiceContractNumberToWOListMap);
                System.debug('SAYALI: mServiceContractNumberToSRObjMap--> '+mServiceContractNumberToSRObjMap);
                setParentRecordType(wobj);
                
                System.debug('Sercice contract on WO'+wobj.ServiceContractId);            
                setValues(wobj);
                wobj.StartDate = System.now();
                createChildWo(wobj);
                //WorkOrder woNew = [select WorkType.Name,ServiceTerritory.Name,PriceBook2Id from WorkOrder where id=:wobj.id];
                System.debug('SAYALI: Work Type on WO-->'+wobj.WorkTypeId);
                System.debug('SAYALI: Service Territory on WO-->'+wobj.ServiceTerritoryId);
                System.debug('SAYALI: PriceBook on WO-->'+wobj.PriceBook2Id);
                System.debug('SAYALI: WOLis on WO-->'+woliToCreateList);
    
            }
            
        }
        if(mWOListToProcess!=null) {
            System.debug('SAYALI mWOListToProcess-->'+mWOListToProcess);
            isWOUpdated = true;
            update mWOListToProcess;
            
        }
        if(!childWorkOrderToInsert.isEmpty()) {
            insert childWorkOrderToInsert;
            System.debug('SAYALI: PriceBook on WO Child-->'+childWorkOrderToInsert[0].PriceBook2Id);
            getParentToChildWO();
            for(WorkOrder wobj : mWOListToProcess){
                if(wobj.RecordTypeId != Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.WO_CHILD).getRecordTypeId()){
                  
                    checkANDMapReferencesOnWO(wobj,WOLI_AUTOMATION);
                }
            }
            System.debug('SAYALI WOLI mParentWOIdToChildWOLst Inside MapWorkOrder:'+mParentWOIdToChildWOLst);
            System.debug('SAYALI WOLI woliToCreateList:'+woliToCreateList);
            if(!woliToCreateList.isEmpty()) {
                insert woliToCreateList;
            }
        }
        
        

    }


    private void setServiceContract(WorkOrder woRecord) {
        System.debug('SAYALI : setServiceContract from common');
        woRecord.ServiceContractId = mServiceContractNumberToSRObjMap.get(woRecord.Service_Contract_Number__c).Id;

    }
    private void setParentRecordType (WorkOrder woRecord) {
        Id parentRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.WO_PARENT).getRecordTypeId();
        woRecord.RecordTypeId = parentRecordTypeId;
    }

    private void setValues(WorkOrder woRecord){

        checkANDMapReferencesOnWO(woRecord,WT_AUTOMATION);
        checkANDMapReferencesOnWO(woRecord,ST_AUTOMATION);
        checkANDMapReferencesOnWO(woRecord,PB_AUTOMATION);
        //checkANDMapReferencesOnWO(woRecord,WOLI_AUTOMATION);
        checkANDMapReferencesOnWO(woRecord,SM_AUTOMATION);
        checkANDMapReferencesOnWO(woRecord,GL_AUTOMATION);
        checkANDMapReferencesOnWO(woRecord,PT_AUTOMATION);

    }

    public void checkANDMapReferencesOnWO(WorkOrder woRecord, String recordTypeDevName) {

        Boolean isExactMatch = true;
        System.debug('woRecord.ServiceContractId-->'+woRecord.ServiceContractId);
         System.debug('recordTypeDevName-->'+recordTypeDevName);
        Map<String,String> mWOToWOARFieldsMapping = mWOARNameToFieldAPIMappings.get(recordTypeDevName);
        Work_Order_Automation_Rule__c woarObjNew = new Work_Order_Automation_Rule__c();
        List<Work_Order_Automation_Rule__c> lstWOLIWOARs = new List<Work_Order_Automation_Rule__c>();
        if(mServiceContractNumberToWOARListMap.get(woRecord.ServiceContractId+':'+recordTypeDevName)!=null)
        {
            Integer i=0;
            for(Work_Order_Automation_Rule__c woarObj : mServiceContractNumberToWOARListMap.get(woRecord.ServiceContractId+':'+recordTypeDevName)){
                System.debug('SAYALI :WOAR Object'+i+'-->'+woarObj);
                i++;
                isExactMatch = true;
                for(String str: mWOToWOARFieldsMapping.keySet()){
                    System.debug('SAYALI:recordTypeDevName'+recordTypeDevName);
                    System.debug('SAYALI:WO value'+woRecord.get(str) );
                    System.debug('SAYALI:WOAR Value' +mWOToWOARFieldsMapping.get(str));
                    
                        if(woRecord.get(str) != woarObj.get(mWOToWOARFieldsMapping.get(str))){
                            isExactMatch = false;
                            break;
                        }
                    
                    
                }
                //System.debug('IsExact match '+isExactMatch);
                if(isExactMatch){
                    if(recordTypeDevName == WOLI_AUTOMATION){
                        lstWOLIWOARs.add(woarObj);
                    }
                    else{
                    woarObjNew =woarObj;
                    break;
                    }
                }
                
            }
            if(recordTypeDevName == WOLI_AUTOMATION && lstWOLIWOARs!=null && lstWOLIWOARs.size()>0){
                System.debug('SAYALI WOLI lstWOLIWOARs:'+lstWOLIWOARs);
                System.debug('SAYALI WOLI mParentWOIdToChildWOLst:'+mParentWOIdToChildWOLst);
                Id woliRecTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get(BSA_ConstantsUtility.WOLI_CUI).getRecordTypeId();
                for(WorkOrder objChild : mParentWOIdToChildWOLst.get(woRecord.Id)) {
                    for(Work_Order_Automation_Rule__c objWOAR:lstWOLIWOARs){
                        WorkOrderLineItem woli = new WorkOrderLineItem();
                        woli.Type__c = objWOAR.Type__c;
                        woli.Description = objWOAR.Action__c;
                        woli.WOrkOrderId = objChild.Id;
                        woli.WorkTypeId = objWOAR.Work_Type__c;
                        woli.Required_Pre_Finish__c = objWOAR.Required_Pre_Finish__c;
                        woli.Required_Pre_Notification__c = objWOAR.Required_Pre_Notification__c;
                        woli.RecordTypeId = woliRecTypeId;
                        woliToCreateList.add(woli);
                    }
                }
                System.debug('SAYALI WOLI woliToCreateList Inside CheckMapReference:'+woliToCreateList);
            }
            if(isExactMatch) {
                    if(recordTypeDevName == WT_AUTOMATION){
    
                        woRecord.WorkTypeId = woarObjNew.Work_Type__c;
                    }
                    else if(recordTypeDevName == ST_AUTOMATION){
    
                        woRecord.ServiceTerritoryId = woarObjNew.Service_Territory__c;
                    }
                    else if(recordTypeDevName == PB_AUTOMATION){
    
                        woRecord.PriceBook2Id = woarObjNew.Price_Book__c;
                    }
                    else if(recordTypeDevName == SM_AUTOMATION){
    
                        woRecord.Status = woarObjNew.Status__c;
                    }
                	else if(recordTypeDevName == GL_AUTOMATION){
    
                        woRecord.GL_CODE__c = woarObjNew.GL_CODE__c;
                    }
                	else if(recordTypeDevName == PT_AUTOMATION){
    
                        woRecord.Pronto_Territory_Id__c = woarObjNew.Pronto_Territory_Id__c;
                    }
                	else if(recordTypeDevName == SV_AUTOMATION){
                        WorkOrder oldWoRec = (WorkOrder)mOldWoList.get(woRecord.Id);
                        System.debug('SAYALI DEBUG oldWoRec=='+oldWoRec);
                        System.debug('SAYALI DEBUG woarObjNew=='+woarObjNew);
                        System.debug('SAYALI DEBUG oldWoRec=='+oldWoRec.SA_Status__c);
                        if(oldWoRec.SA_Status__c!=null && !woarObjNew.Valid_for_States__c.containsIgnoreCase(oldWoRec.SA_Status__c)){
                            
                            String msg = 'Action "'+woRecord.Client_Inbound_Action__c+'" is invalid for "'+oldWoRec.SA_Status__c+'" Status';
                            Application_Log__c log = new Application_Log__c();
                            log.Log_Level__c = 'Error';
                            log.Message__c = msg;
                            log.Work_Order__c = woRecord.Id;
                            log.Correlation_ID__c = woRecord.CorrelationID__c;
                            woRecord.Client_Inbound_Action__c = oldWoRec.Client_Inbound_Action__c;
                            woRecord.SA_Status__c = oldWoRec.SA_Status__c;
                            //logList.add(log);
                            insert log;
                            ApplicationLogUtility.sendSuccessErrorToMulesoft('Correlation ID:' + woRecord.CorrelationID__c + '\n' + msg, woRecord.Client_Inbound_Action__c, false);
                        }
                    }
                    
                    
                }
                else {
                    if(recordTypeDevName == ST_AUTOMATION){
    
                        //woRecord.ServiceTerritoryId = Label.UnAllocated_Service_Territory_Id;
                    }
                    else if(recordTypeDevName == PB_AUTOMATION){
    
                        woRecord.PriceBook2Id = Label.UnAllocated_PriceBook_Id;
                    }
                }
        }
        

    }
    
    private void getWOForServiceContracts(){
        mServiceContractNumberToWOListMap = new Map<String,List<WorkOrder>>();
        for(WorkOrder wobj:  mWOListToProcess){
            
            if(mServiceContractNumberToWOListMap.get(wobj.Service_Contract_Number__c)!=null){
                 List<WorkOrder> tempWOList =(List<WorkOrder>) (mServiceContractNumberToWOListMap.get(wobj.Service_Contract_Number__c)).add(wobj);
                mServiceContractNumberToWOListMap.put(wobj.Service_Contract_Number__c,tempWOList);
                
                mServiceContractNumberToWOListMap.put(wobj.Service_Contract_Number__c,new List<WorkOrder>{wobj});
                
            } else{ 
               mServiceContractNumberToWOListMap.put(wobj.Service_Contract_Number__c,new List<WorkOrder>{wobj});
            }                        
            
        }
        System.debug('Service contract to WO List Map'+mServiceContractNumberToWOListMap);
    }
 
    
    /*private String encryptId(String woid){
        
        Blob data = Blob.valueOf(woid);
        Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey , data );
        String encryptedId = EncodingUtil.base64Encode(encryptedData);
        encryptedId = EncodingUtil.urlEncode(encryptedId,'UTF-8');
        return encryptedId;
    }*/
    private void createChildWo(WorkOrder woObj)
    {
        
        Decimal noOfChildWO= mServiceContractNumberToSRObjMap.get(woObj.Service_Contract_Number__c).Number_of_Child_WO__c;
        Id woChildRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.WO_CHILD).getRecordTypeId();
        if(noOfChildWO!=null && noOfChildWO!=0)
        {
            for(integer i = 0; i < noOfChildWO; i++) {
                WorkOrder childWo  = woObj.clone(false, false, false, false);
                
                
                childWo.ParentWorkOrderId = woObj.Id;
                childWo.NBN_Reference_ID__c = '';
                childWo.NBN_Activity_Status_Info__c = '';
                childWo.Client_Work_Order_Number__c = '';
                childWo.NBN_Parent_Work_Order_Id__c = '';
                childWo.NBN_NA_JSON_Payload__c = '';
                childWo.Status = 'New';
                
                    childWo.RecordTypeId = woChildRecordTypeId;
               
                childWorkOrderToInsert.add(childWo);
            }
        }
        else{
            WorkOrder childWo  = woObj.clone(false, false, false, false);
                
                
                childWo.ParentWorkOrderId = woObj.Id;
                childWo.NBN_Reference_ID__c = '';
                childWo.NBN_Activity_Status_Info__c = '';
                childWo.Client_Work_Order_Number__c = '';
                childWo.NBN_Parent_Work_Order_Id__c = '';
                childWo.NBN_NA_JSON_Payload__c = '';
                childWo.Status = 'New';
                
                    childWo.RecordTypeId = woChildRecordTypeId;
               
                childWorkOrderToInsert.add(childWo);
            System.debug('SAYALI: PriceBook on WO Child inside createchild-->'+childWorkOrderToInsert[0].PriceBook2Id);
        }
        
    }
    private void getServiceContractRecords(){
        
        mServiceContractNumberToSRObjMap = new Map<String,ServiceContract>();
        for(ServiceContract serObj : [SELECT Id,ContractNumber,Number_Of_Child_WO__c FROM ServiceContract WHERE ContractNumber IN :mServiceContractNumberToWOListMap.keySet()]){
            mServiceContractNumberToSRObjMap.put(serObj.ContractNumber,serObj);
            
        }
        System.debug('Service Contract Name to ID Map'+mServiceContractNumberToSRObjMap);
    }

       
    private void getWOARRecords(){
        
        mServiceContractNumberToWOARListMap = new Map<String,List<Work_Order_Automation_Rule__c>>();
        mApiNameToSchemaType = UtilityClass.fetchFieldTypesforObject('Work_Order_Automation_Rule__c');
        String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        Set<String> serviceContractSet = mServiceContractNumberToWOListMap.keySet();
        System.debug('Contract number set'+serviceContractSet);
        String woarQuery = 'SELECT '+ fieldsForSOQL+ ', RecordType.Developername FROM Work_Order_Automation_Rule__c WHERE Active__c = true AND Service_Contract__r.ContractNumber IN :serviceContractSet order by Sequence__c';
        System.debug('WOQR Query'+woarQuery);
        for(Work_Order_Automation_Rule__c woarObj : Database.Query(woarQuery)){

            
            if(!mServiceContractNumberToWOARListMap.containsKey(woarObj.Service_Contract__c+':'+woarObj.RecordType.Developername)){
                mServiceContractNumberToWOARListMap.put(woarObj.Service_Contract__c+':'+woarObj.RecordType.Developername,new List<Work_Order_Automation_Rule__c>{woarObj});
                
            } else{ 
                List<Work_Order_Automation_Rule__c> tempWOARList =(List<Work_Order_Automation_Rule__c>) mServiceContractNumberToWOARListMap.get(woarObj.Service_Contract__c+':'+woarObj.RecordType.Developername);
                tempWOARList.add(woarObj);
               //System.debug('Temp List'+tempWOARList);
                mServiceContractNumberToWOARListMap.put(woarObj.Service_Contract__c+':'+woarObj.RecordType.Developername,tempWOARList);
            }  
        }
        System.debug('WOAR SC to Obj Map'+mServiceContractNumberToWOARListMap);
    }
     public static void syncSAWithParentWorkOrder(Map<Id,SObject> newWorkOrderMap,Map<Id,SObject> oldWorkOrderMap){
        
        Set<Id> SAWOIDSet= new Set<Id>();
        Map<id,WorkOrder> parentWOIdToCurrentStatusMap = new Map<Id,WorkOrder>();
        List<ServiceAppointment> serviceAppointmentListToUpdate  =  new List<ServiceAppointment>();
        List<WorkOrder> workOrderListToUpdate  = new List<WorkOrder>();
        
        try{
            
            for(Id sobj : newWorkOrderMap.keyset()){
                
                WorkOrder woObj = (WorkOrder)newWorkOrderMap.get(sobj);                    
                if(woObj.ParentWorkOrderId == null && String.isNotBlank(woObj.Status) && woObj.SA_Status__c != ((WorkOrder)oldWorkOrderMap.get(woObj.id)).SA_Status__c){
                    // parentWOIDSet.add(woObj.Id);
                    parentWOIdToCurrentStatusMap.put(woObj.Id,woObj);
                    
                }
                
            }
            System.debug('parentWOIdToCurrentStatusMap =='+parentWOIdToCurrentStatusMap);
            if(parentWOIdToCurrentStatusMap.size() >0){
                
                for(WorkOrder woObj: [SELECT Id,Status,ParentWorkOrderId,(SELECT Id,Status,Work_order__r.Id,Work_order__r.ParentWorkOrder.Status,Work_order__r.ParentWorkOrderId FROM ServiceAppointments) FROM Workorder WHERE ParentWorkOrderId IN : parentWOIdToCurrentStatusMap.keyset() ]){
                    System.debug('woObj.ServiceAppointments =='+woObj.ServiceAppointments);
                    if(woObj.ServiceAppointments != null && woObj.ServiceAppointments.size()>0){
                        for(ServiceAppointment servAppObj : woObj.ServiceAppointments){
                          //if(!BSA_ConstantsUtility.NOT_ALLOWED_STATUS_SET.contains(servAppObj.status)){ 
                        System.debug('servAppObj.Work_order__r.ParentWorkOrderId).SA_Status__c='+parentWOIdToCurrentStatusMap.get(servAppObj.Work_order__r.ParentWorkOrderId).SA_Status__c);
                        if(BSA_ConstantsUtility.SA_STATUS_DIFFERS_FROM_WO.contains(parentWOIdToCurrentStatusMap.get(servAppObj.Work_order__r.ParentWorkOrderId).SA_Status__c)) {                          
                                servAppObj.Status = BSA_ConstantsUtility.SA_ENROUTE_STATUS;
                                serviceAppointmentListToUpdate.add(servAppObj);
                        }
                        else if(BSA_ConstantsUtility.SA_INCOMPLETE_STATUS.contains(parentWOIdToCurrentStatusMap.get(servAppObj.Work_order__r.ParentWorkOrderId).Status)){
                                servAppObj.Status = BSA_ConstantsUtility.SA_INCOMPLETE_STATUS_APINAME;
                                serviceAppointmentListToUpdate.add(servAppObj);
                        }
                        else{
                            servAppObj.Status = parentWOIdToCurrentStatusMap.get(servAppObj.Work_order__r.ParentWorkOrderId).Status;
                                serviceAppointmentListToUpdate.add(servAppObj);
                        }  
                            
                        }
                    }
                    else{
                        
                        if(!BSA_ConstantsUtility.NOT_ALLOWED_STATUS_SET.contains(woObj.status)){
                            
                            woObj.status = parentWOIdToCurrentStatusMap.get(woObj.ParentWorkOrderId).Status;
                            workOrderListToUpdate.add(woObj);
                        }
                    }
                    
                }
            }
            
            System.debug('serviceAppointmentListToUpdate='+serviceAppointmentListToUpdate);
            System.debug('workOrderListToUpdate='+workOrderListToUpdate);
            
            if(!serviceAppointmentListToUpdate.isEmpty()){
                
                UPDATE serviceAppointmentListToUpdate;
            }
            
            if(!workOrderListToUpdate.isEmpty()){
                
                UPDATE workOrderListToUpdate;
            }
        } catch(Exception excp) {
            
            System.debug('Exception occured while syncing Service appointment with WO status'+excp.getMessage()+excp.getStackTraceString());
        }
        
        
    }

    public void copyRelatedRecordstoChild(map<id,List<WorkOrder>> mapWo){
       
        
        //Just For Time Being --as utility class is not Working
        /*String SobjectApiName = 'Work_Order_Asset__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
 
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }*/
        /*
         *  //Copy Work Order Asset
       Map<String,Schema.DisplayType> mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Work_Order_Asset__c');
        String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());set<id> idset =mapWo.keySet();
        String assetQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Work_Order_Asset__c WHERE Work_Order__c IN :idset';
        System.debug('SAYALI DEBUG:assetQuery-->'+assetQuery);
        List<Work_Order_Asset__c> lstWoAsset =Database.query(assetQuery);
        List<Work_Order_Asset__c> lstWoAssetToInsert = new List<Work_Order_Asset__c>();
        for(Work_Order_Asset__c objAsset :lstWoAsset){
            
            List<WorkOrder> lstChilds =mapWo.get(objAsset.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
               Work_Order_Asset__c childWoAsset  = objAsset.clone(false, false, false, false); 
                childWoAsset.Work_Order__c =objChild.id;
                childWoAsset.Client_Asset_Id__c =objAsset.Client_Asset_Id__c +':'+(String)objChild.id;
                lstWoAssetToInsert.add(childWoAsset);
            }
        }
        //Copy Work Order Contact
        mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Work_Order_Contact__c');
        fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        String woContactQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Work_Order_Contact__c WHERE Work_Order__c IN :idset';
        
        List<Work_Order_Contact__c> lstWoContact =Database.query(woContactQuery);
        List<Work_Order_Contact__c> lstWoContactToInsert = new List<Work_Order_Contact__c>();
        for(Work_Order_Contact__c objCon :lstWoContact){
            
            List<WorkOrder> lstChilds =mapWo.get(objCon.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
               Work_Order_Contact__c childWoContact  = objCon.clone(false, false, false, false); 
                childWoContact.Work_Order__c =objChild.id;
                childWoContact.External_Contact_Id__c =objCon.External_Contact_Id__c +':'+(String)objChild.id;
                lstWoContactToInsert.add(childWoContact);
            }
        }*/
        set<id> idset =mapWo.keySet();
        //Copy Work Order Tasks
        Map<String,Schema.DisplayType> mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Work_Order_Task__c');
        String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        String woTaskQuery = 'SELECT '+ fieldsForSOQL+ ',Work_Order__r.IsCloned__c FROM Work_Order_Task__c WHERE Work_Order__c IN :idset';
        Map<String,Work_Order_Task__c> mExternalIdToWoTask = new Map<String,Work_Order_Task__c>();
        List<Work_Order_Task__c> lstWoTask =Database.query(woTaskQuery);
        List<Work_Order_Task__c> lstWoTaskToInsert = new List<Work_Order_Task__c>();
        Map<Id,List<Work_Order_Task__c>> maprentWotaskToChilds = new Map<Id,List<Work_Order_Task__c>>(); 
        Set<Id> woTaskIds = new Set<Id>();
        for(Work_Order_Task__c objWotask :lstWoTask){
            woTaskIds.add(objWotask.Id);
            List<WorkOrder> lstChilds =mapWo.get(objWotask.Work_Order__c);
            List<Work_Order_Task__c> lstChildWoTaks =new List<Work_Order_Task__c>();
            for(WorkOrder objChild: lstChilds){
                if(!objChild.IsCloned__c){
                    Work_Order_Task__c childWoTask = objWotask.clone(false, false, false, false); 
                    childWoTask.Work_Order__c =objChild.id;
                    childWoTask.Client_Task_Id__c =objWotask.Client_Task_Id__c +':'+(String)objChild.id;
                    //Rohan FR-291: Adding check to distinguish the records created from Apex
            		childWoTask.isBatch__c = true;
                    mExternalIdToWoTask.put(objWotask.Client_Task_Id__c +':'+(String)objChild.id,childWoTask);
                    lstChildWoTaks.add(childWoTask);
                    //lstWoTaskToInsert.add(childWoTask);
                }
            }
            maprentWotaskToChilds.put(objWotask.Id,lstChildWoTaks);
        }
        if(mExternalIdToWoTask!=null && mExternalIdToWoTask.size()>0){
           // System.debug('Rohan : Before Upsert' );
            upsert mExternalIdToWoTask.values();
           // System.debug('Rohan : After Upsert' );
        }
        //Copy Custom Attributes
        
        mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Custom_Attribute__c');
        fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        String woCustomAttQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Custom_Attribute__c WHERE Work_Order__c IN :idset';
        
        List<Custom_Attribute__c> lstCustomAtt =Database.query(woCustomAttQuery);
        List<Custom_Attribute__c> lstCustomAttToInsert = new List<Custom_Attribute__c>();
        for(Custom_Attribute__c objAtt :lstCustomAtt){
            
            List<WorkOrder> lstChilds =mapWo.get(objAtt.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
                
               Custom_Attribute__c childWoAtt = objAtt.clone(false, false, false, false); 
                childWoAtt.Work_Order__c =objChild.id;
                childWoAtt.CA_External_Id__c =objAtt.CA_External_Id__c +':'+(String)objChild.id;
                lstCustomAttToInsert.add(childWoAtt);
            }
        }
        woCustomAttQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Custom_Attribute__c WHERE Work_Order_Task__c IN :woTaskIds';
        
        lstCustomAtt =Database.query(woCustomAttQuery);
        for(Custom_Attribute__c objAtt :lstCustomAtt){
            
            List<Work_Order_Task__c> lstChilds =maprentWotaskToChilds.get(objAtt.Work_Order_Task__c);
            for(Work_Order_Task__c objChild: lstChilds){
                if(!objChild.Work_Order__r.IsCloned__c){
                   Custom_Attribute__c childWoAtt = objAtt.clone(false, false, false, false); 
                    childWoAtt.Work_Order_Task__c =mExternalIdToWoTask.get(objChild.Client_Task_Id__c).id;
                    childWoAtt.CA_External_Id__c =objAtt.CA_External_Id__c +':'+(String)objChild.id;
                    lstCustomAttToInsert.add(childWoAtt);
                }
            }
        }
         //Copy Log Entries
        mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('NBN_Log_Entry__c');
        fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        String woLogQuery = 'SELECT '+ fieldsForSOQL+ ' FROM NBN_Log_Entry__c WHERE Work_Order__c IN :idset';
        
        List<NBN_Log_Entry__c> lstWoLog =Database.query(woLogQuery);
        List<NBN_Log_Entry__c> lstWoLogToInsert = new List<NBN_Log_Entry__c>();
        for(NBN_Log_Entry__c objLog :lstWoLog){
            
            List<WorkOrder> lstChilds =mapWo.get(objLog.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
                NBN_Log_Entry__c childWoLog = objLog.clone(false, false, false, false); 
                childWoLog.Work_Order__c =objChild.id;
                childWoLog.OwnerId = UserInfo.getUserId();
                childWoLog.Unify_External_Note_Id__c =objLog.Unify_External_Note_Id__c +':'+(String)objChild.id;
                lstWoLogToInsert.add(childWoLog);
            }
        }
		 //Copy WO Contacts
        mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Work_Order_Contact__c');
        fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        String woConQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Work_Order_Contact__c WHERE Work_Order__c IN :idset';
        
        List<Work_Order_Contact__c> lstWoCon =Database.query(woConQuery);
        List<Work_Order_Contact__c> lstWoConToInsert = new List<Work_Order_Contact__c>();
        for(Work_Order_Contact__c objCon :lstWoCon){
            
            List<WorkOrder> lstChilds =mapWo.get(objCon.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
                Work_Order_Contact__c childWoCon = objCon.clone(false, false, false, false); 
                childWoCon.Work_Order__c =objChild.id;
                childWoCon.External_Contact_Id__c =objCon.External_Contact_Id__c +':'+(String)objChild.id;
                lstWoConToInsert.add(childWoCon);
            }
        }
        //Copy Payment Task
        /*mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Payment_Task__c');
        fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
        String PaymentTskQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Payment_Task__c WHERE Work_Order__c IN :idset';
        
        List<Payment_Task__c> lstPaymentTsk =Database.query(PaymentTskQuery);
        List<Payment_Task__c> lstPaymentTskToInsert = new List<Payment_Task__c>();
        for(Payment_Task__c objPaymentTsk :lstPaymentTsk){
            
            List<WorkOrder> lstChilds =mapWo.get(objPaymentTsk.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
                Payment_Task__c childPaymentTsk = objPaymentTsk.clone(false, false, false, false); 
                childPaymentTsk.Work_Order__c =objChild.id;
                childPaymentTsk.Client_Task_Id__c =objPaymentTsk.Client_Task_Id__c +':'+(String)objChild.id;
                lstPaymentTskToInsert.add(childPaymentTsk);
            }
        }*/
        /*if(lstWoAssetToInsert!=null && lstWoAssetToInsert.size()>0){
            upsert lstWoAssetToInsert;
        }
        if(lstWoContactToInsert!=null && lstWoContactToInsert.size()>0){
            upsert lstWoContactToInsert;
        }*/
        if(lstCustomAttToInsert!=null && lstCustomAttToInsert.size()>0){
            upsert lstCustomAttToInsert;
        }
        if(lstWoLogToInsert!=null && lstWoLogToInsert.size()>0){
            upsert lstWoLogToInsert Unify_External_Note_Id__c;
        }
        if(lstWoConToInsert!=null && lstWoConToInsert.size()>0){
            upsert lstWoConToInsert;
        }
        /*if(lstPaymentTskToInsert!=null && lstPaymentTskToInsert.size()>0){
            insert lstPaymentTskToInsert;
        }*/
            
    }
    
    private WorkOrderServiceBaseImplementation setAutomationRuleSource(AutomationRuleSource sourceEnum){
        return this;
        
    }
    
    private WorkOrderServiceBaseImplementation setAPINameToIdentifyRule(String fieldAPIName){
        return this;
        
    }
    
    public void setIdentifiers(Map<String,Map<String,String>> WOARNameToFieldAPIMappings){
        
        mWOARNameToFieldAPIMappings = WOARNameToFieldAPIMappings;
        
    }
    
    
    public void setChildWOCount(Integer count){
        
        
    }
    
    public void setDMLOption(Boolean isDMLRequired){
        
        mISDMLRequired = isDMLRequired;
    }
    
    
}