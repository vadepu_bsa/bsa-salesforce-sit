public class WorkOrderContactTriggerHandler implements ITriggerHandler{
	// Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    public static Map<Id,WorkOrder> workOrderMapForMapping = new Map<Id,WorkOrder>();
    public static Boolean taskGenerationInProgress = false;

    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled(){
        return TriggerDisabled;
    }
    
    public void BeforeInsert(List<SObject> workOrderContactList){ 
        Set<Id> parentWOIdSet = new Set<Id>();
        Map<Id, Integer> parentWOCountMap = new Map<Id, Integer>();
        Map<Id, List<Work_Order_Contact__c>> parentWOContactMap = new Map<Id, List<Work_Order_Contact__c>>();
        
        for(SObject obj: workOrderContactList){
            Work_Order_Contact__c wocObj = (Work_Order_Contact__c)obj;
            if(wocObj.Parent_WO_Id__c == '' || wocObj.Parent_WO_Id__c == null){
				parentWOIdSet.add(wocObj.Work_Order__c);
            }
        }
        
        for(sObject obj: [Select Count(Id) childCount, Work_Order__c From Work_Order_Contact__c where Work_Order__c IN: parentWOIdSet Group By Work_Order__c]){
            parentWOCountMap.put((String)obj.get('Work_Order__c'), (Integer)obj.get('childCount'));
        }
        
        System.debug('DildarLog: WorkOrderContactTriggerHandler - parentWOCountMap: ' + parentWOCountMap);
        
        for(SObject obj: workOrderContactList){
            Work_Order_Contact__c wocObj = (Work_Order_Contact__c)obj;
            if(parentWOContactMap.containsKey(wocObj.Work_Order__c)){
                parentWOContactMap.get(wocObj.Work_Order__c).add(wocObj);
            }else{
                parentWOContactMap.put(wocObj.Work_Order__c, new List<Work_Order_Contact__c>{wocObj});
            }
            
            System.debug('DildarLog: WorkOrderContactTriggerHandler - parentWOContactMap: ' + parentWOContactMap);
            for(Id parentRecordId: parentWOContactMap.keySet()){
                Integer count = 1;
                for(Work_Order_Contact__c wocr: parentWOContactMap.get(parentRecordId)){
                    System.debug('DildarLog: WorkOrderContactTriggerHandler - wocr: ' + wocr);
                    wocr.Contact_Count__c = parentWOCountMap.size() < 1 || parentWOCountMap.get(parentRecordId) == 0 || parentWOCountMap.get(parentRecordId) == null ? 1 : parentWOCountMap.get(parentRecordId) + count;
                    count++;
                }
            }                        
        }
        
        System.debug('DildarLog: WorkOrderContactTriggerHandler - workOrderContactList: ' + workOrderContactList);
               
    }
    
    public void AfterInsert(Map<Id, SObject> newItems){
        set<Id> parentWoIds = new set<Id>();
        Set<Id> recIds = new Set<Id>();
        List<Work_Order_Contact__c> lstWoConToInsert = new List<Work_Order_Contact__c>();
        List<Work_Order_Contact__c> lstWoLogToUpdate = new List<Work_Order_Contact__c>();
        Map<Id,Work_Order_Contact__c> mWoConMap = new Map<Id,Work_Order_Contact__c>();
        for(SObject obj : newItems.values()) {
            Work_Order_Contact__c woConObj = (Work_Order_Contact__c)obj;
            if(UserInfo.getFirstName() =='Automated' && (woConObj.Parent_WO_Id__c== null || woConObj.Parent_WO_Id__c=='')){
            	parentWoIds.add(woConObj.Work_Order__c);
                mWoConMap.put(woConObj.Id,woConObj);
            }
            
        }
        if(parentWoIds!=null && parentWoIds.size()>0){
            List<WorkOrder> lstChildWos =[select id,parentWorkOrderId from WorkOrder where parentWorkOrderId in:parentWoIds and Service_Resource__c!='' ];
            if(lstChildWos!=null && lstChildWos.size()>0){
                Map<Id,List<WorkOrder>> mparenttoChildWos = new Map<Id,List<WorkOrder>>();
                for(WorkOrder objWO : lstChildWos){
                    List<WorkOrder> tempLst;
                    if(mparenttoChildWos.keyset().contains(objWO.ParentWorkOrderId)){
                        tempLst = mparenttoChildWos.get(objWO.ParentWorkOrderId);
                    }
                    else{
                        tempLst = new List<WorkOrder>();
                    }
                    tempLst.add(objWO);
                    mparenttoChildWos.put(objWO.ParentWorkOrderId,tempLst);
                }
                for(SObject obj : mWoConMap.values()) {
                    Work_Order_Contact__c objCon = (Work_Order_Contact__c)obj;
                    List<WorkOrder> lstChilds =mparenttoChildWos.get(objCon.Work_Order__c);
                    for(WorkOrder objChild: lstChilds){
                        Work_Order_Contact__c childCon = objCon.clone(false, false, false, false); 
                        childCon.Work_Order__c =objChild.id;
                        childCon.External_Contact_Id__c =childCon.External_Contact_Id__c +':'+(String)objChild.id;
                        lstWoConToInsert.add(childCon);
                    }
                }
            }
        }
        if(lstWoConToInsert!=null && lstWoConToInsert.size()>0){
            TriggerDisabled = True;
            insert lstWoConToInsert;
        }
        
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
}