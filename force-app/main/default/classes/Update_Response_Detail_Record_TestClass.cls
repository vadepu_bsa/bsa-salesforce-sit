@isTest (seeAllData = true)

public class Update_Response_Detail_Record_TestClass {
    
    
        
    @isTest 
    static void invoke_Update_Response_Detail_Record(){
        List <Update_Response_Detail_Record.FlowInputs> fiList = new List <Update_Response_Detail_Record.FlowInputs>();
        List <Update_Response_Detail_Record.FlowOutputs> foList = new List <Update_Response_Detail_Record.FlowOutputs>();
        
        Update_Response_Detail_Record.FlowInputs fi = new Update_Response_Detail_Record.FlowInputs();
        Update_Response_Detail_Record.FlowOutputs fo = new Update_Response_Detail_Record.FlowOutputs();
    	
        Response_Details__c ResponseRecord = new Response_Details__c();    
        ResponseRecord.Question_Number__c = 'a236F0000034PZuQAM';
        ResponseRecord.Question_Text__c = 'First Parent Question';
        ResponseRecord.Response_Text__c = 'Yes';
        
      	List<Response_Details__c> inputResponseDetailsList = New List<Response_Details__c>(); 
        inputResponseDetailsList.add(ResponseRecord);
        
        Response_Details__c outResponseDetailsList = new Response_Details__c();
      
        fi.response_ID = 'a266F00000LZ9wGQAT';
      	fi.input_Response_Details_List = inputResponseDetailsList;
       
        fiList.add(fi);
        System.debug('FILIST------>' + FILIST);
        System.debug('FOLIST------>' + FOLIST);
        
        foList = Update_Response_Detail_Record.invokeThisMethod(fiList);
        
        fo = folist.get(0);
        outResponseDetailsList = fo.output_Response_Details_List.get(0);
  
        System.assertEquals('a266F00000LZ9wGQAT', outResponseDetailsList.Response__c);
    
               
    }  
    
    

}