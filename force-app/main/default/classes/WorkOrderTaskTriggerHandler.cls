public class WorkOrderTaskTriggerHandler implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
    public static Map<Id,WorkOrder> workOrderMapForMapping = new Map<Id,WorkOrder>();
    public static Boolean taskGenerationInProgress = false;

    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }
    
    List<string> allowedProfiles = new List<string>();
    
    public void BeforeInsert(List<SObject> workOrderTaskList) {
        Set<Id> parentWOIdSet = new Set<Id>();
        Map<Id, Integer> parentWOCountMap = new Map<Id, Integer>();
        Map<Id, List<Work_Order_Task__c>> parentWOTaskMap = new Map<Id, List<Work_Order_Task__c>>();
        
        for(SObject obj: workOrderTaskList){
            Work_Order_Task__c wotObj = (Work_Order_Task__c)obj;
            //if(wocObj.External_Contact_Id__c == '' || wocObj.External_Contact_Id__c == null)
			parentWOIdSet.add(wotObj.Work_Order__c);
        }
        
        for(sObject obj: [Select Count(Id) childCount, Work_Order__c From Work_Order_Task__c where Work_Order__c IN: parentWOIdSet Group By Work_Order__c]){
            parentWOCountMap.put((String)obj.get('Work_Order__c'), (Integer)obj.get('childCount'));
        }
        
        System.debug('DildarLog: WorkOrderTaskTriggerHandler - parentWOCountMap: ' + parentWOCountMap);
        
        for(SObject obj: workOrderTaskList){
            Work_Order_Task__c wotObj = (Work_Order_Task__c)obj;
            if(parentWOTaskMap.containsKey(wotObj.Work_Order__c)){
                parentWOTaskMap.get(wotObj.Work_Order__c).add(wotObj);
            }else{
                parentWOTaskMap.put(wotObj.Work_Order__c, new List<Work_Order_Task__c>{wotObj});
            }
            
            System.debug('DildarLog: WorkOrderTaskTriggerHandler - parentWOTaskMap: ' + parentWOTaskMap);
            for(Id parentRecordId: parentWOTaskMap.keySet()){
                Integer count = 1;
                for(Work_Order_Task__c wotr: parentWOTaskMap.get(parentRecordId)){
                    System.debug('DildarLog: WorkOrderTaskTriggerHandler - wotr: ' + wotr);
                    wotr.Task_Count__c = parentWOCountMap.size() < 1 || parentWOCountMap.get(parentRecordId) == 0 || parentWOCountMap.get(parentRecordId) == null ? 1 : parentWOCountMap.get(parentRecordId) + count;
                    count++;
                }
            }                        
        }
        
        System.debug('DildarLog: WorkOrderTaskTriggerHandler - workOrderTaskList: ' + workOrderTaskList);
    }
    public void AfterInsert(Map<Id, SObject> newItems) { 
        set<Id> parentWoIds = new set<Id>();
        List<Work_Order_Task__c> lstWoTaskToInsert = new List<Work_Order_Task__c>();
        Map<Id,Work_Order_Task__c> mWotaskMap = new Map<Id,Work_Order_Task__c>();
        for(SObject obj : newItems.values()) {
            Work_Order_Task__c woTaskObj = (Work_Order_Task__c)obj;
            if(woTaskObj.Parent_WO_Id__c== null || woTaskObj.Parent_WO_Id__c==''){
            	parentWoIds.add(woTaskObj.Work_Order__c);
                mWotaskMap.put(woTaskObj.Id,woTaskObj);
            }
        }
        List<WorkOrder> lstChildWos =[select id,parentWorkOrderId from WorkOrder where parentWorkOrderId in:parentWoIds and Service_Resource__c!='' ];
        if(lstChildWos!=null && lstChildWos.size()>0){
        Map<Id,List<WorkOrder>> mparenttoChildWos = new Map<Id,List<WorkOrder>>();
        for(WorkOrder objWO : lstChildWos){
            List<WorkOrder> tempLst;
            if(mparenttoChildWos.keyset().contains(objWO.ParentWorkOrderId)){
                tempLst = mparenttoChildWos.get(objWO.ParentWorkOrderId);
            }
            else{
                tempLst = new List<WorkOrder>();
            }
            tempLst.add(objWO);
            mparenttoChildWos.put(objWO.ParentWorkOrderId,tempLst);
        }
        for(SObject obj : mWotaskMap.values()) {
            Work_Order_Task__c objWotask = (Work_Order_Task__c)obj;
            List<WorkOrder> lstChilds =mparenttoChildWos.get(objWotask.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
                Work_Order_Task__c childWoTask = objWotask.clone(false, false, false, false); 
                childWoTask.Work_Order__c =objChild.id;
                childWoTask.Client_Task_Id__c =objWotask.Client_Task_Id__c +':'+(String)objChild.id;
                lstWoTaskToInsert.add(childWoTask);
            }
        }
        if(lstWoTaskToInsert!=null && lstWoTaskToInsert.size()>0){
            Insert lstWoTaskToInsert;
        }
        }
         
        
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        if(!Test.isRunningTest()){
            TriggerDisabled =true;
        }        
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<Work_Order_Task__c> UpdateParentTasksFromChild = new List<Work_Order_Task__c>();
        List<Work_Order_Task__c> UpdateChildTasksFromParent = new List<Work_Order_Task__c>();
        Map<Id,List<Work_Order_Task__c>> mIdtoWotask = new Map<Id,List<Work_Order_Task__c>>();
        set<Id> parentWoIds = new set<Id>();
        //Rohan FR-291
       // string currentUserProfileId = userinfo.getProfileId();
        //String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
        //system.debug('ProfileName: '+profileName);
        /*if(allowedProfiles.size()==0)
        {
            allowedProfiles = getProfileSettings();
        } */
        //system.debug('allowedProfiles: '+allowedProfiles);

        System.debug('TriggerDisabled -->'+TriggerDisabled);
        if(TriggerDisabled) return;
        for(SObject obj : newItems.values()) {
            Work_Order_Task__c woTaskObj = (Work_Order_Task__c)obj;
            Work_Order_Task__c oldWoTaskObj = (Work_Order_Task__c)oldItems.get(woTaskObj.Id);
            if(woTaskObj.Parent_WO_Id__c!=null && woTaskObj.Parent_WO_Id__c!=''){
                String extId = woTaskObj.Client_Task_Id__c;
                //FR-291-Rohan 19/07/21 : Added Null check
                if(!String.isBlank(extId))
                {
                List<String> extIds = extId.split(':');
                Work_Order_Task__c objWotask = woTaskObj.clone(false, false, false, false); 
                objWotask.Client_Task_Id__c = extIds[0];
                objWotask.Work_Order__c = woTaskObj.Parent_WO_Id__c;
                //Rohan FR-279 : To distinguish if WOT is created from apex code.
                objWotask.isBatch__c = true;
                UpdateParentTasksFromChild.add(objWotask);
                }
            }
            else{
                parentWoIds.add(woTaskObj.Work_Order__c);
                List<Work_Order_Task__c> newTaskList;
                if(mIdtoWotask.keyset().contains(woTaskObj.Work_Order__c)){
                    newTaskList = mIdtoWotask.get(woTaskObj.Work_Order__c);
                }
                else{
                    newTaskList = new List<Work_Order_Task__c>();
                }
                newTaskList.add(woTaskObj);
                mIdtoWotask.put(woTaskObj.Work_Order__c,newTaskList);
            }
        }
        if(UpdateParentTasksFromChild!=null && UpdateParentTasksFromChild.size()>0){
            Upsert UpdateParentTasksFromChild Client_Task_Id__c;
            TriggerDisabled =true;
        }
        if(parentWoIds!=null & parentWoIds.size()>0)
        {
            List<WorkOrder> lstChildWos = [select id,ParentWorkOrderId from workorder where ParentWorkOrderId in:parentWoIds and Service_Resource__c!=''];
            for(WorkOrder objWO: lstChildWos){
                
                for(Work_Order_Task__c objTask: mIdtoWotask.get(objWO.ParentWorkOrderId)){
                    Work_Order_Task__c objWotask = objTask.clone(false, false, false, false); 
                    objWotask.Client_Task_Id__c = objTask.Client_Task_Id__c+':'+objWO.Id;
                    objWotask.Work_Order__c =objWO.Id;
                    //Rohan FR-279 : To distinguish if WOT is created from apex code.
                	objWotask.isBatch__c = true;
                    UpdateChildTasksFromParent.add(objWotask);
                }
            }
        }
        if(UpdateChildTasksFromParent!=null && UpdateChildTasksFromParent.size()>0){
            TriggerDisabled =true;
            System.debug('TriggerDisabled inSide If--'+TriggerDisabled);
        	upsert UpdateChildTasksFromParent Client_Task_Id__c;
            
        }
        
        
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}

    /* Private Methods*/
    @TestVisible
    private static List<string> getProfileSettings() {
            List<string> profileList = new List<string>();
            List<Unify_Tech_Profile__mdt> profiles = [SELECT Profile_Name__c FROM Unify_Tech_Profile__mdt];
            for (Unify_Tech_Profile__mdt profile : profiles) {
                //system.debug('profiles: '+profiles);
                //system.debug('CurrentProfile Id: '+[Select Id from Profile where Name=:profile.Profile_Name__c].Id);
                profileList.add([Select Id from Profile where Name=:profile.Profile_Name__c].Id);
            }
        //system.debug(profileList);
        return profileList;
   }
}