/**
 * @File Name          : TimeSheetTrigger.trigger
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Ben Lee
 * @Last Modified On   : 10/05/2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    25/08/2020         Tom Henson             Initial Version
 * 1.1    10/05/2021         Ben Lee                Added condition to only update when there is a Pronto ID
**/
public class TimeSheetEntryTriggerHandler implements ITriggerHandler  {
// Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void BeforeInsert(List<SObject> newItems){}   public void BeforeDelete(Map<Id, SObject> oldItems){}
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){} public void AfterDelete(Map<Id, SObject> oldItems){}
    
 
    
    public void AfterInsert(Map<Id, SObject> newItems){
          Set<Id> timeSheetsToSendEvent = new Set<Id>();
        for (Id key : newItems.keySet()) {
         TimeSheetEntry newTs = (TimeSheetEntry)newItems.get(key);
            //if(newTs.Status == 'Submitted' && newTs.TimeSheetId != null){  //Ben Lee - Disabled 20210510
            TimeSheetEntry GetData = [SELECT Id, WorkOrder.Pronto_WO_ID__c  FROM TimeSheetEntry WHERE Id =: newTs.Id LIMIT 1];
            if(GetData != null){
                if(newTs.Status == 'Submitted' && newTs.TimeSheetId != null && GetData.WorkOrder.Pronto_WO_ID__c != null){  //Ben Lee - Added 20210510
                    timeSheetsToSendEvent.add(newTs.Id);
                }
            }
        }
        if (timeSheetsToSendEvent.size() > 0){
            ProntoTimeSheetSerialize.generateProntoTimeSheetPlatformEvent(timeSheetsToSendEvent);
        }
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        Set<Id> timeSheetsToSendEvent = new Set<Id>();
        for (Id key : newItems.keySet()) {
            TimeSheetEntry oldTs = (TimeSheetEntry)oldItems.get(key);
            TimeSheetEntry newTs = (TimeSheetEntry)newItems.get(key);
            TimeSheetEntry GetData = [select Id, WorkOrder.Pronto_WO_ID__c  FROM TimeSheetEntry WHERE Id =: newTs.Id LIMIT 1];
            system.debug('Ben Lee - TimeSheetEntry GetData - ' + GetData);
            if(GetData != null){
                system.debug('Ben Lee - TimeSheetEntry oldTs - ' + oldTs.Status);
                system.debug('Ben Lee - TimeSheetEntry newTs - ' + newTs.Status);
                system.debug('Ben Lee - oldTs.WorkOrder.Pronto_WO_ID__c - ' + oldTs.WorkOrder.Pronto_WO_ID__c);
                system.debug('Ben Lee - newTs.WorkOrder.Pronto_WO_ID__c - ' + newTs.WorkOrder.Pronto_WO_ID__c);            
                //if(oldTs.Status != 'Submitted' && newTs.Status == 'Submitted'){  //Ben Lee - Disabled 20210510
                if(oldTs.Status != 'Submitted' && newTs.Status == 'Submitted' && GetData.WorkOrder.Pronto_WO_ID__c != null){  //Ben Lee - Added 20210510            
                    timeSheetsToSendEvent.add(newTs.Id);
                }
                  system.debug('Ben Lee - GetData.WorkOrder.Pronto_WO_ID__c - ' + GetData.WorkOrder.Pronto_WO_ID__c);
            }
        }
        system.debug('Ben Lee - Before generateProntoTimeSheetPlatformEvent - ' + timeSheetsToSendEvent);
        if (timeSheetsToSendEvent.size() > 0){
            ProntoTimeSheetSerialize.generateProntoTimeSheetPlatformEvent(timeSheetsToSendEvent);
            system.debug('Ben Lee - generateProntoTimeSheetPlatformEvent - ' + timeSheetsToSendEvent);
        }
    }
    
    
    
    public void AfterUndelete(Map<Id, SObject> oldItems){}
}