//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = false)
public class ActionB2BToBSAHandlerTest {
   @testSetup static void setup() {
        
        OperatingHours ohObj = new OperatingHours();
        ohObj.Name = 'Testing';
        ohObj.TimeZone = 'Australia/Sydney';
        insert ohObj;
       
        ServiceTerritory stObj = new ServiceTerritory();
        stObj.Name = 'Testting';
        stObj.Pronto_ID__c = 'TFSS';
        stObj.IsActive = True;
        stObj.Description = 'Testing';
        stObj.APS__c = true;
        stObj.OperatingHoursId = ohObj.Id;
        insert stObj;
       
       Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site').getRecordTypeId();
       Account acc = new Account();
       acc.Name ='Anderson Stuart Building';
       acc.RecordTypeId = accRecordTypeId ;
       acc.Customer_Reference__c = 'F13';
       acc.Status__c = 'Active';
       insert acc;
       
       WorkType wtObj = new WorkType();
       wtObj.Name ='Do  & Charge';
       wtObj.APS__c = true;
       wtObj.Description = 'testing';
       wtObj.EstimatedDuration = 4;    
       insert wtObj;
       
       Asset assetObj = New Asset();
      	assetObj.Name = 'teting';
       assetobj.Status = 'Active';
       assetobj.accountId = acc.Id;
       insert assetobj;
       
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       WorkOrder wObj = new WorkOrder();
       wObj.Client_Work_Order_Number__c = '00044087';
    
       wObj.AccountId = acc.Id;
       wObj.WorkTypeId = wtObj.id;
       Wobj.Description = 'Testing';
       wObj.RecordTypeId = APSRecordTypeId;
       wObj.AssetId = assetObj.Id;
       insert Wobj;
    }
    
   @isTest
    static void InsertSydneyUniTest() {
      
        Action_B2b_To_BSA__e event = new Action_B2b_To_BSA__e();
     	event.Action__c = 'SydneyUniWorkOrder';
  		event.ClientWorkOrderNumber__c ='6061888';
  		event.ServiceContractNumber__c = null;
  		event.EventTimeStamp__c= '2020-09-08T00:42:52.516Z';
  		event.CorrelationId__c = '39c400d2-f16c-11ea-a71a-005056b1eca7';
  		event.WorkOrder__c ='\"workOrderDetails\":{ \"wr_id\": \"6061888\", \"bl_id\": \"F13\", \"bl_name\": \"Anderson Stuart Building\", \"tr_id\": \"TRIPLE M\", \"priority\": \"1\", \"date_assigned\": \"2020-09-07\", \"time_assigned\": \"17:24:41\", \"date_initial_response\": \"\", \"time_initial_response\": \"\", \"date_control_response\": \"\", \"time_control_response\": \"\", \"requestor\": \"MARCUS ROBINSON\", \"email\": \"marcus.robinson@sydney.edu.au\", \"fl_id\": \"04\", \"rm_id\": \"W413\", \"description\": \"Air conditioning is not cooling. Set point 18, room temp 23. AHU 2 is not working. Please investigate faults and rectify.Lab will be in use for dissection tomorrow from 12pm. Commented by OMER MOHAMAD at 8/9/2020 8:51:27 Comments: **CAMPUS RESTART** 2. Impacts air quality/temperature and impacting research. Please investigate room W401, AC in fault. Commented by OMER MOHAMAD at 8/9/2020 9:13:7 Comments: Ignore W413 as it being rectified under a different WR, only attend to W401. OM to call to Emma to notify verbally. Thank you\", \"phone\": \"9351 2816, 0424 021 728\", \"quotation_required\": \"No\"}';
        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
        Test.stopTest();
    }
    
    
      @isTest
    static void updateProntoWOerror_Test() {
      
        
        list<WorkOrder> workorderList = [SELECT id, WorkorderNumber FROM WorkOrder LIMIT 1];
        Action_B2b_To_BSA__e event = new Action_B2b_To_BSA__e();
       	event.Action__c='ProntoWorkorder';
		event.ClientWorkOrderNumber__c='00044088';
		event.ServiceContractNumber__c='45-049';
		event.EventTimeStamp__c='2020-09-16T07:10:40Z';
		event.CorrelationId__c='ID000000000027';
		event.WorkOrder__c='\"workOrderDetails\":{\"workorderId\":\"00044088\",\"prontoWorkorderId\":\"1300029\",\"revenue-amount\":\"0\",\"cost_amount\":\"890\",\"margin-amount\":\"0\",\"labour-amount\":\"0\",\"material-amount\":\"890\",\"sub-contractor_amount\":\"0\",\"dummy\":\"0\"}';


        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
      
        Test.stopTest();

      
    }
    
     @isTest
    static void updateProntoWO_Test() {
      
        
        list<WorkOrder> workorderList = [SELECT id, WorkorderNumber FROM WorkOrder LIMIT 1];
        Action_B2b_To_BSA__e event = new Action_B2b_To_BSA__e();
       	event.Action__c='ProntoWorkorder';
		event.ClientWorkOrderNumber__c='00044087';
		event.ServiceContractNumber__c='45-049';
		event.EventTimeStamp__c='2020-09-16T07:10:40Z';
		event.CorrelationId__c='ID000000000027';
		event.WorkOrder__c='\"workOrderDetails\":{\"workorderId\":\"'+workorderList[0].WorkorderNumber+'\",\"prontoWorkorderId\":\"1300029\",\"revenue-amount\":\"0\",\"cost_amount\":\"890\",\"margin-amount\":\"0\",\"labour-amount\":\"0\",\"material-amount\":\"890\",\"sub-contractor_amount\":\"0\",\"dummy\":\"0\"}';


        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
      
        Test.stopTest();

      
    }

}