public class ProductTriggerHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //After insert method 
    public void AfterInsert(Map<Id, SObject> newItems) { 
        List<Product2> productupdatelist = new List<Product2>(); 
        
        Set<Id> productIdset = new Set<Id>();
       
        for(SObject obj : newItems.values()) {
            Product2 proObj = (Product2)obj;
          
            if(String.isBlank(proObj.Correlation_ID__c) && !proObj.Pronto_Synced__c) {
                productIdset.add(obj.Id);
                productupdatelist.add(New Product2(Id =obj.Id, Pronto_Synced__c = true ));
            }
        }
        
        if (productIdset.size()>0 && !TriggerDisabled ){
            ProntoProductSerialize.generateProntoProductPlatformEvent(productIdset);
            
            update productupdatelist;
            TriggerDisabled = true;
        }
      
        
    }
    
    //After update method 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) { 
     
    }
    public void BeforeInsert(List<SObject> newItems) {}
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    public void AfterUndelete(Map<Id, SObject> oldItems) {}


}