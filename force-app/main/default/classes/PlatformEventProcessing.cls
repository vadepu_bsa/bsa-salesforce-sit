/**
* @author          Karan Shekhar
* @date            20/Jan/2020	
* @description    
* Change Date    Modified by         Description  
* 20/Jan/2020    Karan Shekhar		Entry point class for publishing all platform Events. Supports only below Object to PE mapping	
									ProductConsumed --> Pronto_Van_Stock_Consumption_Update__e
**/

public class PlatformEventProcessing {

    //@InvocableMethod(Label='Process Platform Event')
    public static void  processPlatformEvent(List<sObject> sObjectList, Boolean shouldPublishPE){
        
        IPlatformEvent eventHandler = new PlatformEventFactory(sObjectList[0],sObjectList,shouldPublishPE).getHandlerName();        
        eventHandler.processPlatformEvent();      
        
    }
}