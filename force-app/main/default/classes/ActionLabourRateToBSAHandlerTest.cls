@IsTest
public class ActionLabourRateToBSAHandlerTest {

    @TestSetup
    static void makeData(){
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account1 = new Account();
        account1.Name = 'Test Account';
        account1.RecordTypeId = clientRecordTypeId;
        account1.CustomerCode__c = '56789';
        account1.Status__c = 'Active';
        insert account1;

        OperatingHours oh = new OperatingHours();
        oh.Name = 'ohTest';
        insert oh;
        
        ServiceTerritory st = new ServiceTerritory();
        st.Pronto_ID__c = 'prontoId'; 
        st.OperatingHoursId = oh.Id;
        st.IsActive = true;
        st.name = 'serviceTerr';
        insert st;

        Labour_Rate__c lr = new Labour_Rate__c();
        //lr.Technician_Category__c = 'E';
        //lr.External_Id__c = 'ACC007_A_~';
        //lr.Normal_Rate__c = 50.00;
        //lr.Normal_Time_Blk__c = 60;
        
        lr.Account__c = account1.Id;
        lr.After_Hours_Minimum_Rates__c = 50.00;
        lr.After_Hours_Minimum_Time_Blk__c = 60;
        lr.After_Hours_Normal_Rate__c = 50.00;
        lr.After_Hours_Normal_Time_Blk__c = 60;
        lr.Minimum_Rates__c = 20.00;
        lr.Minimum_Time_Blk__c = 60;
        lr.Normal_Rate__c = 50.00;
        lr.Normal_Time_Blk__c = 60;
        lr.Charge_Type__c = 'Day';
        lr.Job_Type__c = 'Electrician';
        lr.Technician_Category__c = 'E';
        insert lr;        
        
        System.debug('Ben Test: ' + lr);
    }

    @isTest
    static void createLabourRateTest(){

        Action_LabourRate_to_BSA__e event = new Action_LabourRate_to_BSA__e();
        event.Action__c='CreateLabourRate';
        event.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event.CorrelationId__c='9eb99c5f-fdeb-6edc-aa4c-a08d33434234325345234334';
        event.AccountCode__c ='56789';
        event.LabourRates__c='"labourdetails":[{"dummy":"56789","csr-key-account":"56789","csr-engineer-category":"Engineer","csr-charge-type":"~","csr-rates[1]":"100","csr-rates[2]":"60","csr-rates[3]":"0","csr-rates[4]":"0","ah-nr":"130","ah-ntb":"60","ah-mr":"0","ah-mtb":"0","csr-key-territory":"prontoId","record-id":"ACC007_A_~"}]';

        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
        Test.stopTest();

        Labour_Rate__c labourRate = [SELECT Id, Technician_Category__c From Labour_Rate__c LIMIT 1];
   
    }
    
    @isTest
    static void deleteLabourRateTest(){
        Action_LabourRate_to_BSA__e event2 = new Action_LabourRate_to_BSA__e();
        event2.Action__c='CreateLabourRate';
        event2.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event2.CorrelationId__c='9eb99c5f-fdeb-6edc-aa4c-a08d33434234325345234334';
        event2.AccountCode__c ='56789';
        event2.LabourRates__c='"labourdetails":[{"dummy":"56789","csr-key-account":"56789","csr-engineer-category":"Engineer","csr-charge-type":"~","csr-rates[1]":"100","csr-rates[2]":"60","csr-rates[3]":"0","csr-rates[4]":"0","ah-nr":"130","ah-ntb":"60","ah-mr":"0","ah-mtb":"0","csr-key-territory":"prontoId","record-id":"ACC007_A_~","Operations":"Delete"}]';
        
        Labour_Rate__c lr = [select Id, Name, Operations__c from Labour_Rate__c Limit 1];
        lr.Operations__c = 'Delete';
        update lr;
        
        Test.startTest();
        // Publish test event
        Database.SaveResult sr2 = EventBus.publish(event2);
        Test.stopTest();

       }              
}