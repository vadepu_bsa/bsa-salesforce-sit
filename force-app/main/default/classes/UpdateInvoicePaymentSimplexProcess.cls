global class UpdateInvoicePaymentSimplexProcess {
    
    public UpdateInvoicePaymentSimplexProcess(Set<Id> successWOIdSet){
        List<WorkOrder> lwo = [SELECT Id,Service_Resource__c,Total_Summary_Cost_BSA__c,Total_Summary_Cost_Tech__c,servicecontract.AccountId,Client_Work_Order_Number__c from WorkOrder where Id IN: successWOIdSet];
        Set<Id> woIds = new Set<Id>();
        Map<String, WorkOrder> woMap = new Map<String, WorkOrder>();
        for(WorkOrder wo: lwo){
            woMap.put(wo.Client_Work_Order_Number__c, wo);
            woIds.add(wo.Id);
        }
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        List<Payment__c> paymentList = new List<Payment__c>();
        Map<String, List<Line_Item__c>> newWOLIMap = new Map<String, List<Line_Item__c>>();        
        for(Line_Item__c li: [Select Id, BSA_Cost__c, Technician_Cost__c, Work_Order__r.Client_Work_Order_Number__c From Line_Item__c Where Work_Order__c IN:woIds AND Status__c='Pending']){
            if(newWOLIMap.containsKey(li.Work_Order__r.Client_Work_Order_Number__c)){
                newWOLIMap.get(li.Work_Order__r.Client_Work_Order_Number__c).add(li);
            }else{
                newWOLIMap.put(li.Work_Order__r.Client_Work_Order_Number__c, new List<Line_Item__c> {li});
            }
        }
        
        List<Line_Item__c> liListToUpdate = new List<Line_Item__c>();
        if(newWOLIMap.size() > 0){
            for(String newLIWOKey: newWOLIMap.keySet()){
                Invoice__c inv = new Invoice__c();
                Payment__c pay = new Payment__c();
                for(Line_Item__c li: newWOLIMap.get(newLIWOKey)){                 
                    li.Payment__r = new Payment__c(Payment_External_Id__c = newLIWOKey);
                    li.Invoice__r = new Invoice__c(Invoice_External_Id__c = newLIWOKey);
                    liListToUpdate.add(li);                    
                }
                
                inv.Work_Order__r = new WorkOrder(Client_Work_Order_Number__c=newLIWOKey);
                inv.BSA_Total__c = woMap.get(newLIWOKey).Total_Summary_Cost_BSA__c;
                inv.Invoice_External_Id__c = newLIWOKey;
                inv.Account__c = woMap.get(newLIWOKey).servicecontract.AccountId;
                invoiceList.add(inv);
                
                pay.Work_Order__r = new WorkOrder(Client_Work_Order_Number__c=newLIWOKey);
                pay.Amount__c = woMap.get(newLIWOKey).Total_Summary_Cost_Tech__c;
                pay.Payment_External_Id__c = newLIWOKey;
                pay.Service_Resource__c = (woMap.get(newLIWOKey)).Service_Resource__c;
                pay.Status__c = 'Pending';
                paymentList.add(pay);
            }
        }
        if(invoiceList.size() > 0){
            Database.UpsertResult[] upsertINVResult = Database.upsert(invoiceList, Invoice__c.Fields.Invoice_External_Id__c, false);
            System.debug('DildarLog: upsertINVResult - ' + upsertINVResult);
        }
        if(paymentList.size() > 0){
            Database.UpsertResult[] upsertPAYResult = Database.upsert(paymentList, Payment__c.Fields.Payment_External_Id__c, false);
            System.debug('DildarLog: upsertPAYResult - ' + upsertPAYResult);
        }
        if(liListToUpdate.size() > 0){
            //Upsert lineitem/sor data for all simplex workorders with payment and invoice lookup
            Database.SaveResult[] updateLIResultWithINVPAY = Database.update(liListToUpdate, false);
        }
    }
}