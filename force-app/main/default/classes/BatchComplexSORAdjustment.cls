global class BatchComplexSORAdjustment Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
	global String strQuery;
    public List<String> batchException = new List<String>();
    global Set<Id> successWOSet = new Set<Id>();
    global String stagingRecordType = 'Complex_SOR_Adjustment_Before_PA';
    
    //Initialize the query string
    global BatchComplexSORAdjustment(){
        String queryFields = 'Work_Order_Id__c,'+
            'SOR_Id__c,'+
            'SOR_Quantity__c,'+
            'Child_Work_Order_Number__c,'+
            'Status__c';
        strQuery = 'SELECT '+queryFields+' from Simplex_Activity_Staging__c Where RecordType.DeveloperName =:stagingRecordType AND IsProcessed__c = false AND SOR_Id__c !=\'\' ';        
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Simplex_Activity_Staging__c> stagingRecordList){
        Id complexWORecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Child_CUI_Work_Order').getRecordTypeId();
        
        //Store all success and failure on staging
        Map<String, Simplex_Activity_Staging__c> activityStagingMap = new Map<String, Simplex_Activity_Staging__c>();
        
        Set<String> workOrderClientIdSet = new Set<String>();
        
        //Store all staging records
        Map<String, List<Simplex_Activity_Staging__c>> workOrderStagingMap = new Map<String, List<Simplex_Activity_Staging__c>>();
        
        //Store all sor records against workorder
        Map<String, Set<Line_Item__c>> workOrderLineItemMap = new Map<String, Set<Line_Item__c>>();
        
        //Store all SOR Codes uniquely
        Set<String> sorCodeSet = new Set<String>();
        
        //Create sets to store unique data from the staging records
        for(Simplex_Activity_Staging__c stagingRecord:stagingRecordList){
            workOrderClientIdSet.add(stagingRecord.Child_Work_Order_Number__c);
            sorCodeSet.add(stagingRecord.SOR_Id__c);
            if(workOrderStagingMap.containsKey(stagingRecord.Child_Work_Order_Number__c)){
                workOrderStagingMap.get(stagingRecord.Child_Work_Order_Number__c).add(stagingRecord);
            }else{
                workOrderStagingMap.put(stagingRecord.Child_Work_Order_Number__c, new List<Simplex_Activity_Staging__c>{stagingRecord});
            }
        }
        //System.debug('DildarLog: workOrderStagingMap - ' + workOrderStagingMap);
        
        Map<String, PriceBookEntry> sorPriceBookEntryMap = new Map<String, PriceBookEntry>();
        Map<String, PriceBookEntry> sorPriceBookEntryDirectMap = new Map<String, PriceBookEntry>();
        for(PriceBookEntry pbe: [Select Id, Pricebook2Id, Product2Id, BSA_Cost__c, Tech_Cost__c, UnitPrice, ProductCode From PriceBookEntry Where Task_Code_Formula__c != '' AND ProductCode IN: sorCodeSet]){
        	sorPriceBookEntryMap.put(pbe.ProductCode+'-'+pbe.Pricebook2Id, pbe);
            sorPriceBookEntryDirectMap.put(pbe.Id, pbe);
    	}
        
        //Prepare client work order number and SF simplex work order map 
        Map<String, WorkOrder> complexWOMap = new Map<String, WorkOrder>();        
        for(WorkOrder WO:[Select Id, WorkOrderNumber, Sub_Status__c, ParentWorkOrder.Client_Work_Order_Number__c, Pricebook2Id From WorkOrder Where WorkOrderNumber IN : workOrderClientIdSet AND RecordTypeId =:complexWORecordTypeId AND Sub_Status__c != 'Pending Approval']){
            complexWOMap.put(wo.WorkOrderNumber, WO);            
        }
        //System.debug('DildarLog: complexWOMap - ' + complexWOMap);
        
        //Prepare product consumed map with Child WorkOrder Number to use it later
        Map<String, Map<String, ProductConsumed>> complexWOPCMap = new Map<String, Map<String, ProductConsumed>>();
        for(ProductConsumed pc: [Select Id, WorkOrderId, WorkOrder.WorkOrderNumber, WorkOrder.ParentWorkOrder.Client_Work_Order_Number__c, Product2.ProductCode, Product2Id, PricebookEntryId from ProductConsumed Where WorkOrder.WorkOrderNumber IN: complexWOMap.keySet()]){
        	if(complexWOPCMap.containsKey(pc.WorkOrder.WorkOrderNumber)){
                complexWOPCMap.get(pc.WorkOrder.WorkOrderNumber).put(pc.Product2.ProductCode, pc);
            }else{
                Map<String, ProductConsumed> mp = new Map<String, ProductConsumed>();
                mp.put(pc.Product2.ProductCode, pc);
                complexWOPCMap.put(pc.WorkOrder.WorkOrderNumber, new Map<String, ProductConsumed>(mp));
            }
            //System.debug('DildarLog: Product_Code__c ' + pc.Product2.ProductCode);
        }
        //System.debug('DildarLog: complexWOPCMap - ' + complexWOPCMap);
        
        //Prepare line item map with Child WorkOrder Number to use it later
        Map<String, Map<String, Line_Item__c>> complexWOLIMap = new Map<String, Map<String, Line_Item__c>>();
        for(Line_Item__c li: [Select Id, SOR_Code__c, Work_Order__c, Work_Order__r.WorkOrderNumber, Work_Order__r.ParentWorkOrder.Client_Work_Order_Number__c from Line_Item__c Where Work_Order__r.WorkOrderNumber IN: complexWOMap.keySet() AND Status__c != 'Cancelled' AND Status__c != 'Committed']){
        	if(complexWOLIMap.containsKey(li.Work_Order__r.WorkOrderNumber)){
                complexWOLIMap.get(li.Work_Order__r.WorkOrderNumber).put(li.SOR_Code__c, li);
            }else{
                Map<String, Line_Item__c> liMap = new Map<String, Line_Item__c>();
                liMap.put(li.SOR_Code__c, li);
                complexWOLIMap.put(li.Work_Order__r.WorkOrderNumber, new Map<String, Line_Item__c>(liMap));
            }
            System.debug('DildarLog: LineItem.SOR_Code__c ' + li.SOR_Code__c);
        }
    	
        //System.debug('DildarLog: complexWOLIMap - ' + complexWOLIMap);
        
        List<ProductConsumed> pcListToUpdate = new List<ProductConsumed>();
        List<ProductConsumed> pcListToDelete = new List<ProductConsumed>();
        List<ProductConsumed> pcListToAdd = new List<ProductConsumed>();
        List<Line_Item__c> liListToUpdate = new List<Line_Item__c>();
        List<Line_Item__c> liListToDelete = new List<Line_Item__c>();
        List<Line_Item__c> liListToAdd = new List<Line_Item__c>();
        
        if(workOrderStagingMap.size() > 0){
            for(String cwon: workOrderStagingMap.keySet()){
                if(complexWOMap.get(cwon) != null && complexWOMap.get(cwon).sub_status__c != 'Ready For Review'){
                    //if(complexWOPCMap.get(cwon) != null){
                        for(Simplex_Activity_Staging__c sas: workOrderStagingMap.get(cwon)){
                            if(complexWOPCMap.get(cwon) != null && complexWOPCMap.get(cwon).get(sas.SOR_Id__c) != null && sas.SOR_Id__c == complexWOPCMap.get(cwon).get(sas.SOR_Id__c).Product2.ProductCode){
                                if(sas.Status__c != 'Cancel Line'){
                                    ProductConsumed pcToUpdate = new ProductConsumed();
                                    pcToUpdate.Id = complexWOPCMap.get(cwon).get(sas.SOR_Id__c).Id;
                                    pcToUpdate.QuantityConsumed = Decimal.valueOf(sas.SOR_Quantity__c);
                                    pcToUpdate.Activity_Staging_Ref_Id__c = sas.Id;
                                    pcListToUpdate.add(pcToUpdate);
                                }else{
                                    ProductConsumed pcToDelete = complexWOPCMap.get(cwon).get(sas.SOR_Id__c);
                                    pcToDelete.Activity_Staging_Ref_Id__c = sas.Id;
                                    pcListToDelete.add(pcToDelete);
                                }
                            }else{
                                String sorPriceBookCode = sas.SOR_Id__c+'-'+(complexWOMap.get(cwon)).Pricebook2Id;
                                ProductConsumed pcToAdd = new ProductConsumed();
                                pcToAdd.QuantityConsumed = Decimal.valueOf(sas.SOR_Quantity__c);
                                pcToAdd.WorkOrderId = complexWOMap.get(cwon).Id;
                                pcToAdd.PricebookEntryId = sorPriceBookEntryMap.get(sorPriceBookCode).Id;
                                pcToAdd.Activity_Staging_Ref_Id__c = sas.Id;
                                pcListToAdd.add(pcToAdd);
                            }
                        }
                    //}
                }else if(complexWOMap.get(cwon) != null && complexWOMap.get(cwon).sub_status__c == 'Ready For Review'){
                    //if(complexWOLIMap.get(cwon) != null){
                        for(Simplex_Activity_Staging__c sas: workOrderStagingMap.get(cwon)){
                            String sorPriceBookCode = sas.SOR_Id__c+'-'+(complexWOMap.get(cwon)).Pricebook2Id;
                            if(complexWOLIMap.get(cwon)!= null && complexWOLIMap.get(cwon).get(sas.SOR_Id__c) != null && sas.SOR_Id__c == complexWOLIMap.get(cwon).get(sas.SOR_Id__c).SOR_Code__c){
                                if(sas.Status__c != 'Cancel Line'){
                                    Line_Item__c updateLI = new Line_Item__c();
                                    updateLI.Quantity__c = Decimal.valueOf(sas.SOR_Quantity__c);
                                    updateLI.Id = complexWOLIMap.get(cwon).get(sas.SOR_Id__c).Id;
                                    Decimal bsaUnitRate = (sorPriceBookEntryMap.get(sorPriceBookCode)).BSA_Cost__c != null ? (sorPriceBookEntryMap.get(sorPriceBookCode)).BSA_Cost__c : (sorPriceBookEntryMap.get(sorPriceBookCode)).UnitPrice;
                                    Decimal techUnitRate = (sorPriceBookEntryMap.get(sorPriceBookCode)).Tech_Cost__c != null ? (sorPriceBookEntryMap.get(sorPriceBookCode)).Tech_Cost__c : (sorPriceBookEntryMap.get(sorPriceBookCode)).UnitPrice;
                                    updateLI.BSA_Unit_Rate__c = bsaUnitRate;
                                    updateLI.BSA_Cost__c = bsaUnitRate * Decimal.valueOf(sas.SOR_Quantity__c);
                                    updateLI.Technician_Unit_Rate__c = techUnitRate;
                                    updateLI.Technician_Cost__c = techUnitRate * Decimal.valueOf(sas.SOR_Quantity__c);
                                    updateLI.Activity_Staging_Ref_Id__c = sas.Id;
                                    liListToUpdate.add(updateLI);                                     
                                }else{
                                    Line_Item__c deleteLI = (Line_Item__c)complexWOLIMap.get(cwon).get(sas.SOR_Id__c);
                                    deleteLI.Activity_Staging_Ref_Id__c = sas.Id;
                                    deleteLI.Status__c = 'Cancelled';
                                    liListToDelete.add(deleteLI);
                                }
                            }else{
                                Line_Item__c newLI = new Line_Item__c();
                                newLI.Quantity__c = Decimal.valueOf(sas.SOR_Quantity__c);
                                newLI.SOR_Code__c = sas.SOR_Id__c;
                                newLI.Work_Order__c = complexWOMap.get(cwon).Id;
                                Decimal bsaUnitRate = (sorPriceBookEntryMap.get(sorPriceBookCode)).BSA_Cost__c != null ? (sorPriceBookEntryMap.get(sorPriceBookCode)).BSA_Cost__c : (sorPriceBookEntryMap.get(sorPriceBookCode)).UnitPrice;
                                Decimal techUnitRate = (sorPriceBookEntryMap.get(sorPriceBookCode)).Tech_Cost__c != null ? (sorPriceBookEntryMap.get(sorPriceBookCode)).Tech_Cost__c : (sorPriceBookEntryMap.get(sorPriceBookCode)).UnitPrice;
                                newLI.BSA_Unit_Rate__c = bsaUnitRate;
                                newLI.BSA_Cost__c = bsaUnitRate * Decimal.valueOf(sas.SOR_Quantity__c);
                                newLI.Technician_Unit_Rate__c = techUnitRate;
                                newLI.Technician_Cost__c = techUnitRate * Decimal.valueOf(sas.SOR_Quantity__c);
                                newLI.Price_Book__c = (complexWOMap.get(sas.Child_Work_Order_Number__c)).Pricebook2Id;
                                newLI.Activity_Staging_Ref_Id__c = sas.Id;
                                liListToAdd.add(newLI);
                            }
                        }
                    //}
                }
            }
        }
        
        //System.debug('DildarLog: pcListToUpdate:List - ' + pcListToUpdate);
        //System.debug('DildarLog: pcListToAdd:List - ' + pcListToUpdate);
        //System.debug('DildarLog: liListToUpdate:List - ' + liListToUpdate);
        //System.debug('DildarLog: liListToAdd:List - ' + liListToAdd);
        
        if(pcListToUpdate.size() > 0){
            Database.SaveResult[] pcListToUpdateResult = Database.update(pcListToUpdate, false);
            for(integer i =0; i<pcListToUpdate.size();i++){
                String msg='';
                ProductConsumed pcObj = pcListToUpdate[i];
                If(pcListToUpdateResult[i].isSuccess()){
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(pcObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(pcObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =pcObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;                    
                    }
                    activityStagingMap.put(pcObj.Activity_Staging_Ref_Id__c, objStaging);
                }
                else{
                    msg +='Error: "';        
                    for(Database.Error err: pcListToUpdateResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(pcObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(pcObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;
                        objStaging.SOR_Error_Message__c = msg;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =pcObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;                    
                        objStaging.SOR_Error_Message__c = msg;
                    }
                    activityStagingMap.put(pcObj.Activity_Staging_Ref_Id__c, objStaging);             
                }        
            }
        }
        if(pcListToAdd.size() > 0){
            Database.SaveResult[] pcListToAddResult = Database.insert(pcListToAdd, false);
            for(integer i =0; i<pcListToAdd.size();i++){
                String msg='';
                ProductConsumed pcObj = pcListToAdd[i];
                If(pcListToAddResult[i].isSuccess()){
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(pcObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(pcObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =pcObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;                    
                    }
                    activityStagingMap.put(pcObj.Activity_Staging_Ref_Id__c, objStaging);
                }
                else{
                    msg +='Error: "';        
                    for(Database.Error err: pcListToAddResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(pcObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(pcObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;
                        objStaging.SOR_Error_Message__c = msg;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =pcObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;                    
                        objStaging.SOR_Error_Message__c = msg;
                    }
                    activityStagingMap.put(pcObj.Activity_Staging_Ref_Id__c, objStaging);             
                }        
            }
		}
        if(pcListToDelete.size() > 0){
            System.debug('DildarLog: pcListToDelete - ' + pcListToDelete);
            
            Database.DeleteResult[] pcListToDeleteResult = Database.delete(pcListToDelete, false);
            for(integer i =0; i<pcListToDelete.size();i++){
                String msg='';
                ProductConsumed pcObj = pcListToDelete[i];
                If(pcListToDeleteResult[i].isSuccess()){
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(pcObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(pcObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =pcObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;                    
                    }
                    activityStagingMap.put(pcObj.Activity_Staging_Ref_Id__c, objStaging);
                }
                /*else{
                    msg +='Error: "';        
                    for(Database.Error err: pcListToDeleteResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(pcObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(pcObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;
                        objStaging.SOR_Error_Message__c = msg;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =pcObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;                    
                        objStaging.SOR_Error_Message__c = msg;
                    }
                    activityStagingMap.put(pcObj.Activity_Staging_Ref_Id__c, objStaging);             
                }*/        
            }
        }
        if(liListToUpdate.size() > 0){
            Database.SaveResult[] liListToUpdateResult = Database.update(liListToUpdate, false);
            for(integer i =0; i<liListToUpdate.size();i++){
                String msg='';
                Line_Item__c liObj = liListToUpdate[i];
                If(liListToUpdateResult[i].isSuccess()){
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;                    
                    }
                    activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);
                }
                else{
                    msg +='Error: "';        
                    for(Database.Error err: liListToUpdateResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;
                        objStaging.SOR_Error_Message__c = msg;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;                    
                        objStaging.SOR_Error_Message__c = msg;
                    }
                    activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);             
                }        
            }
        }
        if(liListToAdd.size() > 0){
            Database.SaveResult[] liListToAddResult = Database.insert(liListToAdd, false);
            for(integer i =0; i<liListToAdd.size();i++){
                String msg='';
                Line_Item__c liObj = liListToAdd[i];
                If(liListToAddResult[i].isSuccess()){
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;                    
                    }
                    activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);
                }
                else{
                    msg +='Error: "';        
                    for(Database.Error err: liListToAddResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;
                        objStaging.SOR_Error_Message__c = msg;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;                    
                        objStaging.SOR_Error_Message__c = msg;
                    }
                    activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);             
                }        
            }
        }
        if(liListToDelete.size() > 0){
            //System.debug('DildarLog: liListToDelete - ' + liListToDelete);
            
            Database.SaveResult[] liListToDeleteResult = Database.update(liListToDelete, false);
            for(integer i =0; i<liListToDelete.size();i++){
                String msg='';
                Line_Item__c liObj = liListToDelete[i];
                If(liListToDeleteResult[i].isSuccess()){
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = true;
                        objStaging.IsProcessed__c = true;                    
                    }
                    activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);
                }
                /*else{
                    msg +='Error: "';        
                    for(Database.Error err: liListToDeleteResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    Simplex_Activity_Staging__c objStaging;
                    if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                        objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;
                        objStaging.SOR_Error_Message__c = msg;
                    }else{
                        objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                        objStaging.IsSORSuccess__c = false;
                        objStaging.IsProcessed__c = true;                    
                        objStaging.SOR_Error_Message__c = msg;
                    }
                    activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);             
                }*/        
            }
        }
		
        //System.debug('DildarLog: activityStagingMap:List - ' + activityStagingMap);
        if(activityStagingMap.size() > 0 ){
            Update activityStagingMap.values();
        }
    }
	global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exception list
        
	}
}