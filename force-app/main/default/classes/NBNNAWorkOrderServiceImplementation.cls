/**
 * @File Name          : NBNNAWorkOrderServiceImplementation.cls
 * @Description        : 
 * @Author             : Karan Shekhar
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 11/05/2020, 4:57:56 pm
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/05/2020   Karan Shekhar     Initial Version
**/
public with sharing class NBNNAWorkOrderServiceImplementation extends WorkOrderServiceBaseImplementation {
    public static boolean isWOUpdated= false;
    String WT_AUTOMATION = 'Work_Type_Automation';
        String ST_AUTOMATION = 'Service_Territory_Automation';
        String PB_AUTOMATION = 'Price_Book_Automation';
        String WOLI_AUTOMATION = 'WOLI_Automation';
        String SM_AUTOMATION = 'Status_Mapping_Automation';
    	String SV_AUTOMATION = 'State_Validation_Automation';
    	String GL_AUTOMATION = 'GL_Code_Automation';
    	String PT_AUTOMATION = 'Pronto_Territory_Automation';
    public NBNNAWorkOrderServiceImplementation() {
        
        
        Map<String, Map<String,String>> mWOARNameToFieldAPIMappings = new Map<String, Map<String,String>>();
        Map<String,String> str = new Map<String,String>();
        str.put('NBN_Field_Work_Specified_By_Id__c','NBN_Field_Work_Specified_By_Id__c');
        str.put('Work_Area__c','Work_Area__c');
        mWOARNameToFieldAPIMappings.put(WT_AUTOMATION,str);
        //setWorkTypeIdentifiers(str);
        
        Map<String,String> strST = new Map<String,String>();
        strST.put('SAM_ID__c','Service_Area_Module__c');
        mWOARNameToFieldAPIMappings.put(ST_AUTOMATION,strST);
        //setServiceTerritoryIdentifiers(strST);
        //
        Map<String,String> strPB = new Map<String,String>();
        //strPB.put('WorkTypeId','Work_Type__c');
        strPB.put('ServiceTerritoryId','Service_Territory__c');
        strPB.put('Record_Type_Name__c','Record_Type_Name__c');
        mWOARNameToFieldAPIMappings.put(PB_AUTOMATION,strPB);
        
        Map<String,String> strWoli = new Map<String,String>();
        strWoli.put('WorkTypeId','Work_Type__c');
        mWOARNameToFieldAPIMappings.put(WOLI_AUTOMATION,strWoli);
        
        Map<String,String> strSM = new Map<String,String>();
        strSM.put('SA_Status__c','State__c');
        mWOARNameToFieldAPIMappings.put(SM_AUTOMATION,strSM);
        
        Map<String,String> strSV = new Map<String,String>();
        strSV.put('Client_Inbound_Action__c','Action__c');
        strSV.put('NBN_Field_Work_Specified_By_Id__c','NBN_Field_Work_Specified_By_Id__c');
        mWOARNameToFieldAPIMappings.put(SV_AUTOMATION,strSV);
        
        Map<String,String> strGL = new Map<String,String>();
        strGL.put('Record_Type_Name__c','Record_Type_Name__c');
        strGL.put('State','State__c');
        //strGL.put('Primary_Access_Technology__c','Primary_Access_Technology__c');
        mWOARNameToFieldAPIMappings.put(GL_AUTOMATION,strGL);
        
        Map<String,String> strPT = new Map<String,String>();
        strPT.put('Record_Type_Name__c','Record_Type_Name__c');
        strPT.put('State','State__c');
        //strPT.put('Primary_Access_Technology__c','Primary_Access_Technology__c');
        mWOARNameToFieldAPIMappings.put(PT_AUTOMATION,strPT);
        
        setIdentifiers(mWOARNameToFieldAPIMappings);
    }  
    public void processWO(Map<Id, SObject> newItems) {
        List<WorkOrder> woListToProcess = new List<WorkOrder>();
        
        set<id> woIds = newItems.keySet();
        DescribeSObjectResult describeResult = Schema.getGlobalDescribe().get('WorkOrder').getDescribe();
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
        String query =' SELECT Account.Name,' +String.join( fieldNames, ',' ) +' FROM ' +describeResult.getName() +' WHERE ' +' id in :woIds ' ;
        woListToProcess =Database.query(query);
        for(WorkOrder woRecord : woListToProcess){
            if(woRecord.NBN_Field_Work_Specified_By_Id__c !=null && woRecord.NBN_Field_Work_Specified_By_Id__c.contains(Label.NBNNetwotkOptimizationSpecificationId)){
                List<ServiceContract> lstServiceContracts =[select Id,ContractNumber,Number_Of_Child_WO__c from serviceContract where name=:Label.NBNNetwotkOptimizationSCName];   
                if(lstServiceContracts!= null && lstServiceContracts.size()>0){
                    ServiceContract serObj =lstServiceContracts[0];
                    //woRecord.ServiceContractId = serObj.id;
                    woRecord.Service_Contract_Number__c = serObj.contractNumber;
                    
                }
            }
        }
        System.debug('SAYALI: woListToProcess--> '+woListToProcess);
        if(!woListToProcess.isEmpty()) {
            isWOUpdated = true;
        }
        setupWorkOrder(woListToProcess);
        
        System.debug('SAYALI: isWOUpdated-->'+isWOUpdated);
    }  
    public void processWoAfterUpdate(Map<Id, SObject> newItems,Map<Id, SObject> oldItems) {
        List<WorkOrder> lstWorkOrderToUpdate = new List<WorkOrder>();
        Map<Id,sObject> mWOChangedStatus = new Map<Id,sObject>();
        Map<Id,WorkOrder> mold = new Map<Id,WorkOrder>();
        //setupOldWOMap(oldItems);
        setupWorkOrderAfterUpdate(newItems.values(),oldItems);
        for(SObject obj : newItems.values()) {
            
            WorkOrder woObj = (WorkOrder)obj;
            WorkOrder woObjOld = (WorkOrder)oldItems.get(woObj.Id);
                
            if(woObj.ServiceTerritoryId!= woObjOld.ServiceTerritoryId){
                checkANDMapReferencesOnWO(woObj,PB_AUTOMATION);
                //lstWorkOrderToUpdate.add(woObj);
            }
            if(woObj.Client_Inbound_Action__c!=woObjOld.Client_Inbound_Action__c) {
                checkANDMapReferencesOnWO(woObj,SV_AUTOMATION);
                mWOChangedStatus.put(woObj.Id,woObj);
                //lstWorkOrderToUpdate.add(woObj);
            }
            if(woObj.SA_Status__c!=woObjOld.SA_Status__c) {
                checkANDMapReferencesOnWO(woObj,SM_AUTOMATION);
                mWOChangedStatus.put(woObj.Id,woObj);
                //lstWorkOrderToUpdate.add(woObj);
            }
                
                
                    
                
        }
        if(mWOChangedStatus!=null && mWOChangedStatus.size()>0){
            syncSAWithParentWorkOrder(mWOChangedStatus,oldItems);
        }
        
        
    }
    public void generateRelatedRecordsforChilds(Map<id,sObject> mIdtoWoWithChangesSR){
        
        Map<Id,List<WorkOrder>> mParentWoIdToChildWOLst = new Map<Id,List<WorkOrder>>();
        for(Id woId: mIdtoWoWithChangesSR.keySet())
        {
            WorkOrder woObj =(WorkOrder)mIdtoWoWithChangesSR.get(woId);
            List<WorkOrder> lstChildWo = new List<WorkOrder>();
            if(mParentWoIdToChildWOLst.get(woObj.ParentWorkOrderId)!=null){
                lstChildWo = mParentWoIdToChildWOLst.get(woObj.ParentWorkOrderId);
            }
            lstChildWo.add(woObj);
            mParentWoIdToChildWOLst.put(woObj.ParentWorkOrderId,lstChildWo);
        }
        if(mParentWoIdToChildWOLst!=null && mParentWoIdToChildWOLst.size()>0)
            copyRelatedRecordstoChild(mParentWoIdToChildWOLst);
        //isWoUpdated = getExtRednMethodsForServiceContract(mIdtoWoWithChangesSR);
        
    }
    public void syncChildWithParent(Map<id,WorkOrder> newItems, Map<Id,sObject> oldItems){
        WorkOrder woTempObj = newItems.values()[0];
        String actionMapping = woTempObj.Service_Contract_Number__c+' - Setup WO';
        List<PE_Subscription_Field_DTO_Mapping__mdt> fieldMappingList = [select Mapped_Object_Field_API_Name__c from PE_Subscription_Field_DTO_Mapping__mdt where Default_Value__c='' and PE_Subscription_Object_Action_Mapping__r.MasterLabel=:actionMapping and Active__c=true];
        List<String> fields = new List<String>();
        String soqlFields ='';
        for(PE_Subscription_Field_DTO_Mapping__mdt objMapping : fieldMappingList){
            fields.add(objMapping.Mapped_Object_Field_API_Name__c);
            soqlFields = soqlFields +','+objMapping.Mapped_Object_Field_API_Name__c;
            
        }
        //soqlFields = soqlFields.removeStart(',');
        Set<Id> idset = newItems.keyset();
        String childWoQuery = 'SELECT Id,ParentWorkOrderId  '+ soqlFields+ ' FROM WorkOrder WHERE parentWorkOrderId IN :idset';
        List<WorkOrder> lstWO =Database.query(childWoQuery);
        Map<Id,List<Workorder>> mparentToChildWoList = new Map<Id,List<Workorder>>();
        for(WorkOrder childWoObj : lstWO){
            List<Workorder> tempList = new List<WorkOrder>();
            if(mparentToChildWoList.get(childWoObj.ParentWorkOrderId)!=null){
                tempList = mparentToChildWoList.get(childWoObj.ParentWorkOrderId);
                
            }
            tempList.add(childWoObj);
            mparentToChildWoList.put(childWoObj.ParentWorkOrderId,tempList);
            
        }
        
        List<WorkOrder> lstChildWos = new List<WorkOrder>();
        for(WorkOrder wObj :newItems.Values()){
            WorkOrder oldObj = (WorkOrder)oldItems.get(wObj.Id);
            
            for(WorkOrder objChildWo : mparentToChildWoList.get(wObj.Id)){
            	for(String field: fields){
                  	if(wObj.get(field)!= oldObj.get(field))
                    {
                        objChildWo.put(field,wObj.get(field));
                        
                    }
                }
                lstChildWos.add(objChildWo);
            }
            
        }
        if(lstChildWos!=null && lstChildWos.size()>0){
            Update lstChildWos;
        }
    }
    public void setPRDnPCDDatesOnWO(Map<id,sObject> newItems){
        List<Workorder> lstChildWos =[select id,planned_remediation_date__c,Planned_Start_Date__c,ParentWorkOrderId from workorder where ParentWorkOrderId in:newItems.keySet()];
        List<WorkOrder> updatedchildWoList = new List<WorkOrder>();
        Map<Id,List<WorkOrder>> mparenttoChildWos = new Map<Id,List<WorkOrder>>();
        for(WorkOrder objWO : lstChildWos){
            List<WorkOrder> tempLst;
            if(mparenttoChildWos.keyset().contains(objWO.ParentWorkOrderId)){
                tempLst = mparenttoChildWos.get(objWO.ParentWorkOrderId);
            }
            else{
                tempLst = new List<WorkOrder>();
            }
            tempLst.add(objWO);
            mparenttoChildWos.put(objWO.ParentWorkOrderId,tempLst);
        }
        
        Map<String,Set<Date>> mStateToHoliday = New Map<String,Set<Date>>();
        List<Holiday> holidays = [SELECT activityDate,Description FROM Holiday where Description!=''];
        for(Holiday record : holidays){
            
            List<String> lstStates = record.Description.Split(',');
            for(String state:lstStates){
                Set<Date> temp = new Set<Date>();
                if(mStateToHoliday.keySet().contains(state)){
                    temp = mStateToHoliday.get(state);
                    
                }
               	temp.add(record.ActivityDate);
                mStateToHoliday.put(state,temp);
            }
            //holidaysMap.put(record.activitydate, record.activitydate);
        }
        String opDem ='OPERATE \\ DEMAND INSTALL';
        
        String opServRest = 'OPERATE \\ SERVICE RESTORATION';
        Date prdDate;
        Date pcdDate;
        for(sObject Obj : newItems.values()){
            WorkOrder woObj =(WorkOrder)Obj;
            System.debug('SAYALI DEBUG PCDPCR Classification__c'+woObj.Classification__c);
            System.debug('SAYALI DEBUG PCDPCR  woObj.NBN_Reference_ID__c'+woObj.NBN_Reference_ID__c);
            System.debug('SAYALI DEBUG PCDPCR  woObj.Reference_ID_Tech__c'+woObj.Reference_ID_Tech__c);
            if((woObj.Classification__c!=null && woObj.Classification__c.startsWithIgnoreCase(opDem)) || woObj.NBN_Field_Work_Specified_By_Id__c=='GreenfieldPreInstallWOS'||  woObj.NBN_Field_Work_Specified_By_Id__c=='GeneralWOS'){
                prdDate= System.today()+45;
                Set<Date> holidaySet = mStateToHoliday.get(woObj.state);
                System.debug('SAYALI DEBUG PCDPCR holidaySet'+holidaySet);
                Integer offDaysCount = getOffDaysWithinTat(holidaySet,0,1,prdDate);
                woObj.planned_remediation_date__c = prdDate-offDaysCount-1;
                //woObj.Planned_End_Date__c = woObj.planned_remediation_date__c;
                woObj.EndDate = woObj.planned_remediation_date__c;
                //woObj.StartDate = 
            	System.debug('SAYALI DEBUG PCDPCR planned_remediation_date__c = '+woObj.planned_remediation_date__c);
            }
            if(woObj.Classification__c!=null && woObj.Classification__c.startsWithIgnoreCase(opServRest) &&  ((woObj.NBN_Reference_ID__c!=null && woObj.NBN_Reference_ID__c.isNumeric())||(woObj.Reference_ID_Tech__c!=null && woObj.Reference_ID_Tech__c.isNumeric()))){
                pcdDate= System.today()+45;
                Set<Date> holidaySet = mStateToHoliday.get(woObj.state);
                System.debug('SAYALI DEBUG PCDPCR holidaySet'+holidaySet);
                Integer offDaysCount = getOffDaysWithinTat(holidaySet,0,1,pcdDate);
                woObj.Planned_Completion_Date__c =pcdDate-offDaysCount-1;
                woObj.planned_remediation_date__c =pcdDate-offDaysCount-1;
                //woObj.Planned_End_Date__c = woObj.Planned_Completion_Date__c;
                woObj.EndDate = woObj.Planned_Completion_Date__c;
            }
            else if(woObj.Classification__c!=null && woObj.Classification__c.startsWithIgnoreCase(opServRest) &&  ((woObj.NBN_Reference_ID__c!=null && !woObj.NBN_Reference_ID__c.isNumeric())||(woObj.Reference_ID_Tech__c!=null && !woObj.Reference_ID_Tech__c.isNumeric()))){
                pcdDate= System.today()+14;
                Set<Date> holidaySet = mStateToHoliday.get(woObj.state);
                System.debug('SAYALI DEBUG PCDPCR holidaySet'+holidaySet);
                Integer offDaysCount = getOffDaysWithinTat(holidaySet,0,1,pcdDate);
                System.debug('offDaysCount Final=='+offDaysCount);
                woObj.Planned_Completion_Date__c =pcdDate-offDaysCount-1;
                woObj.planned_remediation_date__c =pcdDate-offDaysCount-1;
                //woObj.Planned_End_Date__c = woObj.Planned_Completion_Date__c;
                woObj.EndDate = woObj.Planned_Completion_Date__c;
                System.debug('SAYALI DEBUG PCDPCR Planned_Completion_Date__c = '+woObj.Planned_Completion_Date__c);
            }
            /*if(mparenttoChildWos.get(woObj.Id)!=null){
                for(WorkOrder childWoObj : mparenttoChildWos.get(woObj.Id)){
                    childWoObj.planned_remediation_date__c = woObj.planned_remediation_date__c;
                    childWoObj.Planned_Start_Date__c  =woObj.Planned_Start_Date__c ;
                    updatedchildWoList.add(childWoObj);
                }
            }*/
        }
        if(updatedchildWoList!=null && updatedchildWoList.size()>0){
            //update updatedchildWoList;
        }
    }
    private static Integer getOffDaysWithinTat(Set<Date> holidaysSet, Integer lastCount, Integer tatIterator, Datetime startDate){
        Integer inc = 0;
        for(Integer i=1;i<=tatIterator;i++){
            if(holidaysSet.contains((startDate.date()-i)) || startDate.addHours(-(i*24)).format('EEE') == 'Sun' || startDate.addHours(-(i*24)).format('EEE') == 'Sat'){
                inc = inc+1;                    
            }
        }
        Integer offDaysCount = inc;
        System.debug('offDaysCount=='+offDaysCount);
        if(inc != lastCount && startDate.date() >= System.today()){
            offDaysCount= getOffDaysWithinTat(holidaysSet, inc, (1+inc), startDate);
        }
          
        	return offDaysCount;  
        
    }
    public void cloneWO(Map<Id, SObject> newItems) {
        Set<Id> sourceWoIds = new Set<Id>();
        Map<Id,Id> mSourceVsClonedId = New Map<Id,Id>();
        List<WorkOrderLineItem> woliListToCreate = new List<WorkOrderLineItem>(); 
        for(SObject obj : newItems.values()) {
            
            WorkOrder woObj = (WorkOrder)obj;
            //System.debug('AKASH-->NBNNA-->Line 308 called. woObj.Client_Work_Order_Number__c : '+woObj.Client_Work_Order_Number__c);
            //System.debug('AKASH-->NBNNA-->Line 309 called. woObj.ParentWorkOrderId : '+woObj.ParentWorkOrderId);
            Id srcId =woObj.getCloneSourceId();
            sourceWoIds.add(srcId);
            mSourceVsClonedId.put(srcId,woObj.Id);
        }
        if(sourceWoIds!=null){
            List<WorkOrderLineItem> lstWoli =[select Type__c,Description,WOrkOrderId,WorkTypeId,Required_Pre_Finish__c,Required_Pre_Notification__c,RecordTypeId from WorkOrderLineItem where WorkOrderId in :sourceWoIds];
            for(WorkOrderLineItem woliObj : lstWoli){
                WorkOrderLineItem woli = new WorkOrderLineItem();
                woli.Type__c = woliObj.Type__c;
                woli.Description = woliObj.Description;
                woli.WOrkOrderId = mSourceVsClonedId.get(woliObj.WOrkOrderId);
                woli.WorkTypeId = woliObj.WorkTypeId;
                woli.Required_Pre_Finish__c = woliObj.Required_Pre_Finish__c;
                woli.Required_Pre_Notification__c = woliObj.Required_Pre_Notification__c;
                woli.RecordTypeId = woliObj.RecordTypeId;
                
               
                woliListToCreate.add(woli);
            }
            if(woliListToCreate!=null){
                Insert woliListToCreate;
            }
            //System.debug('AKASH-->NBNNA-->Line 330 called, SourcewoIDs:'+sourceWoIds);
            Map<String,Schema.DisplayType> mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Work_Order_Task__c');
            String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
            String woTaskQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Work_Order_Task__c WHERE Status__c!=\'Complete\' and Work_Order__c IN :sourceWoIds';
            Map<String,Work_Order_Task__c> mExternalIdToWoTask = new Map<String,Work_Order_Task__c>();
            List<Work_Order_Task__c> lstWoTask =Database.query(woTaskQuery);
            Set<Id> woTaskIds = new Set<Id>();
            
            for(Work_Order_Task__c objWotask :lstWoTask){
                //System.debug('AKASH-->NBNNA-->Line 339 called. lstWoTask'+lstWoTask);
                woTaskIds.add(objWotask.Id);
                Id newWoId = mSourceVsClonedId.get(objWotask.Work_Order__c);
                Work_Order_Task__c childWoTask = objWotask.clone(false, false, false, false); 
                childWoTask.Work_Order__c = newWoId;
                String[] clienttaskIds = childWoTask.Client_Task_Id__c.Split(':');
                childWoTask.Client_Task_Id__c =clienttaskIds[0] +':'+(String)newWoId;
                //Rohan FR-279 : To distinguish if WOT is created from apex code.
                childWoTask.isBatch__c = true;
                mExternalIdToWoTask.put(objWotask.Id,childWoTask);
                //lstChildWoTaks.add(childWoTask);
            }
            if(mExternalIdToWoTask!=null && mExternalIdToWoTask.size()>0){
            	insert mExternalIdToWoTask.values();
        	}
            mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('Custom_Attribute__c');
            fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
            String woCustomAttQuery = 'SELECT '+ fieldsForSOQL+ ' FROM Custom_Attribute__c WHERE Work_Order_Task__c IN :woTaskIds';
        
            List<Custom_Attribute__c> lstCustomAtt =Database.query(woCustomAttQuery);
            List<Custom_Attribute__c> lstCustomAttToInsert =Database.query(woCustomAttQuery);
            for(Custom_Attribute__c objAtt :lstCustomAtt){
                
                Custom_Attribute__c childWoAtt = objAtt.clone(false, false, false, false); 
                childWoAtt.Id=null;
                childWoAtt.Work_Order_Task__c =mExternalIdToWoTask.get(objAtt.Work_Order_Task__c).id;
                String[] extIds = objAtt.CA_External_Id__c.Split(':');
                childWoAtt.CA_External_Id__c =extIds[0] +':'+(String)mExternalIdToWoTask.get(objAtt.Work_Order_Task__c).Work_Order__c;
                lstCustomAttToInsert.add(childWoAtt);
                
            }
            if(lstCustomAttToInsert!=null && lstCustomAttToInsert.size()>0){
                upsert lstCustomAttToInsert;
            }
            
        }
    }
    public void populatePRDonClonedWO(Map<Id, SObject> newItems) {
        Set<Id> sourceWoIds = new Set<Id>();
        Set<Id> woIds = new Set<Id>();
        Map<Id,WorkOrder> mSourceVsClonedId = New Map<Id,WorkOrder>();
        List<WorkOrder> lstWOToBeUpdated = New List<WorkOrder>();
        List<WorkOrderLineItem> woliListToCreate = new List<WorkOrderLineItem>(); 
        for(SObject obj : newItems.values()) {
            
            WorkOrder woObj = (WorkOrder)obj;
            woIds.add(woObj.Id);
            Id srcId =woObj.getCloneSourceId();
            sourceWoIds.add(srcId);
            mSourceVsClonedId.put(srcId,woObj);
        }
        if(sourceWoIds!=null){
            List<WorkOrder> lstSrcWOs =[select id,Planned_remediation_Date__c,eot_cause__c,eot_reason__c,Delivery_Confidence_Level__c from workorder where id in:sourceWoIds];
            
            for(WorkOrder srcWO:lstSrcWOs){
                WorkOrder woObj = mSourceVsClonedId.get(srcWO.Id);
                woObj.planned_remediation_date__c = srcWO.planned_remediation_date__c;
                
            }
            
        }  
    }
    /*public void setServiceContract(WorkOrder woRecord) {
        System.debug('SAYALI : setServiceContract from NBN');
        if(woRecord.NBN_Field_Work_Specified_By_Id__c !=null && woRecord.NBN_Field_Work_Specified_By_Id__c== 'NetworkAssuranceWOS'){
            woRecord.ServiceContractId = mServiceContractNumberToSRObjMap.get(woRecord.Service_Contract_Number__c).Id;
        }
        else if(woRecord.NBN_Field_Work_Specified_By_Id__c !=null && woRecord.NBN_Field_Work_Specified_By_Id__c== 'NetworkOptimizationWOS'){
            List<ServiceContract> lstServiceContracts =[select Id,ContractNumber,Number_Of_Child_WO__c from serviceContract where name=:'NBN Network Optimization'];   
                if(lstServiceContracts!= null && lstServiceContracts.size()>0){
                    ServiceContract serObj =lstServiceContracts[0];
                    woRecord.ServiceContractId = serObj.id;
                    woRecord.Service_Contract_Number__c = serObj.contractNumber;
                    List<WorkOrder> lstWO = mServiceContractNumberToWOListMap.get(woRecord.Service_Contract_Number__c);
                    if(lstWO!=null && lstWO.size()>0){
                        lstWO.add(woRecord);
                    }
                    else{
                        lstWO = new List<WorkOrder>{woRecord};
                    }
                        
                    mServiceContractNumberToWOListMap.put(serObj.contractNumber,lstWO);
                    mServiceContractNumberToSRObjMap.put(serObj.ContractNumber,serObj);
                }
        }

    }*/
}