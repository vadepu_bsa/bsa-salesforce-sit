/* Author: Abhijeet Anand
* Date: 2 December, 2019
* Handler class containing the business logic around the automation for post insert of NBN Log Entry records
*/

public class LogEntryBusinessLogic {
    
    //Create Log Entries On Work Order when triggerd by NBN NA/ Create Submit Notes on Work Order in BSA
    public static void logEntryFromBSAToNBN(List<NBN_Log_Entry__c> newList) {
        
        System.debug('Inside After Insert Method');
        Boolean logEntryResponse = false;
        
        for(NBN_Log_Entry__c obj : [SELECT Id, Log_Entry_Direction__c,Work_Order__r.Based_Revision_Number__c, Work_Order__r.NBN_Activity_Start_Date_Time__c,Work_Order__r.Client_Work_Order_Number__c,Work_Order__r.NBN_Field_Work_Specified_By_Category__c,Work_Order__r.NBN_Activity_State_Id__c,Work_Order__r.NBN_Current_Field_Work_Order_Status__c,Action_Type__c, Work_Order__c, Work_Order__r.NBN_Parent_Work_Order_Id__c,Submit_Notes_Additional_Info__c,Work_Order__r.Primary_Access_Technology__c, Work_Order__r.NBN_Field_Work_Specified_By_Id__c,Work_Order__r.NBN_Field_Work_Specified_By_Type__c, Work_Order__r.NBN_Field_Work_Specified_By_Version__c, Work_Order__r.NBN_Field_Work_Note_Description__c, Work_Order__r.NBN_Field_Work_Instructions__c, Work_Order__r.WorkType.Name FROM NBN_Log_Entry__c WHERE Id IN : newList]){
            //NBN_Log_Entry__c logEntry = (NBN_Log_Entry__c)obj;
            List<NBN_Log_Entry__c> listToGenerateJSON = new List<NBN_Log_Entry__c>();
            listToGenerateJSON.add(obj);
            
            String jsonPayload = ''; //String to build a JSON payload
            String msg = ''; //String to write a successful/failure message
            //Pass the sobject list to JSON generator apex class to build a JSON string
            jsonPayload = JSON.serializePretty(listToGenerateJSON);
            
            if(obj.Action_Type__c == 'Submit Notes') {
                msg = 'Submit Notes Action BSA to NBN'; 
            }
            else if(obj.Action_Type__c == 'Log Entry') {
                msg = 'Log Entry Created in Salesforce';
            }
            
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            if(obj.Action_Type__c == 'Log Entry' && logEntryResponse == false) {
                logEntryResponse = true;
                ApplicationLogUtility.addException(obj.Work_Order__c, jsonPayload, msg, 'Info', 'WorkOrder');
            }
            
            if(obj.Action_Type__c.equalsIgnoreCase(BSA_ConstantsUtility.SUBMIT_NOTES)) {
                invokeSubmitNotesPlatformEvent(obj,msg);
            }
            
        }
        
        //Insert the application log entry in the SF database
        ApplicationLogUtility.saveExceptionLog(); 
        
    }
    
    
    private static void invokeSubmitNotesPlatformEvent(NBN_Log_Entry__c logentry, String message){
        
        List<sobject> platformEventsToPublish = new List<sobject>();
        Action_Submit_Notes_BSA_to_NBN__e event = new Action_Submit_Notes_BSA_to_NBN__e();
        event.NBN_Field_Work_Specified_By_Id__c = logentry.Work_Order__r.NBN_Field_Work_Specified_By_Id__c;
        event.NBN_Field_Work_Specified_By_Version__c = logentry.Work_Order__r.NBN_Field_Work_Specified_By_Version__c;
        event.NBN_Field_Work_Specified_By_Type__c = logentry.Work_Order__r.NBN_Field_Work_Specified_By_Type__c;
        event.NBN_Field_Work_Specified_By_Category__c = logentry.Work_Order__r.NBN_Field_Work_Specified_By_Category__c;
        
        try{
            
            event.Revision_Number__c =String.valueOf((Integer.valueOf(logentry.Work_Order__r.Based_Revision_Number__c)+1));    
        }
        catch(TypeException excp){
            event.Revision_Number__c = '0';
        }
        event.Work_Order_Status__c = logentry.Work_Order__r.NBN_Current_Field_Work_Order_Status__c;
        event.NBN_Activity_Status_Info_ID__c = logentry.Work_Order__r.Client_Work_Order_Number__c;
        event.NBN_Activity_State_Id__c = logentry.Work_Order__r.NBN_Activity_State_Id__c.contains(':') ? logentry.Work_Order__r.NBN_Activity_State_Id__c.SubStringBefore(':') : logentry.Work_Order__r.NBN_Activity_State_Id__c ;
        event.NBN_Activity_State_Date_Time__c = DateTime.valueOfGMT(String.valueOf(logentry.Work_Order__r.NBN_Activity_Start_Date_Time__c));
        event.NBN_Activity_Instantiated_By__c = logentry.Work_Order__r.WorkType.Name;
        event.Notes__c = logentry.Submit_Notes_Additional_Info__c;
        event.CorrelationID__c = UtilityClass.getUUID();
        event.NBN_Field_Work_ID__c = logentry.Work_Order__r.NBN_Parent_Work_Order_Id__c;
        event.NBN_Related_ID__c = logentry.Work_Order__r.NBN_Parent_Work_Order_Id__c;
        event.NBN_Related_Sub_Id__c = logentry.Work_Order__r.Id;  
        
        /*

event.NBN_Related_ID__c = logentry.Work_Order__r.NBN_Parent_Work_Order_Id__c;
event.NBN_Related_Sub_Id__c = logentry.Work_Order__r.Id;
event.NBN_Occurs_Within_Activity_ID__c = logentry.Work_Order__r.Client_Work_Order_Number__c;        
//event.NBN_Activity_State_Date_Time__c = obj.NBN_Activity_Start_Date_Time__c;
event.NBN_Action_Date__c = logentry.Work_Order__r.LastModifiedDate;
// event.Action__c = logentry.Work_Order__r.NBN_Action__c;*/
        System.debug('Submit notes event'+event);
        platformEventsToPublish.add(event);
        System.debug('platformEventsToPublish to publish'+platformEventsToPublish); 
        
        ApplicationLogUtility.addException(logentry.Id, JSON.serializePretty(platformEventsToPublish), message, 'Info', 'NBN_Log_Entry__c');  
        
        if(!platformEventsToPublish.isEmpty()){
            
             ApplicationLogUtility.publishEvents(platformEventsToPublish);
        }
    }
    
}