@isTest
public class ProntoStockReplenishmentAutomationTest {
    static testMethod void testValidEvent() {
        
        Test.startTest();
        
        // Create a test location record
        Schema.Location locationRecord = new Schema.Location();
        locationRecord.name = 'Testing Location';
        locationRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        locationRecord.Description = 'Testing Location';
        locationRecord.Van_Id__c = 'van007';
        locationRecord.LocationType = 'Warehouse';
        locationRecord.IsMobile = true;
        locationRecord.IsInventoryLocation = true;
        locationRecord.Territory__c = 'NSW';
        locationRecord.Correlation_ID__c = '88a57e4e-b22b-4683';
        locationRecord.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        insert locationRecord;
        
        // Create a test location record
        Schema.Location locationRecord1 = new Schema.Location();
        locationRecord1.name = 'Testing Location';
        locationRecord1.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78969';
        locationRecord1.Description = 'Testing Location One';
        locationRecord1.Van_Id__c = 'van008';
        locationRecord1.LocationType = 'Warehouse';
        locationRecord1.IsMobile = true;
        locationRecord1.IsInventoryLocation = true;
        locationRecord1.Territory__c = 'NSW';
        locationRecord1.Correlation_ID__c = '88a57e4e-b22b-4686';
        locationRecord1.Event_TimeStamp__c = '2019-04-03T00:35:11.000Z';
        
        insert locationRecord1;
        
        // Create a test product record
        Product2 productRecord = new Product2();
        productRecord.Name = 'Test Product Name';
        productRecord.Product_Code__c = 'prod007';
        productRecord.Client_ID__c = 'cl007';
        productRecord.Is_Serialized__c = true;
        
        insert productRecord;
        
        Product2 productRecord1 = new Product2();
        productRecord1.Name = 'Test Product Name1';
        productRecord1.Product_Code__c = 'prod008';
        productRecord1.Client_ID__c = 'cl007';
        productRecord1.Is_Serialized__c = false;
        
        insert productRecord1;
        
        // Create a test productitem record
        productitem productItemRecord = new productitem();
        productItemRecord.Correlation_ID__c = locationRecord.Correlation_ID__c;
        productItemRecord.Product2Id = productRecord.Id;
        productItemRecord.QuantityOnHand = 10;
        productItemRecord.LocationId = locationRecord.id;
        productItemRecord.Pronto_Serial_Number__c = 'sl007';
        productItemRecord.SerialNumber = 'sl007';
        productItemRecord.Is_Serialized__c = true;
        
        insert productItemRecord;
        
        
        productitem productItemRecord1 = new productitem();
        productItemRecord1.Correlation_ID__c = locationRecord.Correlation_ID__c;
        productItemRecord1.Product2Id = productRecord.Id;
        productItemRecord1.QuantityOnHand = 10;
        productItemRecord1.LocationId = locationRecord.id;
        productItemRecord1.Pronto_Serial_Number__c = 'sl008';
        productItemRecord.SerialNumber =  'sl008';
        productItemRecord1.Is_Serialized__c = true;
        
        insert productItemRecord1;
        
        productitem productItemRecord2 = new productitem();
        productItemRecord2.Correlation_ID__c = locationRecord.Correlation_ID__c;
        productItemRecord2.Product2Id = productRecord1.Id;
        productItemRecord2.QuantityOnHand = 10;
        productItemRecord2.LocationId = locationRecord1.id;
        // productItemRecord2.Pronto_Serial_Number__c = 'sl008';
        // productItemRecord2.SerialNumber =  'sl008';
        //  productItemRecord2.Is_Serialized__c = true;
        
        insert productItemRecord2;
        
        List<Pronto_Van_Warehouse_Replenished_Updated__e> eventList = new List<Pronto_Van_Warehouse_Replenished_Updated__e>();
        // Create a test event instance
        Pronto_Van_Warehouse_Replenished_Updated__e syncEvent = new Pronto_Van_Warehouse_Replenished_Updated__e();
        syncEvent.Warehouse_Code__c = 'van007';
        syncEvent.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        syncEvent.Serial_Numbers__c = 'sl007^sl008';
        syncEvent.Stock_Code__c = 'prod007';  
        syncEvent.Quantity_On_Hand__c = 8;
        syncEvent.Is_Serialized__c = 'Y';
        eventList.add(syncEvent);
        
        
        Pronto_Van_Warehouse_Replenished_Updated__e syncEvent1 = new Pronto_Van_Warehouse_Replenished_Updated__e();
        syncEvent1.Warehouse_Code__c = 'van008';
        syncEvent1.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent1.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';        
        syncEvent1.Stock_Code__c = 'prod008';  
        syncEvent1.Quantity_On_Hand__c =8;
        syncEvent1.Is_Serialized__c = 'N';
        eventList.add(syncEvent1);
        
        Pronto_Van_Warehouse_Replenished_Updated__e syncEvent2 = new Pronto_Van_Warehouse_Replenished_Updated__e();
        syncEvent2.Warehouse_Code__c = 'van007';
        syncEvent2.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent2.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';        
        syncEvent2.Stock_Code__c = 'prod007';  
        syncEvent2.Serial_Numbers__c='ABC^AWR';
        syncEvent2.Is_Serialized__c = 'Y';
        syncEvent2.Quantity_On_Hand__c =1;
        eventList.add(syncEvent2);
        
        // Publish test event
        Database.SaveResult[] srs = EventBus.publish(eventList);
        
        Test.stopTest();
        
        for(Database.SaveResult sr : srs) {
            System.assertEquals(true, sr.isSuccess());
        }

        // This will fire the associated event trigger.
        Test.getEventBus().deliver(); 
        
        // TODO
        //System.assertEquals(1, locations.size());
        
    }        
}