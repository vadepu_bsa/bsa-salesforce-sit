@isTest
public class ServiceResourceAutoBusinessLogicTest {
   /*
    static testMethod void testValidateSrLinkwithLocationInsert(){
        
        Test.startTest();
        // Create a test location record
        Schema.Location locationRecord = new Schema.Location();
        locationRecord.name = 'Testing Service Automation';
        locationRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78963';
        locationRecord.Description = 'Testing Service Automation';
        locationRecord.Van_Id__c = 'test0007';
        locationRecord.LocationType = 'Warehouse';
        locationRecord.IsMobile = true;
        locationRecord.IsInventoryLocation = true;
        locationRecord.Territory__c = 'NSW';
		locationRecord.Correlation_ID__c = '88a57e4e-b22b-4683';
		locationRecord.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        
        // Create a test user record
       	Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Field Technician' LIMIT 1];
        User userRec = new User( email='test.user@gmail.com',
                            profileid = profileDetails.Id, 
                            UserName='test.user@gmail.com', 
                            Alias = 'GDS',
                            TimeZoneSidKey='America/New_York',
                            EmailEncodingKey='ISO-8859-1',
                            LocaleSidKey='en_US', 
                            LanguageLocaleKey='en_US',
                            PortalRole = 'Manager',
                            FirstName = 'Test',
                            LastName = 'User');
        insert userRec;
        
        // Create a test Service Resource record
        ServiceResource serviceResourceRecord = new ServiceResource();
        serviceResourceRecord.Name = 'Test Service User';
        serviceResourceRecord.User_Id__c = userRec.Id;
        serviceResourceRecord.RelatedRecordId = userRec.Id;
        serviceResourceRecord.IsActive = true;
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78963';
              //serviceResourceRecord.LocationId=locationRecord.id;
        Database.SaveResult srServiceResource = database.insert(serviceResourceRecord);
        
        insert locationRecord;
        
        Test.stopTest();
                
        // Verify SaveResult value
        System.assertEquals(true, srServiceResource.isSuccess());
        
        // Verify that a service resource with matching pronto service resource id found.
        List<Schema.Location> locations = [SELECT Id FROM Location where Pronto_Service_Resource_ID__c=:serviceResourceRecord.Pronto_Service_Resource_ID__c limit 1];
        // Validate that the service resource found.
        System.assertEquals(1, locations.size());
    }
    static testMethod void testValidateSrLinkwithLocationUpdate(){
        
        Test.startTest();
        // Create a test location record
        Schema.Location locationRecord = new Schema.Location();
        locationRecord.name = 'Testing Service Automation';
        locationRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78963';
        locationRecord.Description = 'Testing Service Automation';
        locationRecord.Van_Id__c = 'test0007';
        locationRecord.LocationType = 'Warehouse';
        locationRecord.IsMobile = true;
        locationRecord.IsInventoryLocation = true;
        locationRecord.Territory__c = 'NSW';
		locationRecord.Correlation_ID__c = '88a57e4e-b22b-4683';
		locationRecord.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        insert locationRecord;
        
        // Create a test user record
       	Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Field Technician' LIMIT 1];
        User userRec = new User( email='test.user@gmail.com',
                            profileid = profileDetails.Id, 
                            UserName='test.user@gmail.com', 
                            Alias = 'GDS',
                            TimeZoneSidKey='America/New_York',
                            EmailEncodingKey='ISO-8859-1',
                            LocaleSidKey='en_US', 
                            LanguageLocaleKey='en_US',
                            PortalRole = 'Manager',
                            FirstName = 'Test',
                            LastName = 'User');
        insert userRec;
        
        // Create a test Service Resource record
        ServiceResource serviceResourceRecord = new ServiceResource();
        serviceResourceRecord.Name = 'Test Service User';
        serviceResourceRecord.User_Id__c = userRec.Id;
        serviceResourceRecord.RelatedRecordId = userRec.Id;
        serviceResourceRecord.IsActive = true;
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'changed-srid-88a57e4e-b22b-78963';
        
        Database.SaveResult srServiceResource = database.insert(serviceResourceRecord);
        
		// Update the pronto service resource id as same as location inserted above.
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78963';
		update serviceResourceRecord;
            
        Test.stopTest();
                
        // Verify SaveResult value
        System.assertEquals(true, srServiceResource.isSuccess());
        
        // Verify that a service resource with matching pronto service resource id found.
        List<Schema.Location> locations = [SELECT Id FROM Location where Pronto_Service_Resource_ID__c=:serviceResourceRecord.Pronto_Service_Resource_ID__c limit 1];
        // Validate that the service resource found.
        System.assertEquals(1, locations.size());
    }*/
}