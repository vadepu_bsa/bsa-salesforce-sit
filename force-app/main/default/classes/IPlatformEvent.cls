/**
* @author          Karan Shekhar
* @date            20/Jan/2020	
* @description    
* Change Date    Modified by         Description   
* 20/Jan/2020    Karan Shekhar		Platform processing Interface. Implement this interface for publishing PE via APEX.Base Handler support added as well PlatformEventBaseHandler 
**/
public interface IPlatformEvent {
    
    //void shouldPublishPlatformEvent(Boolean publishPE);
    void mapPlatformEvent(List<sObject> recordsToMaps); 
    void publishEvents();  
    void processPlatformEvent(); 

     
}