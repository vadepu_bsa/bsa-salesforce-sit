/*
*  @description apex controller for DynamicFormComponent.cmp
*
*  @author      Mahmood Zubair
*  @date        2020-06-19
*/
public without sharing class DynamicFormComponentController {
    
    /*
    *  @description queries for fields to display based off JIRA @FSL3-35
    *
    *  @author      Mahmood Zubair
    *  @date        2020-06-19
    */
        @AuraEnabled
        public static List<String> getFieldsToDisplay(Id recordId, string sObjectName) {
            
            List<String> fieldsToDisplay = new List<String>();
            string developername = null;
            string fieldOneName = '';
            string fieldOneValue = null;
            string fieldTwoValue = null; 
            string fieldTwoName = '';
            
            String recordQuery = 'SELECT Id, RecordTypeId FROM ' + sobjectName +' WHERE Id=:recordId';
            sobject s = Database.query(recordQuery);
            // system.debug('!!! sobject =' + s);
            Id rID = (Id) s.get('RecordTypeId');
            // system.debug('!! recordType Id' + rId);
            developername = [Select DeveloperName from RecordType Where Id =:rID LIMIT 1].DeveloperName;
            
            List<Dynamic_Field_Form__mdt> dFFObjectList = 
                [ SELECT Field_List__c, Field_2_Value__c,Field_2_Name__c,Field_1_Value__c,Field_1_Name__c, Object_Name__c, Record_Type_Developer_Name__c
                 FROM Dynamic_Field_Form__mdt
                 WHERE Object_Name__c =: sObjectName AND Record_Type_Developer_Name__c =:developername ];
            // system.debug('dFFObjectList = ' + dFFObjectList);
            // system.debug('sObjectName = ' + sObjectName);
            // system.debug('developername = ' + developername);

            if (!dFFObjectList.isEmpty()){ //David Azzi - Alteration
                if ( dFFObjectList[0].Field_1_Name__c != null || dFFObjectList[0].Field_2_Name__c !=null){
                    fieldOneName = ','+dFFObjectList[0].Field_1_Name__c;
                    if (dFFObjectList[0].Field_2_Name__c !=null) {
                        fieldTwoName = ','+dFFObjectList[0].Field_2_Name__c;
                    }
                }
                
                String record2Query = 'SELECT Id '+fieldOneName +' '+ fieldTwoName+  ' FROM ' + sobjectName +' WHERE Id=:recordId';
                // system.debug('!!! record2Query ' + record2Query);
                sobject s2 = Database.query(record2Query);
                // system.debug('!!!! s2 = ' + s2);
                fieldOneValue = (string)s2.get(fieldOneName.replace(',',''));
                if (fieldTwoName != ''){
                    fieldtwoValue = (string)s2.get(fieldTwoName.replace(',',''));
                }
                string fieldList = null;
                for (Integer i = 0 ; i < dFFObjectList.size(); i++){
                    
                    if (dFFObjectList[i].Field_1_Value__c == fieldOneValue ){
                        fieldList =dFFObjectList[i].Field_List__c;
                    }
                }
                // system.debug('!!!!! fieldList' + fieldList);
                if ( fieldList != null ){
                    fieldsToDisplay = fieldList.split(';');
                }
                return new List<String>(fieldsToDisplay);
            } 
            
            return fieldsToDisplay;
        }
    
        //IBM Dev - David Azzi
        //Get the full icon name, if custom, make custom, if standard get standard icon for that object
        @AuraEnabled
        public static String getIconNameStr(String sObjectName){
            String output = 'standard:asset_object'; //default is asset standard object

            // System.debug('sObjectName::' + sObjectName);
            List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
            List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
            List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();
        
            for(Schema.DescribeTabSetResult tsr : tabSetDesc) { tabDesc.addAll(tsr.getTabs()); }
        
            for(Schema.DescribeTabResult tr : tabDesc) {
                if( sObjectName == tr.getSobjectName() ) {
                    
                    if( tr.isCustom() == true ) {
                        iconDesc.addAll(tr.getIcons());
                    } else {
                        output = 'standard:' + sObjectName.toLowerCase();
                    }
                }
            }
            
            for (Schema.DescribeIconResult ir : iconDesc) {
                if (ir.getContentType() == 'image/svg+xml'){
                    output = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                    break;
                }
            }

            // system.debug('!!! Icon Output:  ' + output);
            return output;
        }
    }