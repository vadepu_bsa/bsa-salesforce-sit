/**
 * @author          Sunila.M
 * @date            18/May/2019	
 * @description     Trigger Handler added fo rthe Work Order trigger
 *
 * Change Date    Modified by         Description  
 * 18/May/2019		Sunila.M		Trigger handler for AssignedResource before delete/update
  **/
public class AssignedResourceTriggerHandler implements ITriggerHandler{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
       
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        
        AssignedResourceWOSharingHelper.deleteOldTechWOSharingRecord(true, newItems, oldItems);
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        AssignedResourceWOSharingHelper.deleteOldTechWOSharingRecord(false, null, oldItems);
    } 
    
    public void BeforeInsert(List<SObject> assignedResourceList) {} public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}public void AfterInsert(Map<Id, SObject> newItems) {}
    
    // 09/Sep/2020  Bohao Chen @IBM JIRA FSL3-550 upddate service resource on woli tasks if the assigned resource updated on SA
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

        Map<Id, Id> newServiceResourceIdByServiceAppointmentId = new Map<Id, Id>();

        for(Id objId : newItems.keySet()) {

            AssignedResource newAssignedResource = (AssignedResource)newItems.get(objId);
            AssignedResource oldAssignedResource = (AssignedResource)oldItems.get(objId);

            if(oldAssignedResource.ServiceResourceId != newAssignedResource.ServiceResourceId) {
                newServiceResourceIdByServiceAppointmentId.put(newAssignedResource.ServiceAppointmentId, newAssignedResource.ServiceResourceId);
            }
        }

        List<WorkOrderLineItem> wolisWtChangedAssignedResource = new List<WorkOrderLineItem>();

        // System.debug('@newServiceResourceIdByServiceAppointmentId: ' + newServiceResourceIdByServiceAppointmentId);

        if(!newServiceResourceIdByServiceAppointmentId.isEmpty()) {
            for(ServiceAppointment sa : [SELECT Id,
                                            (SELECT Id, Service_Resource__c FROM Work_Order_Line_Items__r)
                                        FROM ServiceAppointment 
                                        WHERE Id IN: newServiceResourceIdByServiceAppointmentId.keySet()]) {
                for(WorkOrderLineItem woli : sa.Work_Order_Line_Items__r) {
                    woli.Service_Resource__c = newServiceResourceIdByServiceAppointmentId.get(sa.Id);
                    wolisWtChangedAssignedResource.add(woli);
                }
            }
        }
        
        // System.debug('@wolisWtChangedAssignedResource: ' + wolisWtChangedAssignedResource);

        if(!wolisWtChangedAssignedResource.isEmpty()) {
            Update wolisWtChangedAssignedResource;
        }

    } 
    
}