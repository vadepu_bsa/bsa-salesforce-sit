/**
* @author          Sunila.M
* @date            23/April/2019 
* @description     
*
* Change Date       Modified by         Description  
* 23/April/2019     Sunila.M           Created Trigger to restrict more than one Service Appointment for same Work Order
* 01/Jul/2020       Bohao Chen @IBM     JIRA FSL3-149 clone bsa and site tasks upon cloning service appointments
* 02/Jul/2020       David Azzi          JIRA FSL3-100 Track SA Status, update WorkOrder status based on SA Status
* 24/Jul/2020		David Azzi			JIRA FSL3-158 Added ScheduledSLAsEnhancement to Before insert
**/
public without sharing class ServiceAppointmentTriggerHandler implements ITriggerHandler{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void BeforeInsert(List<SObject> serviceAppointmentList) {        
        //--validate SA record for the Parent WO
        //ServiceAppointmentValidationHelper.validateServiceAppointment((List<ServiceAppointment>)serviceAppointmentList);
        
        //Legacy code above, new code - David Azzi #FSL3-152 30/06/20
        List<ServiceAppointment> lstAPSServiceAppointment = new List<ServiceAppointment>();
        List<ServiceAppointment> lstNonAPSServiceAppointment = new List<ServiceAppointment>();
        Id SARecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('APS').getRecordTypeId();
        for (SObject obj : serviceAppointmentList){ 
            ServiceAppointment sa = (ServiceAppointment)obj;            
            if (sa.RecordTypeId != SARecordTypeId){
                lstNonAPSServiceAppointment.add(sa);
            }
            else {
                lstAPSServiceAppointment.add(sa);
                sa.Status = 'None'; // this is the API name of the picklist option 'New'
            }
        }
        
        if (!lstNonAPSServiceAppointment.isEmpty()){
            ServiceAppointmentValidationHelper.validateServiceAppointment(lstNonAPSServiceAppointment);
        }
        
        if (lstAPSServiceAppointment.size()>0){
            
        }
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} 
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        // delete all woli tasks associated with the SA
        List<WorkOrderLineItem> woliTasks = [SELECT Id FROM WorkOrderLineItem WHERE Service_Appointment__c IN: oldItems.keySet()];
        if(!woliTasks.isEmpty()) {
            delete woliTasks;
        }
    }  
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
         
        List<ServiceAppointment> lstAPSServiceAppointment = new List<ServiceAppointment>();
        List<ServiceAppointment> lstNBNServiceAppointment = new List<ServiceAppointment>();
        //List<>
        Id SARecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('APS').getRecordTypeId();
        for (SObject obj : newItems.values()){ 
            ServiceAppointment sa = (ServiceAppointment)obj;
            ServiceAppointment saOld = (ServiceAppointment)oldItems.get(sa.Id);
            if (sa.RecordTypeId == SARecordTypeId){
                lstAPSServiceAppointment.add(sa);
            }
            if(sa.RecordTypeId != SARecordTypeId && sa.Status =='Scheduled' && saOld.Status!='Scheduled'){
                lstNBNServiceAppointment.add(sa);
            }
        }
        if (lstNBNServiceAppointment.size() > 0){
            //Call @Future Callout to fetch List
            String jsonString = json.serialize(lstNBNServiceAppointment);
            //ServiceAppointmentTriggerHelper.getMediaList(jsonString);
            ID jobID = System.enqueueJob(new FetchMediaListQueueable(jsonString));
			System.debug('jobID'+jobID);
        }
        if (lstAPSServiceAppointment.size() > 0){
            ServiceAppointmentTriggerHelper.updateWorkOrderStatus(lstAPSServiceAppointment);
            APS_TaskCreationHelper.assignTaskResource(lstAPSServiceAppointment, (Map<Id, ServiceAppointment>)oldItems);
        }
        
    } 

    public void AfterDelete(Map<Id, SObject> oldItems) {} 

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {
        List<ServiceAppointment> lstAPSServiceAppointment = new List<ServiceAppointment>();
        Set<Id> setSAId = new Set<Id>();

        Id SARecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('APS').getRecordTypeId();
        for (SObject obj : newItems.values()){ 
            ServiceAppointment sa = (ServiceAppointment)obj;
            if (sa.RecordTypeId == SARecordTypeId){
                lstAPSServiceAppointment.add(sa);
                setSAId.add(sa.Id);
            }
        }

        if(!lstAPSServiceAppointment.isEmpty()) {
            APS_TaskCreationHelper.cloneBsaAndSiteTasks(lstAPSServiceAppointment);
            // TODO: Future method cannot be called from a future or batch method. need to move this to queueable job
            ServiceAppointmentTriggerHelper.ScheduledSLAsEnhancement(setSAId);
            //ServiceAppointmentTriggerHelper.populateWorkOrderDuration(lstAPSServiceAppointment);
            serviceAppointmentTriggerHelper.fireWoTrigger(lstAPSServiceAppointment);
        }
    }      

}