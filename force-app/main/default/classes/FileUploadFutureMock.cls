@isTest
global class FileUploadFutureMock implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"OK","statusCode":"200"}');
        res.setStatus('Success');
        res.setStatusCode(200);
        return res; 
    }
}