/**
* @author          Sunila.M
* @date            11/Feb/2019	
* @description    
* Change Date    Modified by         Description  
* 11/Feb/2019		Sunila.M		Trigger handler for Product consumed before create/update
* 22/Feb/2019		Sunila.M		FSL-554- Only Approved Price Book Entries should be allowed when WorkOrder.Client_Approval_Received__c is true
* 20/Jan/2020       Karan Shekhar   MOB-71 Added support to send  Pronto_Van_Stock_Consumption_Update__e PE when a product consumed record is deleted from FSL App
**/
public class ProductConsumedValidationHandler {
    
    public static void populateCorrelationID (List<ProductConsumed> newList) {
        
        for(ProductConsumed obj : newList) {    
            if(String.isNotBlank(obj.Product_Group__c) && obj.Product_Group__c.EqualsIgnoreCase(BSA_ConstantsUtility.PRODUCT_GROUP_PART)){
                
                obj.CorrelationID__c = UtilityClass.getUUID();
            }
            
        }
        
    }
    
    //method to publish Pronto_Van_Stock_Consumption_Update__e when a product consumed record is deleted by Techs from FSL App
    public static void publishVanStockConsumptionPlatformEvent(List<ProductConsumed> oldProductConsumedList){ 
        List<ProductConsumed> productConsumedListToProcess = new List<ProductConsumed>();
        Set<Id> pcIdSet = new Set<Id>();        
        for(ProductConsumed pc : oldProductConsumedList){
         
            if(String.isNotBlank(pc.Product_Group__c) && pc.Product_Group__c.EqualsIgnoreCase(BSA_ConstantsUtility.PRODUCT_GROUP_PART)){
                
                pcIdSet.add(pc.Id);
            }
        }        
        
        for(ProductConsumed pc :[SELECT ID,Correlationid__c,customer_code__c,ProductConsumedNumber,QuantityConsumed,ProductItemid,ProductItem.SerialNumber,ProductItem.Product_code__c,ProductItem.LocationId,ProductItem.Location.van_id__c,Workorder.workordernumber,Delta_consumption__c,Sales_order_number__c FROM ProductConsumed WHERE Id IN :pcIdSet]){
            System.debug('pc'+pc);
            pc.Delta_Consumption__c = -pc.QuantityConsumed;
            productConsumedListToProcess.add(pc);
        }
        if(!productConsumedListToProcess.isEmpty()){
            PlatformEventProcessing.processPlatformEvent(productConsumedListToProcess,true);           
        }                       
        
    }
    
    
    public static void validatePriceBookCode(List<ProductConsumed> productConsumedList){
        
        //--holds the list of new product consumed record which already exists
        List<Id> PCExistingIdList = new List<String>();
        
        //--query the existing product consumed for the Work Order and build a Map with Key as WorkOrdeId#ProductCode and value as list of existing price Code/Item :START
        
        //--holds the ProductConsumed.ProductCode to query the matadata
        List<String> priceCodeList = new List<String>();
        Set<Id> priceBookEntryIds = new Set<Id>();
        Set<Id> workOrderIdList = new Set<Id>();
        
        //--holds the key as productConsumed.WorkOrderId and value as ProductCode
        Map<Id,String> WOIdPriceBookCodeMap = new Map<Id,String>();
        
        //--map with key as pricebook entry Id and value Work order Id
        Map<Id,Id> PBEntryIdWOIdMap = new Map<Id,Id>();
        try{
            
            
            FSL_General_Field_Value_Setting__mdt accountStructure = [SELECT MasterLabel, QualifiedApiName, Value__c FROM FSL_General_Field_Value_Setting__mdt where QualifiedApiName = 'Account_Structure_Bypass_PC_Check'];
            List<String> accountStructureList = accountStructure.Value__c.split(';');
            
            for(ProductConsumed eachProductConsumed : productConsumedList){
                System.debug('eachProductConsumed'+eachProductConsumed);                
                System.debug('eachProductConsumed'+eachProductConsumed.PricebookEntryId);
                workOrderIdList.add(eachProductConsumed.WorkOrderId);
                priceBookEntryIds.add(eachProductConsumed.PricebookEntryId);
                System.debug('priceBookEntryIds'+priceBookEntryIds);
                PBEntryIdWOIdMap.put(eachProductConsumed.PricebookEntryId, eachProductConsumed.WorkOrderId);
            }
            
            //--FSL-554 : Holds the WOID#ProductCode as key and PBEntryNeedApproved as value
            Map<String, Boolean> WOProductCodeNeedApproved = new Map<String, Boolean>();
            //--holds the Work OrderId where approvalreceived is true
            List<Id> approvalreceivedWOId = new List<Id>();
            
            system.debug('PBEntryIdWOIdMap'+PBEntryIdWOIdMap);
            //query the current Product consumed price book code 
            for(PricebookEntry eachPriceBookEntry : [SELECT Id, ProductCode,Needs_Approval__c  FROM PricebookEntry WHERE Id IN:priceBookEntryIds]){
                WOIdPriceBookCodeMap.put(PBEntryIdWOIdMap.get(eachPriceBookEntry.Id), eachPriceBookEntry.ProductCode );
                priceCodeList.add(eachPriceBookEntry.ProductCode);
                if(eachPriceBookEntry.Needs_Approval__c){
                    WOProductCodeNeedApproved.put(PBEntryIdWOIdMap.get(eachPriceBookEntry.Id)+'#'+eachPriceBookEntry.ProductCode,eachPriceBookEntry.Needs_Approval__c);
                }
            }
            PBEntryIdWOIdMap = null;
            system.debug('WOIdPriceBookCodeMap'+WOIdPriceBookCodeMap);
            
            Map<String,List<String>> wOPBCodeExistingPBCodeMap = new Map<String,List<String>>();
            for(ProductConsumed eachExistingPC : [SELECT Id, WorkOrderId,PricebookEntry.ProductCode, ParentWOAccountStructure__c FROM ProductConsumed WHERE WorkOrderId IN: workOrderIdList] ){
                system.debug('eachExistingPC>>'+eachExistingPC.PricebookEntry.ProductCode);
                for(ProductConsumed eachProductConsumed : productConsumedList){
                    system.debug('new code>>'+WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId));
                    
                    //--if the Product consumed exists with the same code show an error "You cannot add as this already exists"
                    if(eachProductConsumed.WorkOrderId == eachExistingPC.WorkOrderId){
                        if(WOIdPriceBookCodeMap.containsKey(eachProductConsumed.WorkOrderId) && WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId) == eachExistingPC.PricebookEntry.ProductCode && Trigger.isInsert){
                            eachProductConsumed.addError('You cannot add as "'+eachExistingPC.PricebookEntry.ProductCode+'" already exists');
                            //PCExistingIdList.add(eachProductConsumed.id);
                            //--remove this combination from Map as already error messgae shown for this combination
                            WOProductCodeNeedApproved.remove(eachProductConsumed.WorkOrderId+'#'+eachExistingPC.PricebookEntry.ProductCode);
                        }
                        //-- add the record details to a map with key as WOId#ProductCode and value as list of existing product code
                        else{                        
                            if(wOPBCodeExistingPBCodeMap.containsKey(eachExistingPC.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachExistingPC.WorkOrderId)) ){
                                List<String> existingPriceCode = wOPBCodeExistingPBCodeMap.get(eachExistingPC.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachExistingPC.WorkOrderId));
                                existingPriceCode.add(eachExistingPC.PricebookEntry.ProductCode);
                                wOPBCodeExistingPBCodeMap.put(eachExistingPC.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachExistingPC.WorkOrderId),existingPriceCode);
                            }else{
                                List<String> existingPriceCode = new List<String>();
                                existingPriceCode.add(eachExistingPC.PricebookEntry.ProductCode);
                                wOPBCodeExistingPBCodeMap.put(eachExistingPC.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachExistingPC.WorkOrderId),existingPriceCode);
                            }
                        }
                    }
                }            
            }
            //--query the existing product consumed for the Work Order and build a Map with Key as WorkOrdeId#ProductCode and value as list of existing price Code/Item :END
            
            //--if product consumed does not exists already query metadata based on Account, Work Type and Code/Item and check the allowed code and build a Map with Product code as key and value as Allowed code 
            
            //get the current record details like ProductConsumed.WO.Case.Account.Name and ProductConsumed.WO.WorkType.Name
            List<String> accountNameList = new List<String>();
            List<String> workTypeNameList = new List<String>();
            Map<String,String> woIdPCDetailsMap = new Map<String,String>();
            system.debug('workOrderIdList'+workOrderIdList);
            for(WorkOrder eachnewPCWO : [SELECT Id, Case.Account.Name , WorkType.Name, Client_Approval_Received__c   FROM WorkOrder WHERE Id IN:workOrderIdList]){
                accountNameList.add(eachnewPCWO.Case.Account.Name);
                workTypeNameList.add(eachnewPCWO.WorkType.Name);
                woIdPCDetailsMap.put(eachnewPCWO.Id,eachnewPCWO.Case.Account.Name+'#@#'+eachnewPCWO.WorkType.Name);
                if(eachnewPCWO.Client_Approval_Received__c){
                    approvalreceivedWOId.add(eachnewPCWO.Id);
                }
            }
            
            //--FSL-554
            system.debug('WOProductCodeNeedApproved'+WOProductCodeNeedApproved+'>>>>approvalreceivedWOId>>'+approvalreceivedWOId);
            if(WOProductCodeNeedApproved.size() > 0){            
                for(ProductConsumed eachProductConsumed : productConsumedList){               
                    //check whether the priceBook entry has need Approval checked, if checked allow else do not allow
                    if(WOProductCodeNeedApproved.containsKey(eachProductConsumed.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId))){
                        if(!approvalreceivedWOId.contains(eachProductConsumed.WorkOrderId)){
                            eachProductConsumed.addError('This SOR code needs approval from Operations.');
                            //--remove this combination from Map as already error messgae shown for this combination
                            wOPBCodeExistingPBCodeMap.remove(eachProductConsumed.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId));
                        }                                        
                    }               
                }            
            }     
            //--FSL-554: END
            
            system.debug('workTypeNameList'+workTypeNameList);        
            system.debug('accountNameList'+accountNameList);        
            system.debug('priceCodeList'+priceCodeList);  
            Map<String, List<String>> codeAllowedCodeListMap = new Map<String, List<String>>();
            for(Price_Book_Dependent_Code__mdt eachDependentCode : [SELECT Allowed_Code__c,Account__c,Code__c, Work_Type__c FROM Price_Book_Dependent_Code__mdt WHERE Account__c IN:accountNameList AND Work_Type__c IN:workTypeNameList AND Code__C IN:priceCodeList] ){
                
                String allowedCode = eachDependentCode.Allowed_Code__c;
                List<String> allowedCodeList = (List<String>)allowedCode.split(',');
                
                codeAllowedCodeListMap.put(eachDependentCode.Code__c+'#@#'+eachDependentCode.Account__c+'#@#'+eachDependentCode.Work_Type__c, allowedCodeList);
                system.debug('codeAllowedCodeListMap'+codeAllowedCodeListMap);            
            }    
            
            if(codeAllowedCodeListMap.size() >0 ){
                //--iterate, compare and add error messages
                for(ProductConsumed eachProductConsumed : productConsumedList){
                    system.debug('WOIdPriceBookCodeMap'+WOIdPriceBookCodeMap);
                    if(WOIdPriceBookCodeMap.containsKey(eachProductConsumed.WorkOrderId) ){
                        String accountWorkTypeName = woIdPCDetailsMap.get(eachProductConsumed.WorkOrderId);
                        String PCCombination = WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId)+'#@#'+accountWorkTypeName.split('#@#')[0]+'#@#'+accountWorkTypeName.split('#@#')[1];
                        if(codeAllowedCodeListMap.containsKey(PCCombination)){
                            List<String> allowedCodes = codeAllowedCodeListMap.get(PCCombination);
                            
                            if(wOPBCodeExistingPBCodeMap.containsKey(eachProductConsumed.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId))){
                                for(String existingProductCode : wOPBCodeExistingPBCodeMap.get(eachProductConsumed.WorkOrderId+'#'+WOIdPriceBookCodeMap.get(eachProductConsumed.WorkOrderId))){
                                    if(!allowedCodes.contains(existingProductCode)){
                                        
                                        eachProductConsumed.addError('Invalid Price Book selection');                                                                  
                                        break;
                                    }
                                }      
                            }                                  
                        }
                    }
                }
            } 
        }catch (Exception excp){
            
            system.debug('Exception while validating product cosumption'+excp.getMessage()+excp.getStackTraceString());
        }
    }
}