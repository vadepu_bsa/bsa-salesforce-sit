/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 06-28-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   06-21-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class APS_ConvertQuoteToWorkOrderCtr {
    @AuraEnabled
    public static List<String> generateWorkOrders(String orderId, String orderRecordType){

        // System.debug('@orderId: ' + orderId);
        // System.debug('@orderRecordType: ' + orderRecordType);
        List<String> workOrderIds;

        Savepoint sp = Database.setSavepoint();

        try {
            // fetch a list of approved quote line items
            String query = 'SELECT Defect__c, OrderId, Order.AccountId, Asset__c, Asset__r.Parent.Skill_Group__c, Labour_Total__c, Markup_Price__c, Description, Asset__r.ParentId, OrderItemNumber, Order.RecordType.DeveloperName, Product2Id, Product2.ProductCode';
            query += ' FROM OrderItem WHERE OrderId =: orderId AND Status__c = \'Approved\'';

            // for defect quote, line item must have asset: MZ commenting this code to include the labour and material charges in calculation. 
           // if(orderRecordType == 'Defect_Quote') {
           //     query += ' AND Asset__c != NULL';
            //}
            
            List<OrderItem> approvedOrderItems = (List<OrderItem>)Database.query(query);
            // System.debug('@approvedOrderItems: ' + approvedOrderItems);

            String errorMsg = '';
            // for defect order, checking order items to see if the equipment group of all assets which link to order lines/items has value. 
            if(orderRecordType == 'Defect_Quote') {
                for(OrderItem oi : approvedOrderItems) {                
                    if(oi.Asset__c != null && oi.Asset__r.ParentId == null) {
                        errorMsg = 'Order item ' + oi.OrderItemNumber + ' doesn\'t have the equipment group on its asset';
                        break;
                    }
                    else if(oi.Asset__c != null && oi.Asset__r.Parent.Skill_Group__c == null) {
                        errorMsg = 'Order item ' + oi.OrderItemNumber + ' doesn\'t have the skill group on the equipment group that links to its asset';
                        break;
                    }
                }
            }

            if(String.isNotBlank(errorMsg)) {
                throw new APS_DefectController.applicationException(errorMsg);
            }
            else {
                List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrderWrappers;
                if(orderRecordType == 'Defect_Quote') {
                    List<WorkOrderLineItemWrapper> workOrderLineItems = createWorkOrderLineItems(approvedOrderItems);
                    workOrderWrappers = groupWorkOrderLineItems(workOrderLineItems);
                    
                }
                else if(orderRecordType == 'Non_Defect_Quote') {
                    workOrderWrappers = createNonDefectWorkOrder(orderId);
                }
                workOrderIds = insertRecords(workOrderWrappers);
                
            }
        }
        catch(Exception e) {
            System.debug('Ananti_Error'+ e.getMessage());
            //System.debug(e.getMessage());
            Database.rollback(sp);
            throw new AuraHandledException(JSON.serialize(e.getMessage()));
        }
        // System.debug('@workOrderIds: ' + workOrderIds);
        return workOrderIds;
    }

    private static List<WorkOrderLineItemWrapper> createWorkOrderLineItems(List<OrderItem> approvedOrderItems) {
        System.debug('Ananti_approvedOrderItems: ' + approvedOrderItems);
        List<WorkOrderLineItemWrapper> workOrderLineItems = new List<WorkOrderLineItemWrapper>();

        Id woliRecordTypeId = APS_WorkOrderGeneratorCtr.woliRecordTypeMap.get('Defect').getRecordTypeId();
        Map<Id, List<OrderItem>> processOrderMap = new Map<Id, List<OrderItem>>();
        
        for (integer i = 0; i < approvedOrderItems.size(); i++){
            if (processOrderMap.containsKey(approvedOrderItems[i].Defect__c)){
                processOrderMap.get(approvedOrderItems[i].Defect__c).add(approvedOrderItems[i]);
            } else {
                processOrderMap.put(approvedOrderItems[i].Defect__c,new List<OrderItem>{approvedOrderItems[i]});
            }
        }

        //FR-338 
        //Check if at least one defect is available and each defect should have at least one asset validation    
        //Commented By Neil (Ananti Changes for 338) Start    
        Boolean IsDefectLinked = false;
        for (Id defectId : processOrderMap.KeySet() ){            
            if(defectId != null){
                IsDefectLinked = true;
                Boolean IsAssetLinked = false;
                List<OrderItem> ItemList = processOrderMap.get(defectId);
                for (Integer i = 0; i < ItemList.size(); i++){                    
                    if (ItemList[i].Asset__c != null){
                        IsAssetLinked = true;
                        break;
                    }
                }
                if(IsAssetLinked == false)
                {
                    throw new APS_DefectController.applicationException('For each defect at least one asset need to be linked.');
                }
            }
        }
        if(IsDefectLinked == false)
        {
            throw new APS_DefectController.applicationException('To create work order at least one defect need to be linked.');
        }

     
        System.debug('Ananti_processOrderMap: ' + processOrderMap);
        //Commented By Neil (Ananti Changes for 338) End
        List<Orderitem> filteredOrderitem  = new List<Orderitem>();
        for (Id defectId : processOrderMap.KeySet() ){            
            List<OrderItem> ItemList = processOrderMap.get(defectId);
            decimal LabourTotal = 0;
            decimal MarkUpTotal = 0;
            orderItem itemObj = new OrderItem();
            for (Integer i = 0; i < ItemList.size(); i++){
                //FR-338
                if(defectId != null)
               {
                    if (ItemList[i].Asset__c != null){                    
                        itemObj = ItemList[i];                    
                        LabourTotal += (ItemList[i].Labour_Total__c != null) ? ItemList[i].Labour_Total__c : 0;
                        MarkUpTotal += (ItemList[i].Markup_Price__c != null ) ? ItemList[i].Markup_Price__c : 0;
                    }
                    if (ItemList[i].Product2Id != null && ItemList[i].Product2.ProductCode == 'GenericLabourInternal'){
                        LabourTotal += (ItemList[i].Labour_Total__c != null) ? ItemList[i].Labour_Total__c : 0 ;
                    }
                    if (ItemList[i].Product2Id != null && ItemList[i].Product2.ProductCode == 'GenericPartSOR'){
                        MarkUpTotal += ( ItemList[i].Markup_Price__c != null ) ? ItemList[i].Markup_Price__c : 0 ;
                    }
                }
                else{
                    LabourTotal += (ItemList[i].Labour_Total__c != null) ? ItemList[i].Labour_Total__c : 0;
                    MarkUpTotal += (ItemList[i].Markup_Price__c != null ) ? ItemList[i].Markup_Price__c : 0;                    
                }
            }
            itemObj.Labour_Total__c = LabourTotal;
            itemObj.Markup_Price__c = MarkUpTotal;
            filteredOrderitem.add(itemObj);                        
        }

        System.debug('Ananti_filteredOrderitem: ' + filteredOrderitem);
        for(OrderItem oli : filteredOrderitem) {
            WorkOrderLineItem woli = new WorkOrderLineItem(
                AssetId = oli.Asset__c,
                RecordTypeId = woliRecordTypeId,
                Related_Defect__c = oli.Defect__c,
                Defect_Details__c = oli.Description,
                Notes__c = oli.Description,
                Subject = 'Rectify Defect'
            );

            if(oli.Asset__c != null) {
               workOrderLineItems.add(new WorkOrderLineItemWrapper(woli, oli.Asset__r.Parent.Skill_Group__c, oli.Order.AccountId, oli.OrderId, oli.Labour_Total__c, oli.Markup_Price__c, oli.Defect__c));
             //workOrderLineItems.add(new WorkOrderLineItemWrapper(woli, oli.Asset__r.Parent.Skill_Group__c, oli.Order.AccountId, oli.OrderId, oli.Labour_Total__c, oli.Markup_Price__c));

            }
            else {
               workOrderLineItems.add(new WorkOrderLineItemWrapper(woli, null, oli.Order.AccountId, oli.OrderId, oli.Labour_Total__c, oli.Markup_Price__c, oli.Defect__c));
             //workOrderLineItems.add(new WorkOrderLineItemWrapper(woli, null, oli.Order.AccountId, oli.OrderId, oli.Labour_Total__c, oli.Markup_Price__c));

            }
        }

        return workOrderLineItems;
    }
    
    private static List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> groupWorkOrderLineItems(List<WorkOrderLineItemWrapper> workOrderLineItems) {
     //   System.debug('Ananti_GroupWorkOrderLineItems_workOrderLineItems: ' + workOrderLineItems);
        List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrders = new List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper>();
        Map<String, List<WorkOrderLineItemWrapper>> workOrderLineItemsBySkillGroup = new Map<String, List<WorkOrderLineItemWrapper>>();
        List<WorkOrderLineItemWrapper> NonDefectOrderList = new List<WorkOrderLineItemWrapper>();
     
        Id siteId;
        Id orderId;
        Id workTypeId = [SELECT Id FROM WorkType WHERE Name = 'Defect Rectification' Limit 1].Id;
       
        for(WorkOrderLineItemWrapper woliWrapper : workOrderLineItems) {
            //FR-338            
            System.debug('Ananti_defectId: ' + woliWrapper.defectId);            
            if(woliWrapper.defectId != null)
            {
                            
                String skillGroup = woliWrapper.skillGroup;
                siteId = siteId == null ? woliWrapper.siteId : siteId;
                //FR-338
                //orderId = woliWrapper.orderId;
                orderId = (orderId == null)?  woliWrapper.orderId: orderId;
                //System.debug('Ananti_For Loop orderId: ' + orderId + ', woliWrapper.orderId: ' + woliWrapper.orderId);
                if(!workOrderLineItemsBySkillGroup.containsKey(skillGroup)) {
                    workOrderLineItemsBySkillGroup.put(skillGroup, new List<WorkOrderLineItemWrapper>());
                }

                workOrderLineItemsBySkillGroup.get(skillGroup).add(woliWrapper);
            }
            else{
                NonDefectOrderList.add(woliWrapper);
            }
        }       
        System.debug('Ananti_workOrderLineItemsBySkillGroup: ' + workOrderLineItemsBySkillGroup);
        // System.debug('@workOrderLineItemsBySkillGroup: ' + workOrderLineItemsBySkillGroup);
        System.debug('Ananti_orderId: ' + orderId);
        //FR-338
       String QDescription = null;
        String QScopec = null;
        if(orderId != null){
            Order DefectQuote = [SELECT AccountId, Description, Scope__c, Sub_Total__c FROM Order WHERE Id =: orderId LIMIT 1];
           System.debug('Ananti_DefectQuote : ' + DefectQuote);
            if(DefectQuote != null)
            {
                QDescription = (DefectQuote.Description != null) ? DefectQuote.Description : null;
                QScopec = (DefectQuote.Scope__c != null) ? DefectQuote.Scope__c : null;
            }
        }       
        Id apsWorkOrderRecordTypeId = APS_WorkOrderGeneratorCtr.woRecordTypeMap.get('APS_Work_Order').getRecordTypeId();
        

        for(String skillGroup : workOrderLineItemsBySkillGroup.keySet()) {
            APS_WorkOrderGeneratorCtr.WorkOrderWrapper wow = new APS_WorkOrderGeneratorCtr.WorkOrderWrapper();

            // calculate quoted value 
            Decimal quotedValue = 0;                     
            for(WorkOrderLineItemWrapper woliw : workOrderLineItemsBySkillGroup.get(skillGroup)) {
                quotedValue += woliw.labourTotal + woliw.markupPrice;                          
                wow.workOrderLineItems.add(woliw.woli);                            
            }

            //System.debug('Ananti_For Loop_quotedValue : ' + quotedValue);
            //System.debug('Ananti_For Loop_wow.workOrderLineItems : ' + wow.workOrderLineItems);
                      
            WorkOrder wo = new WorkOrder(
                Skill_Group__c = skillGroup,
                WorkTypeId = workTypeId,
                RecordTypeId = apsWorkOrderRecordTypeId,
                AccountId = siteId,
                Quoted_Value__c = quotedValue,
                Order__c = orderId,
                Line_Item_Count__c = wow.workOrderLineItems.size(),
                //FR-338
               // Description = DefectQuote.Description,
               // Scope__c = DefectQuote.Scope__c,        
                Description = QDescription,
                Scope__c = QScopec,
                Asset_WOLIs_Creation_Completed__c=false
                
            );

            wow.workOrder = wo;
            workOrders.add(wow);            
        }
        System.debug('Ananti_workOrders: ' + workOrders);

        //FR-338 -- Add the non defect quote value to the first work order in the list
        if(NonDefectOrderList != null && NonDefectOrderList.size() > 0 && workOrders!= null && workOrders.size() > 0)
        {
            System.debug('Ananti_AfterUpdate_NonDefectOrderList: ' + NonDefectOrderList);
            Decimal quotedValue = 0;
            for(WorkOrderLineItemWrapper woliw : NonDefectOrderList) {
                quotedValue += woliw.labourTotal + woliw.markupPrice;                
            }
            workOrders[0].workOrder.Quoted_Value__c += quotedValue;
            System.debug('Ananti_AfterUpdate_ForLoop_quotedValue: ' + quotedValue);
        }

        System.debug('Ananti_AfterUpdate_workOrders: ' + workOrders);
        return workOrders;
    }

    private static List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> createNonDefectWorkOrder(String orderId) {
        List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrders = new List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper>();

        Order nonDefectQuote = [SELECT AccountId, Description, Scope__c, Sub_Total__c FROM Order WHERE Id =: orderId LIMIT 1];
        Id workTypeId = [SELECT Id FROM WorkType WHERE Name = 'Quote' Limit 1].Id;
        Id apsWorkOrderRecordTypeId = APS_WorkOrderGeneratorCtr.woRecordTypeMap.get('APS_Work_Order').getRecordTypeId();

        APS_WorkOrderGeneratorCtr.WorkOrderWrapper wow = new APS_WorkOrderGeneratorCtr.WorkOrderWrapper();

        WorkOrder wo = new WorkOrder(
            WorkTypeId = WorkTypeId,
            RecordTypeId = apsWorkOrderRecordTypeId,
            AccountId = nonDefectQuote.AccountId,
            Description = nonDefectQuote.Description,
            Scope__c = nonDefectQuote.Scope__c,            
            Quoted_Value__c = nonDefectQuote.Sub_Total__c,
            Order__c = orderId
        );

        wow.workOrder = wo;
        workOrders.add(wow);
        return workOrders;
    }
    private static List<Id> insertRecords(List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrderWrappers) {

        List<Id> workOrderIds = new List<Id>();

        List<WorkOrder> workOrders = new List<WorkOrder>();
        List<WorkOrderLineItem> workOrderLineItems = new List<WorkOrderLineItem>();

        for(APS_WorkOrderGeneratorCtr.WorkOrderWrapper workOrderWrapper : workOrderWrappers) {
            workOrderWrapper.workOrder.Number_of_Remaining_Assets__c = 0;
            workOrders.add(workOrderWrapper.workOrder);
        }

        Savepoint sp = Database.setSavepoint();

        try {
            if(!workOrders.isEmpty()) {
                Insert workOrders;

                for(APS_WorkOrderGeneratorCtr.WorkOrderWrapper workOrderWrapper : workOrderWrappers) {
                    // System.debug('@workOrder id' + workOrderWrapper.workOrder.Id);

                    // populate work order id for work order line items
                    for(WorkOrderLineItem woli : workOrderWrapper.workOrderLineItems) {
                        woli.WorkOrderId = workOrderWrapper.workOrder.Id;
                        workOrderLineItems.add(woli);
                    }

                }

                if(!workOrderLineItems.isEmpty()) {
                    //Insert workOrderLineItems;

                    List<WorkOrderLineItem> taskWolis = new List<WorkOrderLineItem>();

                    for(WorkOrderLineItem woli : workOrderLineItems) {
                        WorkOrderLineItem taskWoli = new WorkOrderLineItem();
                        // System.debug('@woli: ' + woli);
                        taskWoli.WorkOrderId = woli.WorkOrderId;
                        taskWoli.RecordTypeId = woli.RecordTypeId;
                        //taskWoli.ParentWorkOrderLineItemId = woli.Id;
                        taskWoli.AssetId = woli.AssetId;
                        taskWoli.Related_Defect__c = woli.Related_Defect__c;
                        taskWoli.Defect_Details__c = woli.Defect_Details__c;
                        taskWoli.Notes__c = woli.Notes__c;
                        taskWolis.add(taskWoli);
                    }
                    
        
                    if(!taskWolis.isEmpty()) {
                        Insert taskWolis;
                    }
                    for (integer i = 0; i <taskWolis.size(); i ++){
                        for (integer k = 0; k<workOrderLineItems.size(); k++){
                            if (workOrderLineItems[k].WorkOrderId == taskWolis[i].WorkOrderId  && workOrderLineItems[k].RecordTypeId == taskWolis[i].RecordTypeId && 
                                workOrderLineItems[k].AssetId == taskWolis[i].AssetId && workOrderLineItems[k].Related_Defect__c == taskWolis[i].Related_Defect__c){
                                 workOrderLineItems[k].ParentWorkOrderLineItemId = taskWolis[i].Id;
                                }
                        }
                    }
                    insert workOrderLineItems;
                }  
                //Updating field Asset_WOLIs_Creation_Completed__c=true
                for(Workorder wo:workOrders){
                    wo.Asset_WOLIs_Creation_Completed__c=true;
                }
                update workOrders;
            }
        } catch(Exception e) {
            Database.rollback(sp);
            throw new APS_DefectController.applicationException(e.getMessage());
        }

        for(WorkOrder wo : workOrders) {
            workOrderIds.add(wo.Id);
        }
        return workOrderIds;
    }

    class WorkOrderLineItemWrapper {
        @AuraEnabled
        public WorkOrderLineItem woli;
        @AuraEnabled
        public String skillGroup;
        @AuraEnabled
        public Id siteId;
        @AuraEnabled
        public Decimal labourTotal;
        @AuraEnabled
        public Decimal markupPrice;
        @AuraEnabled
        public Id orderId;
        @AuraEnabled
        public Id defectId;

        WorkOrderLineItemWrapper(WorkOrderLineItem woli, String skillGroup, Id siteId, Id orderId, Decimal labourTotal, Decimal markupPrice,Id defectId) {
            this.woli = woli;
            this.skillGroup = skillGroup;
            this.siteId = siteId;
            this.orderId = orderId;
            this.labourTotal = labourTotal != null ? labourTotal : 0;
            this.markupPrice = markupPrice != null ? markupPrice : 0;
           this.defectId = defectId;
        }

    }
}