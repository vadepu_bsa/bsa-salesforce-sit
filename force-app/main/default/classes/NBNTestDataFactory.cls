@isTest
public with sharing class NBNTestDataFactory {
    public static void createNBNTestData(){ 
        Account acc = createTestAccounts();
        WorkType workType =  createTestWorkType();
        Pricebook2 priceBook = createTestPriceBook();
        OperatingHours operatingHrs = createTestOperatingHours();
        ServiceTerritory servTerr = createTestServiceTerritory(operatingHrs.id);
        

        ServiceContract serviceContract =  createTestServiceContract(acc.id);
        createTestWOARS(acc.id,workType.id,servTerr.id,priceBook.id,serviceContract.Id);
        WorkOrder workorder = createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
    }
    public static User createAutomatedUser(){
        Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User userRec = new User( email='test.user@testmail.com',
                                profileid = profileDetails.Id, 
                                UserName='testauto.user@testmail1234.com', 
                                Alias = 'GDS',
                                TimeZoneSidKey='America/New_York',
                                EmailEncodingKey='ISO-8859-1',
                                LocaleSidKey='en_US', 
                                LanguageLocaleKey='en_US',
                                PortalRole = 'Manager',
                                FirstName = 'Automated',
                                LastName = 'TestUser');
        insert userRec;
        return userRec;
    }
    public static ServiceResource createTestResource(){
        Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Field Technician' LIMIT 1];
        User userRec = new User( email='test.user@testmail.com',
                                profileid = profileDetails.Id, 
                                UserName='test.user@testmail123.com', 
                                Alias = 'GDS',
                                TimeZoneSidKey='America/New_York',
                                EmailEncodingKey='ISO-8859-1',
                                LocaleSidKey='en_US', 
                                LanguageLocaleKey='en_US',
                                PortalRole = 'Manager',
                                FirstName = 'Test',
                                LastName = 'User');
        insert userRec;
        // Create a test Service Resource record
        ServiceResource serviceResourceRecord = new ServiceResource();
        serviceResourceRecord.Name = 'Test Service User';
        //serviceResourceRecord.User_Id__c = userRec.Id;
        serviceResourceRecord.RelatedRecordId = userRec.Id;
        serviceResourceRecord.IsActive = true;
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78963';
        //serviceResourceRecord.LocationId=locationRecord.id;
        //Database.SaveResult srServiceResource = database.insert(serviceResourceRecord);
        Insert serviceResourceRecord;
        return serviceResourceRecord;
    }
    public static Account createTestAccounts(){
        
        //create Account
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account1 = new Account();
        account1.Name = 'Test Account1234509';
        account1.RecordTypeId = clientRecordTypeId;
        account1.Structure__c = 'Hierarchy';
        insert account1;
        system.debug('account1'+account1.Id);
        
        return account1;
    }
    public static Asset createAsset(Id accountId){
        
        //--create Asset
        Asset asset1 = new Asset();         
        asset1.Name = 'Test Asset Rec';
        asset1.AccountId = accountId;
        insert asset1;        
        
        return asset1;    
    }

    public static Contact createContact(Id accountId){
        
        //--create Contact
        Contact contact1 = new Contact();         
        contact1.Email = 'test.email@bsaa.com';
        contact1.Phone = '001234567899';
        contact1.MobilePhone = '021234567899';
        contact1.LastName = 'Test Contact BSA';
        contact1.AccountId = accountId;
        insert contact1;        
        
        return contact1;    
    }

    public static Work_Order_Asset__c createWOAsset(Id workorderId, Id assetId){
    
        //--create WorkOrder Asset
        Work_Order_Asset__c woAsset = new Work_Order_Asset__c();
        woAsset.Asset__c = assetId;
        woAsset.Work_Order__c = workorderId;
        Insert woAsset;

        return woAsset;
    }

    public static Work_Order_Contact__c createWOContact(Id workorderId, Id contactId){
    
        //--create WorkOrder Contact
        Work_Order_Contact__c woContact = new Work_Order_Contact__c();
        woContact.Contact__c = contactId;
        woContact.Work_Order__c = workorderId;
        Insert woContact;

        return woContact;
    }
    
    public static Work_Order_Task__c createWOTasks(Id workorderId){
    
        //--create WorkOrder Contact
        Work_Order_Task__c wotaskObj = new Work_Order_Task__c();
        wotaskObj.Work_Order__c = workorderId;
        wotaskObj.Work_Order_Task_Id__c ='NBNExt001';
        Insert wotaskObj;

        return wotaskObj;
    }

    public static Custom_Attribute__c createCustomAttributes(Id workorderId,Id workorderTaskId){
        
        //--create Custom Attributes on WO
        Custom_Attribute__c customattr1 = new Custom_Attribute__c();         
        if(workorderId!=null){
            
            customattr1.Name__c = 'Test Custom Attribute';
            customattr1.Value__c = 'Test Custom Attribute Value';
            customattr1.Work_Order__c = workorderId;
            insert customattr1; 
        }
        else if(workorderTaskId!=null){
            
            customattr1.Name__c = 'Test Custom Attribute';
            customattr1.Value__c = 'Test Custom Attribute Value';
            customattr1.Work_Order_Task__c = workorderTaskId;
            insert customattr1;  
        }
        
        return customattr1;    
    }

    public static NBN_Log_Entry__c createNBNLogEntry(Id workorderId){
        
        //--create NBN Log Entry
        NBN_Log_Entry__c nbnLogEntry1 = new NBN_Log_Entry__c();         
        nbnLogEntry1.Summary__c = 'Testing note creation';
        nbnLogEntry1.NBN_Field_Work_Instructions__c	 = 'Test Instructions';
        nbnLogEntry1.Work_Order__c = workorderId;
        insert nbnLogEntry1;        
        
        return nbnLogEntry1;    
    }
    public static WorkType  createTestWorkType(){
        
        //--Create Work type
        WorkType workType = new WorkType();
        workType.CUI__c =TRUE;
        workType.Name = 'Activity_HFC_Network_Assurance';
        Date todayDate = Date.today();
        workType.EstimatedDuration = 5;
        workType.Primary_Access_Technology__c  = 'HFC';
        insert workType;
        system.debug('workType'+workType.Id);
        return workType;
    }
    
    public static Pricebook2 createTestPriceBook(){
        
        //--create Price Book
        
        Pricebook2 priceBook = new Pricebook2();
        priceBook.Name = 'NBN HFC Network Assurance NSW Central Metro Year 4';
        priceBook.IsActive = true;
        insert priceBook;
        return priceBook;
    }
    
    public static OperatingHours createTestOperatingHours(){
        
        //--create operating hours
        OperatingHours operatingHours = new OperatingHours();
        operatingHours.Name = 'calender 1';
        insert operatingHours;
        return operatingHours;
    }
    
    public static ServiceTerritory createTestServiceTerritory(Id operatingHourId){
        
        //--create service territory         
        ServiceTerritory serviceTerritory = new ServiceTerritory();
        serviceTerritory.Name = 'NBN Central Metro';
        serviceTerritory.OperatingHoursId = operatingHourId;
        serviceTerritory.IsActive = true;
        insert serviceTerritory;
        
        return serviceTerritory;
        
    }
    public static void createTestServiceTerritoryMapping(Id serviceTerritoryId){
        
        //--create service territory         
        Service_Territory_Mappings__c serviceTerritorymapping = new Service_Territory_Mappings__c();
        serviceTerritorymapping.Active__c = true;
        serviceTerritorymapping.SAM_Id__c = '4RDC-72';
        serviceTerritorymapping.Service_Territory__c = serviceTerritoryId;
        insert serviceTerritorymapping;
        
        
    }
    
    public static void createTestWOARS(Id accountId, Id workTypeId, Id serviceTerritoryId, Id priceBookId, Id serviceContractId){
        
        Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
        Id woliDevRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('WOLI Automation').getRecordTypeId();
        Id workTypeRecTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Work Type Automation').getRecordTypeId();
        Id STARecTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Service Territory Automation').getRecordTypeId();
        Id SMARecTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Status Mapping Automation').getRecordTypeId();
        Id SVARecTypeId =Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('State Validation Automation').getRecordTypeId();
        System.debug('devRecordTypeId'+devRecordTypeId);
        
        //--create WOAR
        Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
        woar.Service_Contract__c = serviceContractId;
        woar.RecordTypeId = devRecordTypeId;
        woar.Work_Area__c = 'Urban';
        woar.Work_Type__c = workTypeId;
        woar.Service_Territory__c = serviceTerritoryId;
        woar.Price_Book__c =priceBookId;
        insert woar;
        
        Work_Order_Automation_Rule__c woarWoli = new Work_Order_Automation_Rule__c();
        woarWoli.Service_Contract__c = serviceContractId;
        woarWoli.RecordTypeId = woliDevRecordTypeId;
        woarWoli.Work_Type__c = workTypeId;
        woarWoli.Type__c = 'Capture Artefacts';
        woarWoli.Action_Type__c = 'Create WOLI';
        woarWoli.Action__c = 'Arrival site photo';
        woarWoli.Primary_Access_Technology__c = 'HFC';
        woarWoli.Sequence__c = 1;
        insert woarWoli;
        
        Work_Order_Automation_Rule__c woarWT = new Work_Order_Automation_Rule__c();
        woarWT.Service_Contract__c= serviceContractId;
        woarWT.Primary_Access_Technology__c ='HFC';
        woarWT.NBN_Field_Work_Specified_By_Id__c = 'RemediationWOS';
        woarWT.Work_Area__c ='Urban';
        woarWT.Work_Type__c = workTypeId;
        woarWT.RecordTypeId =workTypeRecTypeId;
        insert woarWT;
        
        Work_Order_Automation_Rule__c woarWSM = new Work_Order_Automation_Rule__c();
        woarWSM.Status__c ='New';
        woarWSM.Service_Contract__c = serviceContractId;
        woarWSM.State__c ='Assigned';
        woarWSM.RecordTypeId =SMARecTypeId;
        insert woarWSM;
        woarWSM = new Work_Order_Automation_Rule__c();
        woarWSM.Status__c ='Canceled';
        woarWSM.Service_Contract__c = serviceContractId;
        woarWSM.State__c ='Canceled';
        woarWSM.RecordTypeId =SMARecTypeId;
        insert woarWSM;
        woarWSM = new Work_Order_Automation_Rule__c();
        woarWSM.Status__c ='In Progress';
        woarWSM.Service_Contract__c = serviceContractId;
        woarWSM.State__c ='Onsite';
        woarWSM.RecordTypeId =SMARecTypeId;
        insert woarWSM;
        woarWSM = new Work_Order_Automation_Rule__c();
        woarWSM.Status__c ='In Progress';
        woarWSM.Service_Contract__c = serviceContractId;
        woarWSM.State__c ='Enroute';
        woarWSM.RecordTypeId =SMARecTypeId;
        insert woarWSM;
        
        Work_Order_Automation_Rule__c woarSTA = new Work_Order_Automation_Rule__c();
        woarSTA.RecordTypeId =STARecTypeId;
        woarSTA.Service_Area_Module__c ='3RCM-20';
        woarSTA.Service_Contract__c = serviceContractId;
        woarSTA.Service_Territory__c = serviceTerritoryId;
        insert woarSTA;
        
        Work_Order_Automation_Rule__c woarSV = new Work_Order_Automation_Rule__c();
        woarSV.Service_Contract__c = serviceContractId;
        woarSV.Action__c = 'enroute';
        woarSV.NBN_Field_Work_Specified_By_Id__c = 'RemediationWOS';
        woarSV.Valid_for_States__c ='Confirm Schedule';
        woarSV.RecordTypeId =SVARecTypeId;
        insert woarSV;
        
    }
    
    public static ServiceContract createTestServiceContract(Id accountId){
        
        //--create service territory
        ServiceContract serviceContract = new ServiceContract();         
        serviceContract.StartDate = System.today();
        serviceContract.EndDate = System.today().addYears(100);
        serviceContract.AccountId = accountId;
        serviceContract.Name='Test Service Contract';
        //serviceContract.ContractNumber ='00000485';
        insert serviceContract;
        ServiceContract serviceContractNew =[select Id,ContractNumber,StartDate,EndDate,AccountId,Name from ServiceContract where id=:serviceContract.Id];
        
        return serviceContractNew;
        
    }
    
    private static List<Product2> createProducts(){
        
        List<Product2> productList = new List<Product2>();
        Product2 product = new Product2();
        product.ProductCode = '02-03-00-01';
        product.Name ='02-03-00-01 /product';
        product.Family = 'None';
        product.Product_Group__c='Part';
        productList.add(product);
        
        Product2 product2 = new Product2();
        product2.ProductCode = '02-03-00-02';
        product2.Name ='02-03-00-02 /product';
        product2.Family = 'None';
        product.Product_Group__c='Part';
        productList.add(product2);       
        
        Product2 product3 = new Product2();
        product3.ProductCode = '02-03-00-03';
        product3.Name ='02-03-00-03 /product';
        product3.Family = 'None';
        product.Product_Group__c='Part';
        productList.add(product3);  
        
        Product2 product4 = new Product2();
        product4.ProductCode = '02-03-00-04';
        product4.Name ='02-03-00-04 /product';
        product.Product_Group__c='Part';//added for ProductConsumed
        product4.Family = 'None';
        productList.add(product4);
        insert productList;
        return productList;
    }
    
    private static List<PricebookEntry> createPriceBookEntries(List<Product2> productList, Id priceBookId){
        List<PricebookEntry> priceBookEntryList = new List<PricebookEntry>();
        List<PricebookEntry> stdPriceBookEntryList = new List<PricebookEntry>();
        Id stdpricebookId = Test.getStandardPricebookId();
        
        PriceBook2 stdPriceBook = new PriceBook2();
        stdPriceBook.Id = stdpricebookId;
        stdPriceBook.IsActive =true;
        update stdPriceBook;
        
        for(Product2 prod : productList){
            
            PricebookEntry priceBookEntry = new PricebookEntry();
            pricebookEntry.Pricebook2Id = priceBookId;
            pricebookEntry.Product2Id = prod.id;
            pricebookEntry.UnitPrice = 0.0;
            pricebookEntry.IsActive = true;
            pricebookEntry.UseStandardPrice = false;
            priceBookEntryList.add(pricebookEntry);
        }
        
        for(Product2 prod : productList){
            
            PricebookEntry priceBookEntry = new PricebookEntry();
            pricebookEntry.Pricebook2Id = stdpricebookId;
            pricebookEntry.Product2Id = prod.id;
            pricebookEntry.UnitPrice = 0.0;
            pricebookEntry.IsActive = true;
            pricebookEntry.UseStandardPrice = false;
            stdPriceBookEntryList.add(pricebookEntry);  
        }
        
        insert stdPriceBookEntryList;
        insert priceBookEntryList;
        
        System.debug('priceBookEntryList'+priceBookEntryList);
        return priceBookEntryList;
    }
    
    public static WorkOrder createTestParentWorkOrders(Id accountId, String ServiceContractNumber){
        System.debug('SAYALI LOG -- ServiceContractNumber-->'+ServiceContractNumber);
        Id parentRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.WO_PARENT).getRecordTypeId();
        List<WorkOrder> workOrderList = new List<WorkOrder>();
        WorkOrder workOrder = new WorkOrder();
        workOrder.Work_Area__c = 'Urban';
        workOrder.RecordTypeId=parentRecordTypeId;//Added by Sindhu
        workOrder.SAM_ID__c='3RCM-20';
        //workOrder.WorkTypeId = workTypeId;
        //workOrder.ServiceTerritoryId = serviceTerritoryId;
        workOrder.NBN_Activity_Status_Info__c = 'Test';
        workOrder.city = 'City';
        workOrder.country ='Australia';
        workOrder.state='NSW';
        workOrder.street ='Street1';
        workOrder.PostalCode = '2132';
        workOrder.CorrelationId__c ='COR100';
        workOrder.Client_Work_Order_Number__c  ='WOR100';
        workOrder.Work_Area__c ='Urban';
        //workOrder.ServiceContractId = serviceContactId;
        workOrder.NBN_Activity_State_Id__c='Assigned';
        workOrder.Service_Contract_Number__c=ServiceContractNumber;
        workOrder.Classification__c = 'HFC';
        workOrder.Primary_Access_Technology__c = 'HFC';
        workOrder.NBN_Activity_Start_Date_Time__c = System.now();
        workOrder.NBN_Parent_Work_Order_Id__c = 'AJGHKITOI33';
        workOrder.Based_Revision_Number__c  = '-1';
        //workOrder.Pricebook2Id = priceBookId;
        workOrder.NBN_Field_Work_Specified_By_Id__c = 'RemediationWOS';
        workOrder.SA_Status__c ='Assigned';

        insert workOrder;

        
        return workOrder;
    }
    public static  ServiceAppointment createTestServiceAppointment(Id workOrderId){
        Id cuiRecordTypeId = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByName().get('CUI').getRecordTypeId();
        List<ServiceAppointment> serviceAppointmentList = new List<ServiceAppointment>();
        
            
            ServiceAppointment sa1 = new ServiceAppointment();
            sa1.ParentRecordId = workorderId;
            sa1.EarliestStartTime = System.today().addDays(3);
            sa1.DueDate = System.today().addDays(3);
            sa1.SchedStartTime = System.today();
            sa1.SchedEndTime = System.today().addDays(1);
            sa1.Status = 'None';
            sa1.RecordTypeId = cuiRecordTypeId;
            serviceAppointmentList.add(sa1);
        
        
        insert serviceAppointmentList;
        return serviceAppointmentList[0];
    }
}