@isTest
public class LogEntryTriggerhandlerTest {
    
    @testSetup static void setup() {
        BSA_TestDataFactory.createSeedDataForTesting();  
        
    }
    
    static testMethod void testPlatformEvent(){
        WorkOrder workOrderObj = [SELECT Id FROM WorkOrder limit 1];
        BSA_TestDataFactory.createSubmitNotes(workOrderObj.id);
    }
}