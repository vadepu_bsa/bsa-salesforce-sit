public class ParseWOHistory {

    @InvocableMethod(label='ParseNBNWOHistory' description='Updates NBN Onsite and Completion Dates on the WO')
    public static void ParseNBNWOHistory(List<String> WOIds){
        
        String inputParamWOId;
        Datetime NBNWOOnsiteStatusUpdateDate;
        Datetime NBNWOCompleteStatusUpdateDate;
        Boolean recordFirstOnsiteUpdate = true;
         
        if(WOIds.size() > 0){
        	System.debug(WOIds.get(0));
            inputParamWOId = WOIds.get(0);
            for( WorkOrderHistory woh : [SELECT Id, WorkOrderId, Field, OldValue, NewValue, 
                                        CreatedById, CreatedDate, DataType FROM WorkOrderHistory 
                                        where Field = 'NBN_Activity_State_Id__c' and 
                                        WorkOrderId =: inputParamWOId
                                        order by createdDate asc]){
                if(woh.NewValue == 'ACKNOWLEDGED:Step_SDP_Tech_On_Site' && recordFirstOnsiteUpdate){recordFirstOnsiteUpdate = false;NBNWOOnsiteStatusUpdateDate = woh.CreatedDate;}
                       
                
            	if(woh.NewValue == 'COMP:Step_NBN_Complete_Activity'){NBNWOCompleteStatusUpdateDate = woh.CreatedDate;}
                                                        
			}
            System.debug('First OnsiteDate' + NBNWOOnsiteStatusUpdateDate);
            System.debug('Last CompletionDate' + NBNWOCompleteStatusUpdateDate);
            
            if(NBNWOOnsiteStatusUpdateDate != null || NBNWOCompleteStatusUpdateDate != null ){List<WorkOrder> workOrders = [select Id, WorkOrderNumber,NBN_WorkOrder_Onsite_Time__c,NBN_WorkOrder_Completion_Time__c from WorkOrder where Id = :inputParamWOId];List<WorkOrder> workOrdersToUpdate = new List<WorkOrder>();for(WorkOrder wo: workOrders){wo.NBN_WorkOrder_Onsite_Time__c = NBNWOOnsiteStatusUpdateDate;wo.NBN_WorkOrder_Completion_Time__c = NBNWOCompleteStatusUpdateDate;workOrdersToUpdate.add(wo);
                }
                update workOrdersToUpdate;
            }
        }
   
	}
}