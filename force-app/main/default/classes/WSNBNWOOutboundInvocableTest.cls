@isTest
public class WSNBNWOOutboundInvocableTest {
	@testSetup static void setup() {
        List<WorkOrder> woList = new List<WorkOrder>();
        BSA_TestDataFactory.createSetupDataForWSNBNOutboundTesting();
        
    }
	static testMethod void validateDoAction() {
        Test.startTest();
        
        WorkOrder wo = [Select Id from WorkOrder where ParentWorkOrderId = null AND Client_Work_Order_Number__c != '' limit 1];
        Work_Order_Contact__c woc = [Select Id from Work_Order_Contact__c where work_order__c =:wo.Id Limit 1];
        WorkOrderLineItem woli = [Select Id from WorkOrderLineItem where Subject = 'Document Completed' Limit 1]; 
        NBN_Log_Entry__c note = [Select Id from NBN_Log_Entry__c where work_order__c =: wo.Id Limit 1];
        Work_Order_Task__c wot = [Select Id from Work_Order_Task__c where work_order__c =: wo.Id Limit 1];
        
        //String actionName, String specification, Id workOrderId
        callAction('update_confidence_level', 'GeneralWOS', wo.Id);
        callAction('suspend', 'GeneralWOS', wo.Id);
        callAction('complete', 'RemediationWOS', wo.Id);
        callAction('incomplete', 'GeneralWOS', wo.Id);
        callAction('update_schedule', 'GeneralWOS', wo.Id);
        callAction('update_prd', 'GeneralWOS', wo.Id);
        callAction('add_contact', 'GreenfieldPreInstallWOS', woc.Id);
        callAction('documentation_complete', 'GeneralWOS', woli.Id);
        callAction('add_notes', 'GeneralWOS', note.Id);
        callAction('update_task', 'GeneralWOS', wot.Id);
        
        Test.stopTest();
    }
    
    static void callAction(String actionName, String specification, Id recordId) {
        List<WSNBNWOOutboundInvocable.WSNBNWOOutboundInputs> inputList = new List<WSNBNWOOutboundInvocable.WSNBNWOOutboundInputs>();
        WSNBNWOOutboundInvocable.WSNBNWOOutboundInputs input = new WSNBNWOOutboundInvocable.WSNBNWOOutboundInputs();
        input.action = actionName;
        input.clientWorkOrdrNumber = 'WOR12123';
        input.spefication = specification;
        input.Id = recordId;
        
        inputList.add(input);
        
        WSNBNWOOutboundInvocable.doAction(inputList);
    }
    
}