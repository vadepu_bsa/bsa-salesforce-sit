public class woEntitlementController {
    
    
    
     @AuraEnabled
    public static  boolean entitlementAccess(string workorderId) {
        boolean HasAccess = true;
        // system.debug('Mahmood: entitlementAccess fired');
        UserRecordAccess urAccessObj = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =: Userinfo.getUserId() AND RecordId =:workorderId];
        if (urAccessObj.HasEditAccess == false){
            HasAccess = false;
        }
        if (!Schema.sObjectType.WorkOrder.fields.EntitlementId.isUpdateable()) {
             HasAccess = false;
		}
        return HasAccess;
    }
    
    @AuraEnabled
    public static  Entitlement fetchEntitlmentName(string workorderId) {
        string id = '';
         Entitlement eobj = new  Entitlement();
        List<workorder> WorkorderList  = [Select Id , Entitlement.name , EntitlementId FROM WorkOrder WHERE Id =:workorderId LIMIT 1];
        if (WorkorderList.size() >0){
            if (WorkorderList[0].EntitlementId != null){
               id = WorkorderList[0].EntitlementId;
            }
            if (id.length() > 2){
            	eobj = [SELECT ID, Name, Type, Status,AccountId, Account.Name , Account.RecordType.Name, Account.Type  , StartDate, EndDate, slaProcess.Name  
                    	       , Service_Territory__r.Name, Work_Type__c, Work_Type__r.Name
                    	FROM Entitlement 
                     	WHERE ID =: id];
            }
        }
       
        // system.debug('eobj =' + eobj);
        return eobj;
    }
     
    @AuraEnabled
    public static Entitlement updateEntitlmentName(string entitlementId, string workorderId) {
    //    system.debug('!!!Mahmood entitlementId = '+ entitlementId);
    //    system.debug('!!!Mahmood workorderid = '+ workorderId);
        workorder wObj = new WorkOrder();
        wObj.id = workorderId;
        wObj.entitlementId = entitlementId;
        
        update wObj;
        
        Entitlement eobj = [SELECT ID, Name, Type, Status,AccountId, Account.Name , Account.RecordType.Name, Account.Type  , StartDate, EndDate, slaProcess.Name  , Service_Territory__r.Name  FROM Entitlement 
                            WHERE ID =: entitlementId];
        return eobj;
        
    }
    
    
    @AuraEnabled
    public static list<Entitlement> FetchEntitlements (string workorderId)   {
        List<workorder> WorkorderList  = [Select Id , AccountId, Account.ParentId, ServiceTerritoryId, WorkTypeId from WorkOrder Where Id =:workorderId];
        set<id> AccountIdset = new set<Id>();
        set<id> serviceTerritoryIdset = new set<Id>();
        for (Integer i = 0; i < WorkorderList.size(); i++ ){
            if (WorkorderList[i].AccountId != null){
                AccountIdset.add(WorkorderList[i].AccountId);
            }
            if (WorkorderList[i].Account.ParentId != null){
                AccountIdset.add(WorkorderList[i].Account.ParentId);
            }
            if (WorkorderList[i].ServiceTerritoryId != null){
                serviceTerritoryIdset.add(WorkorderList[i].ServiceTerritoryId);
            }
            
        }
     
        List<Entitlement> entitlementList = [SELECT ID, Name, Type, Status,AccountId, Account.Name , Account.RecordType.Name, Account.Type  
                                             		,StartDate, EndDate, SLAProcess.Name  , Service_Territory__r.Name, Work_Type__c, Work_Type__r.Name
                                             FROM Entitlement 
                                             WHERE  Status ='Active' AND Work_Type__c =: WorkorderList[0].WorkTypeId AND   (AccountId =:AccountIdset
                                             OR Service_Territory__c =: serviceTerritoryIdset) Order By SLAProcess.Name];
        // System.debug('!!! entitlementList = ' +entitlementList);
        Map<id,List<Entitlement>> entitlementIdMap = new Map<id,List<Entitlement>>();
        List<Entitlement> entitlementReturnList = new List<Entitlement>();
         for (Integer i = 0; i < entitlementList.size(); i++ ){
          /*  System.debug('!!! entitlementList = ' +entitlementList[i]);
            // System.debug('!!! entitlementList = ' +entitlementList[i].Account.Name + ' Type ' +  entitlementList[i].Account.RecordType.Name);
            // System.debug('!!! entitlementList = ' +entitlementList[i].SLAProcess.Name);
            if ( WorkorderList[0].AccountId != null && entitlementList[i].AccountId == WorkorderList[0].AccountId){
                // system.debug('site found');
                entitlementReturnList.add(entitlementList[i]);
       
            } else if (entitlementReturnList.size() == 0 &&  WorkorderList[0].Account.ParentId != null && entitlementList[i].AccountId == WorkorderList[0].Account.ParentId){
                //  system.debug('customer found');
                entitlementReturnList.add(entitlementList[i]);
            } else if (entitlementReturnList.size() == 0 &&  WorkorderList[0].ServiceTerritoryId != null && entitlementList[i].Service_Territory__c == WorkorderList[0].ServiceTerritoryId){
                //   system.debug('ST found');
                entitlementReturnList.add(entitlementList[i]);
            }*/
            if (!entitlementIdMap.ContainsKey(entitlementList[i].AccountId)){
			     entitlementIdMap.put(entitlementList[i].AccountId, new List<Entitlement>{entitlementList[i]});
			}else {
			     entitlementIdMap.get(entitlementList[i].AccountId).add(entitlementList[i]);
			}
			 if (!entitlementIdMap.ContainsKey(entitlementList[i].Service_Territory__c)){
			     entitlementIdMap.put(entitlementList[i].Service_Territory__c, new List<Entitlement>{entitlementList[i]});
			}else {
			     entitlementIdMap.get(entitlementList[i].Service_Territory__c).add(entitlementList[i]);
			}
        }
        if (WorkorderList[0].AccountId != null && entitlementIdMap.containsKey(WorkorderList[0].AccountId)){
             entitlementReturnList =   entitlementIdMap.get(WorkorderList[0].AccountId) ;         
        }
        if ( entitlementReturnList.size() == 0 && WorkorderList[0].Account.Parentid != null && entitlementIdMap.containsKey(WorkorderList[0].Account.Parentid)){
             entitlementReturnList =   entitlementIdMap.get(WorkorderList[0].Account.Parentid) ;         
        }
        if ( entitlementReturnList.size() == 0 && WorkorderList[0].ServiceTerritoryId != null && entitlementIdMap.containsKey(WorkorderList[0].ServiceTerritoryId)){
             entitlementReturnList =   entitlementIdMap.get(WorkorderList[0].ServiceTerritoryId) ;         
        }
        return entitlementReturnList;
    }
    
}