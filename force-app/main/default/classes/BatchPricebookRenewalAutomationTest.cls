@isTest
public class BatchPricebookRenewalAutomationTest {
	static testMethod void testServiceResourceTrigger(){   
		Test.startTest();
			BSA_TestDataFactory.createSeedDataForTesting(); 
        
            BatchPricebookRenewalAutomation obj = new BatchPricebookRenewalAutomation();
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}