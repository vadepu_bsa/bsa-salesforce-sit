/*
*  @description apex controller for APS_WorkOrderGenerator.cmp for JIRA @FSL3-32
*
*  @author      Bohao Chen
*  @date 		2020-06-18
*/
public with sharing class APS_WorkOrderGeneratorCtr {
                                                                            
    // private List<String> serviceTerritoryIds {get;set;}
    private List<String> servicePeriods {get;set;}
    private List<String> customerIds {get;set;}
    private List<String> siteIds {get;set;}
    private List<String> equipmentTypeIds {get;set;}
    private Date startDate {get;set;}
    private Date endDate {get;set;}
    private Boolean contractedChk {get;set;}
    private Boolean nonContractedChk {get;set;}

    public static Map<String, Schema.RecordTypeInfo> woRecordTypeMap {
        get {
            if(woRecordTypeMap == null) {
                woRecordTypeMap = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName();
            }
            return woRecordTypeMap;
        }
        private set;
    }
    public static Map<String, Schema.RecordTypeInfo> woliRecordTypeMap {
        get {
            if(woliRecordTypeMap == null) {
                woliRecordTypeMap = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByDeveloperName();
            }
            return woliRecordTypeMap;
        }
        private set;
    }

    public APS_WorkOrderGeneratorCtr(String servicePeriodsJSON, String customersJSON, String sitesJSON, String equipmentTypesJSON, 
                                        String startDueDate, String endDueDate, Boolean contractedChk, Boolean nonContractedChk) {

        // List<Id> MaintenancePlanIds = new List<Id>();

        if(String.isNotBlank(servicePeriodsJSON)) {
            this.servicePeriods = (List<String>)JSON.deserialize(servicePeriodsJSON, Type.forName('List<String>'));
        }
        
        if(String.isNotBlank(sitesJSON)) {
            this.siteIds = (List<String>)JSON.deserialize(sitesJSON, Type.forName('List<String>'));
        }        

        if(String.isNotBlank(equipmentTypesJSON)) {
            this.equipmentTypeIds = (List<String>)JSON.deserialize(equipmentTypesJSON, Type.forName('List<String>'));
        }  
        
        this.startDate = Date.valueOf(startDueDate);
        this.endDate = Date.valueOf(endDueDate);

        this.contractedChk = contractedChk; 
        this.nonContractedChk = nonContractedChk;
    }

    // generate work order line items for assets in the selected equipment groups of the selected sites
    public List<EquipmentGroup> generateEquipmentGroups() {

        List<EquipmentGroup> equipmentGroupWrappers = new List<EquipmentGroup>();

        List<String> lstSiteIds = new List<String>();
        lstSiteIds.addAll(this.siteIds);

        List<String> lstEquipmentTypeIds = new List<String>();
        lstEquipmentTypeIds.addAll(this.equipmentTypeIds);

        //@FSL3-118
        List<Asset> equipmentGroups = [SELECT Name, AccountId, Trade__c, Skill_Group__c, Account.Include_New_Assets_in_PM_Routine__c, Technician__c
                                        FROM Asset WHERE RecordType.DeveloperName = 'Equipment_Group'
                                        AND AccountId IN: lstSiteIds
                                        AND Equipment_Type__c IN: lstEquipmentTypeIds];


        String maQuery = 'SELECT AssetId, MaintenancePlanId, MaintenancePlan.Frequency, Asset.Skill_Group__c, Contracted__c, '
                        + 'MaintenancePlan.FrequencyType, Maintenance_Routine_Global__c,MaintenancePlan.EndDate, MaintenancePlan.MaintenanceWindowStartDays, '
                        + 'MaintenancePlan.Technician__c, WorkTypeId, WorkType.Name, NextSuggestedMaintenanceDate '
                        + 'FROM MaintenanceAsset '
                        + 'WHERE WorkTypeId != null AND AssetId IN: equipmentGroups ';
        //@FSL3-456 - David A (IBM) 
        if (this.contractedChk != this.nonContractedChk) {
            if (!this.contractedChk && this.nonContractedChk){
                maQuery += 'AND Contracted__c = false ';
            } else {
                maQuery += 'AND Contracted__c = true ';
            }
        }                           

       // FR-190. Start
        List<MaintenanceAsset> allMaintenanceAssets = Database.query(maQuery);
        List<MaintenanceAsset> maintenanceAssets = new List<MaintenanceAsset>();
        for(MaintenanceAsset masset : allMaintenanceAssets){
            if(masset.MaintenancePlan.EndDate != null && (masset.NextSuggestedMaintenanceDate <= masset.MaintenancePlan.EndDate)){
                maintenanceAssets.add(masset); 
            }
            else if (masset.MaintenancePlan.EndDate == null){
                maintenanceAssets.add(masset);
            }
        }
        // FR-190. End

        Map<Id, List<MaintenanceAsset>> maintenanceAssetsByEgId = new Map<Id, List<MaintenanceAsset>>();
        for(MaintenanceAsset ma : maintenanceAssets) {
            if(!maintenanceAssetsByEgId.containsKey(ma.AssetId)) {
                maintenanceAssetsByEgId.put(ma.AssetId, new List<MaintenanceAsset>());    
            }
            maintenanceAssetsByEgId.get(ma.AssetId).add(ma);
        }
       //System.debug('@maintenanceAssetsByEgId: ' + maintenanceAssetsByEgId);

        // fetch techs for equipment groups from preferred resources assigned to a equipment group or those assigned to the PM routine (maintenance plan) or the site
        Map<Id, Map<Id, Id>> techIdByMaIdByEgId = findTechs(equipmentGroups, maintenanceAssetsByEgId);

        Map<Id, Account> accountById = new Map<Id, Account>([SELECT Name, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, Service_Territory__c,
                                                                Service_Territory__r.Name, Contract_Number__c, Parent.CustomerCode__c
                                                            FROM Account 
                                                            WHERE Id IN: this.siteIds]);

        Integer size = equipmentGroups.size(); 
	
	    for(Integer i = 0; i < size; i++) {
            Asset eg = equipmentGroups[i];
            if(maintenanceAssetsByEgId.containsKey(eg.Id)) {
                List<MaintenanceAsset> mas = maintenanceAssetsByEgId.get(eg.Id);
                Integer masSize = mas.size();
                for(Integer j = 0; j < masSize; j++){ 
                    MaintenanceAsset ma = mas[j];

                    if(servicePeriods.contains(ma.Maintenance_Routine_Global__c) && ma.NextSuggestedMaintenanceDate >= this.startDate && ma.NextSuggestedMaintenanceDate <= this.endDate) {
                        // for HVAC, it creates a work order even though the equipment group has no asset @FSL3-223
                        // for Fire, it creates a work order even though the equipment group has no asset @FSL3-119
                        EquipmentGroup equipmentGroup = new EquipmentGroup();
                        equipmentGroup.dueDate = ma.NextSuggestedMaintenanceDate; 
                        equipmentGroup.skillGroup = ma.Asset.Skill_Group__c;
                        equipmentGroup.workTypeId = ma.WorkTypeId;
                        equipmentGroup.workTypeName = ma.WorkType.Name;
                        equipmentGroup.site = accountById.containsKey(eg.AccountId) ? accountById.get(eg.AccountId) : null;
                        equipmentGroup.maintenanceAssetId = ma.Id;
                        equipmentGroup.maintenancePlanId = ma.MaintenancePlanId;
                        equipmentGroup.maintenancePlanIds.add(ma.MaintenancePlanId);
                        equipmentGroup.maintenanceRoutine = ma.Maintenance_Routine_Global__c;
                        equipmentGroup.equipmentGroup = eg;
                        equipmentGroup.department = eg.Trade__c;
                        if(techIdByMaIdByEgId.containsKey(eg.Id) && techIdByMaIdByEgId.get(eg.Id).containsKey(ma.Id)) {
                            equipmentGroup.techId = techIdByMaIdByEgId.get(eg.Id).get(ma.Id); // get tech from PM routine, Equipment Group, or site
                        }
                        equipmentGroupWrappers.add(equipmentGroup);
                    }
                }
            }
        }

        return equipmentGroupWrappers;
    }

    // group work order line items by asset id, due date, skill group, work type and site
    public List<WorkOrderWrapper> groupEquipmentGroups(List<EquipmentGroup> equipmentGroupWrappers) {

        List<WorkOrderWrapper> workOrders = new List<WorkOrderWrapper>();

        Map<String, Map<Id, EquipmentGroup>> equipmentGroupByAssetIdByKey = new Map<String, Map<Id, EquipmentGroup>>();

        Integer workOrderIndex = 0;

        // @FSL3-118
        Map<String, Decimal> maintenanceRoutineHierarchy = new Map<String, Decimal>();
        for(Maintenance_Routine_Hierarchy__mdt mrh : [SELECT MasterLabel, Hierarchical_Order__c FROM Maintenance_Routine_Hierarchy__mdt]) {
            maintenanceRoutineHierarchy.put(mrh.MasterLabel, mrh.Hierarchical_Order__c);
        }
        
        Map<Id, ServiceResource> preferredEngineerNamesById = new Map<Id, ServiceResource>();

        Integer woliSize = equipmentGroupWrappers.size(); 
	
        for(Integer i = 0; i < woliSize; i++){ 
            EquipmentGroup equipmentGroup = equipmentGroupWrappers[i];

            String key = equipmentGroup.site.Id + '&' + equipmentGroup.workTypeId + '&' + equipmentGroup.skillGroup + '&' + equipmentGroup.dueDate + '&' + equipmentGroup.techId;
           //System.debug('@key: ' + key);
            if(!equipmentGroupByAssetIdByKey.containsKey(key)) {
                equipmentGroupByAssetIdByKey.put(key, new Map<Id, EquipmentGroup>());
            }

            String egId = equipmentGroup.equipmentGroup.Id;
            if(!equipmentGroupByAssetIdByKey.get(key).containsKey(egId)) {
                equipmentGroupByAssetIdByKey.get(key).put(egId, equipmentGroup);
            }
            else {
                EquipmentGroup existingEquipmentGroup = equipmentGroupByAssetIdByKey.get(key).get(egId);
                //System.debug('@existingWoliWrapper routine: ' + existingWoliWrapper.maintenanceRoutine);
                //System.debug('@woliWrapper routine: ' + woliWrapper.maintenanceRoutine);

                // concatenate existing maintenance plan id with the new maintenance plan id 
                // so that we can update NextSuggestedMaintenanceDate of all related maintenance plan of that equipment group to a new date in the next cycle.
                // The reason of doing this is because we want to know how many maintenance plans need to be updated if they have the same NextSuggestedMaintenanceDate
                // because we only generate one asset woli for multiple maintenance plans witht the same NextSuggestedMaintenanceDate.
                // e.g. (Quarterly maintenance plan and Monthly maintenance plan have the same NextSuggestedMaintenanceDate)
                if(maintenanceRoutineHierarchy.get(equipmentGroup.maintenanceRoutine) != maintenanceRoutineHierarchy.get(existingEquipmentGroup.maintenanceRoutine)) {
                    // @FSL3-309
                    List<String> tempMpIds = new List<String>();
                    tempMpIds.addAll(existingEquipmentGroup.maintenancePlanIds);
                    tempMpIds.addAll(equipmentGroup.maintenancePlanIds);

                    existingEquipmentGroup.maintenancePlanIds = tempMpIds; // Sync maintenance ids
                    equipmentGroup.maintenancePlanIds = tempMpIds; // Sync maintenance ids
                }

                // compare the maintenance routine here in order to assign the correct asset tasks which also depends on the maintenance routine later on
                // routine hierarchy (Lowest) Monthly -> Bi-Monthly-> Quarterly	-> Six-Monthly-> Yearly	-> Bi-Annual-> 5 Yearly (Highest)
                if(maintenanceRoutineHierarchy.get(equipmentGroup.maintenanceRoutine) > maintenanceRoutineHierarchy.get(existingEquipmentGroup.maintenanceRoutine)) {
                    equipmentGroupByAssetIdByKey.get(key).put(egId, equipmentGroup);
                }
            }

            preferredEngineerNamesById.put(equipmentGroup.techId, null);
        }
    
       //System.debug('@woliWrapperByAssetIdByKey: ' + woliWrapperByAssetIdByKey);

        if(!preferredEngineerNamesById.isEmpty()) {
            preferredEngineerNamesById = new Map<Id, ServiceResource>([SELECT Id, Name FROM ServiceResource WHERE Id IN: preferredEngineerNamesById.keySet()]);
        }

        for(String key : equipmentGroupByAssetIdByKey.keySet()) {
           //System.debug('@key: ' + key);
            List<String> keyElements = key.split('&');
           //System.debug('@keyElements ' + keyElements);
            String techId = keyElements[4] == 'null' ? '' : keyElements[4];
            String workTypeId = keyElements[1];

            // create a new work order
            WorkOrder wo = new WorkOrder();
            wo.Row_Number__c = workOrderIndex++;
            // @FSL3-384
            wo.Maintenance_Asset_Ids__c = '';
            if(String.isNotBlank(techId)) {
                wo.Service_Resource__c = techId; // @FSL3-119
                if(preferredEngineerNamesById.get(techId) != null) {
                    wo.Preferred_Engineer__c = preferredEngineerNamesById.get(techId).Name;
                }
            }
            workOrderWrapper woWrapper = new workOrderWrapper();                        
            woWrapper.workOrder = wo;                        
            workOrders.add(woWrapper);
            // @FSL3-236
            // Description = Equipment Group Name + Maintenance Routine + Month Year (if multiple EGs, add separate lines)
            Map<String, String> descriptionByEquipmentGroupId = new Map<String, String>();

            Set<Id> maintenanceAssetIds = new Set<Id>();

            // @FSL3-309
            Map<String, List<String>> mpIdsByEgIds = new Map<String, List<String>>();
            for(EquipmentGroup equipmentGroup : equipmentGroupByAssetIdByKey.get(key).values()) {
                
                if(equipmentGroup.maintenanceAssetId != null) {
                    maintenanceAssetIds.add(equipmentGroup.maintenanceAssetId);
                }

                // @FSL3-236
                if(!descriptionByEquipmentGroupId.containsKey(equipmentGroup.equipmentGroup.Id)) {
                    descriptionByEquipmentGroupId.put(equipmentGroup.equipmentGroup.Id, equipmentGroup.equipmentGroup.Name + ' | ' + equipmentGroup.maintenanceRoutine + ' | ' + 
                                                        equipmentGroup.dueDate.day() + '/' + equipmentGroup.dueDate.month() + '/' + equipmentGroup.dueDate.year());
                }

                if(!mpIdsByEgIds.containsKey(equipmentGroup.equipmentGroup.Id)) {
                    mpIdsByEgIds.put(equipmentGroup.equipmentGroup.Id, new List<String>());
                }
                
                mpIdsByEgIds.get(equipmentGroup.equipmentGroup.Id).addAll(equipmentGroup.maintenancePlanIds);
                
                wo.WorkTypeId = workTypeId;
                wo.RecordTypeId = woRecordTypeMap.get('APS_Work_Order').getRecordTypeId();
                wo.Work_Type__c = equipmentGroup.workTypeName;

                if(equipmentGroup.site != null) {
                    wo.AccountId = equipmentGroup.site.Id;
                    wo.Site_Name__c = equipmentGroup.site.Name;
                    wo.Street = equipmentGroup.site.ShippingStreet;
                    wo.City = equipmentGroup.site.ShippingCity;
                    wo.State = equipmentGroup.site.ShippingState;
                    wo.PostalCode = equipmentGroup.site.ShippingPostalCode;
                    wo.Country = equipmentGroup.site.ShippingCountry;
                    wo.ServiceTerritoryId = equipmentGroup.site.Service_Territory__c;
                    wo.Service_Territory__c = equipmentGroup.site.Service_Territory__r.Name;
                    wo.Contract_Number__c = equipmentGroup.site.Contract_Number__c;
                    wo.Customer_Code__c = equipmentGroup.site.Parent.CustomerCode__c;
                }

                wo.SuggestedMaintenanceDate = equipmentGroup.dueDate;
                wo.MaintenancePlanId = equipmentGroup.maintenancePlanId;
                wo.Maintenance_Plan__c = equipmentGroup.maintenanceRoutine;                            
                wo.Skill_Group__c = equipmentGroup.skillGroup;
                
                // @FSL3-384
                wo.Maintenance_Asset_Ids__c = '';
                
                for(String maintenanceAssetId : maintenanceAssetIds) {
                    wo.Maintenance_Asset_Ids__c += maintenanceAssetId + ',';
                }
                wo.Maintenance_Asset_Ids__c = wo.Maintenance_Asset_Ids__c.removeEnd(',');

                // @FSL3-236
                //System.debug('@descriptionByEquipmentGroupId: ' + descriptionByEquipmentGroupId);
                wo.Description = '';
                if(!descriptionByEquipmentGroupId.isEmpty()) {
                    for(String description : descriptionByEquipmentGroupId.values()) {
                        wo.Description += description + '\n';
                    }
                }

                if(String.isNotBlank(wo.Description)) {
                    wo.Description = wo.Description.removeEnd('\n');
                }
            }
            // serialise mpIdsByEgIds in order to update the next suggested maintenance date on the maintenance asset of equipment groups in the asychonous call
            wo.EGs_wt_MPs__c = JSON.serialize(mpIdsByEgIds);
        }

        return workOrders;
    }
    
    @AuraEnabled
    public static void approveOrDownloadWorkOrders(String guid, String workOrdersJSON, String selectedServiceTerritories, String dueDateFrom, 
                                        String dueDateTo, String contracted, String noncontracted, String selectedServicePeriods,
                                        String selectedCustomers, String selectedSites, String selectedEquipmentTypes, String actionType) {

        
        if(String.isBlank(guid)) {
            guid = APS_WorkOrderGeneratorHelper.getGUID();
        }
        // System.debug('@guid: ' + guid);
        // System.debug('@workOrdersJSON: ' + workOrdersJSON);
        List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrderWrappers = (List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper>)JSON.deserialize(workOrdersJSON, List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper>.class);
        
        // prepare work orders
        List<WorkOrder> workOrders = new List<WorkOrder>();
        for(APS_WorkOrderGeneratorCtr.WorkOrderWrapper workOrderWrapper: workOrderWrappers) {
            workOrders.add(workOrderWrapper.workOrder);
        }

        String userSelectionSettingsJSON = APS_WorkOrderGeneratorHelper.convertUserSettingsToJSON(selectedServiceTerritories, dueDateFrom, dueDateTo, contracted, noncontracted, selectedServicePeriods, selectedCustomers, selectedSites, selectedEquipmentTypes);
        Id draftWorkOrderId = APS_WorkOrderGeneratorHelper.upsertDraftWorkOrder(guid, userSelectionSettingsJSON, workOrders.size());
        
        // time to run work order creation batch to insert work orders into the system
        if(!workOrders.isEmpty()) {
            if(actionType == 'approveWorkOrders') {
                for(WorkOrder wo : workOrders) {
                    wo.Draft_Work_Order__c = draftWorkOrderId;
                }
            }
            APS_WorkOrderCreationBatch woCreationBatch = new APS_WorkOrderCreationBatch(workOrders, draftWorkOrderId, guid, actionType);
            ID batchProcessId = Database.executeBatch(woCreationBatch, APS_WorkOrderGeneratorHelper.findBatchSize('APS_WorkOrderCreationBatch'));
        }
    }

    // @FSL3-119
    // This method is used to find technician based on the preferred resourse on equipment groups and the preferred resourse on their maintenance plans
    @TestVisible
    private static Map<Id, Map<Id, Id>> findTechs(List<Asset> equipmentGroups, Map<Id, List<MaintenanceAsset>> maintenanceAssetsByEgId) {
        
        // prepare tech for equipment groups from preferred resources assigned to the equipment group or those assigned to the PM routine (maintenance plan)
        Map<Id, Map<Id, Id>> techIdByMaIdByEgId = new Map<Id, Map<Id, Id>>();
        List<Asset> equipmentGroupsWithoutTech = new List<Asset>();

        // filter out techIdByMaIdByEgId without tech id
        // only run the logic to get site tech when a tech hasn't been found for the eg so far
        // looping through techIdByMaIdByEgId to check if all equipment groups have tech Id value for all maintenance routines (PM routines)
        // if tech id is null, it needs to find a tech from the site based on the EG skill group
        // Map<Id, Map<String, Id>> techIdBySkillGroupBySiteId = new Map<Id, Map<String, Id>>();
        Set<String> skillGroups = new Set<String>();
        Set<Id> siteIds = new Set<Id>();

        Integer egSize = equipmentGroups.size(); 	
	    for(Integer i = 0; i < egSize; i++){ 
        // for(Asset eg : equipmentGroups) {
            Asset eg = equipmentGroups[i];

            if(!techIdByMaIdByEgId.containsKey(eg.Id)) {
                techIdByMaIdByEgId.put(eg.Id, new Map<Id, Id>());
            }
            
            // tech on the equipment group
            Id egTechId = eg.Technician__c;
                        
            if(maintenanceAssetsByEgId.containsKey(eg.Id)) {

                List<MaintenanceAsset> mas = maintenanceAssetsByEgId.get(eg.Id);
                Integer masSize = mas.size();
                for(Integer j = 0; j < masSize; j++){ 
                // for(MaintenanceAsset ma : maintenanceAssetsByEgId.get(eg.Id)) {
                    MaintenanceAsset ma = mas[j];
                    // PM routine tech can override EG tech because the tech on PM routine has higher priority than the one on EG
                    // tech on the maintenance plan
                    Id mpTechId = ma.MaintenancePlan.Technician__c;
                    
                    Id techId = mpTechId != null ? mpTechId : egTechId;
                    if(techId != null) {
                        techIdByMaIdByEgId.get(eg.Id).put(ma.Id, techId); 
                    }
                    else if(eg.Skill_Group__c != null) {
                        siteIds.add(eg.AccountId);
                        skillGroups.add(eg.Skill_Group__c);
                        equipmentGroupsWithoutTech.add(eg);
                    }
                }
            }
        }

        // There would be multiple technicians assigned to the site 
        // but we would expect to match Skill from the equipment group to skill on the technician and then use the first preferred Engineer in the list that has the appropriate skill,
        // We only need to show one engineer.
        if(!siteIds.isEmpty() && !skillGroups.isEmpty()) {
            Set<Id> techIds = new Set<Id>();
            Map<Id, Set<Id>> techIdsBySiteId = new Map<Id, Set<Id>>();
            Map<Id, Set<String>> skillsByTechId = new Map<Id, Set<String>>();

            // get resource preferences for sites
            List<ResourcePreference> rps = [SELECT ServiceResourceId, RelatedRecordId 
                                            FROM ResourcePreference 
                                            WHERE RelatedRecordId IN: siteIds];

            for(ResourcePreference rp : rps) {
                if(!techIdsBySiteId.containsKey(rp.RelatedRecordId)) {
                    techIdsBySiteId.put(rp.RelatedRecordId, new Set<Id>());
                }

                techIdsBySiteId.get(rp.RelatedRecordId).add(rp.ServiceResourceId);
                techIds.add(rp.ServiceResourceId);
            }
           //System.debug('@techIds: ' + techIds);
           //System.debug('@techIdsBySiteId: ' + techIdsBySiteId);

            // get skill groups for site resource preferences
            List<ServiceResourceSkill> srss = [SELECT Skill.DeveloperName, ServiceResourceId
                                               FROM ServiceResourceSkill 
                                               WHERE Skill.DeveloperName IN: skillGroups
                                               AND ServiceResourceId IN: techIds];
            
            for(ServiceResourceSkill srs : srss) {
                if(!skillsByTechId.containsKey(srs.ServiceResourceId)) {
                    skillsByTechId.put(srs.ServiceResourceId, new Set<String>());
                }

                skillsByTechId.get(srs.ServiceResourceId).add(srs.Skill.DeveloperName);
            }
           //System.debug('@skillsByTechId: ' + skillsByTechId);

            for(Asset eg : equipmentGroupsWithoutTech) {                
                for(MaintenanceAsset ma : maintenanceAssetsByEgId.get(eg.Id)) {
                    if(techIdByMaIdByEgId.get(eg.Id).get(ma.Id) == null && techIdsBySiteId.containsKey(eg.AccountId)) {

                        Set<Id> siteTechIds = techIdsBySiteId.get(eg.AccountId);
                        String skillGroup = eg.Skill_Group__c;

                        for(Id siteTechId : siteTechIds) {
                           //System.debug('@siteTechId: ' + siteTechId);
                           //System.debug('@skillGroup: ' + skillGroup);
                            if(!skillsByTechId.isEmpty() && skillsByTechId.keySet().contains(siteTechId) && skillsByTechId.get(siteTechId).contains(skillGroup)) {
                                techIdByMaIdByEgId.get(eg.Id).put(ma.Id, siteTechId);
                            }
                        }
                    }
                }
            } 
        }

        return techIdByMaIdByEgId;
    }

    @AuraEnabled
    public static String retrieveDraftJson(String guid) {
        String jsonDraft;
        try {
            List<Draft_Work_Order__c> dwo = [SELECT Selection_Details__c FROM Draft_Work_Order__c WHERE Reference_Id__c =: guid LIMIT 1];

            if(dwo.isEmpty()) {
                throw new APS_DefectController.applicationException('Couldn\'t find the saved setting for GUID: ' + guid);
            }
            else {
                jsonDraft = validateSelection(dwo[0].Selection_Details__c);
            }
        }
        catch (Exception e) {
            throw new AuraHandledException(JSON.serialize(e.getMessage()));
        }

        return jsonDraft;
    }

    //using APS_DataTablePagination.fetchRecords to retrieve records, and APS_WorkOrderGeneratorHelper.convertUserSettingsToJSON to regenerate json
    public static String validateSelection(String jsonDraft) {
        Set<String> selectedServiceTerritories = new Set<String>();
        String dueDateFrom;
        String dueDateTo;
        Boolean contracted;
        Boolean noncontracted;
        Set<String> selectedServicePeriods = new Set<String>();
        Set<String> selectedCustomers = new Set<String>();
        Set<String> selectedSites = new Set<String>();
        Set<String> selectedEquipmentTypes = new Set<String>();

        //parse json
        String field;
        JSONParser parser = JSON.createParser(jsonDraft);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                field = parser.getText();
            }
            else if (parser.getCurrentToken() != System.JSONToken.VALUE_NULL) {
                if (parser.getText() != '[' && parser.getText() != ']') {
                    if (field == 'selectedServiceTerritories') {
                        selectedServiceTerritories.add(parser.getText());
                    }
                    else if (field == 'selectedEquipmentTypes') {
                        selectedEquipmentTypes.add(parser.getText());
                    }
                    else if (field == 'selectedSites') {
                        selectedSites.add(parser.getText());
                    }
                    else if (field == 'selectedCustomers') {
                        selectedCustomers.add(parser.getText());
                    }
                    else if (field == 'selectedServicePeriods') {
                        selectedServicePeriods.add(parser.getText());
                    }
                    else if (field == 'dueDateFrom') {
                        dueDateFrom = parser.getText();
                    }
                    else if (field == 'dueDateTo') {
                        dueDateTo = parser.getText();
                    }
                    else if (field == 'contracted') {
                        contracted = parser.getBooleanValue();
                    }
                    else if (field == 'noncontracted') {
                        noncontracted = parser.getBooleanValue();
                    }
                }
            }
        }

        //service territory
        String checkedServiceTerritories;
        Set<String> ids = new Set<String>();
        List<APS_DataTablePagination.RowWrapper> lstRows = APS_DataTablePagination.fetchRecords('ServiceTerritory', 'Work_Order_Generator', 'IsActive', 'true', null, null, null, null, null, null, null, null, null, 'service_territory').lstRows;
        for (APS_DataTablePagination.RowWrapper row : lstRows) {
            if (selectedServiceTerritories.contains(row.record.Id)) {
                ids.add(row.record.Id);
            }
        }
        //overwrite the selectedServiceTerritories
        selectedServiceTerritories = ids;
        checkedServiceTerritories = JSON.serialize(selectedServiceTerritories);
        
        //date, service period, contracted/non-contracted
        String checkedServicePeriods;
        ids.clear();
        for (APS_FrequencyPicklistCtr.SelectOptionObj opt : APS_FrequencyPicklistCtr.getPicklistOptions('MaintenanceAsset','Maintenance_Routine_Global__c')) {
            if (selectedServicePeriods.contains(opt.value)) {
                ids.add(opt.value);
            }
        }
        selectedServicePeriods = ids;
        checkedServicePeriods = JSON.serialize(selectedServicePeriods);
        // System.debug('selectedServicePeriods: ' + selectedServicePeriods);
        
        //customer
        String checkedCustomers;
        ids.clear();
        lstRows = APS_DataTablePagination.fetchRecords('Account', 'Work_Order_Generator_Customer_List', 'RecordType.DeveloperName', 'Client', checkedServiceTerritories, dueDateFrom, dueDateTo, contracted, noncontracted, checkedServicePeriods, null, null, null, 'customer').lstRows;
        for (APS_DataTablePagination.RowWrapper row : lstRows) {
            if (selectedCustomers.contains(row.record.Id)) {
                ids.add(row.record.Id);
            }
        }
        selectedCustomers = ids;
        checkedCustomers = JSON.serialize(selectedCustomers);
        // System.debug('selectedCustomers: ' + selectedCustomers);

        //sites
        String checkedSites;
        ids.clear();
        lstRows = APS_DataTablePagination.fetchRecords('Account', 'Work_Order_Generator_Site_List', 'RecordType.DeveloperName', 'Site', checkedServiceTerritories, dueDateFrom, dueDateTo, contracted, noncontracted, checkedServicePeriods, checkedCustomers, null, null, 'site').lstRows;
        for (APS_DataTablePagination.RowWrapper row : lstRows) {
            if (selectedSites.contains(row.record.Id)) {
                ids.add(row.record.Id);
            }
        }
        selectedSites = ids;
        checkedSites = JSON.serialize(selectedSites);
        // System.debug('selectedSites: ' + selectedSites);

        //equipment types
        String checkedEquipmentTypes;
        ids.clear();
        lstRows = APS_DataTablePagination.fetchRecords('Asset', 'Work_Order_Generator', 'Name', 'null', checkedServiceTerritories, dueDateFrom, dueDateTo, contracted, noncontracted, checkedServicePeriods, checkedCustomers, checkedSites, null, 'equipment_type').lstRows;
        for (APS_DataTablePagination.RowWrapper row : lstRows) {
            if (selectedEquipmentTypes.contains((String)row.record.get('Equipment_Type__c'))) {
                ids.add((String)row.record.get('Equipment_Type__c'));
            }
        }
        selectedEquipmentTypes = ids;
        checkedEquipmentTypes = JSON.serialize(selectedEquipmentTypes);
        // System.debug('selectedEquipmentTypes: ' + selectedEquipmentTypes);

        String checkedDraft = APS_WorkOrderGeneratorHelper.convertUserSettingsToJSON(checkedServiceTerritories, JSON.serialize(dueDateFrom), JSON.serialize(dueDateTo), JSON.serialize(contracted), JSON.serialize(noncontracted), checkedServicePeriods, checkedCustomers, checkedSites, checkedEquipmentTypes);
        return checkedDraft;
    }

    public class SobjectFieldSet {
        @AuraEnabled
        public String sObjectName {get;set;}
        @AuraEnabled
        public List<APS_DataTablePagination.FieldsWrapper> fieldSets {
            get {
                return fieldSets == null ? new List<APS_DataTablePagination.FieldsWrapper>() : fieldSets;
            }
            set;
        }
    }

    public class WorkOrderWrapper {
        @AuraEnabled
        public Boolean isChecked;
        @AuraEnabled
        public List<WorkOrderLineItem> workOrderLineItems;
        @AuraEnabled
        public WorkOrder workOrder;
        
        public WorkOrderWrapper() {
            workOrderLineItems = new List<WorkOrderLineItem>();
            workOrder = new WorkOrder();
            isChecked = false;
        }

        public WorkOrderWrapper(WorkOrder wo) {
            workOrder = wo;
            isChecked = false;
        }
    }

    public class EquipmentGroup {
        @AuraEnabled
        public List<WorkOrderLineItem> childAssets;
        String frequency;
        Asset equipmentGroup;
        String skillGroup;
        String techId;
        Account customer;
        Account site;
        Date startDate;
        Date endDate;
        Date dueDate;
        String workTypeId;
        String workTypeName;
        String department;
        String maintenancePlanId;
        String maintenanceAssetId;        
        List<String> maintenancePlanIds;
        String maintenanceRoutine;

        public EquipmentGroup() {
            maintenancePlanIds = new List<String>();
            childAssets = new List<WorkOrderLineItem>();
        }
    }

}