/**
* @author          IBM - David Azzi
* @date            17/Aug/2020 
* @description     
*
* Change Date       Modified by         Description  
* 17/Aug/2020		David Azzi			Created Trigger Helper
**/
public class DeleteWoliAssetBatchHelper {

    public static void updateAssetStatus(List<WorkOrderLineItem> lstWOLIs){
        // System.debug('*-* updateAssetStatus  ');
        List<WorkOrderLineItem> lstWoliRemoval = new List<WorkOrderLineItem>();
        Set<Id> setWorkOrderIds = new Set<Id>();
        for(WorkOrderLineItem woli : lstWOLIs){
            if (woli.WorkOrder.StartDate > System.today()){
                if (woli.Asset.Status == 'Inactive'){
                    if (woli.WorkOrder.SuggestedMaintenanceDate != null && woli.Asset.Reactivate_Date__c != null){
                        if (woli.WorkOrder.SuggestedMaintenanceDate < woli.Asset.Reactivate_Date__c){
                            setWorkOrderIds.add(woli.WorkOrderId);
                            lstWoliRemoval.add(woli);
                        }
                    }
                } else {
                    setWorkOrderIds.add(woli.WorkOrderId);
                    lstWoliRemoval.add(woli);
                }
            }
        }

        if (lstWoliRemoval.size() > 0){
            try{
                Database.delete(lstWoliRemoval);
                populateBudgetedHoursFromEG(setWorkOrderIds);
            }catch(Exception e){
                // System.debug('*-* Exception on DeleteWoliAssetBatchHelper.updateAssetStatus:' + e.getMessage());
            }
        }
    }    


    //Copy from WOTriggerHelper, Cannot have future in Batch file
    public static void populateBudgetedHoursFromEG(Set<Id> setWorkOrderId){
        
        Map<Id, Set<Id>> msAssetIds = new Map<Id, Set<Id>>();
        Map<Id, Set<Id>> workorderMaintenanceAssetIdMaps = new Map<Id, Set<Id>>();
        map<Id, workOrder> workOrderIdMap = new Map<Id,WorkOrder>();
        
        if (setWorkOrderId.size () > 0 ){
            set<id> mainMAssetIdSet = new set<id>();
            List<Workorder> workorderprocessList = [SELECT Id, duration,  Maintenance_Asset_Ids__c FROM WorkOrder WHERE Id =: setWorkOrderId ];
            for (integer i = 0; i < workorderprocessList.size(); i++){
                // System.debug(workorderprocessList[i].Maintenance_Asset_Ids__c);
                // System.debug(workorderprocessList[i]);
                //if( workorderprocessList[i].Maintenance_Asset_Ids__c != null || workorderprocessList[i].Maintenance_Asset_Ids__c != ''){
                if (String.isNotBlank(workorderprocessList[i].Maintenance_Asset_Ids__c)){
                    set<id> massetIdset = new set<Id>();
                    if (workorderprocessList[i].Maintenance_Asset_Ids__c.contains(',')){
                        List<String> massetidsList = workorderprocessList[i].Maintenance_Asset_Ids__c.split(',');
                        for (string massetid : massetidsList ){
                            mainMAssetIdSet.add(massetid);
                            massetIdset.add(massetid);
                        }
                    } else {
                        mainMAssetIdSet.add(workorderprocessList[i].Maintenance_Asset_Ids__c);
                        massetIdset.add(workorderprocessList[i].Maintenance_Asset_Ids__c);
                    }
                    List<String> massetidsList = workorderprocessList[i].Maintenance_Asset_Ids__c.split(',');
                    
                    for (string massetid : massetidsList ){
                        mainMAssetIdSet.add(massetid);
                        massetIdset.add(massetid);
                    }
                    workorderMaintenanceAssetIdMaps.put(workorderprocessList[i].Id,massetIdset);
                }
            }   

            List<Workorder> woUpdateList = new List<WorkOrder>();
            if (mainMAssetIdSet.size() > 0){
                Map<Id, MaintenanceAsset> MaintenanceAssetMap = new  Map<Id, MaintenanceAsset>([SELECT Id, AssetId, Asset.Quantity , Per_Asset_time_for_service__c, Quoted_Value__c
                                                                                          FROM MaintenanceAsset 
                                                                                          WHERE Id IN: mainMAssetIdSet]); 

                for (id workorderId : workorderMaintenanceAssetIdMaps.keySet() ){
                    set<Id> massetidSet = workorderMaintenanceAssetIdMaps.get(workorderId);
                    decimal totalduration = 0;
                    decimal dQuotedValue = 0;
                    if (massetidSet.size() > 0){
                        for (Id MaintenanceAssetId : massetidSet){
                            decimal quantity = 0;
                            decimal perAssetTime = 0;
                            decimal individualduration = 0;
                            if (MaintenanceAssetMap.containskey(MaintenanceAssetId)){
                                if (MaintenanceAssetMap.get(MaintenanceAssetId).AssetId != null && MaintenanceAssetMap.get(MaintenanceAssetId).Asset.Quantity != null){
                                    quantity = MaintenanceAssetMap.get(MaintenanceAssetId).Asset.Quantity;
                                }
                                if (MaintenanceAssetMap.get(MaintenanceAssetId).Per_Asset_time_for_service__c != null ){
                                    perAssetTime = MaintenanceAssetMap.get(MaintenanceAssetId).Per_Asset_time_for_service__c;
                                }
                                //David A @IBM 2020-08-28 - FSL3-455
                                if (MaintenanceAssetMap.get(MaintenanceAssetId).Quoted_Value__c != null){
                                    dQuotedValue += MaintenanceAssetMap.get(MaintenanceAssetId).Quoted_Value__c;
                                }
                                
                                individualduration = quantity* perAssetTime;
                                totalduration = totalduration + individualduration;
                            }
                        }
                    }
                    woUpdateList.add(new Workorder (Id = workorderId , Duration=totalduration, Quoted_Value__c = dQuotedValue));
                }
            }

            if (woUpdateList.size () > 0){
                WorkOrderTriggerHandler.TriggerDisabled = true;
                update woUpdateList; 
            }
        }       
    }
}