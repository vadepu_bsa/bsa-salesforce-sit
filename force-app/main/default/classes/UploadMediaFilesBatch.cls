global class UploadMediaFilesBatch Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
	global String strQuery;
    public List<String> batchException = new List<String>();
    public Id woId;
    //Initialize the query string
    global UploadMediaFilesBatch(String woId){
        this.woId =woId;
        strQuery = 'SELECT Id,Media_Id__c,ContentDocumentId,WOMediaId__c from ContentVersion where WOMediaId__c =\''+woId+'\'';
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<ContentVersion> listCVs){
        //System.debug('Sayali Debug:listCVs'+listCVs[0]);
        Set<Id> contentDocIds = new Set<Id>();
        for(ContentVersion cv : listCVs){
            contentDocIds.add(cv.ContentDocumentId);
            NBNFileUploadDownloadHandler nbnHandler = New NBNFileUploadDownloadHandler(woId,'',cv);
            nbnHandler.process(Label.Get_Media_Resource_Process);
        }
        if(contentDocIds!=null && contentDocIds.size()>0){
            List<ContentDocument> lstDoc =[select id from ContentDocument where id in:contentDocIds];
            if(lstDoc!=null && lstDoc.size()>0){
                delete lstDoc;
            }
        }
        
    }
     global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exceltion list        
        if(batchException.size() > 0){
            System.debug('Sayali Debug: Batch - BatchPricebookRenewalAutomation - Finish - Exceptions - ' + batchException);
        } 
     }
}