public class LinkParentChildResponse {
    
    //@Future
    public static void linkChildResponse(){
        
        List<Response_Details__c> responseUpdateList = New List<Response_Details__c>();
        
        // Get list of response to be linked
        List<Response_Details__c>  responseList = [SELECT Parent_Response__c, Temp_Child_Link__c, Temp_Parent_Id__c, response__c 
                                                   FROM Response_Details__c 
                                                   WHERE Temp_Child_Link__c <> '' 
                                                   AND Parent_Response__c = '' AND CreatedDate = TODAY];
        System.debug('DildarLog: responseList - ' + responseList.size());
        if(responseList != Null){
            
            // Create List of parent record to be fetched
            Set<String> ParentRecordList = new Set<String>();
            for(Response_Details__c rd : responseList){
                ParentRecordList.add(rd.Temp_Child_Link__c);
            }
            System.debug('DildarLog: ParentRecordList - ' + ParentRecordList.size());
            List<Response_Details__c> parentResponseRecord = 
                [SELECT Temp_Parent_Id__c,Id FROM Response_Details__c where Temp_Parent_Id__c in :ParentRecordList];
            
            Map <String, ID> ParentChildLink = new Map<String,ID>();
            
            If(parentResponseRecord != Null){
                for(Response_Details__c rd : parentResponseRecord){
                    ParentChildLink.put(rd.Temp_Parent_Id__c, rd.id);
                }
            }
            System.debug('DildarLog: ParentChildLink - ' + ParentChildLink.size());
            for(Response_Details__c rd : responseList){
                
                // Get Parent Record using Link Field      
                ID ParentId = ParentChildLink.get(rd.Temp_Child_Link__c);
                
                //Update parent record ID
                if(ParentId != null){
                    rd.Parent_Response__c = ParentId;
                    responseUpdateList.add(rd);    
                }
            }
            System.debug('DildarLog: responseUpdateList - ' + responseUpdateList.size());
            Update responseUpdateList;
        }
    }
}