/* Author: Abhijeet Anand
* Date: 17 September, 2019
* Utiltiy class containing reusable methods to serve as a single one-stop-shop for reusable pieces of code
*/

public class UtilityClass {
    
    //Generic method to convert a string in YYYY-MM-DD format to Date YYYY-MM-DD format.
    public static Date setStringToDateFormat(String myDate) {
        
        String[] strDate = myDate.split('-');
        Integer myIntDate = Integer.valueOf(strDate[2]);
        Integer myIntMonth = Integer.valueOf(strDate[1]);
        Integer myIntYear = Integer.valueOf(strDate[0]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        System.debug('Date is '+d);
        
        return d;
        
    }
    
    //Generic method to convert a string in YYYY-MM-DDThh:mm:ss.000Z format to DateTime YYYY-MM-DD hh:mm:ss format.
    public static DateTime setStringToDateTimeFormat(String myDate) {
        
        DateTime dateOne = (DateTime)Json.deserialize('"'+myDate+'"', DateTime.class);
        
        System.debug(dateOne);
        
        String dateformat = 'yyyy-MM-dd HH:mm:ss:sssZ';
        String dtString = dateOne.format(dateformat,''+userinfo.getTimeZone().toString());
        system.debug('dtString is -->'+dtString);
        system.debug('DateTime is -->'+DateTime.valueOf(dtString));
        
        return DateTime.valueOf(dtString);
        
    }
    
    //Generic method which allows us to pass an sObject list containing all data records to be upserted for a specific sObject
    public static void dynamicUpsert(List<SObject> records, String externalIdField) {
        
        Schema.SObjectType sObjectType = records.getSObjectType();
        if (sObjectType != null) {
            String listType = 'List<' + sObjectType + '>';
            List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
            castRecords.addAll(records);
            upsert castRecords;
        }
        
    }   
    
   /* public static void doUpsert(Sobject[] source) {
        
        if(source != null && source.isEmpty()) {
            
            List<sObject> lstRecordToInsert =  new List<sObject>();
            List<sObject> lstRecordToUpdate =  new List<sObject>(); 
            
            for(sObject obj : source) {
                if(obj.Id != null) {
                    lstRecordToUpdate.add(obj);
                }
                else {
                    lstRecordToInsert.add(obj); 
                }                      
            }   
            
            if(!lstRecordToUpdate.isEmpty()){
                Database.update(lstRecordToUpdate,false);
            }
            
            if(!lstRecordToInsert.isEmpty()) {
                Database.insert(lstRecordToInsert,false);
            }
            
        }
        
    }*/
    
    /**
* Arguments : Id recordId - Record Id of the object record which needs to be shared
* Id userOrGroupId - Record Id of user or public group with which the object record needs to be shared
* String recordAccessLevel - Record sharing access level - Read, Edit, All
* Return type : sObject - share object specific to the object
* Description : Creates object share record as per the parameters passed to the method
**/
    
    public static SObject createObjectShare(Id recordId, Id userOrGroupId, String recordAccessLevel) {
        
        String objName = recordId.getSObjectType().getDescribe().getName();
        
        if(objName.endsWithIgnoreCase('__c')) {
            objName = objName.replace('__c','__Share');
        } 
        else {
            objName = objName + 'Share';
        }      
        
        SObject sObj = Schema.getGlobalDescribe().get(objName).newSObject();
        System.debug('sObj '+sObj);
        sObj.put('ParentId',recordId);
        sObj.put('UserOrGroupId',userOrGroupId);
        sObj.put('AccessLevel',recordAccessLevel);
        return sObj;
        
    }
    
    //Generic method which accepts the DeveloperName of Public Group as a parameter and returns the Salesforce Id of that Public Group
    public static Id getGroupIdByName(String groupName) {
        
        List<Group> grpObj = [SELECT Id FROM GROUP WHERE Type = 'Regular' AND DeveloperName =: groupName LIMIT 1];
        
        return grpObj[0].Id;
        
    }
    
    //Method to to generate a GUID/UUID (random unique ID's to be used for external integrations) in Apex
    public static String getUUID() {
        
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        system.debug(guid);
        return guid;
        
    }
    public static String getUUIDForMedia() {
        String guid='';
        for(Integer i=0;i<100;i++){  
            System.debug('i=='+i);
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
             system.debug('guId before=='+h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20));
            String node =h.SubString(12,16);
            String node2 =h.SubString(16,20);
        	//System.debug('h.charAt(12)=='+h.charAt(12));
            If(node.startsWithIgnoreCase('1') || node.startsWithIgnoreCase('2') ){
                   
                guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                    system.debug(guid);
                    break;
                
            }
            else if(node.startsWithIgnoreCase('3') || node.startsWithIgnoreCase('4') ||node.startsWithIgnoreCase('5')){
                if(node2.startsWithIgnoreCase('8') || node2.startsWithIgnoreCase('9') ||node2.startsWithIgnoreCase('a')
                   || node2.startsWithIgnoreCase('b')){
                       guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                        system.debug(guid);
                        break;
                   }
            }
       }
        return guid;
        
        
    }
    
    //Constructs an Address for a given SObject		
    public static void setAddress(SObject sObj, String fieldPrefix, String unitNumber, String streetNumber, String streetName, String streetTypeCode, String city, String stateCode, String postCode) {
        
        sObj.put(fieldPrefix + 'Street', unitNumber + ' ' + streetNumber + ' ' + streetName + ' ' + streetTypeCode);
        sObj.put(fieldPrefix + 'City', city);
        sObj.put(fieldPrefix + 'State', stateCode);
        sObj.put(fieldPrefix +'PostalCode',postCode);
        sObj.put(fieldPrefix +'Country','Australia');
        
    }
    
    //Pass the object name and get the keyprefix of the object
    public static String getObjectKeyPrefix(String objName) {
        
        schema.sObjectType sObjType = Schema.getGlobalDescribe().get(objName);
        system.debug(sObjType.getDescribe().getKeyPrefix());
        return (sObjType.getDescribe().getKeyPrefix());
        
    }
    
    
    public static Map<String, String> picklistValues(String objectName, String fieldName) {
        Map<String, String> values = new Map<String, String>{};
            
            List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(new List<String>{objectName});
        
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.put(entry.getValue(), entry.getLabel());
                }
            }
        }
        
        return values;
    }
    
    public static Map<String,Schema.DisplayType> fetchFieldTypesforObject(String sObjectname) {
        Map<String,Schema.DisplayType> fieldAPINameToDataTypeMap = new Map<String,Schema.DisplayType>();
        Map<String, Schema.SObjectField> fields = new Map<String, Schema.SObjectField>();
        
        if(sObjectname == 'Schema.Location')
        	fields = Schema.getGlobalDescribe().get('Location').getDescribe().fields.getMap();
        else
            fields = Schema.getGlobalDescribe().get(sObjectname).getDescribe().fields.getMap();
        
        for(String fieldName : fields.keySet()){
            if((fields.get(fieldName)).getDescribe().getType() == Schema.DisplayType.LOCATION){
                
                fieldAPINameToDataTypeMap.put((fields.get(fieldName)).getDescribe().getName().replace('__c','__latitude__s'),(fields.get(fieldName)).getDescribe().getType());
                fieldAPINameToDataTypeMap.put((fields.get(fieldName)).getDescribe().getName().replace('__c','__longitude__s'),(fields.get(fieldName)).getDescribe().getType());                
            }
            
            fieldAPINameToDataTypeMap.put((fields.get(fieldName)).getDescribe().getName(),(fields.get(fieldName)).getDescribe().getType());
        }
        System.debug('fieldAPINameToDataTypeMap'+fieldAPINameToDataTypeMap);
        System.debug('fieldAPINameToDataTypeMap1'+fieldAPINameToDataTypeMap.get('WorkOrder_Geolocation__c'));
    
        return fieldAPINameToDataTypeMap;
    }
    
    public static Object  transformAndMapFieldValues(Schema.DisplayType fieldType, Object value){
        System.debug('fieldType*****'+fieldType);
        System.debug('valuex*****'+value);
        
        if(fieldType == Schema.DisplayType.DATE){
            if (value == null  ||  value =='' ){return null;}
            return setStringToDateFormat((String)value);
        } else if(fieldType == Schema.DisplayType.DATETIME){
              if (value == null  ||  value =='' ){return null;}
            return setStringToDateTimeFormat((String)value);
        } else if(fieldType == Schema.DisplayType.DOUBLE  || Fieldtype == schema.DisplayType.CURRENCY || Fieldtype == schema.DisplayType.PERCENT){
              if (value == null  ||  value =='' ){return 0;}
                return Decimal.valueOf((String)value);
        } else if ( fieldType == Schema.DisplayType.INTEGER){
              if (value == null  ||  value =='' ){return 0;}
            return integer.valueof((string)value);
        } else if(fieldType == Schema.DisplayType.BOOLEAN){
              if (value == null  ||  value =='' ){return null;}
            return Boolean.valueOf((String)value);
         
        } else if(fieldType == Schema.DisplayType.ADDRESS ||  fieldType == Schema.DisplayType.STRING ||  fieldType == Schema.DisplayType.PHONE || fieldType == Schema.DisplayType.TEXTAREA || fieldType == Schema.DisplayType.PICKLIST){
                 if (value == null ){return '';}
        } else if(fieldType == Schema.DisplayType.LOCATION){
              if (value == null ){return '';}
            return Decimal.valueOf((String)value);
        }
        return value;
    }

    public static String buildFieldsForSOQLQuery(Set<String> fieldAPINameList) {
        String fieldsQuery = '';
        for(String str : fieldAPINameList) {

            fieldsQuery = (fieldsQuery + ',' + str).removeStart(',');
        }
        System.debug('Buld query'+fieldsQuery);

        return fieldsQuery;
    }
    
    public static Set<Id> readSaveResults(DataBase.SaveResult[] results, String jsonPayload, String objectName, String correlationId, String eventId, string externalId){
        //Success Id set, used to return for further processing/operations.
        Set<Id> successIdSet = new Set<Id>();
        //String to capture Log Level(Info/Warning/Error) on the Application Log object
        String logLevel = '';
        String msg;
        for(Database.SaveResult result : results) {
            msg = '';
            if(result.isSuccess()) {
                successIdSet.add(result.getId());
                sendSuccessErrorToMulesoft(' Success: Record Created  '+ objectName+ ', Id =  ' + result.getId()  , eventId, true, correlationId, String.valueof(Datetime.Now()),jsonPayload);
            }
			else{
                logLevel = 'Error';
            	for(Database.Error err : result.getErrors()) {                    
                	System.debug('DildarLog: ErrorMessage - ' + err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
                ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, objectName+';'+correlationId+';'+eventId+';'+ externalId );
			}
        }
        //Insert application log entry in SF database
        if(logLevel != '') {
            ApplicationLogUtility.saveExceptionLog();
            
            if((eventId != '')){
                ApplicationLogUtility.sendSuccessErrorToMulesoft('Correlation ID:' + correlationId + '\n' + msg, eventId, false);
            }
        }
        //TODO - ENABLE ONCE CONFIRMED
        /*else if(logLevel == '' & ((eventId == BSA_ConstantsUtility.BEAKON_COMPANY_CREATED_EVENT || eventId == BSA_ConstantsUtility.BEAKON_COMPANY_UPDATED_EVENT) || (eventId == BSA_ConstantsUtility.BEAKON_COMPANY_INACTIVATED_EVENT && objectName == 'ServiceResource'))){            
            //ApplicationLogUtility.sendSuccessErrorToMulesoft('Success - ' + correlationId, eventId, true);
        }*/
        if(successIdSet.size() > 0)
            return successIdSet;
        return null;
	}
    
    
    /*
     * Added by - Dildar Hussain
     * Desc - This method is used to perform multiple operations like - read sobject list upserted, 
     * insert application log and return success Id Set. 
	*/
   public static Set<Id> readUpsertResults(DataBase.UpsertResult [] results, String jsonPayload, String objectName, String correlationId, String eventId, string externalId){
        //Success Id set, used to return for further processing/operations.
        Set<Id> successIdSet = new Set<Id>();
        //String to capture Log Level(Info/Warning/Error) on the Application Log object
        String logLevel = '';
        String msg;
        for(Database.upsertResult result : results) {
            msg = '';
            if(result.isSuccess()) {
                successIdSet.add(result.getId());
                if(!externalId.contains('-UnifyWOTCreation'))
                	sendSuccessErrorToMulesoft(' Success: Record Created  '+ objectName+ ', Id =  ' + result.getId()  , eventId, true, correlationId, String.valueof(Datetime.Now()),jsonPayload);
            }
			else{
                logLevel = 'Error';
            	for(Database.Error err : result.getErrors()) {                    
                	System.debug('DildarLog: ErrorMessage - ' + err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
                ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, objectName+';'+correlationId+';'+eventId+';'+ externalId );
			}
        }
        //Insert application log entry in SF database
        if(logLevel != '') {
            ApplicationLogUtility.saveExceptionLog();
            
            if((eventId != '')){
                ApplicationLogUtility.sendSuccessErrorToMulesoft('Correlation ID:' + correlationId + '\n' + msg, eventId, false);
            }
        }
        //TODO - ENABLE ONCE CONFIRMED
        //else if(logLevel == '' & ((eventId == BSA_ConstantsUtility.BEAKON_COMPANY_CREATED_EVENT || eventId == BSA_ConstantsUtility.BEAKON_COMPANY_UPDATED_EVENT) || (eventId == BSA_ConstantsUtility.BEAKON_COMPANY_INACTIVATED_EVENT && objectName == 'ServiceResource'))){            
            //ApplicationLogUtility.sendSuccessErrorToMulesoft('Success - ' + correlationId, eventId, true);
        //}
        if(successIdSet.size() > 0)
            return successIdSet;
        return null;
	}
    
    /*
     * Added by - Dildar Hussain
     * Desc - This method is used to perform multiple operations like - read sobject list inserted/updated specifically, 
     * insert application log and return success Id Set.
	*/
	/*public static Set<Id> readUpdateResults(DataBase.SaveResult [] results, String jsonPayload, String objectName, String correlationId, String eventId, Id parentRecordId){
        //Success Id set, used to return for further processing/operations.
        Set<Id> successIdSet = new Set<Id>();
        //String to capture Log Level(Info/Warning/Error) on the Application Log object
        String logLevel = '';
        for(Database.SaveResult result : results) {
            String msg = '';
            if(result.isSuccess()) {
                successIdSet.add(result.getId());
                sendSuccessErrorToMulesoft(' Success: Record Created  '+ objectName+ ', Id =  ' + result.getId()  , eventId, true, correlationId, String.valueof(Datetime.Now()),jsonPayload);
           
            }
			else{
                logLevel = 'Error';
            	for(Database.Error err : result.getErrors()) {
                	System.debug('DildarLog: ErrorMessage - ' + err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
                if(parentRecordId != null)
                    ApplicationLogUtility.addException(parentRecordId, jsonPayload, msg, logLevel, objectName+';'+correlationId+';'+eventId);
                else
                    ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, objectName+';'+correlationId+';'+eventId);
			}
        }
        //Insert application log entry in SF database
        if(logLevel != '') {
            ApplicationLogUtility.saveExceptionLog();
        }
        if(successIdSet.size() > 0)
            return successIdSet;
        return null;
    } */
    
	/* 
     * Added by - Dildar Hussain
     * Date - 05 June 2020
     * Description - This method is used to send event success/failure message to Mulesoft.
    */
    public static void sendSuccessErrorToMulesoft(String successErrorMessage, String source, Boolean isSuccess, string CorrelationID, string EventTimeStamp, string response){
        SF_FSL_SUCCESS_PROCESSING_EVENT__e successEvent = new SF_FSL_SUCCESS_PROCESSING_EVENT__e();
        SF_FSL_FAILED_PROCESSING_EVENT__e failEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
        if(isSuccess){
        	successEvent.Response__c = successErrorMessage;
            successEvent.Source__c = source;
            successEvent.CorrelationID__c = CorrelationID;
            successEvent.Event_TimeStamp__c = EventTimeStamp;
            successEvent.Response__c = response;
        	Database.SaveResult result = EventBus.publish(successEvent);
        }else{
            failEvent.Response__c = successErrorMessage;
            failEvent.Source__c = source;
            Database.SaveResult result = EventBus.publish(failEvent);
        }
    }
    
    public static void fireOutboundPlatformEvent(id recordId, String objectName){
        set<Id> recordIdSet = new set<Id>();
        Map<Id,String> recordOperationMap = new Map<Id,String>();
        
        recordIdSet.add(recordId);
        recordOperationMap.put(recordId,'Update');
        if (objectName == 'WorkOrder'){
            ProntoWorkOrderSerialize.generateProntoWorkOrderPlatformEvent(recordIdSet);
        }else if(objectName == 'TimeSheetEntry') {
            ProntoTimeSheetSerialize.generateProntoTimeSheetPlatformEvent(recordIdSet);
        }else if(objectName == 'Order'){
            ProntoPurchaseOrderSerialize.generateProntoPurchaseOrderPlatformEvent(recordIdSet, recordOperationMap);
        }else if(objectName == 'ProductConsumed') {
            ProntoProductConsumedSerialize.generateProntoProductPlatformEvent(recordIdSet, recordOperationMap);
        }else if(objectName == 'Product2') {
            ProntoProductSerialize.generateProntoProductPlatformEvent(recordIdSet);
        }
    }
    /*public static void chatterPostPushNotification(Map<Id,Map<Id,String>> notificationMap){
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        
        List<FeedItem> postList = new LIst<FeedItem>();
        
        //messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        System.debug('DildarLog: notificationMap - ' + notificationMap);
        for(Id entityId: notificationMap.keySet()){
            for(Id mentionId: notificationMap.get(entityId).keySet()){
                mentionSegmentInput.id = mentionId;
                messageBodyInput.messageSegments.add(mentionSegmentInput);    
                
                textSegmentInput.text = notificationMap.get(entityId).get(mentionId);
                messageBodyInput.messageSegments.add(textSegmentInput);
                
                System.debug('DildarLog: notificationMap.get(entityId).get(mentionId) - ' + notificationMap.get(entityId).get(mentionId));
                FeedItem post = new FeedItem();
                post.ParentId = entityId;
                post.Body = notificationMap.get(entityId).get(mentionId);
                post.visibility = 'AllUsers';
                postList.add(post);
            }
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = entityId;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        }
        insert postList;
  }*/
}