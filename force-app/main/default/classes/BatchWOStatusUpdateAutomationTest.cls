@isTest
public class BatchWOStatusUpdateAutomationTest {
    static testMethod void testServiceResourceTrigger(){   
        List<Invoice__c> invList = new List<Invoice__c>();
        Test.startTest();
        BSA_TestDataFactory.createWOStatusBatchDataForTesting(); 
        for(Invoice__c inv :[SELECT id,Status__c FROM Invoice__c]){
            inv.Status__c = 'Approved';
            invList.add(inv);
        }
        Update invList;
        BatchWOStatusUpdateAutomation obj = new BatchWOStatusUpdateAutomation();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
    }
}