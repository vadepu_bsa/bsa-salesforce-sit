/**
 * @File Name          : ActionMaterialMarkupToBSATriggerHandler.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 31/07/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/07/2020         Tom Henson             Initial Version
**/
public class ActionMaterialMarkupToBSAHandler extends PlatformEventSubscriptionBaseHandler{
    
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private MaterialMarkupDTO mBaseDTO;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    private String eventActionName;
    private String correlationId;
    private string materialmarkupcode;
    private string materialmarkupname;
    
    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
       
        for(sObject sObj : peToProcessList){
            Action_MaterialMarkup_to_BSA__e event = (Action_MaterialMarkup_to_BSA__e)sObj;
            eventActionName = event.action__c;
            correlationId = event.correlationId__c;
            materialmarkupcode = event.materialmarkupcode__c;
            materialmarkupname = event.materialmarkupname__c;
            deserializeJSON(event);
            setupAdditonalSettings(event);   
        }
        super.process(peSobjectType,peToProcessList, mappedSobjectType);       
    }

    private void deserializeJSON(Action_MaterialMarkup_to_BSA__e event){
     
        String jsonString = '';
        if(String.isNotBlank(event.MaterialMarkup__c)){
            String tempString  = (event.MaterialMarkup__c);
            // System.debug('MaterialMarkup JSON'+tempString);
            jsonString = jsonString + tempString;
        }
 
        // System.debug('BuildJson'+ '{'+jsonString.removeEnd(',')+'}');

        mSpecificDTOClassName ='MaterialMarkupDTO';
     
        // System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        if(mSpecificDTOClassName.equalsIgnoreCase('MaterialMarkupDTO')){
            MaterialMarkupDTO newMaterialMarkupDTO = new MaterialMarkupDTO();
            mBaseDTO = newMaterialMarkupDTO.parse('{'+jsonString+'}');
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
        }
                
        // System.debug('Deserilize class'+mBaseDTO);
        // System.debug('Serialized class'+mSerializedData);
    }

	public override void processBuinessLogic(){
        // System.debug('!!!!Tom processBuinessLogic !!!! ');
        List<sObject> finalsObjectList = getMappedObjectList();
        // System.debug('!!!Final sObjectList '+ finalsObjectList);
        List<Material_Markup__c> finalMaterialMarkupList = new List<Material_Markup__c>();
        String accountId = null;
        Set<Id> successIdSet = new Set<Id>();
        String materialMarkupExternalField = getMappedExternalIdFromNode('materialMarkupDetails',false);
 
        if(finalsObjectList.size() > 0){
            
            Markup_Code__c markupCodeObj = new Markup_Code__c();
            markupCodeObj.External_ID__c = materialmarkupcode;
            markupCodeObj.Code__c = materialmarkupcode;
            markupCodeObj.Name = materialmarkupname;
            markupCodeObj.CorrelationId__c = correlationId;
            SObjectField MarkupCodeRefId = getSObjectFieldName('Markup_Code__c', 'External_ID__c');
            Database.UpsertResult markupCodeUpsertResult = Database.upsert(markupCodeObj,  MarkupCodeRefId , false);
           	
            for(integer i= 0; i < finalsObjectList.size(); i++){
                Map<String, Object> materialMarkupMap = new Map<String, Object>(finalsObjectList[i].getPopulatedFieldsAsMap());
                materialMarkupMap.put('Markup_Code__c',markupCodeObj.Id);
                materialMarkupMap.put('CorrelationId__c',correlationId);
                Material_Markup__c materialMarkupObj =  (Material_Markup__c) JSON.deserialize( JSON.serialize( materialMarkupMap ), Material_Markup__c.class );
             
                finalMaterialMarkupList.add(materialMarkupObj);
            }
            
            SObjectField materialMarkupRefId = getSObjectFieldName('Material_Markup__c', 'External_Id__c');

            Database.UpsertResult[] upsertResult = Database.upsert(finalMaterialMarkupList, materialMarkupRefId , false);
            // System.debug('!!!!Tom Log: upsertResult Material Markup - ' + upsertResult);   
            successIdSet = UtilityClass.readUpsertResults(upsertResult, JSON.serialize(finalMaterialMarkupList), 'MaterialMarkup', correlationId, eventActionName, materialmarkupcode); 			
        }
    }

    private void setupAdditonalSettings(Action_MaterialMarkup_To_BSA__e event){

        mIdentifierMapping = new Map<string,String>();
        //mIdentifierMapping.put('AccountCode__c',event.AccountCode__c);  
        mIdentifierMapping.put('Action_Name__c',event.Action__c);  

        setAdditionalMappingFilters('Action_Name__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);
    }
}