/* Author: Abhijeet Anand
* Date: 09 September, 2019
* Utility class that handles all the changes(for every DML event) on Application Log sObject and also provides 2 methods to create Application Log data in the system
*/
public class ApplicationLogUtility { 
    
    //Application Log collection variable to accumulate the success/error details of Platform Events
    private static list<Application_Log__c> lstException = new list<Application_Log__c>();
    
    //Method to accumulate the success/error details of Platform Events in a Application Log collection variable
    public static void addException (Id recordId, String payload, String message, String loglevel, String objectName) {
        
        Application_Log__c log = new Application_Log__c();
        log.Log_Level__c = loglevel;
        log.Message__c = message;
        
        if(objectName == 'Product2') {
            log.Catalogue_Sync_Pronto_Event_JSON__c = payload;
            log.Product__c = recordId;
            log.Object__c = 'Product2';
        } else if (objectName.containsIgnoreCase('Action_Order_From_BSA__e')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
       
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }   else if (objectName.containsIgnoreCase('Action_ProductConsumed_From_BSA__e')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
       
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        } else if (objectName.containsIgnoreCase('Action_Timesheet_From_BSA__e')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
       
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }  else if (objectName.containsIgnoreCase('Action_Product_From_BSA__e')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
       
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }
        else if (objectName.containsIgnoreCase('Account')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Event__c = stList[2];
            log.External_Identifier__c = stList[3];
       
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }
         else if (objectName.containsIgnoreCase('LabourRate')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Event__c = stList[2];
            log.External_Identifier__c = stList[3];
       
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }
         else if (objectName.containsIgnoreCase('MaterialMarkup')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Event__c = stList[2];
            log.External_Identifier__c = stList[3];
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }
        else if (objectName.containsIgnoreCase('Action_B2B_From_BSA__e')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Event__c = 'Action_B2B_From_BSA__e';
            log.External_Identifier__c = stList[3];
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }
        else if (objectName.containsIgnoreCase('Order;')){
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Event__c = stList[2];
            log.External_Identifier__c = stList[3];
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
        }
        else if(objectName == 'Location') {
            log.Van_Warehouse_Sync_Pronto_Event_JSON__c = payload;
            log.Location__c = recordId;
            log.Object__c = 'Location';
        }
        else if(objectName == 'ProductItem') {
            log.Stock_Replenishment_JSON__c = payload;
            log.Product_Item__c = recordId;
            log.Object__c = 'ProductItem';
        }
        else if(objectName == 'WorkOrder') {
            log.NBN_NA_JSON__c = payload;
            log.Work_Order__c = recordId;
            log.Object__c = 'WorkOrder';
            WorkOrder woObj = new WorkOrder(Id = recordId); 
            if(woObj.Id != null) {
                Map<Id,WorkOrder> woMap;
                if(message.containsIgnoreCase(BSA_ConstantsUtility.SUBMIT_NOTES) || message.containsIgnoreCase(BSA_ConstantsUtility.LOG_ENTRY)){
                    woMap = new Map<Id,WorkOrder>([SELECT Id,NBN_Parent_Work_Order_Id__c,Primary_Access_Technology__c,NBN_Field_Work_Specified_By_Id__c,NBN_Field_Work_Specified_By_Type__c, NBN_Field_Work_Specified_By_Version__c,NBN_NA_JSON_Payload__c,NBN_Activity_State_Id__c,CorrelationID__c 
                                                   FROM Workorder 
                                                   WHERE id = :recordId]);
                    
                    
                    //List<NBN_Log_Entry__c> logEntry = (List<NBN_Log_Entry__c>)Json.deserialize(payload, List<NBN_Log_Entry__c>.class) ;
                    //log.Type__c = message.containsIgnoreCase(BSA_ConstantsUtility.SUBMIT_NOTES) ? BSA_ConstantsUtility.SUBMIT_NOTES : BSA_ConstantsUtility.LOG_ENTRY;
                    //log.Notes__c = (logEntry[0] != null && logEntry[0].get('Submit_Notes_Additional_Info__c') != null ) ? (String)logEntry[0].get('Submit_Notes_Additional_Info__c') : '';
                    
                } else{
                    
                    woMap = WorkOrderTriggerHandler.workOrderMap();                    
                }
                
                if(woMap.containskey(recordId)) {
                    for(WorkOrder obj : woMap.values()) {
                        
                        if(obj.id == recordId){
                            log.NBN_Parent_Work_Order_Id__c = obj.NBN_Parent_Work_Order_Id__c;
                            log.NBN_Field_Work_Specified_By_Category__c = obj.Primary_Access_Technology__c;
                            log.NBN_Field_Work_Specified_By_Id__c = obj.NBN_Field_Work_Specified_By_Id__c;
                            log.NBN_Field_Work_Specified_By_Type__c = obj.NBN_Field_Work_Specified_By_Type__c;
                            log.NBN_Field_Work_Specified_By_Version__c = obj.NBN_Field_Work_Specified_By_Version__c;
                            System.debug('json payload --> '+obj.NBN_NA_JSON_Payload__c);
                            log.NBN_NA_JSON_Payload__c = obj.NBN_NA_JSON_Payload__c;
                            log.Correlation_ID__c = obj.CorrelationID__c;
                            if(message.contains('ACKNOWLEDGED')) {
                                log.NBN_Activity_State_Id__c = 'ACKNOWLEDGED:Step_SDP_Tech_On_Site';
                            }
                            else{
                                log.NBN_Activity_State_Id__c = obj.NBN_Activity_State_Id__c;
                            }
                            
                            log.NBN_Field_Work_Interaction_Status__c = 'Success';
                            
                            
                        }
                    }
                    
                }
                
            }
        }
        else if(objectName == 'NBN_Log_Entry__c') {
            log.NBN_NA_JSON__c = payload;
            log.NBN_Log_Entry__c = recordId;
            log.Object__c = 'NBN Log Entry';
            
        }    
        else if(objectName.equalsIgnoreCase(BSA_ConstantsUtility.SUCCESS_EVENT_NAME) || objectName.equalsIgnoreCase(BSA_ConstantsUtility.FAILURE_EVENT_NAME) ) {
            log.Correlation_ID__c = message;
            log.Message__c = payload;
            log.Log_Level__c = loglevel;
            
        }
        else if(objectName.containsIgnoreCase(BSA_ConstantsUtility.ACTION_B2B_SETUP_WORK_ORDER_EVENT)) {
            //i.e. '[0]objectName;[1]correlationId;[2]eventAction[3]woExternalId
            System.debug('DildarLog: Action B2B Event Logs - [0]objectName;[1]correlationId;[2]eventId;[3]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
            if(stList.size() > 3 ){
				log.NBN_Field_Work_Interaction_Status__c = stList[4];            
                
                String woRelationshipName = Application_Log__c.Work_Order__c.getDescribe().getRelationshipName();
                SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
                
                SObject workOrderParentObj = workOrderType.newSObject();
                workOrderParentObj.put('Client_Work_Order_Number__c', stList[3]);
                log.putSObject(woRelationshipName, workOrderParentObj);
            }
        }
        else if(objectName.containsIgnoreCase('ContentDocument')) {
            //i.e. '[0]objectName;[1]correlationId;[2]woExternalId
            System.debug('BSALog: Action ContentDocument Upload Logs - [0]objectName;[1]correlationId;[2]woExternalIdValue - ' + objectName);
            String [] stList = objectName.split(';');
            log.Correlation_ID__c = stList[1];
            log.Object__c = stList[0];
            log.Message__c = message+'\n --------------- \n'+payload;
            log.Log_Level__c = loglevel;
            if(stList[2] != ''){
			    
                String woRelationshipName = Application_Log__c.Work_Order__c.getDescribe().getRelationshipName();
                SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
                
                SObject workOrderParentObj = workOrderType.newSObject();
                workOrderParentObj.put('Client_Work_Order_Number__c', stList[2]);
                log.putSObject(woRelationshipName, workOrderParentObj);
            }
        }
        lstException.add(log); 
        
    }
    
    //Method to perform DML on the Application Log collection variable
    public static void saveExceptionLog() {
        
        System.debug('lstException --> '+lstException);
        if(!lstException.isEmpty()) {
            insert lstException; 
        }
        
        lstException.clear(); 
        
    }
    
    //Method to publish Success/Failed Processing Event so that Mule soft gets a response on the subscription channel
    public static void processApplicationLogs(List<Application_Log__c> applicationLogList) {
        
        //A generic sObject collection variable to accumulate details of Platform Events to be published                          
        List<sobject> platformEventsToPublish = new List<sobject>();
        
        for(Application_Log__c al : applicationLogList) {
            
            if(al.Catalogue_Sync_Pronto_Event_JSON__c != null) {
                Integer index = al.Catalogue_Sync_Pronto_Event_JSON__c.lastIndexOf(',');
                System.debug('last index --> '+ index);
                
                //When the Pronto.Catalogue.Product.Created/Updated PE fails to sync with SF
                if(al.Log_Level__c == 'Error' && al.Product__c == null) {
                    //Create a  SF.FSL.FAILED_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_FAILED_PROCESSING_EVENT__e newEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the exception occurred
                    newEvent.Response__c = al.Catalogue_Sync_Pronto_Event_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'false'+','+al.Catalogue_Sync_Pronto_Event_JSON__c.right(al.Catalogue_Sync_Pronto_Event_JSON__c.length()-index-1);
                    platformEventsToPublish.add(newEvent);
                }
                
                //When the Pronto.Catalogue.Product.Created/Updated PE successfully syncs with SF
                else if(al.Log_Level__c == 'Info' && al.Product__c != null) {
                    //Create a SF.FSL.SUCCESS_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_SUCCESS_PROCESSING_EVENT__e successEvent = new SF_FSL_SUCCESS_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the message of successfull product upsert in SF
                    successEvent.Response__c = al.Catalogue_Sync_Pronto_Event_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'true'+','+al.Catalogue_Sync_Pronto_Event_JSON__c.right(al.Catalogue_Sync_Pronto_Event_JSON__c.length()-index-1);
                    platformEventsToPublish.add(successEvent);
                }            
            }
            else if(al.Van_Warehouse_Sync_Pronto_Event_JSON__c != null) {
                Integer index = al.Van_Warehouse_Sync_Pronto_Event_JSON__c.lastIndexOf(',');
                System.debug('last index --> '+ index);
                
                //When the Pronto.Van.Warehouse.Created/Updated PE fails to sync with SF
                if(al.Log_Level__c == 'Error' && al.Location__c == null) {
                    //Create a  SF.FSL.FAILED_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_FAILED_PROCESSING_EVENT__e newEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the exception occurred
                    newEvent.Response__c = al.Van_Warehouse_Sync_Pronto_Event_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'false'+','+al.Van_Warehouse_Sync_Pronto_Event_JSON__c.right(al.Van_Warehouse_Sync_Pronto_Event_JSON__c.length()-index-1);
                    platformEventsToPublish.add(newEvent);
                }
                
                
                else if(al.Log_Level__c == 'Info' && al.Location__c != null) {
                    //Create a SF.FSL.SUCCESS_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_SUCCESS_PROCESSING_EVENT__e successEvent = new SF_FSL_SUCCESS_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the message of successfull product upsert in SF
                    successEvent.Response__c = al.Van_Warehouse_Sync_Pronto_Event_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'true'+','+al.Van_Warehouse_Sync_Pronto_Event_JSON__c.right(al.Van_Warehouse_Sync_Pronto_Event_JSON__c.length()-index-1);
                    platformEventsToPublish.add(successEvent);
                }   
            }
            else if(al.Stock_Replenishment_JSON__c != null) {
                Integer index = al.Stock_Replenishment_JSON__c.lastIndexOf(',');
                System.debug('last index --> '+ index);
                
                //When the Pronto.Van.Warehouse.Created/Updated PE fails to sync with SF
                if(al.Log_Level__c == 'Error' && al.Product_Item__c == null) {
                    //Create a  SF.FSL.FAILED_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_FAILED_PROCESSING_EVENT__e newEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the exception occurred
                    newEvent.Response__c = al.Stock_Replenishment_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'false'+','+al.Stock_Replenishment_JSON__c.right(al.Stock_Replenishment_JSON__c.length()-index-1);
                    platformEventsToPublish.add(newEvent);
                }
                
                
                else if(al.Log_Level__c == 'Info' && al.Product_Item__c != null) {
                    //Create a SF.FSL.SUCCESS_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_SUCCESS_PROCESSING_EVENT__e successEvent = new SF_FSL_SUCCESS_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the message of successfull product upsert in SF
                    successEvent.Response__c = al.Stock_Replenishment_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'true'+','+al.Stock_Replenishment_JSON__c.right(al.Stock_Replenishment_JSON__c.length()-index-1);
                    platformEventsToPublish.add(successEvent);
                }   
            }
            else if(al.NBN_NA_JSON__c != null) {
                Integer index = al.NBN_NA_JSON__c.lastIndexOf(',');
                System.debug('last index --> '+ index);
                
                //When the HFC.Action.Assign.From.NBN.To.BSA PE fails to sync with SF
                if(al.Log_Level__c == 'Error' && al.Work_Order__c == null) {
                    //Create a  SF.FSL.FAILED_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_FAILED_PROCESSING_EVENT__e newEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the exception occurred
                    newEvent.Response__c = al.NBN_NA_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'false'+','+al.NBN_NA_JSON__c.right(al.NBN_NA_JSON__c.length()-index-1);
                    platformEventsToPublish.add(newEvent);
                }
                
                else if(al.Log_Level__c == 'Info' && al.Work_Order__c != null && (al.Message__c.containsIgnoreCase('Created') ||  al.Message__c.containsIgnoreCase('Updated')) /*al.NBN_Activity_State_Id__c != 'ACKNOWLEDGED:Step_SDP_Tech_On_Site'*/) {
                    //Create a SF.FSL.SUCCESS_PROCESSING_EVENT platform event to publish to Mulesoft
                    SF_FSL_SUCCESS_PROCESSING_EVENT__e successEvent = new SF_FSL_SUCCESS_PROCESSING_EVENT__e();
                    
                    //build a response variable to include the message of successfull product upsert in SF
                    successEvent.Response__c = al.NBN_NA_JSON__c.left(index+1)+'\n'+'"'+'message'+'"'+' : '+'"'+al.Message__c+'"'+','+'\n'+'"'+'success'+'"'+' : '+'true'+','+al.NBN_NA_JSON__c.right(al.NBN_NA_JSON__c.length()-index-1);
                    WorkOrder wo = new WorkOrder(Id = al.Work_Order__c);
                    Map<Id,WorkOrder> woMap;
                    if(al.Message__c.containsIgnoreCase(BSA_ConstantsUtility.SUBMIT_NOTES) || al.Message__c.containsIgnoreCase(BSA_ConstantsUtility.LOG_ENTRY)){
                        woMap = new Map<Id,WorkOrder>([SELECT Id,WorkType.Name,Based_Revision_Number__c,NBN_Action__c,Update_Type__c,Update_Sub_Type__c,NBN_Parent_Work_Order_Id__c,NBN_Activity_Start_Date_Time__c,NBN_Field_Work_Specified_By_Category__c,Primary_Access_Technology__c,NBN_Field_Work_Specified_By_Id__c,NBN_Field_Work_Specified_By_Type__c, NBN_Field_Work_Specified_By_Version__c,NBN_NA_JSON_Payload__c,NBN_Activity_State_Id__c,CorrelationID__c 
                                                       FROM Workorder 
                                                       WHERE id = :al.Work_Order__c]);
                        
                        
                        //successEvent.Type__c = al.Type__c;
                        //successEvent.Notes__c = al.Notes__c;
                        
                        
                    } else{
                        
                        woMap = WorkOrderTriggerHandler.workOrderMap();                    
                    }
                    
                    
                    if(woMap.containskey(al.Work_Order__c)) {
                        for(WorkOrder obj : woMap.values()) {
                            if(obj.id == al.Work_Order__c){
                                
                                successEvent.NBN_Parent_Work_Order_Id__c = obj.NBN_Parent_Work_Order_Id__c;
                                successEvent.NBN_Related_ID__c = obj.NBN_Parent_Work_Order_Id__c;
                                successEvent.NBN_Related_Sub_Id__c = obj.Id;
                                successEvent.NBN_Field_Work_ID__c = obj.NBN_Parent_Work_Order_Id__c;
                                successEvent.NBN_Field_Work_Specified_By_Category__c = obj.NBN_Field_Work_Specified_By_Category__c;
                                successEvent.NBN_Field_Work_Specified_By_Id__c = obj.NBN_Field_Work_Specified_By_Id__c;
                                successEvent.NBN_Field_Work_Specified_By_Type__c = obj.NBN_Field_Work_Specified_By_Type__c;
                                successEvent.NBN_Field_Work_Specified_By_Version__c = obj.NBN_Field_Work_Specified_By_Version__c;
                                successEvent.NBN_Activity_State_Date_Time__c = obj.NBN_Activity_Start_Date_Time__c;
                                successEvent.CorrelationID__c = obj.CorrelationID__c;
                                successEvent.NBN_Update_Type__c = obj.Update_Type__c;
                                successEvent.NBN_Update_SubType__c = obj.Update_Sub_Type__c;
                                successEvent.Action__c = obj.NBN_Action__c;
                                successEvent.NBN_Activity_State_Id__c = obj.NBN_Activity_State_Id__c;
                                //successEvent.Revision_Number__c = obj.Based_Revision_Number__c;
                               //successEvent.NBN_Activity_Instantiated_By__c = obj.WorkType.Name;
                                successEvent.NBN_Field_Work_Interaction_Status__c = 'Success'; 
                            }
                        }
                    }
                    
                    platformEventsToPublish.add(successEvent);
                }
                
            }
            
            
        }
        
        System.debug('platformEventsToPublish -->'+platformEventsToPublish);
        // Call method to publish success/failure of events
        if(!platformEventsToPublish.isEmpty()) {
            platformEventsToPublish.sort();
            ApplicationLogUtility.publishEvents(platformEventsToPublish);
        }
        
    }
    //A generic method to publish platform events
    public static void publishEvents(List<sobject> eventListToPublish) {
        
        //Publish the events
        List<Database.SaveResult> results = EventBus.publish(eventListToPublish);
        
        //Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' +
                                 err.getStatusCode() +
                                 ' - ' +
                                 err.getMessage());
                }
            }       
        }
        
    }
    
    // Method to populate the Sales Order Number returned from Pronto on Product Consumed 
    public static void popupalteSalesOrderNumber (Map<String,String> consumptionMap) {
        
        List<ProductConsumed> productToUpdate = new List<ProductConsumed> ();
        
        for(ProductConsumed obj : [SELECT Id, ProductConsumedNumber, Sales_Order_Number__c FROM ProductConsumed WHERE ProductConsumedNumber IN : consumptionMap.keyset()]) {
            obj.Sales_Order_Number__c = consumptionMap.get(obj.ProductConsumedNumber);
            productToUpdate.add(obj);
        }
        
        if(!productToUpdate.isEmpty()) {
            update productToUpdate;
        }
        
    }
    /* 
     * Added by - Dildar Hussain
     * Date - 05 June 2020
     * Description - This method is used to send event success/failure message to Mulesoft.
    */
    public static void sendSuccessErrorToMulesoft(String successErrorMessage, String source, Boolean isSuccess){
        SF_FSL_SUCCESS_PROCESSING_EVENT__e successEvent = new SF_FSL_SUCCESS_PROCESSING_EVENT__e();
        SF_FSL_FAILED_PROCESSING_EVENT__e failEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
        if(isSuccess){
        	successEvent.Response__c = successErrorMessage;
            successEvent.Source__c = source;
        	Database.SaveResult result = EventBus.publish(successEvent);
        }else{
            failEvent.Response__c = successErrorMessage;
            failEvent.Source__c = source;
            Database.SaveResult result = EventBus.publish(failEvent);
        }
    }
}