/**
 * @File Name          : DTOFactory.cls
 * @Description        : 
 * @Author             : Karan Shekhar
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 16/05/2020, 12:11:49 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/05/2020   Karan Shekhar     Initial Version
**/
public  class DTOFactory {

    private String mImplExtIdentifier ;  
    private DTOFactory() {}


    public DTOFactory(String implExtIdentifier){ 
        
        //mPESobjectType = peSobjectType;
        mImplExtIdentifier = implExtIdentifier;
    }

    public IDTO getDTOClass(){     
        String defaultImplClassName = 'ClientDTO';
        String specificImplClassName;
        List<PE_Subscription_Object_Action_Mapping__mdt> setting = [SELECT DTO_Class_Name__c FROM PE_Subscription_Object_Action_Mapping__mdt WHERE Service_Implementation_Ext_Identifier__c = :mImplExtIdentifier AND Setting_Type__c = :BSA_ConstantsUtility.CLASS_IMPL_SETTING_TYPE_NAME LIMIT 1];
       
        if(setting.size()>0 && String.isNotBlank(setting[0].DTO_Class_Name__c)){
            
            specificImplClassName = setting[0].DTO_Class_Name__c;
        } else{
            specificImplClassName = defaultImplClassName;

        }
        
        System.debug('defaultImplClassName'+defaultImplClassName);
        System.debug('specificImplClassName'+specificImplClassName);
        
        Type customType = Type.forName(specificImplClassName);  
        System.debug('Custom type'+customType);
        if(customType == null){
            customType = Type.forName(defaultImplClassName);  
        }
        return (IDTO)customType.newInstance();        
        
    } 
}