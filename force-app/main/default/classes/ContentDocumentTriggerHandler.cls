/**
 * @author          Eliana G
 * @date            11/Jan/2021 
 * @description     Trigger delete ContentDocument Object
 *
 * Change Date    Modified by         Description  
 * 10/05/2021     Ben Lee             Added condition to only update when there is a Pronto ID
 **/
public class ContentDocumentTriggerHandler implements ITriggerHandler{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
  public void BeforeInsert(List<SObject> ContentDocumentList) {}
  public void AfterInsert(Map<Id, SObject> newItems) {}
  public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}  
  public void AfterDelete(Map<Id, SObject> oldItems) {}  
  public void AfterUndelete(Map<Id, SObject> oldItems) {}

  // created by Eliana for FSL3-837
  public void BeforeDelete(Map<Id, SObject> oldItems) {
  
    string LinkedEntityId = null;
    string Client_Work_Order_Number  = null;
    integer total = 0;
    //List<ContentDocument> APSRecordList = new List<ContentDocument>();
    //List<ContentDocument> nonAPSRecordList = new List<ContentDocument>();
    Map<Id, string> ContentDocumentAPSIddMap = new map<id,string>();
    Set<Id> contentDocumentIdSet = new Set<Id>();
    
     //if(trigger.IsDelete){
        Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();

        for(sobject con : oldItems.values()) {
            System.debug('FSL3-837 BeforeDelete handler trigger con.Id -->' +  con.Id);
            //contentDocId.add(con.Id);

            List<ContentDocumentLink> documents = [SELECT LinkedEntityId  FROM ContentDocumentLink WHERE ContentDocumentId  =: con.Id];
/*             total = documents.size();
            System.debug('FSL3-837 Total ContentDocumentLink find -->' +  total); */

            if(documents.size()>0){
                for (ContentDocumentLink d : documents) {
                     LinkedEntityId = d.LinkedEntityId;

                     System.debug('FSL3-837 Before search workorder LinkedEntityId -->' +  LinkedEntityId  + ' APSRecordTypeId ' + APSRecordTypeId);
                     //List<workorder> listwo = [SELECT ID, RecordTypeId, Client_Work_Order_Number__c                  FROM WorkOrder WHERE                            Id =: LinkedEntityId AND RecordTypeId =: APSRecordTypeId];  //Ben Lee - Disabled 20210510
                     List<workorder> listwo = [SELECT ID, RecordTypeId, Client_Work_Order_Number__c, Pronto_WO_ID__c FROM WorkOrder WHERE Pronto_WO_ID__c != null AND Id =: LinkedEntityId AND RecordTypeId =: APSRecordTypeId];  //Ben Lee - Updated 20210510
                     if(listwo.size()>0){
                         for (workorder wo : listwo){
         
                             System.debug('FSL3-837 LinkedEntityId --> LinkedEntityId  ' + LinkedEntityId);
                             System.debug('FSL3-837 ContentDocumentId --> con.Id  ' + con.Id);
         
                             //for (ContentDocumentLink p : documents) {
                             Client_Work_Order_Number = wo.Client_Work_Order_Number__c;
                             contentDocumentIdSet.add(con.Id);
                             System.debug('FSL3-837---> Client_Work_Order_Number = '+ Client_Work_Order_Number +  ' con.Id =  ' + con.Id + ' wo.ID = ' + wo.ID );
                             //}
         
                             if (Client_Work_Order_Number != null & contentDocumentIdSet != null){
         
                                 if (wo.ID.getsobjecttype().getDescribe().getName() == 'WorkOrder'){ 
                                     //APSRecordList.add(con);
                                     system.debug('FSL3-837  wo.Id is a WorkOrder ---> con.ID ' + wo.ID);
         
                                     ContentDocumentAPSIddMap.put(con.Id, 'Delete');
        
                                     //contentDocumentIdSet.add(con);
                                     //  START contentDocumentSerialize code
                                     //system.debug('FSL3-837  contentDocumentIdSet added ---> con.Id ' + con.Id);
                                     system.debug('FSL3-837--->get URL from ContentDistribution for ContentDocumentId = ' + con.Id );
                                     List<ContentDistribution> distributionObj = [SELECT DistributionPublicUrl, Name
                                                                         FROM ContentDistribution
                                                                         WHERE ContentDocumentId =: con.Id]; 
                                     string URL = null;
                                     string name = null;
                                     if(distributionObj.size()>0){
                                         for (ContentDistribution dt : distributionObj){
                                            URL = dt.DistributionPublicUrl;
                                         }
                                     }
                                     // query to Content Document to get fileType 
                                     //ContentDocument cd = new ContentDocument();
                                     system.debug('FSL3-837--->   URL from ContentDistribution for ContentDocumentId = ' + con.Id  + ' URL  ' + URL);
                             
                                     system.debug('FSL3-837--->get fileType from ContentDocument for ID = ' + con.Id );
                             
                                     List<ContentDocument> contentDocument  = [SELECT FileType from ContentDocument where Id =: con.Id];
                                     string fileType = null;
                                     if(contentDocument.size()>0){
                                         for (contentDocument cd : contentDocument){
                                              fileType = cd.FileType;
                                         }
                                     }
                                     system.debug('FSL3-837--->   fileType from ContentDocument for ID = ' + con.Id  + ' fileType  ' + fileType);
                                     List<ContentDistributionDetails> ContentDistributionDetailsList = new List<ContentDistributionDetails>();
                                     for (integer i = 0; i < distributionObj.size(); i++){
                                         ContentDistributionDetails pObj = new ContentDistributionDetails();

                                         pObj.DocumentType = null;

                                         name = distributionObj[i].Name.toLowerCase();
                                         if(name.contains('service') && name.contains('report')){
                                         
                                             pObj.DocumentType = 'Service Docket';
                                         }
                             
                                         system.debug('FSL3-837 --> DocumentType' +  pObj.DocumentType);
                             
                                         pObj.WorkOrderId = Client_Work_Order_Number;
                                         pObj.DocumentId = contentDocumentIdSet;
                                         pObj.ContentDownloadUrl = (distributionObj[i].DistributionPublicUrl != null) ? distributionObj[i].DistributionPublicUrl : null;
                                         pObj.FileType = fileType;
                                         pObj.DocumentName = distributionObj[i].Name; 
                                         pObj.Operation ='Delete';     // by default 
 /*                                         if (ContentDocumentAPSIddMap.ContainsKey(distributionObj[i].Id)){
                                             pObj.Operation = ContentDocumentAPSIddMap.get(distributionObj[i].Id);
                                         } */
                                         ContentDistributionDetailsList.add(pObj);
                                         system.debug('FSL3-837 --> JSON Serialize ' + JSON.serialize(pObj,false));
                                     }
                                     system.debug('FSL3-837 -->  JSON Serialize ALL ' + JSON.serialize(ContentDistributionDetailsList,false));
                                     //return ContentDistributionDetailsList;
                                     //  END contentDocumentSerialize code
         
                                     
                                     //List<Action_ProductConsumed_From_BSA__e> InsertplatformEventList = ContentDocumentPublish.generatePlatformEvents(finalDocumentList);
                                     //  START generatePlatformEvents code
                                     List<Action_ProductConsumed_From_BSA__e> InsertplatformEventList = new List<Action_ProductConsumed_From_BSA__e>();

                                     system.debug('FSL3-837 -->  before START generatePlatformEvents code ' );
          
                                     for (ContentDistributionDetails p : ContentDistributionDetailsList){
                                         Action_ProductConsumed_From_BSA__e eventObj = new Action_ProductConsumed_From_BSA__e();
                             
                                         system.debug('FSL3-837 -->  before eventObj populate' );

                                         eventObj.Action__c = 'Generate_ContentDocumentProntoLink';
                                         eventObj.CorrelationId__c  = UtilityClass.getUUID();
                                         eventObj.ProductConsumed__c = '"contentDocumentDetails":' + JSON.serialize(p,false);
                                         eventObj.WorkOrder__c = '';
                                         eventObj.Account__c = ''; 
                                         eventObj.EventTimeStamp__c = String.ValueOf(System.DateTime.Now());
                                         eventObj.Type__c = 'APS';
                                         InsertplatformEventList.add(eventObj);

                                         system.debug('FSL3-837 -->  after eventObj populate' );
                                     }
                                     //  END generatePlatformEvents code
         
                                     ///List<Action_ProductConsumed_From_BSA__e> InsertplatformEventList = new List<Action_ProductConsumed_From_BSA__e>();

                                     string logs =  JSON.serializePretty(InsertplatformEventList); 
                                     System.Debug('FSL3-837 --> !!! Full Output');  
                                     for (Integer i = 0; i < logs.length(); i=i+300) {
                                         Integer iEffectiveEnd = (i+300 > (logs.length()-1) ? logs.length()-1 : i+300);
                                         System.debug('FSL3-837---> logs = '+ logs.substring(i,iEffectiveEnd));
                                     }
                                     if (InsertplatformEventList.size() > 0 ) {
                                         List<Database.SaveResult> results = EventBus.publish(InsertplatformEventList);
                                         System.debug('FSL3-837---> results = '+ results );
                                     }   
                                     System.debug('FSL3-837---> logs = '+ logs );
                                     
                                     System.debug('FSL3-837---> InsertplatformEventList = '+ InsertplatformEventList );
                                 }  
                             }                      
                         }
                     } else {
                         System.debug('FSL3-837 LinkedEntityId Not found  for document Id  ' + con.Id);
                     }

                }
            }
            else {
                System.debug('FSL3-837 ContentDocumentLink Not found  for document Id  ' + con.Id);
            }
 
        }
    //}
  }

    Public Class ContentDistributionDetails{
        public String WorkOrderId {get;set;}
        public String ContentDownloadUrl{get;set;}
        public string Operation {get;set;}
        public set<id> DocumentId {get;set;}
        public String FileType  {get;set;}
        public String DocumentType{get;set;}
        public String DocumentName  {get;set;}

        public ContentDistributionDetails(){
            WorkOrderId = null;
            ContentDownloadUrl = null;
            Operation = null;
            DocumentId = null;
            FileType = null;
            DocumentType = null;
            DocumentName = null;
        }
    }      
}