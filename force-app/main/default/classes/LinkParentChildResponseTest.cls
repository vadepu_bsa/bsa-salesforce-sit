@isTest(SeeAllData=true)
public class LinkParentChildResponseTest {
    
    @isTest static void linkChildResponseTest(){
        
        createTestDate();
        
        LinkParentChildResponse.linkChildResponse();
        
        
        
    }

    static void createTestDate(){
        
              
        List<Response_Details__c> responseDetailList = 
            [SELECT Parent_Question_Text__c,Parent_Response__c,Question_Number__c,
             Question_Text__c,Response_Text__c,Response__c,Temp_Child_Link__c,
             Temp_Parent_Id__c,Temp_Response_Header_Link__c,Textbox_1_Response__c,
             Textbox_2_Response__c,Textbox_3_Response__c,Textbox_4_Response__c FROM Response_Details__c where Response__c = 'a1D5O0000000knJUAQ'];
 
        List<Response_Details__c> newResponseDetailList = new List<Response_Details__c>();
        
        For (Response_Details__c responseDetails :responseDetailList ){
            responseDetails.Parent_Response__c = null;
            responseDetails.Response__c=null;
            responseDetails.id=null;
            newResponseDetailList.add(responseDetails);
        }
        
        insert newResponseDetailList;
        
    }
}