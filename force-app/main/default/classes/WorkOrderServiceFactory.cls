/**
 * @File Name          : WorkOrderServiceFactory.cls
 * @Description        : Factory class to route work order trigger to correct handler
 * @Author             : Sayali Limaye
 * @Group              : 
 **/
public class WorkOrderServiceFactory {

    private sObjectType mPESobjectType ;  
    private String mImplExtIdentifier ;  
    
    //Private constructor to avoid accidental initialization of Factory
    private  WorkOrderServiceFactory(){}
    
    public WorkOrderServiceFactory(sObjectType peSobjectType, String implExtIdentifier){ 
        
        mPESobjectType = peSobjectType;
        mImplExtIdentifier = implExtIdentifier;
    }
    
    /**
    * @description : Method to get specific handler name. Identifying handler follows a naming pattern.
                    e.g. If PE API name id Action_B2B_To_BSA then handler name should be ActionB2BTtoBSAHandler.
                    Contract specific implementation should have contract name in front of it. e.g. NBNOMMAActionB2BTtoBSAHandler
    * @author Karan Shekhar | 04/05/2020 
    * @return IPlatformEventSubscription 
    **/
    public IWorkOrderService gethandlerName(){ 
        System.debug('mPESobjectType'+mPESobjectType);
        String defaultImplClassName = 'WorkOrderServiceBaseImplementation';
        String specificImplClassName;
        List<PE_Subscription_Object_Action_Mapping__mdt> lstSetting = [SELECT WO_Service_Implementation_Ext_Identifier__c,Service_Implementation_Int_Identifier__c FROM PE_Subscription_Object_Action_Mapping__mdt WHERE WO_Service_Implementation_Ext_Identifier__c = :mImplExtIdentifier LIMIT 1];
        SYstem.debug('class settings'+lstSetting);
        if(lstSetting !=null && lstSetting.size()>0 && String.isNotBlank(lstSetting[0].Service_Implementation_Int_Identifier__c)){
            
            specificImplClassName = lstSetting[0].Service_Implementation_Int_Identifier__c;
        }
        else if (test.isRunningTest()){
            specificImplClassName = 'NBNNAWorkOrderServiceImplementation';
        }
        
        System.debug('defaultImplClassName'+defaultImplClassName);
        System.debug('specificImplClassName'+specificImplClassName);
        specificImplClassName = specificImplClassName == null ? defaultImplClassName: specificImplClassName;
        Type customType = Type.forName(specificImplClassName);  
        
        if(customType == null){
            customType = Type.forName(defaultImplClassName);  
        }
        return (IWorkOrderService)customType.newInstance();        
        
    } 
}