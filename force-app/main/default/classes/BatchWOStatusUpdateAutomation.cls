global class BatchWOStatusUpdateAutomation Implements Database.Batchable<sObject>, Database.Stateful{
    global String strQuery;
    public List<String> batchException = new List<String>();
    Set<Id> invoiceIdSet = new Set<Id>();
    
    //Initialize the query string
    global BatchWOStatusUpdateAutomation(){
        List<Invoice__History> invoiceHistoryList = [Select Id, OldValue, NewValue, ParentId, CreatedDate From Invoice__History Where Field = 'Status__c' AND CreatedDate = TODAY];
        if(invoiceHistoryList.size() > 0){
            for(Invoice__History invHistory:invoiceHistoryList){
                if((invHistory.OldValue == 'Approved' || invHistory.OldValue == 'Pending' || invHistory.OldValue == 'Sent To Client') && (invHistory.NewValue == 'Approved' || invHistory.NewValue == 'Invoiced' || invHistory.NewValue == 'Sent To Client'))
                    invoiceIdSet.add(invHistory.ParentId);
            }
        }
        if(!Test.isRunningTest()){
            strQuery = 'SELECT Id, Work_Order__c, Status__c FROM Invoice__c WHERE (Status__c = \'Approved\' OR Status__c = \'Invoiced\' OR Status__c = \'Sent To Client\') AND Id IN:invoiceIdSet';
            
        } else {
            
            strQuery = 'SELECT Id, Work_Order__c, Status__c FROM Invoice__c WHERE (Status__c = \'Approved\' OR Status__c = \'Invoiced\' OR Status__c = \'Sent To Client\')';
        }
        
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        System.debug('DildarLog: Batch - BatchWOStatusUpdateAutomation - Start - strQuery - ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Invoice__c> listBatchRecords){
        Set<Id> accountIds = new Set<Id>();
        Map<Id,String> parentWOWithStatusMap = new Map<Id,String>();
        
        for(Invoice__c invoiceRecord:listBatchRecords){
            parentWOWithStatusMap.put(invoiceRecord.Work_Order__c, invoiceRecord.Status__c);
        }
        
        System.debug('DildarLog: Batch - BatchWOStatusUpdateAutomation - Execute - parentWOWithStatusMap - ' + parentWOWithStatusMap);
        List<WorkOrder> relatedWOList = [Select Id, ParentWorkOrderId,ParentWorkOrder.Sub_Status__c, Sub_Status__c From WorkOrder Where ParentWorkOrderId IN:parentWOWithStatusMap.keySet() AND Sub_Status__c != 'Invoiced'];
        
        System.debug('DildarLog: Batch - BatchWOStatusUpdateAutomation - Execute - relatedWOList - ' + relatedWOList);
        
        List<WorkOrder> workOrderListToBeUpdated = new List<WorkOrder>();
        Map<Id,List<WorkOrder>> parentWOWithChildMap = new Map<Id,List<WorkOrder>>();
        Map<Id,String> parentWOWithSubStatusMap = new Map<Id,String>();
        
        for(WorkOrder wo:relatedWOList){
            if(parentWOWithChildMap.containsKey(wo.ParentWorkOrderId)){
                parentWOWithChildMap.get(wo.ParentWorkOrderId).add(wo);
            }else{
                parentWOWithChildMap.put(wo.ParentWorkOrderId, new List<WorkOrder>{wo});
            }
            parentWOWithSubStatusMap.put(wo.ParentWorkOrderId,wo.ParentWorkOrder.Sub_Status__c);
        }
        
        for(Id parentWOId:parentWOWithChildMap.keySet()){
            for(WorkOrder childWO:parentWOWithChildMap.get(parentWOId)){
                if(childWO.Sub_Status__c != parentWOWithStatusMap.get(parentWOId)){
                    
                    childWO.Sub_Status__c = parentWOWithStatusMap.get(parentWOId);
                    workOrderListToBeUpdated.add(childWO);
                }
                //childWO.Sub_Status__c = childWO.Sub_Status__c != parentWOWithStatusMap.get(parentWOId) ? parentWOWithStatusMap.get(parentWOId) : ;
                
            }
            if(parentWOWithSubStatusMap.containsKey(parentWOId) && parentWOWithStatusMap.containsKey(parentWOId) && parentWOWithSubStatusMap.get(parentWOId) != parentWOWithStatusMap.get(parentWOId)){
                                
                WorkOrder wo = new WorkOrder();
                wo.Id = parentWOId;
                wo.Sub_Status__c = parentWOWithStatusMap.get(parentWOId);
                workOrderListToBeUpdated.add(wo);
            }
            
        }
        
        try{
            if(workOrderListToBeUpdated.size() > 0){
                Database.update(workOrderListToBeUpdated);
            }
        }catch(Exception batchEx){
            batchException.add(batchEx.getMessage());
            System.debug('DildarLog: Batch - BatchWOStatusUpdateAutomation - Execute - Exception - ' + batchEx.getMessage());
        }
        
        System.debug('DildarLog: Batch - BatchWOStatusUpdateAutomation - Execute - relatedWOListToBeUpdated - ' + workOrderListToBeUpdated);
    }
    
    global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exception list        
        if(batchException.size() > 0){
            System.debug('DildarLog: Batch - BatchWOStatusUpdateAutomation - Finish - Exceptions - ' + batchException);
        }
    }
}