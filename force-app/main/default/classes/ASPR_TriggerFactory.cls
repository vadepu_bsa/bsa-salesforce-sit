/**
  * Class ASPR_TriggerFactory
  *
  * Used to instantiate and execute Trigger Handlers associated with sObjects.
*/
public class ASPR_TriggerFactory {
    //for tests
    public static Boolean isBeforeTrigger = false;
    public static Boolean isAfterTrigger = false;
    public static Boolean isInsertTrigger = false;
    public static Boolean isUpdateTrigger = false;
    public static Boolean isDeleteTrigger = false;
    /**
      * Public static method to create and execute a trigger handler
      *
      * Arguments:   Schema.sObjectType soType - Object type to process (SObject.sObjectType)
      *
      * Throws a TriggerException if no handler has been coded.PSVC_PetServiceStatus_Handler
    */
    public static void createHandler(Schema.sObjectType soType) {
        // Get a handler appropriate to the object being processed
        List<ASPR_ITrigger> handlerList = getHandlers(soType);

        if (handlerList != null) {
            for (ASPR_ITrigger handler : handlerList) {
                if (handler.isActive()) { execute(handler); }
            }
        }
    }

    /**
      * private static method to control the execution of the handler
      *
      * Arguments:   ASPR_ITrigger handler - A Trigger Handler to execute
    */
    @TestVisible
    private static void execute(ASPR_ITrigger handler) {
        // Before Trigger
        if (Trigger.isBefore == true || (Test.isRunningTest() && isBeforeTrigger)) {
            // Iterate through the records to be deleted passing them to the handler.
            if (Trigger.isDelete == true || (Test.isRunningTest() && isDeleteTrigger)) {
                // Call the bulk before to handle any caching of data and enable bulkification
                handler.bulkBefore(Trigger.old);
                if(Trigger.old != null) {
                    for (SObject so : Trigger.old) { handler.beforeDelete(so); }
                }
            }
            // Iterate through the records to be inserted passing them to the handler.
            else if (Trigger.isInsert == true || (Test.isRunningTest() && isInsertTrigger)) {
                // Call the bulk before to handle any caching of data and enable bulkification
                handler.bulkBefore(Trigger.new);
                
                if(Trigger.new != null) {
                    for (SObject so : Trigger.new) { handler.beforeInsert(so); }
                }
            }
            // Iterate through the records to be updated passing them to the handler.
            else if (Trigger.isUpdate == true || (Test.isRunningTest() && isUpdateTrigger)) {
                // Call the bulk before to handle any caching of data and enable bulkification
                handler.bulkBefore(Trigger.old);
                
                if(Trigger.old != null) {
                    for (SObject so : Trigger.old) { handler.beforeUpdate(so, Trigger.newMap.get(so.Id)); }
                }
            }
        }
        if (Trigger.isAfter == true || (Test.isRunningTest() && isAfterTrigger)) {
            // Call the bulk after to handle any caching of data and enable bulkification
            //handler.bulkAfter();

            // Iterate through the records deleted passing them to the handler.
            if (Trigger.isDelete == true || (Test.isRunningTest() && isDeleteTrigger)){ 
                handler.bulkAfter(Trigger.old);
                
                if(Trigger.old != null) {
                    for (SObject so : Trigger.old) { handler.afterDelete(so); }
                }
            }
            // Iterate through the records inserted passing them to the handler.
            else if (Trigger.isInsert == true || (Test.isRunningTest() && isInsertTrigger)) {
                handler.bulkAfter(Trigger.new);
                
                if(Trigger.new != null) {
                    for (SObject so : Trigger.new) { handler.afterInsert(so); }
                }
            }
            // Iterate through the records updated passing them to the handler.
            else if (Trigger.isUpdate == true || (Test.isRunningTest() && isUpdateTrigger)) {
                handler.bulkAfter(Trigger.old);
                
                if(Trigger.old != null) {
                    for (SObject so : Trigger.old) { handler.afterUpdate(so, Trigger.newMap.get(so.Id)); }
                }
            }
        }

        // Perform any post processing
        handler.andFinally();
    }

    /**
      * private static method to get the appropriate handler for the object type.
      * Modify this method to add any additional handlers.
      *
      * Arguments:   Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
      *
      * Returns:     List<ASPR_ITrigger> - A list of trigger handlers.
    */
    @TestVisible
    private static List<ASPR_ITrigger> getHandlers(Schema.sObjectType soType) {
        List<ASPR_ITrigger> handlerList = new List<ASPR_ITrigger> ();

        if (soType == Shift.sObjectType) {
            addClassToHandler('BSA_ShiftTriggerHandler', handlerList);
        } 
        return handlerList;
    }

    @TestVisible
    private static void addClassToHandler(String className, List<ASPR_ITrigger> handlerList) {
        Type classTypeObj = Type.forName(className);
        if (classTypeObj == null) {
            return;
        }
        handlerList.add((ASPR_ITrigger) classTypeObj.newInstance());
    }
}