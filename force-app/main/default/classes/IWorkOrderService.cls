/**
 * @File Name          : IWorkOrderService.cls
 * @Description        : 
 * @Author             : Sayali Limaye
 * @Group              : 
 * @Last Modified By   : Sayali Limaye
 * @Last Modified On   : 10/12/2020, 4:01:11 pm
 * @Modification Log   : 
 
**/
public interface IWorkOrderService {
	void setupWorkOrder(List<WorkOrder> woList);
    void processWO(Map<id, Sobject> newItems);
    void cloneWO(Map<id, Sobject> newItems);
    void populatePRDonClonedWO(Map<id, Sobject> newItems);
    void processWoAfterUpdate(Map<id, Sobject> newItems,Map<id, Sobject> oldItems);
    void generateRelatedRecordsforChilds(Map<id,sObject> mIdtoWoWithChangesSR);
    void syncChildWithParent(Map<id,WorkOrder> newItems, Map<Id,sObject> oldItems);   
    void setPRDnPCDDatesOnWO(Map<id, Sobject> newItems);
}