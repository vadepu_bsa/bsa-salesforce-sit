public class UnifyPopulateMissingWODataScript {
	//Apex properties or variables

    public Id Id { get; set; }
    public WorkOrder workOrderRecord {get; set;}
    public Datetime planned_remediation_date {get; set;}
    public String clientWorkOrdrNumber {get; set;}
    public String correlationId {get; set;}
    public String specificationId {get; set;}
    public String specificationVersion {get; set;}
    public String workTypeName {get; set;}
    public String message {get; set;}
    
    //constructor to get the Case record
    public UnifyPopulateMissingWODataScript(ApexPages.StandardController controller) {
        WorkOrder wo = (WorkOrder) controller.getRecord();
        message = '';
        Id = wo.Id;
    }

    //Method that can is called from the Visual Force page action attribute
    public PageReference unifyWorkOrderPageReference() {
        try{
            List<WorkOrderLineItem> woliList = [Select Id from WorkOrderLineItem where WorkOrder.ParentWorkOrderId =:Id];
            
            if(woliList == null || woliList.size() < 1){
                workOrderRecord = [Select Id, planned_remediation_date__c, CorrelationID__c, NBN_Field_Work_Specified_By_Id__c, NBN_Field_Work_Specified_By_Version__c, Client_Work_Order_Number__c from WorkOrder where Id =:Id];
                System.debug('DildarLog: workOrderRecord - ' + workOrderRecord);
                
                clientWorkOrdrNumber = workOrderRecord.Client_Work_Order_Number__c;
                correlationId = workOrderRecord.CorrelationID__c;
                specificationId = workOrderRecord.NBN_Field_Work_Specified_By_Id__c;
				planned_remediation_date = (specificationId == 'RemediationWOS') ? workOrderRecord.planned_remediation_date__c : System.now();
                workTypeName = workOrderRecord.NBN_Field_Work_Specified_By_Id__c == 'RemediationWOS' ? 'NBN Unify Remediation' : workOrderRecord.NBN_Field_Work_Specified_By_Id__c == 'GeneralWOS' ? 'NBN Unify General' : workOrderRecord.NBN_Field_Work_Specified_By_Id__c == 'GreenfieldPreInstallWOS' ? 'NBN Unify Pre-Install' : '';
                specificationVersion = workOrderRecord.NBN_Field_Work_Specified_By_Version__c;
                
                //sent accept outbound to nbn and create missing wolis on child work order
                createWoliRecords();
                
                if(specificationId == 'RemediationWOS')
                    sentPRDOutbound();
                
                System.debug('DildarLog: WorkOrder Record - unifyWorkOrderPageReference - workOrderRecord: ' + workOrderRecord);
			}
        }catch(Exception ex){
            message = 'Exception @UnifyPopulateMissingWODataScript - '+ ex.getMessage() + ' <br><br> <b>Error may be because -</b> <br> (1) PRD date is not populated on work order. <br> (2) Specification is missing on work order. <br> <br> Please go back, correct the data and re-process the Work Order. <br><br><b>DO NOT REFRESH THIS PAGE!</b>';
            //TODO Deletion Action
            //Delete [Select Id from WorkOrderLineItem where WorkOrderId =: Id];
            System.debug('DildarLog: ' + message);
            return null;
		}
        
       	PageReference pageRef = new PageReference('/'+Id);
       	pageRef.setRedirect(true);
       	return pageRef; //Returns to the workorder page
    }
    
    public void createWoliRecords(){
        if(workTypeName != '' && workTypeName != null){
            WorkType wt = [Select Id, Name From WorkType Where Name =:workTypeName];
            
            WorkOrder parWO = new WorkOrder();
            parWO.Id = Id;
            parWO.WorkTypeId = wt.Id; //Remediation/General/Pre-Install
            update parWO;
            
            WorkOrder childWO =[select id, WorkTypeId, ParentWorkorder.WorkTypeId from Workorder where ParentWorkorderId=:Id];
            childWO.WorkTypeId = childWO.ParentWorkorder.WorkTypeId;
            Update childWO;
            
            Id woliRecTypeId = [Select Id from RecordType where DeveloperName = 'CUI' AND SobjectType = 'WorkOrderLineItem'].Id;
            List<WorkOrderLineItem> woliToCreateList = new List<WorkOrderLineItem>();
            List<Work_Order_Automation_Rule__c> lstWOLIWoars =[select id,Type__c,Action__c,Work_Type__c,Required_Pre_Finish__c,Required_Pre_Notification__c from Work_Order_Automation_Rule__c where Service_Contract__c=:Label.Unify_Services_Simplex_Contract_Id and recordtype.developername ='WOLI_Automation'and Work_type__c=:childWO.WorkTypeId and Active__c = true order by sequence__c];
            
            //acknowldge NBN on WO acceptance
            sentAcceptOutbound();
            
            System.debug('DildarLog: Record - createWoliRecords - lstWOLIWoars: ' + lstWOLIWoars);
            
            for(Work_Order_Automation_Rule__c objWOAR:lstWOLIWOARs){
                WorkOrderLineItem woli = new WorkOrderLineItem();
                woli.Type__c = objWOAR.Type__c;
                woli.Description = objWOAR.Action__c;
                woli.WOrkOrderId = childWO.Id;
                woli.WorkTypeId = objWOAR.Work_Type__c;
                woli.Required_Pre_Finish__c = objWOAR.Required_Pre_Finish__c;
                woli.Required_Pre_Notification__c = objWOAR.Required_Pre_Notification__c;
                woli.RecordTypeId = woliRecTypeId;
                woliToCreateList.add(woli);
            }
            insert woliToCreateList;
		}
    }
    
    public void sentAcceptOutbound(){
        WSNBNWOOutboundActionDTO.Specification specification;
        List<WSNBNWOOutboundActionDTO.input_data_bindings> inputBindingsList;
        WSNBNWOOutboundActionDTO payloadObj;
        WSNBNWOOutboundActionDTO.input_data_bindings inp1;
        
        String type_z = specificationId.remove('WOS');
        System.debug('DildarLog: Record - sentAcceptOutbound - type_z: ' + type_z);
        
        String actionName = 'accept';
        
        Datetime dt = System.now();
        String formatedDt = dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
        Datetime dtPRD = planned_remediation_date;
        String formatedDtPRD = dtPRD.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
        String currentTransactionCorrelationId = UtilityClass.getUUID();
        specification = new WSNBNWOOutboundActionDTO.Specification(specificationId,specificationVersion);
        inp1 = new WSNBNWOOutboundActionDTO.input_data_bindings('interaction_datetime',formatedDt);
        inputBindingsList = new List<WSNBNWOOutboundActionDTO.input_data_bindings>();
        inputBindingsList.add(inp1);
        
        String header = Label.Get_WO_Outbound_Header;
        header = header.replace('{0}',currentTransactionCorrelationId);
        header = header.replace('{1}', correlationId);
        
        List<String> headerList = header.split(';');
        Map<String, String> headerMap = new Map<String, String>();
        for(String headerItem : headerList){
        headerMap.put(headerItem.split(':')[0], headerItem.split(':')[1]);
        }
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);
        WSNBNWOOutboundActionDTR.publishEvents(json.serialize(headerMap), (payloadObj.getPayload()).replace('type_Z','type'), Label.WS_NBN_WO_Outbound_Action_URL, 'post', currentTransactionCorrelationId);
    }
    
    public void sentPRDOutbound(){
        WSNBNWOOutboundActionDTO.Specification specification;
        List<WSNBNWOOutboundActionDTO.input_data_bindings> inputBindingsList;
        WSNBNWOOutboundActionDTO payloadObj;
        WSNBNWOOutboundActionDTO.input_data_bindings inp1;
        
        String type_z = specificationId.remove('WOS');
        System.debug('DildarLog: Record - sentPRDOutbound - type_z: ' + type_z);
        
        String actionName = 'update_prd';
        
        Datetime dt = System.now();
        String formatedDt = dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
        Datetime dtPRD = planned_remediation_date;
        String formatedDtPRD = dtPRD.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
        String currentTransactionCorrelationId = UtilityClass.getUUID();
        specification = new WSNBNWOOutboundActionDTO.Specification(specificationId,specificationVersion);
        inp1 = new WSNBNWOOutboundActionDTO.input_data_bindings('interaction_datetime',formatedDt);
        inputBindingsList = new List<WSNBNWOOutboundActionDTO.input_data_bindings>();
        inputBindingsList.add(inp1);
        
        WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('eot_cause','');
        WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('eot_reason','');
        WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('planned_remediation_date',formatedDtPRD);
        WSNBNWOOutboundActionDTO.input_data_bindings inp5 = new WSNBNWOOutboundActionDTO.input_data_bindings('delivery_confidence_level','Green');
        inputBindingsList.add(inp2);
        inputBindingsList.add(inp3);
        inputBindingsList.add(inp4);
        inputBindingsList.add(inp5);
        
        String header = Label.Get_WO_Outbound_Header;
        header = header.replace('{0}',currentTransactionCorrelationId);
        header = header.replace('{1}', correlationId);
        
        List<String> headerList = header.split(';');
        Map<String, String> headerMap = new Map<String, String>();
        for(String headerItem : headerList){
        headerMap.put(headerItem.split(':')[0], headerItem.split(':')[1]);
        }
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);
        WSNBNWOOutboundActionDTR.publishEvents(json.serialize(headerMap), (payloadObj.getPayload()).replace('type_Z','type'), Label.WS_NBN_WO_Outbound_Action_URL, 'post', currentTransactionCorrelationId);
    }
}