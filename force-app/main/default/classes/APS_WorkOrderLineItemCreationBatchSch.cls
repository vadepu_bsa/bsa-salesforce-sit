global class APS_WorkOrderLineItemCreationBatchSch implements Schedulable {

    global List<WorkOrderLineItem> assetWolis;    
    global String guid;
    

    global APS_WorkOrderLineItemCreationBatchSch(List<WorkOrderLineItem> assetWolis, String guid) {
        this.assetWolis = assetWolis;
        this.guid = guid;
    }

    global void execute(SchedulableContext SC) { 
        String message='';
        Draft_Work_Order__c df=new Draft_Work_Order__c();
        df.Reference_Id__c='d011f2c8-ebdd-66b1-d774-e98d2ae05ea8';
        insert df;
        APS_WorkOrderLineItemCreationBatch b = new APS_WorkOrderLineItemCreationBatch(assetWolis, guid,message,df.id);
        database.executebatch(b);
    }
 }