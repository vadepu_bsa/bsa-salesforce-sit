/**
 * @author          Sunila.M
 * @date            01/April/2019 
 * @description     Interface implementation of trigger logic. 
 *
 * Change Date   	 Modified by         Description  
 * 01/April/2019   	 Sunila.M        Created the Handler class for the trigger.
 **/
public interface ITriggerHandler {

    void BeforeInsert(List<SObject> newItems);
    
    void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    
    void BeforeDelete(Map<Id, SObject> oldItems);
    
    void AfterInsert(Map<Id, SObject> newItems);
    
    void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    
    void AfterDelete(Map<Id, SObject> oldItems);
    
    void AfterUndelete(Map<Id, SObject> oldItems);
    
    Boolean IsDisabled();
    
}