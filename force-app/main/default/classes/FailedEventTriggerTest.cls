@isTest
public class FailedEventTriggerTest {
	static testMethod void testValidEventWith() {

        // Create a test event instance for consumed products
        SF_FSL_FAILED_PROCESSING_EVENT__e syncEvent = new SF_FSL_FAILED_PROCESSING_EVENT__e();
        syncEvent.Response__c = 'Failed';
        syncEvent.Source__c = 'API';
        syncEvent.Product_Consumed__c = 'TestProduct';
        syncEvent.Product_Transfer__c = null;
        syncEvent.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';

        // Create a test event instance without consumed products and trasfer
        SF_FSL_FAILED_PROCESSING_EVENT__e syncEvent2 = new SF_FSL_FAILED_PROCESSING_EVENT__e();
        syncEvent2.Response__c = 'Failed';
        syncEvent2.Source__c = null;
        syncEvent2.Product_Consumed__c = null;
        syncEvent2.Product_Transfer__c = null;
        syncEvent2.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent2.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        // Create a test event instance for products transfer
        SF_FSL_FAILED_PROCESSING_EVENT__e syncEvent3 = new SF_FSL_FAILED_PROCESSING_EVENT__e();
        syncEvent3.Response__c = 'Failed';
        syncEvent3.Source__c = 'API';
        syncEvent3.Product_Consumed__c = null;
        syncEvent3.Product_Transfer__c = 'TestProduct';
        syncEvent3.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent3.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        Test.startTest();
        
            // Publish test event
            Database.SaveResult sr = EventBus.publish(syncEvent);
        	Database.SaveResult sr2 = EventBus.publish(syncEvent2);
        	Database.SaveResult sr3 = EventBus.publish(syncEvent3);
            
        Test.stopTest();
                
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());
        System.assertEquals(true, sr2.isSuccess());
        System.assertEquals(true, sr3.isSuccess());                
    }
}