/* Author: Abhijeet Anand
 * Date: 26 September, 2019
 * Trigger Handler added for ProductItemTrigger
 */
 
 public class ProductItemHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //After insert method 
    public void AfterInsert(Map<Id, SObject> newItems) { 
        
        List<SObject> productItemList = new List<SObject>();

        //Pass ProductItem List to dynamically share Product Items with Service Resources of the ProductItem's Location
        if(!newItems.isEmpty()) {
            productItemList.addAll(newItems.values());
            ProductItemAutomationBusinessLogic.shareProductItemWithServiceResource(productItemList,null);
        }

    }
    
    
    //After update method 
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) { 
        List<SObject> productItemList = new List<SObject>();
        productItemList.addAll((List<SObject>)newItems.values());
        
        //Pass ProductItem List
        if(!productItemList.isEmpty()) {
            ProductItemAutomationBusinessLogic.shareProductItemWithServiceResource(productItemList,oldItems);
        }
    }
    
    
    public void BeforeInsert(List<SObject> newItems) {} public void BeforeDelete(Map<Id, SObject> oldItems) {} public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}

}