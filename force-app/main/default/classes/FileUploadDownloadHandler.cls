public virtual class FileUploadDownloadHandler {
    private String processId;
    private ContentVersion cv;
    public FileUploadDownloadHandler(){}
    
    public virtual void  download(String URL,String woId,ContentVersion cv,String processId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload responseProcessingDTR){
        this.processId =processId;
        this.cv = cv;
        if(processId ==Label.Get_Media_List_Process){
            downloadMediaList(Label.NBN_Get_Media_List_Endpoint,woId,responseProcessingDTO,responseProcessingDTR); 
        }
        else if(processId ==Label.Get_Media_Resource_Process){
            downloadMediaResource(Label.NBN_Get_Media_Resource,woId,responseProcessingDTO,responseProcessingDTR);
        }
        else if(processId ==Label.Get_Media_Image_Process){
            downloadMediaImage(URL,woId,responseProcessingDTO,responseProcessingDTR);
        }
    
    }  
    
    public virtual void  upload(String URL,String woId,ContentVersion cv,String processId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload responseProcessingDTR){
        this.processId =processId;
        this.cv = cv;
        if(processId ==Label.Create_Media_Resource){
            uploadMediaResource(Label.NBN_Upload_Media_Enpoint,woId,responseProcessingDTO,responseProcessingDTR); 
        }
        else if(processId ==Label.Post_to_URL){
            uploadResourceToURL(URL,woId,responseProcessingDTO,responseProcessingDTR);
        }
       
    
    }  
    
    public virtual void  downloadMediaList(String URL ,String woId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload responseProcessingDTR){
        List<workorder> woList =[select id,CorrelationID__c,Client_Work_Order_Number_Tech__c from workorder where id=:woId];
        //invoke REST callout with this URL
        String dryptData =woId;
        String callOutName = '';
        String jsonS ='';
        String urlParameter ='woid:'+dryptData;
        String correlationId = UtilityClass.getUUIDForMedia();
        HttpResponse response = new HttpResponse();
        callOutName = 'MediaListAPIGet';
        List<String> parameters = new List<String>();
		
        parameters.add(correlationId);
        
        parameters.add(woList[0].CorrelationID__c);
        
        String sampleHeaderwithParam = String.format(Label.Get_Media_List_Body_Header, parameters);
        sampleHeaderwithParam ='{'+sampleHeaderwithParam+'}';
        sampleHeaderwithParam = JSON.serialize(sampleHeaderwithParam);
        //URL = URL +'?$.relatedEntity.id='+woId;
        URL = URL +'?$.relatedEntity.id='+woList[0].Client_Work_Order_Number_Tech__c;
        jsons ='{"requestHeader" :'+sampleHeaderwithParam+',"requestBody": "null","requestUrl":"'+URL+'","httpMethod":"GET"}';
		//WSOutboundActionBasePayload.setPayload(sampleHeaderwithParam,null,'',null);
        HTTPCalloutUtility callout = new HTTPCalloutUtility(callOutName, jsonS);
        //callout.appendURLParameters('woid', dryptData);
        //callout.addHeaderParameters('X-Correlation-ID', correlationId);
        //callout.addHeaderParameters('x-client-id', UserInfo.getOrganizationId());            
        response = callout.sendRequest();
		System.debug('SAYALI response:'+response.getBody());
        if(response.getBody().contains('Exception')){
            
            System.debug('Exception occured');
            return;
        }
        //Response should deserailize as responseProcessingDTO class
        Type customType = Type.forName(responseProcessingDTO.setResponseProcessingDTOName());
        IDTOList instance = (IDTOList)customType.newInstance();
        List<Object> lstDTO = instance.parse(response.getBody());
        // call post deserlization transformation
        Type customType1 = Type.forName(responseProcessingDTO.setResponseProcessingDTRName());
        IDTR instance1 = (IDTR)customType1.newInstance();
        for(Object objDTO:lstDTO){
        	instance1.transform(objDTO,woId,processId);
        }
        WorkOrder woObj = [select id,MediaFilesUpdated__c from workorder where id=:woId];
        woObj.MediaFilesUpdated__c = true;
        update woObj;
        
    }
    public virtual void  downloadMediaResource(String URL,String woId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload responseProcessingDTR){
        List<workorder> woList =[select id,CorrelationID__c from workorder where id=:woId];
        //invoke REST callout with this URL
        String dryptData =cv.Media_Id__c;
        String callOutName = '';
        String jsonS ='';
        String urlParameter ='woid:'+dryptData;
        String correlationId = UtilityClass.getUUIDForMedia();
        HttpResponse response = new HttpResponse();
        
        List<String> parameters = new List<String>();
		
        parameters.add(correlationId);
        
        parameters.add(woList[0].CorrelationID__c);
        
        String sampleHeaderwithParam = String.format(Label.Get_Media_List_Body_Header, parameters);
        sampleHeaderwithParam ='{'+sampleHeaderwithParam+'}';
        sampleHeaderwithParam = JSON.serialize(sampleHeaderwithParam);
        URL = URL +'/'+cv.Media_Id__c;
        jsons ='{"requestHeader" :'+sampleHeaderwithParam+',"requestBody": "null","requestUrl":"'+URL+'","httpMethod":"GET"}';
		//WSOutboundActionBasePayload.setPayload(sampleHeaderwithParam,null,'',null);
		
        callOutName = 'MediaResourceAPIGet';
        HTTPCalloutUtility callout = new HTTPCalloutUtility(callOutName, jsonS);
        //callout.appendURLParameters('woid', dryptData);
        //callout.addHeaderParameters('X-Correlation-ID', correlationId);
        //callout.addHeaderParameters('x-client-id', UserInfo.getOrganizationId());            
        response = callout.sendRequest();
		//System.debug('SAYALIDEBUG response:'+response.getBody());
        //Response should deserailize as responseProcessingDTO class
        Type customType = Type.forName(responseProcessingDTO.setResponseProcessingDTOName());
        IDTOList instance = (IDTOList)customType.newInstance();
        List<Object> lstDTO = instance.parse('['+response.getBody()+']');
        
        // call post deserlization transformation
        Type customType1 = Type.forName(responseProcessingDTO.setResponseProcessingDTRName());
        IDTR instance1 = (IDTR)customType1.newInstance();
        for(Object objDTO:lstDTO){
        	instance1.transform(objDTO,woId,processId);
        }
        
        
    }
    public virtual void  downloadMediaImage(String URL,String woId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload responseProcessingDTR){
        
        //invoke REST callout with this URL
        String dryptData =woId;
        String callOutName = '';
        String jsonS ='';
        String urlParameter ='id:'+dryptData;
        String correlationId = UtilityClass.getUUIDForMedia();
        HttpResponse response = new HttpResponse();
        //callOutName = 'MediaResourceAPIGet';
        HTTPCalloutUtility callout = new HTTPCalloutUtility(URL);
        //callout.appendURLParameters('woid', dryptData);
        //callout.addHeaderParameters('X-Correlation-ID', correlationId);
        //callout.addHeaderParameters('x-client-id', UserInfo.getOrganizationId());            
        response = callout.sendRequest();
		//System.debug('SAYALIDEBUG response:'+response.getBody());
        //responseValue = response.getBody();
		//system.debug('Response Body for File: ' + responseValue);
		
		blob image = response.getBodyAsBlob();
        cv.VersionData = image;
        cv.IsCreatedFromFetchMedia__c = true;
        insert cv;
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: cv.Id].ContentDocumentId;
        cdl.LinkedEntityId = (Id) woId;
        cdl.ShareType = 'V';
        insert cdl;
        
    }
    
    public virtual void  uploadMediaResource(String URL,String woId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload requestProcessingDTR){
        List<workorder> woList =[select id,CorrelationID__c,Client_Work_Order_Number__c,Client_Work_Order_Number_Tech__c from workorder where id=:woId];
        //invoke REST callout with this URL
        String dryptData =woId;
        String callOutName = '';
        String jsonS ='';
        String urlParameter ='woid:'+dryptData;
        String correlationId = UtilityClass.getUUIDForMedia();
        
        HttpResponse response = new HttpResponse();
        
        callOutName = 'Create_Media_Resource';
        
        String contentType = cv.FileType;
        String mimeType = BSA_ConstantsUtility.FILE_TYPE_MIME_MAP.get(cv.FileType);
        
        if(mimeType != null && mimeType != ''){
            contentType = mimeType+'/'+contentType;
        }
        
		System.debug('DildarLog : fileTitle: ' + cv.Title + ', contentType: ' + contentType + ', mimeType: ' + mimeType);
        
        String body ='{'+
          '"relatedEntity": {'+ 
             '"id": "'+woList[0].Client_Work_Order_Number_Tech__c+'",'+
             '"@type": "WorkOrderRef"'+
            '},'+
            '"metadata": {'+
              '"objectType":"MetaDataWorkOrder",'+
              '"notes": "'+cv.Description+'",'+
              '"fileName":"'+cv.Title+'",'+
              '"contentType": "'+contentType+'"'+
             '}'+
           '}';
        
        System.debug('DildarLog : Payload: ' + body);
        
        List<String> parameters = new List<String>();
		parameters.add(correlationId);
        parameters.add(woList[0].CorrelationID__c);
        
        String sampleHeaderwithParam = String.format(Label.Get_Media_List_Body_Header, parameters);
        sampleHeaderwithParam ='{'+sampleHeaderwithParam+'}';
        sampleHeaderwithParam = JSON.serialize(sampleHeaderwithParam);
        body =JSON.serialize(body);
        jsons ='{"requestHeader" :'+sampleHeaderwithParam+',"requestBody": '+body+',"requestUrl":"'+URL+'","httpMethod":"POST"}';
		//WSOutboundActionBasePayload.setPayload(sampleHeaderwithParam,null,'',null);
		//System.debug('jsons =='+jsons);
        HTTPCalloutUtility callout = new HTTPCalloutUtility(callOutName, jsonS);
        //callout.appendURLParameters('woid', dryptData);
        //callout.addHeaderParameters('X-Correlation-ID', correlationId);
        //callout.addHeaderParameters('x-client-id', UserInfo.getOrganizationId());            
        response = callout.sendRequest();
        //System.debug('SAYALI response Code:'+response.getStatus());
		//System.debug('SAYALI response:'+response.getBody());
        //Response should deserailize as responseProcessingDTO class
        Type customType = Type.forName(responseProcessingDTO.setResponseProcessingDTOName());
        IDTOList instance = (IDTOList)customType.newInstance();
        List<Object> lstDTO = instance.parse('['+response.getBody()+']');
        // call post deserlization transformation
        Type customType1 = Type.forName(responseProcessingDTO.setResponseProcessingDTRName());
        IDTR instance1 = (IDTR)customType1.newInstance();
        for(Object objDTO:lstDTO){
        	instance1.transformUpload(objDTO,woId,processId,cv);
        } 
    }
    public virtual void  uploadResourceToURL(String URL,String woId, IFileUploadDownload responseProcessingDTO,IFileUploadDownload requestProcessingDTR){
        List<workorder> woList =[select id,CorrelationID__c from workorder where id=:woId];
       HttpRequest request = new HttpRequest();
        //URL ='https://anypoint.mulesoft.com/mocking/api/v1/links/1bdfa19b-30cc-476c-9aa4-61913aa7c103/media';
        request.setEndpoint(URL);
        request.setMethod('PUT');
        request.setHeader('content-type', 'multipart/form-data');
        //request.setHeader('Content-Type','application/JSON');
        request.setBodyAsBlob(cv.VersionData);
       // String uploadBody = '{"fileContent":'+ '"'+EncodingUtil.Base64Encode(cv.VersionData)+'","fileName":'+'"'+cv.Title+'"}';
        //System.debug('uploadBody1'+uploadBody);
        //request.setBody(uploadBody);
        //request.setBody(strBody);
        Http http = new Http();
		HttpResponse response = http.send(request);
        //System.debug('SAYALI DEBUG POST FILE RESPONSE:'+response.getBody());
		
    }
    
    private void callout(){}
    
    private void processresponse(){
        

    }
    
}