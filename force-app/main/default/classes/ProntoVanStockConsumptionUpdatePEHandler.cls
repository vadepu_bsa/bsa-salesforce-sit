/**
* @author          Karan Shekhar
* @date            20/Jan/2020	
* @description    
* Change Date    Modified by         Description  
* 20/Jan/2020    Karan Shekhar		Handler class for Pronto_Van_Stock_Consumption_Update__e Event
**/

public class ProntoVanStockConsumptionUpdatePEHandler extends  PlatformEventBaseHandler{
 
    
    private List<sObject> mPlatformEventsToPublish;   
    private Boolean mPublishPE;
    
    public ProntoVanStockConsumptionUpdatePEHandler (SObjectType sObjectRecord, List<sObject> sObjectList, Boolean shouldPublishPE){        
        super(sObjectRecord, sObjectList, shouldPublishPE); 
    } 
    
    public override sObject mapPlatformEventFields(sObject prodConsumedRecordToProcess){
        System.debug('mapPlatformEventFields Mapping PE');
        Pronto_Van_Stock_Consumption_Update__e event = new Pronto_Van_Stock_Consumption_Update__e();
        ProductConsumed prodConsumedRecord = (ProductConsumed)prodConsumedRecordToProcess;
        event.CorrelationID__c = prodConsumedRecord.CorrelationID__c;
        event.Customer_Code__c = prodConsumedRecord.Customer_Code__c;
        event.Delta_Consumption__c = prodConsumedRecord.Delta_Consumption__c;
        event.Platform_Event_Name__c = BSA_ConstantsUtility.PE_VAN_STOCK_CONSUMPTION;
        event.Product_Consumed_Number__c = prodConsumedRecord.ProductConsumedNumber;
        event.Quantity_Consumed__c = prodConsumedRecord.QuantityConsumed;
        event.Sales_Order_Number__c = prodConsumedRecord.Sales_Order_Number__c;
        event.Serial_Number__c = prodConsumedRecord.ProductItemid != null ? prodConsumedRecord.ProductItem.SerialNumber:'' ;
        event.Stock_Code__c = prodConsumedRecord.ProductItemid != null ? prodConsumedRecord.ProductItem.Product_code__c :'' ;
        event.Time_Stamp__c =  System.Now();
        event.Warehouse_Code__c = prodConsumedRecord.ProductItemid != null ? (prodConsumedRecord.ProductItem.LocationId !=null ? prodConsumedRecord.ProductItem.Location.van_id__c: '') : '';
        event.Work_Order_Number__c = prodConsumedRecord.WorkOrder.WorkorderNumber;
        
        RETURN event;
    }
}