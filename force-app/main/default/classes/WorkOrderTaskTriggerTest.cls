@isTest
public class WorkOrderTaskTriggerTest {
     @testsetup
    public static void setup(){
        Account acc = NBNTestDataFactory.createTestAccounts();
         WorkType workType =  NBNTestDataFactory.createTestWorkType();
        Pricebook2 priceBook = NBNTestDataFactory.createTestPriceBook();
        OperatingHours operatingHrs = NBNTestDataFactory.createTestOperatingHours();
        ServiceTerritory servTerr = NBNTestDataFactory.createTestServiceTerritory(operatingHrs.id);
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        
        NBNTestDataFactory.createTestWOARS(acc.id,workType.id,servTerr.id,priceBook.id,serviceContract.Id);
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
    }
    @isTest
    public static void insertTasksOnParentWO(){
        WorkOrder workorder = [select id from workorder where parentWorkOrderId=null limit 1 ];
        WorkOrder woChild = [select id,Service_Resource__c from WorkOrder where parentWorkOrderId=:workorder.Id LIMIT 1];
		ServiceResource serviceRes = NBNTestDataFactory.createTestResource();
        NBNNAWorkOrderServiceImplementation.isWOUpdated =false;
		woChild.Service_Resource__c = serviceRes.Id;
        update woChild;
        Test.startTest();
        work_order_task__c objTask =NBNTestDataFactory.createWOTasks(workorder.Id);
        NBNLogEntryTriggerHandler.TriggerDisabled = false;
       	objTask.Code__c ='123';
        update objTask;
        Test.stopTest();
        work_order_task__c objTask1 =[select id from work_order_task__c LIMIT 1];
        System.assert(objTask1!=null);
        WorkOrderTaskTriggerHandler.getProfileSettings();
    }    
 
}