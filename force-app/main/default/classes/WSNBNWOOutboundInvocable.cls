public class WSNBNWOOutboundInvocable {
	@InvocableMethod(label='WSNBNWOOutboundAction')
    public static void doAction(List<WSNBNWOOutboundInputs> actionInput){
        System.debug('DildarLog: actionInput - ' + actionInput);
        
        String spefication = actionInput[0].spefication;
        String clientWorkOrdrNumber = actionInput[0].clientWorkOrdrNumber;
        String action = actionInput[0].action;
        action = action.toLowerCase();
        String recordId = actionInput[0].Id;
        sObject sObjectRecord;
        WSNBNWOOutboundActionDTR dtr;
        
        if(action == 'update_contact' || action == 'add_contact'){
            sObjectRecord = new Work_Order_Contact__c(Id = recordId);
            dtr = new WSNBNWOOutboundActionDTR(action,sObjectRecord,spefication);
            dtr.fetchWOCRecords(((Work_Order_Contact__c)sObjectRecord).Id);            
        
        }else if(action == 'add_notes' || action == 'update_notes'){
            sObjectRecord = new NBN_Log_Entry__c(Id = recordId);
            dtr = new WSNBNWOOutboundActionDTR(action,sObjectRecord,spefication);
            dtr.fetchNotesRecords(((NBN_Log_Entry__c)sObjectRecord).Id);
        
        }else if(action == 'add_task' || action == 'update_task'){
            sObjectRecord = new Work_Order_Task__c(Id = recordId);
            dtr = new WSNBNWOOutboundActionDTR(action,sObjectRecord,spefication);
            dtr.fetchTaskRecords(((Work_Order_Task__c)sObjectRecord).Id);
            
        }else if(action == 'documentation_complete'){
            sObjectRecord = new WorkOrderLineItem(Id = recordId);
            dtr = new WSNBNWOOutboundActionDTR(action,sObjectRecord,spefication);
            dtr.fetchWOLIRecords(((WorkOrderLineItem)sObjectRecord).Id);
            
        }else{
            //this includes - Enrout, Onsite, Complete, Incomplete, update delivery level and Schedule Update actions
            sObjectRecord = new WorkOrder(Id = recordId);
            dtr = new WSNBNWOOutboundActionDTR(action,sObjectRecord,spefication);
            dtr.fetchWORecords(((WorkOrder)sObjectRecord).Id);
        }
                        
        System.debug('DildarLog: spefication - ' + spefication + ', recordId - ' + recordId + ', action - ' + action);
        
        dtr.initiateWOActions();
    }
    
	//input details that comes to apex from PB
	public class WSNBNWOOutboundInputs{
    	@InvocableVariable
        public String spefication;        
        @InvocableVariable
        public String clientWorkOrdrNumber;
        @InvocableVariable
        public String Id; 
        @InvocableVariable
        public String action;      
	}
}