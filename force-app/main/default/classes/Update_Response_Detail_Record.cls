public class Update_Response_Detail_Record {
    
    @InvocableMethod(label='Update Response Detail Records with Response ID')
    public static List<FlowOutputs> invokeThisMethod(List<FlowInputs> request) {
        
        List<FlowOutputs> results = new List<FlowOutputs>();
        for (FlowInputs fi : request){
            FlowOutputs fo = new FlowOutputs();
            fo = UpdateResponseDetailRecord(fi);
            results.add(fo);
        }
        return results;
    }
    
    
    //input details that comes to apex from flow
    public class FlowInputs{
        @InvocableVariable
        public List<Response_Details__c> input_Response_Details_List;
        @InvocableVariable
        public String response_ID;
    }
    
    //output details which goes from apex to flow
    public class FlowOutputs{
        @InvocableVariable
       // public List<Response_Details__c> output_Response_Details_List = new list<Response_Details__c>();
       public List<Response_Details__c> output_Response_Details_List = new list<Response_Details__c>();
    }
    
    public static FlowOutputs UpdateResponseDetailRecord(FlowInputs fi){
        FlowOutputs fo = new FlowOutputs();
        
        List<Response_Details__c> updated_Response_List = new List<Response_Details__c>();
        Id recordID = Id.valueOf(fi.response_ID);        
        //system.debug('Response Reord ID------->' + recordID);
        For (Response_Details__c rd :fi.input_Response_Details_List ){
            rd.Response__c = recordID;
            updated_Response_List.add(rd);
            //system.debug('RD ------>' + rd);
        }
        
        //system.debug('updated_Response_List ------->' + updated_Response_List);
        //system.debug('Before output_Response_Details_List ------->' + fo.output_Response_Details_List);
        
         
        fo.output_Response_Details_List = updated_Response_List;

        //system.debug('After output_Response_Details_List ------->' + fo.output_Response_Details_List);
        
        return fo;
    }    
}