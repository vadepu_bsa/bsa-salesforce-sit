@isTest
public class BatchUpsertSimplexWOTest {
	static testMethod void testSimplexActivities(){   
		
			//BSA_TestDataFactory.createSeedDataForTesting();	
        
        	Simplex_Work_Order_Staging__c swos = new Simplex_Work_Order_Staging__c();
        	swos.Cause_Code__c = 'P140';
        	swos.Activity_Type__c = 'Test';
        	swos.Actual_Onsite_Response__c = System.Now();
        	swos.Actual_WO_Complete_Date_Time__c = System.Now();
            swos.Address__c = 'Sydney';
            swos.Business_WO_Type__c = 'NONE';
            swos.Classification__c = 'Test';
            swos.Closure_Summary__c = 'Test';
            swos.Location_Name__c = 'Test-TestVan1';
            swos.Location_SDU_MDU__c = 'SMDD';
            swos.WO_External_Id__c = 'WOR700078429209';
        swos.State__c ='VIC';
        swos.Technology__c ='HFC';
        
        	Insert swos;
        swos = new Simplex_Work_Order_Staging__c();
        	swos.Cause_Code__c = 'P140';
        	swos.Activity_Type__c = 'Test';
        	swos.Actual_Onsite_Response__c = System.Now();
        	swos.Actual_WO_Complete_Date_Time__c = System.Now();
            swos.Address__c = 'Sydney';
            swos.Business_WO_Type__c = 'NONE';
            swos.Classification__c = 'Test';
            swos.Closure_Summary__c = 'Test';
            swos.Location_Name__c = 'Test-TestVan1';
            swos.Location_SDU_MDU__c = 'SMDD';
            swos.WO_External_Id__c = 'WOR700078429209';
        swos.Location_Enable_Id__c='SR001';
        	
        	Insert swos;
        	Simplex_Activity_Staging__c sas = new Simplex_Activity_Staging__c();
        	sas.Work_Order_Id__c = 'WOR700078429209';
            sas.Work_Order_Lines_Description__c = 'WOR700078429209DESC';
            sas.SOR_Id__c = '03-23-00-01';
            sas.SOR_Quantity__c = '1';
            sas.Task_Id__c = 'RTASK0002422243';
            sas.Task_Approval_Required__c = 'Approved';
            sas.Task_Classification__c = 'Classification';
            sas.Task_Code__c = 'TSK000452';
            sas.Task_Comments__c = 'Test';
            sas.Task_Length__c = 'Test';
            sas.Task_Quantity__c = '1';
            sas.Task_Status__c = 'Test Open';
            sas.Task_Title__c = 'Test Task';
            sas.SAM_Id__c = 'XWA';
        	sas.Documentation_Complete__c ='Y';	
        	Insert sas;
        Id glCodeRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByDeveloperName().get('GL_Code_Automation').getRecordTypeId();
        Work_Order_Automation_Rule__c woarObj = new Work_Order_Automation_Rule__c();
        woarObj.recordTypeId = glCodeRecordTypeId;
        woarObj.State__c ='VIC';
        woarObj.Primary_Access_Technology__c ='HFC';
        woarObj.GL_Code__c ='SBI6775';
        woarObj.record_Type_name__c ='Simplex_CUI_Work_Order';
        woarObj.Service_Contract__c=Label.Unify_Services_Simplex_Contract_Id;
        woarObj.Active__c=true;
        insert woarObj;
        Id prontoCodeRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByDeveloperName().get('Pronto_Territory_Automation').getRecordTypeId();
        Work_Order_Automation_Rule__c woarObj1 = new Work_Order_Automation_Rule__c();
        woarObj1.recordTypeId = glCodeRecordTypeId;
        woarObj1.State__c ='VIC';
        woarObj1.Primary_Access_Technology__c ='HFC';
        woarObj1.Pronto_Territory_Id__c ='BVCO';
        woarObj1.record_Type_name__c ='Simplex_CUI_Work_Order';
        woarObj1.Service_Contract__c=Label.Unify_Services_Simplex_Contract_Id;
        woarObj1.Active__c=true;
        insert woarObj1;
        Test.startTest();
            //BatchUpsertSimplexWO batchObj = new BatchUpsertSimplexWO();
            //DataBase.executeBatch(batchObj); 
        ScheduleBatchUpsertSimplexWO sh1 = new ScheduleBatchUpsertSimplexWO();

		String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();
    }
}