public class NBNLogEntryTriggerHandler implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
    public static Map<Id,WorkOrder> workOrderMapForMapping = new Map<Id,WorkOrder>();
    public static Boolean taskGenerationInProgress = false;
	
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }
    
    public void BeforeInsert(List<SObject> nbnLogList) { 
        NBNLogEntryTriggerHelper.mapDirection(nbnLogList);
    }
    public void AfterInsert(Map<Id, SObject> newItems) { 
        set<Id> parentWoIds = new set<Id>();
        Set<Id> recIds = new Set<Id>();
        List<NBN_Log_Entry__c> lstWoLogToInsert = new List<NBN_Log_Entry__c>();
        List<NBN_Log_Entry__c> lstWoLogToUpdate = new List<NBN_Log_Entry__c>();
        Map<Id,NBN_Log_Entry__c> mWoLogMap = new Map<Id,NBN_Log_Entry__c>();
        for(SObject obj : newItems.values()) {
            NBN_Log_Entry__c logObj = (NBN_Log_Entry__c)obj;
            System.debug('logobj.Unify_External_Note_Id__c=='+logobj.Unify_External_Note_Id__c);
            System.debug('logObj.Parent_WO_Id__c=='+logobj.Parent_WO_Id__c);
            if(logObj.Parent_WO_Id__c== null || logObj.Parent_WO_Id__c==''){
            	parentWoIds.add(logObj.Work_Order__c);
                mWoLogMap.put(logObj.Id,logObj);
            }
            else if(logobj.Unify_External_Note_Id__c=='' || logobj.Unify_External_Note_Id__c==null){
                recIds.add(logObj.Id);
                NBN_Log_Entry__c parentLog = logObj.clone(false, false, false, false); 
                parentLog.Work_Order__c =logObj.Parent_WO_Id__c;
                parentLog.Unify_External_Note_Id__c ='FSL'+logObj.Id;
                lstWoLogToInsert.add(parentLog);
            }
        }
        System.debug('lstWoLogToInsert=='+lstWoLogToInsert);
        if(recIds!=null && recIds.size()>0){
            Map<String,Schema.DisplayType> mApiNameToSchemaType= UtilityClass.fetchFieldTypesforObject('NBN_Log_Entry__c');
            String fieldsForSOQL = UtilityClass.buildFieldsForSOQLQuery(mApiNameToSchemaType.keySet());
            String woLogQuery = 'SELECT '+ fieldsForSOQL+ ' FROM NBN_Log_Entry__c WHERE Id IN :recIds';
        
        	Map<id,NBN_Log_Entry__c> mWoLog =new Map<id,NBN_Log_Entry__c>((List<NBN_Log_Entry__c>)Database.query(woLogQuery));
            for(NBN_Log_Entry__c objLog:mWoLog.values() ){
                objLog.Unify_External_Note_Id__c ='FSL'+ objLog.Id+':'+objLog.Work_Order__c;
                lstWoLogToUpdate.add(objLog);
            }
            if(lstWoLogToUpdate!=null && lstWoLogToUpdate.size()>0){
                TriggerDisabled = true;
                Update lstWoLogToUpdate;
            }
        }
        List<WorkOrder> lstChildWos =[select id,parentWorkOrderId from WorkOrder where parentWorkOrderId in:parentWoIds and Service_Resource__c!='' ];
        if(lstChildWos!=null && lstChildWos.size()>0){
        Map<Id,List<WorkOrder>> mparenttoChildWos = new Map<Id,List<WorkOrder>>();
        for(WorkOrder objWO : lstChildWos){
            List<WorkOrder> tempLst;
            if(mparenttoChildWos.keyset().contains(objWO.ParentWorkOrderId)){
                tempLst = mparenttoChildWos.get(objWO.ParentWorkOrderId);
            }
            else{
                tempLst = new List<WorkOrder>();
            }
            tempLst.add(objWO);
            mparenttoChildWos.put(objWO.ParentWorkOrderId,tempLst);
        }
        for(SObject obj : mWoLogMap.values()) {
            NBN_Log_Entry__c objLog = (NBN_Log_Entry__c)obj;
            List<WorkOrder> lstChilds =mparenttoChildWos.get(objLog.Work_Order__c);
            for(WorkOrder objChild: lstChilds){
                NBN_Log_Entry__c childLog = objLog.clone(false, false, false, false); 
                childLog.Work_Order__c =objChild.id;
                childLog.Unify_External_Note_Id__c =childLog.Unify_External_Note_Id__c +':'+(String)objChild.id;
                lstWoLogToInsert.add(childLog);
            }
        }
        
        }
        if(lstWoLogToInsert!=null && lstWoLogToInsert.size()>0){
            TriggerDisabled = true;
            Upsert lstWoLogToInsert Unify_External_Note_Id__c;
        }
         
        
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<sObject> lstNoteEntry = new List<sObject>();
        for(SObject obj : newItems.values()) {
            NBN_Log_Entry__c logEntryObj = (NBN_Log_Entry__c)obj;
            NBN_Log_Entry__c oldLogEntryObj = (NBN_Log_Entry__c)oldItems.get(logEntryObj.Id);
            if(logEntryObj.Source__c!=oldLogEntryObj.Source__c){
                lstNoteEntry.add(obj);
            }
        }
        if(lstNoteEntry!=null && lstNoteEntry.size()>0){
        	NBNLogEntryTriggerHelper.mapDirection(lstNoteEntry);
        }
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<NBN_Log_Entry__c> UpdateParentTasksFromChild = new List<NBN_Log_Entry__c>();
        List<NBN_Log_Entry__c> UpdateChildTasksFromParent = new List<NBN_Log_Entry__c>();
        Map<Id,List<NBN_Log_Entry__c>> mIdtoWotask = new Map<Id,List<NBN_Log_Entry__c>>();
        set<Id> parentWoIds = new set<Id>();
        System.debug('TriggerDisabled -->'+TriggerDisabled);
        if(TriggerDisabled) return;
        for(SObject obj : newItems.values()) {
            NBN_Log_Entry__c woTaskObj = (NBN_Log_Entry__c)obj;
            NBN_Log_Entry__c oldWoTaskObj = (NBN_Log_Entry__c)oldItems.get(woTaskObj.Id);
            if(woTaskObj.Parent_WO_Id__c!=null && woTaskObj.Parent_WO_Id__c!=''){
                String extId = woTaskObj.Unify_External_Note_Id__c;
                List<String> extIds = extId.split(':');
                NBN_Log_Entry__c objWotask = woTaskObj.clone(false, false, false, false); 
                objWotask.Unify_External_Note_Id__c = extIds[0];
                objWotask.Work_Order__c = woTaskObj.Parent_WO_Id__c;
                UpdateParentTasksFromChild.add(objWotask);
            }
            else{
                parentWoIds.add(woTaskObj.Work_Order__c);
                List<NBN_Log_Entry__c> newTaskList;
                if(mIdtoWotask.keyset().contains(woTaskObj.Work_Order__c)){
                    newTaskList = mIdtoWotask.get(woTaskObj.Work_Order__c);
                }
                else{
                    newTaskList = new List<NBN_Log_Entry__c>();
                }
                newTaskList.add(woTaskObj);
                mIdtoWotask.put(woTaskObj.Work_Order__c,newTaskList);
            }
        }
        if(UpdateParentTasksFromChild!=null && UpdateParentTasksFromChild.size()>0){
            Upsert UpdateParentTasksFromChild Unify_External_Note_Id__c;
            TriggerDisabled =true;
        }
        if(parentWoIds!=null & parentWoIds.size()>0)
        {
            List<WorkOrder> lstChildWos = [select id,ParentWorkOrderId from workorder where ParentWorkOrderId in:parentWoIds and Service_Resource__c!=''];
            for(WorkOrder objWO: lstChildWos){
                
                for(NBN_Log_Entry__c objTask: mIdtoWotask.get(objWO.ParentWorkOrderId)){
                    NBN_Log_Entry__c objWotask = objTask.clone(false, false, false, false); 
                    objWotask.Unify_External_Note_Id__c = objTask.Unify_External_Note_Id__c+':'+objWO.Id;
                    objWotask.Work_Order__c =objWO.Id;
                    UpdateChildTasksFromParent.add(objWotask);
                }
            }
        }
        if(UpdateChildTasksFromParent!=null && UpdateChildTasksFromParent.size()>0){
            TriggerDisabled =true;
            System.debug('TriggerDisabled inSide If--'+TriggerDisabled);
        	upsert UpdateChildTasksFromParent Unify_External_Note_Id__c;
            
        }
        
        
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}


}