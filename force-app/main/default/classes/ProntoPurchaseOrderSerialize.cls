/**
 * @File Name          : ProntoPurchaseOrderSerialize.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 13/08/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    13/08/2020         Tom Henson             Initial Version
**/
public class ProntoPurchaseOrderSerialize {
    
    public static void generateProntoPurchaseOrderPlatformEvent (set<id> orderIdSet, map<id,string> orderItemOperationMap){
        
        List<PurchaseOrderDetails> finalOrderList = purchaseOrderSerialize(orderIdSet);
        List<OrderItemEntryDetails> finalOrderItemEntryList = purchaseOrderItemSerialize(orderIdSet, orderItemOperationMap);
        
        List<Action_Order_From_BSA__e> InsertplatformEventList = generatePlatformEvents(finalOrderList,finalOrderItemEntryList);
        string logs =  JSON.serializePretty(InsertplatformEventList); 
        System.Debug('!!! Full Output');   
        for (Integer i = 0; i < logs.length(); i=i+300) {
            Integer iEffectiveEnd = (i+300 > (logs.length()-1) ? logs.length()-1 : i+300);
            System.debug(logs.substring(i,iEffectiveEnd));
          }
        if (InsertplatformEventList.size() > 0 ) {
            List<Database.SaveResult> results = EventBus.publish(InsertplatformEventList);
        }
    } 
    
    public static List<PurchaseOrderDetails>  purchaseOrderSerialize(set<id> purchaseOrderIdSet){
        
        Id recordType = Schema.Sobjecttype.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        List<Order> purchaseOrderObj = [SELECT AccountId, Account.CustomerCode__c, ActivatedById, ActivatedDate,BillingAddress,BillingCity,BillingCountry,BillingGeocodeAccuracy,BillingLatitude,
                                                    BillingLongitude,BillingPostalCode,BillingState,BillingStreet,CompanyAuthorizedById, ContractId,CustomerAuthorizedById,Description,
                                                    EffectiveDate,EndDate,IsReductionOrder,OrderNumber,OriginalOrderId,OriginalOrder.OrderNumber,ShippingAddress,ShippingCity,ShippingCountry,CustomerAuthorizedBy.ID__c,
                                                    ShippingGeocodeAccuracy,ShippingLatitude,ShippingLongitude,ShippingPostalCode,ShippingState,ShippingStreet,Status,StatusCode,Supplier__c, Supplier__r.Supplier_Account_Code__c,
                                                    TotalAmount,Type,Work_Order__c,Work_Order__r.Client_Work_Order_Number__c,  Originator_Type__c
                                                    FROM Order 
                                                    WHERE RecordTypeId = :recordType AND id =: purchaseOrderIdSet]; 
        List<PurchaseOrderDetails> purchaseOrderDetailsList = new List<PurchaseOrderDetails>();
        for (integer i = 0; i < purchaseOrderObj.size(); i++){
            PurchaseOrderDetails pObj = new PurchaseOrderDetails();
           
            pobj.Id = (purchaseOrderObj[i].Id != null) ? purchaseOrderObj[i].Id : null;
            pobj.AccountId = (purchaseOrderObj[i].AccountId != null && purchaseOrderObj[i].Account.CustomerCode__c != null  ) ? purchaseOrderObj[i].Account.CustomerCode__c : null;
          //  pobj.ActivatedById = (purchaseOrderObj[i].ActivatedById != null && purchaseOrderObj[i].ActivatedBy.Rep_Code__c != null ) ? purchaseOrderObj[i].ActivatedBy.Rep_Code__c : null;
            pobj.ActivatedDate = (purchaseOrderObj[i].ActivatedDate != null) ? string.valueof(purchaseOrderObj[i].ActivatedDate) : null;
            pobj.BillingAddress = (purchaseOrderObj[i].BillingAddress != null) ? string.valueof(purchaseOrderObj[i].BillingAddress) : null;
            pobj.BillingCity = (purchaseOrderObj[i].BillingCity != null) ? purchaseOrderObj[i].BillingCity : null;
            pobj.BillingCountry = (purchaseOrderObj[i].BillingCountry != null) ? purchaseOrderObj[i].BillingCountry : null;
            pobj.BillingGeocodeAccuracy = (purchaseOrderObj[i].BillingGeocodeAccuracy != null) ? purchaseOrderObj[i].BillingGeocodeAccuracy : null;
            pobj.BillingLatitude = (purchaseOrderObj[i].BillingLatitude != null) ? string.valueof(purchaseOrderObj[i].BillingLatitude) : null;
            pobj.BillingLongitude = (purchaseOrderObj[i].BillingLongitude != null) ? string.valueof(purchaseOrderObj[i].BillingLongitude) : null;
            pobj.BillingPostalCode = (purchaseOrderObj[i].BillingPostalCode != null) ? purchaseOrderObj[i].BillingPostalCode : null;
            pobj.BillingState = (purchaseOrderObj[i].BillingState != null) ? purchaseOrderObj[i].BillingState : null;
            pobj.BillingStreet = (purchaseOrderObj[i].BillingStreet != null) ? purchaseOrderObj[i].BillingStreet : null;
          //  pobj.CompanyAuthorizedById = (purchaseOrderObj[i].CompanyAuthorizedById != null && purchaseOrderObj[i].CompanyAuthorizedBy.Rep_code__c != null ) ? purchaseOrderObj[i].CompanyAuthorizedBy.Rep_code__c : null;
            //pobj.ContractId = (purchaseOrderObj[i].ContractId != null) ? purchaseOrderObj[i].ContractId : null;
            pobj.CustomerAuthorizedById = (purchaseOrderObj[i].CustomerAuthorizedById != null && purchaseOrderObj[i].CustomerAuthorizedBy.ID__c != null) ? purchaseOrderObj[i].CustomerAuthorizedBy.ID__c : null;
            pobj.Description = (purchaseOrderObj[i].Description != null) ? purchaseOrderObj[i].Description : null;
            pobj.EffectiveDate = (purchaseOrderObj[i].EffectiveDate != null) ? string.valueof(purchaseOrderObj[i].EffectiveDate) : null;
            pobj.EndDate = (purchaseOrderObj[i].EndDate != null) ? string.valueof(purchaseOrderObj[i].EndDate) : null;
            pobj.IsReductionOrder = (purchaseOrderObj[i].IsReductionOrder != null) ? string.valueof(purchaseOrderObj[i].IsReductionOrder) : null;
            pobj.OrderNumber = (purchaseOrderObj[i].OrderNumber != null) ? purchaseOrderObj[i].OrderNumber : null;
            pobj.OriginalOrderId = (purchaseOrderObj[i].OriginalOrderId != null) ? purchaseOrderObj[i].OriginalOrder.OrderNumber : null;
          //  pobj.OwnerId = (purchaseOrderObj[i].OwnerId != null && purchaseOrderObj[i].Owner.Rep_code__c != null ) ? purchaseOrderObj[i].Owner.Rep_code__c : null;
            pobj.ShippingAddress = (purchaseOrderObj[i].ShippingAddress != null) ? string.valueof(purchaseOrderObj[i].ShippingAddress) : null;
            pobj.ShippingCity = (purchaseOrderObj[i].ShippingCity != null) ? purchaseOrderObj[i].ShippingCity : null;
            pobj.ShippingCountry = (purchaseOrderObj[i].ShippingCountry != null) ? purchaseOrderObj[i].ShippingCountry : null;
            pobj.ShippingGeocodeAccuracy = (purchaseOrderObj[i].ShippingGeocodeAccuracy != null) ? purchaseOrderObj[i].ShippingGeocodeAccuracy : null;
            pobj.ShippingLatitude = (purchaseOrderObj[i].ShippingLatitude != null) ? string.valueof(purchaseOrderObj[i].ShippingLatitude) : null;
            pobj.ShippingLongitude = (purchaseOrderObj[i].ShippingLongitude != null) ? string.valueof(purchaseOrderObj[i].ShippingLongitude) : null;
            pobj.ShippingPostalCode = (purchaseOrderObj[i].ShippingPostalCode != null) ? purchaseOrderObj[i].ShippingPostalCode : null;
            pobj.ShippingState = (purchaseOrderObj[i].ShippingState != null) ? purchaseOrderObj[i].ShippingState : null;
            pobj.ShippingStreet = (purchaseOrderObj[i].ShippingStreet != null) ? purchaseOrderObj[i].ShippingStreet : null;
            pobj.Status = (purchaseOrderObj[i].Status != null) ? purchaseOrderObj[i].Status : null;
            pobj.StatusCode = (purchaseOrderObj[i].StatusCode != null) ? purchaseOrderObj[i].StatusCode : null;
            pobj.TotalAmount = (purchaseOrderObj[i].TotalAmount != null) ? string.valueof(purchaseOrderObj[i].TotalAmount) : null;
            pobj.Type = (purchaseOrderObj[i].Type != null) ? purchaseOrderObj[i].Type : null;
            pobj.Work_Order = (purchaseOrderObj[i].Work_Order__c != null && purchaseOrderObj[i].Work_Order__r.Client_Work_Order_Number__c != null) ? purchaseOrderObj[i].Work_Order__r.Client_Work_Order_Number__c : null;
            pobj.Supplier = (purchaseOrderObj[i].Supplier__c != null && purchaseOrderObj[i]. Supplier__r.Supplier_Account_Code__c != null)  ? purchaseOrderObj[i].Supplier__r.Supplier_Account_Code__c : null;
            pobj.OriginatorType = (purchaseOrderObj[i].Originator_Type__c != null )  ? purchaseOrderObj[i].Originator_Type__c : null;
            purchaseOrderDetailsList.add(pObj);
            system.debug('JSON Serialize ' + JSON.serialize(pObj,false));
        }
        system.debug('JSON Serialize ALL ' + JSON.serialize(purchaseOrderDetailsList,false));
        return purchaseOrderDetailsList;
    }

    public static List<OrderItemEntryDetails>  purchaseOrderItemSerialize(set<id> purchaseOrderIdSet,  map<id,string> orderItemOperationMap){
        List<OrderItem> orderItemEntryObj = [SELECT AvailableQuantity,Charge_out_fee__c,Charge_Type__c,Description,EndDate,Labour_Total__c,ListPrice,Markup_Price__c,No_of_Hours__c,
                                             OrderId, Order.OrderNumber, OrderItemNumber,OriginalOrderItemId,Quantity,ServiceDate,Technician_Category__c,TotalPrice,UnitPrice, Product2Id, 
                                             Product2.Product_Code__c,  Product2.ProductCode, Product2.Type__c, Status__c, Id
                                                FROM OrderItem
                                                WHERE OrderId = :purchaseOrderIdSet]; 
        List<OrderItemEntryDetails> OrderItemEntryDetailsList = new List<OrderItemEntryDetails>();
        for (integer i = 0; i < orderItemEntryObj.size(); i++){
            OrderItemEntryDetails oiObj = new OrderItemEntryDetails();
            
            oiObj.AvailableQuantity = (orderItemEntryObj[i].AvailableQuantity != null) ? string.valueof(orderItemEntryObj[i].AvailableQuantity) : null; 
            oiObj.Charge_out_fee = (orderItemEntryObj[i].Charge_out_fee__c != null) ? string.valueof(orderItemEntryObj[i].Charge_out_fee__c) : null; 
            oiObj.Charge_Type = (orderItemEntryObj[i].Charge_Type__c != null) ? orderItemEntryObj[i].Charge_Type__c : null; 
            oiObj.Description = (orderItemEntryObj[i].Description != null) ? orderItemEntryObj[i].Description : null; 
            oiObj.EndDate = (orderItemEntryObj[i].EndDate != null) ? string.valueof(orderItemEntryObj[i].EndDate) : null; 
            oiObj.Labour_Total = (orderItemEntryObj[i].Labour_Total__c != null) ? string.valueof(orderItemEntryObj[i].Labour_Total__c) : null; 
            oiObj.ListPrice = (orderItemEntryObj[i].ListPrice != null) ? string.valueof(orderItemEntryObj[i].ListPrice) : null; 
            oiObj.Markup_Price = (orderItemEntryObj[i].Markup_Price__c != null) ? string.valueof(orderItemEntryObj[i].Markup_Price__c) : null; 
            oiObj.No_of_Hours = (orderItemEntryObj[i].No_of_Hours__c != null) ? string.valueof(orderItemEntryObj[i].No_of_Hours__c) : null; 
            oiObj.OrderId = (orderItemEntryObj[i].OrderId != null) ? orderItemEntryObj[i].Order.OrderNumber : null; 
            oiObj.OrderItemNumber = (orderItemEntryObj[i].OrderItemNumber != null) ? orderItemEntryObj[i].OrderItemNumber : null; 
           // oiObj.OriginalOrderItemId = (orderItemEntryObj[i].OriginalOrderItemId != null) ? orderItemEntryObj[i].OriginalOrderItemId : null; 
            oiObj.Quantity = (orderItemEntryObj[i].Quantity != null) ? string.valueof(orderItemEntryObj[i].Quantity) : null; 
            oiObj.ServiceDate = (orderItemEntryObj[i].ServiceDate != null) ? string.valueof(orderItemEntryObj[i].ServiceDate) : null; 
            //oiObj.Supplier = (orderItemEntryObj[i].Supplier__c != null) ? orderItemEntryObj[i].Supplier__c : null; 
            oiObj.Technician_Category = (orderItemEntryObj[i].Technician_Category__c != null) ? orderItemEntryObj[i].Technician_Category__c : null; 
            oiObj.TotalPrice = (orderItemEntryObj[i].TotalPrice != null) ? string.valueof(orderItemEntryObj[i].TotalPrice) : null; 
            oiObj.UnitPrice = (orderItemEntryObj[i].UnitPrice != null) ? string.valueof(orderItemEntryObj[i].UnitPrice) : null; 
            oiObj.product = (orderItemEntryObj[i].product2Id != null && orderItemEntryObj[i].Product2.Product_Code__c != null) ? string.valueof(orderItemEntryObj[i].Product2.Product_Code__c) : null;
            if ( oiObj.product == null){
                oiObj.product = (orderItemEntryObj[i].product2Id != null && orderItemEntryObj[i].Product2.ProductCode != null) ? string.valueof(orderItemEntryObj[i].Product2.ProductCode) : null;
                
            }
            oiObj.producttype = (orderItemEntryObj[i].product2Id != null && orderItemEntryObj[i].Product2.Type__c != null) ? string.valueof(orderItemEntryObj[i].Product2.Type__c) : null; 
            oiObj.status = (orderItemEntryObj[i].status__c != null) ? string.valueof(orderItemEntryObj[i].status__c) : null; 
            oiObj.SfOrderId = (orderItemEntryObj[i].OrderId != null) ? orderItemEntryObj[i].Order.Id : null; 
            oiObj.Operation = 'Insert';
            if ( orderItemOperationMap != null && !orderItemOperationMap.IsEmpty()){
                if (orderItemOperationMap.containsKey(orderItemEntryObj[i].Id)){
                    oiObj.Operation = orderItemOperationMap.get(orderItemEntryObj[i].Id);
                }
            }
            OrderItemEntryDetailsList.add(oiObj);
            system.debug('JSON Serialize Item ' + JSON.serialize(oiObj,false));
        }
        system.debug('JSON Serialize ALL Items ' + JSON.serialize(OrderItemEntryDetailsList,false));
        return OrderItemEntryDetailsList;
    }
    
    Public Static List<Action_Order_From_BSA__e> generatePlatformEvents(List<PurchaseOrderDetails> finalOrderList, List<OrderItemEntryDetails> finalOrderItemEntryList){
       List<Action_Order_From_BSA__e> returnlist = new List<Action_Order_From_BSA__e>();
        for (integer i = 0 ; i < finalOrderList.size(); i++){
            Action_Order_From_BSA__e eventObj = new Action_Order_From_BSA__e();

            List<OrderItemEntryDetails> relatedOrderItemList = new List<OrderItemEntryDetails>();
            for (OrderItemEntryDetails oie : finalOrderItemEntryList){
                System.debug('Order Item Id '+oie.OrderId +'Order Id '+finalOrderList[i].Id);
                if (oie.SfOrderId == finalOrderList[i].Id){
                    relatedOrderItemList.add(oie);
                }
            }

            eventObj.Action__c = 'Generate_Order';
            eventObj.CorrelationId__c  = UtilityClass.getUUID();
            eventObj.OrderHeader__c = '"orderDetails":' + JSON.serialize(finalOrderList[i],true);
            eventObj.OrderItemEntry__c = '"orderItemEntryDetails":' + JSON.serialize(relatedOrderItemList,true);
            eventObj.EventTimeStamp__c = String.valueOf(System.DateTime.Now());
            returnlist.add(eventObj);
        }
        return returnlist;
    }
    Public Class PurchaseOrderDetails{
        public String Id {get;set;}
        public String AccountId {get;set;}
        public String ActivatedById {get;set;}
        public String ActivatedDate {get;set;}
        public String BillingAddress {get;set;}
        public String BillingCity {get;set;}
        public String BillingCountry {get;set;}
        public String BillingGeocodeAccuracy {get;set;}
        public String BillingLatitude {get;set;}
        public String BillingLongitude {get;set;}
        public String BillingPostalCode {get;set;}
        public String BillingState {get;set;}
        public String BillingStreet {get;set;}
        public String CompanyAuthorizedById {get;set;}
        //public String ContractId {get;set;}
        public String CustomerAuthorizedById {get;set;}
        public String Description {get;set;}
        public String EffectiveDate {get;set;}
        public String EndDate {get;set;}
        public String IsReductionOrder {get;set;}
        public String OrderNumber {get;set;}
        public String OriginalOrderId {get;set;}
       // public String OwnerId {get;set;}
        public String ShippingAddress {get;set;}
        public String ShippingCity {get;set;}
        public String ShippingCountry {get;set;}
        public String ShippingGeocodeAccuracy {get;set;}
        public String ShippingLatitude {get;set;}
        public String ShippingLongitude {get;set;}
        public String ShippingPostalCode {get;set;}
        public String ShippingState {get;set;}
        public String ShippingStreet {get;set;}
        public String Status {get;set;}
        public String StatusCode {get;set;}
        public String TotalAmount {get;set;}
        public String Type {get;set;}
        public String Work_Order {get;set;}
        public String Supplier {get;set;}
        public string OriginatorType {get;set;}
        
        public PurchaseOrderDetails(){
            AccountId = null;
            ActivatedById = null;
            ActivatedDate = null;
            BillingAddress = null;
            BillingCity = null;
            BillingCountry = null;
            BillingGeocodeAccuracy = null;
            BillingLatitude = null;
            BillingLongitude = null;
            BillingPostalCode = null;
            BillingState = null;
            BillingStreet = null;
            CompanyAuthorizedById = null;
           // ContractId = null;
            CustomerAuthorizedById = null;
            Description = null;
            EffectiveDate = null;
            EndDate = null;
            IsReductionOrder = null;
            OrderNumber = null;
            OriginalOrderId = null;
          //  OwnerId = null;
            ShippingAddress = null;
            ShippingCity = null;
            ShippingCountry = null;
            ShippingGeocodeAccuracy = null;
            ShippingLatitude = null;
            ShippingLongitude = null;
            ShippingPostalCode = null;
            ShippingState = null;
            ShippingStreet = null;
            Status = null;
            StatusCode = null;
            TotalAmount = null;
            Type = null;
            Work_Order = null;
            Supplier = null;
            OriginatorType = null;
        }
    }
    Public Class OrderItemEntryDetails{
        public String AvailableQuantity {get;set;}
        public String Charge_out_fee {get;set;}
        public String Charge_Type {get;set;}
        public String Description {get;set;}
        public String EndDate {get;set;}
        public String Labour_Total {get;set;}
        public String ListPrice {get;set;}
        public String Markup_Price {get;set;}
        public String No_of_Hours {get;set;}
        public String OrderId {get;set;}
        public String OrderItemNumber {get;set;}
        public String OriginalOrderItemId {get;set;}
        public String Quantity {get;set;}
        public String ServiceDate {get;set;}
        public String Supplier {get;set;}
        public String Technician_Category {get;set;}
        public String TotalPrice {get;set;}
        public String UnitPrice {get;set;}
        public String Product {get;set;}
        public String SfOrderId {get;set;}
        public string producttype {get;set;}
        public string status {get;set;}
        public string Operation {get;set;}
        
        public OrderItemEntryDetails(){

            AvailableQuantity = null;
            Charge_out_fee = null;
            Charge_Type = null;
            Description = null;
            EndDate = null;
            Labour_Total = null;
            ListPrice = null;
            Markup_Price = null;
            No_of_Hours = null;
            OrderId = null;
            OrderItemNumber = null;
            OriginalOrderItemId = null;
            Quantity = null;
            ServiceDate = null;
            Technician_Category = null;
            TotalPrice = null;
            UnitPrice = null;  
            product = null;
            SfOrderId = null;
            producttype = null;
            status = null;
            Operation = null;
        }
    }
}