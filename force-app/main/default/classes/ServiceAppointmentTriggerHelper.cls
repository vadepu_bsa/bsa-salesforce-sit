/**
* @author          IBM
* @date            06/June/2020 
* @description     
*
* Change Date        Modified by        Description  
* 06/June/2020       IBM                Created Trigger helper
**/
public class ServiceAppointmentTriggerHelper {

    public static void updateWorkOrderStatus(List<ServiceAppointment> serviceAppointmentList){
        List<WorkOrder> lstUpdateWO = new List<WorkOrder>();
        List<ServiceAppointment> lstSA = serviceAppointmentList;
        set<Id> workorderIdSet = new set<Id>();
        Map<Id,Workorder> workOrderMap = new Map<Id,Workorder>();
        map<Id,Integer> workorderServiceAppointmentNotCompleted = new Map<Id,Integer>();
        map<Id,Integer> workorderServiceAppointmentNoAccessMap = new Map<Id,Integer>();
        //Mahmood 18/08/2020 FS3-144 No Access Implementation. 
        for (integer i = 0; i < lstSA.size(); i++){
            if (String.isNotEmpty(lstSA[i].ParentRecordId)){
               Id saWOId = lstSA[i].ParentRecordId;
               String sObjName = saWOId.getSObjectType().getDescribe().getName();
               if (sObjName == 'WorkOrder' ){
                   WorkOrder wo = new WorkOrder(Id = lstSA[i].ParentRecordId);
                   workorderIdSet.add(wo.Id);
                }
            }
        }
        //Mahmood 18/08/2020 FS3-144 No Access Implementation. 
        if (workorderIdSet.size() > 0){
            workOrderMap = new Map<Id,Workorder>([SELECT ID, WorkOrderNumber, Access_Attempts__c, Total_No_of_Asset_WOLIs__c, No_of_Completed_Asset_Action_WOLI__c
                                                  FROM WorkOrder WHERE ID =:workorderIdSet]);
            List<ServiceAppointment> serviceAppointmentStatusList = [SELECT ParentRecordId, Status, Completion_Status__c FROM ServiceAppointment 
                                                                     WHERE  ParentRecordId =: workorderIdSet];
                                               
            for (integer i = 0; i < serviceAppointmentStatusList.size(); i++){
                if (serviceAppointmentStatusList[i].Status == 'Completed' && serviceAppointmentStatusList[i].Completion_Status__c == 'No Access'){
                    if (!workorderServiceAppointmentNoAccessMap.containsKey(serviceAppointmentStatusList[i].ParentRecordId)){
                        workorderServiceAppointmentNoAccessMap.put(serviceAppointmentStatusList[i].ParentRecordId, 1);
                    } else {
                        integer count = workorderServiceAppointmentNoAccessMap.get(serviceAppointmentStatusList[i].ParentRecordId);
                        count = count + 1;
                         workorderServiceAppointmentNoAccessMap.put(serviceAppointmentStatusList[i].ParentRecordId,count);    
                    }
                } else if ( serviceAppointmentStatusList[i].Status == 'New' ||
                             serviceAppointmentStatusList[i].Status == 'Scheduled' ||
                             serviceAppointmentStatusList[i].Status == 'Dispatched' ||
                             serviceAppointmentStatusList[i].Status == 'In Progress' ||
                             serviceAppointmentStatusList[i].Status == 'En-Route' ||
                             serviceAppointmentStatusList[i].Status == 'Accepted')
                    {
                        if (!workorderServiceAppointmentNotCompleted.containsKey(serviceAppointmentStatusList[i].ParentRecordId)){
                            workorderServiceAppointmentNotCompleted.put(serviceAppointmentStatusList[i].ParentRecordId, 1);
                        } else {
                            integer count = workorderServiceAppointmentNotCompleted.get(serviceAppointmentStatusList[i].ParentRecordId);
                            count = count + 1;
                            workorderServiceAppointmentNotCompleted.put(serviceAppointmentStatusList[i].ParentRecordId,count);    
                        } 
                               
                    }
            
            }
      
        }
        List<ServiceAppointment> newServiceAppointmentList = new List<ServiceAppointment>();
        for (integer i = 0; i < lstSA.size(); i++){
            if (String.isNotEmpty(lstSA[i].ParentRecordId)){
                Id saWOId = lstSA[i].ParentRecordId;
                String sObjName = saWOId.getSObjectType().getDescribe().getName();
                if (sObjName == 'WorkOrder' && (lstSA[i].Status == 'Completed')){
                    if ( lstSA[i].Completion_Status__c == 'No Further Action Required' && !workorderServiceAppointmentNotCompleted.containskey(lstSA[i].ParentRecordId)){
                            workorder existingWOObj = workOrderMap.get(lstSA[i].ParentRecordId);
                        if (existingWOObj.Total_No_of_Asset_WOLIs__c > existingWOObj.No_of_Completed_Asset_Action_WOLI__c ){
                            lstSA[i].addError('The Workorder has Incomplete WOLI associated to the workorder');
                        } else {
                            WorkOrder wo = new WorkOrder(Id = lstSA[i].ParentRecordId);
                            wo.Status = 'Completed';
                            wo.EndDate = datetime.now();
                            wo.Completion_Status__c = lstSA[i].Completion_Status__c;
                            lstUpdateWO.add(wo);
                        }
                    } else if (lstSA[i].Completion_Status__c != 'No Further Action Required') {
                        WorkOrder wo = new WorkOrder(Id = lstSA[i].ParentRecordId);
                        //Mahmood 18/08/2020 FS3-144 No Access Implementation. 
                        if (lstSA[i].Completion_Status__c == 'No Access'){
                            integer accountaccesscount = 3;
                            integer currentaccesscount = 0;
                            if (workOrderMap.containsKey(wo.Id) && workOrderMap.get(wo.Id).Access_Attempts__c != null){
                                accountaccesscount = Integer.valueof(workOrderMap.get(wo.Id).Access_Attempts__c) ;
                            }
                            if (workorderServiceAppointmentNoAccessMap.containsKey(wo.Id) && workorderServiceAppointmentNoAccessMap.get(wo.Id) != null){
                                currentaccesscount = workorderServiceAppointmentNoAccessMap.get(wo.Id);
                            }
                            if (currentaccesscount >= accountaccesscount){
                                wo.Status = 'Cannot Complete';
                            }else {
                                wo.Status = 'Incomplete';
                                newServiceAppointmentList.add(lstSA[i]);
                            }
                            
                        } else {
                            wo.Status = 'Incomplete';
                            newServiceAppointmentList.add(lstSA[i]);
                        }
                        wo.EndDate = datetime.now();
                        wo.Completion_Status__c = lstSA[i].Completion_Status__c;
                        lstUpdateWO.add(wo);
                    }
                }
            }
        }
    
        if (lstUpdateWO.size() > 0){
           Database.update(lstUpdateWO);
        }
 
    }


    //FSL3-158 - David Azzi - 22 July 2020
    //@future
    public static void ScheduledSLAsEnhancement(Set<Id> setServiceAppointment){

        System.debug('*-* ScheduledSLAsEnhancement start *-*');

        Map<Id, ServiceAppointment> mapWorkOrderSA = new Map<Id, ServiceAppointment>();
        Map<Id, ServiceAppointment> mapFilteredWorkOrderSA = new Map<Id, ServiceAppointment>();

        List<ServiceAppointment> lstServiceAppointment = [SELECT Id, ParentRecordId, DueDate FROM ServiceAppointment WHERE Id IN: setServiceAppointment];

        //make Id Set for query & map of all WOId/ServiceAppointments
        Integer lstSAsize = lstServiceAppointment.size();
        for(Integer i = 0; i < lstSAsize; i++){
            if (String.isNotBlank(lstServiceAppointment[i].ParentRecordId)){
                mapWorkOrderSA.put(lstServiceAppointment[i].ParentRecordId, lstServiceAppointment[i]);
            }
        }

        //create filtered list of WOId/ServiceAppointments
        List<WorkOrder> lstWorkOrderSA = [SELECT Id, WorkType.Name FROM WorkOrder WHERE Id IN: mapWorkOrderSA.keySet() AND (NOT WorkType.Name LIKE '%Preventative Maintenance%')]; //NOT IN WORKTYPE
        for(Integer i = 0; i < lstWorkOrderSA.size(); i++){
            mapFilteredWorkOrderSA.put(lstWorkOrderSA[i].Id, mapWorkOrderSA.get(lstWorkOrderSA[i].Id));
        }

        System.debug('*-* ScheduledSLAsEnhancement mapFilteredWorkOrderSA *-* ' + mapFilteredWorkOrderSA);

        //Get all TargetResponseInMins related to the WorkOrders (Pulling SlaProcessId and Name for expansion incase It's needed)
        List<EntityMilestone> lstEntityMilestone = [SELECT Id, SlaProcessId, SlaProcess.Name, ParentEntityId, TargetResponseInMins, MilestoneType.Id, MilestoneType.Name FROM EntityMilestone WHERE ParentEntityId IN: mapFilteredWorkOrderSA.keySet() ];
        Map<Id, ServiceAppointment> mapInsertSA = new Map<Id, ServiceAppointment>();

        System.debug('*-* ScheduledSLAsEnhancement lstEntityMilestone *-* ' + lstEntityMilestone);
       
        for(Integer i = 0; i < lstEntityMilestone.size(); i++){
            if (mapFilteredWorkOrderSA.containsKey(lstEntityMileStone[i].ParentEntityId)){
                System.debug('*-* ScheduledSLAsEnhancement lstEntityMileStone[i] *-* ' + lstEntityMileStone[i]);
                System.debug('*-* ScheduledSLAsEnhancement mapFilteredWorkOrderSA.get(lstEntityMileStone[i].ParentEntityId); *-* ' + mapFilteredWorkOrderSA.get(lstEntityMileStone[i].ParentEntityId));
                //Check if blank & response comes first in list, fill DueDate, if Repair comes after and has Minutes, replace the response (REPAIR IS PRIMARY)
                if (lstEntityMileStone[i].MilestoneType.Name == 'Response Time' && (!mapInsertSA.containsKey(lstEntityMilestone[i].ParentEntityId))){
                    ServiceAppointment sa = mapFilteredWorkOrderSA.get(lstEntityMileStone[i].ParentEntityId);
                    sa.DueDate = System.now().addMinutes(lstEntityMilestone[i].TargetResponseInMins);
                    System.debug('*-* ServiceAppointmentResponse *-*' + sa);
                    mapInsertSA.put(lstEntityMileStone[i].ParentEntityId, sa);
                } else if (lstEntityMilestone[i].TargetResponseInMins != null) {
                    ServiceAppointment sa = (mapInsertSA.containsKey(lstEntityMilestone[i].ParentEntityId)) ? mapInsertSA.get(lstEntityMilestone[i].ParentEntityId) : mapFilteredWorkOrderSA.get(lstEntityMileStone[i].ParentEntityId);
                    sa.DueDate = System.now().addMinutes(lstEntityMilestone[i].TargetResponseInMins);
                    System.debug('*-* ServiceAppointmentRepair *-*' + sa);
                    mapInsertSA.put(lstEntityMileStone[i].ParentEntityId, sa);
                }      
            }
        }

        List<ServiceAppointment> lstInsertSA = new List<ServiceAppointment>();
        for (ServiceAppointment sa : mapInsertSA.values()){
            lstInsertSA.add(sa);
        }
        if (lstInsertSA.size() > 0){
            try {
                System.debug('*-* ScheduledSLAsEnhancement lstInsertSA *-* ' + lstInsertSA);
                Database.update(lstInsertSA);
            } catch (Exception e){
                System.debug('There was an error updating the Service Appointment due date.\n' + e.getMessage());
            }
        }

        System.debug('*-* ScheduledSLAsEnhancement stop *-*');
    }
    
   /* public static void populateWorkOrderDuration(List<ServiceAppointment> newItems){
        set<Id> workorderIdSet = new set<Id>();
        for (integer i = 0; i < newItems.size(); i++){
            if (String.isNotEmpty(newItems[i].ParentRecordId)){
               Id saWOId = newItems[i].ParentRecordId;
               String sObjName = saWOId.getSObjectType().getDescribe().getName();
               if (sObjName == 'WorkOrder' ){
                  workorderIdSet.add(newItems[i].ParentRecordId);
               }
            }
        }
        if (workorderIdSet.size() >0 ){
            List<ServiceAppointment> updateSAList = New List<ServiceAppointment>();
            Map<Id,WorkOrder> workOrderMap = new Map<Id,WorkOrder>([SELECT ID, Duration FROM WorkOrder WHERE Id =:workorderIdSet]);
            for (integer i = 0; i < newItems.size(); i++){
                if(workOrderMap.containsKey(newItems[i].ParentRecordId) && workOrderMap.get(newItems[i].ParentRecordId).Duration != null){
                    double woduration =   workOrderMap.get(newItems[i].ParentRecordId).Duration;
                    updateSAList.add(new ServiceAppointment(Id =newItems[i].Id, Duration =  woduration));
                }
            }
            
            if (updateSAList.size() > 0){
                ServiceAppointmentTriggerHandler.TriggerDisabled = true;
                update updateSAList;
                ServiceAppointmentTriggerHandler.TriggerDisabled = False;
            }
        }
    }
    */
    public static void fireWoTrigger(List<ServiceAppointment> newItems){
        set<Id> workorderIdSet = new set<Id>();
        for (integer i = 0; i < newItems.size(); i++){
            if (String.isNotEmpty(newItems[i].ParentRecordId)){
               Id saWOId = newItems[i].ParentRecordId;
               String sObjName = saWOId.getSObjectType().getDescribe().getName();
               if (sObjName == 'WorkOrder' ){
                  workorderIdSet.add(newItems[i].ParentRecordId);
               }
            }
        }
        if (workorderIdSet.size() >0 ){
            List<workorder> updateWOList = New List<workorder>();
            for (id woId : workorderIdSet ){
                updateWOList.add(new WorkOrder(Id=woId,Other__c='fire trigger please' ));
            }
            if (updateWOList.size() > 0){
                update updateWOList;
            }
        }
            
        
    }
    
   
}