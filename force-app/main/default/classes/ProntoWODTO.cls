/**
 * @File Name          : ProntoWODTO.cls
 * @Description        : 
 * @Author             : Mahmood Zubair 
 * @Group              : 
 * @Last Modified By   : Mahmoood Zubair 
 * @Last Modified On   : 11/08/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/08/2020   Mahmood Zubair     Initial Version
**/
public class ProntoWODTO  implements IDTO{
    
    
    public workOrderDetails workorderDetails {get;set;}
    public ProntoWODTO(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'workOrderDetails') {
						workOrderDetails = new workOrderDetails(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'Pronto Integration consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class workOrderDetails {
		public String workorderId {get;set;} 
		public String revenueamount {get;set;} 
		public String costamount {get;set;} 
		public String marginamount {get;set;} 
		public String labouramount {get;set;} 
		public String materialamount {get;set;} 
		public String subcontractoramount {get;set;} 
        public String prontoworkorderId {get;set;}
        public string correlationId {get;set;}
		
		
		public WorkOrderDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'workorderId') {
							workorderId = parser.getText();
						} else if (text == 'revenue-amount') {
							revenueamount = parser.getText();
						} else if (text == 'margin-amount') {
							marginamount = parser.getText();
						} else if (text == 'labour-amount') {
							labouramount = parser.getText();
						} else if (text == 'material-amount') {
							materialamount = parser.getText();
						} else if (text == 'sub-contractor_amount') {
							subcontractoramount = parser.getText();
                        } else if (text == 'cost_amount') {
							costamount = parser.getText();
                        }  else if (text == 'correlationId') {
							correlationId = parser.getText();
                        } else if (text == 'prontoworkorderId') {
							prontoworkorderId = parser.getText();
                        }   else {
							System.debug(LoggingLevel.WARN, ' Pronto WorkOrderDetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	

    public  ProntoWODTO() {
	}
	
	public  ProntoWODTO parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new ProntoWODTO(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	

    public String getSpecificDTOClassName(){

        return 'ProntoWODTO';
    }


}