public abstract with sharing class ASPR_AbstractITrigger implements ASPR_ITrigger {
    
    public virtual Boolean isActive() {
        String theName = String.valueOf(this).split(':')[0];
        List<ASPR_Trigger_Handler_Setting__mdt> settings = new List<ASPR_Trigger_Handler_Setting__mdt> { getTriggerSettings(theName) };
        if (settings == null || settings.size() == 0 || getTriggerSettings(theName) == null) {
            return false;
        }

        return settings.get(0).Is_Active__c;
    }

    private static Map<String, ASPR_Trigger_Handler_Setting__mdt> settingsMap;
    
    private static ASPR_Trigger_Handler_Setting__mdt getTriggerSettings(String developerName) {
        if (settingsMap == null || (settingsMap != null && !settingsMap.containsKey(developerName))) {
            settingsMap = new Map<String, ASPR_Trigger_Handler_Setting__mdt> ();
            List<ASPR_Trigger_Handler_Setting__mdt> settings = [SELECT DeveloperName, Is_Active__c FROM ASPR_Trigger_Handler_Setting__mdt];
            for (ASPR_Trigger_Handler_Setting__mdt setting : settings) {
                settingsMap.put(setting.DeveloperName, setting);
            }
        }
        
        return settingsMap.get(developerName);
    }
    
}