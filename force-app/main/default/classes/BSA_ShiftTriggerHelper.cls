/**
 * Owner: Dildar Hussain
 * Date : 10 Jan 2020
*/
public class BSA_ShiftTriggerHelper {
    public static void updateRelatedTimeSlots(Map<Id,sObject> oldShiftMap, List<Shift> updatedShiftList){
        Set<Id> territoryIdSet = new Set<Id>();
        Set<Id> resourceIdSet = new Set<Id>();
        Map<Id,Shift> modifiedShift = new Map<Id,Shift>();
        for(Shift shiftRecord: updatedShiftList){
            Shift sft = (Shift)oldShiftMap.get(shiftRecord.Id);
            if(shiftRecord.StartTime != sft.StartTime || shiftRecord.EndTime != sft.EndTime){
                territoryIdSet.add(shiftRecord.ServiceTerritoryId);
                resourceIdSet.add(shiftRecord.ServiceResourceId);
                modifiedShift.put(shiftRecord.Id, shiftRecord);
            }
        }
		List<ServiceTerritoryMember> territoryMemberList = [Select Id, OperatingHours.TimeZone, EffectiveStartDate, EffectiveEndDate, OperatingHoursId from ServiceTerritoryMember where ServiceResourceId IN:resourceIdSet AND ServiceTerritoryId IN:territoryIdSet];
        
        Map<Id, List<TimeSlot>> matchedTSMap = new Map<Id, List<TimeSlot>>();
        Map<Id, Shift> matchedShiftMap = new Map<Id, Shift>();
        
        
        for(Id sfId: modifiedShift.keySet()){
            Shift sft = (Shift)oldShiftMap.get(sfId);
            for(ServiceTerritoryMember territoryMember: territoryMemberList){
                if(territoryMember.EffectiveEndDate >= sft.StartTime && territoryMember.EffectiveStartDate <= sft.StartTime){                    
                    matchedShiftMap.put(territoryMember.OperatingHoursId, modifiedShift.get(sft.Id));
					System.debug('DildarLog: sft - ' + sft);
                }
            }
        }
		
        System.debug('DildarLog: matchedShiftMap - ' + matchedShiftMap);
        if(matchedShiftMap.size() > 0){
            List<TimeSlot> timeSlotList = [Select Id, StartTime, EndTime, DayOfWeek, OperatingHours.TimeZone, OperatingHoursId from TimeSlot where OperatingHoursId IN:matchedShiftMap.keySet()];

            for(TimeSlot ts: timeSlotList){
                if(matchedTSMap.containsKey(ts.OperatingHoursId)){
                    matchedTSMap.get(ts.OperatingHoursId).add(ts);
                }else{
                    matchedTSMap.put(ts.OperatingHoursId,new List<TimeSlot>{ts});
                }                
            }
            List<TimeSlot> timeSlotListToUpdate = new List<TimeSlot>();
            for(Id ohId: matchedShiftMap.keySet()){
                for(TimeSlot ts: matchedTSMap.get(ohId)){
                    
                    String tzOpHours = ts.OperatingHours.TimeZone;
                    Timezone userTz = UserInfo.getTimeZone();
                    Timezone tz = Timezone.getTimeZone(tzOpHours);
                    Integer startDateOffset = tz.getOffset(matchedShiftMap.get(ohId).StartTime);
                    Integer userStartDateOffset = userTz.getOffset(matchedShiftMap.get(ohId).StartTime);
                        
                    Datetime starttime = matchedShiftMap.get(ohId).StartTime.addSeconds(-(userStartDateOffset-startDateOffset) / 1000);
                    Datetime endtime = matchedShiftMap.get(ohId).EndTime.addSeconds(-(userStartDateOffset-startDateOffset) / 1000);
                    
                    Time sTime = starttime.Time().addSeconds(+(userStartDateOffset-startDateOffset) / 1000);
                    Time eTime = endtime.Time().addSeconds(+(userStartDateOffset-startDateOffset) / 1000);
                    
                    if(starttime.format('EEEE') == ts.DayOfWeek){
                        timeSlotListToUpdate.add(new TimeSlot(Id=ts.Id, StartTime=sTime, EndTime=eTime));
                    }
                }
            }
            System.debug('DildarLog: timeSlotListToUpdate - ' + timeSlotListToUpdate);
            if(timeSlotListToUpdate.size() > 0){
                try{
                    Update timeSlotListToUpdate;
                }catch(Exception ex){
                    System.debug('DildarLog: BSA_ShiftTriggerHelper - UpdateRelatedTimeSlots - ' + timeSlotListToUpdate);
                }
            }
        }        
    }
}