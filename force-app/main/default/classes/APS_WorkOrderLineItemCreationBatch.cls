global class APS_WorkOrderLineItemCreationBatch implements Database.batchable<WorkOrderLineItem>, Database.Stateful {
    
    String guid;
    List<WorkOrderLineItem> assetWolis;    
    Map<Id, WorkOrder> workOrdersById;
    // Integer totalNumOfWorkOrders;
    Set<Id> successWorkOrderIds;
    
    Map<String, String> errorMsgByWorkOrderId; // error messages when inserting asset wolis
    List<WorkOrderLineItem> successWoli;
    String errorMsg;
    Map<id,integer> woVsWoliCount;
    Id draftWorkOrder;
    Map<Id,String> assetIdVsName=new Map<Id,String>();
    
    global APS_WorkOrderLineItemCreationBatch(List<WorkOrderLineItem> assetWolis, String guid,String message,Id draftWorkOrder) {
        this.assetWolis = assetWolis;
        this.errorMsgByWorkOrderId = new Map<String, String>();
        this.guid = guid; 
        // this.totalNumOfWorkOrders = totalNumOfWorkOrders;
        this.successWorkOrderIds = new Set<Id>();
        this.successWoli=new List<WorkOrderLineItem>();
        this.errorMsg=message;
        this.woVsWoliCount=new Map<Id,Integer>();
        this.draftWorkOrder=draftWorkOrder;
        
        Set<Id> workOrderIds = new Set<Id>();
        for(WorkOrderLineItem woli : assetWolis) {
            workOrderIds.add(woli.WorkOrderId);
        }
        this.workOrdersById = new Map<Id, WorkOrder>([SELECT Id, WorkOrderNumber,Account.Name,Pricebook2.Name,ServiceTerritory.Name,EGs_wt_MPs__c FROM WorkOrder WHERE Id IN: workOrderIds]); 
        // System.debug('@batch started: ' + workOrdersById);
    }
    
    global Iterable<WorkOrderLineItem> start(Database.BatchableContext bc) {
        APS_WorkOrderLineItemFeeder woliFeeder = new APS_WorkOrderLineItemFeeder(assetWolis);
        return woliFeeder;
    }
    
    global void execute(Database.BatchableContext bc, List<WorkOrderLineItem> assetWolis) {
        // System.debug('@record size: ' + assetWolis.size());       
        
        try {
            Set<Id> workOrderIds = new Set<Id>();
            for(WorkOrderLineItem woli : assetWolis) {
                workOrderIds.add(woli.WorkOrderId);
            }
            
            //List<WorkOrder> workOrders = [SELECT Id FROM WorkOrder WHERE Id IN: workOrderIds FOR UPDATE]; // Locking Statements to lock work orders exclusively
            
            Database.SaveResult[] srList = Database.insert(assetWolis, false);
            
            Map<Id, Integer> insertedWolisByWorkOrderId = new Map<Id, Integer>();
            
            Integer srListSize = srList.size();
            for(Integer i = 0; i < srListSize; i++) {
                Database.SaveResult sr = srList[i];
                WorkOrderLineItem woli = assetWolis[i];
                String messsage = ''; 
                if(!sr.isSuccess()) {
                    // process errors when inserting asset wolis
                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {messsage += err.getMessage() + ' ';}
                    
                    if(!this.errorMsgByWorkOrderId.containsKey(woli.WorkOrderId)) {this.errorMsgByWorkOrderId.put(woli.WorkOrderId, messsage + '; ');}
                    else {this.errorMsgByWorkOrderId.put(woli.WorkOrderId, this.errorMsgByWorkOrderId.get(woli.WorkOrderId) + messsage + '; ');}
                }
                else {
                    successWorkOrderIds.add(woli.WorkOrderId);
                    successWoli.add(woli);
                    if(!woVsWoliCount.containsKey(woli.WorkOrderId)){
                        system.debug('Print If '+woli.WorkOrderId);
                        woVsWoliCount.put(woli.WorkOrderId,1);
                    }else{
                        system.debug('Print else '+woli.WorkOrderId);
                        woVsWoliCount.put(woli.WorkOrderId,woVsWoliCount.get(woli.WorkOrderId)+1);
                    }
                }
            }
        } 
        catch(QueryException e) { if(e.getMessage().contains('Record Currently Unavailable: ')) {
            // running a job 30 seconds from now only once
            Datetime schDateTime = Datetime.now().addminutes(1); 
            //parse to cron expression
            String nextFireTime = String.valueOf(schDateTime.second()) + ' ' + String.valueOf(schDateTime.minute()) + ' ' + String.valueOf(schDateTime.hour()) + ' * * ?';
            for(WorkOrderLineItem woli : assetWolis) {woli.Id = null;}
            APS_TaskCreationHelper.abortLongRunningScheduleJob();                
            APS_WorkOrderLineItemCreationBatchSch s = new APS_WorkOrderLineItemCreationBatchSch(assetWolis, guid); System.schedule('Job Started At ' + String.valueOf(Math.random() * 10000) + '-' + UserInfo.getUserId() + ' for APS_WorkOrderLineItemCreationBatch', nextFireTime, s);
        }else {APS_WorkOrderGeneratorHelper.QuickEmail('Error on creating work order(s) and task(s)', e.getMessage() + '\n' + e.getStackTraceString() + ' Please contact helpdesk.', UserInfo.getUserEmail());
              }
                                }                     
    }
    
    global void finish(Database.BatchableContext bc) {   
        
        
        Set<WorkOrder> toBeDeleteWorkOrders = new Set<WorkOrder>();
        List<WorkOrderLineItem> equipmentTaskWolis = APS_TaskCreationHelper.generateEquipmentTasks(successWoli);
        Map<id,Map<Id,List<WorkOrderLineItem>>> WoIdVsassetIdVsEquipmentWolis=new Map<id,Map<Id,List<WorkOrderLineItem>>>();
        
        for(WOrkOrderLineItem w:successWoli){
            if(WoIdVsassetIdVsEquipmentWolis.containsKey(w.WorkOrderId)){
                WoIdVsassetIdVsEquipmentWolis.get(w.WorkOrderId).put(w.AssetId,new List<WorkOrderLineItem>());
                // assetIdVsName.put(w.AssetId,w.Asset_Name__c);
                
            }else{
                WoIdVsassetIdVsEquipmentWolis.put(w.WorkOrderId,new Map<Id,List<WorkOrderLineItem>>());
                WoIdVsassetIdVsEquipmentWolis.get(w.WorkOrderId).put(w.AssetId,new List<workOrderLineItem>());
                // assetIdVsName.put(w.AssetId,w.Asset_Name__c);
            }
            //assetIdVsEquipmentWolis.put(w.AssetId,new List<WorkOrderLineItem>());
        }
        system.debug('Printing Map 1'+WoIdVsassetIdVsEquipmentWolis);
        Database.SaveResult[] srList = Database.insert(equipmentTaskWolis, false);
        Map<Id,WorkOrderLineItem> successWoliMap=new Map<Id,WorkOrderLineItem>([select id,AssetId,Asset_Name__c,Asset.Equipment_Type__r.Name from WorkOrderLineItem where id IN:successWoli]);
        for(WorkOrderLineItem woli:successWoliMap.values()){
            assetIdVsName.put(woli.assetId,woli.Asset.Equipment_Type__r.Name);
        }
        // String errorMessage = '';
        Set<Id> failedWorkOrderIds = new Set<Id>();
        Integer srListSize = srList.size();
        for(Integer i = 0; i < srListSize; i++) {
            Database.SaveResult sr = srList[i];
            if (!sr.isSuccess()) {
                toBeDeleteWorkOrders.add(this.workOrdersById.get(equipmentTaskWolis[i].WorkOrderId));
                successWorkOrderIds.remove(equipmentTaskWolis[i].WorkOrderId);
            }else{
                WoIdVsassetIdVsEquipmentWolis.get(equipmentTaskWolis[i].WorkOrderId).get(equipmentTaskWolis[i].assetId).add(equipmentTaskWolis[i]);
            }
        }
        system.debug('Printing Map 2'+WoIdVsassetIdVsEquipmentWolis);
        // error message  
        String incompleteWoMsg= 'Work Order which could not generate completely. Reason: No Work Order Automation Rule found (Below Work Order may also contains Work Order which may get deleted due to failure in Non-Asset woli creation,You may receive reason in separate mail thread)\n';
        Boolean ismsgavailable=false;
        Map<Id,List<String>> idVsEquipmentType=new Map<Id,List<String>>();
        for(Id ids:WoIdVsassetIdVsEquipmentWolis.KeySet()){
            for(Id assetId:WoIdVsassetIdVsEquipmentWolis.get(ids).keySet()){
                if(WoIdVsassetIdVsEquipmentWolis.get(ids).get(assetId).size()==0){
                    system.debug('Inside Printing Method');
                    system.debug('Print Asset Id'+assetId);
                    ismsgavailable=true;
                    // incompleteWoMsg+=' For Workorder: '+this.workOrdersById.get(ids).WorkOrderNumber+' for Asset: '+assetIdVsName.get(assetId)+'\n';
                    if(idVsEquipmentType.containsKey(ids)){
                        if(!idVsEquipmentType.get(ids).contains(assetIdVsName.get(assetId))){
                            idVsEquipmentType.get(ids).add(assetIdVsName.get(assetId));
                        }
                    }else{
                        idVsEquipmentType.put(ids, new List<String>{assetIdVsName.get(assetId)});
                    }
                }
                
            }
        }
        for(Id ids:idVsEquipmentType.KeySet()){
            incompleteWoMsg+=' For WorkOrder: '+this.workOrdersById.get(ids).WorkOrderNumber+ ' for Equipment Types: ';
            String temp='';
            for(String types:idVsEquipmentType.get(ids)){
                temp+=' '+types+',';
            }
            incompleteWoMsg+=temp+'\n';
        }
        if(!this.errorMsgByWorkOrderId.isEmpty() || Test.isRunningTest()) {
            
            Map<id,Map<String,List<String>>> idVsDetails=new Map<id,Map<String,List<String>>>();
            Map<id,Set<String>> woIdVsEgs=new Map<id,Set<String>>();
            String  errorDetailMsg, woNumbersString = '';
            if(Test.isRunningTest()){
                Try{
                    WorkOrderLineItem wol=successWoli.get(0);
                    errorMsgByWorkOrderId.put(wol.WorkOrderId,'');
                }catch(Exception e){
                }
                
            }
            for(String woId : this.errorMsgByWorkOrderId.keySet()) {
                if(this.workOrdersById.get(woId).EGs_wt_MPs__c != '' || this.workOrdersById.get(woId).EGs_wt_MPs__c != null || Test.isRunningTest()){
                    Map<String, List<String>> mpIdsByEgId = (Map<String, List<String>>)JSON.deserialize(this.workOrdersById.get(woId).EGs_wt_MPs__c, Map<String, List<String>>.class);
                    idVsDetails.put(woId,mpIdsByEgId);
                    for(Id wo:idVsDetails.keySet()){
                        Set<String> tempSet=idVsDetails.get(wo).keySet();
                        woIdVsEgs.put(wo,tempSet);
                    }
                }
            } 
            Map<id,String> egIdVsName=new Map<id,String>();
            List<String> egIds=new List<String>();
            for(Set<String> ids:woIdVsEgs.values()){
                for(String i:ids){
                    egIds.add(i);
                }
            }
            Map<id,Asset> idVsAsset=new Map<id,Asset>([select id,Name from Asset where Id IN: egIds ]);
            Map<id,String> idVsEgGroupName=new Map<id,String>();
            String EquipmentGroupName;
            for(Id w:woIdVsEgs.keySet()){
                for(String i:woIdVsEgs.get(w)){
                    if(idVsAsset.get(i) !=null)
                        if(EquipmentGroupName != null){
                            EquipmentGroupName =EquipmentGroupName+','+idVsAsset.get(i).Name;
                        }else{
                            EquipmentGroupName=idVsAsset.get(i).Name;
                        }
                }
                idVsEgGroupName.put(w,EquipmentGroupName);
                EquipmentGroupName=null;
            }
            
            
            for(String woId : this.errorMsgByWorkOrderId.keySet()) {
                toBeDeleteWorkOrders.add(this.workOrdersById.get(woId));
                successWorkOrderIds.remove(woId);
                woNumbersString += this.workOrdersById.get(woId).WorkOrderNumber + ',';
                errorDetailMsg += 'Error details for work order ' + this.workOrdersById.get(woId).WorkOrderNumber + ': ' + this.errorMsgByWorkOrderId.get(woId) + 'with Site :'+workOrdersById.get(woId).Account.Name+',Territory :'+workOrdersById.get(woId).ServiceTerritory.Name+',Equipment Group :'+idVsEgGroupName.get(woId)+'\n';
            } 
            
            
            errorMsg += 'Inserting asset work order line item(s) for work order(s) ' + woNumbersString.removeEnd(',') + ' failed. \n' + errorDetailMsg;            
            system.debug('Printing toBeDeleteWorkOrders toBeDeleteWorkOrders.isEmpty()'+toBeDeleteWorkOrders.isEmpty());
            if(!toBeDeleteWorkOrders.isEmpty() || Test.isRunningTest()) {
                if(draftWorkOrder != null){
                    APS_WorkOrderGeneratorHelper.updateNumOfFailedWorkOrders(null, guid, new List<WorkOrder>(toBeDeleteWorkOrders), errorMsg);
                    Draft_Work_Order__c df=[select id,All_Wolis_Inserted__c,Name from Draft_Work_Order__c where id=:draftWorkOrder];
                    system.debug('Printing DF::'+df);
                    system.debug('Printing successWorkOrderIds.size() '+successWorkOrderIds.size());
                    if(successWorkOrderIds.size() == 0){
                        if(df != null){
                            df.All_Wolis_Inserted__c=true;
                            update df;  
                        } 
                    }
                    if(df != null){
                        if(ismsgavailable){
                            errorMsg +=incompleteWoMsg+'View Draft Work Order for details : '+df.Name+ '\n';
                        }else{
                            errorMsg +='View Draft Work Order for details : '+df.Name+ '\n';
                        }
                    }
                }
                
                APS_WorkOrderGeneratorHelper.QuickEmail('Work Order Generation Summary', errorMsg, UserInfo.getUserEmail());
                
            }
            
        }else if(ismsgavailable){
            system.debug('Sending Email');
            APS_WorkOrderGeneratorHelper.QuickEmail('Work Order Generation Summary(Incomplete Work Orders)', incompleteWoMsg, UserInfo.getUserEmail());
        }
        //Update a Field Stating all Asset WOLIs has been insert |Neil
        Boolean isUdpateReq=false;
        Integer countOfWoIncorrectlyCreated=0;
        List<WorkOrder> workOrders = [SELECT Id,Asset_WOLIs_Creation_Completed__c,Line_Item_Count__c,Number_of_Remaining_Assets__c FROM WorkOrder WHERE Id IN: successWorkOrderIds FOR UPDATE];
        if(workOrders != null){
            for(workOrder wo:workOrders){
                //Updating field for Asset woli's insert completed
                system.debug('Printing N#'+woVsWoliCount.get(wo.id));
                wo.Asset_WOLIs_Creation_Completed__c=true;
                Integer count=0;
                Map<Id,List<WorkOrderLineItem>> wolisMap=new Map<Id,List<WorkOrderLineItem>>();
                wolisMap= WoIdVsassetIdVsEquipmentWolis.get(wo.id);
                for(Id i: wolisMap.keySet()){
                    if(wolisMap.get(i).size()>0){
                        count++;
                    }
                }
                wo.Number_of_Remaining_Assets__c=wo.Number_of_Remaining_Assets__c-count;
                if(wo.Number_of_Remaining_Assets__c != 0){
                    countOfWoIncorrectlyCreated++;
                }
                isUdpateReq= true;
            }
            if(isUdpateReq)
                update workOrders;
            if(countOfWoIncorrectlyCreated > 0 || Test.isRunningTest()){
                Draft_Work_Order__c draft=[select id,All_Wolis_Inserted__c,Num_Of_Failed_Work_Orders__c,Num_of_Success_Work_Orders_v2__c,Total_Num_Of_Work_Orders__c,name from Draft_Work_Order__c where id=:draftWorkOrder];
                draft.Num_Of_Failed_Work_Orders__c=draft.Num_Of_Failed_Work_Orders__c+countOfWoIncorrectlyCreated;
                draft.Num_of_Success_Work_Orders_v2__c=draft.Total_Num_Of_Work_Orders__c-draft.Num_Of_Failed_Work_Orders__c;
                update draft;
            }
            //Update a Field Stating all Asset WOLIs has been insert |Neil
            
        }
        
    }
}