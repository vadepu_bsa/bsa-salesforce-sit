@isTest
public class BatchUpsertSimplexSORTest {
	static testMethod void testSimplexActivities(){   
		Test.startTest();
			BSA_TestDataFactory.createSeedDataForTesting();
        	List<Simplex_Activity_Staging__c> sasList = new List<Simplex_Activity_Staging__c>();
        	
        	Simplex_Activity_Staging__c sas = new Simplex_Activity_Staging__c();
        	sas.Work_Order_Id__c = 'number';
            sas.Work_Order_Lines_Description__c = 'WOR700078429209';
            sas.SOR_Id__c = '03-23-00-01';
            sas.SOR_Quantity__c = '1';
            sas.Task_Id__c = 'RTASK0002422243';
            sas.Task_Approval_Required__c = 'Approved';
            sas.Task_Classification__c = 'Classification';
            sas.Task_Code__c = 'TSK000452';
            sas.Task_Comments__c = 'Test';
            sas.Task_Length__c = 'Test';
            sas.Task_Quantity__c = '1';
            sas.Task_Status__c = 'Test Open';
            sas.Task_Title__c = 'Test Task';
            sas.SAM_Id__c = 'XWA';
        	sasList.add(sas);
                
        	Simplex_Activity_Staging__c sas2 = new Simplex_Activity_Staging__c();
        	sas2.Work_Order_Id__c = 'number';
            sas2.Work_Order_Lines_Description__c = 'WOR700078429209';
            sas2.SOR_Id__c = '03-23-00-01';
            sas2.SOR_Quantity__c = '1';
            sas2.Task_Id__c = 'RTASK0002422243';
            sas2.Task_Approval_Required__c = 'Approved';
            sas2.Task_Classification__c = 'Classification';
            sas2.Task_Code__c = 'TSK000452';
            sas2.Task_Comments__c = 'Test';
            sas2.Task_Length__c = 'Test';
            sas2.Task_Quantity__c = '1';
            sas2.Task_Status__c = 'Test Open';
            sas2.Task_Title__c = 'Test Task';
            sas2.SAM_Id__c = 'XWA';
        	sasList.add(sas2);
        
        	Simplex_Activity_Staging__c sas3 = new Simplex_Activity_Staging__c();
        	sas3.Work_Order_Id__c = 'number1';
            sas3.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas3.SOR_Id__c = '03-23-00-00';
            sas3.SOR_Quantity__c = '1';
            sas3.Task_Id__c = 'RTASK00024222431';
            sas3.Task_Approval_Required__c = 'Approved';
            sas3.Task_Classification__c = 'Classification';
            sas3.Task_Code__c = 'TSK0004521';
            sas3.Task_Comments__c = 'Test1';
            sas3.Task_Length__c = 'Test1';
            sas3.Task_Quantity__c = '1';
            sas3.Task_Status__c = 'Test Open';
            sas3.Task_Title__c = 'Test Task1';
            sas3.SAM_Id__c = 'XWA1';
        	sasList.add(sas3);
        
        	insert sasList;
        
            BatchUpsertSimplexSOR batchObj = new BatchUpsertSimplexSOR();
            DataBase.executeBatch(batchObj);
        	
        	WorkOrder wo = [Select Id, Client_Work_Order_Number__c from WorkOrder where parentworkorderid = null limit 1];
        	//wo.Client_Work_Order_Number__c = '12345532';
        	//update wo;
        
        	Set<Id> woSet = new Set<Id>();
        	woSet.add(wo.id);
        	
        	Line_Item__c newLI = new Line_Item__c();
        	
        	newLI.BSA_Unit_Rate__c = 10;
			newLI.BSA_Cost__c = 10;
            newLI.Technician_Unit_Rate__c = 10;
            newLI.Technician_Cost__c = 10;
        	newLI.SOR_Code__c = '03-23-00-00';
            newLI.Line_Item_External_Id__c = '03-23-00-00-'+wo.id;
           	newLI.Quantity__c = 1;
			newLI.Work_Order__c = wo.id;
            newLI.SOR_Description__c = 'Testing';
            newLI.Main_Work_Order__c = wo.id;
        	
        	insert newLI;
        
        	BatchProcessSimplexInvoiceAndPayment bt = new BatchProcessSimplexInvoiceAndPayment(woSet);
        	Database.executeBatch(bt, 25);
            
        Test.stopTest();
    }
}