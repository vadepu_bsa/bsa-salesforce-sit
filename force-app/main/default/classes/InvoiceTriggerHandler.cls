/**
 * @author          Dildar Hussain
 * @date            06/March/2020
 * 
 **/
public class InvoiceTriggerHandler implements ITriggerHandler{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

        List<Invoice__c> invoiceList = new List<Invoice__c>();
        invoiceList.addAll((List<Invoice__c>)newItems.values());
        
        InvoiceTriggerHelper.recalculateFieldsForFinancialReport(invoiceList,oldItems);                
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void BeforeInsert(List<SObject> newItems) {} public void AfterInsert(Map<Id, SObject> newItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}
}