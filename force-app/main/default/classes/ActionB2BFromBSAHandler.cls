/**
 * @File Name          : ActionB2BFromBSAHandler.cls
 * @Description        : Handler to capture published event on NBN actions from PlatformEventSubscriptionFactory 
 * @Author             : Dildar Hussain
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 03/06/2020, 9:02:16 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/06/2020   	 Dildar Hussain            Initial Version
**/

public class ActionB2BFromBSAHandler{
    
    public static void captureB2BActionLog(Action_B2B_From_BSA__e event){
        ApplicationLogUtility.addException(null, JSON.serialize(event), event.Type__c, 'INFO', BSA_ConstantsUtility.ACTION_B2B_SETUP_WORK_ORDER_EVENT+';'+event.CorrelationId__c+';'+event.Action__c+';'+event.ClientWorkOrderNumber__c+';'+event.SubAction__c+';'+BSA_ConstantsUtility.ACTION_B2B_FROM_BSA_EVENT);
    }
}