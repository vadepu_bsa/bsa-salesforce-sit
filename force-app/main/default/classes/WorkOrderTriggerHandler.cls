/**
 * @author          Sunila.M
 * @date            11/April/2019   
 * @description     Trigger Handler added fo rthe Work Order trigger
 *
 * Change Date    Modified by         Description  
 * 11/April/2019        Sunila.M        Trigger handler for WO before insert/update
 * 03/May/2019          Gourav Sood     Updated to send the Old WO to Price Book handler
 * 29/Jun/2020    Bohao Chen @IBM     JIRA FSL3-11 create BSA task and site task work order line items
 * 27/July/2021		Rohan Chopra		FR-291: Update Parent WO sub status to "Ready for Validation" if all the Child WO have Sub-status = 'Ready For Validation'
  **/
public class WorkOrderTriggerHandler implements ITriggerHandler{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    public static Map<Id,WorkOrder> workOrderMapForMapping = new Map<Id,WorkOrder>();
    public static Boolean taskGenerationInProgress = false;
	public static Boolean isFirstTime = true;
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }
    
    public void BeforeInsert(List<SObject> workOrderList) 
    { 
       //Mahmood IBM-- Code Refactor, FLS3-8, Start
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       Id SimplexRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();
       Id childCUIRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Child_CUI_Work_Order').getRecordTypeId();
       List<WorkOrder> nonAPSWorkOrderList = new List<WorkOrder>();
       List<WorkOrder> APSWorkOrderList = new List<WorkOrder>();
       Set<Id> setAccountIds = New Set<Id>();
       Set<Id> setCaseIds = New Set<Id>();
       Map<String,Map<Id,Sobject>> mServiceContractToMClonedWo = new Map<String,Map<Id,Sobject>>();
       //Map<Id,List<workOrder>> mAccountToWoList = New Map<Id,List<workOrder>>();
       List<WorkOrder> lstWoTemp = New List<WorkOrder>();
       List<WorkOrder> lstWoCaseTemp = New List<WorkOrder>();
       for(SObject obj : workOrderList) {
           WorkOrder woObj = (WorkOrder)obj;
           System.debug('woObj.isClone()=='+woObj.isClone());
           //System.debug('AKASH--> Before Insert--> WONumber-->'+woObj.workordernumber);

            if (woObj.RecordTypeId == APSRecordTypeId){
                APSWorkOrderList.add(woObj);
            } else if((woObj.Service_Contract_Number__c==null || woObj.Service_Contract_Number__c=='') && woObj.RecordTypeId!=SimplexRecordTypeId ) {
                //System.debug('AKASH-->Line 49 called');
                nonAPSWorkOrderList.add(woObj);
                if(woObj.AccountId!=null){
                   lstWoTemp.add(woObj);
                   setAccountIds.add(woObj.AccountId);
                }
                else if(woObj.CaseId!=null){
                   lstWoCaseTemp.add(woObj); 
                   setCaseIds.add(woObj.CaseId);
                }
            } //else if(woObj.Service_Contract_Number__c!=null && woObj.Service_Contract_Number__c!='' && woObj.recordTypeId==childCUIRecordTypeId && woObj.isClone() && UserInfo.getFirstName() != 'Automated'){
                else if(woObj.Service_Contract_Number__c!=null && woObj.Service_Contract_Number__c!='' && woObj.recordTypeId==childCUIRecordTypeId && UserInfo.getFirstName() != 'Automated'){
               //System.debug('AKASH-->Line 60 called');
               woObj.Status = 'New';
               woObj.Service_Resource__c =null;
               woObj.IsCloned__c =true;
               Id key= woObj.Id;
               if(mServiceContractToMClonedWo.get(woObj.Service_Contract_Number__c)!=null){
                   //System.debug('AKASH-->Line 66 called');
                   Map<Id,SObject> newItemsParentWO = mServiceContractToMClonedWo.get(woObj.Service_Contract_Number__c);
                   newItemsParentWO.put(key,woObj);
                   mServiceContractToMClonedWo.put(woObj.Service_Contract_Number__c,newItemsParentWO);
               }
               else{
               		//System.debug('AKASH-->Line 72 called');
                   Map<Id,SObject> newItemsParentWO = new Map<Id,SObject>();   
					newItemsParentWO.put(key,woObj);
                   mServiceContractToMClonedWo.put(woObj.Service_Contract_Number__c,newItemsParentWO);
               }
           }
           
       }
       for(String serviceContractno : mServiceContractToMClonedWo.keySet()) {
           //System.debug('AKASH-->Line 81 called'); 
           Map<Id,sObject> mIdToWo = mServiceContractToMClonedWo.get(serviceContractno);
            IWorkOrderService eventHandler = new WorkOrderServiceFactory(mIdToWo.values()[0].getSobjectType(),serviceContractno).getHandlerName();        
        	eventHandler.populatePRDonClonedWO(mIdToWo); 
        }
         if(setAccountIds!=null){
            Map<Id,Account> mAccounts = new Map<Id,Account>([select id,GLCode__c from Account where id in :setAccountIds]);
            for(WorkOrder woObj:lstWoTemp){
                if(mAccounts.get(woObj.AccountId)!=null && mAccounts.get(woObj.AccountId).GLCode__c!='' && mAccounts.get(woObj.AccountId).GLCode__c!=null){
                    woObj.GL_CODE__c = mAccounts.get(woObj.AccountId).GLCode__c;
                }
                else if(woObj.CaseId!=null){
                   lstWoCaseTemp.add(woObj); 
                   setCaseIds.add(woObj.CaseId);
                }
            }
        }
        if(setCaseIds!=null){
            Map<Id,Case> mCases = new Map<Id,Case>([select id,Account.GLCode__c from Case where id in :setCaseIds]);
            for(WorkOrder woObj:lstWoCaseTemp){
                if(mCases.get(woObj.CaseId)!=null && mCases.get(woObj.CaseId).Account.GLCode__c!='' && mCases.get(woObj.CaseId).Account.GLCode__c!=null){
                    woObj.GL_CODE__c = mCases.get(woObj.CaseId).Account.GLCode__c;
                }
                else{
                   //setup default GLCODE
                }
            }
            
        }
    //    system.debug('!!!APSWorkOrderList =' +APSWorkOrderList);
       if (nonAPSWorkOrderList.size() > 0){
           //System.debug('AKASH-->Line 112 called');
           WorkOrderAutomationPriceBookHelper.getWOARPriceBook(true, nonAPSWorkOrderList, null);
       }
      	
       //Mahmood IBM-- Code Refactor, FLS3-8, End 
        
        if(!APSWorkOrderList.isEmpty()) {
            // Bohao Chen @IBM 2020-09-01 populate work order details from the site // @FSL3-514
            // to replace Process builder 'Update Address as of Site' in 'Work Order Automation - APS'
            WorkOrderTriggerHelper.updateWorkOrderAddressAsOfSite(APSWorkOrderList);
            // Mahmood @IBM 2020-09-09 populate work order price book  from the site, customer or service territory. // @FSL3-514
            // to replace Process builder 'Is New Wo' in 'Work Order Automation - APS'
             WorkOrderTriggerHelper.workorderPriceBookPopulate(APSWorkOrderList);
            WorkOrderTriggerHelper.entitlementIdPopulate(APSWorkOrderList);
          
        }
    }
    
    public void AfterInsert(Map<Id, SObject> newItemsWO) 
    { 
        // System.debug('@WorkOrderTriggerHandler afterinsert');
        Map<Id,SObject> workOrderMap = new Map<Id,SObject>();
        Map<Id,WorkOrder> workOrderMapForWoli = new Map<Id,WorkOrder>();
        List<WorkOrder> apsWorkOrders = new List<WorkOrder>(); // Bohao Chen @IBM 2020-06-29 FSL3-11
        
        Set<Id> woAPSIds = new Set<Id>();//David Azzi After insert DML operation
        Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
        Id CUIRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Parent_CUI_Work_Order').getRecordTypeId();
        Id SimplexRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();
        
        Map<Id,SObject> newItems = new Map<Id,SObject>();
       Map<String,Map<Id,Sobject>> mServiceContractToMWo = new Map<String,Map<Id,Sobject>>();
        Map<String,Map<Id,Sobject>> mServiceContractToMClonedWo = new Map<String,Map<Id,Sobject>>();
       for(Id key :newItemsWO.keyset()) {    
           
           WorkOrder objWo = (WorkOrder)newItemsWO.get(key);
           if(objWo.ParentWorkOrderId== null && objWo.Service_Contract_Number__c!=null && objWo.Service_Contract_Number__c!='' && objWo.RecordTypeId != APSRecordTypeId) {
               if(mServiceContractToMWo.get(objWo.Service_Contract_Number__c)!=null){
                   //System.debug('AKASH-->Line 150 called');
                   Map<Id,SObject> newItemsParentWO = mServiceContractToMWo.get(objWo.Service_Contract_Number__c);
                   newItemsParentWO.put(key,objWo);
                   mServiceContractToMWo.put(objWo.Service_Contract_Number__c,newItemsParentWO);
               }
               else{
               		//System.debug('AKASH-->Line 156 called');
                   Map<Id,SObject> newItemsParentWO = new Map<Id,SObject>();   
					newItemsParentWO.put(key,objWo);
                   mServiceContractToMWo.put(objWo.Service_Contract_Number__c,newItemsParentWO);
               }
         	
           }
           else if(objWo.ParentWorkOrderId!= null && objWo.Service_Contract_Number__c!=null && objWo.Service_Contract_Number__c!='' && objWo.isClone()){
               if(mServiceContractToMClonedWo.get(objWo.Service_Contract_Number__c)!=null){
                   //System.debug('AKASH-->Line 165 called');
                   Map<Id,SObject> newItemsParentWO = mServiceContractToMClonedWo.get(objWo.Service_Contract_Number__c);
                   newItemsParentWO.put(key,objWo);
                   mServiceContractToMClonedWo.put(objWo.Service_Contract_Number__c,newItemsParentWO);
               }
               else{
               		//System.debug('AKASH-->Line 171 called');
                   Map<Id,SObject> newItemsParentWO = new Map<Id,SObject>();   
					newItemsParentWO.put(key,objWo);
                   mServiceContractToMClonedWo.put(objWo.Service_Contract_Number__c,newItemsParentWO);
               }
           }
           else if((objWo.Service_Contract_Number__c==null || objWo.Service_Contract_Number__c=='') && objWo.RecordTypeId!=SimplexRecordTypeId){
           		//System.debug('AKASH-->Line 178 called');
               newItems.put(objWo.Id,objWo);	   
           }
       	
       }
        for(String serviceContractno : mServiceContractToMWo.keySet()) {
            //System.debug('AKASH-->Line 184 called');
            Map<Id,sObject> mIdToWo = mServiceContractToMWo.get(serviceContractno);
            IWorkOrderService eventHandler = new WorkOrderServiceFactory(mIdToWo.values()[0].getSobjectType(),serviceContractno).getHandlerName();        
        	eventHandler.processWO(mIdToWo); 
        }
        for(String serviceContractno : mServiceContractToMClonedWo.keySet()) {
            //System.debug('AKASH-->Line 190 called');
            Map<Id,sObject> mIdToWo = mServiceContractToMClonedWo.get(serviceContractno);
            IWorkOrderService eventHandler = new WorkOrderServiceFactory(mIdToWo.values()[0].getSobjectType(),serviceContractno).getHandlerName();        
        	eventHandler.cloneWO(mIdToWo); 
        }
        for(SObject obj : newItems.values()) {
            WorkOrder woObj = (WorkOrder)obj;
            workOrderMapForMapping.put(woObj.Id,woObj);
            if(!String.isBlank(woObj.NBN_Activity_Status_Info__c) && woObj.ParentWorkOrderId == null && woObj.RecordTypeid != APSRecordTypeId) {
                workOrderMap.put(obj.Id,obj);
            }
            if(woObj.ParentWorkOrderId != null  && woObj.RecordTypeid != APSRecordTypeId) {
                workOrderMapForWoli.put(woObj.Id,woObj);
            }
            if (woObj.RecordTypeId == APSRecordTypeId){
                apsWorkOrders.add((WorkOrder)obj);
                woAPSIds.add(obj.Id);
            }
        }
        
        if(!workOrderMap.isEmpty()) {
            //System.debug('AKASH-->Line 211 called');
            WorkOrderAutomationPriceBookHelper.createNbnWorkOrders(workOrderMap);
        }

        if(!workOrderMapForWoli.isEmpty()) {
            //System.debug('AKASH-->Line 216 called');
            WorkOrderAutomationPriceBookHelper.createWolisForNbnchildWo(workOrderMapForWoli);
        }
        
        if (apsWorkOrders.size() > 0){
            //David Azzi @IBM 2020-07-06 FLS3-8 - Moved from Before Insert
            // if (APSWorkOrderList.size() > 0){
            WorkOrderTriggerHelper.populateClientWorkOrderID(apsWorkOrders);
       
            WorkOrderTriggerHelper.AddSkillsToWo(woAPSIds);
            // TODO: Future method cannot be called from a future or batch method. need to move this to queueable job
            //
            //Mahmood Zubair @IBM 2020-06-30 FSL3-142
            WorkOrderTriggerHelper.populateAPSskillGroupFromEG(apsWorkOrders);
            WorkOrderTriggerHelper.syncResourcePreference(apsWorkOrders);
            //Mahmood Zubair @IBM 2020-08-06 WorderOrder Outbound Integration 
            // WorkOrderTriggerHelper.populateAPSProntoWorkOrder(woAPSIds);
            //   //Mahmood @IBM: Added method to populate 
            WorkOrderTriggerHelper.populateEquipmentTypeObject(apsWorkOrders);
            
            // System.enqueueJob(new WorkOrderTriggerHelperQueueable(woAPSIds, null, null, 'Insert'));
             WorkOrderTriggerHelper.populateBudgetedHoursFromEG(woAPSIds);
        }
        
    }    
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<WorkOrder> NonAPSworkOrderList = new List<WorkOrder>();
        Map<Id,sObject> nonAPSnewItemMap = new Map<Id,sObject>();
        List<WorkOrder> APSWorkOrderList = new List<WorkOrder>();


        Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       Id SimplexRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();
        for(SObject obj : newItems.values()) {
            WorkOrder woObj = (WorkOrder)obj;
            if (woObj.RecordTypeId != APSRecordTypeId && woObj.RecordTypeId !=SimplexRecordTypeId){
                NonAPSworkOrderList.add(woObj);
                nonAPSnewItemMap.put(obj.Id,obj);
            }
            else if (woObj.RecordTypeId == APSRecordTypeId){
                APSWorkOrderList.add(woObj);
                         System.debug('Inside Trigger');
                 //Niranjan-CUI - SFDC - Custom validation rule - Start - 28/07/2021 - 
              /*   if(woObj.Status=='Completed' ){//&& oldWoObj.Status!=woObj.Status
                     if(!woObj.SyncWOLIs__c){ 
                         System.debug('Inside If 1');
                         if(woObj.Total_OpenCount_EType__C > 0 ){
                           //   System.debug('Inside If 2');
                           //  if(woObj.No_of_Completed_Asset_Action_WOLI__c<woObj.Total_No_of_Asset_WOLIs__c)
                                //    {
                               list<Workorderlineitem> openWOLIs=[select id from workorderlineitem where parent_work_order_id__C=:woObj.Id and status='Open'];
                             if(openWOLIs.size()>0){
                                          System.debug('Inside error');
                                        woObj.addError('Work Order cannot be completed as there are incomplete Assets');
                                       }
                                    }
                            //}   
                        }
                    }*/
                  //Niranjan-CUI - SFDC - Custom validation rule - Start - 28/07/2021
            }
            workOrderMapForMapping.put(woObj.Id,woObj);

        }

        //--populate Price Book field on Work order update
        System.debug('SAYALI DEBUG1 --'+TriggerDisabled);
        if (NonAPSworkOrderList.size() > 0 && !TriggerDisabled){
            System.debug('SAYALI DEBUG2 --'+WorkOrderAutomationPriceBookHelper.workOrderUpdated);
            if(WorkOrderAutomationPriceBookHelper.workOrderUpdated == true) return;
            System.debug('SAYALI DEBUG NBNNAWorkOrderServiceImplementation.isWOUpdated=='+NBNNAWorkOrderServiceImplementation.isWOUpdated);
            if(NBNNAWorkOrderServiceImplementation.isWOUpdated != true){
                List<WorkOrder> workOrderList = new List<WorkOrder>();
                List<WorkOrder> workOrderLstWithSvContract= new List<WorkOrder>();
                Map<String,Map<id,SObject>>  mServiceContractToWoWithChangedSR= new Map<String,Map<id,SObject>>();
                Map<String,Map<Id,Sobject>> mServiceContractToMWo = new Map<String,Map<Id,Sobject>>();
                for(SObject obj : nonAPSnewItemMap.values()) {
                    WorkOrder woObj = (WorkOrder)obj;
                    WorkOrder woObjOld = (WorkOrder)oldItems.get(woObj.Id);
                    if(woObj.Service_Contract_Number__c !=woObjOld.Service_Contract_Number__c){
                        woObj.Service_Contract_Number__c = woObjOld.Service_Contract_Number__c;
                    }
                    if(woObj.Service_Contract_Number__c!=null &&  woObj.Service_Contract_Number__c!=''){
                        System.debug('UserInfo-->'+UserInfo.getFirstName()+','+UserInfo.getLastName()+','+woObj.SA_Status__c);
                        System.debug('Sayali Debug ProcessWOAfterUpdate1-'+woObj.SA_Status__c +','+woObjOld.SA_Status__c);
                        System.debug('Sayali Debug ProcessWOAfterUpdate2-'+woObj.Client_Inbound_Action__c +','+woObjOld.Client_Inbound_Action__c);
                       if(woObj.ServiceTerritoryId!= woObjOld.ServiceTerritoryId || (woObj.SA_Status__c!=woObjOld.SA_Status__c && UserInfo.getFirstName() == 'Automated') || woObj.Client_Inbound_Action__c!=woObjOld.Client_Inbound_Action__c){
                           workOrderLstWithSvContract.add(woObj);
                           if(mServiceContractToMWo.get(woObj.Service_Contract_Number__c)!=null){
                               Map<Id,SObject> newItemsParentWO = mServiceContractToMWo.get(woObj.Service_Contract_Number__c);
                               newItemsParentWO.put(woObj.Id,woObj);
                               mServiceContractToMWo.put(woObj.Service_Contract_Number__c,newItemsParentWO);
                           }
                           else{
                                Map<Id,SObject> newItemsParentWO = new Map<Id,SObject>();   
                                newItemsParentWO.put(woObj.Id,woObj);
                               mServiceContractToMWo.put(woObj.Service_Contract_Number__c,newItemsParentWO);
                           }
                        }
                        if(woObj.ParentWorkOrderId!=null && woObj.Service_Resource__c!=woObjOld.Service_Resource__c && woObjOld.Service_Resource__c==null){
                            if(mServiceContractToWoWithChangedSR.get(woObj.Service_Contract_Number__c)!=null){
                               Map<Id,SObject> newItemsParentWO = mServiceContractToMWo.get(woObj.Service_Contract_Number__c);
                               newItemsParentWO.put(woObj.Id,woObj);
                               mServiceContractToWoWithChangedSR.put(woObj.Service_Contract_Number__c,newItemsParentWO);
                           }
                           else{
                                Map<Id,SObject> newItemsParentWO = new Map<Id,SObject>();   
                                newItemsParentWO.put(woObj.Id,woObj);
                               mServiceContractToWoWithChangedSR.put(woObj.Service_Contract_Number__c,newItemsParentWO);
                           }
                    
                        
                	}
                }
                else{
                  	workOrderList.add(woObj);
                    workOrderMapForMapping.put(woObj.Id,woObj);
                }
                
        	}
                if(workOrderList!=null && workOrderList.size()>0) {
                    WorkOrderAutomationPriceBookHelper.getWOARPriceBook(false, workOrderList, oldItems);
                    WorkOrderAutomationPriceBookHelper.syncSAWithParentWorkOrder(workOrderMapForMapping,oldItems); 
                }
               
                //For NBN new Implementation
                
                for(String serviceContractno : mServiceContractToMWo.keySet()) {
                    Map<Id,sObject> mIdToWo = mServiceContractToMWo.get(serviceContractno);
                    IWorkOrderService eventHandler = new WorkOrderServiceFactory(mIdToWo.values()[0].getSobjectType(),serviceContractno).getHandlerName();        
                    eventHandler.processWoAfterUpdate(mIdToWo,oldItems); 
                }
                for(String serviceContractno : mServiceContractToWoWithChangedSR.keySet()) {
                    Map<Id,sObject> mIdToWo = mServiceContractToWoWithChangedSR.get(serviceContractno);
                    IWorkOrderService eventHandler = new WorkOrderServiceFactory(mIdToWo.values()[0].getSobjectType(),serviceContractno).getHandlerName();        
                    eventHandler.generateRelatedRecordsforChilds(mIdToWo); 
                }
                
                System.debug('End Time'+System.now());
            }
            System.debug('SAYALI DEBUG PCDPCR INSIDE ELSE');
                 List<WorkOrder> workOrderList = new List<WorkOrder>();
                List<WorkOrder> workOrderLstWithSvContract= new List<WorkOrder>();
                Map<String,Map<id,SObject>>  mServiceContractToWoWithChangedSR= new Map<String,Map<id,SObject>>();
                Map<String,Map<Id,Sobject>> mServiceContractToMWo = new Map<String,Map<Id,Sobject>>();
                List<String> allowWorkOrderTypes = new List<String>();
                //allowWorkOrderTypes.add('RemediationWOS');
                for(SObject obj : nonAPSnewItemMap.values()) {
                    WorkOrder woObj = (WorkOrder)obj;
                    WorkOrder woObjOld = (WorkOrder)oldItems.get(woObj.Id);
                    if( woObj.Service_Contract_Number__c!=null &&  woObj.Service_Contract_Number__c!='' ){
                       if( woObj.NBN_Field_Work_Specified_By_Id__c=='RemediationWOS' && woObj.planned_remediation_date__c==null && woObj.Classification__c!= woObjOld.Classification__c ){
                           workOrderLstWithSvContract.add(woObj);
                           if(mServiceContractToMWo.get(woObj.Service_Contract_Number__c)!=null){
                               Map<Id,SObject> newItemsParentWO = mServiceContractToMWo.get(woObj.Service_Contract_Number__c);
                               newItemsParentWO.put(woObj.Id,woObj);
                               mServiceContractToMWo.put(woObj.Service_Contract_Number__c,newItemsParentWO);
                           }
                           else{
                                Map<Id,SObject> newItemsParentWO = new Map<Id,SObject>();   
                                newItemsParentWO.put(woObj.Id,woObj);
                               mServiceContractToMWo.put(woObj.Service_Contract_Number__c,newItemsParentWO);
                           }
                        }
                    }
                }
                for(String serviceContractno : mServiceContractToMWo.keySet()) {
                    Map<Id,sObject> mIdToWo = mServiceContractToMWo.get(serviceContractno);
                    IWorkOrderService eventHandler = new WorkOrderServiceFactory(mIdToWo.values()[0].getSobjectType(),serviceContractno).getHandlerName();        
                    eventHandler.setPRDnPCDDatesOnWO(mIdToWo); 
                }
            
            
            
            
        }

        if(!APSWorkOrderList.isEmpty()) {
            // Bohao Chen @IBM 2020-09-01 populate work order details from the site // @FSL3-514
            // to replace Process builder 'Update Address as of Site' in 'Work Order Automation - APS'
            WorkOrderTriggerHelper.updateWorkOrderAddressAsOfSite(APSWorkOrderList);
        }
    }

    public static Map<Id,WorkOrder> workOrderMap () {

        return workOrderMapForMapping;
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        // System.debug('DildarLog: Running WorkOrder:AfterUpdate');
        //System.debug('Rohan : FR-291:After Update');
        List<WorkOrder> nonAPSWorkOrderList = new List<WorkOrder>();
        List<WorkOrder> APSWorkOrderList = new List<WorkOrder>();
        List<WorkOrder> APSProntoWorkOrderList = new List<WorkOrder>();
        List<WorkOrder> workOrderWtNewSa = new List<WorkOrder>(); 
		List<WorkOrder> mediaworkOrderLst = new List<WorkOrder>();
       if (!TriggerDisabled){
        
        //Rohan FR-291
        Integer numNonValidatedChildWO = 0;
        List<WorkOrder> updateworkOrderLst = new List<WorkOrder>();
        //Get custom metadata settings by passing the developer name of respective settings.
        Unify_Toggle_Setting__mdt toggleSetting = getCustomSettings('ParentWO_ReadyForValidation');
        Boolean updateParentWO = toggleSetting != null ? toggleSetting.isAllowed__c : false;
        Id complexChildRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Child_CUI_Work_Order').getRecordTypeId();
        Id readOnlyRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Read_Only').getRecordTypeId();
   
        Map<Id,WorkOrder> mNBNWos = new Map<Id,WorkOrder>();
        Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
		Id SimplexRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();
        
        //woObj.RecordTypeId!=SimplexRecordTypeId
        for(SObject obj : newItems.values()) {
            WorkOrder woObj = (WorkOrder)obj;
            WorkOrder oldWoObj = (WorkOrder)oldItems.get(woObj.Id);
            
               
            //Rohan FR-291: Update Parent WO sub status to "Ready for Validation" if all the Child WO have Sub-status = 'Ready For Validation'
            if(updateParentWO && (woObj.RecordTypeId == complexChildRecordTypeId || woObj.RecordTypeId == readOnlyRecordTypeId) && woObj.ParentWorkOrderId !=null && (woObj.Sub_Status__c != oldWoObj.Sub_Status__c || woObj.Status != oldWoObj.Status) && (woObj.Sub_Status__c == 'Ready For Validation' || woObj.Status == 'Canceled')){
                string Pid = woObj.ParentWorkOrderId;
                WorkOrder ParentWOObj = [SELECT Sub_Status__c FROM WorkOrder WHERE ID=:Pid];

                if(ParentWOObj.Sub_Status__c != 'Ready For Validation'){
                    numNonValidatedChildWO = Database.countQuery('SELECT COUNT() FROM WorkOrder where ParentWorkOrderId=:Pid and Status != \'Canceled\' and Sub_Status__c != \'Ready For Validation\'');
                    List<WorkOrder> temp = [SELECT id,Status,Sub_Status__c,ParentWorkOrderId FROM WorkOrder where ParentWorkOrderId=:Pid and Status != 'Canceled' and Sub_Status__c != 'Ready For Validation'];
                    if(numNonValidatedChildWO == 0){
                    ParentWOObj.Sub_Status__c = 'Ready For Validation';
                    updateworkOrderLst.add(ParentWOObj);
                	}
                }
                
            }
  
            
            if(NBNNAWorkOrderServiceImplementation.isWOUpdated != true && woObj.ParentWorkOrderId==null && woObj.Service_Contract_Number__c!=null && woObj.Service_Contract_Number__c!=''){
                mNBNWos.put(woObj.Id,woObj);
            }
            if(woObj.MediaFilesUpdated__c && !oldWoObj.MediaFilesUpdated__c){
                mediaworkOrderLst.add(woObj);
            }
            if (woObj.RecordTypeId != APSRecordTypeId && woObj.RecordTypeId !=SimplexRecordTypeId ){
                nonAPSWorkOrderList.add(woObj);
            }
            else if (woObj.RecordTypeId == APSRecordTypeId) {  
                	APSWorkOrderList.add(woObj);
                //Niranjan-CUI - 26/08/2021 - Start
                	//SFDC- Sonal - Prevent child WO data sent to pronto - start - 02/07/2021
                   	//if (woObj.Work_Type_Name__c != 'Automation'){ 
                   		APSProntoWorkOrderList.add(woObj); 
                	//}
                	//SFDC- Sonal - Prevent child WO data sent to pronto - End - 02/07/2021
                	// System.debug('@check has sa');
                	// System.debug('@taskGenerationInProgress: ' + taskGenerationInProgress);                
                	// only generate work order tasks after asset wolis have been created to avoid the record locking issue
                	if((oldWoObj.Asset_WOLIs_Creation_Completed__c != woObj.Asset_WOLIs_Creation_Completed__c ) && woObj.Asset_WOLIs_Creation_Completed__c==true &&
                   		woObj.ServiceAppointmentCount == 1 && !taskGenerationInProgress) {
                   		 // System.debug('@has sa');
                    	workOrderWtNewSa.add(woObj);
                    	System.debug('Ben Lee - WorkOrderTriggerHandler - Line 401');
                	}
                 	//Niranjan - SFDC change - Update Parent WOLIs - start
                   //	System.debug('Lipi- Trigger'+ oldWoObj.SyncWOLIs__c);
                	//System.debug('Lipi- Trigger'+ woObj.SyncWOLIs__c);
                
           			/*if(woObj.SyncWOLIs__c && oldWoObj.SyncWOLIs__c!=woObj.SyncWOLIs__c)
           			{
               			System.debug('Lipi- Trigger');
              			WorkOrderTriggerHelper.updateParentWoliStatus(woObj);   
            		}*/
            		//Niranjan - SFDC change - Update Parent WOLIs - End
            	//Niranjan-CUI - 26/08/2021 - End
            	/* VENU - COMMENTED FOR CUI DEPLOYMENT. START
            		// FR-358 - When the work order is completed --> Automatically Close the CASE
            		
            		if(woObj.Status == 'Completed'){
						System.debug('FR-358 W Order Completed START');
                		// Count for WO's Completed for the Case number
                		Integer numWOOpenedbyCaseNumber = 0;
                		string woCaseId = woObj.CaseId;
                        
                        System.debug('FR-358 W Order Completed Case Id: ' + woCaseId);
                		numWOOpenedbyCaseNumber = Database.countQuery('SELECT COUNT() FROM WorkOrder where CaseId =:woCaseId and Status != \'Canceled\' and Status != \'Completed\'');
                        System.debug('FR-358 W Order Completed numWOOpenedbyCaseNumber: ' + numWOOpenedbyCaseNumber);
                		if (numWOOpenedbyCaseNumber == 0) {
                    		// Get Case record to update Status and Case Outcome fields
                    		Id DDAMRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('DDAM').getRecordTypeId();
                            
                    		List<Case> lstAllCases = [SELECT Id, Status, Case_Outcome__c FROM Case WHERE Id = :woCaseId and RecordTypeId = :DDAMRecordTypeId];
                            
                            System.debug('FR-358 W Order Completed lstAllCases size : ' + lstAllCases.size());
                    		if(lstAllCases.size() > 0){
                       			for(Case objCases : lstAllCases) {
                           			objCases.Status = 'Completed';
                           			objCases.Case_Outcome__c = 'Completed by WorkOrder';
                                    System.debug('FR-358 W Order Completed Close Case Status : ' + objCases.Status + ' objCases.Case_Outcome__c : ' + objCases.Case_Outcome__c);
                           			update(objCases);
                        		}
                     		}
                 		}
              		}
               		System.debug('FR-358 W Order Completed END');
                	// FR-358  -- End   
                	VENU - COMMENTED FOR CUI DEPLOYMENT. END */             
            }
        }

        //FR-291: Update ParentWO if  updateParentWO = true
        if(updateParentWO && updateworkOrderLst.size() > 0){
               Update updateworkOrderLst;
        }

        if (nonAPSWorkOrderList.size() > 0 && !TriggerDisabled){
            WorkOrderTriggerHelper.populateFinancialReportingData(nonAPSWorkOrderList, oldItems);
            WorkOrderTriggerHelper.updateParentStatusCompleteInComplete(nonAPSWorkOrderList, oldItems);
        }

        // System.debug('@workOrderWtNewSa: ' + workOrderWtNewSa);
        if(!workOrderWtNewSa.isEmpty()) {
            taskGenerationInProgress = true;
            // Bohao Chen @IBM 2020-06-29 @FSL3-11
            // filter asset WOLIs before calling queueable job to generate site tasks
            //David Azzi @IBM 2020-08-06 - @FSL3-172
            workOrderWtNewSa = [SELECT AccountId, Maintenance_Plan__c, ServiceTerritoryId, Account.ParentId, Maintenance_Asset_Ids__c, 
                                Account.Trade__c, JSA_Type__c, WorkTypeId, EGs_wt_MPs__c, WorkType.Name, 
                                (SELECT Id 
                                FROM ServiceAppointments 
                                WHERE RecordType.DeveloperName = 'APS') 
                            FROM WorkOrder 
                            WHERE Id IN: workOrderWtNewSa];
            List<WorkOrderLineItem> successRecords=new List<WOrkOrderLineItem>();
            List<WorkOrderLineItem> bsaTaskWolis = APS_TaskCreationHelper.generateBsaTasks(workOrderWtNewSa);
            List<Id> failedRecords=new List<Id>();
            String message=''; 
             
           // 
         //Niranjan-CUI - SFDC change - Update Parent WOLIs - start
         System.enqueueJob(new APS_TaskCreationDispatcher('BSA Task', bsaTaskWolis, workOrderWtNewSa, workOrderWtNewSa,successRecords,failedRecords,message)); 
            //System.enqueueJob(new APS_TaskCreationDispatcher('BSA Task', bsaTaskWolis, workOrderWtNewSa, workOrderWtNewSa,new List<LinkedArticle>(),successRecords,failedRecords,message));
        //Niranjan-CUI - SFDC change - Update Parent WOLIs - End
        }
        if(mNBNWos!=null && mNBNWos.values()!=null && mNBNWos.values().size()>0  && Userinfo.getFirstName() == 'Automated'){
            WorkOrder woObj = mNBNWos.values()[0];
             IWorkOrderService eventHandler = new WorkOrderServiceFactory(woObj.getSobjectType(),woObj.Service_Contract_Number__c).getHandlerName();        
                    eventHandler.syncChildWithParent(mNBNWos,oldItems); 
        }
        if (APSWorkOrderList.size()> 0){
            APS_WorkOrderGeneratorHelper.updateNextMaintenanceDate(APSWorkOrderList, (Map<Id, WorkOrder>)oldItems);
            WorkOrderTriggerHelper.sendProntoWOUpdate(APSWorkOrderList, oldItems);   
        }
        //Niranjan-CUI - Start
        //SFS- Sonal - Prevent child WO data sent to pronto - Start - 02/07/2021
        // if (APSProntoWorkOrderList.size()> 0){
         //   WorkOrderTriggerHelper.sendProntoWOUpdate(APSProntoWorkOrderList, oldItems);   
       // }
        //Niranjan-CUI - End
        //SFS- Sonal - Prevent child WO data sent to pronto - End - 02/07/2021
        if(mediaworkOrderLst!=null && mediaworkOrderLst.size()>0 && !Test.isRunningTest()){
           UploadMediaFilesBatch shn = new UploadMediaFilesBatch(mediaworkOrderLst[0].Id); 
        	database.executeBatch(shn,1 ); 
        }
      }
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {
        
       //  APS_WorkOrderGeneratorHelper.updatePreviousMaintenanceDate((Map<Id, WorkOrder>)oldItems);
        
    }  public void AfterDelete(Map<Id, SObject> oldItems) {
        /* VENU - COMMENTED FOR CUI DEPLOYMNET. START.
        List<Id> orderList=new List<Id>();
        for(SObject obj : oldItems.values()) {
            WorkOrder woObj = (WorkOrder)obj;
            if (woObj.Work_Type_Name__c=='Defect Rectification'){
                if(woObj.Order__c != null){
                    orderList.add(woObj.Order__c);
                }
            }
        }
        if(orderList.size() >0){
            List<Order> ordList=[select id,Is_WO_Generated__c from Order where id IN: orderList];
            for(Order o:ordList){
                o.Is_WO_Generated__c=false;
            }
            update ordList;
        }
        VENU - COMMENTED FOR CUI DEPLOYMNET. END. */
    }     public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
/* Private Methods*/
   private static Unify_Toggle_Setting__mdt getCustomSettings(string devName) {
        Unify_Toggle_Setting__mdt toggleSettings = [SELECT isAllowed__c FROM Unify_Toggle_Setting__mdt where DeveloperName=:devName limit 1];
        return toggleSettings;
   }
    
}