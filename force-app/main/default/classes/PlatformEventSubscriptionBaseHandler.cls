/**
 * @File Name          : PlatformEventSubscriptionBaseHandler.cls
 * @Description        : 
 * @Author             : Karan Shekhar
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 31/05/2020, 8:18:04 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    01/05/2020   Karan Shekhar     Initial Version
**/
public abstract class PlatformEventSubscriptionBaseHandler implements IPlatformEventSubscription {
    
    private sObjectType mPESobjectType;
    private List<sObject> mPEToProcessList;
    private sObjectType mMappedSobjectType;
    private Map<String,String> mMappingIdentifierKeyValueMap;
    private Map<String,Object> mMappedPayloadMap;
    private String mMappingIdentifierName;
    private String mAdditionalWhereClause;
    // private ApplicationLogsBaseHandler mAppLogsHandler;
    private sObject mMappedObjectWithValues;
    private List<sObject> mMappedObjectWithValuesList; 
    public List<sObject> mMappedObjectWithValuesListForInsert;
    public List<sObject> mMappedObjectWithValuesListForUpdate;
    public List<sobject> finalList;
    public List<Custom_Attribute__c> customAttributeFinalObjectList = new List<Custom_Attribute__c>();
    public Map<String, List<Custom_Attribute__c>> customAttributeFinalObjectMap = new Map<String, List<Custom_Attribute__c>>();
    private String mExternalIdFieldAPIName;
    private String mappedSObjectname = '';
    private Map<String,Schema.DisplayType> mFiedlAPINameToFieldTypeMap;
    private Database.SaveResult [] results;
    private List<Map<String,Object>> mDeserializePayloadList;
    private Map<String,String> mIdentifierNameToValueMap;
    private Boolean mISDMLRequired = true;
    private Map<String,List<PE_Subscription_Field_DTO_Mapping__mdt>> mPayloadMainNodeNameTOFieldMappingsMap;
    private Map<String,Object>  mPayloadMainNodeNameToVariablesMap;
    public String woExternalIdValue;

    public virtual void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
        
        mPESobjectType = peSobjectType; 
        mPEToProcessList = peToProcessList;
        mMappedSobjectType = mappedSobjectType;
        storeLogs();       
        // setMappingIdentifierANDMappingData(mMappingIdentifierName,mMappingIdentifierKeyValueMap); 
        //deserializeJSONPayload(mMappedPayloadMap);
        queryMappings();
        mapPEToObjectFields();           
        processBuinessLogic(); 
        System.debug('IsDML Required'+mISDMLRequired);
        IF(mISDMLRequired){
            //saveMappedObject();
        }
        
        //saveLogs();
    } 

    public void setAdditionalMappingFilters(String mappingIdentifierName,Map<String,String> identifierNameToValueMap, String additionalWhereClause){
        
        mMappingIdentifierName = mappingIdentifierName;
        mIdentifierNameToValueMap = identifierNameToValueMap;              
        mAdditionalWhereClause = additionalWhereClause;
        System.debug('mMappingIdentifierName'+mMappingIdentifierName);
        System.debug('mMappedPayloadMap'+mMappedPayloadMap);
        
    }

    public void setFieldAndPayloadMappingRecords(Map<String,Object> payloadMainNodeNameToVariablesMap){
        
        mPayloadMainNodeNameToVariablesMap = payloadMainNodeNameToVariablesMap;
        System.debug(' 	'+mPayloadMainNodeNameToVariablesMap);
        
    }
    
    public virtual void queryMappings(){
        String peSObjectname = String.valueOf(mPESobjectType);
        mPayloadMainNodeNameTOFieldMappingsMap = new Map<String,List<PE_Subscription_Field_DTO_Mapping__mdt>>();
        List<PE_Subscription_Field_DTO_Mapping__mdt> fieldMappings = new List<PE_Subscription_Field_DTO_Mapping__mdt>();
        String mappingQuery = 'SELECT ID,PE_Subscription_Object_Action_Mapping__r.Mapped_Object_API_Name__c,PE_Subscription_Object_Action_Mapping__r.Payload_Main_Node_Name__c,External_id__c,Mapped_Object_Field_API_Name__c,PE_Field_API_Name__c,Payload_Key_Name__c,PE_Subscription_Object_Action_Mapping__c, Default_Value__c FROM PE_Subscription_Field_DTO_Mapping__mdt WHERE Active__c =true AND  PE_Subscription_Object_Action_Mapping__r.Platform_Event_Name__c = :peSObjectname AND PE_Subscription_Object_Action_Mapping__r.'+mMappingIdentifierName+ '= \''+mIdentifierNameToValueMap.get(mMappingIdentifierName)+'\'' ;
        if (mAdditionalWhereClause != null && mAdditionalWhereClause.length() > 1 ){
        	mappingQuery = mappingQuery + 'AND '+mAdditionalWhereClause;
        }
        System.debug('mappingQuery'+mappingQuery);
        
        

        for(PE_Subscription_Field_DTO_Mapping__mdt obj : Database.query(mappingQuery)){

            if(!mPayloadMainNodeNameTOFieldMappingsMap.containsKey(obj.PE_Subscription_Object_Action_Mapping__r.Payload_Main_Node_Name__c)){
                mPayloadMainNodeNameTOFieldMappingsMap.put(obj.PE_Subscription_Object_Action_Mapping__r.Payload_Main_Node_Name__c, new List<PE_Subscription_Field_DTO_Mapping__mdt>{obj});

            } else {
                List<PE_Subscription_Field_DTO_Mapping__mdt> tempList =  mPayloadMainNodeNameTOFieldMappingsMap.get(obj.PE_Subscription_Object_Action_Mapping__r.Payload_Main_Node_Name__c);
                tempList.add(obj);
                mPayloadMainNodeNameTOFieldMappingsMap.put(obj.PE_Subscription_Object_Action_Mapping__r.Payload_Main_Node_Name__c, tempList);
            }
            
        }

        
        System.debug('fieldMappings Size'+mPayloadMainNodeNameTOFieldMappingsMap.size());
        System.debug('fieldMappings'+mPayloadMainNodeNameTOFieldMappingsMap);
    }
    
    public virtual void storeLogs(){
        
        // mAppLogsHandler = new ApplicationLogsBaseHandler(BSA_EnumsUtility.ApplicationLogType.viaPlatformEventSubscription,mPESobjectType,mPEToProcessList);
        // mAppLogsHandler.addLogs();
        
    }
    
    public virtual void deserializeJSONPayload(String mappingIdentifierName,Map<String,String> identifierNameToValueMap,List<Map<String,Object>> deserializePayloadList, String additionalWhereClause){
        
        mMappingIdentifierName = mappingIdentifierName;
        mIdentifierNameToValueMap = identifierNameToValueMap;      
        mDeserializePayloadList = deserializePayloadList;
        mAdditionalWhereClause = additionalWhereClause;
        System.debug('mMappingIdentifierName'+mMappingIdentifierName);
        System.debug('mMappedPayloadMap'+mMappedPayloadMap);   
    }
    public virtual void mapPEToObjectFields(){                   
        Map<String,String> fieldPayloadKeyToSObjectFieldMap = new Map<String,String>();
        finalList = new List<sObject>();       
        for(String str: mPayloadMainNodeNameTOFieldMappingsMap.keySet()){
            System.debug('Mapped object name'+mPayloadMainNodeNameTOFieldMappingsMap.get(str)[0].PE_Subscription_Object_Action_Mapping__r.Mapped_Object_API_Name__c);
            mappedSObjectname = mPayloadMainNodeNameTOFieldMappingsMap.get(str)[0].PE_Subscription_Object_Action_Mapping__r.Mapped_Object_API_Name__c;
            String listType = 'List<' + mappedSObjectname + '>';
            mMappedObjectWithValuesList = (List<SObject>)Type.forName(listType).newInstance();
            
            Schema.SObjectType sObjectType = mappedSObjectname == 'Schema.Location'? Schema.getGlobalDescribe().get('Location'):Schema.getGlobalDescribe().get(mappedSObjectname);
            
            mFiedlAPINameToFieldTypeMap = UtilityClass.fetchFieldTypesforObject(mappedSObjectname);            
            Object obj = mPayloadMainNodeNameToVariablesMap.get(str);
            system.debug('!@#Mahmood obj' + obj );
            if(obj instanceof List<Object> ){
                for(Object nestedObj : (List<Object>)obj){
                    Map<String,Object> tempMap = (Map<string,Object>)nestedObj;
                 
                    /*mMappedObjectWithValues = sObjectType.newSObject();
                    System.debug('DildarLog: Array - tempMap -' + nestedObj);
                	for(PE_Subscription_Field_DTO_Mapping__mdt mapping : mPayloadMainNodeNameTOFieldMappingsMap.get(str)){
                        System.debug('payload'+mapping.Payload_Key_Name__c);
                        System.debug('DildarLog: Array - default payload value'+mapping.Default_Value__c);
                        if(String.isBlank(mapping.Payload_Key_Name__c)){continue;}
                        if(!tempMap.containsKey(mapping.Payload_Key_Name__c)){continue;}
                        System.debug('value of key'+tempMap.get(mapping.Payload_Key_Name__c));
                        Object obj1 = null;                        
                        if(tempMap.get(mapping.Payload_Key_Name__c) == ''){
                            if(mapping.Default_Value__c != null){
                                obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),mapping.Default_Value__c);
                            }else if(mapping.Payload_Key_Name__c == null || tempMap.get(mapping.Payload_Key_Name__c) == null){ 
                                continue;
                                //obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
                            }else{
                                obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
                            }
                        }else{
                            obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
                        }
                        
                        System.debug('Transformed value'+obj1);
                        System.debug('Mapped_Object_Field_API_Name__c'+mapping.Mapped_Object_Field_API_Name__c);
                        mMappedObjectWithValues.put(mapping.Mapped_Object_Field_API_Name__c,obj1);
                    }*/
					mMappedObjectWithValuesList.add(getMappedObjectWithValues(tempMap,str,sObjectType));
                }

            } 
            if(obj instanceof Map<string,Object>){

                Map<String,Object> tempMap = (Map<string,Object>)obj;
                System.debug('temp map'+tempMap);
               /* mMappedObjectWithValues = sObjectType.newSObject();
                for(PE_Subscription_Field_DTO_Mapping__mdt mapping : mPayloadMainNodeNameTOFieldMappingsMap.get(str)){
                                        
                    System.debug('payload'+mapping.Payload_Key_Name__c);
                    System.debug('DildarLog: default payload value'+mapping.Default_Value__c);
                    if(String.isBlank(mapping.Payload_Key_Name__c)){continue;}
                    if(!tempMap.containsKey(mapping.Payload_Key_Name__c)){continue;}
                    System.debug('value of key'+tempMap.get(mapping.Payload_Key_Name__c));
                    Object obj1 = null;
                    if(tempMap.get(mapping.Payload_Key_Name__c) == ''){
                            if(mapping.Default_Value__c != null){
                                obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),mapping.Default_Value__c);
                            }else{
                                obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
                            }
                        }else{
                            obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
                        }
                    
                    System.debug('Transformed value'+obj1);
                    mMappedObjectWithValues.put(mapping.Mapped_Object_Field_API_Name__c,obj1);

                }*/

        System.debug('!tempmap '+tempMap+'str '+str+'sobjecttype '+sObjectType);
                
        mMappedObjectWithValuesList.add(getMappedObjectWithValues(tempMap,str,sObjectType));
			
            }
            finalList.addAll(mMappedObjectWithValuesList); 
        }

        System.debug('Final List'+finalList);
    
    }
    public virtual void mapCurrentAttributes(Object customAttrebutes, String objectName, String nodeName, SObjectField lookupOnCustomAttribute, String externalIdValue, String parentType){
		Object attObj = customAttrebutes;        
        if(customAttrebutes instanceof List<Object>){
        	List<Object> attObjList =(List<Object>)customAttrebutes;
			
            String relationshipName = lookupOnCustomAttribute.getDescribe().getRelationshipName();            
            String externalField = getMappedExternalIdFromNode(nodeName,false);
            SObjectType prType = ((SObject)(Type.forName(objectName).newInstance())).getSObjectType();			
            SObjectField refId = getSObjectFieldName(objectName, externalField);
            System.debug('Dildarlog: Custom_attributes:attList Fields -'+'relationshipName - '+relationshipName +',externalField - '+ externalField+', prType - '+ prType + ', refId - ' + refId + ', externalIdValue - '+externalIdValue);
            
            String objName = String.valueOf(prType);
            
            if(objName == 'Work_Order_Task__c'){
                externalIdValue = externalIdValue + '-' + woExternalIdValue;
            }
            
            for(Object attObjRecord:attObjList){
                Custom_Attribute__c att = new Custom_Attribute__c();
                att.name__c = (String)((Map<String, Object>)attObjRecord).get('name');
            	att.value__c = (String)((Map<String, Object>)attObjRecord).get('value');
                if(objName == 'Work_Order_Task__c'){
                    att.Field_Id__c = (String)((Map<String, Object>)attObjRecord).get('field_id');
                }
                att.CA_External_Id__c = (String)((Map<String, Object>)attObjRecord).get('name') + externalIdValue;
                
                SObject parent = prType.newSObject();
                parent.put(refId, externalIdValue);
                att.putSObject(relationshipName, parent);
                att.Parent_Type__c = parentType;
                customAttributeFinalObjectList.add(att);
			}
		}
        //customAttributeFinalObjectMap.put(objectName, attList);
		
        System.debug('Dildarlog: arrayOfCustom_attributes:attList -'+customAttributeFinalObjectList);
        //System.debug('Dildarlog: arrayOfCustom_attributes:customAttributeFinalObjectMap -'+customAttributeFinalObjectMap);
    }
    public void setAdditionalFields(sObject mappedSObject){
        
        mMappedObjectWithValuesList.add(mappedSObject);
        System.debug('mMappedObjectWithValuesList with custom fields'+mMappedObjectWithValuesList);
    }
    
    public virtual void processBuinessLogic(){
        
        
    }
    
   /* public virtual void saveMappedObject(){ 
        Schema.SObjectField externalId = mMappedSobjectType.getDescribe().fields.getMap().get(mExternalIdFieldAPIName);
        
        Database.SaveResult []  insertResults;  
        Database.SaveResult []  updateResults;
        results = new Database.SaveResult[]{};
            splitInsertAndUpdateLists();
        if(!mMappedObjectWithValuesListForInsert.isEmpty()){ 
            
            insertResults = Database.insert(mMappedObjectWithValuesListForInsert,false);    
        }
        
        if(!mMappedObjectWithValuesListForUpdate.isEmpty()){ 
            
            updateResults = Database.update(mMappedObjectWithValuesListForUpdate,false);    
        }
        
        if(insertResults != null && !insertResults.isEmpty()){
            
            results.addAll(insertResults);
        }
        
        if(updateResults != null && !updateResults.isEmpty()){
            
            results.addAll(updateResults);
        }
        System.debug('results'+results);                        
    }
    */
   /* private void splitInsertAndUpdateLists(){
        
        Set<String> externalIdsSet = new Set<String>();
        Map<String,sObject> externalIdToSObjectMap = new Map<String,sObject>();
        for(sObject sobj : mMappedObjectWithValuesList){
            
            externalIdsSet.add((String)sobj.get(mExternalIdFieldAPIName));
            externalIdToSObjectMap.put((String)sobj.get(mExternalIdFieldAPIName),sobj);
        }
        
        System.debug('externalIdsSet'+externalIdsSet);
        String str = 'SELECT Id, '+mExternalIdFieldAPIName+' FROM '+mappedSObjectname+' WHERE '+ mExternalIdFieldAPIName +' IN :externalIdsSet';
        System.debug('Query'+str);
        for(sObject sobj : Database.query(str)){
            
            if(externalIdToSObjectMap.containsKey((String)sobj.get(mExternalIdFieldAPIName))){
                sObject sObjToUpdate =  externalIdToSObjectMap.get((String)sobj.get(mExternalIdFieldAPIName));
                sObjToUpdate.Id = sobj.Id ;
                mMappedObjectWithValuesListForUpdate.add(sObjToUpdate);
                externalIdToSObjectMap.remove((String)sobj.get(mExternalIdFieldAPIName));
            }
        }
        mMappedObjectWithValuesListForInsert.addAll(externalIdToSObjectMap.values());
        
        System.debug('mMappedObjectWithValuesListForUpdate'+mMappedObjectWithValuesListForUpdate);
        System.debug('mMappedObjectWithValuesListForInsert'+mMappedObjectWithValuesListForInsert);
        
    }
*/
    
    private void saveLogs(){
        
        //mAppLogsHandler.saveLogs();
    }

    /**
    * @description  Method to set whether Base handler should insert  records in DB
    * @author Karan Shekhar | 01/05/2020 
    * @param isDMLRequired : Boolean 
    * @return void 
    **/

    public void setDMLOption(Boolean isDMLRequired){

        mISDMLRequired = isDMLRequired;
        System.debug('Is DML setting'+mISDMLRequired);
    }
    
    public List<sObject> getMappedObjectList(){        
        system.debug('Get Retruned objectList:'+finalList);
        return finalList;
    }
 	public List<Custom_Attribute__c> getMappedAttributeObjectList(){
        return customAttributeFinalObjectList;
    }
    public void setWOExternalIdValue(String externalId){
        woExternalIdValue = externalId;
    }
    public SObjectField getSObjectFieldName(String objectName, String fieldName){
        SObjectType r = ((SObject)(Type.forName(objectName).newInstance())).getSObjectType();
		DescribeSObjectResult d = r.getDescribe();
        return d.fields.getMap().get(fieldName);
    }
    public String getMappedExternalIdFromNode(String nodeName, Boolean payloadNameOfExternalId){ 
        List<PE_Subscription_Field_DTO_Mapping__mdt> objNodeList = mPayloadMainNodeNameTOFieldMappingsMap.get(nodeName);
        String externalField = '';
        for(PE_Subscription_Field_DTO_Mapping__mdt objNodejRecord:objNodeList){
            if(objNodejRecord.get('External_Id__c') == true){

               externalField = payloadNameOfExternalId ? objNodejRecord.Payload_Key_Name__c : objNodejRecord.Mapped_Object_Field_API_Name__c;
            }
        }
        System.debug('Extenal Id '+externalField);
        return externalField;
    }
    
    private sObject getMappedObjectWithValues(Map<String,Object> tempMap, String str, Schema.SObjectType sObjectType){
        mMappedObjectWithValues = sObjectType.newSObject();
        if(tempMap.get('dynamic_location_attributes') != null){
        	mapCurrentAttributes(tempMap.get('custom_location_attributes'), mappedSObjectname, 'locationDetails', Custom_Attribute__c.Location__c, (String)tempMap.get(getMappedExternalIdFromNode('locationDetails',true)), 'Location');
		}if(tempMap.get('dynamic_work_order_attributes') != null){
            //System.debug('get ExteanalId'+getMappedExternalIdFromNode('workOrderDetails',true));
            //System.debug('get externalId Value'+tempMap.get(getMappedExternalIdFromNode('workOrderDetails',true)));
           mapCurrentAttributes(tempMap.get('dynamic_work_order_attributes'), mappedSObjectname, 'workOrderDetails', Custom_Attribute__c.Work_Order__c, (String)tempMap.get(getMappedExternalIdFromNode('workOrderDetails',true)), 'Work Order');
            if(tempMap.get('dynamic_crq_attributes') != null){
                mapCurrentAttributes(tempMap.get('dynamic_crq_attributes'), mappedSObjectname, 'workOrderDetails', Custom_Attribute__c.Work_Order__c, (String)tempMap.get(getMappedExternalIdFromNode('workOrderDetails',true)), 'CRQ');
            }if(tempMap.get('dynamic_site_attributes') != null){
                mapCurrentAttributes(tempMap.get('dynamic_site_attributes'), mappedSObjectname, 'workOrderDetails', Custom_Attribute__c.Work_Order__c, (String)tempMap.get(getMappedExternalIdFromNode('workOrderDetails',true)), 'Site');
            }if(tempMap.get('network_summary') != null){
                mapCurrentAttributes(tempMap.get('network_summary'), mappedSObjectname, 'workOrderDetails', Custom_Attribute__c.Work_Order__c, (String)tempMap.get(getMappedExternalIdFromNode('workOrderDetails',true)), 'Network Summary');
            }
        }if(tempMap.get('dynamic_task_attributes') != null){                    
			mapCurrentAttributes(tempMap.get('dynamic_task_attributes'), mappedSObjectname, 'tasks', Custom_Attribute__c.Work_Order_Task__c, (String)tempMap.get(getMappedExternalIdFromNode('tasks',true)), 'Work Order Task');
		}if(tempMap.get('dynamic_resource_attributes') != null){
			mapCurrentAttributes(tempMap.get('dynamic_resource_attributes'), mappedSObjectname, 'assetDetails', Custom_Attribute__c.Asset__c, (String)tempMap.get(getMappedExternalIdFromNode('assetDetails',true)), 'Asset');
		}
        
        for(PE_Subscription_Field_DTO_Mapping__mdt mapping : mPayloadMainNodeNameTOFieldMappingsMap.get(str)){
			System.debug('payload'+mapping.Payload_Key_Name__c);
            System.debug('DildarLog: Array - default payload value'+mapping.Default_Value__c);
            System.debug('value of key1'+tempMap.get(mapping.Payload_Key_Name__c));
            System.debug('value of key - String.isBlank - '+String.isBlank(mapping.Payload_Key_Name__c));
            System.debug('value of key - IsContains - '+tempMap.containsKey(mapping.Payload_Key_Name__c));
            Object obj1 = null;      
            
            if(tempMap.get(mapping.Payload_Key_Name__c) == '' || tempMap.get(mapping.Payload_Key_Name__c) == null){
            	if(mapping.Default_Value__c != null){
                	obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),mapping.Default_Value__c);
                }else if(mapping.Payload_Key_Name__c == null || tempMap.get(mapping.Payload_Key_Name__c) == null){ 
                    continue;
                }else{
                    obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
                }
			}else{
				obj1 = UtilityClass.transformAndMapFieldValues(mFiedlAPINameToFieldTypeMap.get(mapping.Mapped_Object_Field_API_Name__c),tempMap.get(mapping.Payload_Key_Name__c));
			}
            
            System.debug('Transformed value'+obj1);
            System.debug('mappedobjectfieldname' + mapping.Mapped_Object_Field_API_Name__c);            
			mMappedObjectWithValues.put(mapping.Mapped_Object_Field_API_Name__c,obj1);            
		}
        return mMappedObjectWithValues;
    }
    
    public Database.SaveResult [] getSaveResults(){
        
        return results;
    }
    
}