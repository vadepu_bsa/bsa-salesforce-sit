public class EmailMessage {
    @InvocableMethod(label='SendEmail' description='Sends TOW Email on WO Complete')
    public static void EmailMessage(List<String> workOrderID){
        List<WorkOrder> lstWorkOrder =[select id,recordtype.Developername,Service_Resource__r.Contact__r.Email from WorkOrder where Id=:workOrderID];
        if(lstWorkOrder!=null && lstWorkOrder.size()>0 
           && lstWorkOrder[0].recordtype.Developername !='APS_Work_Order' && lstWorkOrder[0].Service_Resource__r.Contact__r.Email!=null && lstWorkOrder[0].Service_Resource__r.Contact__r.Email!=''){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String accTeamEmail = System.Label.Account_Team_Email;
               String contentSizeStr = System.Label.Content_Size;
               Integer contentSize = Integer.valueOf(contentSizeStr);
               String[] toEmails = new String[]{lstWorkOrder[0].Service_Resource__r.Contact__r.Email,accTeamEmail};
                //Email ID for the report to be sent
                //mail.setToAddresses(new String[] {lstWorkOrder[0].Service_Resource__r.Contact__r.Email});
                mail.setToAddresses(toEmails);
                //Retriving the document reference from ContentDocumentLink, ContentDocument & ContentVersion Objects
                List<ContentDocumentLink> cdlList = [SELECT Id, ContentDocumentId FROM ContentDocumentLink where LinkedEntityId =: workOrderID LIMIT :contentSize ];
                List<Messaging.EmailFileAttachment> ContentDocuments = new List<Messaging.EmailFileAttachment>();
                List<ContentDocument> lstContentDocs = new List<ContentDocument>();
               Map<Id,ContentDocument> mapContentDocs = new Map<Id,ContentDocument>();
               Set<Id> contentDocIds = new Set<Id>();
                if (!cdlList.isEmpty()) {
                    
                    for(ContentDocumentLink cdl: cdlList)
                    {
                            //lstContentDocs.Add([SELECT ID,title,FileType, LatestPublishedVersionId from ContentDocument where ID = :cdl.ContentDocumentId]);
                            contentDocIds.add(cdl.ContentDocumentId);
                    }
                    /*if(contentDocIds!=null && contentDocIds.size()>0){
                        mapContentDocs = new Map<Id,ContentDocument>([SELECT ID,title,FileType, LatestPublishedVersionId from ContentDocument where ID in :contentDocIds]);
                        
                    }*/
                    if(contentDocIds!=null && contentDocIds.size()>0){
               			ContentVersion[] cdv = [SELECT VersionData, PathOnClient,filetype  FROM ContentVersion WHERE ContentDocumentId in : contentDocIds AND IsLatest = true];
                    
                       for (ContentVersion cv: cdv) {
                            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                            //ContentVersion[] cdv = [SELECT VersionData, PathOnClient,filetype  FROM ContentVersion WHERE ContentDocumentId = :cd.Id AND IsLatest = true];
                            //ContentVersion cv = new ContentVersion();
                            //cv = cdv[0];
                            efa.setFileName(cv.PathOnClient);
                            efa.setBody(cv.VersionData);
                            ContentDocuments.add(efa);
                        }
                    }
                    mail.setFileAttachments(ContentDocuments);
                }
                // Set the message Email template created 
        
                List<EmailTemplate> templates = [SELECT Id,  Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'TOW_Template_Custom'];
             
                if (!templates.isEmpty()) {
                    mail.setTemplateId(templates[0].id);
                    mail.setSubject(templates[0].Subject);
                    mail.setHtmlBody(templates[0].HtmlValue);
                    mail.setTreatBodiesAsTemplate(true);
                    mail.setWhatId(workOrderID.get(0));
                }
                
                //Set the From Address
                //List<OrgWideEmailAddress> addresses = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'enablisreply@bsa.com.au'];
        
                /*if (!addresses.isEmpty()) {
                    mail.setOrgWideEmailAddressId(addresses[0].Id);
                }*/
               Map<String, String> fslGeneralSettingsMap = New Map<String, String>();
               for(FSL_General_Field_Value_Setting__mdt setting: [SELECT MasterLabel, QualifiedApiName, Value__c FROM FSL_General_Field_Value_Setting__mdt]){
                   fslGeneralSettingsMap.put(setting.QualifiedApiName, setting.Value__c);
               }
               mail.setSenderDisplayName(fslGeneralSettingsMap.get('Woli_Email_Sender_Name'));
            // Send the message
                try {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
                } catch (Exception e) {
              throw e;

    	}
        }

    }
}