public class FetchMediaListQueueable implements Queueable,Database.AllowsCallouts {
	public String jsonString ; 
    public FetchMediaListQueueable(String jsonString){
        this.jsonString = jsonString ;  
    }
    public void execute(QueueableContext context) {
        List<ServiceAppointment> lstApp = (List<ServiceAppointment>)Json.deserialize(jsonString,List<ServiceAppointment>.class);
        for(ServiceAppointment sa :lstApp){
            System.debug('SAYALI lstApp:'+lstApp);
        	NBNFileUploadDownloadHandler nbnHandler = New NBNFileUploadDownloadHandler(sa.Work_Order__c,'');
            nbnHandler.process(Label.Get_Media_List_Process);
        }
    }
}