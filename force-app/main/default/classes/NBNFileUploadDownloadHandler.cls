public class NBNFileUploadDownloadHandler  extends FileUploadDownloadHandler implements IFileUploadDownload {
	Id sobjId;
    String url;
    ContentVersion cv;
    boolean isUpload;
    public NBNFileUploadDownloadHandler(Id identifierId,String url){ 
    	sobjId = identifierId;  
        this.url = url;
        this.cv = null;
        this.isUpload = false;
    }
    public NBNFileUploadDownloadHandler(Id identifierId,String url,ContentVersion cv){ 
    	sobjId = identifierId;  
        this.url = url;
        this.cv = cv;
        this.isUpload = false;
    }
    public NBNFileUploadDownloadHandler(Id identifierId,String url,ContentVersion cv,boolean isUpload){ 
    	sobjId = identifierId;  
        this.url = url;
        this.cv = cv;
        this.isUpload = isUpload;
    }
    
    public void process(String processId){
        
        //check based on id object type if != content document link then it it download scenario
        System.debug('sobjId=='+sobjId);
        String sobjectType = sobjId.getSObjectType().getDescribe().getName();
        if(!isUpload){
            download(url,sobjId,cv,processId,this,this); 
        }
        else{
        	upload(url,sobjId,cv,processId,this,this);
        }
        
    }
    
    public String setResponseProcessingDTOName(){
        
        return 'WSNBNFileInboundActionDTO';
    }
    
    public String setRequestProcessingDTOName(){
        
        return '';
    }
    
    public String setResponseProcessingDTRName(){ 
        
        return 'WSNBNFileInboundActionDTR'; 
    }
    
    
    
}