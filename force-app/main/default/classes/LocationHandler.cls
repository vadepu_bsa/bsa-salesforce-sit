/* Author: Abhijeet Anand
 * Date: 13 September, 2019
 * Trigger Handler added for the LocationTrigger
 */
 public class LocationHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //After insert method on Location to associate Location with a Service Resource
    public void AfterInsert(Map<Id,SObject> newItems) { 
        List<Schema.Location> locationList = new List<Schema.Location>();
        locationList .addAll((List<Schema.Location>)newItems.values());

        //Pass Location list to associate Location with a Service Resource
        if(!locationList .isEmpty()) {
            LocationAutomationBusinessLogic.linkServiceResourceToLocation(locationList);
        }
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {} public void BeforeInsert(List<SObject> newItems) {} public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}

}