/* Author: Abhijeet Anand
* Date: 13 September, 2019
* Apex class containing the business logic around the automation of Pronto_Van_Warehouse_Replenished_Updated Platform Event
*/

public class ProntoStockReplenishmentAutomation {
    
    public static Map<String,Id> vanMap = new Map<String,Id>();
    //public static Map<String,Pronto_Van_Warehouse_Replenished_Updated__e> vanStockMap2 = new Map<String,Pronto_Van_Warehouse_Replenished_Updated__e>();
    
    public static void syncVanStockWithSF(List<Pronto_Van_Warehouse_Replenished_Updated__e> vanStockList,Map<String,List<Pronto_Van_Warehouse_Replenished_Updated__e>> vanStockMap) {
        
        List<ProductItem> productItemToUpsert = new List<ProductItem>();
        List<ProductItem> productItemToUpsertSerialized = new List<ProductItem>();
        Set<String> wareHouseCode = new Set<String>();
        Set<String> serialNumbers = new Set<String>();
        Map<String,List<String>> serialNumberMap = new Map<String,List<String>>();
        Map<String,List<String>> stockserialMap = new Map<String,List<String>>();
        Map<String,List<String>> whseStockMap = new Map<String,List<String>>();
        //vanStockMap2 = vanStockMap.deepClone(); //Makes a duplicate copy of vanStockMap, including sObject records.
        //system.debug('vanStockMap2'+vanStockMap2);
        
        // Iterate through each notification to get the warehouse Codes & Serial Numbers(if present)
        for(Pronto_Van_Warehouse_Replenished_Updated__e event : vanStockList) {
            System.debug('Warehouse_Code__c'+event.Warehouse_Code__c);
            if(event.Warehouse_Code__c != null) {
                wareHouseCode.add(event.Warehouse_Code__c);
            }
            if(event.Serial_Numbers__c != null && event.Is_Serialized__c == 'Y') {
                for(String str : event.Serial_Numbers__c.split('\\^')) {
                    System.debug('serialNumber inside event loop -->'+str);
                    serialNumbers.add(str);
                    if(event.Stock_Code__c != null) {
                        if(stockserialMap.containsKey(event.Stock_Code__c)) {
                            List<String> serials = stockserialMap.get(event.Stock_Code__c);
                            serials.add(str);
                            stockserialMap.put(event.Stock_Code__c, serials);
                        } else {
                            stockserialMap.put(event.Stock_Code__c, new List<String> { str });
                        }
                    }
                   if(serialNumberMap.containsKey(event.Warehouse_Code__c)) {
                        List<String> serials = serialNumberMap.get(event.Warehouse_Code__c);
                        serials.add(str);
                        serialNumberMap.put(event.Warehouse_Code__c, serials);
                    } else {
                        serialNumberMap.put(event.Warehouse_Code__c, new List<String> { str });
                    }
                }
            }
            else{
                if(whseStockMap.containsKey(event.Warehouse_Code__c)) {
                    List<String> stock = whseStockMap.get(event.Warehouse_Code__c);
                    stock.add(event.Stock_Code__c);
                    whseStockMap.put(event.Warehouse_Code__c, stock);
                } else {
                    whseStockMap.put(event.Warehouse_Code__c, new List<String> { event.Stock_Code__c });
                }
            }
        }
        
        // Iterate through the Stock Code set to get the matching Salesforce IDs of the Location
        if(!wareHouseCode.isEmpty()) {
            for(Schema.Location obj : [SELECT Id, Van_Id__c FROM Location WHERE Van_Id__c IN : wareHouseCode]) {
                vanMap.put(obj.Van_Id__c,obj.Id);
            }
        }
        system.debug('vanMap'+vanMap);
        
        //Query the Serials which have been exhausted in Pronto and mark their quantity as 0 in FSL for that particluar Stock & VAN
        if(!serialNumberMap.isEmpty()) {
            System.debug('serialNumberMap'+serialNumberMap);
            for(ProductItem obj : [SELECT Id, Location.Van_Id__c, QuantityOnHand, LocationId,Pronto_Serial_Number__c FROM ProductItem WHERE Product_Code__c IN : stockserialMap.keyset() 
                                AND Pronto_Serial_Number__c != null AND Is_Serialized__c = true AND Location.Van_Id__c IN : serialNumberMap.keySet()]) {
                
                if(obj.Location != null && obj.Location.Van_Id__c != null && serialNumberMap.containskey(obj.Location.Van_Id__c) && !serialNumberMap.get(obj.Location.Van_Id__c).contains(obj.Pronto_Serial_Number__c)){
                    System.debug('obj'+obj);
                    obj.QuantityOnHand = 0;
                    productItemToUpsertSerialized.add(obj);
                }

            }
        }
        //Query the Serials which have been exhausted in Pronto and mark their quantity as 0 in FSL for that particluar Stock & VAN when Serial Numbers are coming as blank/null from Pronto
        else{
            System.debug('whseStockMap'+whseStockMap);
            for(ProductItem obj : [SELECT Id, Location.Van_Id__c, QuantityOnHand, LocationId, Pronto_Serial_Number__c, Product2.Product_Code__c FROM ProductItem WHERE Pronto_Serial_Number__c != null AND Is_Serialized__c = true 
                                   AND Location.Van_Id__c IN : whseStockMap.keySet() AND Product2.Product_Code__c != null AND QuantityOnHand != 0]) {
                
                if(whseStockMap.get(obj.Location.Van_Id__c).contains(obj.Product2.Product_Code__c)){
                    System.debug('obj'+obj);
                    obj.QuantityOnHand = 0;
                    productItemToUpsertSerialized.add(obj);
                }

            }
        }
        
        system.debug('serialNumbers -->'+serialNumbers);
     
        for(Product2 obj : [SELECT Id, Product_Code__c, Is_Serialized__c FROM Product2 WHERE Product_Code__c IN : vanStockMap.keyset()]) {
            if(vanStockMap.containsKey(obj.Product_Code__c)) {
                for(String whs : vanMap.keySet()) {
                    Set<String> duplicateSerials = new Set<String>();
                    for(Pronto_Van_Warehouse_Replenished_Updated__e pe : vanStockMap.get(obj.Product_Code__c)) {
                        if(pe.Warehouse_Code__c == whs) {
                            System.debug('event from mule -->'+pe);
                            if(pe.Serial_Numbers__c != null && obj.Is_Serialized__c == true) {
                                String serialNumber = pe.Serial_Numbers__c;
                                
                                for(String str : serialNumber.split('\\^')) {
                                    System.debug('serialNumber -->'+str);
                                    if(serialNumbers.contains(str)) {
                                        ProductItem piSerialObj = createProductItem(obj, str, vanMap.get(pe.Warehouse_Code__c), pe);
                                        
                                        if(duplicateSerials.contains(piSerialObj.Pronto_Serial_Number__c)) {
                                            continue;
                                        }
                                        else{
                                            productItemToUpsertSerialized.add(piSerialObj);
                                        }
                                        duplicateSerials.add(piSerialObj.Pronto_Serial_Number__c);    
                                    }
                                }
                            }
                            else if (obj.Is_Serialized__c == false) {
                                ProductItem piObj = createProductItem(obj, '', vanMap.get(pe.Warehouse_Code__c), pe);
                                System.debug('PRD ID inside non serial: -->' + piObj.Product2Id);
                                productItemToUpsert.add(piObj);
                            }
                        }
                    }
                }
            }
        }
        
        System.debug('productItemToUpsert -->'+productItemToUpsert);
        System.debug('productItemToUpsertSerialized -->'+productItemToUpsertSerialized);
        
        // Upsert all Product Items in the list.
        List<Database.UpsertResult> serializedResults = new List<Database.UpsertResult>();
        List<Database.UpsertResult> results = new List<Database.UpsertResult>();
        
        if(!productItemToUpsertSerialized.isEmpty()) {
            serializedResults = Database.upsert(productItemToUpsertSerialized, ProductItem.Pronto_Serial_Number__c, false);
        }
        if(!productItemToUpsert.isEmpty()) {
            results = Database.upsert(productItemToUpsert, ProductItem.DeSerialized_External_Key__c, false);
        }
        
        String jsonPayload = ''; //String to build a JSON payload
        Integer index = 0;
        Integer listSize = serializedResults.size();
        
        for(Database.UpsertResult result : serializedResults) {
            
            String msg = ''; //String to write a successful/failure message
            String logLevel = ''; // String to capture Log Level(Info/Warning) on the Application Log object
            Boolean outcome = result.isSuccess();
            List<ProductItem> genericList = new List<ProductItem>();
            
            System.debug('Outcome :'+outcome);
            
            if(index <= listSize - 1) {
                genericList.add(productItemToUpsertSerialized[index]);
                System.debug('genericList'+genericList);
                //Pass the sobject list to JSON generator apex class to build a JSON string
                jsonPayload = JSON.serialize(genericList);
            }
            
            if(result.isSuccess()) {
                
                logLevel = 'Info';
                if(result.isCreated()) { //successfull insert
                    System.debug('Product Item with Id ' + result.getId() +' was created ');
                    msg = 'Product Item with Id ' + result.getId() +' was created ';
                }
                else { //successfull update
                    System.debug('Product Item with Id ' + result.getId() +' was updated ');
                    msg = 'Product Item with Id ' + result.getId() +' was updated ';
                }
                
            }
            else if(!result.isSuccess()) { // failure
                
                System.debug('Inside failure');
                // Operation failed, so get all errors
                logLevel = 'Error';
                for(Database.Error err : result.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
                
            }
            
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, 'ProductItem');
            index++;
            
        }
        
        //Insert the application log entry in the SF database
        ApplicationLogUtility.saveExceptionLog();
        
        String jsonPayloadNS = ''; //String to build a JSON payload
        
        //Pass the sobject list to JSON generator apex class to build a JSON string
        jsonPayloadNS = JSON.serialize(productItemToUpsert);//JSONGeneratorUtility.generateJSON(productItemToUpsert);
        
        for(Database.UpsertResult result : results) {
            
            String msg = ''; //String to write a successful/failure message
            String logLevel = ''; // String to capture Log Level(Info/Warning) on the Application Log object
            Boolean outcome = result.isSuccess();
            
            System.debug('Outcome :'+outcome);
            
            if(result.isSuccess()) {
                
                logLevel = 'Info';
                if(result.isCreated()) { //successfull insert
                    System.debug('Product Item with Id ' + result.getId() +' was created ');
                    msg = 'Product Item with Id ' + result.getId() +' was created ';
                }
                else { //successfull update
                    System.debug('Product Item with Id ' + result.getId() +' was updated ');
                    msg = 'Product Item with Id ' + result.getId() +' was updated ';
                }
                
            }
            else if(!result.isSuccess()) { // failure
                
                System.debug('Inside failure');
                // Operation failed, so get all errors
                logLevel = 'Error';
                for(Database.Error err : result.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
                
            }
            
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            ApplicationLogUtility.addException(result.getId(), jsonPayloadNS, msg, logLevel, 'ProductItem');
            
        }
        
        //Insert the application log entry in the SF database
        ApplicationLogUtility.saveExceptionLog();
        
    }
    
    //Method to create ProductItem record
    private static ProductItem createProductItem(Product2 prd, String serialNumber, Id locationId, Pronto_Van_Warehouse_Replenished_Updated__e pe) {
        
        ProductItem newObj = new ProductItem();
        
        if(pe.Is_Serialized__c == 'Y') {
            newObj.Is_Serialized__c = true;
            newObj.QuantityOnHand = 1;
            newObj.Pronto_Serial_Number__c = serialNumber;
            newObj.SerialNumber = serialNumber;
        }
        else {
            newObj.Pronto_Serial_Number__c = '';
            newObj.SerialNumber = '';
            newObj.Is_Serialized__c = false;
            newObj.QuantityOnHand = pe.Quantity_On_Hand__c;
            newObj.DeSerialized_External_Key__c = String.valueOf(prd.Id)+'-'+locationId;
        }
        
        System.debug('ProductItem Product2Id-->'+newObj.Product2Id);
        if(newObj.Product2Id == null) {
            newObj.Product2Id = prd.Id;
        }
        
        System.debug('ProductItem LocationId-->'+newObj.LocationId);
        if(newObj.LocationId == null) {
            newObj.LocationId = locationId; //vanMap.get(vanStockMap2.get(prd.ProductCode).Warehouse_Code__c);
        }
        
        newObj.QuantityUnitOfMeasure = pe.Quantity_Unit_Of_Measure__c;
        newObj.Replenishment_Date__c = System.today();
        newObj.Correlation_ID__c = pe.Correlation_ID__c;
        newObj.Event_TimeStamp__c = pe.Event_TimeStamp__c;
        newObj.Product_Code__c = prd.Product_Code__c;
        
        return newObj;
        
    }
    
}