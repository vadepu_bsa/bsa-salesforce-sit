public class ActionOrderToBSAHandler extends PlatformEventSubscriptionBaseHandler{
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private OrderDTO mBaseDTO;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    private String eventActionName;
    private String correlationId;
    private String OrderNumber;
    
    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
       
        for(sObject sObj : peToProcessList){
            Action_Order_to_BSA__e event = (Action_Order_to_BSA__e)sObj;
            eventActionName = event.action__c;
            correlationId = event.correlationId__c;
            orderNumber = event.orderNumber__c;
            // System.debug('!OrderNumber '+ OrderNumber);
            deserializeJSON(event);
            setupAdditonalSettings(event);   
        }
        super.process(peSobjectType,peToProcessList, mappedSobjectType);       
    }

    private void deserializeJSON(Action_Order_to_BSA__e event){
     
        String jsonString = '';
        if(String.isNotBlank(event.OrderHeader__c)){
            String tempString  = (event.OrderHeader__c);
            // System.debug('Order JSON'+tempString);
            jsonString = jsonString + tempString;
        }

        if(String.isNotBlank(event.OrderItemEntry__c)){

            jsonString = jsonString +','+ event.OrderItemEntry__c+',';
        }
        boolean singleItem = false;
        if (jsonstring.contains('"orderItemDetails":{')){
          jsonstring =   jsonstring.replace('"orderItemDetails":{','"orderItemDetails":[{');
          singleItem = true;
        }
        if (singleItem){
            jsonstring = '{'+jsonString.removeEnd(',')+']}';
        }else {
              jsonstring = '{'+jsonString.removeEnd(',')+'}';
        }
        // System.debug('BuildJson '+ jsonstring);
 
        mSpecificDTOClassName ='OrderDTO';
     
        // System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        if(mSpecificDTOClassName.equalsIgnoreCase('OrderDTO')){
            OrderDTO newOrderDTO = new OrderDTO();
            mBaseDTO = newOrderDTO.parse(jsonString);
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
        }
                
        // System.debug('Deserialize class'+mBaseDTO);
        // System.debug('Serialized class'+mSerializedData);
    }

    public override void processBuinessLogic(){
        // System.debug('!!!! Mahmood processBuinessLogic !!!! ');
        List<sObject> finalsObjectList = getMappedObjectList();
        // System.debug('!finalSobList'+finalsObjectList);

        Map<String,Order> newOrderList = new Map<String,Order>();
        Map<String,OrderItem> newOrderItemList = new Map<String,OrderItem>();
        List<String> orderNumbers = new List<String>();
        List<String> orderItemNumbers = new List<String>();
        
        for(sObject obj: finalsObjectList){
            String orderItemNumber = '';
            if(String.valueOf(obj.getSObjectType()) == 'Order'){
                newOrderList.put(orderNumber,(Order)obj);
                orderNumbers.add(orderNumber);
            }else if(String.valueOf(obj.getSObjectType()) == 'OrderItem'){
                orderItemNumber = (String)((OrderItem)obj).get('OrderItemNumber');
                newOrderItemList.put(orderItemNumber,(OrderItem)obj);
                orderItemNumbers.add(orderItemNumber);
            }
        }

        List<Order> oldOrders = [SELECT OrderNumber, Status, PoNumber FROM Order WHERE OrderNumber IN: orderNumbers];
        // System.debug('!oldOrders '+oldOrders);
        List<OrderItem> oldOrderItems = [SELECT OrderItemNumber FROM OrderItem WHERE OrderItemNumber IN: orderItemNumbers];
        List<Order> ordersToUpdate = new List<Order>();
        List<OrderItem> orderItemsToUpdate = new List<OrderItem>();

        for (Order o : oldOrders) {
            Order newO = newOrderList.get(o.OrderNumber);
            o.Pronto_Status__c = newO.Pronto_Status__c;   
            o.PoNumber = newO.PoNumber;
            ordersToUpdate.add(o);
        }
        // System.Debug('!OrdersToUpdate '+ordersToUpdate);

        // for (OrderItem oi : oldOrderItems) {
        //     OrderItem newOi = newOrderItemList.get(oi.OrderItemNumber);
        //    // oi. = newOi.;   
           
        //     orderItemsToUpdate.add(oi);
        // }h

        Database.SaveResult[] updateResults = Database.update(ordersToUpdate);
        UtilityClass.readSaveResults(updateResults, JSON.serialize(finalsObjectList), 'Order', correlationId, eventActionName, orderNumber); 			
      
        // System.Debug('!Update Result '+updateresults);
        //update orderItemsToUpdate;
    }


    private void setupAdditonalSettings(Action_Order_to_BSA__e event){
        mIdentifierMapping = new Map<string,String>(); 
        mIdentifierMapping.put('Action_Name__c',event.Action__c);  
        setAdditionalMappingFilters('Action_Name__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);
    }
}