@isTest
public with sharing class OrderItemTriggerTest {

    @TestSetup
    static void setup(){
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = false;
        lstWorkType[1].CUI__c = true;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;

        List<Markup_Code__c> lstMarkupCode = APS_TestDataFactory.createMarkupCode(1);
        insert lstMarkupCode;

        List<Material_Markup__c> lstMaterialMarkup = APS_TestDataFactory.createMaterialMarkup(lstMarkupCode[0].Id, 1);
        insert lstMaterialMarkup;

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPriceBook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(lstMarkupCode[0].Id, lstServiceTerritory[0].Id, AccountClientRTypeId, 2);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Include_New_Assets_in_PM_Routine__c = true;
        lstAccount[1].RecordTypeId = AccountSiteRTypeId;
        lstAccount[1].Include_New_Assets_in_PM_Routine__c = true;
        insert lstAccount;

        Id contactRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactRTypeId, 1);
        insert lstContact;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[1].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        lstAssetParents[1].AccountId = lstAccount[1].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[1].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[1].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;
        
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[1].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 6);
        lstWorkOrder[0].ParentWorkOrderId = null;
        lstWorkOrder[0].AccountId = lstAccount[1].Id;
        lstWorkOrder[1].AccountId = lstAccount[1].Id;
        lstWorkOrder[1].ParentWorkOrderId = lstWorkOrder[1].Id;
        insert lstWorkOrder;

        List<Labour_Rate__c> lstLabourRate = APS_TestDataFactory.createLabourRate(lstAccount[1].Id, 1);
        insert lstLabourRate;

        Id orderPurchaseOrder = APS_TestDataFactory.getRecordTypeId('Order', 'Purchase_Order');
        List<Order> lstOrder = APS_TestDataFactory.createOrder(lstAccount[1].Id, null, lstWorkOrder[1].Id, lstWorkOrder[0].Pricebook2Id, orderPurchaseOrder, 1);
        insert lstOrder;
        List<Order> lstOrderDefect = APS_TestDataFactory.createDefectOrder(lstAccount[1].Id, null, lstWorkOrder[1].Id, lstWorkOrder[0].Pricebook2Id, orderPurchaseOrder, 1);
        insert lstOrderDefect;
         

      


    }

    @IsTest
    static void testHandler(){

        List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Order> lstOrder = [SELECT Id, Pricebook2Id FROM Order ];

        List<Product2> lstProducts = APS_TestDataFactory.createProducts(5);
        insert lstProducts;

        Id stdPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> lstPricebookEntryStd = APS_TestDataFactory.createPriceBookEntries(lstProducts, stdPricebookId);
        insert lstPricebookEntryStd;

        List<PricebookEntry> lstPricebookEntry = new List<PricebookEntry>();
        for(PricebookEntry pbEntry : lstPricebookEntryStd){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = lstOrder[0].Pricebook2Id;
            pbe.Product2Id = pbEntry.Product2Id;
            pbe.UnitPrice = pbEntry.UnitPrice;
            pbe.IsActive = pbEntry.IsActive;
            pbe.UseStandardPrice = pbEntry.UseStandardPrice;
            lstPricebookEntry.add(pbe);
        }
        insert lstPricebookEntry;

        Test.startTest();
        List<OrderItem> lstOrderItem = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem;
       
         List<OrderItem> lstOrderItem2 = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem2[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem2[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem2[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem2[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem2[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem2;
        
        delete lstOrderItem2;
        
        lstOrderItem[0].Quantity = 3;
        update lstOrderItem;
        
        lstOrder[0].Status = 'Submitted';
        
        update lstOrder;
        
      // delete lstOrderItem;

        Test.stopTest();
        
    }
    @IsTest
    static void testHandler1(){

        List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Order> lstOrder = [SELECT Id, Pricebook2Id FROM Order ];

        List<Product2> lstProducts = APS_TestDataFactory.createProducts(5);
        insert lstProducts;

        Id stdPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> lstPricebookEntryStd = APS_TestDataFactory.createPriceBookEntries(lstProducts, stdPricebookId);
        insert lstPricebookEntryStd;

        List<PricebookEntry> lstPricebookEntry = new List<PricebookEntry>();
        for(PricebookEntry pbEntry : lstPricebookEntryStd){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = lstOrder[0].Pricebook2Id;
            pbe.Product2Id = pbEntry.Product2Id;
            pbe.UnitPrice = pbEntry.UnitPrice;
            pbe.IsActive = pbEntry.IsActive;
            pbe.UseStandardPrice = pbEntry.UseStandardPrice;
            lstPricebookEntry.add(pbe);
        }
        insert lstPricebookEntry;

        Test.startTest();
        List<OrderItem> lstOrderItem = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem;
       
         List<OrderItem> lstOrderItem2 = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem2[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem2[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem2[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem2[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem2[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem2;
        
        delete lstOrderItem2;
        
        lstOrderItem[0].Quantity = 3;
        update lstOrderItem;
        
        lstOrder[0].Status = 'Submitted';
        
        update lstOrder;
        
      // delete lstOrderItem;

        Test.stopTest();
        
    }
    @IsTest
    static void testHandler2(){

        List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Order> lstOrder = [SELECT Id, Pricebook2Id FROM Order ];

        List<Product2> lstProducts = APS_TestDataFactory.createProducts(5);
        insert lstProducts;

        Id stdPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> lstPricebookEntryStd = APS_TestDataFactory.createPriceBookEntries(lstProducts, stdPricebookId);
        insert lstPricebookEntryStd;

        List<PricebookEntry> lstPricebookEntry = new List<PricebookEntry>();
        for(PricebookEntry pbEntry : lstPricebookEntryStd){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = lstOrder[0].Pricebook2Id;
            pbe.Product2Id = pbEntry.Product2Id;
            pbe.UnitPrice = pbEntry.UnitPrice;
            pbe.IsActive = pbEntry.IsActive;
            pbe.UseStandardPrice = pbEntry.UseStandardPrice;
            lstPricebookEntry.add(pbe);
        }
        insert lstPricebookEntry;

        Test.startTest();
        List<OrderItem> lstOrderItem = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem;
       
         List<OrderItem> lstOrderItem2 = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem2[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem2[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem2[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem2[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem2[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem2;
        
        delete lstOrderItem2;
        
        lstOrderItem[0].Quantity = 3;
        update lstOrderItem;
        
        lstOrder[0].Status = 'Cancelled';
        
        update lstOrder;
        
      // delete lstOrderItem;

        Test.stopTest();
        
    }
    @IsTest
    static void testHandler3(){
        
         List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        Id DefectQuoteRecordTypeId = Schema.Sobjecttype.Order.getRecordTypeInfosByDeveloperName().get('Defect_Quote').getRecordTypeId();
        List<Order> lstOrder = [SELECT Id, Pricebook2Id FROM Order where RecordTypeId =: DefectQuoteRecordTypeId ];

        List<Product2> lstProducts = APS_TestDataFactory.createProducts(5);
        insert lstProducts;

        Id stdPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> lstPricebookEntryStd = APS_TestDataFactory.createPriceBookEntries(lstProducts, stdPricebookId);
        insert lstPricebookEntryStd;

        List<PricebookEntry> lstPricebookEntry = new List<PricebookEntry>();
        for(PricebookEntry pbEntry : lstPricebookEntryStd){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = lstOrder[0].Pricebook2Id;
            pbe.Product2Id = pbEntry.Product2Id;
            pbe.UnitPrice = pbEntry.UnitPrice;
            pbe.IsActive = pbEntry.IsActive;
            pbe.UseStandardPrice = pbEntry.UseStandardPrice;
            lstPricebookEntry.add(pbe);
        }
        insert lstPricebookEntry;

        Test.startTest();
        List<OrderItem> lstOrderItem = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem;
       
         List<OrderItem> lstOrderItem2 = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem2[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem2[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem2[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem2[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem2[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem2;
        
        delete lstOrderItem2;
        
        lstOrderItem[0].Quantity = 3;
        update lstOrderItem;
        
        lstOrder[0].Status = 'Submitted';
        List<Defect__c> defs=[select id from Defect__c Limit 1];
        Defect__c df=new Defect__c();
        insert df;
        lstOrderItem.get(0).Defect__c=df.id;
        update lstOrderItem; 
        update lstOrder;
        
      // delete lstOrderItem;

        Test.stopTest();
    }
    
    
}