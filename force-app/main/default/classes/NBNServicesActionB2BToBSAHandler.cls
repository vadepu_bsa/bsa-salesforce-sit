public class NBNServicesActionB2BToBSAHandler extends PlatformEventSubscriptionBaseHandler {
    
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private Id contractAccountId;
    private String peAction;
    private String jsonHistoryWOString = '';
    private sObjectType mPESobjectType;
    private IDTO mDTOClass;
    //private BaseDTO mBaseDTO;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    
    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
        
        for(sObject sObj : peToProcessList){
            Action_B2B_To_BSA__e event = (Action_B2B_To_BSA__e)sObj;
            Boolean isValidPayload = preProcessValidation(event);
            if(isValidPayload){
                deserializeJSON(event);
                setupAdditonalSettings(event);
            }
        }
        super.process(peSobjectType,peToProcessList, mappedSobjectType);        
    }
    
    private void setupAdditonalSettings(Action_B2B_To_BSA__e event){
        
        String eventActionUnify = 'SetupWorkOrder';
        mIdentifierMapping = new Map<string,String>();
        mIdentifierMapping.put('ServiceContractNumber__c',event.ServiceContractNumber__c);
        //FR-220 change
        //mAdditionalWhereClause = 'PE_Subscription_Object_Action_Mapping__r.Action_Name__c = \''+event.Action__c+'\'';
        mAdditionalWhereClause = 'PE_Subscription_Object_Action_Mapping__r.Action_Name__c = \''+eventActionUnify+'\'';
        setAdditionalMappingFilters('ServiceContractNumber__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);
        
    }
    
    private void deserializeJSON(Action_B2B_To_BSA__e event){
        
        String jsonString = '';
        peAction = event.Action__c;
        if(String.isNotBlank(event.WorkOrder__c)){
            
            //String tempString  = ((event.WorkOrder__c).removeEnd('}')) + ',"serviceContractNumber":"'+event.ServiceContractNumber__c+'"},';
            String tempString  = ((event.WorkOrder__c).removeEnd('}')) + ',"serviceContractNumber":"'+event.ServiceContractNumber__c+'","correlationId":"'+event.CorrelationId__c+'"},';
            System.debug('Work ORder JSON'+tempString);
            jsonString = jsonString + tempString;
            jsonHistoryWOString = jsonHistoryWOString + jsonHistoryWOString;
        }
        if(String.isNotBlank(event.Tasks__c)){
            
            jsonString = jsonString + event.Tasks__c+',' ;
        }
        if(String.isNotBlank(event.Notes__c)){
            
            jsonString = jsonString + event.Notes__c+',' ;
        }
        if(String.isNotBlank(event.Location__c)){
            
            jsonString = jsonString + event.Location__c+',';
        }
        if(String.isNotBlank(event.Asset__c)){
            
            jsonString = jsonString + event.Asset__c+',';
        }
        if(String.isNotBlank(event.Contact__c)){
            
            jsonString = jsonString + event.Contact__c+',';
        }
        if(String.isNotBlank(event.History__c)){
            
            jsonHistoryWOString = jsonHistoryWOString + event.History__c+',';
        }
        
        jsonHistoryWOString = '{'+jsonHistoryWOString.removeEnd(',')+'}';
        System.debug('BuildJson'+ '{'+jsonString.removeEnd(',')+'}');
        System.debug('BuildJson - jsonHistoryWOString: '+ JSON.deserializeUntyped(jsonHistoryWOString));
        
        if (test.isRunningTest()){
            contractAccountId = [SELECT Id FROM Account WHERE Name = 'NBNServiceAccount' Limit 1].Id;
            mSpecificDTOClassName = 'NBNWorkOrderDTO';
            Type customType = Type.forName(mSpecificDTOClassName);  
            //System.debug('Custom type'+customType);
            mDTOClass = (IDTO)customType.newInstance();
        }else{
            contractAccountId = [Select Id, AccountId From ServiceContract Where ContractNumber =: event.ServiceContractNumber__c Limit 1].AccountId;
            mDTOClass = new DTOFactory(event.ServiceContractNumber__c).getDTOClass();
            mSpecificDTOClassName =  mDTOClass.getSpecificDTOClassName();
        }
        
        System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        //if(mSpecificDTOClassName.equalsIgnoreCase('BaseDTO')){
            
            //mBaseDTO = (BaseDTO)mDTOClass.parse('{'+jsonString.removeEnd(',')+'}');
            //mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
      //  }
        if(mSpecificDTOClassName.equalsIgnoreCase('NBNWorkOrderDTO')){
            
            NBNWorkOrderDTO nbnDTO = (NBNWorkOrderDTO)mDTOClass.parse('{'+jsonString.removeEnd(',')+'}');
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(nbnDTO));
        }
        
        //System.debug('Deserilize class'+mBaseDTO);
        System.debug('Serialized class'+mSerializedData);
        //System.debug('DildarLog: WorkOrderNodeDetails - ' + ((Map<String, Object>)mSerializedData.get('workOrderDetails')).get('id'));
        setWOExternalIdValue((String)((Map<String, Object>)mSerializedData.get('workOrderDetails')).get('id'));
        
    }
    /* Business Logic Start */
    public override void processBuinessLogic(){
        List<sObject> finalsObjectList = getMappedObjectList();
        List<WorkOrder> finalListWorkOrder = new List<WorkOrder>();
        List<Schema.Location> finalListLocation = new List<Schema.Location>();
        List<Asset> finalListAsset = new List<Asset>();
        List<Work_Order_Contact__c> finalListContact = new List<Work_Order_Contact__c>();
        List<NBN_Log_Entry__c> finalListNote = new List<NBN_Log_Entry__c>();
        List<Work_Order_Task__c> finalListTask = new List<Work_Order_Task__c>();
        
        String woExternalField = getMappedExternalIdFromNode('workOrderDetails',false);
        String locationExternalField = getMappedExternalIdFromNode('locationDetails',false);
        String assetExternalField = getMappedExternalIdFromNode('assetDetails',false);
        String contactExternalField = getMappedExternalIdFromNode('contactDetails',false);
        String noteExternalField = getMappedExternalIdFromNode('noteDetails',false);
        String taskExternalField = getMappedExternalIdFromNode('tasks',false);
        
        String locationExternalIdValue = '';
        String woExternalIdValue = '';
        String woCorrelationId = '';
        
        for(sObject obj: finalsObjectList){
            if(String.valueOf(obj.getSObjectType()) == 'Location'){
                finalListLocation.add((Schema.Location)obj);
                locationExternalIdValue = (String)((Schema.Location)obj).get(locationExternalField);
            }else if(String.valueOf(obj.getSObjectType()) == 'WorkOrder'){
                finalListWorkOrder.add((WorkOrder)obj);
                woExternalIdValue = (String)((WorkOrder)obj).get(woExternalField);
                woCorrelationId = (String)((WorkOrder)obj).get('CorrelationId__c');
            }else if(String.valueOf(obj.getSObjectType()) == 'Asset'){
                finalListAsset.add((Asset)obj);                
            }else if(String.valueOf(obj.getSObjectType()) == 'Work_Order_Contact__c'){
                finalListContact.add((Work_Order_Contact__c)obj);
            }else if(String.valueOf(obj.getSObjectType()) == 'NBN_Log_Entry__c'){
                finalListNote.add((NBN_Log_Entry__c)obj);
            }else if(String.valueOf(obj.getSObjectType()) == 'Work_Order_Task__c'){
                finalListTask.add((Work_Order_Task__c)obj);
            }
        }
        if(finalListLocation.size() > 0){        
            SObjectField LocationRefId = getSObjectFieldName('Schema.Location', locationExternalField);
            SObjectField lookupOnCustomAttribute = Custom_Attribute__c.Location__c;
            Database.UpsertResult[] upsertResult = Database.upsert(finalListLocation, LocationRefId , false);
            UtilityClass.readUpsertResults(upsertResult, JSON.serialize(finalListLocation), 'Location', woCorrelationId, 'WorkOrderSetup', locationExternalIdValue);
            //System.debug('DildarLog: upsertResult Location - ' + upsertResult);
        }
        if(finalListWorkOrder.size() > 0){
            SObjectType locationType = ((SObject)(Type.forName('Schema.Location').newInstance())).getSObjectType();         
            SObjectField woRefId = getSObjectFieldName('WorkOrder', woExternalField);
            String locationRelationshipName = WorkOrder.LocationId.getDescribe().getRelationshipName();
            Set<Id> successWOIdSet = new Set<Id>();
            
            if(locationExternalIdValue != '' && locationExternalIdValue != null){
                for(WorkOrder workOrderRecord: finalListWorkOrder){
                    SObject locationParentObj = locationType.newSObject();
                    locationParentObj.put(locationExternalField, locationExternalIdValue);
                    workOrderRecord.putSObject(locationRelationshipName, locationParentObj);                
                }
            }
            Database.UpsertResult[] upsertResult = Database.upsert(finalListWorkOrder, woRefId , false);
            successWOIdSet = UtilityClass.readUpsertResults(upsertResult, JSON.serialize(finalListWorkOrder), 'WorkOrder', woCorrelationId, 'WorkOrderSetup', woExternalIdValue);
            //System.debug('DildarLog: upsertResult WorkOrder - ' + upsertResult);            
            
            //Save related child records
            if(successWOIdSet != null && successWOIdSet.size() > 0){
                //doOutboundWOAction(woExternalIdValue);
                saveMappedAssetWithWO(finalListAsset, woRefId, woExternalIdValue, locationExternalIdValue, locationExternalField, assetExternalField, woCorrelationId);
                saveMappedContactWithWO(finalListContact, woRefId, woExternalIdValue, contactExternalField, woCorrelationId);
                saveMappedNoteWithWO(finalListNote, woRefId, woExternalIdValue, noteExternalField, woCorrelationId, finalListWorkOrder[0].Primary_Access_Technology__c);
                saveMappedTaskWithWO(finalListTask, woRefId, woExternalIdValue, taskExternalField, woCorrelationId);
                
                System.debug('Ananti_getMappedAttributeObjectList(): ' + getMappedAttributeObjectList());
                
                //upsert getMappedAttributeObjectList() CA_External_Id__c;
                Schema.SObjectField f = Custom_Attribute__c.Fields.CA_External_Id__c;
                Database.upsert(getMappedAttributeObjectList(), f, false);
                                
                processQueuedWOUpdates(woExternalIdValue, woCorrelationId);
            }
        }
    }
    private void saveMappedAssetWithWO(List<Asset> assetList, SObjectField woExternalField, String woExternalIdValue, String locationExternalIdValue, String locationExternalField, String assetExternalField, String woCorrelationId){
        String woRelationshipName = Work_Order_Asset__c.Work_Order__c.getDescribe().getRelationshipName();
        String assetRelationshipName = Work_Order_Asset__c.Asset__c.getDescribe().getRelationshipName();
        String locationRelationshipName = Asset.LocationId.getDescribe().getRelationshipName();
                
        SObjectType assetType = ((SObject)(Type.forName('Asset').newInstance())).getSObjectType();
        SObjectType locationType = ((SObject)(Type.forName('Schema.Location').newInstance())).getSObjectType();         
        SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
        SObjectField assetRefId = getSObjectFieldName('Asset', assetExternalField);
        SObjectField woAssetRefId = getSObjectFieldName('Work_Order_Asset__c', assetExternalField);
            
        List<Work_Order_Asset__c> assetWOList = new List<Work_Order_Asset__c>();
        if(assetList != null && assetList.size() > 0){
            for(Asset assetRecord: assetList){
                Work_Order_Asset__c assetWORecord = new Work_Order_Asset__c();
                SObject assetParentObj = assetType.newSObject();
                SObject workOrderParentObj = workOrderType.newSObject();
                
                assetParentObj.put(assetExternalField, assetRecord.get(assetExternalField));
                workOrderParentObj.put(woExternalField, woExternalIdValue);
                assetWORecord.putSObject(assetRelationshipName, assetParentObj);
                assetWORecord.putSObject(woRelationshipName, workOrderParentObj);
                assetWORecord.Client_Asset_Id__c = assetRecord.Client_Asset_Id__c+'-'+woExternalIdValue;
                assetWOList.add(assetWORecord);
                
                //assetRecord.Client_Asset_Id__c = assetRecord.Client_Asset_Id__c+'-'+woExternalIdValue;
                
                if(locationExternalIdValue != '' && locationExternalIdValue != null){
                    SObject locationParentObj = locationType.newSObject();
                    locationParentObj.put(locationExternalField, locationExternalIdValue);
                    assetRecord.putSObject(locationRelationshipName, locationParentObj);
                }
                assetRecord.AccountId = contractAccountId;//'0015P0000071ynuQAA'; //@TODO: remove hardcoded ID
            }
            Database.UpsertResult[] upsertAssetResult = Database.upsert(assetList, assetRefId, false);
            UtilityClass.readUpsertResults(upsertAssetResult, JSON.serialize(assetList), 'Asset', woCorrelationId, 'WorkOrderSetup', 'Asset');
            Database.UpsertResult[] upsertAssetWOResult = Database.upsert(assetWOList, woAssetRefId, false);
            UtilityClass.readUpsertResults(upsertAssetWOResult, JSON.serialize(assetWOList), 'Work_Order_Asset__c', woCorrelationId, 'WorkOrderSetup', 'Work_Order_Asset__c');
            //System.debug('DildarLog: upsertAssetResult - ' + upsertAssetResult + ', upsertAssetWOResult - ' + upsertAssetWOResult);
        }
    }
    
    private void saveMappedContactWithWO(List<Work_Order_Contact__c> contactList, SObjectField woExternalField, String woExternalIdValue, String contactExternalField, String woCorrelationId){
        String woRelationshipName = Work_Order_Contact__c.Work_Order__c.getDescribe().getRelationshipName();
        SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
        SObjectField contactRefId = getSObjectFieldName('Work_Order_Contact__c', contactExternalField);
        
        //System.debug('DildarLog: saveMappedContactWithWO - ContactList - '+contactList);
        
        if(contactList != null && contactList.size() > 0){
            for(Work_Order_Contact__c contactRecord: contactList){
                SObject workOrderParentObj = workOrderType.newSObject();                
                workOrderParentObj.put(woExternalField, woExternalIdValue);
                
                contactRecord.putSObject(woRelationshipName, workOrderParentObj);
                contactRecord.External_Contact_Id__c = contactRecord.External_Contact_Id__c+'-'+woExternalIdValue;
            }
            Database.UpsertResult[] upsertContactResult = Database.upsert(contactList, contactRefId, false);
            UtilityClass.readUpsertResults(upsertContactResult, JSON.serialize(contactList), 'Work_Order_Contact__c', woCorrelationId, 'WorkOrderSetup', woExternalIdValue);
            //System.debug('DildarLog: upsertContactResult - ' + upsertContactResult);             
        }
    }
    
    private void saveMappedNoteWithWO(List<NBN_Log_Entry__c> noteList, SObjectField woExternalField, String woExternalIdValue, String noteExternalField, String woCorrelationId, String technology){
        String woRelationshipName = NBN_Log_Entry__c.Work_Order__c.getDescribe().getRelationshipName();       
        SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
        SObjectType noteType = ((SObject)(Type.forName('NBN_Log_Entry__c').newInstance())).getSObjectType();
        SObjectField noteRefId = getSObjectFieldName('NBN_Log_Entry__c', noteExternalField);
        Map<Id,Map<Id,String>> notificationMap = new Map<Id,Map<Id,String>>();
        
        System.debug('DildarLog: saveMappedNoteWithWO - NoteList - '+noteList);                
        if(noteList != null && noteList.size() > 0){
            for(NBN_Log_Entry__c noteRecord: noteList){                
                SObject workOrderParentObj = workOrderType.newSObject();
                workOrderParentObj.put(woExternalField, woExternalIdValue);
                noteRecord.putSObject(woRelationshipName, workOrderParentObj);
                noteRecord.Field_Work_Note_ID__c = noteRecord.Field_Work_Note_ID__c+'-'+woExternalIdValue;
                noteRecord.Unify_External_Note_Id__c = noteRecord.Field_Work_Note_ID__c;                
            }
            Database.UpsertResult[] upsertNoteResult = Database.upsert(noteList, noteRefId, false);
            Set<Id> successIdSet = UtilityClass.readUpsertResults(upsertNoteResult, JSON.serialize(noteList), 'NBN_Log_Entry__c', woCorrelationId, 'WorkOrderSetup', woExternalIdValue);
            System.debug('BSALog: upsertNoteResult - ' + upsertNoteResult + ', successIdSet - ' + successIdSet);
            
            if(jsonHistoryWOString != '' && jsonHistoryWOString != null)
                processHistoricalWorkOrders(jsonHistoryWOString, woExternalIdValue, technology);
            
            /*if(successIdSet != null && successIdSet.size() > 0){
                for(NBN_Log_Entry__c noteRecord: noteList){
                    if(noteRecord.Field_Work_Note_Type__c == 'Artefact Added'){
                        for(WorkOrder woRecord : [Select Id, Service_Resource__c, Service_Resource__r.Name from WorkOrder Where ParentWOrkOrder.Client_Work_Order_Number__c =: woExternalIdValue]){
                            Map<Id, String> resourceMessageMap = new Map<Id, String>();
                            resourceMessageMap.put(woRecord.Service_Resource__c, '@'+woRecord.Service_Resource__r.Name+' - Notifiation to check for artefact updates');
                            notificationMap.put(woRecord.Id, resourceMessageMap);
                        }
                        //UtilityClass.chatterPostPushNotification(notificationMap);

                    break;
                    }
                }
            }*/  
        }
    }
    private void saveMappedTaskWithWO(List<Work_Order_Task__c> taskList, SObjectField woExternalField, String woExternalIdValue, String taskExternalField, String woCorrelationId){
        String taskRelationshipName = Work_Order_Task__c.Work_Order__c.getDescribe().getRelationshipName();       
        SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
        SObjectType taskType = ((SObject)(Type.forName('Work_Order_Task__c').newInstance())).getSObjectType();
        SObjectField taskRefId = getSObjectFieldName('Work_Order_Task__c', taskExternalField);
        
        //System.debug('DildarLog: saveMappedTaskWithWO - TaskList - '+taskList);
        if(taskList != null && taskList.size() > 0){
            for(Work_Order_Task__c taskRecord: taskList){
                SObject workOrderParentObj = workOrderType.newSObject();
                workOrderParentObj.put(woExternalField, woExternalIdValue);
                taskRecord.putSObject(taskRelationshipName, workOrderParentObj);
                taskRecord.Client_Task_Id__c = taskRecord.Client_Task_Id__c+'-'+woExternalIdValue;
            }
            Database.UpsertResult[] upsertTaskResult = Database.upsert(taskList, taskRefId , false);
            UtilityClass.readUpsertResults(upsertTaskResult, JSON.serialize(taskList), 'Work_Order_Task__c', woCorrelationId, 'WorkOrderSetup', woExternalIdValue+'-UnifyWOTCreation');
            //System.debug('DildarLog: upsertTaskResult - ' + upsertTaskResult);
        }
    }
    /* Business Logic End */
    
    // Post creation of work order, this method will initial outbound call to NBN
    /*private void doOutboundWOAction(String clientWorkOrdrNumber){
        
        WorkOrder wo = [Select Id, NBN_Field_Work_Specified_By_Id__c from workorder where Client_Work_Order_Number__c =: clientWorkOrdrNumber];
        
        String spefication = wo.NBN_Field_Work_Specified_By_Id__c;
        String action = 'accept';
        sObject sObjectRecord = new WorkOrder(Id = wo.Id);
        
        //System.debug('DildarLog:@NBN Handler- spefication - ' + spefication + ', clientWorkOrdrNumber - ' + clientWorkOrdrNumber + ', action - ' + action);
        
        WSNBNWOOutboundActionDTR dtr = new WSNBNWOOutboundActionDTR(action,sObjectRecord,spefication);
        dtr.fetchWORecords(((WorkOrder)sObjectRecord).Id);
        dtr.initiateWOActions();
    }*/
    
    @future
    public static void processHistoricalWorkOrders(String jsonPayloadString, String sfdcWONumber, String technology) {
        Map<String, object> mpobj = (Map<String, object>)JSON.deserializeUntyped(jsonPayloadString);
        List<object> objList = (List<object>)mpobj.get('historyDetails');
        
        List<WorkOrder> historicalWOList = new List<WorkOrder>();
        List<NBN_Log_Entry__c> historicalWONoteList = new List<NBN_Log_Entry__c>();
        
        SObjectType r = ((SObject)(Type.forName('NBN_Log_Entry__c').newInstance())).getSObjectType();
        DescribeSObjectResult d = r.getDescribe();
        SObjectField  fieldNRef = d.fields.getMap().get('Field_Work_Note_ID__c');
        
        SObjectType rW = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
        DescribeSObjectResult dW = rW.getDescribe();
        SObjectField  fieldWRef = dW.fields.getMap().get('Client_Work_Order_Number__c');
        
        String woRelationshipName = NBN_Log_Entry__c.Work_Order__c.getDescribe().getRelationshipName(); 
        
        for(object objRec: objList){
            
            Map<String, Object> objMapList = (Map<String, Object>)objRec;
            WorkOrder hWO = new WorkOrder();
            hWO.recordtypeid = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('CUI_Historical_Work_Order').getRecordTypeId();
            hWO.Site_Classification__c = (String)objMapList.get('classification');
            hWO.Primary_Access_Technology__c = technology;
            hWO.Closure_Summary__c = (String)objMapList.get('closure_summary');
            hWO.Client_Work_Order_Number__c = (String)objMapList.get('id');
            hWO.NBN_Activity_State_Id__c = (String)objMapList.get('state');
            String closure_date = (String)objMapList.get('closure_date');
            if(closure_date != '' && closure_date != null)
                hWO.Date_Completed__c = UtilityClass.setStringToDateTimeFormat((String)objMapList.get('closure_date'));
            
            WorkOrder sfdcWO = new WorkOrder(Client_Work_Order_Number__c = sfdcWONumber);
            
            hWO.SFDC_WO__r = sfdcWO;
            
            historicalWOList.add(hWO);
            
            List<object> objListNotes = (List<object>)objMapList.get('notes');
            for(object objNRec: objListNotes){
                
                Map<String, Object> objMapNoteList = (Map<String, Object>)objNRec;
                NBN_Log_Entry__c hWONote = new NBN_Log_Entry__c();
                hWONote.NBN_Field_Work_Note_Description__c = (String)objMapNoteList.get('details');
                hWONote.Field_Work_Note_ID__c = (String)objMapNoteList.get('id');
                hWONote.Source__c = (String)objMapNoteList.get('source');
                hWONote.Field_Work_Note_Type__c = (String)objMapNoteList.get('type');
                SObjectType workOrderType = ((SObject)(Type.forName('WorkOrder').newInstance())).getSObjectType();
                SObjectField noteRefId = fieldNRef;
                SObject workOrderParentObj = workOrderType.newSObject();
                workOrderParentObj.put(fieldWRef, (String)objMapList.get('id'));
                hWONote.putSObject(woRelationshipName, workOrderParentObj);
                String created_date = (String)objMapNoteList.get('created_date');
                if(created_date != '' && created_date != null)
                    hWONote.Field_Work_Note_Date_Time__c = UtilityClass.setStringToDateTimeFormat((String)objMapNoteList.get('created_date'));
                historicalWONoteList.add(hWONote);
                
            }    
        }
        
        System.debug('BuildJson - historicalWOList: '+ historicalWOList);
        System.debug('BuildJson - historicalWONoteList: '+ historicalWONoteList);
        
        Database.UpsertResult[] upsertResultWO = Database.upsert(historicalWOList, fieldWRef, false);
        
        Database.UpsertResult[] upsertResultWONote = Database.upsert(historicalWONoteList, fieldNRef, false);
    }
    
    private boolean preProcessValidation(Action_B2B_To_BSA__e event){
        Boolean woExist = checkWorkOrderExistence(event.ClientWorkOrderNumber__c);
        Boolean processValidity = false;
        
        if(woExist){
            if(event.Action__c == Label.Unify_Update_Work_Order_Action){
                capturePayloadLog(event, 'Processed');
                processValidity = true;
            }else{
                capturePayloadLog(event, 'Not Applicable');
                processValidity = false;
            }
        }else{
            if(event.Action__c == Label.Unify_Setup_Work_Order_Action){
                capturePayloadLog(event, 'Processed');
                processValidity = true;
            }else{
                capturePayloadLog(event, 'Queued');
                processValidity = false;
            }
        }
        return processValidity;
    }
    
    private boolean checkWorkOrderExistence(String clientWorkOrderNumber){
        List<WorkOrder> woRecordList = [Select Id From WorkOrder Where Client_Work_Order_Number__c =:clientWorkOrderNumber];
        return woRecordList.size() > 0 ? true : false;
    }
    
    private void capturePayloadLog(Action_B2B_To_BSA__e event, String payloadStatus){
        
        List<PE_Payload_Log__c> payloadLogList = new List<PE_Payload_Log__c>();
        
        PE_Payload_Log__c payloadLog = new PE_Payload_Log__c();
        
        if(String.isNotBlank(event.WorkOrder__c)){
            payloadLog.WorkOrder__c = event.WorkOrder__c;
        }
        if(String.isNotBlank(event.Tasks__c)){
            payloadLog.Tasks__c = event.Tasks__c;
        }
        if(String.isNotBlank(event.Notes__c)){
            payloadLog.Notes__c = event.Notes__c;
        }
        if(String.isNotBlank(event.Location__c)){
            payloadLog.Location__c = event.Location__c;
        }
        if(String.isNotBlank(event.Asset__c)){
            payloadLog.Asset__c = event.Asset__c;
        }
        if(String.isNotBlank(event.Contact__c)){            
            payloadLog.Contact__c = event.Contact__c;
        }
        if(String.isNotBlank(event.History__c)){            
            payloadLog.History__c = + event.History__c;
        }
        if(String.isNotBlank(event.ServiceContractNumber__c)){            
            payloadLog.ServiceContractNumber__c = + event.ServiceContractNumber__c;
        }
        if(String.isNotBlank(event.Action__c)){            
            payloadLog.Action__c = + event.Action__c;
        }
        if(String.isNotBlank(event.ClientWorkOrderNumber__c)){            
            payloadLog.ClientWorkOrderNumber__c = + event.ClientWorkOrderNumber__c;
        }
        if(String.isNotBlank(event.CorrelationId__c)){            
            payloadLog.CorrelationId__c = + event.CorrelationId__c;
        }
        
        payloadLog.Status__c = payloadStatus;
        payloadLog.Business_Unit__c = 'UNIFY';
        payloadLogList.add(payloadLog);
        
        Database.UpsertResult[] results = Database.Upsert(payloadLogList, PE_Payload_Log__c.CorrelationId__c, false);
        UtilityClass.readUpsertResults(results, JSON.serialize(payloadLogList), 'PE_Payload_Log__c', payloadLog.CorrelationId__c, 'WorkOrderSetup', payloadLog.ClientWorkOrderNumber__c);
    }
    
    @future
    public static void processQueuedWOUpdates(String clientWorkOrderNumber, String woCorrelationId){
        
        List<PE_Payload_Log__c> processedPayloadList = new List<PE_Payload_Log__c>();
        for(PE_Payload_Log__c processedPayload: [Select Id, CorrelationId__c From PE_Payload_Log__c Where CorrelationId__c =: woCorrelationId AND Status__c = 'Queued' AND Business_Unit__c = 'UNIFY']){
            processedPayload.Status__c = 'Processed';
            processedPayloadList.add(processedPayload);
        }
        
        if(processedPayloadList.size() > 0){
            Update processedPayloadList;
        }
        
        List<PE_Payload_Log__c> queuedPayloadLogList = [Select Id, 
                                                  CorrelationId__c, ClientWorkOrderNumber__c,
                                                  Action__c, History__c,
                                                  Contact__c, Asset__c,
                                                  Location__c, Notes__c,
                                                  Tasks__c, WorkOrder__c,
                                                  ServiceContractNumber__c
                                                    
                                                  From PE_Payload_Log__c
                                                  
                                                  Where ClientWorkOrderNumber__c =:clientWorkOrderNumber AND
                                                    Status__c = 'Queued' AND
                                                    Business_Unit__c = 'UNIFY' AND
                                                    Action__c =: Label.Unify_Update_Work_Order_Action
                                                        Order By CreatedDate ASC LIMIT 1
                                                 ]; 
        for(PE_Payload_Log__c pplog: queuedPayloadLogList){ 
            Action_B2b_To_BSA__e event = new Action_B2b_To_BSA__e();

            event.Action__c = pplog.Action__c;
            event.ClientWorkOrderNumber__c = pplog.ClientWorkOrderNumber__c;
            event.ServiceContractNumber__c = pplog.ServiceContractNumber__c;
            //event.EventTimeStamp__c='2020-09-16T07:10:40Z';
            event.CorrelationId__c = pplog.CorrelationId__c;
            event.WorkOrder__c = pplog.WorkOrder__c;
            event.Notes__c = pplog.Notes__c;
            event.Location__c = pplog.Location__c;
            event.Asset__c = pplog.Asset__c;
            event.Contact__c = pplog.Contact__c;
            event.Tasks__c = pplog.Tasks__c;
            event.History__c = pplog.History__c;
            
            Database.SaveResult sr = EventBus.publish(event);
            System.debug('DildarLog: processQueuedWOUpdates - sr - ' + sr);
        }
        System.debug('DildarLog: processQueuedWOUpdates - Result - ' + queuedPayloadLogList.size());
    }
}