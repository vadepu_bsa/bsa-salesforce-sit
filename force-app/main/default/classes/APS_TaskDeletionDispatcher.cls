public class APS_TaskDeletionDispatcher implements Queueable {
    
    private Set<Id> woToBeDeleted;
    private List<WorkOrderLineItem> woliList;
    String message;
    
    public APS_TaskDeletionDispatcher(Set<Id> woIds,List<WorkOrderLineItem> wolis,String msg){
        system.debug('Printing Debug 2');
        this.woToBeDeleted=woIds;
        this.woliList=wolis;
        this.message=msg;
    }
    public void execute(QueueableContext context) {
        integer count=0;
        
        if(!woToBeDeleted.isEmpty()){
            List<WorkOrder> deletionWorkOrders=[select id from WorkOrder where id IN:woToBeDeleted ];
            if(!deletionWorkOrders.isEmpty()){
                count=deletionWorkOrders.size();
                List<Draft_Work_Order__c> df= APS_WorkOrderGeneratorHelper.updateDraftWorkOrder(woliList,count,null);
                Id draftWoId;
                if(df!=null && df.size() > 0 ){
                    message += '\n View Draft Work Order for details : '+df.get(0).Name+ '\n';
                    draftWoId=df.get(0).id;
                }
                
                APS_WorkOrderGeneratorHelper.QuickEmail('Work Order Generation Summary', message, UserInfo.getUserEmail());
                if(message != null){
                    Application_Log__c log = new Application_Log__c(Log_Level__c = 'Error', Message__c = message.length() > 32768 ? message.substring(0, 32767) : message,Draft_Work_Order__c=draftWoId);
                    insert log;
                }
                delete deletionWorkOrders;
            }
        }
        system.debug('');
        
        
        
    }
    
}