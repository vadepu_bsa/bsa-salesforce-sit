/**
 * @File Name          : OrderDTO.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 25/08/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/08/2020         Tom Henson             Initial Version
**/
public class OrderDTO implements IDTO {

    public OrderDetails purchaseOrderDetails {get;set;} 
    public List<OrderItemDetails> orderItemDetails {get;set;} 
    public OrderDTO(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'purchaseOrderDetails') {
						purchaseOrderDetails = new OrderDetails(parser);
					} else if (text == 'orderItemDetails') {
						orderItemDetails = arrayOfOrderDetails(parser);
                    } else {
						System.debug(LoggingLevel.WARN, 'BaseDTO consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
        
    private static List<OrderItemDetails> arrayOfOrderDetails(System.JSONParser p) {
        List<OrderItemDetails> res = new List<OrderItemDetails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new OrderItemDetails(p));
        }
        return res;
    }

	
	public class OrderItemDetails {
		public String prontoOrderNumber {get;set;} 
		public String orderItemNumber {get;set;} // in json: type

		public OrderItemDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'OrderItemNumber') {
							orderItemNumber = parser.getText();
						} else if (text == 'ProntoOrderItemNumber') {
							prontoOrderNumber = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'OrderItemDetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class OrderDetails {
	   	public String orderNumber {get;set;} 
		public String prontoOrderNumber {get;set;} 
        public String orderStatus {get;set;}

		public OrderDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'OrderNumber') {
							orderNumber = parser.getText();
						} else if (text == 'ProntoOrderNumber') {
							prontoOrderNumber = parser.getText();
						}else if (text == 'OrderStatus') {
							orderStatus = parser.getText();
                        }else {
							System.debug(LoggingLevel.WARN, 'orderdetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
    
    public  OrderDTO() {
	}
	
	public  OrderDTO parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new OrderDTO(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

  
    public String getSpecificDTOClassName(){

        return 'OrderDTO';
    }
}