public class HTTPCalloutUtility {
    HTTPCalloutConfiguration__mdt calloutMetadata;
    String endpointURL, requestMethod, requestBody;
    Integer requestTimeout;
    Boolean isCompressedRequest;
    Map<String, String> urlParametersMap;
    Map<String, String> headerParametersMap;
    static final String TYPE_URL_PARAMETERS = 'URL_PARAMETERS';
    static final String TYPE_HEADER_PARAMETERS = 'HEADER_PARAMETERS';
    HTTPRequest request;
    
	/*
    * Constructor with Custom Metadata
    */
    public HTTPCalloutUtility(String customMetadataName, String JSON) {
        requestBody = JSON;
        try {
            calloutMetadata = [
                SELECT Method__c, URLParameters__c, HeaderParameters__c, Endpoint__c, Timeout__c
                FROM HTTPCalloutConfiguration__mdt
                WHERE DeveloperName =:customMetadataName
            ];
        } catch (Exception e) {
            System.debug('DildarLog: HTTPCalloutUtility - Custom Metadata Not Found! - ' + e.getMessage());
        }
        initialize();
    }
    
    /*
    * Constructor without Custom Metadata
    */
    public HTTPCalloutUtility(String url) {
        //requestBody = JSON;
        this.endpointURL = url;
        initialize();
    }
    
    /*
    * Constructor with Custom Metadata
    */
    public HTTPCalloutUtility(String customMetadataName, String JSON, String urlParameters) {
        requestBody = JSON;
        setUrlOrHeaderParameters(TYPE_URL_PARAMETERS, urlParameters);
        try {
            calloutMetadata = [
                SELECT Method__c, URLParameters__c, HeaderParameters__c, Endpoint__c, Timeout__c
                FROM HTTPCalloutConfiguration__mdt
                WHERE DeveloperName =:customMetadataName
            ];
        } catch (Exception e) {
            System.debug('DildarLog: HTTPCalloutUtility - Custom Metadata Not Found! - ' + e.getMessage());
        }
        initialize();
    }
    
    /*
    * Initialization of class variables
    */
    private void initialize() {
        //urlParametersMap = new Map<String, String>();
        headerParametersMap = new Map<String, String>();
        if(calloutMetadata != null) {
            endpointURL = calloutMetadata.Endpoint__c;
            requestMethod = calloutMetadata.Method__c;
            requestTimeout = Integer.valueOf(calloutMetadata.Timeout__c);
            setUrlOrHeaderParameters(TYPE_URL_PARAMETERS, calloutMetadata.URLParameters__c);
            setUrlOrHeaderParameters(TYPE_HEADER_PARAMETERS, calloutMetadata.HeaderParameters__c);
            request = new HTTPRequest();
        }
        else{
            endpointURL = endpointURL;
            requestMethod = 'GET';
            requestTimeout = 12000;
            //setUrlOrHeaderParameters(TYPE_URL_PARAMETERS, calloutMetadata.URLParameters__c);
            //setUrlOrHeaderParameters(TYPE_HEADER_PARAMETERS, calloutMetadata.HeaderParameters__c);
            request = new HTTPRequest();
        }
    }
    /*
    * This method is used to set URL or Header parameters from Custom Metadata
    */
    private void setUrlOrHeaderParameters(String parameterType, String parameterInfo) {
        urlParametersMap =  new Map<String, String>();
        if(String.isNotEmpty(parameterInfo)) {
            Map<String, String> parametersMap = new Map<String, String>();
            List<String> parameters = parameterInfo.split('\n');
            for(String urlParam : parameters) {
                List<String> keyValuePair = urlParam.trim().split(':');
                if(!keyValuePair.isEmpty()) {
                    if(keyValuePair.size() == 2) {
                        if(String.isNotEmpty(keyValuePair[0]) && String.isNotEmpty(keyValuePair[1])) {
                            parametersMap.put(keyValuePair[0], keyValuePair[1]);
                        }
                    } else if(
                        (keyValuePair.size() == 1) &&
                        (parameterType != TYPE_HEADER_PARAMETERS)
                    ) {
                        if(String.isNotEmpty(keyValuePair[0])) {
                            parametersMap.put(keyValuePair[0], '');
                        }
                    }
                }
            }
            if(parameterType == TYPE_URL_PARAMETERS) {
                urlParametersMap.putAll(parametersMap);
            } else if(parameterType == TYPE_HEADER_PARAMETERS) {
                headerParametersMap.putAll(parametersMap);
            }
        }
    }
    
    /*
    * This method is used to append the URL parameters at the end of URL
    */
    /*private void appendURLParameters() {
        Set<String> urlParamKeys = urlParametersMap.keySet();
        if(!urlParamKeys.isEmpty()) {
            endpointURL += '?';
            for(String urlParamKey : urlParamKeys) {
                endpointURL += urlParamKey + '=' + urlParametersMap.get(urlParamKey) + '&';
            }
            endpointURL = endpointURL.substringBeforeLast('&');
        }
    }*/
    
    /*
    * This method is used to append the URL parameters at the end of URL
    */
    public void appendURLParameters(String urlParamKey, String urlParamValue) {       
        if(urlParamValue != '' && String.isNotBlank(urlParamKey)) {
            if(endpointURL.contains('?')){
                endpointURL += urlParamKey + '=' + urlParamValue + '&';
            }else{
                endpointURL += '?';
                endpointURL += urlParamKey + '=' + urlParamValue + '&';
            }            
            endpointURL = endpointURL.substringBeforeLast('&');
        }
        if(String.isBlank(urlParamKey)){
            
			  endpointURL = endpointURL+'/'+urlParamValue;   
        }
        System.debug('endpointURL=='+endpointURL);
    }
    
    /*
    * This method is used to set Header parameters using headerParametersMap
    */
    private void addHeaderParameters() {
        for(String key : headerParametersMap.keySet()) {
            System.debug('Key:' + key + ', Value: '+ headerParametersMap.get(key));
            request.setHeader(key, headerParametersMap.get(key));
        }
    }
    
    /*
    * This method is used to set Header parameters using headerParametersMap
    */
    public void addHeaderParameters(String key, String value) {
        System.debug('Key:' + key + ', Value: '+ value);
		request.setHeader(key, value);
    }
    
    /*
    * This method is used to form HTTP Request
    */
    public void formHTTPRequest() {
        //request = new HTTPRequest();
        addHeaderParameters();
        if(String.isNotEmpty(endpointURL)) {
            //endpointURL = endpointURL.substringBefore('?');
            //appendURLParameters();
            request.setEndpoint(endpointURL);
        }
        if(String.isNotEmpty(requestMethod)) {
            request.setMethod(requestMethod);
        }
        if(String.isNotEmpty(requestBody)) {
            request.setBody(requestBody);
        }
        if(requestTimeout!=null) {
            request.setTimeout(requestTimeout);
        }        
    }
    
    /*
    * This method forms and returns the HTTP Request without sending (for debugging purposes)
    */
    public HTTPRequest getRequest() {
        formHTTPRequest();
        return request;
    }
    
    /*
    * This method is used to send HTTP Request and return the response
    */
    public HTTPResponse sendRequest() {
        formHTTPRequest();
        System.debug('DildarLog: Request Details - Request - ' + request + ', endpointURL - ' + endpointURL );
        System.debug('DildarLog: Request Details - Body - ' + requestBody );
        Http http = new Http();
        return http.send(request);
    }
    
    /*
    * This method is used to send HTTP Request received in parameter and return the response
    */
    public HTTPResponse sendRequest(HTTPRequest request) {
        Http http = new Http();
        return http.send(request);
    }
}