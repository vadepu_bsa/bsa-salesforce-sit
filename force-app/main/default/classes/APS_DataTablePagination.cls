/*
*  @description apex controller for APS_DynamicDataTable.cmp for JIRA @FSL3-61
*
*  @author      Bohao Chen
*  @date     2020-06-08
*/
public with sharing class APS_DataTablePagination {
    
    public class ResultWrapper {        
        @AuraEnabled
        public List<RowWrapper> lstRows;
        @AuraEnabled
        public List<FieldsWrapper> lstFields;
        
        public ResultWrapper(List<SObject> lstSObject, List<FieldsWrapper> lstFields) {            
            this.lstRows = new List<RowWrapper>();
            if(lstSObject != null && !lstSObject.isEmpty()) {
                for(SObject rec : lstSObject) {
                    this.lstRows.add(new RowWrapper(rec));
                }
            }            
            this.lstFields  = lstFields;
        }
        
        public ResultWrapper(List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrderWrappers, List<FieldsWrapper> lstFields) {
            this.lstRows = new List<RowWrapper>();
            for(APS_WorkOrderGeneratorCtr.WorkOrderWrapper wow : workOrderWrappers) {
                // System.debug('@wow: ' + wow);
                this.lstRows.add(new RowWrapper(wow));
            }
            this.lstFields  = lstFields;
        }
    }
    
    public class RowWrapper {
        @AuraEnabled
        public SObject record;
        @AuraEnabled
        public Boolean isChecked {get {return isChecked == null ? false : isChecked;}set;}
        @AuraEnabled
        public List<RowWrapper> children;
        @AuraEnabled
        public WorkOrder workOrder;
        @AuraEnabled
        public List<WorkOrderLineItem> workOrderLineItems;
        
        public RowWrapper(SObject record) {
            this.record = record;
        }
        
        public RowWrapper(APS_WorkOrderGeneratorCtr.WorkOrderWrapper workOrderWrapper) {
            this.workOrder = workOrderWrapper.workOrder;
            // System.debug('@line items: ' + workOrderWrapper.workOrderLineItems);
            if(workOrderWrapper.workOrderLineItems != null && !workOrderWrapper.workOrderLineItems.isEmpty()) {
                this.workOrderLineItems = workOrderWrapper.workOrderLineItems;
            }
            else {
                this.workOrderLineItems = new List<WorkOrderLineItem>();
            }
        }
    }
    
    public class FieldsWrapper {
        @AuraEnabled
        public String fieldPath { get;set; }
        @AuraEnabled
        public String label     { get;set; }
        @AuraEnabled
        public String type      { get; set; }
        
        public FieldsWrapper(String fieldPath, String strLabel, String strType) {
            this.fieldPath = fieldPath;
            this.label = strLabel;
            this.type = strType;
        }
    }
    
    @AuraEnabled
    public static ResultWrapper fetchRecords(String sObjectName, String fieldsetName, String searchField, String searchKeyWord, 
                                             String serviceTerritoriesJSON, String startDueDate, String endDueDate, 
                                             Boolean contractedChk, Boolean nonContractedChk,
                                             String servicePeriodsJSON, String customersJSON,  String sitesJSON, 
                                             String equipmentTypesJSON, String stepName) {
                                                 // System.debug('fetchRecords');
                                                 // System.debug('@sObjectName: ' + sObjectName);
                                                 // System.debug('@fieldsetName: ' + fieldsetName);
                                                 // System.debug('@searchField: ' + searchField);
                                                 // System.debug('@searchKeyWord: ' + searchKeyWord);
                                                 // System.debug('@stepName: ' + stepName);
                                                 // System.debug('@serviceTerritoriesJSON:' + serviceTerritoriesJSON);
                                                 // System.debug('@servicePeriodsJSON: ' +  servicePeriodsJSON); 
                                                 // System.debug('@customersJSON: ' +  customersJSON);                                                                         
                                                 // System.debug('@sitesJSON: ' +  sitesJSON); 
                                                 // System.debug('@equipmentTypesJSON: ' +  equipmentTypesJSON); 
                                                 // System.debug('@startDueDate: ' +  startDueDate); 
                                                 // System.debug('@endDueDate: ' +  endDueDate);  
                                                 // System.debug('@contractedChk: ' +  contractedChk);  
                                                 // System.debug('@nonContractedChk: ' +  nonContractedChk);  
                                                 // System.debug('@stepName: ' +  stepName);  
                                                 
                                                 Set<String> setFieldsToQuery = new set<String>();
                                                 List<FieldsWrapper> lstFieldsetWrapper = new List<FieldsWrapper>();
                                                 // Map<String, String> mapfiledpathlael = new Map<String, String>();
                                                 
                                                 for(Schema.FieldSetMember fieldSetMemberObj : readFieldSet(fieldsetName, sObjectName)) {            
                                                     setFieldsToQuery.add(fieldSetMemberObj.getFieldPath().toLowerCase());
                                                     // mapfiledpathlael.put(String.valueOf(fieldSetMemberObj.getType()), fieldSetMemberObj.getFieldPath());
                                                     
                                                     lstFieldsetWrapper.add(
                                                         new FieldsWrapper(fieldSetMemberObj.getFieldPath(), fieldSetMemberObj.getLabel(),String.valueOf(fieldSetMemberObj.getType()))
                                                     );
                                                 }
                                                 
                                                 ResultWrapper rw;
                                                 try {
                                                     if(sObjectName == 'WorkOrder') {
                                                         APS_WorkOrderGeneratorCtr workOrderGenerator = new APS_WorkOrderGeneratorCtr(servicePeriodsJSON, customersJSON, sitesJSON, equipmentTypesJSON, startDueDate, endDueDate, contractedChk, nonContractedChk);
                                                         List<APS_WorkOrderGeneratorCtr.EquipmentGroup> equipmentGroups = workOrderGenerator.generateEquipmentGroups();
                                                         // System.debug('@fetchRecords equipmentGroups: ' + equipmentGroups);
                                                         List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrdersWrappers = workOrderGenerator.groupEquipmentGroups(equipmentGroups);
                                                         // System.debug('@fetchRecords workOrdersWrappers: ' + workOrdersWrappers);
                                                         rw = new ResultWrapper(workOrdersWrappers,lstFieldsetWrapper);
                                                     }
                                                     else {
                                                         List<SObject> lstSObject = new List<SObject>();
                                                         Boolean isQueryable = false;
                                                         List<String> serviceTerritoryIds = (List<String>)JSON.deserialize(serviceTerritoriesJSON, Type.forName('List<String>'));                                                         
                                                         //@FSL3-118
                                                         Set<String> relatedRecordIds = fetchRelatedRecordIds(startDueDate, endDueDate, servicePeriodsJSON, stepName, contractedChk, nonContractedChk,serviceTerritoryIds);
                                                         system.debug('Related record Ids::'+relatedRecordIds);
                                                         String queryString = 'Select Id, ';
                                                         
                                                         if(stepName == 'equipment_type') {
                                                             queryString = 'Select Equipment_Type__c, ';
                                                         }
                                                         setFieldsToQuery.remove('Id');
                                                         queryString += String.join( new List<String>(setFieldsToQuery), ',');
                                                         queryString.removeEnd(',');
                                                         queryString += ' FROM ' + sObjectName;
                                                         
                                                         if(stepName == 'service_territory') {
                                                            //FR-169 - Work Order Generator tab - Start - Ben Lee 20210818
                                                            Id userid = UserInfo.getUserId();
                                                            List<String> upList = new List<String>();
                                                            List<User> userList = [SELECT Profile.Name FROM User WHERE Id =: userid LIMIT 1];
                                                            for (User us : userList){
                                                                if (us.Profile.Name == 'APS Business System Admin' || us.Profile.Name == 'System Administrator') {
                                                                    queryString += ' WHERE IsActive = true AND APS__c = true ORDER by Name ASC';
                                                                }
                                                                else {
                                                                    List<String> stList = new List<String>();
                                                                    List<FSL__User_Territory__c> utList = [SELECT FSL__ServiceTerritory__r.Service_Territory_ID__c 
                                                                                                           FROM FSL__User_Territory__c 
                                                                                                           WHERE FSL__User__c =: userid];
                                                                    for(FSL__User_Territory__c ut : utList){
                                                                        stList.add(ut.FSL__ServiceTerritory__r.Service_Territory_ID__c);
                                                                    }
                                                                    queryString += ' WHERE IsActive = true AND Service_Territory_ID__c IN: stList ORDER by Name ASC';
                                                                }
                                                            }
                                                            //FR-169 - Work Order Generator tab - End - Ben Lee 20210818
                                                            isQueryable = true;
                                                         }
                                                         else if(stepName == 'customer') {
                                                             Set<Id> parentIdset = new set<Id>();
                                                            // List<String> serviceTerritoryIds = (List<String>)JSON.deserialize(serviceTerritoriesJSON, Type.forName('List<String>'));
                                                             
                                                             List<Account> siteSTList = [SELECT ParentId FROM Account WHERE RecordType.DeveloperName = 'Site' AND Service_Territory__c IN: serviceTerritoryIds];
                                                             for (integer i = 0; i < siteSTList.size(); i++){
                                                                 parentIdset.add(siteSTList[i].ParentId);
                                                             }
                                                             queryString += ' WHERE RecordType.DeveloperName = \'Client\'';
                                                             System.debug('@searchField: ' + searchField);
                                                             
                                                             if(String.isNotBlank(searchField) && String.isNotBlank(searchKeyWord)) {
                                                                 queryString += ' AND ' + searchField + ' LIKE \'%' + searchKeyWord + '%\'';
                                                             }
                                                             
                                                             if(!parentIdset.isEmpty()) {
                                                                 set<Id> queryIdSet = new Set<Id>();
                                                                 system.debug('!!! parentIdset = ' + parentIdset);
                                                                 for (string cId : relatedRecordIds){
                                                                     if (parentIdset.contains(cid)){
                                                                         queryIdSet.add(id.valueof(cid));
                                                                     }
                                                                     isQueryable = true;
                                                                 }
                                                                 queryString += ' AND Id IN: queryIdSet';
                                                             }
                                                             system.debug('Query String::'+queryString);
                                                             
                                                         }
                                                         else if(stepName == 'site') {
                                                             queryString += ' WHERE RecordType.DeveloperName = \'Site\'';
                                                             List<String> customerIds = (List<String>)JSON.deserialize(customersJSON, Type.forName('List<String>'));
                                                            // List<String> serviceTerritoryIds = (List<String>)JSON.deserialize(serviceTerritoriesJSON, Type.forName('List<String>')); 
                                                             // System.debug('@customersJSON: ' + customersJSON);
                                                             // system.debug('@serviceTerritoryIds:' + serviceTerritoryIds);
                                                             if(!customerIds.isEmpty()) {            
                                                                 queryString += ' AND ParentId IN: customerIds';
                                                                 queryString += ' AND Service_Territory__c IN: serviceTerritoryIds';
                                                                 queryString += ' AND Contract_Type__c = \'PM\'';
                                                                 if(!relatedRecordIds.isEmpty()) {
                                                                     queryString += ' AND Id IN: relatedRecordIds';
                                                                     isQueryable = true;
                                                                 }
                                                             }
                                                         }
                                                         else if(stepName == 'equipment_type') {
                                                             List<String> siteIds = (List<String>)JSON.deserialize(sitesJSON, Type.forName('List<String>'));
                                                             if(!siteIds.isEmpty()) {
                                                                 queryString += ' WHERE AccountId IN: siteIds AND RecordType.DeveloperName = \'Equipment_Group\' AND EG_Equipment_Type__c != null';
                                                                 if(!relatedRecordIds.isEmpty()) {
                                                                     queryString += ' AND Id IN: relatedRecordIds AND Equipment_Type__c != NULL';
                                                                     isQueryable = true;
                                                                 }
                                                                 queryString += ' Group By Equipment_Type__c, ';
                                                                 queryString += String.join( new List<String>(setFieldsToQuery), ',');
                                                                 queryString.removeEnd(',');
                                                             }
                                                         }
                                                         
                                                         if(sObjectName == 'Account') {
                                                             queryString += ' Order By Name ASC';
                                                         }
                                                         
                                                         if(isQueryable) {
                                                             System.debug('queryString:: ' + queryString);
                                                             lstSObject = Database.query(queryString);
                                                         }
                                                         rw = new ResultWrapper(lstSObject,lstFieldsetWrapper);
                                                     }
                                                 }
                                                 catch(Exception e) {
                                                     // System.debug(e.getMessage());
                                                     throw new AuraHandledException(JSON.serialize(e.getMessage()));
                                                 }
                                                 return rw;
                                             }
    
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName) {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        if(!DescribeSObjectResultObj.FieldSets.getMap().containsKey(fieldSetName)) {
            throw new applicationException('Fieldset ' + fieldSetName + ' doesn\'t exist for ' + ObjectName);
        }
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }
    
    //@FSL3-118
    private static Set<String> fetchRelatedRecordIds(String startDateStr, String endDateStr, String servicePeriodsJSON, String stepName, Boolean contractedChk, Boolean nonContractedChk,List<String> serviceTerritoryIds) {
        
        List<String> servicePeriods = new List<String>();
        
        if(String.isNotBlank(servicePeriodsJSON)) {
            servicePeriods = (List<String>)JSON.deserialize(servicePeriodsJSON, Type.forName('List<String>'));
        }
        
        Set<String> filteredRecordIds = new Set<String>();
        
        if(String.isNotBlank(startDateStr) && String.isNotBlank(endDateStr) && !servicePeriods.isEmpty() && String.isNotBlank(stepName)) {
            
            Date startDate = Date.valueOf(startDateStr);
            Date endDate = Date.valueOf(endDateStr);
            
            String dynQuery = 'SELECT AssetId, Asset.AccountId, Asset.Account.ParentId, Contracted__c,MaintenancePlan.EndDate,NextSuggestedMaintenanceDate,Asset.Account.Service_Territory__c,Asset.Account.Contract_Type__c '
                + 'FROM MaintenanceAsset '
                + 'WHERE WorkTypeId != null '
                + 'AND Maintenance_Routine_Global__c IN: servicePeriods '
                + 'AND NextSuggestedMaintenanceDate >=: startDate '
                + 'AND NextSuggestedMaintenanceDate <=: endDate '
                + 'AND Asset.RecordType.DeveloperName = \'Equipment_Group\' '
                + 'AND Asset.Account.RecordType.DeveloperName = \'Site\' '
                + 'AND Asset.Account.Parent.RecordType.DeveloperName = \'Client\' ';
            //FSL3-456
            if (contractedChk != nonContractedChk) {
                if (!contractedChk && nonContractedChk){
                    dynQuery +=   'AND Contracted__c = false ';
                } else {
                    dynQuery +=   'AND Contracted__c = true ';
                }
            }
            
            System.debug('@dynQuery: ' + dynQuery);
            System.debug('@startDate: ' + startDate);
            System.debug('@endDate: ' + endDate);
            System.debug('@servicePeriods: ' + servicePeriods);
            
            List<MaintenanceAsset> mas = Database.query(dynQuery);
            
            // System.debug('@mas: ' + mas);
            
            for(MaintenanceAsset ma : mas) {
                switch on stepName {
                    when 'customer' {
                        if(serviceTerritoryIds.contains(ma.Asset.Account.Service_Territory__c) && ma.Asset.Account.Contract_Type__c=='PM' &&
                          ((ma.MaintenancePlan.EndDate != null && ma.MaintenancePlan.EndDate >= ma.NextSuggestedMaintenanceDate) || ma.MaintenancePlan.EndDate == null)){
                            filteredRecordIds.add(ma.Asset.Account.ParentId);
                        }
                    }
                    when 'site' {
                        if((ma.MaintenancePlan.EndDate != null && ma.MaintenancePlan.EndDate >= ma.NextSuggestedMaintenanceDate) || ma.MaintenancePlan.EndDate == null)
                        filteredRecordIds.add(ma.Asset.AccountId);
                    }
                    when 'equipment_type' {
                        if(ma.MaintenancePlan.EndDate != null && ma.MaintenancePlan.EndDate >= ma.NextSuggestedMaintenanceDate){
                            filteredRecordIds.add(ma.AssetId);
                        }else if(ma.MaintenancePlan.EndDate == null){
                            filteredRecordIds.add(ma.AssetId);
                        }
                    }
                }
            }
        }
        // System.debug('@filteredRecordIds: ' + filteredRecordIds);
        return filteredRecordIds;
    }
    
    public class applicationException extends Exception {}
}