/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-05-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-03-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
//@isTest(SeeAllData = false)
@isTest
public class WebMergeDocumentGeneratorTest {

    
    @TestSetup
    static void setup(){
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(1);
        lstWorkType[0].APS__c = true;
        lstWorkType[0].CUI__c = false;
        lstWorkType[0].Name = 'After Hour Call';        
        insert lstWorkType;
        
        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(1);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;        

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 3);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        //lstAccount[2].RecordTypeId = AccountClientRTypeId;
        //lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[0].Access_Attempts__c = 2;
        lstAccount[2].Access_Attempts__c = 2;
        //lstAccount[3].Access_Attempts__c = 2;
        //lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;
        
        Id contactClientRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactClientRTypeId, 1);
        insert lstContact;

        Id contactSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Site');
        List<Contact> lstContact2 = APS_TestDataFactory.createContact(lstAccount[2].Id, contactSiteRTypeId, 1);
        lstContact2[0].Service_Report_Recipient__c = true;
        insert lstContact2;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 1);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 1);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[0].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 1);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[0].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 2);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[0].Id;
        insert lstWOAR;

        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill Limit 10];
        system.debug('!!!!! skillList = ' + skillList);

        Id nonAPSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'APS_Work_Order');
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 1);
        lstWorkOrder[0].RecordTypeId = nonAPSrecordType;
        lstWorkOrder[0].WorkTypeId = lstWorkType[0].Id;
        lstWorkOrder[0].Skill_Group__c = skillList[0].DeveloperName;
        lstWorkOrder[0].Internal_Jeopardy_Comments__c = 'Asfsdf asdf sdfa asdf asdf asdf sdf sdf asdf asdfa sdfasdfsdfsdfsdf sdfs adfsdf';
        insert lstWorkOrder;      
        System.debug('Ananti Work type 1: ' + lstWorkType);
        System.debug('Ananti nonAPSrecordType 1: ' + nonAPSrecordType);    

        ///////////////////////////////////////        
        List<WorkOrderLineItem> woLineList = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id,lstWorkOrder[0].Id,lstMaintenancePlan[0].Id,2);
        insert woLineList;

        //createServiceAppointment(Id WorkOrderId, Id ServiceTerritoryId, Id SARecordTypeId, Integer numToCreate){
        Id SAppTypeId = APS_TestDataFactory.getRecordTypeId('ServiceAppointment', 'APS');
        List<ServiceAppointment> sAppList = APS_TestDataFactory.createServiceAppointment(lstWorkOrder[0].Id,lstServiceTerritory[0].Id,SAppTypeId,1);
        sAppList[0].ArrivalWindowStartTime = system.now();        
        sAppList[0].ArrivalWindowEndTime = system.now().addDays(1);
        sAppList[0].ActualStartTime = system.now();
        sAppList[0].ActualEndTime = system.now().addDays(1);
        //sAppList[0].Schedule_Delay__c = system.now().addDays(1);
        insert sAppList;        

        List<Labour_Rate__c> lstLabourRate = APS_TestDataFactory.createLabourRate(lstAccount[1].Id, 1);
        insert lstLabourRate;

        Id orderPurchaseOrder = APS_TestDataFactory.getRecordTypeId('Order', 'Non_Defect_Quote');
        List<Order> lstOrder = APS_TestDataFactory.createOrder(lstAccount[1].Id, null, lstWorkOrder[0].Id, lstWorkOrder[0].Pricebook2Id, orderPurchaseOrder, 1);
        insert lstOrder;

        lstWorkOrder[0].Order__c = lstOrder[0].Id;
        update lstWorkOrder;
        
        /////$$$$$$$$$$$$$$$
        TimeSheet ts = new TimeSheet();
        ts.ServiceResourceId = lstServiceResource[0].Id;
        ts.StartDate = Date.today();
        ts.EndDate = Date.today().addDays(1);
        insert ts;

        TimeSheetEntry tse = new TimeSheetEntry();
        tse.TimeSheetId = ts.Id;
        tse.WorkOrderId = lstWorkOrder[0].Id;
        tse.StartTime = datetime.now();
        tse.EndTime = datetime.now();
        tse.Status = 'Submitted';
        insert tse;

        ServiceReport sr = new ServiceReport();
        sr.DocumentBody = Blob.valueOf('Test Content') ; 
        sr.DocumentContentType ='application/pdf';
        sr.DocumentName='Test';
        sr.ParentId = lstWorkOrder[0].Id; 
        insert sr ;

        /*    
        List<Asset> lstAssetParents_lst = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren_lst = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Order> lstOrder_lst = [SELECT Id, Pricebook2Id FROM Order ];
        */
        List<Product2> lstProducts = APS_TestDataFactory.createProducts(5);
        insert lstProducts;

        Id stdPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> lstPricebookEntryStd = APS_TestDataFactory.createPriceBookEntries(lstProducts, stdPricebookId);
        insert lstPricebookEntryStd;

        List<PricebookEntry> lstPricebookEntry = new List<PricebookEntry>();
        for(PricebookEntry pbEntry : lstPricebookEntryStd){
            PricebookEntry pbe = new PricebookEntry();
            pbe.Pricebook2Id = lstOrder[0].Pricebook2Id;
            pbe.Product2Id = pbEntry.Product2Id;
            pbe.UnitPrice = pbEntry.UnitPrice;
            pbe.IsActive = pbEntry.IsActive;
            pbe.UseStandardPrice = pbEntry.UseStandardPrice;
            lstPricebookEntry.add(pbe);
        }
        insert lstPricebookEntry;

        List<OrderItem> lstOrderItem = APS_TestDataFactory.createOrderItem(lstAssetChildren[0].Id, lstOrder[0].Id, 2);
        lstOrderItem[0].PricebookEntryId = lstPricebookEntry[0].Id;
        lstOrderItem[0].Product2Id = lstPricebookEntry[0].Product2Id;
        lstOrderItem[1].PricebookEntryId = lstPricebookEntry[1].Id;
        lstOrderItem[1].Product2Id = lstPricebookEntry[1].Product2Id;
        lstOrderItem[1].Charge_Type__c = 'After Hours';
        insert lstOrderItem;
        
        ProductConsumed pcRecord = new ProductConsumed();
        pcRecord.PricebookEntryId = lstPricebookEntry[0].Id;
        pcRecord.QuantityConsumed = 1;
        pcRecord.WorkOrderId = lstWorkOrder[0].Id;
        insert pcRecord;             
        
        List<ResourcePreference> resPrefList = APS_TestDataFactory.createResourcePreference(lstServiceResource[0].Id,lstMaintenancePlan[0].Id,lstWorkOrder[0].Id,lstAssetChildren[0].Id,'Preferred',1);
        insert resPrefList;
        
    }       

    @isTest
    static void TestWebMergeDocumentGenerationWorkOrder() { 
        List<WorkOrder> WoOrderlistVal = [SELECT Id FROM WorkOrder];  
        //WebMergeDocumentGenerator gen = new WebMergeDocumentGenerator();
        System.debug('Ananti WoOrderlistVal 2_1: ' + WoOrderlistVal);   
        Test.StartTest();
        WebMergeDocumentGenerator.WebMergeDocumentGenerationAura(WoOrderlistVal[0].Id,'WorkOrder');
        Test.StopTest();         
    }
    
    @isTest
    static void TestWebMergeDocumentGenerationOrder() { 
        List<Order> orderlistVal = [SELECT Id FROM Order];  
        //WebMergeDocumentGenerator gen = new WebMergeDocumentGenerator();
        System.debug('Ananti orderlistVal 3_1: ' + orderlistVal);   
        Test.StartTest();
        WebMergeDocumentGenerator.WebMergeDocumentGenerationAura(orderlistVal[0].Id,'Order');
        Test.StopTest();         
    }
}