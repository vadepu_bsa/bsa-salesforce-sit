public class CustomAttributeTriggerHelper {
    public static void copyAttributesOnParent(List<Custom_Attribute__c> newCAList){
        Map<Id, List<Map<String, String>>> parentWOIdCAMap = new Map<Id, List<Map<String, String>>>();
        List<WorkOrder> pWOList = new List<WorkOrder>();
        Set<Id> pWOIdSet = new Set<Id>();
        
        for(Custom_Attribute__c ca: newCAList){
            Map<String, String> attValMap = new Map<String, String>();
            
            if(ca.Parent_Type__c == 'Work Order' && ca.Name__c == 'pcr_group' && (ca.Parent_WO_Id__c == null || ca.Parent_WO_Id__c == '')){
                attValMap.put('pcr_group__c', ca.Value__c);
            }else if(ca.Parent_Type__c == 'Work Order' && ca.Name__c == 'work_order_classification' && (ca.Parent_WO_Id__c == null || ca.Parent_WO_Id__c == '')){
                attValMap.put('Classification__c', ca.Value__c);
            }else if(ca.Parent_Type__c == 'Work Order' && ca.Name__c == 'end_user_type' && (ca.Parent_WO_Id__c == null || ca.Parent_WO_Id__c == '')){
                attValMap.put('SEGMENT_Type__c', ca.Value__c);
            }else if(ca.Parent_Type__c == 'Work Order' && ca.Name__c == 'business_wo_type' && (ca.Parent_WO_Id__c == null || ca.Parent_WO_Id__c == '')){
                attValMap.put('BWO_Type__c', ca.Value__c);
            }
            
            if(attValMap.size() > 0){
                System.debug('DildarLog : attValMap - ' + attValMap);
                
                if(parentWOIdCAMap.containsKey(ca.Work_Order__c)){
                    parentWOIdCAMap.get(ca.Work_Order__c).add(attValMap);
                }else{
                    parentWOIdCAMap.put(ca.Work_Order__c, new List<Map<String, String>>{attValMap});
                }
                
                System.debug('DildarLog : parentWOIdCAMap - ' + parentWOIdCAMap);
            }
        }
        if(parentWOIdCAMap.size() > 0){
            for(Id pWId: parentWOIdCAMap.keySet()){
                WorkOrder pWO = new WorkOrder();
                for(Object obj: parentWOIdCAMap.get(pWId)){
                    for(String keyName: ((Map<String,String>)obj).keySet()){
                        pWO.put(keyName, ((Map<String,String>)obj).get(keyName));
                    }
                }
                pWO.put('Id',pWId);
                pWOList.add(pWO);
                pWOIdSet.add(pWId);
            }
            
            System.debug('DildarLog : pWOList - ' + pWOList);
            System.debug('DildarLog : pWOIdSet - ' + pWOIdSet);
            
            List<WorkOrder> cWOList = new List<WorkOrder>();
            
            if(pWOList.size() > 0){
                update pWOList;                                
                for(WorkOrder cWORecord : [SELECT Id, ParentWorkOrder.SEGMENT_Type__c, ParentWorkOrder.BWO_Type__c, ParentWorkOrder.Classification__c, ParentWorkOrder.pcr_group__c, Classification__c, pcr_group__c FROM WorkOrder WHERE ParentWorkOrderId IN:pWOIdSet]){
                	cWORecord.Classification__c = cWORecord.ParentWorkOrder.Classification__c;
                    cWORecord.pcr_group__c = cWORecord.ParentWorkOrder.pcr_group__c;
                    cWORecord.SEGMENT_Type__c = cWORecord.ParentWorkOrder.SEGMENT_Type__c;
                    cWORecord.BWO_Type__c = cWORecord.ParentWorkOrder.BWO_Type__c;
                    cWOList.add(cWORecord);
            	}                
                update cWOList;
            }                    
        }
    }
    public static List<Custom_Attribute__c> CopyCAfromWoTask(Set<String> wotasksIds,Map<Id,Custom_Attribute__c> mcaWOTaskMap,List<Custom_Attribute__c> lstCAToInsert){
        Map<String,Work_Order_Task__c> mExtIdToWOTask = new Map<String,Work_Order_Task__c>();
        
        List<Work_Order_Task__c> lstChildWoTasks =[Select id,Client_Task_Id__c from Work_Order_Task__c where Client_Task_Id__c like :wotasksIds and Work_Order__r.Service_Resource__c!=null];
        Map<String,List<Work_Order_Task__c>> mparenttoChildWoTasks = new Map<String,List<Work_Order_Task__c>>();
        for(Work_Order_Task__c objWOTask : lstChildWoTasks){
            List<Work_Order_Task__c> tempLst;
            String key = objWOTask.Client_Task_Id__c.split(':')[0];
            if(mparenttoChildWoTasks.keyset().contains(key)){
                tempLst = mparenttoChildWoTasks.get(key);
            }
            else{
                tempLst = new List<Work_Order_Task__c>();
            }
            tempLst.add(objWOTask);
            mparenttoChildWoTasks.put(key,tempLst);
        }
        for(SObject obj : mcaWOTaskMap.values()) {
            Custom_Attribute__c objca = (Custom_Attribute__c)obj;
            List<Work_Order_Task__c> lstChildTasks =mparenttoChildWoTasks.get(objca.ExternalId_WOTask__c);
            if(lstChildTasks!=null && lstChildTasks.size()>0){
                for(Work_Order_Task__c objChildTask: lstChildTasks){
                    Custom_Attribute__c childCAObj = objca.clone(false, false, false, false); 
                    childCAObj.Work_Order_Task__c = objChildTask.Id;
                    String childWoId = objChildTask.Client_Task_Id__c.split(':')[1];
                    childCAObj.CA_External_Id__c =objca.CA_External_Id__c +':'+childWoId;
                    
                    lstCAToInsert.add(childCAObj);
                }
                
            }
        }
        
        return lstCAToInsert;
    }
    public static List<Custom_Attribute__c> CopyCAfromWO(set<Id> parentWoIds,Map<Id,Custom_Attribute__c> mcaMap,List<Custom_Attribute__c> lstCAToInsert ){
        List<WorkOrder> lstChildWos =[select id,parentWorkOrderId from WorkOrder where parentWorkOrderId in:parentWoIds and Service_Resource__c!='' ];
            if(lstChildWos!=null && lstChildWos.size()>0){
                Map<Id,List<WorkOrder>> mparenttoChildWos = new Map<Id,List<WorkOrder>>();
                for(WorkOrder objWO : lstChildWos){
                    List<WorkOrder> tempLst;
                    if(mparenttoChildWos.keyset().contains(objWO.ParentWorkOrderId)){
                        tempLst = mparenttoChildWos.get(objWO.ParentWorkOrderId);
                    }
                    else{
                        tempLst = new List<WorkOrder>();
                    }
                    tempLst.add(objWO);
                    mparenttoChildWos.put(objWO.ParentWorkOrderId,tempLst);
                }
                for(SObject obj : mcaMap.values()) {
                    Custom_Attribute__c objca = (Custom_Attribute__c)obj;
                    List<WorkOrder> lstChilds =mparenttoChildWos.get(objca.Work_Order__c);
                    if(lstChilds!=null && lstChilds.size()>0){
                        for(WorkOrder objChild: lstChilds){
                            Custom_Attribute__c childCAObj = objca.clone(false, false, false, false); 
                            childCAObj.Work_Order__c =objChild.id;
                            childCAObj.CA_External_Id__c =objca.CA_External_Id__c +':'+(String)objChild.id;
                            lstCAToInsert.add(childCAObj);
                        }
                    }
                }
            
            }
        return lstCAToInsert;
    }
}