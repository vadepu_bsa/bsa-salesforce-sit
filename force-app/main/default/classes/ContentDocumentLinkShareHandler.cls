/**
 * @author          Sunila.M
 * @date            11/Feb/2019 
 * @description    
 * Change Date    Modified by         Description  
 * 11/Feb/2019      Sunila.M        Trigger handler for ContentDocumentTrigger to make the files related to a Work Order visible to 
 *                  technician's in the Community.
 * 15/12/2020       Eliana          To send document URL to Pronto for delete /  insert / update document   (FLS3-837)
 * 15/01/2021       Sayali          To send media to NBN on Insert of Docuemnt on Parent CUI WO
 * 13/07/2021		Akash Saraswat 	FR-269: To send payload to pronto on insert of a document on technician's account object in the Community.
 **/
public class ContentDocumentLinkShareHandler implements ITriggerHandler{
    
  // Allows unit tests (or other code) to disable this trigger for the transaction
  public static Boolean TriggerDisabled = false;
  
  /*
  Checks to see if the trigger has been disabled either by custom setting or by running code
  */
  public Boolean IsDisabled()
  {
    if(TriggerDisabled==true ){
      system.debug('TriggerDisabled is true --> '   );
    }else 
    {system.debug('TriggerDisabled is false --> '   );
  }
      return TriggerDisabled;
  }
  
  List<string> allowedProfiles = new List<string>();

  public void BeforeInsert(List<SObject> ContentDocumentLinkList) 
  {
      system.debug('before BeforeInsert  ContentDocumentLink -->');
      
      //Akash - FR-269
      //system.debug('FR-269 Begins.');
      string accountLinkedEntityId=null;
      SObject fileItem=null;
      
      //make the files related to a Work Order visible to technician's in the Community.
      for(ContentDocumentLink eachContentDocLink : (List<ContentDocumentLink>) ContentDocumentLinkList){
        //Akash - FR-269          
		string objectType = eachContentDocLink.LinkedEntityId.getSObjectType().getDescribe().getName();
  		if(objectType=='Account')
        {
            accountLinkedEntityId = eachContentDocLink.LinkedEntityId;
            fileItem=(SObject)eachContentDocLink;
      	} 
          system.debug('object name -->'+eachContentDocLink.LinkedEntityId.getsobjecttype().getDescribe().getName());
          if(eachContentDocLink.LinkedEntityId.getsobjecttype().getDescribe().getName() == 'WorkOrder'){
              eachContentDocLink.Visibility = 'AllUsers';
          }
      }
      //Akash - FR-269
      if(accountLinkedEntityId!=null)
      {
                //System.debug('Before Insert accountLinkedEntityId: '+accountLinkedEntityId);
            	string currentUserProfileId = userinfo.getProfileId();
                //String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
                //system.debug('ProfileName: '+profileName);
          		if(allowedProfiles.size()==0)
          		{
          			allowedProfiles = getProfileSettings();
          		}
          		//system.debug('allowedProfiles: '+allowedProfiles);
                if(allowedProfiles.contains(currentUserProfileId))
                {
                    Account acc = getAccount(accountLinkedEntityId);
                    if(validate(acc))
                    {
                        Datetime fileUploadDateTime = System.now();
                        fileUploadDateTime=ReturnUserCurrentTime(fileUploadDateTime);
                        //System.debug('fileUploadDateTime: '+fileUploadDateTime);      
                        DateValidation item= GetNotAllowedDateTimeValue();
                        Datetime dateTimeTillNotAllowed=item.dtTmEndLimit;
                        string vMessage=item.validationMessage;
                        //System.debug('dateTimeTillNotAllowed: '+dateTimeTillNotAllowed);      
                        if(fileUploadDateTime<=dateTimeTillNotAllowed)
                        {
                            fileItem.addError(vMessage);
                            //System.debug('AddError Executed');
                        }
                    }
                }
      }
        system.debug('after BeforeInsert ContentDocumentLink -->');
  }
  
  public void AfterInsert(Map<Id, SObject> newItems) {
    system.debug('start AfterInsert trigger ContentDocumentLink -->');
      system.debug('newItems -->'+newItems);
      Set<string> objectNames;
      Map<Id,Id> documentEntityMap = new Map<Id,Id>();
       Map<Id,Id> ContentToWOMap = new Map<Id,Id>();
      Map<Id,string> documentEntityObjectMap = new Map<Id,string>();
      Set<Id> documentIds = new Set<Id>();
      Set<Id> documentIdsToPost = new Set<Id>();
      
      //Akash 
      //system.debug('FR-269 begins');
      String objectType;
      Id accountLinkedEntityId;
      string currentUserProfileId;
      
      //Added by karan 
      Set<Id> linkedEntitityIdSet = new Set<Id>();      
      for(ContentDocumentLink eachContentDocLink : (List<ContentDocumentLink>) newItems.values()){
          linkedEntitityIdSet.add(eachContentDocLink.LinkedEntityId);
          //Akash - FR-269
          objectType = eachContentDocLink.LinkedEntityId.getSObjectType().getDescribe().getName();
		  if(objectType == 'Account')
          {
					accountLinkedEntityId = eachContentDocLink.LinkedEntityId;
          }
      }
      
      //Akash - FR-269
      if(accountLinkedEntityId!=null)
		{
              		Integer uploadedAttachmentCount=0;
					DateValidation item=GetNotAllowedDateTimeValue();
					Datetime dateTimeTillNotAllowed = item.dtTmEndLimit;
            		uploadedAttachmentCount=[SELECT COUNT() FROM ContentDocumentLink where linkedentityid=:accountLinkedEntityId and SystemModStamp>:dateTimeTillNotAllowed];
                    //System.debug('uploadedAttachmentCount: '+uploadedAttachmentCount);      
                    Limit_to_Stop_Notification_To_Pronto__mdt limitValue=[SELECT Limit_Value__c FROM Limit_to_Stop_Notification_To_Pronto__mdt LIMIT 1];	
                    //System.debug('Limit Value: '+limitValue.Limit_Value__c);      
                    if(uploadedAttachmentCount<=limitValue.Limit_Value__c)	
                    {
                        //Added by Akash. FR-269: Validate technician & account details
                        currentUserProfileId = userinfo.getProfileId();
                        //String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
                        //system.debug('ProfileName: '+profileName);
                        if(allowedProfiles.size()==0)
                        {
                            allowedProfiles = getProfileSettings();
                        	//system.debug('allowedProfiles: '+allowedProfiles);
                        }
                        if(allowedProfiles.contains(currentUserProfileId))
                        {
                            Account acc = getAccount(accountLinkedEntityId);
                            if(validate(acc))
                            {
                                //system.debug('Call pronto');
                                GenerateAndSendPlatformEvent(acc, dateTimeTillNotAllowed);
                            }
                        }
         	 		}
          }
      
      Map<Id,Workorder> mWoRecs = new Map<Id,Workorder>();
      if(!Label.WOC_Team_Profile_Ids.contains(UserInfo.getProfileId())){
      		mWoRecs = new Map<Id,Workorder>([select id,recordtype.developerName from workOrder where id in :linkedEntitityIdSet and recordtype.developerName='Child_CUI_Work_Order' and Service_Contract_Number__c=:Label.NBN_Services_Contract_Number]);
      }
     
      for(ContentDocumentLink eachContentDocLink : (List<ContentDocumentLink>) newItems.values()) {
          documentIds.add(eachContentDocLink.ContentDocumentId);
          String sobjectType = eachContentDocLink.LinkedEntityId.getSObjectType().getDescribe().getName();
            if(sobjectType=='WorkOrder' && mWoRecs!=null && mWoRecs.keySet()!=null && mWoRecs.Keyset().contains(eachContentDocLink.LinkedEntityId)){
                documentIdsToPost.add(eachContentDocLink.ContentDocumentId);
                ContentToWOMap.put(eachContentDocLink.ContentDocumentId,eachContentDocLink.LinkedEntityId);
            }
          documentEntityObjectMap.put(eachContentDocLink.ContentDocumentId,eachContentDocLink.LinkedEntityId.getsobjecttype().getDescribe().getName());
          system.debug('object name -->'+eachContentDocLink.LinkedEntityId.getsobjecttype().getDescribe().getName());
          if(eachContentDocLink.LinkedEntityId.getsobjecttype().getDescribe().getName() == 'WorkOrderLineItem'){
              documentEntityMap.put(eachContentDocLink.ContentDocumentId,eachContentDocLink.LinkedEntityId);
          }
      }
      List<ContentVersion> cvList = new List<ContentVersion>();
      cvList = [Select Id from ContentVersion where ContentDocumentId =: documentIds];
/*       system.debug('cvList -->'+cvList);
      system.debug('documentEntityMap -->'+documentEntityMap);
      system.debug('documentEntityObjectMap -->'+documentEntityObjectMap); */
  
      if(!documentEntityMap.isEmpty()) {
          ContentDocumentLinkBusinessLogic.uploadFile(documentEntityMap);
      }
      system.debug(documentIds + '==documentIds');
      if(documentIds.size() > 0){
        List<WebMerge_Document_Setting__mdt> WebMergeMetadataSettingList = new List<WebMerge_Document_Setting__mdt>([SELECT Id,Included_Object_for_ContentFile_Upload__c
        FROM WebMerge_Document_Setting__mdt limit 1]);
        
        if(WebMergeMetadataSettingList.size() > 0){
          string objectName = WebMergeMetadataSettingList[0].Included_Object_for_ContentFile_Upload__c;
          List<String> lstString = objectName.split(',');
          objectNames = new Set<String>(lstString);
        } 
        List<ContentVersion> listContentVersion = new List<ContentVersion>([SELECT Id,Title,ContentDocumentId from ContentVersion where ContentDocumentId =: documentIds]);
        if(listContentVersion.size() > 0){
          set<Id> contentVerIDs = new set<Id>();
          for(ContentVersion cvObj : listContentVersion) {
            contentVerIDs.add(cvObj.Id);
          }
          system.debug(contentVerIDs + '==contentVerIDs123');
          List<ContentDistribution> listContentDistribution = new List<ContentDistribution>([Select Id,Name from ContentDistribution where ContentVersionId =: contentVerIDs]);
          
          system.debug(listContentDistribution + '==listContentDistribution123');
          
          system.debug(listContentDistribution.size() + '==listContentDistribution.size');
          if(listContentDistribution.size() == 0){
          listContentDistribution = new List<ContentDistribution>();
            for(ContentVersion cvObj : listContentVersion) {
              if(documentEntityObjectMap.containsKey(cvObj.ContentDocumentId)){
                if(objectNames.contains(documentEntityObjectMap.get(cvObj.ContentDocumentId))){
                  system.debug(cvObj.Id + '==cvObj.Id');
                  system.debug(cvObj.ContentDocumentId + '==ContentDocumentId');
                  ContentDistribution distribution = new ContentDistribution();
                  distribution.Name = cvObj.Title;
                  distribution.ContentVersionId = cvObj.Id;
                  distribution.PreferencesNotifyOnVisit = false;
                  distribution.PreferencesNotifyRndtnComplete = false;
                  listContentDistribution.add(distribution);
                }
              }
            }
            if(listContentDistribution.size() > 0)
            insert listContentDistribution;
            system.debug(listContentDistribution + '==listContentDistribution');
            
          }
          
          //////
          // Added by Eliana for FSL3-837
            system.debug('FSL3-837 AfterInsert');
            Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
            List<ContentDocumentLink> APSRecordList = new List<ContentDocumentLink>();
            List<ContentDocumentLink> nonAPSRecordList = new List<ContentDocumentLink>();

            Map<Id,string> contentdocumentlinkAPSIddMap = new map<id,string>();
          
              for(sobject obj : newItems.values()) {
                ContentDocumentLink pcObj = (ContentDocumentLink)obj;
                system.debug('FSL3-837---> before getsobjecttype WorkOrder');
                if(pcObj.LinkedEntityId.getsobjecttype().getDescribe().getName() == 'WorkOrder'){

                    //system.debug(ContentDocumentId + '==ContentDocumentId');
                    system.debug('FSL3-837---> found getsobjecttype WorkOrder');

                    APSRecordList.add(pcObj);
                    // contentdocumentlinkAPSIddMap.put(pcObj.ContentDocumentId, 'Insert');
                    contentdocumentlinkAPSIddMap.put(pcObj.Id, 'Insert');
                  } else {
                      nonAPSRecordList.add(pcObj);
                  }
              }
              if (APSRecordList.size() > 0){
                system.debug('FSL3-837--> before call generateProntoDocumentPlatformEvent - AfterInsert');
                ContentDocumentPublish.generateProntoDocumentPlatformEvent(contentdocumentlinkAPSIddMap.Keyset(),contentdocumentlinkAPSIddMap,APSRecordTypeId);
              }
          //////
          
        }
      }
      List<ContentVersion> cvListToUpload =[select id,IsCreatedFromFetchMedia__c,Media_Id__c,Title,contentDocumentId from ContentVersion where contentDocumentId in:documentIdsToPost and IsCreatedFromFetchMedia__c!=true];
      
      Map<Id,Id> ContentDocToWOMapToUpload = new Map<Id,Id>();
      if(cvListToUpload!=null && cvListToUpload.size()>0){
          for(ContentVersion cv :cvListToUpload){
            ContentDocToWOMapToUpload.put(cv.contentDocumentId,ContentToWOMap.get(cv.contentDocumentId));
              CreateMediaResourceBatch batchId = new CreateMediaResourceBatch(ContentDocToWOMapToUpload);
              database.executeBatch(batchId,1 );
              //TestMediaAPI.sendReq();
          }
      }
    }
    //Akash - FR-269
    private static List<string> getProfileSettings() {
            List<string> profileList = new List<string>();
            List<Technician_Profile_Setting__mdt> profiles = [SELECT Profile_Name__c FROM Technician_Profile_Setting__mdt];
            for (Technician_Profile_Setting__mdt profile : profiles) {
                //system.debug('profiles: '+profiles);
                //system.debug('CurrentProfile Id: '+[Select Id from Profile where Name=:profile.Profile_Name__c].Id);
                profileList.add([Select Id from Profile where Name=:profile.Profile_Name__c].Id);
            }
        //system.debug(profileList);
        return profileList;
   }
   
     /**
     * Gets the parent account if the file is linked to an account.
     */
    private Account getAccount(String linkedEntityId) {
        //system.debug('getAccount');
        Account acc;
        String id = linkedEntityId;
        if (id.substring(0,3) == '001') {
            acc = [select Pronto_Supplier_Code__c,RecordTypeId from Account where Id = :id].get(0);
        } 
		//system.debug(acc);
        return acc;
    }
    
    private boolean validate(Account acc){
        //system.debug('Validate');
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Technician').getRecordTypeId();
        
        //system.debug(accRecordTypeId);
        If(acc.RecordTypeId == accRecordTypeId && acc.Pronto_Supplier_Code__c !=null){
            return true;
        }
        
        return false;
    }
    private DateValidation GetNotAllowedDateTimeValue()
    {
        Date_for_File_Notifications_to_Pronto__mdt dayValue=[SELECT Day_Value__c, Validation_Message__c FROM Date_for_File_Notifications_to_Pronto__mdt LIMIT 1];
        Datetime dtTmTillNotAllowed = Datetime.newInstance(System.now().year(), System.now().month(), (Integer)(dayValue.Day_Value__c), 23, 59, 59);
		dtTmTillNotAllowed=ReturnUserCurrentTime(dtTmTillNotAllowed);
        DateValidation objDtValidations=new DateValidation();
        objDtValidations.dtTmEndLimit=dtTmTillNotAllowed;
        objDtValidations.validationMessage=((dayValue.Validation_Message__c!=null)?dayValue.Validation_Message__c:'You cannot upload Statutory Declaration during this time of every Month.');
        return objDtValidations;
    }
    private Datetime ReturnUserCurrentTime(Datetime dtTmValue)
    {
        Integer timeZoneOffSet = UserInfo.getTimeZone().getOffSet(dtTmValue);
        return (dtTmValue.addSeconds(timeZoneOffSet/1000));
    }
    
    private void GenerateAndSendPlatformEvent(Account obj, Datetime dateTimeTillNotAllowed)
    {
        //System.debug('Generating...');
        AccountDetails accObj=new AccountDetails();
        accObj.prontoSupplierCode=(obj.Pronto_Supplier_Code__c!=null)?obj.Pronto_Supplier_Code__c:null;
        accObj.paymentReleaseDate=string.valueOf(dateTimeTillNotAllowed.date().addDays(-1));
        Action_B2B_From_BSA__e eventObj = new Action_B2B_From_BSA__e();
        eventObj.Action__c='StatDeclNotification';
        eventObj.Notes__c=('"StatutoryDeclarationNotification":'+JSON.serialize(accObj, true));
        //System.debug('Final JSON: '+eventObj.Notes__c);
        eventObj.CorrelationId__c  = UtilityClass.getUUID();
        Datetime currentTime=System.DateTime.Now();
        currentTime=ReturnUserCurrentTime(currentTime);
        eventObj.EventDateTime__c = currentTime;
        Database.SaveResult result=EventBus.publish(eventObj);
        //System.debug('Publish Result: '+((result.isSuccess()==true)?'true':'false'));
	}
    public void BeforeDelete(Map<Id, SObject> oldItems) {}      public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}      public void AfterDelete(Map<Id, SObject> oldItems) {}      public void AfterUndelete(Map<Id, SObject> oldItems) {}    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    private class AccountDetails{
            private string prontoSupplierCode {get; set;}
            private string paymentReleaseDate {get; set;}
            private AccountDetails()
            {
                prontoSupplierCode=null;
                paymentReleaseDate=null;
            }
    	}
    private class DateValidation{
            private DateTime dtTmEndLimit {get; set;}
            private string validationMessage {get; set;}
            private DateValidation()
            {
                dtTmEndLimit=null;
                validationMessage=null;
            }
    	}

}