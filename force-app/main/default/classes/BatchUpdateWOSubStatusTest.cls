@isTest
public class BatchUpdateWOSubStatusTest {
    static testMethod void TestUpdateSubStatus(){
        	Set<Id> woSet = new Set<Id>();
        	Id validationRecordTypeId = Schema.SObjectType.Simplex_Activity_Staging__c.getRecordTypeInfosByDeveloperName().get('SOR_Activity_Validation_Report').getRecordTypeId();
        	Simplex_Activity_Staging__c sv = new Simplex_Activity_Staging__c();
        	sv.recordtypeId = validationRecordTypeId;
        	sv.Data_Validation_Outcome__c = 'VALID';
        	sv.Reason_Code__c ='Error';
        	sv.Work_Order_Id__c ='WOR700078429209';
        insert sv;
        sv = new Simplex_Activity_Staging__c();
        	sv.recordtypeId = validationRecordTypeId;
        	sv.Data_Validation_Outcome__c = 'ERROR';
        	sv.Reason_Code__c ='Error';
        	sv.Work_Order_Id__c ='WOR700078429209';
        insert sv;
        sv = new Simplex_Activity_Staging__c();
        	sv.recordtypeId = validationRecordTypeId;
        	sv.Data_Validation_Outcome__c = 'VALID';
        	sv.Reason_Code__c ='Error';
        	sv.Work_Order_Id__c ='WOR700078429210';
        insert sv;
        Id simplexWORecType = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();
        WorkOrder woSimplex = new WorkOrder();
        woSimplex.Client_Work_Order_Number__c ='WOR700078429209';
        woSimplex.RecordTypeId = simplexWORecType;
        woSimplex.GL_CODE__c ='BSE1610';
        woSimplex.Pronto_Territory_Id__c ='BVCO';
        insert woSimplex;
        woSimplex = new WorkOrder();
        woSimplex.Client_Work_Order_Number__c ='WOR700078429210';
        woSimplex.RecordTypeId = simplexWORecType;
        woSimplex.GL_CODE__c ='BSE1610';
        woSimplex.Pronto_Territory_Id__c ='BVCO';
        insert woSimplex;
        woSet.add(woSimplex.id);
        test.startTest();
         UpdateInvoicePaymentSimplexProcess uipsp = new UpdateInvoicePaymentSimplexProcess(woSet);
        BatchUpdateWOSubStatus batch = new BatchUpdateWOSubStatus();
        Database.executeBatch(batch);
        test.stoptest();
        
    }

}