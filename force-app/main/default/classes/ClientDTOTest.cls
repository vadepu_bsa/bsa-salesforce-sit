/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-23-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest(SeeAllData = false)
public class  ClientDTOTest {    
        
    /*
    @testSetup static void setup() {        
    }
    */

    @isTest
    static void TestClientSite() {                                

        Map<string,Object> mSerializedData;
        String mSpecificDTOClassName;
        String jsonString = '';
        ClientDTO mBaseDTO = new ClientDTO();
        
        Action_Account_To_BSA__e event = new Action_Account_To_BSA__e();
        event.Action__c = 'CreateSite';
		event.AccountCode__c ='NSWTF001';
		event.ContractNumber__c ='45-049';
		event.EventTimeStamp__c ='2020-09-16T05:09:58Z';
		event.CorrelationId__c ='ID000000000025';
		event.Account__c = '"accountDetails":{"accountcode":"NSWTF001","contract-site-desc[1]":"B05507 - Block A - Unit 1-2 an","contract-site-desc[2]":"d 5-6 and Common Room 368 Macq","contract-site-desc[3]":"uarie St Dubbo","territory":"","contract-rep-code":"","contract-branch-code":"TFSS","contract-start-date":"2010-11-01","contract-tfr-flag":"PM","contract-period":"52","contract-no":"45-049","contract-end-date":"2015-02-28","contract-review-date":"","na-name":"","contract-status":"Active","contract-on-hold-reason-code":"","contract-contact-phone":"","na-street":"","na-suburb":"","na-postcode":"","na-country":"","contract-charge-flag[2]":"Advance","contract-reference":"B05507","contract-billing-cycle":"N","contract-invoice-number":"0","contract-invoiced-from-date":"2010-11-01","contract-invoiced-to-date":"2010-11-01","contract-misc-field":"GE01","contract-charge-flag[4]":"N","contract-base-anniversary-date":"","xca-code":"","xca-description":"","attributeCLAS":"test","attributeHBLD":"test2","attributeHCMP":"test3","attributeHLSE":"test4","attributeHLTP":"Test5","attributeUNIT":"test6","bill-to":"","business-code":"","cycle-amount":"","deb-status":"","dr-cust-type":"","dr-industry-code":"","dr-marketing-flag":"","na-address-6":"","na-address-7":"","na-company":"","na-fax-no":"", "email":"","building-name":"","abn":"","aacn":"","na-phone":"","na-suburb_state":"","post-code":"","rep-code":"","shortname":"","warehouse":"","dr-company-mask":"","dr-user-only-alpha4-2":"","dr-credit-limit-amount":"","ced-markup-key":"","ndqt":""}';        
        //event.Contact__c = '"contactDetails":null';
        event.Contact__c = '"contactDetails":{"contactID":"N100 - 873_C","contactType":"Contract","contactName":"Debbie Hoskin","phoneNumber":"55647700","notes":" ","afterHoursPhoneNumber":"01245798852","role":"","email":"","privacyStatementIssued":"","privacyStatementIssueDate":"","availableTimes":""}';
        //event.Account__c = '" accountDetails ":{" accountcode ":" HARTNS01 "," contract - site - desc[1]":" VACANT WARRNAMBOOL - LSE000000 "," contract - site - desc[2]":" - COM0670 - BLG0355 "," contract - site - desc[3]":" "," territory ":" "," contract - rep - code ":" MEM "," contract - branch - code ":" AVMS "," contract - start - date ":" 2017 - 01 - 01 "," contract - tfr - flag ":" PM "," contract - period ":" 34 "," contract - no ":" N100 - 873 "," contract - end - date ":" 2020 - 12 - 31 "," contract - review - date ":" 2018 - 10 - 31 "," na - name ":" VACANT WARRNAMBOOL LSE000000 COM0670 BLG0355 "," contract - status ":" Entered "," contract - on - hold - reason - code ":" "," contract - contact - phone ":" "," na - street ":" UNIT 3 84 RAGLAN PDE "," na - suburb ":" WARRNAMBOOL "," na - postcode ":" 3280 "," na - country ":" VIC "," contract - charge - flag[2]":" Advance "," contract - reference ":" "," contract - billing - cycle ":" N "," contract - invoice - number ":" 2971833 "," contract - invoiced - from - date ":" 2020 - 10 - 01 "," contract - invoiced - to - date ":" 2020 - 12 - 31 "," contract - misc - field ":" HNVC "," contract - charge - flag[4]":" N "," contract - base - anniversary - date ":" "," xca - code ":" "," xca - description ":" "," cycle - amount ":" 0 "," ced - markup - key ":" HN "," attributeHBLD ":" BLG0355 "," attributeHCMP ":" COM0670 "," attributeHLSE ":" LSE000000 "}';
        //event.Contact__c = '" contactDetails ":{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "},{" contactID ":" N100 - 873_C "," contactType ":" Contract "," contactName ":" Debbie Hoskin "," phoneNumber ":" 55647700 "," notes ":" "}';

        jsonString = DeserilaizeTest(event);
        
        if (event.Action__c == 'CreateClient' || event.Action__c == 'CreateSite'){
            mSpecificDTOClassName ='ClientDTO';
        }

        if(mSpecificDTOClassName.equalsIgnoreCase('ClientDTO')){
            test.startTest();
            ClientDTO newclientdto = new ClientDTO();
            mBaseDTO = newclientdto.parse(jsonstring);
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
            Test.stopTest();
        }

        System.debug('mSerializedData: '+mSerializedData);
    }    

    static String DeserilaizeTest(Action_Account_To_BSA__e event)
    {
        String jsonString = '';
        if(String.isNotBlank(event.Account__c)){
            String tempString  = ((event.Account__c).removeEnd('}')) + ',"correlationId":"'+event.CorrelationId__c+'"},';
            // System.debug('Account JSON'+tempString);
            jsonString = jsonString + tempString;
        }

        if(String.isNotBlank(event.Contact__c)){

            jsonString = jsonString + event.Contact__c+',';
        }
        boolean singlecontact = false;
        if (jsonstring.contains('"contactDetails":{')){
        jsonstring =   jsonstring.replace('"contactDetails":{','"contactDetails":[{');
        singlecontact = true;
        }
        if (singlecontact){
            jsonstring = '{'+jsonString.removeEnd(',')+']}';
        }else {
            jsonstring = '{'+jsonString.removeEnd(',')+'}';
        }
        // System.debug('BuildJson '+ jsonstring);
        for (Integer i = 0; i < jsonstring.length(); i=i+300) {
            Integer iEffectiveEnd = (i+300 > (jsonstring.length()-1) ? jsonstring.length()-1 : i+300);
            System.debug(jsonstring.substring(i,iEffectiveEnd));
        }

        return jsonString;
    }

}