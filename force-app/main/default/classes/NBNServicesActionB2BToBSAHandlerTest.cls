//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = false)
public class NBNServicesActionB2BToBSAHandlerTest {

   @testSetup static void setup() {
       Account acc = new Account();
       acc.Name ='NBNServiceAccount';
       acc.Status__c = 'Active';
       insert acc;
       
        /*OperatingHours ohObj = new OperatingHours();
        ohObj.Name = 'Testing';
        ohObj.TimeZone = 'Australia/Sydney';
        insert ohObj;
       
        ServiceTerritory stObj = new ServiceTerritory();
        stObj.Name = 'Testting';
        stObj.Pronto_ID__c = 'TFSS';
        stObj.IsActive = True;
        stObj.Description = 'Testing';
        stObj.APS__c = true;
        stObj.OperatingHoursId = ohObj.Id;
        insert stObj;
       
       Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site').getRecordTypeId();
       Account acc = new Account();
       acc.Name ='Anderson Stuart Building';
       acc.RecordTypeId = accRecordTypeId ;
       acc.Customer_Reference__c = 'F13';
       acc.Status__c = 'Active';
       insert acc;
       
       WorkType wtObj = new WorkType();
       wtObj.Name ='Do  & Charge';
       wtObj.APS__c = true;
       wtObj.Description = 'testing';
       wtObj.EstimatedDuration = 4;    
       insert wtObj;
       
       Asset assetObj = New Asset();
        assetObj.Name = 'teting';
       assetobj.Status = 'Active';
       assetobj.accountId = acc.Id;
       insert assetobj;
       
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       WorkOrder wObj = new WorkOrder();
       wObj.Client_Work_Order_Number__c = '00044087';
    
       wObj.AccountId = acc.Id;
       wObj.WorkTypeId = wtObj.id;
       Wobj.Description = 'Testing';
       wObj.RecordTypeId = APSRecordTypeId;
       wObj.AssetId = assetObj.Id;
       insert Wobj;*/
    }
    
    @isTest
    static void validateProcessTest() {
        WorkOrderTriggerHandler.TriggerDisabled = true;
        
        Test.startTest();
        /*Action_B2b_To_BSA__e updateEvent = new Action_B2b_To_BSA__e();
        updateEvent.Action__c='UpdateWorkOrder';
        updateEvent.ClientWorkOrderNumber__c='00000510-WO1';
        updateEvent.ServiceContractNumber__c='00000510';
        updateEvent.EventTimeStamp__c='2020-09-16T07:10:40Z';
        updateEvent.CorrelationId__c='00000510-WO1U';
        updateEvent.WorkOrder__c = '\"workOrderDetails\": { \"id\": \"00000510-WO1\", \"nbn_reference_id\": \"INC10\", \"state\": \"Confirm Schedule\", \"technology\": \"HFC\", \"interaction_status_reason\": \"ASSIGNED\", \"interaction_datetime\": \"2020-10-18T03:42:54Z\", \"pcr_code\": \"P123C234R567\", \"schedule\": { \"planned_start_date\": \"2020-10-18T03:42:54Z\", \"planned_end_date\": \"2020-10-19T03:42:54Z\", \"scheduled_start_date\": \"2020-10-18T03:42:54Z\", \"earliest_start_date\": \"2020-10-19T03:42:54Z\", \"latest_end_date\": \"2020-10-19T03:42:54Z\", \"scheduled_end_date\": \"2020-10-19T03:41:54Z\", \"actual_start_date\": \"2020-10-18T03:42:54Z\", \"actual_end_date\": \"2020-10-18T03:42:54Z\", \"estimated_arrival_time\": \"2020-10-18T03:42:54Z\", \"estimated_restoration_time\": \"2020-10-18T03:42:54Z\", \"planned_remediation_date\": \"2020-10-18T03:42:54Z\" }, \"site_details\": { \"name\": \"string17\", \"description\": \"string18\", \"classification\": \"string1\", \"location_instructions\": \"string2\", \"internal_site_access_instructions\": \"string21\", \"notes\": \"string22\", \"nbn_owned_asset\": \"string19\", \"dynamic_site_attributes\": [ { \"name\": \"string3\", \"value\": \"string4\" } ] }, \"appointment\": { \"id\": \"string20\", \"href\": \"www.google.com\" }, \"service_details\": [ { \"id\": \"string6\", \"dynamic_service_attributes\": [ { \"name\": \"string7\", \"value\": \"string8\" } ] } ], \"supplementary_pcrs\": [ { \"summary\": \"problem text\", \"pcr_code\": \"P123C234R567\" } ], \"related_crq\": { \"id\": \"CRQ03\", \"template_id\": \"Fan Filters Replacement\", \"status\": \"scheduled\", \"start_date\": \"2020-10-18T03:42:54Z\", \"end_date\": \"2020-10-18T03:42:54Z\", \"type\": \"Hazard\", \"dynamic_crq_attributes\": [ { \"name\": \"string9\", \"value\": \"string10\" } ] }, \"network_summary\": [ { \"name\": \"string11\", \"value\": \"string12\" } ], \"specification\": { \"id\": \"string13\", \"version\": \"string14\" }, \"dynamic_work_order_attributes\": [ { \"name\": \"string15\", \"value\": \"string16\" } ]}';
        
        // Publish test event
        Database.SaveResult srUpdate = EventBus.publish(updateEvent);*/
        
        Action_B2b_To_BSA__e setupEvent = new Action_B2b_To_BSA__e();
        setupEvent.Action__c='SetupWorkOrder';
        setupEvent.ClientWorkOrderNumber__c='00000510-WO1';
        setupEvent.ServiceContractNumber__c='00000510';
        setupEvent.EventTimeStamp__c='2020-09-16T07:10:40Z';
        setupEvent.CorrelationId__c='00000510-WO1';
        setupEvent.WorkOrder__c = '\"workOrderDetails\": { \"id\": \"00000510-WO1\", \"nbn_reference_id\": \"INC10\", \"state\": \"Confirm Schedule\", \"technology\": \"HFC\", \"interaction_status_reason\": \"ASSIGNED\", \"interaction_datetime\": \"2020-10-18T03:42:54Z\", \"pcr_code\": \"P123C234R567\", \"schedule\": { \"planned_start_date\": \"2020-10-18T03:42:54Z\", \"planned_end_date\": \"2020-10-19T03:42:54Z\", \"scheduled_start_date\": \"2020-10-18T03:42:54Z\", \"earliest_start_date\": \"2020-10-19T03:42:54Z\", \"latest_end_date\": \"2020-10-19T03:42:54Z\", \"scheduled_end_date\": \"2020-10-19T03:41:54Z\", \"actual_start_date\": \"2020-10-18T03:42:54Z\", \"actual_end_date\": \"2020-10-18T03:42:54Z\", \"estimated_arrival_time\": \"2020-10-18T03:42:54Z\", \"estimated_restoration_time\": \"2020-10-18T03:42:54Z\", \"planned_remediation_date\": \"2020-10-18T03:42:54Z\" }, \"site_details\": { \"name\": \"string17\", \"description\": \"string18\", \"classification\": \"string1\", \"location_instructions\": \"string2\", \"internal_site_access_instructions\": \"string21\", \"notes\": \"string22\", \"nbn_owned_asset\": \"string19\", \"dynamic_site_attributes\": [ { \"name\": \"string3\", \"value\": \"string4\" } ] }, \"appointment\": { \"id\": \"string20\", \"href\": \"www.google.com\" }, \"service_details\": [ { \"id\": \"string6\", \"dynamic_service_attributes\": [ { \"name\": \"string7\", \"value\": \"string8\" } ] } ], \"supplementary_pcrs\": [ { \"summary\": \"problem text\", \"pcr_code\": \"P123C234R567\" } ], \"related_crq\": { \"id\": \"CRQ03\", \"template_id\": \"Fan Filters Replacement\", \"status\": \"scheduled\", \"start_date\": \"2020-10-18T03:42:54Z\", \"end_date\": \"2020-10-18T03:42:54Z\", \"type\": \"Hazard\", \"dynamic_crq_attributes\": [ { \"name\": \"string9\", \"value\": \"string10\" } ] }, \"network_summary\": [ { \"name\": \"string11\", \"value\": \"string12\" } ], \"specification\": { \"id\": \"string13\", \"version\": \"string14\" }, \"dynamic_work_order_attributes\": [ { \"name\": \"string15\", \"value\": \"string16\" } ]}';
        setupEvent.Notes__c = '\"noteDetails\": [ { \"id\": \"NOTE003\", \"source\": \"Upstream System\", \"created_date\": \"2020-10-18T03:42:54Z\", \"type\": \"Upstream System\", \"summary\": \"short string23\", \"details\": \"longer string\" } ]';
        setupEvent.Location__c = '\"locationDetails\":  { \"location_id\": \"LOCID01\", \"primary_access_technology\": \"string25\", \"csa_id\": \"string26\", \"region\": \"string27\", \"region_type\": \"string28\", \"fibre_service_area\": \"string29\", \"serving_area_module\": \"3RCM-20\", \"access_distribution_area\": \"string31\", \"nbn_asset_transfer\": \"string32\", \"multi_premise_site\": \"string33\", \"non_premise_location\": \"Yes\", \"service_class\": \"string34\", \"sdu_or_mdu\": \"string35\", \"boundary\": \"string36\", \"distribution_type\": \"string37\", \"land_type\": \"string38\", \"restriction_type\": \"string39\", \"dynamic_location_attributes\": [ { \"name\": \"string40\", \"value\": \"string41\" } ], \"geographicAddress\": { \"lotNumber\": \"string42\", \"planNumber\": \"\", \"roadNumber1\": \"string44\", \"roadNumber2\": \"string45\", \"roadName\": \"string46\", \"roadTypeCode\": \"string47\", \"postcode\": \"2140\", \"localityName\": \"Water Loo\", \"stateTerritoryCode\": \"VIC\", \"country\": \"Australia\", \"secondaryComplexName\": \"string51\", \"unitTypeCode\": \"string53\", \"unitNumber\": \"string54\", \"levelTypeCode\": \"string55\", \"levelNumber\": \"string56\", \"addressSiteName\": \"string57\", \"complexRoadNumber1\": \"string58\", \"complexRoadNumber2\": \"string59\", \"complexRoadName\": \"string60\", \"complexRoadTypeCode\": \"string61\", \"complexRoadSuffixCode\": \"string62\", \"locationDescriptor\": \"string63\", \"geographicDatum\": \"string64\", \"latitude\": \"-90\", \"longitude\": \"90\" } }';
        setupEvent.Asset__c = '\"assetDetails\":  [ { \"id\": \"AS03\", \"specification\": \"string67\", \"specification_type\": \"string68\", \"specification_category\": \"string69\", \"physical_name\": \"string70\", \"logical_name\": \"string71\", \"type\": \"string72\", \"sub_type\": \"string\", \"is_primary\": \"No\", \"dynamic_resource_attributes\": [ { \"name\": \"string74\", \"value\": \"string75\" } ] } ]';
        setupEvent.Contact__c = '\"contactDetails\":  [ { \"contactID\": \"CNTSN01\", \"contactType\": \"NOC Contact\", \"subtype\": \"Primary\", \"contactName\": \"John Smith\", \"phoneNumber\": \"0412345678\", \"phone_number_after_hours\": \"0412345679\", \"role\": \"Primary\", \"emailAddress\": \"John.Smith@example.com\", \"notes\": \"Dont call before 9:30\", \"available_times\": \"0900-1700\" } ]';
        setupEvent.Tasks__c = '\"tasks\": [ { \"id\": \"RTASK30001\", \"code\": \"TSK101\", \"status\": \"Open\", \"title\": \"Do the thing\", \"description\": \"Go to the place and use your hammer to do the thing\", \"approval_required\": \"Require Approval\", \"applicable\": \"Optional\", \"dynamic_task_attributes\": [ { \"name\": \"Have you modified an in-home amplifier today?\", \"value\": \"\", \"field_id\": \"in_home_amplifier_modified\" } ] },{ \"id\": \"RTASK30002\", \"code\": \"TSK102\", \"status\": \"Open\", \"title\": \"Do the thing\", \"description\": \"Go to the place and use your hammer to do the thing\", \"approval_required\": \"Require Approval\", \"applicable\": \"Optional\", \"dynamic_task_attributes\": [ { \"name\": \"Have you modified an in-home amplifier today?\", \"value\": \"\", \"field_id\": \"in_home_amplifier_modified\" } ] } ]';
        setupEvent.History__c = '\"historyDetails\": [ { \"id\":\"WORJAN0853H\", \"ticket_of_work_id\":\"WORJAN0853TH\", \"state\":\"Complete WO\", \"type\":\"Default\", \"classification\":\"HSE\", \"subtype\":\"Network Cable Down\", \"pcr_summary\":\"General\", \"closure_summary\":\"Faulty PCD. Replaced unit.\", \"closure_date\":\"2020-11-26T05:32:01Z\", \"notes\":[ { \"id\":\"WORJAN0853N\", \"source\":\"nbnExternal\", \"created_date\":\"2020-11-26T04:52:00Z\", \"type\":\"Technician Note\", \"details\":\"Adding sample technician note\" } ] } ]';

        // Publish test event
        Database.SaveResult srSetup = EventBus.publish(setupEvent);
        Test.stopTest();      
    }
    
    /* @isTest
    static void updateProntoWO_Test() {
      
        
        list<WorkOrder> workorderList = [SELECT id, WorkorderNumber FROM WorkOrder LIMIT 1];
        Action_B2b_To_BSA__e event = new Action_B2b_To_BSA__e();
        event.Action__c='ProntoWorkorder';
        event.ClientWorkOrderNumber__c='00044087';
        event.ServiceContractNumber__c='45-049';
        event.EventTimeStamp__c='2020-09-16T07:10:40Z';
        event.CorrelationId__c='ID000000000027';
        event.WorkOrder__c='\"workOrderDetails\":{\"workorderId\":\"'+workorderList[0].WorkorderNumber+'\",\"prontoWorkorderId\":\"1300029\",\"revenue-amount\":\"0\",\"cost_amount\":\"890\",\"margin-amount\":\"0\",\"labour-amount\":\"0\",\"material-amount\":\"890\",\"sub-contractor_amount\":\"0\",\"dummy\":\"0\"}';


        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
      
        Test.stopTest();

      
    }*/         

}