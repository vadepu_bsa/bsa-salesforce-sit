@isTest
public class NBNNAWorkOrderServiceImplementationTest {
    @testsetup
    public static void setup(){
        Account acc = NBNTestDataFactory.createTestAccounts();
        Contact con = NBNTestDataFactory.createContact(acc.Id);
        WorkType workType =  NBNTestDataFactory.createTestWorkType();
        Pricebook2 priceBook = NBNTestDataFactory.createTestPriceBook();
        //List<Product2> prodList=NBNTestDataFactory.createProducts();
       // NBNTestDataFactory.createPriceBookEntries(prodList,priceBook.Id);
        OperatingHours operatingHrs = NBNTestDataFactory.createTestOperatingHours();
        ServiceTerritory servTerr = NBNTestDataFactory.createTestServiceTerritory(operatingHrs.id);
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        ServiceResource serviceRes = NBNTestDataFactory.createTestResource();
        NBNTestDataFactory.createTestWOARS(acc.id,workType.id,servTerr.id,priceBook.id,serviceContract.Id);
    }
    public TestMethod static void  insertWO(){
        //setup();
        Account acc =[select id,Name from account limit 1];
        ServiceContract serviceContract =[select id,ContractNumber from ServiceContract limit 1];
        
        Test.startTest();
        User automatedUser = NBNTestDataFactory.createAutomatedUser();
        System.runAs(automatedUser){
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
        WorkOrder woChild = [select id,Service_Resource__c from WorkOrder where parentworkorderId =:workorder.Id LIMIT 1];
        workorder.NBN_Field_Work_Specified_By_Id__c ='RemediationWOS';
		workorder.Classification__c = 'OPERATE \\ DEMAND INSTALL';
        workorder.PRD_Comments__c ='Test';
        update workorder;
       /* ServiceAppointment sa =NBNTestDataFactory.createTestServiceAppointment(woChild.Id);
		
        sa.Status ='Scheduled';
        update sa;
        sa.Status ='Dispatched';
        update sa;
        sa.Status ='Accepted/*';
        update sa;
        sa.Status ='En-Route';
        update sa;*/
        NBNNAWorkOrderServiceImplementation.isWOUpdated =false;
        //workorder.Client_Inbound_Action__c ='enroute';
        workorder.SA_Status__c ='enroute';
        update workorder;
        
        
   
            workorder.SA_Status__c ='Onsite';
            update workorder;
            
            workorder.Client_Inbound_Action__c ='enroute';
            workorder.SA_Status__c ='enroute';
            update workorder;
        }
        
        
        Test.stopTest();
        //WorkOrder wo = [select id,status from workorder where id=:workorder.id];
        //System.assertEquals(wo.Status, 'Canceled');
        
        
    }
    public TestMethod static void updateSRWO(){
        map<id,List<WorkOrder>> mapWo = new map<id,List<WorkOrder>>();
        map<id,sObject> mapWo1 = new map<id,sObject>();
        Account acc =[select id,Name from account limit 1];
        ServiceContract serviceContract =[select id,ContractNumber,Number_of_Child_WO__c from ServiceContract limit 1];
        serviceContract.Number_of_Child_WO__c =2;
        update serviceContract;
        ServiceResource serviceRes =[select id,Name from ServiceResource limit 1];
        User automatedUser = NBNTestDataFactory.createAutomatedUser();
        WorkOrder workorder = new WorkOrder();
        System.runAs(automatedUser){
        workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
        }
        Work_Order_task__c wotask = NBNTestDataFactory.createWOTasks(workorder.Id);
        NBNTestDataFactory.createCustomAttributes(workorder.id,null);
        NBNTestDataFactory.createCustomAttributes(null,wotask.Id);
        NBNTestDataFactory.createNBNLogEntry(workorder.id);
        Contact con =[select id from contact limit 1];
        NBNTestDataFactory.createWOContact(workorder.Id,con.Id);
        
		WorkOrder woChild = [select id,Service_Resource__c from WorkOrder where parentworkorderId =:workorder.Id LIMIT 1];
		//ServiceAppointment sa =NBNTestDataFactory.createTestServiceAppointment(woChild.Id);
		
        
        NBNNAWorkOrderServiceImplementation.isWOUpdated =false;
		woChild.Service_Resource__c = serviceRes.Id;
        //update woChild;
        woChild.Status ='Canceled';
       // update woChild;
         Test.startTest();
        List<WorkOrder> childWoList = [select id,Service_Resource__c,Service_Contract_Number__c,recordTypeId from WorkOrder where parentworkorderId =:workorder.Id];
        WorkOrderLineItem woli = new WorkOrderLineItem();
        woli.workOrderId = childWoList[0].Id;
        insert woli;
        Work_Order_task__c woTask1 = new Work_Order_task__c();
        woTask1.Work_Order__c = childWoList[0].Id;
		insert woTask1;        
        System.assertEquals(childWoList.size(), 2);
        
        Workorder woObj=childWoList[0];
        
        WorkOrder childWo  = woObj.clone(false, false, false, false);
        insert childWo;
        system.debug(woObj.Service_Contract_Number__c+','+woObj.recordTypeId+','+woObj.isClone() +','+UserInfo.getFirstName());
        
        for (WorkOrder wo: [select id,Service_Resource__c,Service_Contract_Number__c,recordTypeId,IsCloned__c,ParentWorkOrderId from WorkOrder where parentworkorderId =:workorder.Id ]) {
            mapWo.put(wo.Id, new  List <WorkOrder> { wo });
            mapWo1.put(wo.Id,wo);
        }
        NBNNAWorkOrderServiceImplementation nwosi = new NBNNAWorkOrderServiceImplementation();
        nwosi.copyRelatedRecordstoChild(mapWo);
        nwosi.generateRelatedRecordsforChilds(mapWo1);
        
        Test.stopTest(); 
    }
    
    
   

}