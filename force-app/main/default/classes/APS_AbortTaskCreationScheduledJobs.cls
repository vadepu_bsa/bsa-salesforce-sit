global class APS_AbortTaskCreationScheduledJobs implements Schedulable {
    global void execute(SchedulableContext SC) {
        for(CronTrigger job: [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name LIKE 'Job Started At%']) {
            System.abortJob(job.id);
        }
    }
}