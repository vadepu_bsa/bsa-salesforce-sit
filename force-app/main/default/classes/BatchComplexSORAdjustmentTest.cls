@isTest
public class BatchComplexSORAdjustmentTest {
	static testMethod void testComplexActivitiesNew(){   
		Test.startTest();
			BSA_TestDataFactory.createSeedDataForTesting();
        	List<Simplex_Activity_Staging__c> sasList = new List<Simplex_Activity_Staging__c>();
        	Id complexStagingRecordTypeId = Schema.SObjectType.Simplex_Activity_Staging__c.getRecordTypeInfosByName().get('Complex SOR Adjustment Before PA').getRecordTypeId();
        	Id complexRTWOId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Child CUI Work Order').getRecordTypeId();
        	WorkOrder wo = [Select Id, WorkOrderNumber, Client_Work_Order_Number__c from WorkOrder where parentworkorderid != null limit 1];
        	wo.Sub_Status__c = 'New';
        	wo.RecordTypeId = complexRTWOId;
        	
        	
        	PriceBookEntry pbe = [Select Id, pricebook2.Name, pricebook2id, Product2.ProductCode From PriceBookEntry Where Product2.ProductCode ='02-03-00-02' AND Pricebook2.name != 'Standard Price Book' Limit 1];
        	wo.Pricebook2Id = pbe.pricebook2id;
        	Update wo;
        	
        	ProductConsumed pcRecord = new ProductConsumed();
            pcRecord.PricebookEntryId = pbe.Id;
            pcRecord.QuantityConsumed = 1;
            pcRecord.WorkOrderId = wo.Id;
            pcRecord.Sales_Order_Number__c = '';
        	
        	insert pcRecord;
        
        	PriceBookEntry pbe2 = [Select Id, pricebook2.Name, pricebook2id, Product2.ProductCode From PriceBookEntry Where Product2.ProductCode ='02-03-00-03' AND Pricebook2.name != 'Standard Price Book' Limit 1];
        	ProductConsumed pcRecord2 = new ProductConsumed();
            pcRecord2.PricebookEntryId = pbe2.Id;
            pcRecord2.QuantityConsumed = 1;
            pcRecord2.WorkOrderId = wo.Id;
            pcRecord2.Sales_Order_Number__c = '';
        	
        	insert pcRecord2;
        	
        	List<Product2> pr2List = new List<Product2>();
            for(Product2 pr2: [Select Id, Task_Code__c from Product2]){
                pr2.Task_Code__c = 'test-'+pr2.Id;
                pr2List.add(pr2);
            }
        	update pr2List;
        	
        	Simplex_Activity_Staging__c sas = new Simplex_Activity_Staging__c();
        	sas.Work_Order_Id__c = 'number';
            sas.Work_Order_Lines_Description__c = 'WOR700078429209';
            sas.SOR_Id__c = '02-03-00-02';
            sas.SOR_Quantity__c = '1';
        	sas.RecordTypeId = complexStagingRecordTypeId;
        	sas.Child_Work_Order_Number__c = wo.WorkOrderNumber;
            sasList.add(sas);
        
        	Simplex_Activity_Staging__c sas2 = new Simplex_Activity_Staging__c();
        	sas2.Work_Order_Id__c = 'number1';
            sas2.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas2.SOR_Id__c = '02-03-00-01';
            sas2.SOR_Quantity__c = '-1';
        	sas2.RecordTypeId = complexStagingRecordTypeId;
        	sas2.Child_Work_Order_Number__c = wo.WorkOrderNumber;
        	sasList.add(sas2);
                
        	Simplex_Activity_Staging__c sas3 = new Simplex_Activity_Staging__c();
        	sas3.Work_Order_Id__c = 'number1';
            sas3.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas3.SOR_Id__c = '02-03-00-03';
            sas3.SOR_Quantity__c = '1';
        	sas3.RecordTypeId = complexStagingRecordTypeId;
        	sas3.Child_Work_Order_Number__c = wo.WorkOrderNumber;
			sas3.status__c = 'Cancel Line';
        	sasList.add(sas3);
        
        	Simplex_Activity_Staging__c sas4 = new Simplex_Activity_Staging__c();
        	sas4.Work_Order_Id__c = 'number1';
            sas4.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas4.SOR_Id__c = '02-03-00-04';
            sas4.SOR_Quantity__c = '1';
        	sas4.RecordTypeId = complexStagingRecordTypeId;
        	sas4.Child_Work_Order_Number__c = wo.WorkOrderNumber;
			sasList.add(sas4);
        
        	insert sasList;			
	        
            BatchComplexSORAdjustment batchObj = new BatchComplexSORAdjustment();
            DataBase.executeBatch(batchObj);       	
			        
        Test.stopTest();
    }
    static testMethod void testComplexActivitiesRFR(){   
		Test.startTest();
			BSA_TestDataFactory.createSeedDataForTesting();
        	List<Simplex_Activity_Staging__c> sasList = new List<Simplex_Activity_Staging__c>();
        	Id complexStagingRecordTypeId = Schema.SObjectType.Simplex_Activity_Staging__c.getRecordTypeInfosByName().get('Complex SOR Adjustment Before PA').getRecordTypeId();
        	Id complexRTWOId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Child CUI Work Order').getRecordTypeId();
        	WorkOrder wo = [Select Id, WorkOrderNumber, Client_Work_Order_Number__c from WorkOrder where parentworkorderid != null limit 1];
        	wo.Sub_Status__c = 'Ready For Review';
        	wo.RecordTypeId = complexRTWOId;        	
        	
        	PriceBookEntry pbe = [Select Id, pricebook2.Name, pricebook2id, Product2.ProductCode From PriceBookEntry Where Product2.ProductCode ='02-03-00-02' AND Pricebook2.name != 'Standard Price Book' Limit 1];
        	wo.Pricebook2Id = pbe.pricebook2id;
        	Update wo;
        	
        	List<Product2> pr2List = new List<Product2>();
            for(Product2 pr2: [Select Id, Task_Code__c from Product2]){
                pr2.Task_Code__c = 'test-'+pr2.Id;
                pr2List.add(pr2);
            }
        	update pr2List;
        	
        	Line_Item__c newLI = new Line_Item__c();
        	
        	newLI.BSA_Unit_Rate__c = 10;
			newLI.BSA_Cost__c = 10;
            newLI.Technician_Unit_Rate__c = 10;
            newLI.Technician_Cost__c = 10;
        	newLI.SOR_Code__c = '02-03-00-03';
            newLI.Line_Item_External_Id__c = '02-03-00-03-'+wo.id;
           	newLI.Quantity__c = 1;
			newLI.Work_Order__c = wo.id;
            newLI.SOR_Description__c = 'Testing';
            newLI.Main_Work_Order__c = wo.id;
        	
        	insert newLI;
        
        	Line_Item__c newLI2 = new Line_Item__c();
        	
        	newLI2.BSA_Unit_Rate__c = 10;
			newLI2.BSA_Cost__c = 10;
            newLI2.Technician_Unit_Rate__c = 10;
            newLI2.Technician_Cost__c = 10;
        	newLI2.SOR_Code__c = '02-03-00-01';
            newLI2.Line_Item_External_Id__c = '02-03-00-01-'+wo.id;
           	newLI2.Quantity__c = 1;
			newLI2.Work_Order__c = wo.id;
            newLI2.SOR_Description__c = 'Testing';
            newLI2.Main_Work_Order__c = wo.id;
        	
        	insert newLI2;
        	
        	Simplex_Activity_Staging__c sas = new Simplex_Activity_Staging__c();
        	sas.Work_Order_Id__c = 'number';
            sas.Work_Order_Lines_Description__c = 'WOR700078429209';
            sas.SOR_Id__c = '02-03-00-02';
            sas.SOR_Quantity__c = '1';
        	sas.RecordTypeId = complexStagingRecordTypeId;
        	sas.Child_Work_Order_Number__c = wo.WorkOrderNumber;
            sasList.add(sas);
        
        	Simplex_Activity_Staging__c sas2 = new Simplex_Activity_Staging__c();
        	sas2.Work_Order_Id__c = 'number1';
            sas2.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas2.SOR_Id__c = '02-03-00-01';
            sas2.SOR_Quantity__c = '1';
        	sas2.RecordTypeId = complexStagingRecordTypeId;
        	sas2.Child_Work_Order_Number__c = wo.WorkOrderNumber;
        	sas2.status__c = 'Cancel Line';
        	sasList.add(sas2);
                
        	Simplex_Activity_Staging__c sas3 = new Simplex_Activity_Staging__c();
        	sas3.Work_Order_Id__c = 'number1';
            sas3.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas3.SOR_Id__c = '02-03-00-03';
            sas3.SOR_Quantity__c = '-1';
        	sas3.RecordTypeId = complexStagingRecordTypeId;
        	sas3.Child_Work_Order_Number__c = wo.WorkOrderNumber;
        	sasList.add(sas3);
        
        	Simplex_Activity_Staging__c sas4 = new Simplex_Activity_Staging__c();
        	sas4.Work_Order_Id__c = 'number1';
            sas4.Work_Order_Lines_Description__c = 'WOR7000784292091';
            sas4.SOR_Id__c = '02-03-00-04';
            sas4.SOR_Quantity__c = '2';
        	sas4.RecordTypeId = complexStagingRecordTypeId;
        	sas4.Child_Work_Order_Number__c = wo.WorkOrderNumber;
        	sasList.add(sas4);
        
        	insert sasList;
				        
            BatchComplexSORAdjustment batchObj = new BatchComplexSORAdjustment();
            DataBase.executeBatch(batchObj);       	
			        
        Test.stopTest();
    }
}