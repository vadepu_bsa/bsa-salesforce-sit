/* Author: Abhijeet Anand
 * Date: 02 December, 2019
 * Trigger Handler added for LogEntryTrigger
 */

public class LogEntryHandler implements ITriggerHandler {

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //After insert method 
    public void AfterInsert(Map<Id, SObject> newItems) { 

        List<SObject> logEntryList = new List<SObject>();

        //Pass NBNLogEntry List
        if(!newItems.isEmpty()) {
            logEntryList.addAll(newItems.values());
            LogEntryBusinessLogic.logEntryFromBSAToNBN(logEntryList);
        }

    }
    
    
    public void BeforeInsert(List<SObject> newItems) {} public void BeforeDelete(Map<Id, SObject> oldItems) {} public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
}