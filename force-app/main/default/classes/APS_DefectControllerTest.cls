@isTest
private class APS_DefectControllerTest {

    static final List<String> maintenanceRoutines = new String[]{'Monthly', 'Quarterly', 'Bi-Monthly', 'Quarterly', 'Six-Monthly', 'Yearly', 'Bi-Annual', '5 Yearly'};

    static String startDueDateStr {
        get{
            if(String.isBlank(startDueDateStr)) {
                Date startDueDate = Date.today();
                startDueDateStr = startDueDate.year() + '-' + startDueDate.month() + '-' + startDueDate.day();
            }
            return startDueDateStr;
        }
        set;
    }
            
    static String endDueDateStr {
        get{
            if(String.isBlank(endDueDateStr)) {
                Date endDueDate = Date.today().addMonths(1);
                endDueDateStr = endDueDate.year() + '-' + endDueDate.month() + '-' + endDueDate.day();
            }
            return endDueDateStr;
        }
        set;
    }

    @TestSetup
    static void setup(){
        APS_TestDataFactory.testDataSetup();
         List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Product2> products = new List<Product2>();
        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'GenericDefect',
                'Name' => 'Generic Defect',
                'Type__c' => 'Part',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'GenericLabourInternal',
                'Name' => 'Generic Labour Internal',
                'Type__c' => 'Labour',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'GenericPartSOR',
                'Name' => 'Generic Part',
                'Type__c' => 'Part',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'APSF',
                'Name' => 'APS Filter',
                'Type__c' => 'Part',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        insert products;

        String pricebookId = Test.getStandardPricebookId();

        List<PricebookEntry> pbes = APS_TestDataFactory.createPricebookEntries(products, pricebookId);
        insert pbes;

        List<Account> sites = [SELECT Id FROM Account WHERE Name = 'test site'];
        List<Asset> equipments = [SELECT Id FROM Asset WHERE Name = 'test equipment'];
        // create defects
        List<Defect__c> defects = new List<Defect__c>();

        defects.addAll((List<Defect__c>)TestRecordCreator.createSObjects('Defect__c', 1,
            new Map<String, object>{
                'Site__c' => sites[0].Id,
                'Defect_Details__c' => 'Circuit no longer burned',
                'Defect_Status__c' => 'Open',
                'Subject__c' => 'Circuit has burned out',
                'Asset__c' => equipments[0].Id,
                'SOR_Code__c' => 'APSF'
            }, null));

        defects.addAll((List<Defect__c>)TestRecordCreator.createSObjects('Defect__c', 1,
        new Map<String, object>{
            'Site__c' => sites[0].Id,
            'Defect_Details__c' => 'Circuit no longer burned',
            'Defect_Status__c' => 'Open',
            'Subject__c' => 'Circuit has burned out',
            'Asset__c' => equipments[0].Id,
            'SOR_Code__c' => ''
        }, null));

        insert defects;
    }  

    @IsTest
    static void redirecToLightningComponentTest() {

        List<Defect__c> defects = [SELECT Id FROM Defect__c];

        Test.startTest();
        Test.setCurrentPage(Page.APS_CreateDefectQuote);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(defects);
        stdSetController.setSelected(defects);
        APS_DefectController defectCtr = new APS_DefectController(stdSetController);
        PageReference pr = defectCtr.redirectToLC();
      //  System.assert(pr.getUrl().contains('/lightning/cmp/c__APS_CreateDefectQuote?c__listofDefects='));
        Test.stopTest();
    }

    @IsTest
    static void createDefectQuoteNoSiteTest() {

        String defectIdsStr = '';
        for(Defect__c def : [SELECT Id FROM Defect__c]) {
            defectIdsStr += def.Id + ',';
        }
        
        defectIdsStr = defectIdsStr.removeEnd(',');

        Test.startTest();
        try {
            APS_DefectController.createDefectQuote(defectIdsStr);
        } 
        catch(Exception e) {
            System.assertEquals('Price book doesn\'t exist on the site, customer or service territory', e.getMessage());
        }
        Test.stopTest();
    }

    @IsTest
    static void createDefectQuoteTest() {

        String defectIdsStr = '';
        for(Defect__c def : [SELECT Id FROM Defect__c]) {
            defectIdsStr += def.Id + ',';
        }
        
        defectIdsStr = defectIdsStr.removeEnd(',');

        List<Account> sites = [SELECT Id FROM Account WHERE Name = 'test site'];

        for(Account site : sites) {
            site.Price_Book__c = Test.getStandardPricebookId();
        }

        update sites;

        Test.startTest();
        String quoteId = APS_DefectController.createDefectQuote(defectIdsStr);
        System.assert(String.isNotBlank(quoteId));
        Test.stopTest();
    }

}