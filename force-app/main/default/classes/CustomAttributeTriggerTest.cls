@isTest
public class CustomAttributeTriggerTest {
	@testSetup static void setup() {
        List<WorkOrder> woList = new List<WorkOrder>();
        //BSA_TestDataFactory.createSeedDataForTesting(); 
        Account acc = NBNTestDataFactory.createTestAccounts();
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
        ServiceResource serviceRes = NBNTestDataFactory.createTestResource();
        Work_Order_task__c wotask = NBNTestDataFactory.createWOTasks(workorder.id);
        ServiceResource servResource = BSA_TestDataFactory.createTestServiceResource();     
        for(WorkOrder wOj : [SELECT Id, Service_resource__c from WorkOrder where parentWorkorderid != null ]){
            
            wOj.Service_resource__c = servResource.id;
            woList.add(wOj);
        }        
        update woList;        
        /*
        workorder.Service_Resource__c = servResource.Id;
        update workorder;*/
    }
    static testMethod void validateCopyCustomAttributeOnParentWO() {
        WorkOrder parentWO = [Select Id From WorkOrder Where ParentWorkOrderId = null limit 1];
        List<Custom_Attribute__c> newCAList = new List<Custom_Attribute__c>();
        
        Test.startTest();
        	Custom_Attribute__c newCARecord1 = new Custom_Attribute__c();
            newCARecord1.Parent_Type__c = 'Work Order';
            newCARecord1.Work_Order__c = parentWO.Id;
            newCARecord1.CA_External_Id__c = parentWO.Id;
            newCARecord1.Name__c = 'pcr_group';
            newCARecord1.Value__c = 'PCR';
                    	
            newCAList.add(newCARecord1);
            
            Custom_Attribute__c newCARecord2 = new Custom_Attribute__c();
            newCARecord2.Parent_Type__c = 'Work Order';
            newCARecord2.Work_Order__c = parentWO.Id;
            newCARecord2.CA_External_Id__c = parentWO.Id;
            newCARecord2.Name__c = 'work_order_classification';
            newCARecord2.Value__c = 'Classification';
                    	
            newCAList.add(newCARecord2);
        
        	insert newCAList;
            
        Test.stopTest();
    }
    static testMethod void validateCopyCAfromWO() {
        WorkOrder parentWO = [Select Id From WorkOrder Where ParentWorkOrderId = null limit 1];
        WorkOrder childWO = [Select Id From WorkOrder Where ParentWorkOrderId != null limit 1];
        ServiceResource srObj =[select id from ServiceResource limit 1]; 
        childWO.Service_Resource__c = srObj.Id;
        update childWO;
        List<Custom_Attribute__c> newCAList = new List<Custom_Attribute__c>();
        
        Test.startTest();
        	Custom_Attribute__c newCARecord1 = new Custom_Attribute__c();
            newCARecord1.Parent_Type__c = 'Work Order';
            newCARecord1.Work_Order__c = parentWO.Id;
            newCARecord1.CA_External_Id__c = parentWO.Id;
            newCARecord1.Name__c = 'pcr_group';
            newCARecord1.Value__c = 'PCR';
                    	
            newCAList.add(newCARecord1);
            
            Custom_Attribute__c newCARecord2 = new Custom_Attribute__c();
            newCARecord2.Parent_Type__c = 'Work Order';
            newCARecord2.Work_Order__c = parentWO.Id;
            newCARecord2.CA_External_Id__c = parentWO.Id;
            newCARecord2.Name__c = 'work_order_classification';
            newCARecord2.Value__c = 'Classification';
                    	
            newCAList.add(newCARecord2);
        
        	insert newCAList;
            
        Test.stopTest();
        List<Custom_Attribute__c> lstChildAtt = [select id from Custom_Attribute__c where Work_Order__c=:childWo.Id];
//        System.assert(lstChildAtt.size()>0);
        
    }
    static testMethod void validateCopyCAfromWOTask() {
        WorkOrder parentWO = [Select Id From WorkOrder Where ParentWorkOrderId = null limit 1];
        WorkOrder childWO = [Select Id From WorkOrder Where ParentWorkOrderId != null limit 1];
        ServiceResource srObj =[select id from ServiceResource limit 1]; 
        childWO.Service_Resource__c = srObj.Id;
        update childWO;
        Work_Order_task__c woTask =[select id from Work_Order_task__c Limit 1];
        List<Custom_Attribute__c> newCAList = new List<Custom_Attribute__c>();
        
        Test.startTest();
        	Custom_Attribute__c newCARecord1 = new Custom_Attribute__c();
            newCARecord1.Parent_Type__c = 'Work Order Task';
            newCARecord1.Work_Order_Task__c = woTask.Id;
            newCARecord1.CA_External_Id__c = parentWO.Id;
            newCARecord1.Name__c = 'pcr_group';
            newCARecord1.Value__c = 'PCR';
                    	
            newCAList.add(newCARecord1);
            
            Custom_Attribute__c newCARecord2 = new Custom_Attribute__c();
            newCARecord2.Parent_Type__c = 'Work Order Task';
            newCARecord2.Work_Order_Task__c = woTask.Id;
            newCARecord2.CA_External_Id__c = parentWO.Id;
            newCARecord2.Name__c = 'work_order_classification';
            newCARecord2.Value__c = 'Classification';
                    	
            newCAList.add(newCARecord2);
        
        	insert newCAList;
            
        Test.stopTest();
        List<Custom_Attribute__c> lstChildAtt = [select id from Custom_Attribute__c where Work_Order_Task__c=:woTask.Id];
        System.assert(lstChildAtt.size()>0);
        
    }
    static testMethod void validateAfterUpdate() {
        WorkOrder parentWO = [Select Id From WorkOrder Where ParentWorkOrderId = null limit 1];
        WorkOrder childWO = [Select Id From WorkOrder Where ParentWorkOrderId != null limit 1];
        ServiceResource srObj =[select id from ServiceResource limit 1]; 
        childWO.Service_Resource__c = srObj.Id;
        update childWO;
        Work_Order_task__c woTask =[select id from Work_Order_task__c Limit 1];
        List<Custom_Attribute__c> newCAList = new List<Custom_Attribute__c>();
        
        
        	Custom_Attribute__c newCARecord1 = new Custom_Attribute__c();
            newCARecord1.Parent_Type__c = 'Work Order Task';
            newCARecord1.Work_Order__c = parentWO.Id;
            newCARecord1.CA_External_Id__c = parentWO.Id;
            newCARecord1.Name__c = 'pcr_group';
            newCARecord1.Value__c = 'PCR';
                    	
            newCAList.add(newCARecord1);
            insert newCARecord1;
            Test.startTest();
            newCARecord1.Name__c ='pcr_group1';
        update newCARecord1;
        Test.stopTest();
        List<Custom_Attribute__c> lstChildAtt = [select id from Custom_Attribute__c where Work_Order__c=:parentWO.Id];
        System.assert(lstChildAtt.size()>0);
        
    }
    
    // ADDED FOR CUI CLAIMS DEPLOYMENT
    static testMethod void validateDuplicateCustomAttribute() {
        WorkOrder parentWO = [Select Id From WorkOrder Where ParentWorkOrderId = null limit 1];
        List<Custom_Attribute__c> newCAList = new List<Custom_Attribute__c>();
        
        Test.startTest();
        	Custom_Attribute__c newCARecord1 = new Custom_Attribute__c();
            newCARecord1.Parent_Type__c = 'Work Order';
            newCARecord1.Work_Order__c = parentWO.Id;
            newCARecord1.CA_External_Id__c = parentWO.Id;
            newCARecord1.Name__c = 'pcr_group';
            newCARecord1.Value__c = 'PCR';
        	newCARecord1.FromCode__c = true;
                    	
            newCAList.add(newCARecord1);
            
            Custom_Attribute__c newCARecord2 = new Custom_Attribute__c();
            newCARecord2.Parent_Type__c = 'Work Order';
            newCARecord2.Work_Order__c = parentWO.Id;
            newCARecord2.CA_External_Id__c = parentWO.Id;
            newCARecord2.Name__c = 'work_order_classification';
            newCARecord2.Value__c = 'Classification';
                    	
            newCAList.add(newCARecord2);
        
        	insert newCAList;
            
        Test.stopTest();
    }
    
    static testMethod void validateUpdateCA() {
        WorkOrder parentWO = [Select Id From WorkOrder Where ParentWorkOrderId != null limit 1];        
        List<Custom_Attribute__c> newCAList = new List<Custom_Attribute__c>();
        
        Test.startTest();
        	Work_Order_Task__c wotaskObj = new Work_Order_Task__c();
            wotaskObj.Work_Order__c = parentWO.Id;
            wotaskObj.Work_Order_Task_Id__c ='NBNExt001';
            wotaskObj.Code__c='TSK000501';
            wotaskObj.Incorrect_Code__c=false;
            Insert wotaskObj;
        
        	Custom_Attribute__c newCARecord1 = new Custom_Attribute__c();
            newCARecord1.Parent_Type__c = 'Work Order';
            newCARecord1.Work_Order__c = parentWO.Id;
            newCARecord1.CA_External_Id__c = parentWO.Id;
            newCARecord1.Name__c = 'pcr_group';
            newCARecord1.Value__c = 'PCR';
        	newCARecord1.FromCode__c = true;
        	newCARecord1.Work_Order_Task__c = wotaskObj.Id;
                    	
            newCAList.add(newCARecord1);
            
            Custom_Attribute__c newCARecord2 = new Custom_Attribute__c();
            newCARecord2.Parent_Type__c = 'Work Order';
            newCARecord2.Work_Order__c = parentWO.Id;
            newCARecord2.CA_External_Id__c = 'EXTID';
            newCARecord2.Name__c = 'work_order_classification';
            newCARecord2.Value__c = 'Classification'; 
        	newCARecord2.FromCode__c = false;
        	newCARecord2.Work_Order_Task__c = wotaskObj.Id;
                    	
            newCAList.add(newCARecord2);
        
        	insert newCAList;
        /*
        	newCARecord2.CA_External_Id__c = 'EXTID';
        	newCARecord2.Value__c = '8';
        	update newCARecord2;
        */
        	
            
        Test.stopTest();
    }
}