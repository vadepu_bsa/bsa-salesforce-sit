/**
* @author          Sunila.M
* @date            07/March/2019 
* @description     Test class for ProductConsumedTrigger
*
* Change Date    Modified by         Description  
* 07/March/2019 		Sunila.M		Created Test class
**/
@isTest
public class ContentDocumentLinkTriggerTest {
    
    @isTest static void contentDocumentVisibility() {
        
        Test.startTest();
        //create Account
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account1 = new Account();
        account1.Name = 'Optus Business';
        account1.RecordTypeId = clientRecordTypeId;
        insert account1;
        system.debug('account1'+account1.Id);
        
         Id contactClientRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        
         Contact conObj = New Contact();
         conObj.LastName = 'Testing';
         conObj.Phone = '1234567812';
         conObj.MobilePhone  ='987654321987';
         conObj.AccountId = account1.Id;
         conObj.RecordTypeId = contactClientRecordTypeId;
         insert conObj;
        
        Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account account2 = new Account();
        account2.Name = 'Optus Business One';
        account2.RecordTypeId = customerRecordTypeId;
        insert account2;
        
        //--Create Work type
        WorkType workType = new WorkType();
        workType.Name = 'SC (Service Assurance)';
        workType.APS__c = False;
        workType.CUI__c = true;
        Date todayDate = Date.today();
        workType.EstimatedDuration = 5;
        
        insert workType;
        system.debug('workType'+workType.Id);
        
        //--create Price Book
        Pricebook2 priceBook = new Pricebook2();
        priceBook.Name = 'NSW-CN-Metro-PRICE-BOOK';
        priceBook.IsActive = true;
        insert priceBook;
        
        //--create operating hours
        OperatingHours operatingHours = new OperatingHours();
        operatingHours.Name = 'calender 1';
        insert operatingHours;
        
        //--create service territory
        ServiceTerritory serviceTerritory = new ServiceTerritory();
        serviceTerritory.Name = 'New South Wales';
        serviceTerritory.OperatingHoursId = operatingHours.Id;
        serviceTerritory.IsActive = true;
        insert serviceTerritory;
        
        Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
        System.debug('devRecordTypeId'+devRecordTypeId);
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        //--create WOAR
        Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
        woar.Account__c = account1.Id;
        woar.RecordTypeId = devRecordTypeId;
        woar.Work_Area__c = 'Metro';
        woar.Work_Type__c = workType.Id;
        woar.Service_Territory__c = serviceTerritory.Id;
        woar.Price_Book__c = priceBook.Id;
        insert woar;
        
        //create case
        Case caseRecord =new Case();
        caseRecord.Work_Area__c = 'Metro';
        caseRecord.Status = 'New';
        //caseRecord.Task_Type__c = 'Hazard';
        caseRecord.Origin = 'Email';
        caseRecord.Work_Type__c = workType.Id;
        caseRecord.AccountId = account1.Id;
        caseRecord.ContactId = conObj.Id;
        insert caseRecord;
        
        
        //insert WO
        WorkOrder workOrder = new WorkOrder();
        workOrder.Work_Area__c = 'Metro';
        workOrder.WorkTypeId = workType.Id;
        workOrder.ServiceTerritoryId = serviceTerritory.Id;
        workOrder.CaseId = caseRecord.Id;
        workOrder.city = 'City';
        workOrder.country ='Australia';
        workOrder.state='NSW';
        workOrder.street ='Street1';
        workOrder.PostalCode = '2132';
        workOrder.Client_Work_Order_Number__c  ='number';
        workOrder.Client_Approval_Received__c = true;
        insert workOrder;
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true);
        insert contentVersion_1;
        
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = workOrder.id;
        contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink; 
        
        Test.stopTest();
    }
    
}