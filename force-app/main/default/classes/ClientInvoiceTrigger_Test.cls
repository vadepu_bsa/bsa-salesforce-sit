/**
* @author          Sunila.M
* @date            10/April/2019	
* @description     Test class for ClientInvoiceTrigger
*
* Change Date    Modified by         Description  
* 10/April/2019		Sunila.M		Created Test class for ClientInvoiceTrigger
**/
@isTest
public class ClientInvoiceTrigger_Test {
    
    @isTest static void checkClientInvoiceExists() {
        
        Test.startTest();
        //create Account
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account1 = new Account();
        account1.Name = 'Test Account';
        account1.RecordTypeId = clientRecordTypeId;
        insert account1;
        system.debug('account1'+account1.Id);
        
        Id contactClientRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        
         Contact conObj = New Contact();
         conObj.LastName = 'Testing';
         conObj.Phone = '1234567812';
         conObj.MobilePhone  ='987654321987';
         conObj.AccountId = account1.Id;
         conObj.RecordTypeId = contactClientRecordTypeId;
         insert conObj;
        
        Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account account2 = new Account();
        account2.Name = 'Test Account Two';
        account2.RecordTypeId = customerRecordTypeId;
        insert account2;
        
        
        //--Create Work type
        WorkType workType = new WorkType();
        workType.Name = 'CN (Install)';
        workType.APS__c = false;
        workType.CUI__c = true;
        Date todayDate = Date.today();
        workType.EstimatedDuration = 5;
        
        insert workType;
        system.debug('workType'+workType.Id);
        
        //--create Price Book
        Pricebook2 priceBook = new Pricebook2();
        priceBook.Name = 'NSW-CN-Metro-PRICE-BOOK';
        priceBook.IsActive = true;
        insert priceBook;
        
        //--create operating hours
        OperatingHours operatingHours = new OperatingHours();
        operatingHours.Name = 'calender 1';
        insert operatingHours;
        
        //--create service territory
        ServiceTerritory serviceTerritory = new ServiceTerritory();
        serviceTerritory.Name = 'New South Wales';
        serviceTerritory.OperatingHoursId = operatingHours.Id;
        serviceTerritory.IsActive = true;
        insert serviceTerritory;
        
        Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
        System.debug('devRecordTypeId'+devRecordTypeId);
        
        //--create WOAR
        Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
        woar.Account__c = account1.Id;
        woar.RecordTypeId = devRecordTypeId;
        woar.Work_Area__c = 'Metro';
        woar.Work_Type__c = workType.Id;
        woar.Service_Territory__c = serviceTerritory.Id;
        woar.Price_Book__c = priceBook.Id;
        insert woar;
        
        //create case
        Case caseRecord =new Case();
        caseRecord.Work_Area__c = 'Metro';
        caseRecord.Status = 'New';
        //caseRecord.Task_Type__c = 'Hazard';
        caseRecord.Origin = 'Email';
        caseRecord.Work_Type__c = workType.Id;
        caseRecord.AccountId = account1.Id;
        caseRecord.ContactId = conObj.Id;
        insert caseRecord;
        
        
        //insert WO
        WorkOrder workOrder = new WorkOrder();
        workOrder.Work_Area__c = 'Metro';
        workOrder.WorkTypeId = workType.Id;
        workOrder.ServiceTerritoryId = serviceTerritory.Id;
        workOrder.CaseId = caseRecord.Id;
        workOrder.city = 'City';
        workOrder.country ='Australia';
        workOrder.state='NSW';
        workOrder.street ='Street1';
        workOrder.PostalCode = '2132';
        workOrder.Client_Work_Order_Number__c  ='number';
        insert workOrder;
        
        //insert Invoice records
        Invoice__c invoice1= new Invoice__c();
        invoice1.Work_Order__c = workOrder.Id;
        invoice1.WorkType__c = workType.Id;
        invoice1.Account__c = account1.Id;
        invoice1.Invoice_Date__c = todaydate.addDays(1);
        insert invoice1;
        
        //insert Client Invoice
        Client_Invoice__c clientInvoice1 = new Client_Invoice__c();
        clientInvoice1.Client__c = account1.Id;
        clientInvoice1.WorkType__c = workType.Id;
        clientInvoice1.StartDate__c = todaydate;
        clientInvoice1.EndDate__c = todaydate.addDays(10);
        insert clientInvoice1;
        
        system.debug('>>'+[SELECT Id,Client_Invoice__c FROM Invoice__c]);
        
        //insert Client Invoice
        Client_Invoice__c clientInvoice2 = new Client_Invoice__c();
        clientInvoice2.Client__c = account1.Id;
        clientInvoice2.WorkType__c = workType.Id;
        clientInvoice2.StartDate__c = todaydate;
        clientInvoice2.EndDate__c = todaydate.addDays(10);
        try{
            insert clientInvoice2;
        }
        catch(Exception ex){
            System.debug(ex.getMessage());
        }

        Test.stopTest();
    }
}