/* Author: Abhijeet Anand
* Date: 17 October, 2019
* Apex class that handles all pre/post DML operations on the FSL object 'ServiceResource'
*/
public class ServiceResourceAutomationBusinessLogic {
    
    // Method to associate a Location with a Service Resource based on the unique Pronto Service Resource ID post successfull insertion of a Service Resource
    public static void linkServiceResourceToLocation (List<SObject> resourceList, Map<Id,SObject> oldResourceMap) {
        
        Map<String,Schema.Location> resourceLocationMap = new Map<String,Schema.Location>();
        Set<String> prontoServiceIDs = new Set<String>();
        
        System.debug('resourceList: ' + resourceList);
        
        for(SObject obj : resourceList) {
            ServiceResource srRes = (ServiceResource)obj;
            //Added a check store user id field. 
            if(srRes.RelatedRecordId != null){
                
                srRes.User_Id__c = String.valueof(srRes.RelatedRecordId).substring(0,15);
            }
            
            if(oldResourceMap != null) {
                ServiceResource oldResource = (ServiceResource) oldResourceMap.get(srRes.Id);
                if(oldResource.Pronto_Service_Resource_ID__c != srRes.Pronto_Service_Resource_ID__c && srRes.Pronto_Service_Resource_ID__c != null) { //Check if Pronot Service Resource Id has changed on the Service Resource
                    prontoServiceIDs.add(srRes.Pronto_Service_Resource_ID__c); // Set to hold the Pronto Service Resource ID
                }
            }
            else if(srRes.Pronto_Service_Resource_ID__c != null) { // On pre insert of Service Resource
                prontoServiceIDs.add(srRes.Pronto_Service_Resource_ID__c); // Set to hold the Pronto Service Resource ID 
            }
        }
        System.debug('prontoServiceIDs'+prontoServiceIDs);
System.debug('Query resutls'+Database.query('Select Id, Pronto_Service_Resource_ID__c FROM Location WHERE Pronto_Service_Resource_ID__c IN : prontoServiceIDs'));
        if(!prontoServiceIDs.isEmpty()) {
            // Query the Location with matching Pronto Service Resource IDs
            for(Schema.Location obj : [Select Id, Pronto_Service_Resource_ID__c FROM Location WHERE Pronto_Service_Resource_ID__c IN : prontoServiceIDs]) {
                if(prontoServiceIDs.contains(obj.Pronto_Service_Resource_ID__c)) {
                    resourceLocationMap.put(obj.Pronto_Service_Resource_ID__c,obj); // Map of Pronto Service Resource & Location sObject
                }
            }
            
            if(!resourceLocationMap.isEmpty()) {
                for(SObject obj : resourceList) {
                    ServiceResource srRes = (ServiceResource)obj;
                    if(resourceLocationMap.containskey(srRes.Pronto_Service_Resource_ID__c) && srRes.LocationId != resourceLocationMap.get(srRes.Pronto_Service_Resource_ID__c).Id ) {                       
                        System.debug('srRes.LocationId'+resourceLocationMap.get(srRes.Pronto_Service_Resource_ID__c).Id);
                        srRes.LocationId = resourceLocationMap.get(srRes.Pronto_Service_Resource_ID__c).Id; // set the LocationId 
                    }
                }
            }
        }
        
    }
    
    public static void shareProductItemWithServiceResource(List<SObject> resourceList, Map<Id,SObject> oldResourceMap){
        
        Set<Id> newLocationIdSet = new Set<id>();
        Set<Id> oldLocationIdSet = new Set<id>();
        Set<Id> newProdItemIdSet = new Set<id>();
        Set<Id> oldProdItemIdSet = new Set<id>();
        Map<String,String> userIdToOldAndNewLocationIdMap = new Map<String,String>();
        List<SObject> shareListToCreate = new  List<SObject>();
        //List<ProductItemShare> shareListToDelete =  List<ProductItemShare>();
        Map<Id,Set<Id>> locationIdToUserIdSetMap = new Map<Id,Set<Id>>();
        
        for(SObject obj : resourceList) {
            ServiceResource srRes = (ServiceResource)obj;
            if(oldResourceMap != null) {
                ServiceResource oldResource = (ServiceResource) oldResourceMap.get(srRes.Id);
                if(oldResource.LocationId != srRes.LocationId && srRes.LocationId != null) { //Check if location Id has changed on the Service Resource
                    newLocationIdSet.add(srRes.LocationId); // Set to hold the Location ID
                    //oldLocationIdSet.add(oldResource.LocationId);
                    //userIdToOldAndNewLocationIdMap.put(srRes.RelatedRecordId,oldResource.LocationId + '-'+srRes.LocationId);
                    if(!locationIdToUserIdSetMap.containsKey(srRes.LocationId)){
                        
                        locationIdToUserIdSetMap.put(srRes.LocationId, new Set<Id>{srRes.RelatedRecordId});
                    } else {
                        
                        Set<Id> tempSet = locationIdToUserIdSetMap.get(srRes.LocationId);
                        tempSet.add(srRes.RelatedRecordId);
                        locationIdToUserIdSetMap.put(srRes.LocationId,tempSet);
                    }
                    
                }
            }
            else if(srRes.LocationId != null) { // On pre insert of Service Resource
                newLocationIdSet.add(srRes.LocationId); // Set to hold the Location ID 
                if(!locationIdToUserIdSetMap.containsKey(srRes.LocationId)){
                    
                    locationIdToUserIdSetMap.put(srRes.LocationId, new Set<Id>{srRes.RelatedRecordId});
                } else {
                    
                    Set<Id> tempSet = locationIdToUserIdSetMap.get(srRes.LocationId);
                    tempSet.add(srRes.RelatedRecordId);
                    locationIdToUserIdSetMap.put(srRes.LocationId,tempSet);
                }
            }
        }
        
        for(ProductItem prodItem : [SELECT Id,LocationId FROM ProductItem WHERE LocationId IN : newLocationIdSet]){
            for(Id relatedUserId : locationIdToUserIdSetMap.get(prodItem.LocationId)){
                
                SObject prdShare = UtilityClass.createObjectShare(prodItem.Id, relatedUserId, 'Edit');
                shareListToCreate.add(prdShare);
            }
        }
        
        System.debug('ProductItemToShare'+shareListToCreate);
        if(!shareListToCreate.isEmpty()){
            
            INSERT shareListToCreate;
        }
        
    }
    
}