public class NBNLogEntryTriggerHelper {
    public static void mapDirection(List<sObject> nbnLogList){
        Map<String,String> mSourceToDir = new Map<String,String>();
        mSourceToDir.put('INTERNAL NOTES','BSA TO BSA');
        mSourceToDir.put('NOTES TO ROUTER','BSA TO TECH');
        mSourceToDir.put('LATEST UPDATE FOR NBN','FromBSAToNBN');
        mSourceToDir.put('ACTIVITY NOTES FROM NBN','FromNBNToBSA');
        mSourceToDir.put('NOTES BY TECH','FromBSAToNBN');
        mSourceToDir.put('ACMA PRD EXTENSION NOTE','FromBSAToNBN');
        mSourceToDir.put('NOTES TO ACCOUNTS','BSA TO ACCOUNTS');
        mSourceToDir.put('OTHERS','BSA TO BSA');
        for(sObject sobj:nbnLogList){
            NBN_Log_Entry__c objLog = (NBN_Log_Entry__c)sobj;
            if(objLog.Source__c!=null){
                if(mSourceToDir.get(objLog.Source__c)!=null){
                	objLog.Log_Entry_Direction__c = mSourceToDir.get(objLog.Source__c);
                }
            }
            
        }
    }

}