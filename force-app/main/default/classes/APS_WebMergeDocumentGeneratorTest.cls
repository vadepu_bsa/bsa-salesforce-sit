/**
 * @author          Mayukhman
 * @date            8/Sept/2019 
 * @description     Test class for WebMergeDocumentGenerator
 *
 * Change Date    Modified by           Description  
 * 
 **/
@isTest
public class APS_WebMergeDocumentGeneratorTest {
  @TestSetup
    static void setup(){
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = false;
        lstWorkType[1].CUI__c = true;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        //lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;

        Id contactClientRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactClientRTypeId, 1);
        insert lstContact;

        Id contactSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Site');
        List<Contact> lstContact2 = APS_TestDataFactory.createContact(lstAccount[2].Id, contactSiteRTypeId, 1);
        lstContact2[0].Service_Report_Recipient__c = true;
        insert lstContact2;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 5);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 2);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;

        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill Limit 10];
        system.debug('!!!!! skillList = ' + skillList);

        Id nonAPSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'Work_Order');
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 1);
        lstWorkOrder[0].RecordTypeId = nonAPSrecordType;
        lstWorkOrder[0].WorkTypeId = lstWorkType[1].Id;
        lstWorkOrder[0].Skill_Group__c = skillList[0].DeveloperName;
        insert lstWorkOrder;

        Id woliRT = APS_TestDataFactory.getRecordTypeId('WorkOrderLineItem', 'Asset_Action');
        List<WorkOrderLineItem> lstWoli = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id, lstWorkOrder[0].Id, lstMaintenancePlan[0].Id, 3);
        lstWoli[0].RecordTypeId = woliRT;
        lstWoli[1].RecordTypeId = woliRT;
        lstWoli[2].RecordTypeId = woliRT;
        lstWoli[1].AssetId = lstAssetChildren[1].Id;
        lstWoli[2].AssetId = lstAssetChildren[2].Id;
        insert lstWoli;

        Id woliRTJSA_FIRE = APS_TestDataFactory.getRecordTypeId('WorkOrderLineItem', 'JSA_FIRE');
        List<WorkOrderLineItem> lstWoliJSA_FIRE = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id, lstWorkOrder[0].Id, lstMaintenancePlan[0].Id, 1);
        lstWoliJSA_FIRE[0].RecordTypeId = woliRTJSA_FIRE;
        insert lstWoliJSA_FIRE;

        Id SARecordTypeId = APS_TestDataFactory.getRecordTypeId('ServiceAppointment', 'APS');
        List<ServiceAppointment> lstServiceAppointment = APS_TestDataFactory.createServiceAppointment(lstWorkOrder[0].Id, lstServiceTerritory[0].Id, SARecordTypeId, 1);
        lstServiceAppointment[0].Status = 'New';
        lstServiceAppointment[0].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[0].FSL__Scheduling_Policy_Used__c= fslScheduling.Id;
        insert lstServiceAppointment;  

        List<ResourcePreference> lstResourcePreference = APS_TestDataFactory.createResourcePreference(lstServiceResource[0].Id,lstMaintenancePlan[0].Id,lstWorkOrder[0].Id,lstAssetChildren[1].Id,'Preferred',1);
        insert lstResourcePreference;  

        TimeSheet ts = new TimeSheet();
        ts.ServiceResourceId = lstServiceResource[0].Id;
        ts.StartDate = Date.today();
        ts.EndDate = Date.today().addDays(1);
        insert ts;

        TimeSheetEntry tse = new TimeSheetEntry();
        tse.TimeSheetId = ts.Id;
        tse.WorkOrderId = lstWorkOrder[0].Id;
        tse.StartTime = datetime.now();
        tse.EndTime = datetime.now();
        tse.Status = 'Submitted';
        insert tse;

        ServiceReport sr = new ServiceReport();
        sr.DocumentBody = Blob.valueOf('Test Content') ; 
        sr.DocumentContentType ='application/pdf';
        sr.DocumentName='Test';
        sr.ParentId = lstWorkOrder[0].Id; 
        insert sr ;

        SkillRequirement maintSR = new SkillRequirement (RelatedRecordId = lstWorkOrder[0].Id, SkillId = skillList[0].Id);
        insert maintSR;

        Defect__c def = new Defect__c();
        def.Work_Order_Line_Item__c = lstWoli[0].Id;
        def.Work_Order__c = lstWorkOrder[0].Id;
        def.Asset__c = lstAssetParents[0].Id;
        def.Defect_Status__c = 'Open';
        def.Subject__c = 'Test Subject';
        insert def;

        // Insert Product
        Product2 product = new Product2();
        product.ProductCode = 'F1/1.3.4.1';
        product.Name ='F1/1.3.4.1 /product';
        product.Family = 'None';
        insert product;

        Id pricebookId = Test.getStandardPricebookId();
        // Insert PricebookEntry
        PricebookEntry priceBookEntry = new PricebookEntry();
        pricebookEntry.Pricebook2Id = pricebookId;
        pricebookEntry.Product2Id = product.id;
        pricebookEntry.UnitPrice = 0.0;
        pricebookEntry.IsActive = true;
        pricebookEntry.UseStandardPrice = false;
        insert pricebookEntry;
        // Insert Order        
        Order o = new Order();
        o.AccountId = lstAccount[1].Id;
        o.Status = 'draft';
        o.Name='Test12345';
        o.OrderReferenceNumber = '804405';
        o.RecordTypeId = TestRecordCreator.getRecordTypeByDeveloperName('Order', 'Purchase_Order');
        o.EffectiveDate = Date.today(); 
        o.Pricebook2Id =  pricebookId;  
        o.Work_Order__c = lstWorkOrder[0].Id;
        insert o;
        // Insert Order Item
        OrderItem i = new OrderItem();
        i.OrderId = o.id;
        i.Quantity = 24;
        i.UnitPrice = 240;
        i.Product2id = product.id;
        i.PricebookEntryId=priceBookEntry.id;
        //insert i;

        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentVersion> cvdocuments = [SELECT Id, ContentDocumentId FROM ContentVersion where id =: contentVersion.Id];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = lstWorkOrder[0].id;
        cdl.ContentDocumentId = cvdocuments[0].ContentDocumentId;
        cdl.shareType = 'V';
        insert cdl;

        ContentVersion contentVersion2 = new ContentVersion(
            Title = 'Defects',
            PathOnClient = 'Defects.jpg',
            VersionData = Blob.valueOf('Test Defect'),
            IsMajorVersion = true
        );
        insert contentVersion2;    
        List<ContentVersion> cvdocuments2 = [SELECT Id, ContentDocumentId FROM ContentVersion where id =: contentVersion2.Id];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl2 = New ContentDocumentLink();
        cdl2.LinkedEntityId = def.id;
        cdl2.ContentDocumentId = cvdocuments2[0].ContentDocumentId;
        cdl2.shareType = 'V';
        insert cdl2;

    }
  @IsTest
    static void TestWorkOrderDocumentGenerator() {

        List<WorkOrder> workOrders = [SELECT Id, Maintenance_Plan__c FROM WorkOrder];
        Test.startTest();                
        WebMergeDocumentGenerator.WebMergeDocumentGenerationAura(workOrders[0].Id,'WorkOrder');
        Test.stopTest();
    }
    @IsTest
    static void TestWorkOrderDocumentGenerator2() {
      List<WorkOrder> workOrders = [SELECT Id, Maintenance_Plan__c FROM WorkOrder];
      WebMergeDocumentGenerator.WebMergeFlowInputs temp = new WebMergeDocumentGenerator.WebMergeFlowInputs();
      temp.sObjectId = workOrders[0].Id;
      temp.sObjectName = 'WorkOrder';
      List<WebMergeDocumentGenerator.WebMergeFlowInputs> input = new List<WebMergeDocumentGenerator.WebMergeFlowInputs>();
      input.add(temp);
        
      Test.startTest();                
      WebMergeDocumentGenerator.WebMergeDocumentGenerationInvocable(input);
      Test.stopTest();
    }
  @IsTest
    static void TestOrderDocumentGenerator() {
      List<Order> Orders = [SELECT Id FROM Order];
/*
      User user1 = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
      Approval.ProcessSubmitRequest req1 = 
          new Approval.ProcessSubmitRequest();
      req1.setComments('Submitting request for approval.');
      req1.setObjectId(Orders[0].id);
      req1.setSubmitterId(user1.Id); 
      req1.setProcessDefinitionNameOrId('Quote_Limit');
      Approval.ProcessResult result = Approval.process(req1);
*/
      Test.startTest();                
      WebMergeDocumentGenerator.WebMergeDocumentGenerationAura(Orders[0].Id,'Order');
      Test.stopTest();
  }
      
  }