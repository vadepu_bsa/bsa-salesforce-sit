public class WSNBNFileInboundActionDTR implements IDTR {
	
    private WSNBNFileInboundActionDTO fileDTO;
        
    public WSNBNFileInboundActionDTR (){
        
        
    } 
    
    public WSNBNFileInboundActionDTR transform(Object dtoClass,Id woId,String processId){ 
        
        fileDTO = (WSNBNFileInboundActionDTO) dtoClass; 
        if(processId ==Label.Get_Media_List_Process){
            ContentVersion cv = new ContentVersion();
            cv.Media_Id__c = fileDTO.id;
            //cv.ContentLocation = 'S';
            //cv.ContentDocumentId = contentDocumentId;
            cv.Title = 'Blank Image';
            cv.PathOnClient = 'BlankImage'+woId+'.png';
            cv.VersionData = Blob.valueOf('iVBORw0KGgoAAAANSUhEUgAAAd8AAABtCAIAAABvF/zFAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGISURBVHhe7dQxAQAADMOg+Tfd2cgBIrgB0GNngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgBFdgYosjNAkZ0BiuwMUGRngCI7AxTZGaDIzgA92wPLx5jXqhb7swAAAABJRU5ErkJggg==');
            cv.IsMajorVersion = false;
            cv.WOMediaId__c = woId;
            insert cv;
           
            
            System.debug('Sayali Debug:CV-->'+cv.id);
            System.debug('Sayali Debug:CVID-->'+cv.id);
        }
        else if(processId ==Label.Get_Media_Resource_Process){
            String url = fileDTO.Content.presignedUrl;
            ContentVersion cv = new ContentVersion();
            cv.Media_Id__c = fileDTO.id;
            System.debug('SAYALIDEBUG CV Resource :'+fileDTO.Content.presignedUrl);
            //cv.ContentLocation = 'S';
            //cv.ContentDocumentId = contentDocumentId;
            
            cv.Title = fileDTO.Metadata.fileName;
            cv.PathOnClient = fileDTO.Metadata.fileName;
			cv.Media_Id__c = fileDTO.id;
            cv.WOMediaId__c = woId;
            NBNFileUploadDownloadHandler nbnHandler = New NBNFileUploadDownloadHandler(woId,url,cv);
            nbnHandler.process(Label.Get_Media_Image_Process);
        }
        
        
        return null;
    }
    public WSNBNFileInboundActionDTR transformUpload(Object dtoClass,Id woId,String processId,ContentVersion cv){
        fileDTO = (WSNBNFileInboundActionDTO) dtoClass; 
        if(processId ==Label.Create_Media_Resource){
            String url = fileDTO.Content.presignedUrl;
            System.debug('url=='+url);
            if(url!=null && url!=''){
                NBNFileUploadDownloadHandler nbnHandler = New NBNFileUploadDownloadHandler(woId,url,cv,true);
                nbnHandler.process(Label.Post_to_URL);
            }
        }
        return null;
    }
}