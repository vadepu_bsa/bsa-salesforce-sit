/**
* @File Name          : ActionB2BToBSAHandler.cls
* @Description        : Generic PE's handler. This is instantiated from PlatformEventSubscriptionFactory 
* @Author             : Karan Shekhar
* @Group              : 
* @Last Modified By   : Karan Shekhar
* @Last Modified On   : 30/05/2020, 8:20:08 pm
* @Modification Log   : 
* Ver       Date            Author                  Modification
* 1.0    01/05/2020   Karan Shekhar                Initial Version
**/

public class ActionB2BToBSAHandler extends PlatformEventSubscriptionBaseHandler {
    
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private sObjectType mPESobjectType;
    private IDTO mDTOClass;
    private SydUniDTO mSydUniDTO;
    private ProntoWODTO mProntoWODTO;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    private string eventActionName ;
    
    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
        
        for(sObject sObj : peToProcessList){
            Action_B2B_To_BSA__e event = (Action_B2B_To_BSA__e)sObj;
            eventActionName = event.Action__c;
            deserializeJSON(event);
            // setupDeserializedDataForMapping();
            setupAdditonalSettings(event);
            
        }
        super.process(peSobjectType,peToProcessList, mappedSobjectType);        
    }
    
    private void setupAdditonalSettings(Action_B2B_To_BSA__e event){
        
        mIdentifierMapping = new Map<string,String>();
        mIdentifierMapping.put('Action_Name__c',event.Action__c);  
        //mAdditionalWhereClause = 'PE_Subscription_Object_Action_Mapping__r.Action_Name__c = \''+event.Action__c+'\'';            
        setAdditionalMappingFilters('Action_Name__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);
    }
    
    private void deserializeJSON(Action_B2B_To_BSA__e event){
        
        String jsonString = '';
        if(String.isNotBlank(event.WorkOrder__c)){
            
            //String tempString  = ((event.WorkOrder__c).removeEnd('}')) + ',"serviceContractNumber":"'+event.ServiceContractNumber__c+'"},';
            String tempString  = ((event.WorkOrder__c).removeEnd('}')) + ',"correlationId":"'+event.CorrelationId__c+'"},';
            System.debug('Work ORder JSON'+tempString);
            jsonString = jsonString + tempString;
        }
        jsonString = '{'+jsonString.removeEnd(',')+'}';
        System.debug('BuildJson'+ jsonString);
      
        if (event.Action__c == 'SydneyUniWorkOrder'  ){
            mSpecificDTOClassName ='SydUniDTO';
        } else if (event.Action__c == 'ProntoWorkOrder' ){
            mSpecificDTOClassName ='ProntoWODTO';
        }
        System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        if(mSpecificDTOClassName.equalsIgnoreCase('SydUniDTO')){
            SydUniDTO newSydUniDTO = new SydUniDTO();
            mSydUniDTO = newSydUniDTO.parse(jsonstring.unescapeEcmaScript());
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mSydUniDTO));
        }
        if(mSpecificDTOClassName.equalsIgnoreCase('ProntoWODTO')){
            ProntoWODTO newProntoDTO = new ProntoWODTO();
            mProntoWODTO = newProntoDTO.parse(jsonstring.unescapeEcmaScript());
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mProntoWODTO));
        }
        
        System.debug('Serialized class'+mSerializedData);
        
    }
    
    public virtual void deserilaizeToSpecificClass(){
        
        
    }
    
    public override void processBuinessLogic(){
        List<sObject> finalsObjectList = getMappedObjectList();
        List<WorkOrder> workorderList = new list<Workorder>();
        
        List<sobject> sObjectwoList = New List<sobject>();
        String woExternalField = getMappedExternalIdFromNode('workorderDetails',false);
        String woExternalIdValue = '';
        String woCorrelationId = '';
        
        for(sObject obj: finalsObjectList){
            if(String.valueOf(obj.getSObjectType()) == 'WorkOrder'){
                sObjectwoList.add(obj);
                //woExternalIdValue = (String)((WorkOrder)obj).get('Client_Work_Order_Number__c');
                woExternalIdValue = eventActionName == 'ProntoWorkorder' ? (String)((WorkOrder)obj).get('Client_Work_Order_Number__c') : (String)((WorkOrder)obj).get('External_Client_WO_Reference_Number__c');
                woCorrelationId = (String)((WorkOrder)obj).get('CorrelationId__c');
            }       
        }
        
        if(sObjectwoList.size() > 0){
            
            SObjectField woRefId = getSObjectFieldName('WorkOrder', woExternalField);
            Set<Id> successWoIdSet = new Set<Id>();
            if (eventActionName =='SydneyUniWorkOrder'){
                List<Worktype> worktypeList = [SELECT ID, Name FROM Worktype Where Name = 'Do  & Charge'];
                // system.debug('!!!!Mahmood Before finalListWorkOrder' + sObjectwoList);
                set<string> buildingIdSet = new set<String>();
                string accid = null;
                for(integer i= 0; i < sObjectwoList.size(); i++){
                    if (sObjectwoList[i].get('AccountId') != null && sObjectwoList[i].get('AccountId')  != ''){
                        accid =  (String)sObjectwoList[i].get('AccountId');
                        accid = accid.trim();
                        buildingIdSet.add(accid);
                        //sObjectwoList[i].put('AccountId',null);
                    }
                    
                }
                // Also query the Sydney uni Customer to make the query more optimized. 
                // 
                Id siteRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site').getRecordTypeId();
                map<string,id> buildingAccountMap = new map<string,id>();
                for (Account accoBj : [SELECT ID, Customer_Reference__c FROM Account
                                                                        WHERE RecordTypeID =: siteRecordTypeId 
                                                                        AND Customer_Reference__c IN : buildingIdSet
                                                                        AND Service_Territory_Name__c = 'TNUS -Triple M University of Sydney HVAC' ]) {
                    buildingAccountMap.put(accobj.Customer_Reference__c,accobj.id) ;
                }
                
                for(integer i= 0; i < sObjectwoList.size(); i++){
                    if (sObjectwoList[i].get('AccountId') != null && sObjectwoList[i].get('AccountId')  != ''){
                        string buildingNumber =  (String)sObjectwoList[i].get('AccountId');
                        buildingNumber = buildingNumber.trim();
                        if (buildingNumber != null && buildingAccountMap.containsKey(buildingNumber)){
                            id sfAccountId = buildingAccountMap.get(buildingNumber);
                            sObjectwoList[i].put('AccountId',sfAccountId);
                        }else {
                            sObjectwoList[i].put('AccountId',buildingNumber);
                        }
                    // system.debug('Ben Lee Test ' + buildingNumber);
                    }
                    if (worktypeList[0].Id != null){
                        sObjectwoList[i].put('WorkTypeId',worktypeList[0].Id); 
                    }
                    
                    workorderList.add((workOrder)sObjectwoList[i]);
                }
                
            } else if (eventActionName == 'ProntoWorkorder'){
                set<string> workorderIdSet = new Set<string>();
                Map<string,String> workOrderUpdateMap = new Map<String,String>();
                for(integer i= 0; i < sObjectwoList.size(); i++){
                    string workOrderid =  (String)sObjectwoList[i].get('Client_Work_Order_Number__c');
                    //sObjectwoList[i].remove('Client_Work_Order_Number__c');
                    workorderIdSet.add(workOrderId);
                }
                List<WorkOrder> fetchWorkOrderList = [SELECT Id, Client_Work_Order_Number__c, WorkOrderNumber FROM WorkOrder WHERE WorkOrderNumber =: workorderIdSet ];
                for (integer i = 0; i <fetchWorkOrderList.size() ; i++){
                    workOrderUpdateMap.put(fetchWorkOrderList[i].WorkOrderNumber, fetchWorkOrderList[i].Client_Work_Order_Number__c);
                    
                }
                for(integer i= 0; i < sObjectwoList.size(); i++){
                    string workOrdernumber =  (String)sObjectwoList[i].get('Client_Work_Order_Number__c');
                    sObjectwoList[i].put('Client_Work_Order_Number__c',workOrderUpdateMap.get(workOrdernumber));
                    workorderList.add((workOrder)sObjectwoList[i]);
                }

            }
            // system.debug('!!!!Mahmood After workorderList' + workorderList);
           // Database.UpsertResult[] upsertResult = Database.upsert(workorderList, woRefId , false);
           // System.debug('DildarLog: upsertResult workoder syduni - ' + upsertResult);    
           // successWoIdSet = UtilityClass.readUpsertResults(upsertResult, JSON.serialize(workorderList), 'WorkOrder', woCorrelationId, eventActionName,woExternalIdValue);                
           System.enqueueJob(new B2BToBSAUpsertQueueable(woRefId, woCorrelationId, workorderList,eventActionName, woExternalIdValue,1));  

        }
    }
}