/**
 * @File Name          : LabourRateDTO.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 03/03/2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    31/07/2020         Tom Henson             Initial Version
 * 1.1    03/03/2021         Ben Lee                Added Operations field
**/

public class LabourRateDTO implements IDTO{
    
    public List<LabourRateDetails> labourRateDetails {get;set;} 
    public LabourRateDTO(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'labourdetails') {
                        LabourRateDetails = arrayOfLabourRateDetails(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'BaseDTO consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    private static List<LabourRateDetails> arrayOfLabourRateDetails(System.JSONParser p) {
        List<LabourRateDetails> res = new List<LabourRateDetails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new LabourRateDetails(p));
        }
        return res;
    }
    
    public class LabourRateDetails {
        public String csrkey {get;set;} 
        public String csrengineercategory {get;set;} // in json: type
        public String csrchargetype {get;set;} 
        public String csrrates1 {get;set;} 
        public String csrrates2 {get;set;} 
        public String csrrates3 {get;set;} 
        public String csrrates4 {get;set;} 
        public String ahnr {get;set;} 
        public String ahntb {get;set;} 
        public String ahmr {get;set;} 
        public String ahmtb {get;set;} 
        public String csrkeyterritory {get;set;} 
        public string csrtype {get;set;}
        public String recordid {get;set;} 
        public String operations {get;set;} //Ben Lee - added 20210303

        public LabourRateDetails(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'csr-key-account') {
                            csrkey = parser.getText();
                        } else if (text == 'csr-engineer-category') {
                            csrengineercategory = parser.getText();
                        } else if (text == 'csr-charge-type') {
                            csrchargetype = parser.getText();
                        } else if (text == 'csr-rates[1]') {
                            csrrates1 = parser.getText();
                        } else if (text == 'csr-rates[2]') {
                            csrrates2 = parser.getText();
                        } else if (text == 'csr-rates[3]') {
                            csrrates3 = parser.getText();
                        } else if (text == 'csr-rates[4]') {
                            csrrates4 = parser.getText();
                        } else if (text == 'ah-nr') {
                            ahnr = parser.getText();
                        } else if (text == 'ah-ntb') {
                            ahntb = parser.getText();
                        } else if (text == 'ah-mr') {
                            ahmr = parser.getText();
                        } else if (text == 'ah-mtb') {
                            ahmtb = parser.getText();
                        } else if (text == 'csr-key-territory') {
                            csrkeyterritory = parser.getText();
                        } else if (text == 'csr-type') {
                            csrtype = parser.getText();
                        } else if (text == 'record-id') {
                            recordid = parser.getText();
                        } else if (text == 'Operations') {        //Ben Lee - added 20210303
                            operations = parser.getText();        //Ben Lee - added 20210303
                        } else {
                            System.debug(LoggingLevel.WARN, 'LabourDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }   
    }
    
    public  LabourRateDTO() {
    }
    
    public  LabourRateDTO parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new LabourRateDTO(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
  
    public String getSpecificDTOClassName(){

        return 'LabourRateDTO';
    }
}