@isTest
public class HFCNbnToBsaAutomationTest {
    @testSetup static void setup() {
        BSA_TestDataFactory.createSeedDataForTesting(); 
    }
    
    static testMethod void testNBNToBSAPlatformEvent(){   
        Test.startTest();
        BSA_TestDataFactory.createAndPublishPlatformEvent(Action_From_NBN_To_BSA__e.sObjectType,true,1);
        BSA_TestDataFactory.createAndPublishPlatformEvent(Action_From_NBN_To_BSA__e.sObjectType,true,2);
        BSA_TestDataFactory.createAndPublishPlatformEvent(Action_From_NBN_To_BSA__e.sObjectType,true,3);
        BSA_TestDataFactory.createAndPublishPlatformEvent(Action_From_NBN_To_BSA__e.sObjectType,true,4);        
        BSA_TestDataFactory.createAndPublishPlatformEvent(Action_From_NBN_To_BSA__e.sObjectType,true,5);           
        Test.stopTest();
        
    }
}