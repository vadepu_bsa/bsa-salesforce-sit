@IsTest
public class LabourRateDTOTest {

       @isTest
    static void createLabourRate_DTOTest(){

         Map<string,Object> mSerializedData;
        String mSpecificDTOClassName;
        String jsonString = '';
        LabourRateDTO mBaseDTO = new LabourRateDTO ();

        Action_LabourRate_to_BSA__e event = new Action_LabourRate_to_BSA__e();
        event.Action__c='CreateLabourRate';
        event.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event.CorrelationId__c='9eb99c5f-fdeb-6edc-aa4c-a08d33434234325345234334';
        event.AccountCode__c ='56789';
        event.LabourRates__c='"labourdetails":[{"dummy":"56789","csr-key-account":"56789","csr-type":"ddd","Operations":"Delete","csr-engineer-category":"Engineer","csr-charge-type":"~","csr-rates[1]":"100","csr-rates[2]":"60","csr-rates[3]":"0","csr-rates[4]":"0","ah-nr":"130","ah-ntb":"60","ah-mr":"0","ah-mtb":"0","csr-key-territory":"prontoId","record-id":"ACC007_A_~"}]';

        jsonString  = Deserilaize_LabourRateTest(event );
        mSpecificDTOClassName ='LabourRateDTO';
        
        test.startTest();
        LabourRateDTO newdto = new LabourRateDTO();
        mBaseDTO = newdto.parse(jsonstring);
        mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
        Test.stopTest();
           
    }
    
  
        static String Deserilaize_LabourRateTest(Action_LabourRate_to_BSA__e event)
    {
        String jsonString = '';      
        
        if(String.isNotBlank(event.LabourRates__c)){

            jsonString = jsonString + event.LabourRates__c+',';
        }
        boolean singlecontact = false;
        if (jsonstring.contains('"labourdetails":{')){
        jsonstring =   jsonstring.replace('"labourdetails":{','"labourdetails":[{');
        singlecontact = true;
        }
        if (singlecontact){
            jsonstring = '{'+jsonString.removeEnd(',')+']}';
        }else {
            jsonstring = '{'+jsonString.removeEnd(',')+'}';
        }
        // System.debug('BuildJson '+ jsonstring);
        for (Integer i = 0; i < jsonstring.length(); i=i+300) {
            Integer iEffectiveEnd = (i+300 > (jsonstring.length()-1) ? jsonstring.length()-1 : i+300);
            System.debug(jsonstring.substring(i,iEffectiveEnd));
        }

        return jsonString;
    }
}