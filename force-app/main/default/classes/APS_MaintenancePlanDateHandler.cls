// Bohao Chen @IBM 2020-06-29 @FSL3-33, @FSL3-32  @FSL3-309
public with sharing class APS_MaintenancePlanDateHandler implements Queueable {

    Map<String, Set<String>> mpIdsByEgIds; // maintenance plan ids by equipment group id to update next suggested maintenance date

    public APS_MaintenancePlanDateHandler(Map<String, List<String>> mpIdsByEgIds) {
        // convert the list of maintenance plan ids to SET collection to eliminate potential duplicates
        this.mpIdsByEgIds = new Map<String, Set<String>>();
        for(String egId : mpIdsByEgIds.keySet()) {            
            if(!this.mpIdsByEgIds.containsKey(egId)) {
                this.mpIdsByEgIds.put(egId, new Set<String>());
            }
            this.mpIdsByEgIds.get(egId).addAll(mpIdsByEgIds.get(egId));
        }
    }

    public void execute(QueueableContext context) {

        System.debug('@APS_MaintenancePlanDateHandler mpIdsByEgIds: ' + mpIdsByEgIds);

        if(!mpIdsByEgIds.isEmpty()) {
            Set<String> equipmentGroupIds = new Set<String>();
            Set<String> maintenancePlanIds = new Set<String>();

            equipmentGroupIds.addAll(mpIdsByEgIds.keySet());

            for(String egId : mpIdsByEgIds.keySet()) {
                maintenancePlanIds.addAll(mpIdsByEgIds.get(egId));
            }

            System.debug('@equipmentGroupIds: ' + equipmentGroupIds);
            System.debug('@maintenancePlanIds: ' + maintenancePlanIds);

            // need to update the MaintenanceAsset's NextSuggestedMaintenanceDate of Equipment Gorups
            List<MaintenanceAsset> mas = new List<MaintenanceAsset>();
            for(MaintenanceAsset ma : [SELECT NextSuggestedMaintenanceDate, Maintenance_Routine__c, AssetId, MaintenancePlanId
                                        FROM MaintenanceAsset 
                                        WHERE AssetId IN:equipmentGroupIds 
                                        OR MaintenancePlanId IN:maintenancePlanIds]) {
                
                System.debug('@ma: ' + ma);
                if(mpIdsByEgIds.containsKey(ma.AssetId) && mpIdsByEgIds.get(ma.AssetId).contains(ma.MaintenancePlanId)) {
                    System.debug('@AssetId: ' + ma.AssetId);
                    System.debug('@MaintenancePlanId: ' + ma.MaintenancePlanId);
                //         String egIdAndMpId = ma.AssetId + '-' + ma.MaintenancePlanId;
                 

                // String egIdAndMpId = ma.AssetId + '-' + ma.MaintenancePlanId;
                // System.debug('@egIdAndMpId: ' + egIdAndMpId);

                // if(woliByEgIdAndMpId.containsKey(egIdAndMpId)) { ????
                //     System.debug('@ma: ' + ma);
                    Date newNextSuggestedMaintenanceDate;

                    switch on ma.Maintenance_Routine__c {
                        when 'Monthly' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addMonths(1);
                        }
                        when 'Bi-Monthly' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addMonths(2);
                        }
                        when 'Quarterly' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addMonths(3);
                        }
                        when 'Six-Monthly' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addMonths(6);
                        }
                        when 'Yearly' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addYears(1);
                        }
                        when 'Bi-Annual' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addYears(2);
                        }
                        when '5 Yearly' {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate.addYears(5);
                        }
                        when else {
                            newNextSuggestedMaintenanceDate = ma.NextSuggestedMaintenanceDate;
                        }
                    }
                    ma.NextSuggestedMaintenanceDate = newNextSuggestedMaintenanceDate;
                    mas.add(ma);
                }
            }
            System.debug('@mas: ' + mas);

            if(!mas.isEmpty()) {
                Database.SaveResult[] srList = Database.update(mas, true);

                // Iterate through each returned result
                Integer i = 0;
                String errorMessage = '';

                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        errorMessage += 'Error occurred when the system was trying to update the next schedule date for maintenance plan(s).\nError Record: ' + mas[i] + '\n' + 
                        'The following error(s) have occurred: \n';
                        // Operation failed, so get all errors
                        for(Database.Error err : sr.getErrors()) {                    
                            errorMessage += err.getStatusCode() + ': ' + err.getMessage();
                        }
                        errorMessage += '\n';

                        // TODO: may need to ask the user to delete the work order if the suggested maintenance date update fails.

                    }
                    i++;
                }
                if(string.isNotBlank(errorMessage)) {
                    APS_TaskCreationDispatcher.QuickEmail(errorMessage);
                }        
            }
        }
    }
}