/**
* @description       : 
* @author            : ChangeMeIn@UserSettingsUnder.SFDoc
* @group             : 
* @last modified on  : 05-28-2021
* @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   03-31-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class APS_TaskCreationDispatcher implements Queueable {
    
    @testVisible
    private static Boolean doChainJob = true;
    private String taskType;
    private List<WorkOrderLineItem> woliTasks;
    private List<SObject> triggerRecords;
    private List<SObject> originalTriggerRecords;
    private Set<Id> successAssetWoliIds; 
    private List<Id> woIdsToBeDeleted=new List<Id>();
    private List<WorkOrderLineItem> successWolis;
    private String message;
    Private Id draftWoId;
    
    public APS_TaskCreationDispatcher(String taskType, List<WorkOrderLineItem> woliTasks, List<SObject> triggerRecords, List<SObject> originalTriggerRecords,List<WorkOrderLineItem> successRecords,List<Id> failedRecords,String message) {
        this.taskType = taskType;
        this.woliTasks = woliTasks;
        this.triggerRecords = triggerRecords;
        this.originalTriggerRecords = originalTriggerRecords;
        this.successAssetWoliIds = new Set<Id>();
        // this.woIdsToBeDeleted=new Set<Id>();
        this.successWolis=successRecords;
        this.woIdsToBeDeleted=failedRecords;
        this.message=message;
    }
    
    public void execute(QueueableContext context) {
        
        // System.debug('taskType: ' + taskType);
        // System.debug('woliTasks: ' + woliTasks);
        // System.debug('triggerRecords: ' + triggerRecords);
        // TODO: need to add error handling to send email in case of error occuring
        try {
            if(!woliTasks.isEmpty()) {
                List<Id> workOrderIds = new List<Id>();
                Map<Id,WorkOrder> idVsWo=new Map<id,WorkOrder>();
                for(SObject rec : woliTasks) {
                    WorkOrderLineItem woli = (WorkOrderLineItem)rec;
                    workOrderIds.add(woli.WorkOrderId);
                }
                
                if(!workOrderIds.isEmpty()) {
                    List<WorkOrder> workOrders = [SELECT Id,WorkOrderNumber,Account.Name,Description FROM WorkOrder WHERE Id IN: workOrderIds FOR UPDATE]; // Locking Statements to lock work orders exclusively
                    // @FSL3-818
                    // remove work order Ids from the original work order list which has alraedy had site tasks generated following the site, customer, service terrritory hierarchy
                    for(WOrkOrder wo:workOrders){
                        idVsWo.put(wo.id,wo);
                    }
                    if(this.taskType == 'Site Task' || this.taskType == 'Customer Task') {
                        Map<Id, WorkOrder> workOrderById = new Map<Id, WorkOrder>();
                        for(SObject rec : triggerRecords) {
                            WorkOrder wo = (WorkOrder)rec;
                            workOrderById.put(wo.Id, wo);
                        }
                        // System.debug('@before removal workOrderById: ' + workOrderById);
                        
                        for(Id woId : workOrderIds) {
                            workOrderById.remove(woId);
                        }
                        // System.debug('@after removal workOrderById: ' + workOrderById);
                        triggerRecords = workOrderById.values();
                    }
                }
                
                Database.SaveResult[] srList = Database.insert(woliTasks, false);
                
                // Iterate through each returned result
                // String errorMessage = '';
                Set<Id> failedWorkOrderIds = new Set<Id>();
                
                Integer srListSize = srList.size();
                for(Integer i = 0; i < srListSize; i++) {
                    Database.SaveResult sr = srList[i];
                    if (!sr.isSuccess()) {
                        message += 'Error occurred when trying to create ' + taskType + '.\nError Record: ' + woliTasks[i] +'\n Work Order Number:'+idVsWo.get(woliTasks[i].workOrderId).WorkOrderNumber+'\n Site Name:'+idVsWo.get(woliTasks[i].workOrderId).Account.Name+'\n Asset Description: '+idVsWo.get(woliTasks[i].workOrderId).Description+'\nThe following error(s) have occurred: \n';
                        
                        // Operation failed, so get all errors
                        for(Database.Error err : sr.getErrors()) {                    
                            message += err.getStatusCode() + ': ' + err.getMessage();
                        }                        
                        failedWorkOrderIds.add(woliTasks[i].WorkOrderId);
                        woIdsToBeDeleted.add(woliTasks[i].WorkOrderId);
                    }
                    else {
                        WorkOrderLineItem successWoliTask = woliTasks[i];
                        successWolis.add(woliTasks[i]);
                        // check if success inserted woli task is asset task
                        if(successWoliTask.AssetId != null && String.isNotBlank(successWoliTask.Subject) && successWoliTask.ParentWorkOrderLineItemId != null) {
                            successAssetWoliIds.add(successWoliTask.ParentWorkOrderLineItemId);
                        }
                    }
                }
                
                // log error
                if(!failedWorkOrderIds.isEmpty()) {
                    // List<WorkOrder> toBeDeleteWorkOrders = [SELECT Draft_Work_Order__c FROM WorkOrder WHERE Id IN: failedWorkOrderIds];  
                    // if(!toBeDeleteWorkOrders.isEmpty()) {
                    // APS_WorkOrderGeneratorHelper.updateNumOfFailedWorkOrders(failedWorkOrderIds, errorMessage + '\n');
                    // delete toBeDeleteWorkOrders; // delete failed work orders
                    // Application_Log__c log = new Application_Log__c(Log_Level__c = 'Error', Message__c = errorMessage.length() > 32768 ? errorMessage.substring(0, 32767) : errorMessage);
                    // insert log;
                    // }
                }
            }  
            
            // the order for task generation is important. Firstly, site task, Secondly, customer task, then followed by territory task
            if(taskType == 'BSA Task') {
                List<WorkOrderLineItem> jsaTasksWolis = APS_TaskCreationHelper.generateJsaTasks((List<WorkOrder>)triggerRecords);
                if(doChainJob) {
                    System.enqueueJob(new APS_TaskCreationDispatcher('JSA Task', jsaTasksWolis, triggerRecords, originalTriggerRecords,successWolis,woIdsToBeDeleted,message));
                }
            }
            else if(taskType == 'JSA Task') {
                List<WorkOrderLineItem> siteTaskWolis = APS_TaskCreationHelper.generateSiteTasks((List<WorkOrder>)triggerRecords);
                if(doChainJob) {
                    System.enqueueJob(new APS_TaskCreationDispatcher('Site Task', siteTaskWolis, triggerRecords, originalTriggerRecords,successWolis,woIdsToBeDeleted,message));
                }
            }            
            // @FSL3-818
            else if (taskType == 'Site Task'){
                List<WorkOrderLineItem> customerTaskWolis =  APS_TaskCreationHelper.generateCustomerTasks((List<WorkOrder>)triggerRecords);
                if(doChainJob) {
                    System.enqueueJob(new APS_TaskCreationDispatcher('Customer Task', customerTaskWolis, triggerRecords, originalTriggerRecords,successWolis,woIdsToBeDeleted,message));
                }
            }
            else if (taskType == 'Customer Task'){
                // filter asset WOLIs before calling queueable job to generate BSA tasks
                List<WorkOrderLineItem> territoryTaskWolis = APS_TaskCreationHelper.generateTerritoryTask((List<WorkOrder>)triggerRecords);
                if(doChainJob) {
                    System.enqueueJob(new APS_TaskCreationDispatcher('Territory Task', territoryTaskWolis, triggerRecords, originalTriggerRecords,successWolis,woIdsToBeDeleted,message));
                }
            }
            else if (taskType == 'Territory Task' || taskType == 'Equipment Task') {
                
                // if task type is 'equipment task', all asset tasks should have been generated against its work order up to here.
                // mark 'asset tasks generated' flag to true on work order
                
                // if task type is 'Territory Task', all site tasks should have been generated against its work order 
                // as the territory task should be the last type of site task to be inserted up to here.
                // mark 'site tasks generated' flag to true on work order
                List<WorkOrder> workOrders;
                
                if(taskType == 'Territory Task') {
                    workOrders = [SELECT Site_Tasks_Generated__c, WorkOrderNumber, Draft_Work_Order__c FROM WorkOrder
                                  WHERE Site_Tasks_Generated__c = false 
                                  AND Id IN: originalTriggerRecords FOR UPDATE];
                    
                    for(WorkOrder WorkOrder : workOrders) {
                        ////Ananti created ---- Begin  FR-58 -- Rolling back
                        //If(APS_TaskCreationHelper.IsSiteTaskGenerationRequired(WorkOrder) == true)
                        //{                        
                        //If(APS_TaskCreationHelper.IsSiteTaskWOLICreated(WorkOrder) == true)
                        //{
                        if(!woIdsToBeDeleted.contains(WorkOrder.id)){
                            WorkOrder.Site_Tasks_Generated__c = true;
                        }
                        draftWoId=WorkOrder.Draft_Work_Order__c;
                        //}
                        //}    
                        //else
                        //{
                        ////Set flag NoSiteTaskRequired = true (create a new field in the work order object)
                        //WorkOrder.No_Site_Tasks_Generated_Required__c = true;
                        //}  
                    }
                }
                // TODO: potential bug which sets flag on work orders too early
                else if(taskType == 'Equipment Task') {
                    
                    // System.debug('@successAssetWoliIds: ' + successAssetWoliIds);
                    
                    Map<Id, Integer> numOfAssetWolisByWoId = new Map<Id, Integer>();
                    
                    for(WorkOrderLineItem woli : [SELECT WorkOrderId FROM WorkOrderLineItem WHERE Id IN: successAssetWoliIds]) {
                        if(!numOfAssetWolisByWoId.containsKey(woli.WorkOrderId)) {
                            numOfAssetWolisByWoId.put(woli.WorkOrderId, 0);
                        }
                        numOfAssetWolisByWoId.put(woli.WorkOrderId, numOfAssetWolisByWoId.get(woli.WorkOrderId) + 1);
                    }
                    
                    // System.debug('@numOfAssetWolisByWoId: ' + numOfAssetWolisByWoId);
                    
                    Set<Id> workOrderIds = new Set<Id>();
                    for(WorkOrderLineItem woli : (List<WorkOrderLineItem>)originalTriggerRecords) {
                        workOrderIds.add(woli.WorkOrderId);
                    }
                    workOrders = [SELECT WorkOrderNumber, Draft_Work_Order__c, Number_of_Remaining_Assets__c FROM WorkOrder
                                  WHERE Number_of_Remaining_Assets__c > 0
                                  AND Id IN: numOfAssetWolisByWoId.keySet() FOR UPDATE];
                    
                    for(WorkOrder workOrder : workOrders) {
                        workOrder.Number_of_Remaining_Assets__c -= numOfAssetWolisByWoId.get(workOrder.Id);
                    }
                }
                
                Database.SaveResult[] srList = Database.update(workOrders, false);
                
                List<WorkOrder> toBeDeleteWorkOrders = new List<WorkOrder>();
                
                // Iterate through each returned result
                // String errorMessage = '';
                String fieldName = taskType == 'Territory Task' ? 'Site_Tasks_Generated__c' : 'Number_of_Remaining_Assets__c';
                for(Integer i = 0; i < srList.size(); i++) {
                    Database.SaveResult sr = srList[i];
                    if (!sr.isSuccess()) {
                        toBeDeleteWorkOrders.add(workOrders[i]);
                        woIdsToBeDeleted.add(workOrders[i].id);
                        message += 'Error(s) occurred while trying to update the field \'' + fieldName + '\' on the work order ' + workOrders[i].WorkOrderNumber + ':';
                        // Operation failed, so get all errors
                        for(Database.Error err : sr.getErrors()) {                    
                            message += err.getStatusCode() + ': ' + err.getMessage();
                        }                      	  
                    }
                }
                if(!toBeDeleteWorkOrders.isEmpty()) {            
                    // APS_WorkOrderGeneratorHelper.updateNumOfFailedWorkOrders(toBeDeleteWorkOrders, errorMessage + '\n');
                }
                system.debug('Printing woIdsToBeDeleted'+woIdsToBeDeleted);
                system.debug('Printing doChainJob'+doChainJob);
                system.debug('Printing successWolis'+successWolis);
                if(!woIdsToBeDeleted.isEmpty() && doChainJob){
                    system.debug('Printing Debug 1');
                    system.enqueueJob(new APS_TaskDeletionDispatcher(new Set<id>(woIdsToBeDeleted),successWolis,message));
                }else{
                    APS_WorkOrderGeneratorHelper.updateDraftWorkOrder(successWolis,0,draftWoId);
                }
            } 
        } catch(QueryException e) {
            
            if(e.getMessage().contains('Record Currently Unavailable: ')) {
                // running a job 1 minute from now only once
                Datetime schDateTime = Datetime.now().addminutes(1);
                //parse to cron expression
                String nextFireTime = String.valueOf(schDateTime.second()) + ' ' + String.valueOf(schDateTime.minute()) + ' ' + String.valueOf(schDateTime.hour()) + ' * * ?';
                
                for(WorkOrderLineItem woli : woliTasks) {woli.Id = null;}
                
                APS_TaskCreationHelper.abortLongRunningScheduleJob();
                APS_TaskCreationScheduler s = new APS_TaskCreationScheduler(taskType, woliTasks, triggerRecords, originalTriggerRecords); 
                System.schedule('Job Started At ' + String.valueOf(Math.random() * 10000) + '-' + UserInfo.getUserId() + ' for APS_TaskCreationDispatcher', nextFireTime, s);
            }
            else {
                APS_WorkOrderGeneratorHelper.QuickEmail('Error on creating work order(s) and task(s)', e.getMessage() + '\n' + e.getStackTraceString() + ' Please contact helpdesk.',UserInfo.getUserEmail());
            }
        }
        catch(Exception e) {
            APS_WorkOrderGeneratorHelper.QuickEmail('Error on creating work order(s) and task(s)', e.getMessage() + '\n' + e.getStackTraceString() + ' Please contact helpdesk.', UserInfo.getUserEmail());
        }
    }
}