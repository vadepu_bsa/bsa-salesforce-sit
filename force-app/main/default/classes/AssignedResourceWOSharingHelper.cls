/**
 * @author          Sunila.M
 * @date            18/May/2019	
 * @description     Helper class to delete the Work Order share record for the Old assigned resource
 * 					
 *
 * Change Date    Modified by         Description  
 * 18/May/2019		Sunila.M		Created Helper class for the Assigned resource trigger
 **/
public class AssignedResourceWOSharingHelper {
    
     public static void deleteOldTechWOSharingRecord(Boolean isUpdate, Map<Id, SObject> newAssignedResourceMap, Map<Id, SObject> oldAssignedResourceMap){
         
         //--Holds the SA record Id to query ParentWorkOrder Id
         List<Id> serviceAppointmentIdList = new List<Id>();
         
         //--Map with key as SA Is and value as Assigned ResourceId
         Map<Id,Id> SAIdAssignedResourceIdMap  = new Map<Id,Id>();
         
         //--holds the WOshare to be deleted
         List<WorkOrderShare> workOrderShareList = new List<WorkOrderShare>();
         
         //--holds the Service Resource Ids to query the SR User Id
         List<Id> serviceResourceIdList = new List<Id>();
         
         //--holds the Service reource as Key and UserId as value
         Map<Id,Id> serviceResourceIdUserIdMap = new Map<Id,Id>();
         
         //Map<Id,Id> serviceResourceIdWOIdMap = new Map<Id,Id>();
         Map<Id,Id> WOIdserviceResourceIdMap = new Map<Id,Id>();
         
         //--Holds the WO id
         List<Id> WOIdList = new List<Id>();
         
         //--holds the userId of SR
         List<Id> userIdList = new List<Id>();
         
         // get the Old Service Resource Assigned and remove sharing
         for(Id eachAssignedResourceId : oldAssignedResourceMap.keySet() ){
             AssignedResource eachOldAssignedResource = (AssignedResource)oldAssignedResourceMap.get(eachAssignedResourceId);
                         
             //check Service Resource is updated
             if(isUpdate){
                 //--get the new Service resource
                 AssignedResource eachNewAssignedResource = (AssignedResource)newAssignedResourceMap.get(eachAssignedResourceId);
                 
                 if(eachOldAssignedResource.ServiceResourceId != eachNewAssignedResource.ServiceResourceId){
                     serviceAppointmentIdList.add(eachOldAssignedResource.ServiceAppointmentId);
                     SAIdAssignedResourceIdMap.put(eachOldAssignedResource.ServiceAppointmentId, eachOldAssignedResource.ServiceResourceId);
                     serviceResourceIdList.add(eachOldAssignedResource.ServiceResourceId);
                 }                 
             }
             //in case of delete remove sharing record
             else{
                 serviceAppointmentIdList.add(eachOldAssignedResource.ServiceAppointmentId);
                 SAIdAssignedResourceIdMap.put(eachOldAssignedResource.ServiceAppointmentId, eachOldAssignedResource.ServiceResourceId);
                 serviceResourceIdList.add(eachOldAssignedResource.ServiceResourceId);
             }
         }
         
         //--get the User Id of the Service Resource
         for(ServiceResource eachSR : [SELECT Id, User_Id__c FROM ServiceResource WHERE Id IN: serviceResourceIdList]){
             
             //--build Map with Service Resource Id and value as User Id
             serviceResourceIdUserIdMap.put(eachSR.Id, eachSR.User_Id__c);
             
             //--Add used Id to list used to quesy the WO share record
             userIdList.add(eachSR.User_Id__c);
         }
         
         //--get the SA.Parent Work Order Id
         for(ServiceAppointment eachServiceAppointmentRecord : [SELECT Id, ParentRecordId FROM serviceAppointment WHERE Id IN: serviceAppointmentIdList ]){
             
             //build map with key as WO Id and value as Service Resource Id
             if(SAIdAssignedResourceIdMap.containsKey(eachServiceAppointmentRecord.Id)){
                 Id eachServiceResourceId = SAIdAssignedResourceIdMap.get(eachServiceAppointmentRecord.Id);
                 if(serviceResourceIdUserIdMap.containsKey(eachServiceResourceId)){
                     //serviceResourceIdWOIdMap.put(serviceResourceIdUserIdMap.get(eachServiceResourceId), eachServiceAppointmentRecord.ParentRecordId);
                     WOIdserviceResourceIdMap.put(eachServiceAppointmentRecord.ParentRecordId, serviceResourceIdUserIdMap.get(eachServiceResourceId));
                     
                     //--add WO id to list which used to query the share record
                     WOIdList.add(eachServiceAppointmentRecord.ParentRecordId);
                 }                 
             }             
         }

         //--get the Share record and add to the List and delete
         for(WorkOrderShare eachWOshare : [SELECT Id,UserOrGroupId,ParentId FROM WorkOrderShare 
                                           WHERE ParentId IN: WOIdList AND UserOrGroupId IN:userIdList]){
             //Id resourceId = WOIdserviceResourceIdMap.get(eachWOshare.ParentId);
             //system.debug('resourceId'+resourceId);
             if(WOIdserviceResourceIdMap.containsKey(eachWOshare.ParentId) && WOIdserviceResourceIdMap.get(eachWOshare.ParentId) == eachWOshare.UserOrGroupId){
                 workOrderShareList.add(eachWOshare);
             }
         }
         //-delete the WO share record which is shared with Old Service Resource
         delete workOrderShareList;
     }
}