global class CreateMediaResourceBatch Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
	global String strQuery;
    public List<String> batchException = new List<String>();
    //public Id woId;
    Map<Id,Id> ContentDocToWOMapToUpload = new Map<Id,Id>();
   
    
    //Initialize the query string
    global CreateMediaResourceBatch(Map<Id,Id> ContentDocToWOMapToUpload){
        //this.woId =woId;
        this.ContentDocToWOMapToUpload = ContentDocToWOMapToUpload;
        
        String ContentDocs = '(';
        for(Id key: ContentDocToWOMapToUpload.keyset()){
            ContentDocs =ContentDocs+'\''+key+'\',';
        }
        ContentDocs = ContentDocs.substring(0,ContentDocs.length()-1);
        ContentDocs = ContentDocs+')';
        
        
        strQuery = 'SELECT Id,Media_Id__c,ContentDocumentId,WOMediaId__c,Description,VersionData,Title,FileType from ContentVersion where contentDocumentId in '+ContentDocs;
        System.debug('strQuery=='+strQuery);
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<ContentVersion> listCVs){
        //System.debug('Sayali Debug:listCVs'+listCVs[0]);
        //Set<Id> contentDocIds = new Set<Id>();
        for(ContentVersion cv : listCVs){
            //contentDocIds.add(cv.ContentDocumentId);
            System.debug('cv.ContentDocumentId =='+cv.ContentDocumentId);
            System.debug('ContentDocToWOMapToUpload=='+ContentDocToWOMapToUpload);
            System.debug('ContentDocToWOMapToUpload.get(cv.ContentDocumentId) =='+ContentDocToWOMapToUpload.get(cv.ContentDocumentId));
            NBNFileUploadDownloadHandler nbnHandler = New NBNFileUploadDownloadHandler(ContentDocToWOMapToUpload.get(cv.ContentDocumentId),'',cv,true);
            nbnHandler.process(Label.Create_Media_Resource);
        }
        
        
    }
     global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exceltion list        
        if(batchException.size() > 0){
            System.debug('Sayali Debug: Batch - BatchPricebookRenewalAutomation - Finish - Exceptions - ' + batchException);
        } 
     }
}