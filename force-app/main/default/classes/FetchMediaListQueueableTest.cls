@isTest
public class FetchMediaListQueueableTest {
    public TestMethod static void fetchmediaListtest(){
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseForMediaList());
        Account acc = NBNTestDataFactory.createTestAccounts();
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
        Set<Id> woIdSet = new Set<Id>();
        woIdSet.add(workorder.Id);
        ServiceAppointment sa = NBNTestDataFactory.createTestServiceAppointment(workorder.Id);
        test.startTest();
        sa.Status ='Scheduled';
        Update sa;
        
        Test.stopTest();
        
    }

}