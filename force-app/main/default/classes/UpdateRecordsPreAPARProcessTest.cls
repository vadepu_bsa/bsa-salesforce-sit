@isTest
public class UpdateRecordsPreAPARProcessTest {
    @testSetup static void setup() {        
        BSA_TestDataFactory.createSeedDataForTesting();                
        BSA_TestDataFactory.createTestServiceResource();
    }
    
    static testMethod void testPositiveScenarios(){   
        Test.startTest();
        Id parentWOId;
        Id childWOId;
        List<WorkOrder> parentWoList = new List<WorkOrder>();
        Set<String> lineItemIdSet = new Set<String>();
        List<Pricebook2> pbList = [SELECT ID FROM Pricebook2 limit 1];
        List<ServiceResource> serviceResList = [SELECT ID FROM ServiceResource limit 1]; 
        for(WorkOrder wo : [SELECT ID,ParentWorkOrderId FROM WorkOrder]){
            
            if(wo.ParentWorkOrderId == null && parentWOId == null) {
                
                parentWOId = wo.id;
                parentWoList.add(wo);
            }
            if(wo.ParentWorkOrderId != null && childWOId == null) {  
                childWOId = wo.id;
                
            }
        }                        
        System.debug('parentWOId'+parentWOId);
        System.debug('childWOId'+childWOId);
        List<Line_Item__c> lineitemList = BSA_TestDataFactory.createTestLineItems(childWOId,4,pbList[0].id,true);
        String ids = '';
        for(Line_Item__c lineitem :lineitemList ){
            ids = ids+';'+lineitem.id;            
        }
        System.debug('Line item ids'+ids);
        
        List<Payment__c> paymentList  = BSA_TestDataFactory.createTestPayments(childWOId,1,serviceResList[0].id,ids,true);
        List<Invoice__c> invoiceList = BSA_TestDataFactory.createTestInvoices(parentWOId,1,ids,true); 
        UpdateRecordsPreInvoiceAndPaymentProcess.updateLineItemsPaymentsAndInvoices(parentWoList);
        Test.stopTest();
        
    }
}