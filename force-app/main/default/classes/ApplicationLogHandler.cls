/* Author: Abhijeet Anand
 * Date: 09 September, 2019
 * Trigger Handler added for the ApplicationLogTrigger
 */
 public class ApplicationLogHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //After insert method on Application Log to publish Success/Failed Processing Event
    public void AfterInsert(Map<Id,SObject> newItems) { 
        List<Application_Log__c> applicationLogList = new List<Application_Log__c>();
        applicationLogList.addAll((List<Application_Log__c>)newItems.values());

        //Pass Application Log list to publish Success/Failed Processing Event
        if(!applicationLogList.isEmpty()) {
            ApplicationLogUtility.processApplicationLogs(applicationLogList);
        }
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {} public void BeforeInsert(List<SObject> newItems) {} public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}

}