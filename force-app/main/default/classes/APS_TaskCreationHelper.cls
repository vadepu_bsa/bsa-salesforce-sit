/*
*  @description apex helper to create sub-work order line items as tasks for site, equipment group or APS
* this is for JIRA @FSL3-11
*
*  @author      Bohao Chen
*  @date        2020-06-29
*/
public without sharing class APS_TaskCreationHelper {
    
    public static Map<String, Schema.RecordTypeInfo> woliRecordTypeMap {
        get {
            if(woliRecordTypeMap == null) {
                woliRecordTypeMap = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByDeveloperName();
            }
            return woliRecordTypeMap;
        }
        set;
    } 

    public static List<WorkOrderLineItem> generateSiteTasks (List<WorkOrder> lstWorkOrders) {        
        //TODO ADD Filter

        Map<Id, List<WorkOrder>> mapWOs = new Map<Id,  List<WorkOrder>>();
        Map<Id, List<WorkOrderLineItem>> mapWOLI = new Map<Id, List<WorkOrderLineItem>>();
        List<WorkOrderLineItem> output = new List<WorkOrderLineItem>();

        Map<String, List<Work_Order_Automation_Rule__c>> woarBySiteId = filterWorkOrderAutomationRules('site', null);
        for (WorkOrder wo : lstWorkOrders){
            if (woarBySiteId.containsKey(wo.AccountId)){//since the filter doesn't work on AUtomation rules, filter here
                if (!mapWOs.containsKey(wo.AccountId)){
                    mapWOs.put(wo.AccountId, new List<WorkOrder>());
                }
                mapWOs.get(wo.AccountId).add(wo);
            }
        }

        // System.debug('@woarBySiteId: ' + woarBySiteId);

        for(Id woarKey : woarBySiteId.keySet()){
            if(woarBySiteId.containsKey(woarKey)) {
                for (Work_Order_Automation_Rule__c woar : woarBySiteId.get(woarKey)){
                    if ((woar.Work_Action_Type__c == 'Pre-Work Action' || woar.Work_Action_Type__c == 'Post-Work Action') 
                        && mapWOs.containsKey(woar.Account__c)) {
                        for(WorkOrder wo : mapWOs.get(woar.Account__c)){
                            if(isWorkOrderAutomationRuleSelectable(wo, woar)) {
                                WorkOrderLineItem woli = createTaskWorkOrderLineItem(wo, woar, 'site');
                                if (!mapWOLI.containsKey(woar.Account__c)){
                                    mapWOLI.put(woar.Account__c, new List<WorkOrderLineItem>());
                                }
                                mapWOLI.get(woar.Account__c).add(woli);
                                output.add(woli);
                            }
                        }
                    }
                }
            }
        }

        // System.debug('@sorted wolis: ' + output);
        return output;
    }

    //create tasks for work orders based on the service territory of work orders
    public static List<WorkOrderLineItem> generateTerritoryTask(List<WorkOrder> workOrders){

        List<WorkOrderLineItem> serviceTerritoryTaskWolis = new List<WorkOrderLineItem>();
        
        if(!workOrders.isEmpty()) {
            Map<String, List<Work_Order_Automation_Rule__c>> woarByTerritoryId = filterWorkOrderAutomationRules('territory', null);
        
            for(WorkOrder wo : workOrders) {
                if(wo.ServiceTerritoryId != null && woarByTerritoryId.containsKey(wo.ServiceTerritoryId)) {
                    for(Work_Order_Automation_Rule__c woar : woarByTerritoryId.get(wo.ServiceTerritoryId)) {
                        if(isWorkOrderAutomationRuleSelectable(wo, woar)) {
                            serviceTerritoryTaskWolis.add(createTaskWorkOrderLineItem(wo, woar, 'site'));
                        }
                    }
                }
            }
        }

        // System.debug('@serviceTerritoryTaskWolis: ' + serviceTerritoryTaskWolis);

        return serviceTerritoryTaskWolis;
    }


    //create tasks for work orders linked to all sites of the customer
    public static List<WorkOrderLineItem> generateCustomerTasks(List<WorkOrder> lstWorkOrders){
        List<WorkOrderLineItem> customerTaskWolis = new List<WorkOrderLineItem>();

        if(!lstWorkOrders.isEmpty()) {
            Map<String, List<Work_Order_Automation_Rule__c>> woarBySiteId = filterWorkOrderAutomationRules('customer', null);

            // query to get customer id on existing work orders
            for(WorkOrder wo : lstWorkOrders) {
                if(wo.Account != null && wo.Account.ParentId != null && woarBySiteId.containsKey(wo.Account.ParentId)) {
                    for(Work_Order_Automation_Rule__c woar : woarBySiteId.get(wo.Account.ParentId)) {
                        if(isWorkOrderAutomationRuleSelectable(wo, woar)) {
                            customerTaskWolis.add(createTaskWorkOrderLineItem(wo, woar, 'site'));
                        }
                    }
                }
            }
        }
        // System.debug('@customerTaskWolis: ' + customerTaskWolis);

        return customerTaskWolis;
    }

    private class EquipmentGroupWrapper {
        String equipmentTypeId;
        String equipmentGroupId;
        String maintenanceRoutine;
        String standard;

        equipmentGroupWrapper(String equipmentTypeId, String equipmentGroupId, String maintenanceRoutine, String standard) {
            this.equipmentTypeId = equipmentTypeId;
            this.equipmentGroupId = equipmentGroupId;
            this.maintenanceRoutine = maintenanceRoutine;
            this.standard = standard;
        }
    }

    // this method is used to generate equipment tasks for equipment groups with assets.
    // For each child asset of equipment groups, one asset work order line item will be generated. 
    // therefore it needs to find out equipment groups from the field Maintenance_Asset_Ids__c on work order in order
    // to create equipment tasks for those equipment groups
    public static List<WorkOrderLineItem> generateEquipmentTasks(List<WorkOrderLineItem> workOrderLineItems){

        List<WorkOrderLineItem> eqtaskWolis = new List<WorkOrderLineItem>();

        // filter asset wolis only
        List<WorkOrderLineItem> assetWolis = new List<WorkOrderLineItem>();
		
        for(WorkOrderLineItem woli : workOrderLineItems) {
            system.debug('**woli.AssetId'+woli.AssetId);
            system.debug('**woli.Subject'+woli.Subject);
            system.debug('***woli.RecordTypeId'+woli.RecordTypeId);
            system.debug('***RecordTypeId'+woliRecordTypeMap.get('Asset_Action').getRecordTypeId());
            if(woli.AssetId != null && woli.Subject == null && woli.RecordTypeId == woliRecordTypeMap.get('Asset_Action').getRecordTypeId()) {
                assetWolis.add(woli);
            }
        }

        // System.debug('@assetWolis: ' + assetWolis);

        if(!assetWolis.isEmpty()) {
            // query additional related details based on asset wolis such as site id, customer id, equipment id and the selected standard of equipment group
            assetWolis = [SELECT Asset.Parent.Equipment_Type__c, // equipment id
                                Asset.Parent.Select_Standard__c, // equipment group standard
                                AssetId, Maintenance_Routine__c, WorkOrderId,
                                WorkOrder.AccountId, // site id
                                WorkOrder.Account.ParentId, // customer id
                                WorkOrder.ServiceTerritoryId // service territory id
                        FROM WorkOrderLineItem 
                        WHERE Id IN: assetWolis
                        AND (Asset.RecordType.DeveloperName = 'Asset_Fire' 
                            OR Asset.RecordType.DeveloperName = 'Asset_HVAC'
                            OR Asset.RecordType.DeveloperName = 'Asset')
                        AND Asset.ParentId != NULL 
                        AND Asset.Parent.Equipment_Type__c != NULL
                        AND (WorkOrder.WorkType.Name LIKE '%Maintenance%' OR WorkOrder.WorkType.Name LIKE '%Maintenance')];// Bohao Chen @IBM 2020-09-16 @FSL3-580

             System.debug('@assetWolis after requery: ' + assetWolis);

            // fetch all equipment Work Order Automation Rules
            // TODO: potential improvement is to pass set of equipment type id into filter method to filter out unnecessary WOARs further
            Map<String, List<Work_Order_Automation_Rule__c>> woarsByEquipmentId = filterWorkOrderAutomationRules('equipment', null);
			System.debug('**woarsByEquipmentId**'+woarsByEquipmentId);
            // looping through asset wolis and find appropriate work order automation rules
            // There are 3 types of equipment woars: equipment only woar, equipment & customer woar and equipment & site woar
            // Below is the woar hierarchy
            // equipment & site woar > equipment & customer woar > equipment only woar
            for(WorkOrderLineItem assetWoli : assetWolis) {

                // System.debug('@assetWoli: ' + assetWoli);
                String equipmentTypeId = assetWoli.Asset.Parent.Equipment_Type__c;
                String siteId = assetWoli.WorkOrder.AccountId;
                String customerId = assetWoli.WorkOrder.Account.ParentId;
                String serviceTerritoryId = assetWoli.WorkOrder.ServiceTerritoryId;
                String maintenanceRoutine = assetWoli.Maintenance_Routine__c;
                String standard = assetWoli.Asset.Parent.Select_Standard__c;

                List<Work_Order_Automation_Rule__c> finalWoars = filterEquipmentWorkOrderAutomationRules(woarsByEquipmentId, equipmentTypeId, siteId, customerId, serviceTerritoryId, maintenanceRoutine, standard);
				System.debug('**finalWoars**'+finalWoars);
                if(!finalWoars.isEmpty()) {
                    for(Work_Order_Automation_Rule__c woar : finalWoars) {
                        eqtaskWolis.add(createTaskWorkOrderLineItem(new WorkOrder(Id = assetWoli.WorkOrderId), assetWoli, woar, 'equipment'));
                    }                    
                }
            }
        }
        System.debug('*****eqtaskWolis***'+eqtaskWolis); 
        return eqtaskWolis;
    }

    public static List<WorkOrderLineItem> generateBsaTasks(List<WorkOrder> WorkOrders) {

        Map<String, List<Work_Order_Automation_Rule__c>> woarsForBsa = filterWorkOrderAutomationRules('BSA', null);
                
        List<WorkOrderLineItem> wolis = new List<WorkOrderLineItem>();
                
        for(WorkOrder wo : WorkOrders) {
            if(woarsForBsa.containsKey('BSA')) {
                for(Work_Order_Automation_Rule__c woar : woarsForBsa.get('BSA')) {
                    if(isWorkOrderAutomationRuleSelectable(wo, woar)) {
                        WorkOrderLineItem woli = createTaskWorkOrderLineItem(wo, woar, 'bsa');            
                        wolis.add(woli);
                    }
                }
            }
        }
        
        // System.debug('@wolis: ' + wolis);
        return wolis;
    }

    // TODO: remove it when it becomes deprecated
    public static List<WorkOrderLineItem> generateJsaTasks(List<WorkOrder> WorkOrders) {

        Map<String, List<Work_Order_Automation_Rule__c>> woarsForJSA = filterWorkOrderAutomationRules('JSA', null);
        
        List<Work_Order_Automation_Rule__c> woars = woarsForJSA.get('JSA');
        
        // System.debug('@woars: ' + woars);

        List<WorkOrderLineItem> wolis = new List<WorkOrderLineItem>();
        Map<String, List<WorkOrderLineItem>> wolisByWoId = new Map<String, List<WorkOrderLineItem>>();

        Map<Id, WorkOrder> workOrderById = new Map<Id, WorkOrder>();

        for(WorkOrder wo : WorkOrders) {
            workOrderById.put(wo.Id, wo);
        }

        // System.debug('@workOrderById 1: ' + workOrderById);

        // @FSL3-818
        // generate site JSAs first
        for(WorkOrder wo : workOrderById.values()) {
            for(Work_Order_Automation_Rule__c woar : woars) {
                if(woar.Type__c.equalsIgnoreCase(wo.JSA_Type__c) && (woar.Account__c != null && woar.Account__c == wo.AccountId)
                    && isWorkOrderAutomationRuleSelectable(wo, woar)) {
                    WorkOrderLineItem woli = createTaskWorkOrderLineItem(wo, woar, 'jsa');            
                    wolis.add(woli);
                    workOrderById.remove(wo.Id); // remove work orders which have customer tasks
                }
            }
        }

        // System.debug('@workOrderById 2: ' + workOrderById);

        // for work orders which don't have site JSAs, generate customer JSAs
        for(WorkOrder wo : workOrderById.values()) {
            for(Work_Order_Automation_Rule__c woar : woars) {
                if(woar.Type__c.equalsIgnoreCase(wo.JSA_Type__c) && (woar.Account__c != null && woar.Account__c == wo.Account.ParentId)
                    && isWorkOrderAutomationRuleSelectable(wo, woar)) {
                    WorkOrderLineItem woli = createTaskWorkOrderLineItem(wo, woar, 'jsa');            
                    wolis.add(woli);
                    workOrderById.remove(wo.Id); // remove work orders which have customer tasks
                }
            }
        }

        // System.debug('@workOrderById 3: ' + workOrderById);

        // for work orders which don't have site and customer JSAs, generate service territory JSAs
        for(WorkOrder wo : workOrderById.values()) {
            for(Work_Order_Automation_Rule__c woar : woars) {
                if(woar.Type__c.equalsIgnoreCase(wo.JSA_Type__c) && (woar.Service_Territory__c != null &&  woar.Service_Territory__c == wo.ServiceTerritoryId)
                    && isWorkOrderAutomationRuleSelectable(wo, woar)) {
                    WorkOrderLineItem woli = createTaskWorkOrderLineItem(wo, woar, 'jsa');            
                    wolis.add(woli);                    
                }
            }
        }
        
        // System.debug('@wolis: ' + wolis);
        return wolis;
    }

    private static List<Work_Order_Automation_Rule__c> filterEquipmentWorkOrderAutomationRules(Map<String, List<Work_Order_Automation_Rule__c>> woarsByEquipmentId, 
                                                                                            String equipmentTypeId, String siteId, String customerId, String serviceTerritoryId, String maintenanceRoutine, String standard) {

        List<Work_Order_Automation_Rule__c> finalWoars = new List<Work_Order_Automation_Rule__c>();

        if(woarsByEquipmentId.containsKey(equipmentTypeId)) {
            
            List<Work_Order_Automation_Rule__c> eqAndSiteWoars = new List<Work_Order_Automation_Rule__c>();
            List<Work_Order_Automation_Rule__c> eqAndCustomerWoars = new List<Work_Order_Automation_Rule__c>();
            List<Work_Order_Automation_Rule__c> eqAndServiceTerritoryWoars = new List<Work_Order_Automation_Rule__c>();
            List<Work_Order_Automation_Rule__c> eqWoars = new List<Work_Order_Automation_Rule__c>();
            
            for(Work_Order_Automation_Rule__c woar : woarsByEquipmentId.get(equipmentTypeId)) {
                // 1. check if the maintenance routine is matched between a work order automation rule and the maintenance routine on equipment group
                // 2. check if the standard or non-standard (type == 'Asset') is matched between a work order automation rule and the Select_Standard__c field on equipment group
                if(matchMaintenanceRoutines(maintenanceRoutine, woar.Maintenance_Routine_Global__c, 'equipment') && woar.Type__c == standard) {
                    system.debug('Matched**');
                    if(woar.Account__c == siteId) {
                        eqAndSiteWoars.add(woar);
                    }
                    else if(woar.Account__c == customerId) {
                        eqAndCustomerWoars.add(woar);
                    }
                    else if(woar.Service_Territory__c == serviceTerritoryId) {
                        eqAndServiceTerritoryWoars.add(woar);
                    }
                    else if(woar.Account__c == null && woar.Service_Territory__c == null) {
                        eqWoars.add(woar);
                    }
                }
            }

            // Based on the woar hierarchy
            // equipment & site woar > equipment & customer/client woar > equipment & service territory woar > equipment only woar
            // use equipment & site woar if they exist
            // use equipment & customer woar if they exist and equipment & site woar doesn't exist
            // use equipment woar if they exist and neither equipment & customer woar nor equipment & site woar exist
            if(!eqAndSiteWoars.isEmpty()) {
                System.debug('@eqAndSiteWoars: ' + eqAndSiteWoars);
                finalWoars = eqAndSiteWoars;
            }
            else if(!eqAndCustomerWoars.isEmpty()) {
                System.debug('@eqAndCustomerWoars: ' + eqAndCustomerWoars);
                finalWoars = eqAndCustomerWoars;
            }
            else if(!eqAndServiceTerritoryWoars.isEmpty()) {
                System.debug('@eqAndServiceTerritoryWoars: ' + eqAndServiceTerritoryWoars);
                finalWoars = eqAndServiceTerritoryWoars;
            }
            else if(!eqWoars.isEmpty()) {
                System.debug('@eqWoars: ' + eqWoars);
                finalWoars = eqWoars;
            }
        }

        System.debug('@finalWoars: ' + finalWoars);

        return finalWoars;
    }

    // @FSL3-75
    private static Set<String> convertToMaintenanceRoutinesSet(String maintenanceRoutine) {
        Set<String> maintenanceRoutines = new Set<String>();
        if(!maintenanceRoutine.contains(';')) {
            maintenanceRoutines.add(maintenanceRoutine);
        }
        else {
            maintenanceRoutines.addAll(maintenanceRoutine.split(';'));
        }
        // System.debug('@maintenanceRoutines: ' + maintenanceRoutines);
        return maintenanceRoutines;
    }

    // @FSL3-75
    // private static Boolean matchMaintenanceRoutines(WorkOrder wo, WorkOrderLineItem parentWoli, Work_Order_Automation_Rule__c woar, String actionType) {
    public static Boolean matchMaintenanceRoutines(String maintenanceRoutine, String woarMaintenanceRoutines, String actionType) {
        // @FSL3-229
        // need to match maintenance routine on wo automation rule with maintenance plan on wo
        // @FSL3-75
        Boolean isMatch = false;

        // @FSL3-357
        // for non equipment task, don't need to match exact maintenance routine. There are only PM Work Orders (have maintenance routine) or Reactive Work Order (without maintenance routine)
        // for equipment task, need to match maintenance routine on asset WOLIs with the maintainence routine(s) on WOARs
        if((actionType != 'equipment' && String.isNotBlank(maintenanceRoutine) && String.isNotBlank(woarMaintenanceRoutines)) || 
            (actionType == 'equipment' && String.isNotBlank(maintenanceRoutine) && String.isNotBlank(woarMaintenanceRoutines) && convertToMaintenanceRoutinesSet(woarMaintenanceRoutines).contains(maintenanceRoutine)) ||
            (String.isBlank(maintenanceRoutine) && String.isBlank(woarMaintenanceRoutines))) {
            isMatch = true;
        }

        return isMatch;
    }

    // this method will check if standard or non-standard ('Asset' type) is matched between work order automation rule and the Select_Standard__c field on equipment group from asset work order line item
    private static Boolean matchStandard(Work_Order_Automation_Rule__c woar, WorkOrderLineItem assetWoli) {
        return woar.Type__c == assetWoli.Asset.Parent.Select_Standard__c;
    }

    public static void cloneBsaAndSiteTasks(List<ServiceAppointment> serviceAppointments) {
        List<WorkOrderLineItem> clonedWolis = new List<WorkOrderLineItem>();

        // assume the user clones one service appointment for one work order at one time.
        Map<String, ServiceAppointment> serviceAppointmentByWorkOrderId = new Map<String, ServiceAppointment>();

        String prefix = ServiceAppointment.sobjecttype.getDescribe().getKeyPrefix();
        
        for(ServiceAppointment sa : serviceAppointments) {
            if(sa.ParentRecordId != null) {
                String parentRecordIdPrefix = String.valueOf(sa.ParentRecordId).substring(0,3);
                String objectName = APS_SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(parentRecordIdPrefix);

                if(objectName == 'WorkOrder') {
                    serviceAppointmentByWorkOrderId.put(sa.ParentRecordId, sa);
                }             
            }
        }

        // System.debug('@serviceAppointmentByWorkOrderId: ' + serviceAppointmentByWorkOrderId);

        if(!serviceAppointmentByWorkOrderId.isEmpty()) {
            Map<Id, WorkOrder> workOrderById = new Map<Id, WorkOrder>(
                [SELECT 
                    (SELECT Id FROM ServiceAppointments), 
                    (SELECT RecordType.DeveloperName, AssetId, Accessed_working_load_limits__c, Apply_good_manual_handling_techniques__c, Apprentice_performing_Electrical_work__c,
                            Subject, Status, StatusCategory, Defect_Details__c, Sort_Order__c, Task_Completion_Type__c, Level__c
                    FROM WorkOrderLineItems 
                    WHERE (RecordType.DeveloperName = 'Site_Action' 
                    OR RecordType.DeveloperName = 'JSA_FIRE' 
                    OR RecordType.DeveloperName = 'JSA_HVAC')
                    AND Is_Original_Task__c = true
                    )
                FROM WorkOrder 
                WHERE Id IN: serviceAppointmentByWorkOrderId.keySet()]
            );

            for(String woId : serviceAppointmentByWorkOrderId.keySet()) {
                WorkOrder wo = workOrderById.get(woId);
                // check how many SA it has. 
                // if current work order doesn't have more than 1 SA, it means it is a the cloned one
                Integer numOfServiceAppointments = wo.ServiceAppointments.size();
                // System.debug('@numOfServiceAppointments: ' + numOfServiceAppointments);
                if(numOfServiceAppointments > 1) {
                    for(WorkOrderLineItem woli : wo.WorkOrderLineItems) {
                        WorkOrderLineItem clonedWoli = woli.clone(false, true, false, false);
                        clonedWoli.Status = 'Open';
                        clonedWoli.Service_Appointment__c = serviceAppointmentByWorkOrderId.get(woId).Id; //@FSL3-469
                        clonedWolis.add(clonedWoli);
                    }
                }
                // else {
                //     for(WorkOrderLineItem woli : wo.WorkOrderLineItems) {
                //         woli.Service_Appointment__c = serviceAppointmentsByWorkOrderId.
                //     }
                // }
            }
        }

        // System.debug('@clonedWolis: ' + clonedWolis);
        if(!clonedWolis.isEmpty()) {
            insert clonedWolis;
        }
    }

    // @FSL3-413
    public static void assignTaskResource(List<ServiceAppointment> serviceAppointments, Map<Id, ServiceAppointment> oldSA) {

        List<WorkOrderLineItem> assignedWolis = new List<WorkOrderLineItem>();

        Set<Id> scheduledServiceAppointmentIds = new Set<Id>();

        for(ServiceAppointment sa : serviceAppointments) {
            // System.debug('@status changed from ' + oldSA.get(sa.Id).Status + ' to ' + sa.Status);
            if(sa.Status == 'Scheduled' && oldSA.get(sa.Id).Status != 'Scheduled') {
                scheduledServiceAppointmentIds.add(sa.Id);
            }
        }

        Map<String, List<ServiceAppointment>> serviceAppointmentsByWorkOrderId = new Map<String, List<ServiceAppointment>>();

        String prefix = ServiceAppointment.sobjecttype.getDescribe().getKeyPrefix();
                
        for(ServiceAppointment sa : [SELECT ParentRecordId, 
                                        (SELECT ServiceResourceId FROM ServiceResources),
                                        (SELECT RecordType.DeveloperName, AssetId, Accessed_working_load_limits__c, Apply_good_manual_handling_techniques__c, 
                                                Apprentice_performing_Electrical_work__c, Subject, Status, StatusCategory, Defect_Details__c, Service_Resource__c, 
                                                Service_Appointment__c 
                                        FROM Work_Order_Line_Items__r
                                        WHERE (RecordType.DeveloperName = 'Site_Action' 
                                        OR RecordType.DeveloperName = 'JSA_FIRE' 
                                        OR RecordType.DeveloperName = 'JSA_HVAC')) 
                                    FROM ServiceAppointment 
                                    WHERE Id IN: scheduledServiceAppointmentIds]) {
            if(sa.ParentRecordId != null) {
                String parentRecordIdPrefix = String.valueOf(sa.ParentRecordId).substring(0,3);
                String objectName = APS_SchemaGlobalDescribe.findObjectNameFromRecordIdPrefix(parentRecordIdPrefix);

                if(objectName == 'WorkOrder') {
                    // loop through all site tasks or jsa tasks and assign the resource to tasks
                    for(WorkOrderLineItem woli : sa.Work_Order_Line_Items__r) {
                        for(AssignedResource ar : sa.ServiceResources) {
                            woli.Service_Resource__c = ar.ServiceResourceId;
                            break;
                        }
                        assignedWolis.add(woli);
                    }
                }             
            }
        }

        // System.debug('@assignedWolis: ' + assignedWolis);
        if(!assignedWolis.isEmpty()) {
            update assignedWolis;
        }
    }


    private static Map<String, List<Work_Order_Automation_Rule__c>> filterWorkOrderAutomationRules(String type, Set<Id> ids) {
        Map<String, List<Work_Order_Automation_Rule__c>> woarsById = new Map<String, List<Work_Order_Automation_Rule__c>>();
        Map<String, List<Work_Order_Automation_Rule__c>> output = new Map<String, List<Work_Order_Automation_Rule__c>>();

        String  woarQuery = 'SELECT Action__c, Type__c, Maintenance_Routine_Global__c, Account__c, Primary_Access_Technology__c, Equipment_Type__c, Equipment_Type__r.Name, Readings__c, Prompt_Number__c, Sort_Order__c, Work_Action_Type__c, ';
        woarQuery += 'Service_Territory__c, Account__r.RecordType.DeveloperName, Account__r.Type, Level__c, Work_Type__c ';
        woarQuery += 'FROM Work_Order_Automation_Rule__c ';
        woarQuery += 'WHERE RecordType.DeveloperName = \'APS_WOLI_Automation\' ';
        woarQuery += 'AND Active__c = true ';

        if(type == 'site' || type == 'customer') {
            woarQuery += 'AND Type__c = \'Site\' AND Equipment_Type__c = NULL AND Account__c != NULL AND Service_Territory__c = NULL ';
        } 
        else if(type == 'territory') {
            woarQuery += 'AND Type__c = \'Site\' AND Equipment_Type__c = NULL AND Account__c = NULL AND Service_Territory__c != NULL ';
        }
        else if(type == 'BSA') {
            woarQuery += 'AND Type__c = \'BSA\' AND Equipment_Type__c = NULL ';            
        }
        else if(type == 'JSA') {
            woarQuery += 'AND Type__c LIKE \'JSA -%\' AND Equipment_Type__c = NULL ';

        }
        else if(type == 'equipment') {
            woarQuery += 'AND Equipment_Type__c != NULL ';
        }

        woarQuery += 'ORDER BY Sort_Order__c ASC';

        // System.debug('@woarQuery: ' + woarQuery);

        List<Work_Order_Automation_Rule__c> woars = (List<Work_Order_Automation_Rule__c>)Database.query(woarQuery);

        if(type == 'site' || type == 'customer' || type == 'territory') {
            // group by site id
            for(Work_Order_Automation_Rule__c woar : woars) {
                if((type == 'site' || type == 'customer') && woar.Account__c != null) {
                    if(!woarsById.containsKey(woar.Account__c)) {
                        woarsById.put(woar.Account__c, new List<Work_Order_Automation_Rule__c>());
                    }
                    woarsById.get(woar.Account__c).add(woar);
                }
                else if(type == 'territory' && woar.Service_Territory__c != null) {
                    if(!woarsById.containsKey(woar.Service_Territory__c)) {
                        woarsById.put(woar.Service_Territory__c, new List<Work_Order_Automation_Rule__c>());
                    }
                    woarsById.get(woar.Service_Territory__c).add(woar);
                }
            }
        }
        else if(type == 'BSA') {
            // group by empty key
            woarsById.put('BSA', new List<Work_Order_Automation_Rule__c>());

            for(Work_Order_Automation_Rule__c woar : woars) {
                woarsById.get('BSA').add(woar);
            }
        }
        else if(type == 'JSA') {
            // group by empty key
            woarsById.put('JSA', new List<Work_Order_Automation_Rule__c>());

            for(Work_Order_Automation_Rule__c woar : woars) {
                woarsById.get('JSA').add(woar);
            }
        }
        else if(type == 'equipment') {
            // group by equipment type id
            for(Work_Order_Automation_Rule__c woar : woars) {
                if(!woarsById.containsKey(woar.Equipment_Type__c)) {
                    woarsById.put(woar.Equipment_Type__c, new List<Work_Order_Automation_Rule__c>());
                }
                woarsById.get(woar.Equipment_Type__c).add(woar);
            }
        }
        return woarsById;
    }

    private static Boolean isWorkOrderAutomationRuleSelectable(WorkOrder wo, Work_Order_Automation_Rule__c woar) {

        Boolean isWorkOrderAutomationRuleSelectable = false;

        // firstly, if WOAR 'Work_Type__c' is not blank, it needs to match the 'WorkTypeId' field from the work order.
        // secondly, need to check maintenance routines between WOARs and the Work Order
        isWorkOrderAutomationRuleSelectable = 
            ((woar.Work_Type__c != null && woar.Work_Type__c == wo.WorkTypeId) || woar.Work_Type__c == null) &&
            matchMaintenanceRoutines(wo.Maintenance_Plan__c, woar.Maintenance_Routine_Global__c, 'site');

        return isWorkOrderAutomationRuleSelectable;
    }

    private static WorkOrderLineItem createTaskWorkOrderLineItem(WorkOrder wo, Work_Order_Automation_Rule__c woar, String actionType, String equipmentGroupId) {
        return createTaskWorkOrderLineItem(wo, null, woar, actionType, equipmentGroupId);
    }

    private static WorkOrderLineItem createTaskWorkOrderLineItem(WorkOrder wo, Work_Order_Automation_Rule__c woar, String actionType) {
        return createTaskWorkOrderLineItem(wo, null, woar, actionType, null);
    }

    public static WorkOrderLineItem createTaskWorkOrderLineItem(WorkOrder wo, WorkOrderLineItem parentWoli, Work_Order_Automation_Rule__c woar, String actionType) {
        return createTaskWorkOrderLineItem(wo, parentWoli, woar, actionType, null);
    }

    private static WorkOrderLineItem createTaskWorkOrderLineItem(WorkOrder wo, WorkOrderLineItem parentWoli, Work_Order_Automation_Rule__c woar, String actionType, String equipmentGroupId) {
        WorkOrderLineItem woli = new WorkOrderLineItem();
        woli.Subject = woar.Action__c;
        woli.Prompt_Number__c = woar.Prompt_Number__c;
        woli.Sort_Order__c = woar.Sort_Order__c;
        woli.Task_Completion_Type__c = woar.Work_Action_Type__c;
        woli.Level__c = woar.Level__c;

        String recordTypeId;
        woli.WorkOrderId = wo.Id;

        // equipment tasks
        if(actionType == 'equipment') {
            woli.ParentWorkOrderLineItemId = parentWoli != null ? parentWoli.Id : null;
            woli.AssetId = parentWoli != null ? parentWoli.AssetId : equipmentGroupId;
            woli.Readings__c = woar.Readings__c;
            recordTypeId = woliRecordTypeMap.get('Asset_Action').getRecordTypeId();
        }
        // site tasks or jsa tasks
        else {            
            if (actionType == 'site' || actionType == 'bsa') {
                recordTypeId = woliRecordTypeMap.get('Site_Action').getRecordTypeId(); 
            }
            
            if(actionType == 'jsa') { 
                if(wo.Account.Trade__c == 'Fire') {
                    recordTypeId = woliRecordTypeMap.get('JSA_FIRE').getRecordTypeId();
                }
                else if(wo.Account.Trade__c == 'HVAC') {
                    recordTypeId = woliRecordTypeMap.get('JSA_HVAC').getRecordTypeId();
                }
            }

            woli.Is_Original_Task__c = true;
            if(!wo.ServiceAppointments.isEmpty()) {
                woli.Service_Appointment__c = wo.ServiceAppointments[0].Id; //@FSL3-469
            }
        }
        
        woli.RecordTypeId = recordTypeId;
        return woli;
    }

    public static void abortLongRunningScheduleJob() {
        for(CronTrigger job: [SELECT Id, NextFireTime, PreviousFireTime, CronJobDetailId, State FROM CronTrigger 
                            WHERE ( CronJobDetail.Name LIKE '%APS_WorkOrderLineItemCreationBatch%' OR CronJobDetail.Name LIKE '%APS_TaskCreationDispatcher%')
                              AND NextFireTime != null AND PreviousFireTime != null AND State = 'Waiting']) {
            Long minutes = (job.NextFireTime.gettime() - job.PreviousFireTime.getTime()) / 1000 / 60;
            if (minutes > 100){
                try{
                    
                System.abortJob(job.id);    
                } catch (Exception excp ){
                    
                }
                
            
            } 
                                  
        }
    }

    //Ananti created ---- Begin  FR-58
    public static Boolean IsSiteTaskGenerationRequired(WorkOrder wo)
    {
        List<WorkOrder> workOrderWtNewSa = new List<WorkOrder>(); 
        workOrderWtNewSa = [SELECT AccountId, Maintenance_Plan__c, ServiceTerritoryId, Account.ParentId, Maintenance_Asset_Ids__c, 
                            Account.Trade__c, JSA_Type__c, WorkTypeId, EGs_wt_MPs__c, WorkType.Name, 
                            (SELECT Id 
                            FROM ServiceAppointments 
                            WHERE RecordType.DeveloperName = 'APS') 
                        FROM WorkOrder 
                        WHERE Id =: wo.Id];

        List<WorkOrderLineItem> bsaTaskWolis = generateBsaTasks(workOrderWtNewSa);
        List<WorkOrderLineItem> jsaTasksWolis = generateJsaTasks((List<WorkOrder>)workOrderWtNewSa);
        List<WorkOrderLineItem> siteTaskWolis = generateSiteTasks((List<WorkOrder>)workOrderWtNewSa);
        List<WorkOrderLineItem> customerTaskWolis =  generateCustomerTasks((List<WorkOrder>)workOrderWtNewSa);
        List<WorkOrderLineItem> territoryTaskWolis = generateTerritoryTask((List<WorkOrder>)workOrderWtNewSa);
                
        Integer toCompareSize = 0;
        toCompareSize += (bsaTaskWolis != null && bsaTaskWolis.size() > 0) ? bsaTaskWolis.size(): 0;
        toCompareSize += (jsaTasksWolis != null && jsaTasksWolis.size() > 0) ? jsaTasksWolis.size(): 0;
        toCompareSize += (siteTaskWolis != null && siteTaskWolis.size() > 0) ? siteTaskWolis.size(): 0;
        toCompareSize += (customerTaskWolis != null && customerTaskWolis.size() > 0) ? customerTaskWolis.size(): 0;
        toCompareSize += (territoryTaskWolis != null && territoryTaskWolis.size() > 0) ? territoryTaskWolis.size(): 0;

        if(toCompareSize > 0)
        {
            return true;
        }

        return false;
    }

    public static Boolean IsSiteTaskWOLICreated(WorkOrder wo)
    {    
        List<WorkOrder> workOrderWtNewSa = new List<WorkOrder>(); 
        workOrderWtNewSa = [SELECT AccountId, Maintenance_Plan__c, ServiceTerritoryId, Account.ParentId, Maintenance_Asset_Ids__c, 
                            Account.Trade__c, JSA_Type__c, WorkTypeId, EGs_wt_MPs__c, WorkType.Name, 
                            (SELECT Id 
                            FROM ServiceAppointments 
                            WHERE RecordType.DeveloperName = 'APS') 
                        FROM WorkOrder 
                        WHERE Id =: wo.Id];

        System.debug('Ananti WorkOrder: ' + workOrderWtNewSa);

        List<WorkOrderLineItem> woliTest = [SELECT Account_ID__c,AssetId,Asset_Name__c,Barcode__c,Client_Work_Order_Number__c,Description,Equipment_Type__c,Id,IsDeleted,LineItemNumber,OrderId,ParentWorkOrderLineItemId,RecordTypeId,RecordType.DeveloperName,Subject,WorkOrderId,WorkOrder_RecordType__c,WorkTypeId FROM WorkOrderLineItem 
                        WHERE WorkOrderId =: wo.Id AND (RecordType.DeveloperName = 'Site_Action' OR RecordType.DeveloperName LIKE 'JSA%')];

        System.debug('Ananti woliTest: ' + woliTest);

        List<WorkOrderLineItem> bsaTaskWolis = generateBsaTasks(workOrderWtNewSa);
        List<WorkOrderLineItem> jsaTasksWolis = generateJsaTasks((List<WorkOrder>)workOrderWtNewSa);
        List<WorkOrderLineItem> siteTaskWolis = generateSiteTasks((List<WorkOrder>)workOrderWtNewSa);
        List<WorkOrderLineItem> customerTaskWolis =  generateCustomerTasks((List<WorkOrder>)workOrderWtNewSa);
        List<WorkOrderLineItem> territoryTaskWolis = generateTerritoryTask((List<WorkOrder>)workOrderWtNewSa);

        System.debug('Ananti bsaTaskWolis: ' + bsaTaskWolis);
        System.debug('Ananti jsaTasksWolis: ' + jsaTasksWolis);
        System.debug('Ananti siteTaskWolis: ' + siteTaskWolis);
        System.debug('Ananti customerTaskWolis: ' + customerTaskWolis);
        System.debug('Ananti territoryTaskWolis: ' + territoryTaskWolis);

        Integer WoliSize = (woliTest != null && woliTest.size() > 0) ? woliTest.size(): 0;
        Integer toCompareSize = 0;
        toCompareSize += (bsaTaskWolis != null && bsaTaskWolis.size() > 0) ? bsaTaskWolis.size(): 0;
        toCompareSize += (jsaTasksWolis != null && jsaTasksWolis.size() > 0) ? jsaTasksWolis.size(): 0;
        toCompareSize += (siteTaskWolis != null && siteTaskWolis.size() > 0) ? siteTaskWolis.size(): 0;
        toCompareSize += (customerTaskWolis != null && customerTaskWolis.size() > 0) ? customerTaskWolis.size(): 0;
        toCompareSize += (territoryTaskWolis != null && territoryTaskWolis.size() > 0) ? territoryTaskWolis.size(): 0;

        System.debug('Ananti WoliSize'  + WoliSize);
        System.debug('Ananti toCompareSize'  + toCompareSize);

        if(WoliSize == toCompareSize) {return true;}

        return false;
    }
    //Ananti created ---- End

}