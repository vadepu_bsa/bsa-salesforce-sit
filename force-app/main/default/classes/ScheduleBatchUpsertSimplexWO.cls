global class ScheduleBatchUpsertSimplexWO implements Schedulable {
    global void execute(SchedulableContext sc) {
        BatchUpsertSimplexWO batchUpsert = new BatchUpsertSimplexWO(); 
        database.executebatch(batchUpsert,200);
    }
}