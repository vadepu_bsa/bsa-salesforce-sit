/**
 * @File Name          : ContentDocumentPublish.cls
 * @Description        : To send document URL to Pronto for insert files 
 * @Author             : Eliana G
 * @Group              : 
 * @Last Modified By   :  
 * @Last Modified On   :  
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    15/12/2020         Eliana G               Initial Version
**/
public class ContentDocumentPublish  {
    public static void generateProntoDocumentPlatformEvent (set<id> IdVal, Map<id,String> operationMap, Id APSRecordTypeId ){

        system.debug('FSL3-837---> start generateProntoDocumentPlatformEvent  operationMap ' + operationMap);
        string Client_Work_Order_Number  = null;
        string LinkedEntityId = null;
        Set<Id> contentDocumentIdSet = new Set<Id>();

        System.debug('FSL3-837---> IdVal '+ IdVal  + ' 0. In generateProntoDocumentPlatformEvent - ContentDocumentLink ');


        // get OrderID (LinkedEntityId) for this document IdVal
        List<ContentDocumentLink> documents = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE Id  IN: IdVal];
        if(documents.size()>0){
            for (ContentDocumentLink d : documents) {
                 LinkedEntityId = d.LinkedEntityId;
            }
        }
        System.debug('FSL3-837---> LinkedEntityId '+ LinkedEntityId + ' APSRecordTypeId '+ APSRecordTypeId + ' 0. In generateProntoDocumentPlatformEvent ');

        // get WO details to validate if it is APS send to Pronto 
        List<workorder> listwo = [SELECT RecordTypeId, Client_Work_Order_Number__c  FROM WorkOrder  WHERE Id =: LinkedEntityId  AND RecordTypeId =: APSRecordTypeId];
        //List<workorder> listwo = new List<workorder>(SELECT WorkOrder_RecordType__c, Client_Work_Order_Number__c  FROM workorder  WHERE Id IN: LinkedEntityId);
        if(listwo.size()>0){
            for (workorder wo : listwo){
               // if(wo.RecordTypeId  != null &&  wo.RecordTypeId == APSRecordTypeId){
                    System.debug('FSL3-837---> Client_Work_Order_Number '+ wo.Client_Work_Order_Number__c + ' 1. inside the for loop ');
                    for (ContentDocumentLink p : documents) {
                        Client_Work_Order_Number = wo.Client_Work_Order_Number__c;
                        contentDocumentIdSet.add(p.ContentDocumentId);
                        System.debug('FSL3-837---> Client_Work_Order_Number = '+ Client_Work_Order_Number +  ' p.ContentDocumentId =  ' + p.ContentDocumentId + ' 2. inside the for loop ');
                    }
                         
                    if (Client_Work_Order_Number != null & contentDocumentIdSet != null){
    
                        List<ContentDistributionDetails> finalDocumentList = contentDocumentSerialize(contentDocumentIdSet, Client_Work_Order_Number, operationMap);
                        
                        System.debug('FSL3-837---> Client_Work_Order_Number '+ Client_Work_Order_Number + ' ContentDocumentId '+ contentDocumentIdSet + ' 3. inside the for loop - before generatePlatformEvent'  );
                
                        List<Action_ProductConsumed_From_BSA__e> InsertplatformEventList = generatePlatformEvents(finalDocumentList);
                        string logs =  JSON.serializePretty(InsertplatformEventList); 
                        // System.Debug('!!! Full Output');  
                        for (Integer i = 0; i < logs.length(); i=i+300) {
                            Integer iEffectiveEnd = (i+300 > (logs.length()-1) ? logs.length()-1 : i+300);
                            System.debug(logs.substring(i,iEffectiveEnd));
                        }
                        if (InsertplatformEventList.size() > 0 ) {
                            List<Database.SaveResult> results = EventBus.publish(InsertplatformEventList);
                            System.debug('FSL3-837---> results = '+ results );
                        }   
                        System.debug('FSL3-837---> logs = '+ logs );
                        
                        System.debug('FSL3-837---> InsertplatformEventList = '+ InsertplatformEventList );
                    }
                //}
            }
        } else {
            System.debug('FSL3-837---> NOT Found workorder for LinkedEntityId = '+ LinkedEntityId + ' RecordTypeId = ' + APSRecordTypeId);
        }

        system.debug('FSL3-837---> END generateProntoDocumentPlatformEvent');
    }
    
    public static List<ContentDistributionDetails>  contentDocumentSerialize(set<id> contentDocumentIdSet, string Client_Work_Order_Number ,  Map<id,String> operationMap){
        
        // query to ContentDistribution to get URL  
        system.debug('FSL3-837--->get URL from ContentDistribution for ContentDocumentId = ' + contentDocumentIdSet );
        List<ContentDistribution> distributionObj = [SELECT DistributionPublicUrl, Name
                                            FROM ContentDistribution
                                            WHERE ContentDocumentId =:contentDocumentIdSet]; 
        // query to Content Document to get fileType 
        //ContentDocument cd = new ContentDocument();

        system.debug('FSL3-837--->get fileType from ContentDocument for ID = ' + contentDocumentIdSet );

        List<ContentDocument> contentDocument  = [SELECT FileType from ContentDocument where Id =: contentDocumentIdSet];
        string fileType = null;
        string name = null;
        if(contentDocument.size()>0){
            for (contentDocument cd : contentDocument){
                 fileType = cd.FileType;
            }
        }

        List<ContentDistributionDetails> ContentDistributionDetailsList = new List<ContentDistributionDetails>();
        for (integer i = 0; i < distributionObj.size(); i++){
            ContentDistributionDetails pObj = new ContentDistributionDetails();

            pObj.DocumentType = null;

            name = distributionObj[i].Name.toLowerCase();
            if(name.contains('service') && name.contains('report')){
            
                pObj.DocumentType = 'Service Docket';
            }

            system.debug('FSL3-837 --> DocumentType' +  pObj.DocumentType);
            pObj.WorkOrderId = Client_Work_Order_Number;
            pObj.DocumentId = contentDocumentIdSet;
            pObj.ContentDownloadUrl= (distributionObj[i].DistributionPublicUrl != null) ? distributionObj[i].DistributionPublicUrl : null;
            pObj.FileType = fileType;
            pObj.DocumentName = distributionObj[i].Name; 
            pObj.Operation ='Insert';     // by default 
            if (operationMap.ContainsKey(distributionObj[i].Id)){
                pObj.Operation = operationMap.get(distributionObj[i].Id);
            }
            ContentDistributionDetailsList.add(pObj);
            system.debug('FSL3-837 --> JSON Serialize ' + JSON.serialize(pObj,false));
        }
        system.debug('FSL3-837 -->  JSON Serialize ALL ' + JSON.serialize(ContentDistributionDetailsList,false));
        return ContentDistributionDetailsList;
    }
    
    public static List<Action_ProductConsumed_From_BSA__e> generatePlatformEvents(List<ContentDistributionDetails> finalDocumentList){
        List<Action_ProductConsumed_From_BSA__e> returnlist = new List<Action_ProductConsumed_From_BSA__e>();
 
         for (ContentDistributionDetails p : finalDocumentList){
             Action_ProductConsumed_From_BSA__e eventObj = new Action_ProductConsumed_From_BSA__e();
 
             eventObj.Action__c = 'Generate_ContentDocumentProntoLink';
             eventObj.CorrelationId__c  = UtilityClass.getUUID();
             eventObj.ProductConsumed__c = '"contentDocumentDetails":' + JSON.serialize(p,false);
             eventObj.WorkOrder__c = '';
             eventObj.Account__c = ''; 
             eventObj.EventTimeStamp__c = String.ValueOf(System.DateTime.Now());
             eventObj.Type__c = 'APS';
             returnlist.add(eventObj);
         }
         return returnlist;
     }

    Public Class ContentDistributionDetails{
        public String WorkOrderId {get;set;}
        public String ContentDownloadUrl{get;set;}
        public string Operation {get;set;}
        public set<id> DocumentId {get;set;}
        public String FileType  {get;set;}
        public String DocumentType{get;set;}
        public String DocumentName  {get;set;}
        
                                  

        public ContentDistributionDetails(){
            WorkOrderId = null;
            ContentDownloadUrl= null;
            Operation = null;
            DocumentId = null;
            FileType = null;
            DocumentType = null;
            DocumentName = null;
        }
    }
}