//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = true)
public class ProntoPurchaseOrderSerialiseTest {

    @IsTest
    static void generateProntoPurchaseOrderPlatformEventTest(){
        
        List<Pricebook2> lstpricebook = Aps_TestDataFactory.createPricebook(2);
        insert lstpricebook;
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        acc.Status__c = 'Active';
        insert acc;

        Order o = new Order();
        o.AccountId = acc.Id;
        o.Status = 'draft';
        o.Name='Test12345';
        o.OrderReferenceNumber = '804405';
        o.Pronto_Status__c = '00';
        o.RecordTypeId = TestRecordCreator.getRecordTypeByDeveloperName('Order', 'Purchase_Order');
        o.EffectiveDate = Date.today();   
        insert o;
        
        // Insert Product
        Product2 product = new Product2();
        product.ProductCode = 'F1/1.3.4.1';
        product.Name ='F1/1.3.4.1 /product';
        product.Family = 'None';
        insert product;

        Order insertedOrder = [SELECT Pricebook2Id FROM Order WHERE Id = :o.Id LIMIT 1];

        Id pricebookId = Test.getStandardPricebookId();
        // Insert PricebookEntry
        PricebookEntry priceBookEntry = new PricebookEntry();
        pricebookEntry.Pricebook2Id = pricebookId;
        pricebookEntry.Product2Id = product.id;
        pricebookEntry.UnitPrice = 0.0;
        pricebookEntry.IsActive = true;
        insert pricebookEntry;

        // Insert PricebookEntry
        PricebookEntry priceBookEntry2 = new PricebookEntry();
        pricebookEntry2.Pricebook2Id = insertedOrder.Pricebook2Id;
        pricebookEntry2.Product2Id = product.id;
        pricebookEntry2.UnitPrice = 0.0;
        pricebookEntry2.IsActive = true;
        pricebookEntry2.UseStandardPrice = false;
        insert pricebookEntry2;

        OrderItem oi = new OrderItem();
        oi.OrderId = o.Id;
        oi.PriceBookEntryId = priceBookEntry2.Id;
        oi.UnitPrice = 0.0;
        insert oi;

        Set<Id> orderIdSet = new Set<Id>();
        Map<Id,String> orderItemOperationMap = new Map<Id,String>();
        orderIdSet.add(o.Id);

        ProntoPurchaseOrderSerialize.generateProntoPurchaseOrderPlatformEvent(orderIdSet, orderItemOperationMap);
    }
    
    
}