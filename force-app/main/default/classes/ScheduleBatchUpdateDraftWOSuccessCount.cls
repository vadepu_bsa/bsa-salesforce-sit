global class ScheduleBatchUpdateDraftWOSuccessCount implements Schedulable{
 	
    public static void start() { 
        Datetime sysTime = System.now().addminutes(2);       
        String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        System.schedule('Batch Update DWOs Success Count', chronExpression, new ScheduleBatchUpdateDraftWOSuccessCount());
    }
    
    global void execute(SchedulableContext SC) {    
        if(!isJobAlreadyRunning('BatchUpdateDraftWOSuccessCount')){
            Type t = Type.forName('BatchUpdateDraftWOSuccessCount');                  
            Database.Batchable<sObject> batch = (Database.Batchable<sObject>)t.newInstance();
            Database.executeBatch(batch);
        }
                
        // Abort job and start again
        System.abortJob( SC.getTriggerId() );  
        ScheduleBatchUpdateDraftWOSuccessCount.start();                    
    }
    
    public static boolean isJobAlreadyRunning(String apexClassName) {
        ApexClass batchApexClass = [Select Id From ApexClass Where Name = :apexClassName limit 1][0];
        List<AsyncApexJob> apexJobs = [Select Id, CompletedDate From AsyncApexJob Where JobType = 'BatchApex' AND ApexClassID = :batchApexClass.Id AND CompletedDate = null];
        if(apexJobs != null && apexJobs.size() > 0){
            return true;
        }   
        //check if more than 5 concurrant batch is running
        if(!([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)) {
                return true;
        }
        return false; 
  	}  
}