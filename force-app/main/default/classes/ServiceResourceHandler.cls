/* Author: Abhijeet Anand
 * Date: 17 October, 2019
 * Trigger Handler added for the ServiceResourceTrigger
 */
 public class ServiceResourceHandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled() {
        return false;
    }
    
    //Before insert method on Service Resource to associate the Service Resource with a Location based on Pronto Service Resource Id
    public void BeforeInsert(List<SObject> newItems) { 
        //Pass ServiceResource List
        if(!newItems.isEmpty()) {
            ServiceResourceAutomationBusinessLogic.linkServiceResourceToLocation(newItems,null);
            ServiceResourceAutomationBusinessLogic.shareProductItemWithServiceResource(newItems,null);
        }
    }
    
    //Before insert method on Service Resource to associate the Service Resource with a Location based on Pronto Service Resource Id
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) { 
        List<SObject> serviceList = new List<SObject>();
        serviceList.addAll((List<SObject>)newItems.values());
        
        //Pass ServiceResource List
        if(!serviceList.isEmpty()) {
            ServiceResourceAutomationBusinessLogic.linkServiceResourceToLocation(serviceList,oldItems);
            ServiceResourceAutomationBusinessLogic.shareProductItemWithServiceResource(serviceList,oldItems);
        }
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {} public void AfterInsert(Map<Id, SObject> newItems) {} public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}

}