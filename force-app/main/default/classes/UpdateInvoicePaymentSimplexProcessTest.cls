@isTest
public class UpdateInvoicePaymentSimplexProcessTest {
    
    static testMethod void TestUpdateSubStatus(){
        
        Test.startTest();
       	Set<Id> woSet = new Set<Id>();
        Map<id,Payment__c> lstPaymentsToBeUpdateds = new Map<id,Payment__c>();
        Map<id,Invoice__c> lstInvoicesToBeUpdateds = new Map<id,Invoice__c>();
        Map<id,Line_Item__c> lstLIToBeUpdateds 	   = new map<id,Line_Item__c>();
        BSA_TestDataFactory.createSeedDataForTesting(); 
        ServiceResource servResource = BSA_TestDataFactory.createTestServiceResource();     
        WorkOrder parentWOj = [SELECT Id,Service_Resource__c,Total_Summary_Cost_BSA__c,Total_Summary_Cost_Tech__c,servicecontract.AccountId,Client_Work_Order_Number__c from  WorkOrder where parentWorkorderid = null Limit 1];
        parentWOj.Service_Resource__c = servResource.Id;
        update parentWOj;
        Pricebook2 pb = [Select Id, Name from Pricebook2 where Name ='NBN HFC Network Assurance NSW Central Metro Year 4'];      
        parentWOj.Sub_Status__c = 'Pending Approval';  
        woSet.add(parentWOj.Id);
        update parentWOj;
        
        List<Invoice__c> inv = BSA_TestDataFactory.createTestInvoices(parentWOj.Id, 1, 'lineItemIdsAsString', true);
        Invoice__c newInv = new Invoice__c();
        newInv.Id = inv[0].Id;
        newInv.BSA_Total__c = parentWOj.Total_Summary_Cost_BSA__c;
        newInv.Invoice_Number__c = '123421';
        inv.add(newInv);
        lstInvoicesToBeUpdateds.putall(inv);
        Update lstInvoicesToBeUpdateds.values(); 
        
        List<Payment__c> pay = BSA_TestDataFactory.createTestPayments(parentWOj.Id, 1, parentWOj.Service_resource__c, 'lineItemIdsAsString', true); 
        Payment__c newPay = new Payment__c();
        newPay.Id = pay[0].Id;
        newPay.Service_Resource__c = servResource.Id;
        newPay.Status__c = 'Pending';        
        pay.add(newPay);
        lstPaymentsToBeUpdateds.putall(pay);
        Update lstPaymentsToBeUpdateds.values();
        
        List<Line_Item__c> lic = BSA_TestDataFactory.createTestLineItems1(parentWOj.Id, 1, pb.Id, true);
        Line_Item__c newLic = new Line_Item__c();
        newLic.Id = lic[0].Id;
        newLic.Invoice__c = newInv.Id;
        newLic.Payment__c = newPay.Id;
        lic.add(newLic);
        lstLIToBeUpdateds.putall(lic);
        update lstLIToBeUpdateds.values();
        UpdateInvoicePaymentSimplexProcess uipsp = new UpdateInvoicePaymentSimplexProcess(woSet);
        Test.stopTest();
    }

}