/**
* @File Name          : ProntoWorkOrderSerialize.cls
* @Description        : class to convert APSworkorderdetails into Pronto Serilize class. 
* @Author             : Mahmood Zubair 
* @Group              : 
* @Last Modified By   : Mahmood Zubair 
* @Last Modified On   : 06/08/2020  , 9:02:16 am
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    06/08/2020   	 Mahmood Zubair            Initial Version
**/
public class ProntoWorkOrderSerialize{
    
    
    
    public static void generateProntoWorkOrderPlatformEvent (set<id> workorderIdset){
        
        List<APSworkOrderDetails> finalprocessList = APSworkOrderSerilize(workorderIdset);
        
        List<Action_B2B_FROM_BSA__e> InsertplatformEventList = generatePlatformEvents(finalprocessList);
        string logs =  JSON.serializePretty(InsertplatformEventList); 
        for (Integer i = 0; i < logs.length(); i=i+300) {
            Integer iEffectiveEnd = (i+300 > (logs.length()-1) ? logs.length()-1 : i+300);
            // System.debug(logs.substring(i,iEffectiveEnd));
        }
        if (InsertplatformEventList.size() > 0 ) {
            List<Database.SaveResult> results = EventBus.publish(InsertplatformEventList);
        }
        
    }
    
    
    public static List<APSworkOrderDetails>  APSworkOrderSerilize(set<id> workorderIdset){
        
        List<Workorder> workorderobj = [SELECT Id, External_Client_WO_Reference_Number__c,Client_Work_Order_Number__c, PO_Number__c, WorkOrderNumber, Status, Account.CustomerCode__c	, Completion_Status__c,
                                        Contact.Name, Contact.Phone, Sub_Status__c, AssetId, WorkType.Pronto_WorkType__c, Description, Entitlement.SlaProcess.Name, StartDate, EndDate , Duration, 
                                        DurationType, SuggestedMaintenanceDate, Work_Notes__c, Street, City, State, PostalCode, Country, ServiceTerritory.Pronto_ID__c, Quoted_Value__c,
                                        Accountid, Contactid, WorktypeId, EntitlementId, Entitlement.Name, Entitlement.SlaProcessId, ServiceTerritoryId, Priority_Syd_Uni__c,
                                        Scope__c
                                        FROM WorkOrder 
                                        WHERE Id =: workorderIdset ]; 
        List<APSworkOrderDetails> workorderDetailsList = new List<APSworkOrderDetails>();
        for (integer i = 0; i < workorderobj.size(); i++){
            APSworkOrderDetails wobj = new APSworkOrderDetails();
            wobj.ClientWorkOrderNumber = (workorderobj[i].Client_Work_Order_Number__c != null) ? workorderobj[i].Client_Work_Order_Number__c  :  workorderobj[i].WorkOrderNumber ;
            wobj.PONumber = (workorderobj[i].PO_Number__c != null) ? workorderobj[i].PO_Number__c  : null;
            wobj.WorkOrderNumber = (workorderobj[i].WorkOrderNumber != null) ? workorderobj[i].WorkOrderNumber  : null;
            wobj.Status = (workorderobj[i].Status != null) ? workorderobj[i].Status  : null;
            wobj.AccountId = (workorderobj[i].AccountId != null && workorderobj[i].Account.CustomerCode__c	 != null) ? workorderobj[i].Account.CustomerCode__c	  : null;
            wobj.CompletionStatus = (workorderobj[i].Completion_Status__c != null) ? workorderobj[i].Completion_Status__c  : null;
            wobj.ContactName = (workorderobj[i].Contactid != null && workorderobj[i].Contact.Name != null) ? workorderobj[i].Contact.Name  : null;
            wobj.ContactPhone = (workorderobj[i].Contactid != null && workorderobj[i].Contact.Phone != null) ? workorderobj[i].Contact.Phone  : null;
            wobj.SubStatus = (workorderobj[i].Sub_Status__c != null) ? workorderobj[i].Sub_Status__c  : null;
            wobj.AssetId = (workorderobj[i].AssetId != null) ? workorderobj[i].AssetId  : null;
            wobj.WorkType = (workorderobj[i].WorkTypeId != null && workorderobj[i].WorkType.Pronto_WorkType__c != null) ? workorderobj[i].WorkType.Pronto_WorkType__c : null;
            wobj.Description = (workorderobj[i].Description != null) ? workorderobj[i].Description  : null;
            wobj.EntitlementId = ( workorderobj[i].EntitlementId != null) ? workorderobj[i].Entitlement.Name  : null;
            if (wobj.EntitlementId == null){
            	wObj.EntitlementId = ( workorderobj[i].EntitlementId != null && workorderobj[i].Entitlement.Name == 'PM - Default Entitlement') ? 'PM' : null;
            }
            if (workorderobj[i].Priority_Syd_Uni__c != null){
                 wobj.EntitlementId = 'P' + workorderobj[i].Priority_Syd_Uni__c;
            }
            wobj.StartDate = (workorderobj[i].StartDate != null) ? string.valueof( workorderobj[i].StartDate)  : null;
            wobj.EndDate  = (workorderobj[i].EndDate != null) ? string.valueof(workorderobj[i].EndDate ) : null;
            wobj.Duration = (workorderobj[i].Duration != null) ?string.valueof(workorderobj[i].Duration)  : null;
            wobj.DurationType = (workorderobj[i].DurationType != null) ? workorderobj[i].DurationType  : null;
            wobj.SuggestedMaintenanceDate = (workorderobj[i].SuggestedMaintenanceDate != null) ? string.valueof(  workorderobj[i].SuggestedMaintenanceDate)  : null;
            wobj.WorkNotes = (workorderobj[i].Work_Notes__c != null) ? workorderobj[i].Work_Notes__c  : null;
            wobj.Street = (workorderobj[i].Street != null) ? workorderobj[i].Street  : null;
            wobj.City = (workorderobj[i].City != null) ? workorderobj[i].City : null;
            wobj.State = (workorderobj[i].State != null) ? workorderobj[i].State : null;
            wobj.PostCode = (workorderobj[i].PostalCode != null) ? workorderobj[i].PostalCode: null;
            wobj.Country = (workorderobj[i].Country != null) ? workorderobj[i].Country : null;
            wobj.ServiceTerritoryId = ( workorderobj[i].ServiceTerritoryId != null && workorderobj[i].ServiceTerritory.Pronto_ID__c != null) ? workorderobj[i].ServiceTerritory.Pronto_ID__c  : null;
            wObj.QuotedValue = ( workorderobj[i].Quoted_Value__c != null ) ? string.valueof(workorderobj[i].Quoted_Value__c) : null;
            wobj.Scope = ( workorderobj[i].Scope__c != null ) ? string.valueof(workorderobj[i].Scope__c) : null;
            wobj.externalClientWORefNumber = ( workorderobj[i].External_Client_WO_Reference_Number__c != null ) ? string.valueof(workorderobj[i].External_Client_WO_Reference_Number__c) : null;   
            workorderDetailsList.add(wobj);
            // system.debug('JSON Serialize ' + JSON.serialize(wobj,true));
        }
        // system.debug('JSON Serialize ALL ' + JSON.serialize(workorderDetailsList,true));
        
        return workorderDetailsList;
    }
    
    Public Static List<Action_B2B_FROM_BSA__e> generatePlatformEvents( List<APSworkOrderDetails> finalprocessList ){
        List<Action_B2B_FROM_BSA__e> returnlist = new List<Action_B2B_FROM_BSA__e>();
        for (integer i = 0 ; i < finalprocessList.size(); i++){
            Action_B2B_FROM_BSA__e eventObj = new Action_B2B_FROM_BSA__e();
            eventObj.Action__c = 'Generate_APS_WorkOrder';
            eventObj.CorrelationId__c  = UtilityClass.getUUID();
            eventObj.WorkOrder__c = '"workorderDetails":' + JSON.serialize(finalprocessList[i],true);
            eventObj.EventDateTime__c = System.DateTime.Now();
            eventObj.ClientWorkOrderNumber__c = finalprocessList[i].ClientWorkOrderNumber;
            returnlist.add(eventObj);
        }
        return returnlist;
    }
    Public Class APSworkOrderDetails{
        public string ClientWorkOrderNumber {get; set;}
        public string PONumber {get;set;}
        public string WorkOrderNumber {get;set;}
        public string Status {get;set;}
        public string AccountId {get;set;}
        public string CompletionStatus {get;set;}
        public string ContactName {get;set;}
        public string ContactPhone {get;set;}
        public string SubStatus {get;set;}
        public string AssetId {get;set;}
        public string WorkType {get;set;}
        public string Description {get;set;}
        public string EntitlementId {get;set;}
        public string StartDate {get;set;}
        public string EndDate  {get;set;}
        public string Duration {get;set;}
        public string DurationType {get;set;}
        public string SuggestedMaintenanceDate {get;set;}
        public string WorkNotes {get;set;}
        public string Street {get;set;}
        public string City {get;set;}
        public string State {get;set;}
        public string PostCode {get;set;}
        public string Country {get;set;}
        public string ServiceTerritoryId {get;set;}
        public string Scope{get;set;}
        public string QuotedValue {get;set;}
        public string externalClientWORefNumber {get;set;}
        
        public APSworkOrderDetails(){
            ClientWorkOrderNumber = null;
            PONumber = null;
            WorkOrderNumber = null;
            Status = null;
            AccountId = null;
            CompletionStatus = null;
            ContactName = null;
            ContactPhone = null;
            SubStatus = null;
            AssetId = null;
            WorkType = null;
            Description = null;
            EntitlementId = null;
            StartDate = null;
            EndDate  = null;
            Duration = null;
            DurationType = null;
            SuggestedMaintenanceDate = null;
            WorkNotes = null;
            Street = null;
            City = null;
            State = null;
            PostCode = null;
            Country = null;
            ServiceTerritoryId = null;
            QuotedValue = null;
            Scope = null;
            externalClientWORefNumber = null;
        }
        
    }
}