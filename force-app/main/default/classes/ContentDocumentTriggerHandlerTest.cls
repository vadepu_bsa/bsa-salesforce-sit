@isTest
public class ContentDocumentTriggerHandlerTest {
  @TestSetup
    static void setup(){
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(1);
        lstWorkType[0].APS__c = true;
        lstWorkType[0].CUI__c = false;
        lstWorkType[0].Name = 'After Hour Call';        
        insert lstWorkType;
        
        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;        

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        //lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[0].Access_Attempts__c = 2;
        lstAccount[2].Access_Attempts__c = 2;
        lstAccount[3].Access_Attempts__c = 2;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;
        
        Id contactClientRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactClientRTypeId, 1);
        insert lstContact;

        Id contactSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Site');
        List<Contact> lstContact2 = APS_TestDataFactory.createContact(lstAccount[2].Id, contactSiteRTypeId, 1);
        lstContact2[0].Service_Report_Recipient__c = true;
        insert lstContact2;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 5);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 2);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;

        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill Limit 10];
        system.debug('!!!!! skillList = ' + skillList);

        Id nonAPSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'APS_Work_Order');
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 1);
        lstWorkOrder[0].RecordTypeId = nonAPSrecordType;
        lstWorkOrder[0].WorkTypeId = lstWorkType[0].Id;
        lstWorkOrder[0].Skill_Group__c = skillList[0].DeveloperName;
        lstWorkOrder[0].Internal_Jeopardy_Comments__c = 'Asfsdf asdf sdfa asdf asdf asdf sdf sdf asdf asdfa sdfasdfsdfsdfsdf sdfs adfsdf';
        lstWorkOrder[0].Pronto_WO_ID__c = '123456';
        insert lstWorkOrder;      
        System.debug('Ananti Work type 1: ' + lstWorkType);
        System.debug('Ananti nonAPSrecordType 1: ' + nonAPSrecordType);       
    }
  
      
    @isTest
    static void DeleteDocumentTest(){               
        List<WorkOrder> lstWorkOrder = [SELECT Id FROM WorkOrder];             
        system.debug('Ananti lstWorkOrder: ' + lstWorkOrder);        
        
        Id APSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'APS_Work_Order');
        system.debug('Ananti APSrecordType: '  + APSrecordType);
        
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsCreatedFromFetchMedia__c =true,
            WOMediaId__c = lstWorkOrder[0].Id,
            IsMajorVersion = true);
        insert contentVersion_1;
        
        ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = lstWorkOrder[0].Id;
        contentlink.ContentDocumentId = contentVersion_2.ContentDocumentId;
        contentlink.ShareType = 'V';
        insert contentlink; 
       
        ContentDocument cd = [select Id FROM ContentDocument  where Id = :contentlink.contentdocumentid LIMIT 1];
             
        /* System.debug('Ananti !ContentDocument' + cd);
        Set<Id> idSet = new Set<Id>();
        idSet.add(contentlink.Id);
        Map<Id,String> orderItemOperationMap = new Map<Id,String>();
        
        List<ContentDocumentLink> documents = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE Id  =: contentlink.Id];
    System.debug('Ananti Document  Test size: ' + documents.size()); */
             
        Test.StartTest();
        delete cd;
        Test.StopTest();                
    }
    
   
    
}