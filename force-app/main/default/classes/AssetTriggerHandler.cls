/**
* @author          IBM - David Azzi
* @date            17/Aug/2020 
* @description     
*
* Change Date       Modified by         Description  
* 17/Aug/2020		David Azzi			Created Trigger Handler
**/

public class AssetTriggerHandler implements ITriggerHandler {
    public AssetTriggerHandler() {

    }

        // Allows unit tests (or other code) to disable this trigger for the transaction
        public static Boolean TriggerDisabled = false;
    
        /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
        */
        public Boolean IsDisabled()
        {
            return false;
        }
        
        public void AfterInsert(Map<Id, SObject> newItems) {
            AssetTriggerHelper.UpdateEGQuantity(newItems);
        } 
        public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
            AssetTriggerHelper.UpdateEGQuantity(newItems);
        } 
        public void AfterDelete(Map<Id, SObject> oldItems) {
            AssetTriggerHelper.UpdateEGQuantity(oldItems);
        } 
        public void BeforeInsert(List<SObject> lstAsset) {} public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}   public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void AfterUndelete(Map<Id, SObject> oldItems) {}
        
}