@isTest
private class APS_TaskCreationSchedulerTest {
    @IsTest
    static void schedulerTest() {
        Datetime schDateTime = Datetime.now().addSeconds(30);
        String hour = String.valueOf(schDateTime.hour());
        String min = String.valueOf(schDateTime.minute()); 
        String ss = String.valueOf(schDateTime.second());

        //parse to cron expression
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';

        APS_TaskCreationScheduler s = new APS_TaskCreationScheduler('Equipment Task', new List<WorkOrderLineItem>(), new List<WorkOrderLineItem>(), new List<WorkOrderLineItem>()); 

        Test.StartTest();
        String sch = System.schedule('Job Started At ' + String.valueOf(Datetime.now()), nextFireTime, s);
        Test.stopTest(); 

        System.assert(String.isNotBlank(sch));
    }
    @IsTest
    static void abortLongRunningScheduleJobTest() {
        // running a job 30 seconds from now only once 
        Datetime schDateTime = Datetime.now().addSeconds(30);
        String hour = String.valueOf(schDateTime.hour());
        String min = String.valueOf(schDateTime.minute()); 
        String ss = String.valueOf(schDateTime.second());

        //parse to cron expression
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';
        List<WorkOrderLineItem> assetWolis = new List<WorkOrderLineItem>();
        APS_WorkOrderLineItemCreationBatchSch s = new APS_WorkOrderLineItemCreationBatchSch(assetWolis, '11234'); 
        System.schedule('Job Started At ' + String.valueOf(Math.random() * 10000) + '-' + UserInfo.getUserId() + ' for APS_WorkOrderLineItemCreationBatch', nextFireTime, s);

        Test.startTest();
        APS_TaskCreationHelper.abortLongRunningScheduleJob();                
        Test.stopTest();
    }
}