global class BatchUpdateDraftWOSuccessCount Implements Database.Batchable<sObject>, Database.Stateful{
    global String strQuery;
    public List<String> batchException = new List<String>();
    String draftWOStatusToConsider;
    Datetime startDate;
    
    //Initialize the query string
    global BatchUpdateDraftWOSuccessCount(){
        FSL_General_Field_Value_Setting__mdt settings = [SELECT MasterLabel, QualifiedApiName, Value__c FROM FSL_General_Field_Value_Setting__mdt where MasterLabel = 'Draft_Work_Order_Start_Date'];
        startDate = Datetime.valueOf(settings.Value__c);
        draftWOStatusToConsider = 'Generation In Progress';
        System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - startDate - ' + startDate);
        strQuery = 'Select Id, Status__c, Total_Num_Of_Work_Orders__c, Num_Of_Failed_Work_Orders__c, Num_of_Success_Work_Orders_v2__c From Draft_Work_Order__c Where Status__c =:draftWOStatusToConsider AND CreatedDate >:startDate';
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - Start - strQuery - ' + strQuery);
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Draft_Work_Order__c> listBatchRecords){
        Map<Id,Draft_Work_Order__c> draftWOIdMap = new Map<Id,Draft_Work_Order__c>();
        for(Draft_Work_Order__c draftWORecord:listBatchRecords){
            draftWOIdMap.put(draftWORecord.Id, draftWORecord);
        }
        
        System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - Execute - draftWOIdMap - ' + draftWOIdMap);
        List<sObject> relatedWOList = [Select Draft_Work_Order__c, count(Id) woCount From WorkOrder Where Draft_Work_Order__c IN:draftWOIdMap.keySet() AND Asset_Tasks_Generated__c = true AND Site_Tasks_Generated__c = true group by Draft_Work_Order__c];
        
        List<Draft_Work_Order__c> draftWOListToProcess = new List<Draft_Work_Order__c>();
        
        for(sobject objRecord: relatedWOList){
            Decimal totalWOCount = (draftWOIdMap.get((String)objRecord.get('Draft_Work_Order__c'))).Total_Num_Of_Work_Orders__c;
            Decimal totalFailedWOCount = (draftWOIdMap.get((String)objRecord.get('Draft_Work_Order__c'))).Num_Of_Failed_Work_Orders__c;
            Decimal totalSuccessWOCount = (Decimal)objRecord.get('woCount');
            Decimal successPlusFailed = totalFailedWOCount + totalSuccessWOCount;
            if(successPlusFailed == totalWOCount){
                Draft_Work_Order__c dwo = draftWOIdMap.get((String)objRecord.get('Draft_Work_Order__c'));
                dwo.Num_of_Success_Work_Orders_v2__c = totalSuccessWOCount;
                draftWOListToProcess.add(dwo);
            }
            System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - Execute - totalWOCount - ' + totalWOCount + ', totalFailedWOCount - ' + totalFailedWOCount + ', totalSuccessWOCount - ' + totalSuccessWOCount);
        }
                
        System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - Execute - draftWOListToProcess - ' + draftWOListToProcess);
        
        try{
			if(draftWOListToProcess.size() > 0){
                Database.update(draftWOListToProcess);
            }
        }catch(Exception batchEx){
            batchException.add(batchEx.getMessage());
            System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - Execute - Exception - ' + batchEx.getMessage());
        }                
    }
    
    global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exception list        
        if(batchException.size() > 0){
            System.debug('DildarLog: Batch - BatchUpdateDraftWOSuccessCount - Finish - Exceptions - ' + batchException);
        }
    }
}