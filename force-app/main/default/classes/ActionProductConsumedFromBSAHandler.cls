public class ActionProductConsumedFromBSAHandler {
    
    public static void captureActionLog(Action_ProductConsumed_From_BSA__e event){
        ApplicationLogUtility.addException(null, JSON.serialize(event), '', 'INFO', 'Action_ProductConsumed_From_BSA__e'+';'+event.CorrelationId__c+';'+event.Action__c);                
    }

}