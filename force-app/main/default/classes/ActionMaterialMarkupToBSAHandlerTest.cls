@isTest
public class ActionMaterialMarkupToBSAHandlerTest {
  

    @TestSetup
    static void setup(){
    
    List<Markup_Code__c> lstMarkupCode = APS_TestDataFactory.createMarkupCode(1);
    insert lstMarkupCode;

    List<Material_Markup__c> lstMaterialMarkup = APS_TestDataFactory.createMaterialMarkup(lstMarkupCode[0].Id, 1);
    insert lstMaterialMarkup;
    
    /*
    Material_Markup__c mm = new Material_Markup__c();
    mm.Markup_Code__c = 'a1U5P000000CYN1UAO';
    
   Markup_Code__c mc = new Markup_Code__c();
    mc.Name = '20000';
    mc.Code__c = '20001';
    insert mm;
    insert mc; 
    */
    }
    
    @isTest
    static void createMarkupTest(){

        Action_MaterialMarkup_to_BSA__e event = new Action_MaterialMarkup_to_BSA__e();
        event.Action__c='CreateMarkup';
        event.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event.CorrelationId__c='9eb99c5f-fdeb-6edc-aa4c-a08d33434234325345234334';
        event.MaterialMarkupCode__c ='12345';
        event.MaterialMarkup__c='"materialDetails":[{"markup-id":"12345","markup-start-value":"5","markup-end-value":"1000","markup-percentage":"25","dummy":"12345","markup-id":"12345","contact-code":"12345","account-code":"12345","markup-key":"12345","markup-name":"12345"}]';
        
        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
        Test.stopTest();
    }
    
    static void deleteMarkupTest(){    

        Action_MaterialMarkup_to_BSA__e event2 = new Action_MaterialMarkup_to_BSA__e();
        event2.Action__c='CreateMarkup';
        event2.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event2.CorrelationId__c='9eb99c5f-fdeb-6edc-aa4c-a08d33434234325345234334';
        event2.MaterialMarkupCode__c ='12345';
        event2.MaterialMarkup__c='"materialDetails":[{"markup-id":"12345","markup-start-value":"5","markup-end-value":"1000","markup-percentage":"25","dummy":"12345","markup-id":"12345","contact-code":"12345","account-code":"12345","markup-key":"12345","markup-name":"12345","Operations":"Delete"}]';
        
        Test.startTest();
        // Publish test event
        //insert mm;
        //insert mc;
        Database.SaveResult sr2 = EventBus.publish(event2);
        
        Test.stopTest();
    }

    static void Material_Markup_CodeTest(){    
        Material_Markup__c mm = new Material_Markup__c();
        mm.Markup_Code__c = '12345';
        
        
        Markup_Code__c mc = new Markup_Code__c();
        mc.Name = '12345';
        mc.Code__c = '12345';
        
        Test.startTest();
        insert mm;
        insert mc;
        Test.stopTest();
    }

    @isTest
    static void updateTest(){
        Material_Markup__c mm = [select Id, Markup_Code__c, Name, Code__c from Material_Markup__c Limit 1];
        //mm.markup_code__c = 'a1U5P000000CYN1UAO';
        mm.code__c = '1';
        mm.Operations__c = 'Delete';
        update mm;
    }
    
}