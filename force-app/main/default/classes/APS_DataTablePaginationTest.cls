@isTest
private class APS_DataTablePaginationTest {
    
    static final List<String> maintenanceRoutines = new String[]{'Monthly', 'Quarterly', 'Bi-Monthly', 'Quarterly', 'Six-Monthly', 'Yearly', 'Bi-Annual', '5 Yearly'};
        
        static String startDueDateStr {
            get{
                if(String.isBlank(startDueDateStr)) {
                    Date startDueDate = Date.today();
                    startDueDateStr = startDueDate.year() + '-' + startDueDate.month() + '-' + startDueDate.day();
                }
                return startDueDateStr;
            }
            set;
        }
    
    static String endDueDateStr {
        get{
            if(String.isBlank(endDueDateStr)) {
                Date endDueDate = Date.today().addMonths(1);
                endDueDateStr = endDueDate.year() + '-' + endDueDate.month() + '-' + endDueDate.day();
            }
            return endDueDateStr;
        }
        set;
    }
    
    @TestSetup
    static void setup(){
        APS_TestDataFactory.testDataSetup();
    }   
    
    @IsTest
    static void fetchTerritoriesOptions() {
        APS_DataTablePagination.ResultWrapper rw = APS_DataTablePagination.fetchRecords('ServiceTerritory', 'Work_Order_Generator', 'IsActive', 'true', '[]', '', '', true, true, '[]', '[]', '[]', '[]', 'service_territory');
        List<APS_DataTablePagination.RowWrapper> rows = rw.lstRows;
        List<APS_DataTablePagination.FieldsWrapper> fields = rw.lstFields;
        
        // assertion
        for(APS_DataTablePagination.RowWrapper row : rows) {
            for(APS_DataTablePagination.FieldsWrapper field : fields) {
                if(field.fieldPath == 'Name') {
                    SObject st = row.record;
                    system.assertEquals('Fire NSW', (String)st.get(field.fieldPath));
                }
            }
        }
    }
    
    @IsTest
    static void fetchCustomerOptions() {
        String selectedServicePeriodsJSON = JSON.serialize(maintenanceRoutines);
        
        List<String> selectedServiceTerritoryIds = new List<String>();
        for(ServiceTerritory st : [SELECT Id FROM ServiceTerritory WHERE IsActive = true]) {
            selectedServiceTerritoryIds.add(st.Id);
        }
        
        String selectedServiceTerritoryIdsJson = JSON.serialize(selectedServiceTerritoryIds);
        
        APS_DataTablePagination.ResultWrapper rw = APS_DataTablePagination.fetchRecords('Account', 'Work_Order_Generator_Customer_List', 'RecordType.DeveloperName', 'Client', selectedServiceTerritoryIdsJson, startDueDateStr, endDueDateStr, false, true, selectedServicePeriodsJSON, '[]', '[]', '[]', 'customer');
        List<APS_DataTablePagination.RowWrapper> rows = rw.lstRows;
        List<APS_DataTablePagination.FieldsWrapper> fields = rw.lstFields;
        
        // assertion
        for(APS_DataTablePagination.RowWrapper row : rows) {
            for(APS_DataTablePagination.FieldsWrapper field : fields) {
                if(field.fieldPath == 'Name') {
                    SObject st = row.record;
                    system.assertEquals('test account', (String)st.get(field.fieldPath));
                }
            }
        }
        
    }
    
    @IsTest
    static void fetchSiteOptions() {
        String selectedServicePeriodsJSON = JSON.serialize(maintenanceRoutines);
        
        List<String> selectedCustomerIds = new List<String>();
        
        for(Account acct : [SELECT Id FROM Account WHERE Name = 'test account']) {
            selectedCustomerIds.add(acct.Id);
        }
        
        String selectedCustomerIdsJson = JSON.serialize(selectedCustomerIds);
        
        APS_DataTablePagination.ResultWrapper rw = APS_DataTablePagination.fetchRecords('Account', 'Work_Order_Generator_Site_List', 'RecordType.DeveloperName', 'Site', '[]', startDueDateStr, endDueDateStr, false, true, selectedServicePeriodsJSON, selectedCustomerIdsJson, '[]', '[]', 'site');
        List<APS_DataTablePagination.RowWrapper> rows = rw.lstRows;
        List<APS_DataTablePagination.FieldsWrapper> fields = rw.lstFields;
        
        // assertion
        for(APS_DataTablePagination.RowWrapper row : rows) {
            for(APS_DataTablePagination.FieldsWrapper field : fields) {
                if(field.fieldPath == 'Name') {
                    SObject st = row.record;
                    system.assertEquals('test site', (String)st.get(field.fieldPath));
                }
            }
        }
    }
    
    @IsTest
    static void fetchEquipmentGroupsOptions() {
        String selectedServicePeriodsJSON = JSON.serialize(maintenanceRoutines);
        
        List<String> selectedSiteIds = new List<String>();
        
        for(Account acct : [SELECT Id FROM Account WHERE Name = 'test site']) {
            selectedSiteIds.add(acct.Id);
        }
        String selectedSiteIdsJson = JSON.serialize(selectedSiteIds);
        
        APS_DataTablePagination.ResultWrapper rw = APS_DataTablePagination.fetchRecords('Asset', 'Work_Order_Generator', '', '', '[]',startDueDateStr, endDueDateStr, false, true, selectedServicePeriodsJSON, '[]', selectedSiteIdsJson, '[]', 'equipment_type');
        List<APS_DataTablePagination.RowWrapper> rows = rw.lstRows;
        List<APS_DataTablePagination.FieldsWrapper> fields = rw.lstFields;
        
        // assertion
        for(APS_DataTablePagination.RowWrapper row : rows) {
            for(APS_DataTablePagination.FieldsWrapper field : fields) {
                if(field.fieldPath == 'Equipment_Group__c') {
                    SObject st = row.record;
                    system.assertEquals('test equipment type', (String)st.get(field.fieldPath));
                }
            }
        }
        List<WorkOrderLineItem> woliList=new List<WorkOrderLineItem>();
        
       
        
    } 
    
 /*   @IsTest
    static void generateWorkOrderOptions() {
        
        // select a service territory
        List<String> selectedServiceTerritoryIds = new List<String>();
        for(ServiceTerritory st : [SELECT Id FROM ServiceTerritory WHERE IsActive = true]) {
            selectedServiceTerritoryIds.add(st.Id);
        }
        
        String selectedServiceTerritoryIdsJson = JSON.serialize(selectedServiceTerritoryIds);
        
        // select a site
        List<String> selectedSiteIds = new List<String>();
        
        for(Account acct : [SELECT Id FROM Account WHERE Name = 'test site']) {
            selectedSiteIds.add(acct.Id);
        }
        String selectedSiteIdsJson = JSON.serialize(selectedSiteIds);
        
        // selected a Customer
        List<String> selectedCustomerIds = new List<String>();
        
        for(Account acct : [SELECT Id FROM Account WHERE Name = 'test account']) {
            selectedCustomerIds.add(acct.Id);
        }
        String selectedCustomerIdsJson = JSON.serialize(selectedCustomerIds);
        
        // select a equipment type   
        List<String> selectedEquipmentTypes = new List<String>();
        
        for(Equipment_Type__c et : [Select Id From Equipment_Type__c Where Name = 'test equipment type']) {       
            selectedEquipmentTypes.add(et.Id);
        }
        String selectedEquipmentTypesJson = JSON.serialize(selectedEquipmentTypes);
        
        // selectedServicePeriods
        String selectedServicePeriodsJSON = JSON.serialize(maintenanceRoutines);
        
        
        // dueDateFrom
        Date startDueDate = Date.today();
        String startDueDateStr = startDueDate.year() + '-' + startDueDate.month() + '-' + startDueDate.day();
        
        // dueDateTo
        Date endDueDate = startDueDate.addMonths(1);
        String endDueDateStr = endDueDate.year() + '-' + endDueDate.month() + '-' + endDueDate.day();
        
        
        APS_DataTablePagination.ResultWrapper rw = APS_DataTablePagination.fetchRecords('WorkOrder', 'Work_Order_Generator', '', '', selectedServiceTerritoryIdsJson, 
                                                                                        startDueDateStr, endDueDateStr, false, true, selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, '');
        List<APS_DataTablePagination.RowWrapper> rows = rw.lstRows;
        List<APS_DataTablePagination.FieldsWrapper> fields = rw.lstFields;
        
        // assertion
        System.assertEquals(1, rows.size());
        for(APS_DataTablePagination.RowWrapper row : rows) {
            for(APS_DataTablePagination.FieldsWrapper field : fields) {
                if(field.fieldPath == 'Line_Item_Count__c') {
                    SObject wo = row.workOrder;
                    system.assertEquals(2, (Decimal)wo.get(field.fieldPath));
                }
                row.isChecked = true;
            }
        }
        
        // insert work order and line items
        String selectedWorkOrderAndLineItemsJSON = JSON.serialize(rows);
        // preview csv
        APS_WorkOrderGeneratorCtr.approveOrDownloadWorkOrders(null, selectedWorkOrderAndLineItemsJSON, selectedServiceTerritoryIdsJson, startDueDateStr, endDueDateStr, 'true', 'true', 
                                                              selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, 'exportToCsv');
        
        Draft_Work_Order__c draftWorkOrder = [SELECT Reference_Id__c, CreatedById, Total_Num_Of_Work_Orders__c, Num_of_Success_Work_Orders_v2__c FROM Draft_Work_Order__c LIMIT 1];
        
        String jsonDraft = APS_WorkOrderGeneratorCtr.retrieveDraftJson(draftWorkOrder.Reference_Id__c);
        System.assert(String.isNotBlank(jsonDraft));
        
        APS_WorkOrderGeneratorCtr.approveOrDownloadWorkOrders(draftWorkOrder.Reference_Id__c, selectedWorkOrderAndLineItemsJSON, selectedServiceTerritoryIdsJson, startDueDateStr, endDueDateStr, 'true', 'true', 
                                                              selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, 'exportToCsv');
        
        // generate work orders
        APS_WorkOrderGeneratorCtr.approveOrDownloadWorkOrders(draftWorkOrder.Reference_Id__c, selectedWorkOrderAndLineItemsJSON, selectedServiceTerritoryIdsJson, startDueDateStr, endDueDateStr, 'true', 'true', 
                                                              selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, 'approveWorkOrders');
        
        APS_WorkOrderGeneratorHelper.sendWorkOrderGenerationSummaryEmails(new List<Draft_Work_Order__c>{draftWorkOrder});
        
        List<WorkOrder> workOrders = [Select Site_Tasks_Generated__c, Asset_Tasks_Generated__c From WorkOrder];
        for(WorkOrder wo : workOrders) {
            wo.Site_Tasks_Generated__c = true;
            wo.Number_of_Remaining_Assets__c = 0;
        }
        update workOrders;        
    }
    
    @IsTest
    static void generateWorkOrderOptions1() {
        
        // select a service territory
        List<String> selectedServiceTerritoryIds = new List<String>();
        for(ServiceTerritory st : [SELECT Id FROM ServiceTerritory WHERE IsActive = true]) {
            selectedServiceTerritoryIds.add(st.Id);
        }
        
        String selectedServiceTerritoryIdsJson = JSON.serialize(selectedServiceTerritoryIds);
        
        // select a site
        List<String> selectedSiteIds = new List<String>();
        
        for(Account acct : [SELECT Id FROM Account WHERE Name = 'test site']) {
            selectedSiteIds.add(acct.Id);
        }
        String selectedSiteIdsJson = JSON.serialize(selectedSiteIds);
        
        // selected a Customer
        List<String> selectedCustomerIds = new List<String>();
        
        for(Account acct : [SELECT Id FROM Account WHERE Name = 'test account']) {
            selectedCustomerIds.add(acct.Id);
        }
        String selectedCustomerIdsJson = JSON.serialize(selectedCustomerIds);
        
        // select a equipment type   
        List<String> selectedEquipmentTypes = new List<String>();
        
        for(Equipment_Type__c et : [Select Id From Equipment_Type__c Where Name = 'test equipment type 1']) {       
            selectedEquipmentTypes.add(et.Id);
        }
        String selectedEquipmentTypesJson = JSON.serialize(selectedEquipmentTypes);
        
        // selectedServicePeriods
        String selectedServicePeriodsJSON = JSON.serialize(new String[]{'Monthly'});
        
        // dueDateFrom
        Date startDueDate = Date.today();
        String startDueDateStr = startDueDate.year() + '-' + startDueDate.month() + '-' + startDueDate.day();
        
        // dueDateTo
        Date endDueDate = startDueDate.addMonths(1);
        String endDueDateStr = endDueDate.year() + '-' + endDueDate.month() + '-' + endDueDate.day();
        
        
        APS_DataTablePagination.ResultWrapper rw = APS_DataTablePagination.fetchRecords('WorkOrder', 'Work_Order_Generator', '', '', selectedServiceTerritoryIdsJson, 
                                                                                        startDueDateStr, endDueDateStr, false, true, selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, '');
        List<APS_DataTablePagination.RowWrapper> rows = rw.lstRows;
        List<APS_DataTablePagination.FieldsWrapper> fields = rw.lstFields;
        
        // assertion
        System.assertEquals(1, rows.size());
        for(APS_DataTablePagination.RowWrapper row : rows) {
            for(APS_DataTablePagination.FieldsWrapper field : fields) {
                if(field.fieldPath == 'Line_Item_Count__c') {
                    SObject wo = row.workOrder;
                    system.assertEquals(0, (Decimal)wo.get(field.fieldPath));
                }
                row.isChecked = true;
            }
        }
        
        // insert work order and line items
        String selectedWorkOrderAndLineItemsJSON = JSON.serialize(rows);
        
        // preview csv
        APS_WorkOrderGeneratorCtr.approveOrDownloadWorkOrders(null, selectedWorkOrderAndLineItemsJSON, selectedServiceTerritoryIdsJson, startDueDateStr, endDueDateStr, 'true', 'true', 
                                                              selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, 'exportToCsv');
        // generate work orders
        APS_WorkOrderGeneratorCtr.approveOrDownloadWorkOrders(null, selectedWorkOrderAndLineItemsJSON, selectedServiceTerritoryIdsJson, startDueDateStr, endDueDateStr, 'true', 'true', 
                                                              selectedServicePeriodsJSON, selectedCustomerIdsJson, selectedSiteIdsJson, selectedEquipmentTypesJson, 'approveWorkOrders');
    }*/
    
    @IsTest
    static void findMaintenancePlanTechsTest() {
        
        List<User> users = APS_TestDataFactory.createUser(null, 3);
        insert users;
        
        List<ServiceResource> srs = new List<ServiceResource>();
        
        List<ServiceResource> equipmentGroupTech = (List<ServiceResource>)TestRecordCreator.createSObjects('ServiceResource', 1,
                                                                                                           new Map<String, object>{
                                                                                                               'Name' => 'eg tech',
                                                                                                                   'IsActive' => true,
                                                                                                                   'Pronto_Service_Resource_ID__c' => '1234',
                                                                                                                   'ResourceType' => 'T',
                                                                                                                   'APS_Resource__c' => true,
                                                                                                                   'RelatedRecordId' => users[0].Id
                                                                                                                   }, null);
        srs.addAll(equipmentGroupTech);
        
        List<ServiceResource> maintenancePlanTech = (List<ServiceResource>)TestRecordCreator.createSObjects('ServiceResource', 1,
                                                                                                            new Map<String, object>{
                                                                                                                'Name' => 'eg tech',
                                                                                                                    'IsActive' => true,
                                                                                                                    'Pronto_Service_Resource_ID__c' => '1235',
                                                                                                                    'ResourceType' => 'T',
                                                                                                                    'APS_Resource__c' => true,
                                                                                                                    'RelatedRecordId' => users[1].Id
                                                                                                                    }, null);
        srs.addAll(maintenancePlanTech);
        
        insert srs;
        
        List<Asset> equipmentGroups = [SELECT Id, Technician__c, Skill_Group__c, AccountId, 
                                       (SELECT Id, MaintenancePlanId FROM MaintenanceAssets)
                                       FROM Asset WHERE RecordType.DeveloperName = 'Equipment_Group'];
        
        Set<Id> maintenancePlanIds = new Set<Id>();
        
        Map<Id, List<MaintenanceAsset>> maintenanceAssetsByEgId = new Map<Id, List<MaintenanceAsset>>();
        
        for(Asset eg : equipmentGroups) {
            eg.Technician__c = equipmentGroupTech[0].Id;
            for(MaintenanceAsset ma : eg.MaintenanceAssets) {
                maintenancePlanIds.add(ma.MaintenancePlanId);
            }
        }
        
        update equipmentGroups;
        
        List<MaintenancePlan> maintenancePlans = [SELECT Technician__c FROM MaintenancePlan WHERE Id IN: maintenancePlanIds];
        for(MaintenancePlan mp : maintenancePlans) {
            mp.Technician__c = maintenancePlanTech[0].Id;
        }
        update maintenancePlans;
        
        equipmentGroups = [SELECT Id, Technician__c, Skill_Group__c, AccountId, 
                           (SELECT Id, MaintenancePlanId, AssetId, MaintenancePlan.Technician__c FROM MaintenanceAssets)
                           FROM Asset WHERE RecordType.DeveloperName = 'Equipment_Group'];
        
        for(Asset eg : equipmentGroups) {
            for(MaintenanceAsset ma : eg.MaintenanceAssets) {
                if(!maintenanceAssetsByEgId.containsKey(ma.AssetId)) {
                    maintenanceAssetsByEgId.put(ma.AssetId, new List<MaintenanceAsset>());    
                }
                maintenanceAssetsByEgId.get(ma.AssetId).add(ma);
            }
        }
        
        Test.startTest();      
        Map<Id, Map<Id, Id>> techIdByMaIdByEgId = APS_WorkOrderGeneratorCtr.findTechs(equipmentGroups, maintenanceAssetsByEgId);
        
        Test.stopTest();
        
        // assertion
        for(Map<Id, Id> techIdByMaId : techIdByMaIdByEgId.values()) {
            for(Id techId : techIdByMaId.values()) {
                System.assertEquals(maintenancePlanTech[0].Id, techId);
            }
        }
    }
    
    @IsTest
    static void findSiteTechsTest() {
        
        List<User> users = APS_TestDataFactory.createUser(null, 3);
        insert users;
        
        List<ServiceResource> srs = new List<ServiceResource>();
        
        List<ServiceResource> siteTech = (List<ServiceResource>)TestRecordCreator.createSObjects('ServiceResource', 1,
                                                                                                 new Map<String, object>{
                                                                                                     'Name' => 'site tech',
                                                                                                         'IsActive' => true,
                                                                                                         'Pronto_Service_Resource_ID__c' => '1234',
                                                                                                         'ResourceType' => 'T',
                                                                                                         'APS_Resource__c' => true,
                                                                                                         'RelatedRecordId' => users[0].Id
                                                                                                         }, null);
        srs.addAll(siteTech);
        
        insert srs;
        
        // resource preference
        List<ResourcePreference> rps = new List<ResourcePreference>();
        
        List<Asset> equipmentGroups = [SELECT Id, Technician__c, Skill_Group__c, AccountId 
                                       FROM Asset 
                                       WHERE RecordType.DeveloperName = 'Equipment_Group'];
        // for(Asset eg : equipmentGroups) {
        rps.addAll((List<ResourcePreference>)TestRecordCreator.createSObjects('ResourcePreference', 1,
                                                                              new Map<String, object>{
                                                                                  'RelatedRecordId' => equipmentGroups[0].AccountId, 
                                                                                      'ServiceResourceId' => siteTech[0].Id
                                                                                      }, null));
        // }
        insert rps;
        
        // query AC skill
        Skill sk = [SELECT Id FROM Skill WHERE DeveloperName = 'AC' LIMIT 1];
        
        // associate skill with service resource
        List<ServiceResourceSkill> serviceResourceSkills = (List<ServiceResourceSkill>)TestRecordCreator.createSObjects('ServiceResourceSkill', 1,
                                                                                                                        new Map<String, object>{
                                                                                                                            'SkillId' => sk.Id,
                                                                                                                                'ServiceResourceId' => siteTech[0].Id
                                                                                                                                }, null);
        insert serviceResourceSkills;
        
        equipmentGroups = [SELECT Id, Technician__c, Skill_Group__c, AccountId, 
                           (SELECT Id, MaintenancePlanId, AssetId, MaintenancePlan.Technician__c FROM MaintenanceAssets)
                           FROM Asset WHERE RecordType.DeveloperName = 'Equipment_Group'];
        
        Map<Id, List<MaintenanceAsset>> maintenanceAssetsByEgId = new Map<Id, List<MaintenanceAsset>>();
        
        for(Asset eg : equipmentGroups) {
            for(MaintenanceAsset ma : eg.MaintenanceAssets) {
                if(!maintenanceAssetsByEgId.containsKey(ma.AssetId)) {
                    maintenanceAssetsByEgId.put(ma.AssetId, new List<MaintenanceAsset>());    
                }
                maintenanceAssetsByEgId.get(ma.AssetId).add(ma);
            }
        }
        
        Test.startTest();      
        Map<Id, Map<Id, Id>> techIdByMaIdByEgId = APS_WorkOrderGeneratorCtr.findTechs(equipmentGroups, maintenanceAssetsByEgId);
        
        Test.stopTest();
        
        // assertion
        for(Map<Id, Id> techIdByMaId : techIdByMaIdByEgId.values()) {
            for(Id techId : techIdByMaId.values()) {
                System.assertEquals(siteTech[0].Id, techId);
            }
        }
    }
}