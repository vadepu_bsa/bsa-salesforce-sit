/* Author: Abhijeet Anand
 * Date: 10 September, 2019
 * Apex class containing the business logic around the automation of Pronto_Van_Warehouse Platform Event
 */
 public class ProntoWarehouseSyncAutomation {
    
    public static void syncProntoWarehouseWithSF(List<Pronto_Van_Warehouse_Created_Updated__e> vanWarehouseList) {

        // List to hold all warehouses from Pronto(Location in FSL) to be upserted.
        List<Schema.Location> locationToUpsert = new List<Schema.Location>();

        // Iterate through each notification.
        for(Pronto_Van_Warehouse_Created_Updated__e event : vanWarehouseList) {
            System.debug('Warehouse Code: ' + event.Warehouse_Code__c);
            // Upsert Location to sync SF with Pronto.
            Schema.Location locObj = new Schema.Location();
            locObj.Name = event.Warehouse_Description__c == null?'':event.Warehouse_Description__c;
            locObj.Pronto_Service_Resource_ID__c = event.Pronto_Service_Resource_ID__c == null?'':event.Pronto_Service_Resource_ID__c;
            locObj.Description = event.Warehouse_Description__c == null?'':event.Warehouse_Description__c;
            locObj.Van_Id__c = event.Warehouse_Code__c;
            if(event.Warehouse_Code__c.startsWith('VN')) {
                System.debug('Inside Location Type if');
                locObj.LocationType = 'Van';
            }
            else {
                locObj.LocationType = 'Warehouse';
            }
            locObj.IsMobile = true;
            locObj.IsInventoryLocation = true;
            locObj.Territory__c = event.Warehouse_Region__c;
            locObj.Correlation_ID__c = event.Correlation_ID__c;
            locObj.Event_TimeStamp__c = event.Event_TimeStamp__c;
            locationToUpsert.add(locObj);
        }

        // Upsert all Products in the list.
        //Database.UpsertResult [] results = Database.upsert(locationToUpsert, Schema.Location.Van_Id__c, false);
        Database.UpsertResult [] results = Database.upsert(locationToUpsert, Schema.Location.Pronto_Service_Resource_ID__c, false);

        String jsonPayload = ''; //String to build a JSON payload

        //Pass the sobject list to JSON generator apex class to build a JSON string
        jsonPayload = JSONGeneratorUtility.generateJSON(locationToUpsert);
        
        for(Database.upsertResult result : results) {
            
            
            String msg = ''; //String to write a successful/failure message
            String logLevel = ''; // String to capture Log Level(Info/Warning) on the Application Log object
            Boolean outcome = result.isSuccess();
        
            System.debug('Outcome :'+outcome);

            if(result.isSuccess()) {
                
                logLevel = 'Info';
                if(result.isCreated()) { //successfull insert
                    System.debug('Warehouse with Id ' + result.getId() +' was created ');
                    msg = 'Warehouse with Id ' + result.getId() +' was created ';
                }
                else { //successfull update
                    System.debug('Warehouse with Id ' + result.getId() +' was updated ');
                    msg = 'Warehouse with Id ' + result.getId() +' was updated ';
                }
            }
            else if(!result.isSuccess()) { // failure
                System.debug('Inside failure');
                // Operation failed, so get all errors
                logLevel = 'Error';
                for(Database.Error err : result.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
            }
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, 'Location');

        }
        
        //Insert the application log entry in the SF database
        ApplicationLogUtility.saveExceptionLog();

    }

}