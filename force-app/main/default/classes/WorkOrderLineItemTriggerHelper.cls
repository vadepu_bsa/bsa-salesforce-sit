/**
* @author          Dildar Hussain
* @date            26/Nov/2019
* 
**/
public class WorkOrderLineItemTriggerHelper {
    
    public Static Set<Id> processedWoliIds = new Set<Id>();
    
    public static void sendWoliEmailNotification(List<WorkOrderLineItem> newWoliList, Map<id, SObject> oldWoliMap){   		
        //store ids of all work orders which need to be processed
        Set<Id> parentWorkOrderIds = new Set<Id>();
        
        //store mapping of all the eligible work orders and line items to which system needs to send email notification
        Map<Id, List<WorkOrderLineItem>> eligibleWorkOrderWithWoliList = new Map<Id, List<WorkOrderLineItem>>();
        
        //store all the eligible work orders line items to which system needs to process
        List<WorkOrderLineItem> preNotificationWoliList = new List<WorkOrderLineItem>();
        
        //store attachements with reference with WorkOrders to sent email notification
        Map<Id, List<Messaging.Emailfileattachment>> woAttachmentMap = new Map<Id, List<Messaging.Emailfileattachment>>();
        
        //store attachement links with reference with WorkOrders to sent email notification
        Map<Id, List<String>> woliAttachmentLinkMap = new Map<Id, List<String>>();
        
        //get the parent workorderid for getting other relevant wolis and their statuses
        for(WorkOrderLineItem woli: newWoliList){
            WorkOrderLineItem oldWoli = (WorkOrderLineItem)oldWoliMap.get(woli.Id);
            if(woli.WorkOrderId != null && woli.Status == 'Completed' && woli.IsFaultResolved__c != oldWoli.IsFaultResolved__c && woli.IsFaultResolved__c){
                preNotificationWoliList.add(woli);
                parentWorkOrderIds.add(woli.WorkOrderId);
            }
        }
        
        if(parentWorkOrderIds.size()>0){
            eligibleWorkOrderWithWoliList = requiredCheckForWoliNotification(parentWorkOrderIds);
            // System.debug('DildarLog:@WorkOrderLineItemTriggerHelper.sendWoliEmailNotification- eligibleWorkOrderWithWoliList - ' + eligibleWorkOrderWithWoliList);
            
            if(eligibleWorkOrderWithWoliList != null && eligibleWorkOrderWithWoliList.size()>0){
                woliAttachmentLinkMap = generateDocumentDistributionLinks(eligibleWorkOrderWithWoliList);
                
                //send email against all eligible work orders with WOLIs attachment if applicable
                sendEmail(eligibleWorkOrderWithWoliList, woliAttachmentLinkMap, Null);
            }
        }
    }
    //Mahmood FSL3-144
    public static void cloneWOLINoAccess(List<WorkOrderLineItem> newWoliList, Map<id, SObject> oldWoliMap){ 
         //get the parent workorderid for getting other relevant wolis and their statuses
        List<WorkOrderLineItem> woliToCloneList = new List<WorkOrderLineItem>();
        set<id> parentWorkOrderIds = new set<id>();
        for(WorkOrderLineItem woli: newWoliList){
            WorkOrderLineItem oldWoli = (WorkOrderLineItem)oldWoliMap.get(woli.Id);
            if (String.isNotBlank(woli.Subject)){
                if(woli.WorkOrderId != null && woli.Status == 'No Access' && oldWoli.Status != 'No Access' && woli.WorkOrder_RecordType__c =='APS_Work_Order' &&  woli.AssetId != null
               && woli.Subject.Contains('Do you have access')){
                    woliToCloneList.add(woli);
                    parentWorkOrderIds.add(woli.WorkOrderId);
                }
            }
        }
        if (woliToCloneList.size() > 0){
            List<WorkOrderLineItem> woliClonedList = new List<WorkOrderLineItem>();
            List<WorkOrderLineItem> woliAllList = new List<WorkOrderLineItem>();
            for (integer i = 0; i<woliToCloneList.size(); i++ ){ 
            	
                if (woliToCloneList[i].Current_Access_Attempt__c < woliToCloneList[i].Access_Attempts__c){ 
                    WorkOrderLineItem woliCloneobj  = woliToCloneList[i].clone(false, false, false, false);
                	woliCloneobj.Current_Access_Attempt__c = woliCloneobj.Current_Access_Attempt__c + 1;
                    woliCloneobj.Subject =  'Do you have access (Access Attempt Number ' + string.valueof(woliCloneobj.Current_Access_Attempt__c) +' out of ' + string.valueof(woliToCloneList[i].Access_Attempts__c) +')' ;
               		woliCloneobj.status = 'Open';
                    woliClonedList.add(woliCloneobj);
                } else if (woliToCloneList[i].Current_Access_Attempt__c == woliToCloneList[i].Access_Attempts__c){
                    woliAllList.add(woliToCloneList[i]);
                }
            }
            if (woliClonedList.size() > 0){
               insert woliClonedList;
            }
            if (woliAllList.size() > 0){
                set<Id> parentWOIdSet = new Set<id>();
                set<Id> woAssetIdSet = new Set<id>();
                for (integer i = 0; i <woliAllList.size(); i++){
                    parentWOIdSet.add(woliAllList[i].workorderid);
                    woAssetIdSet.add(woliAllList[i].Assetid);
                }
                List <WorkOrderLineItem> relatedWOLIList = [SELECT Id, Status FROM WorkOrderLineItem WHERE AssetId =: woAssetIdSet AND WorkOrderId =:parentWOIdSet];
                for (integer i = 0; i <relatedWOLIList.size(); i++){
                    relatedWOLIList[i].Status = 'No Access';
                }
                if (relatedWOLIList.size() > 0){
                    update relatedWOLIList;
                }
            }
        }
    
    }
    
     //Mahmood FSL3-810
    public static void updateParentWoliStatus(List<WorkOrderLineItem> newWoliList, Map<id, SObject> oldWoliMap){ 
        //get the parent workorderid for getting other relevant wolis and their statuses
        map<Id,Id> passedWOLIIdMap = new map<Id,Id> ();
        map<Id,Id> failedWOLIIdMap = new map<Id,Id> ();
        
        set<id> parentWOLIIds = new set<id>();
        set<id> workorderIdSet = new set<Id>();
        
        for(WorkOrderLineItem woli: newWoliList){
            WorkOrderLineItem oldWoli = (WorkOrderLineItem)oldWoliMap.get(woli.Id);
            if (String.isNotBlank(woli.Subject)){
                if(woli.WorkOrderId != null 
                && ((woli.Status == 'Passed' || woli.Status == 'N/A') && (oldWoli.Status != 'Passed' && oldWoli.Status != 'N/A'))
                && woli.WorkOrder_RecordType__c =='APS_Work_Order' &&  woli.AssetId != null &&
                woli.Subject.length() > 1){
                       passedWOLIIdMap.put(woli.Id,woli.ParentWorkOrderLineItemId);
                       parentWOLIIds.add(woli.ParentWorkOrderLineItemId);
                       workorderIdSet.add(woli.workorderId);
                   } 
                if(woli.WorkOrderId != null && woli.Status == 'failed' && oldWoli.Status != 'Failed' && woli.WorkOrder_RecordType__c =='APS_Work_Order' &&  woli.AssetId != null &&
                   woli.Subject.length() > 1){
                       failedWOLIIdMap.put(woli.Id,woli.ParentWorkOrderLineItemId);
                       parentWOLIIds.add(woli.ParentWorkOrderLineItemId);
                   } 
            }
        }
        List<WorkOrderLineItem> woliToUpDateList = new List<WorkOrderLineItem>();
        if (!passedWOLIIdMap.IsEmpty() ){
            // system.debug('!!! passedWOLIIdMap: ' + passedWOLIIdMap);
           
            Map<Id,List<WorkOrderLineItem>> allWOLIParentMap = new Map<Id,List<WorkOrderLineItem>>();
            List<WorkOrderLineItem> allWorkOrderList = [SELECT Id, Subject, Status,ParentWorkOrderLineItemId FROM WorkOrderLineItem 
                                                        WHERE Status IN ('Open','On Hold','Failed') AND ParentWorkOrderLineItemId =:parentWOLIIds
                                                       AND workOrderId =:workorderIdSet];
            for (integer i = 0; i < allWorkOrderList.size(); i++){
                if (allWOLIParentMap.containsKey(allWorkOrderList[i].ParentWorkOrderLineItemId)){
                    allWOLIParentMap.get(allWorkOrderList[i].ParentWorkOrderLineItemId).add(allWorkOrderList[i]);
                } else {
                    allWOLIParentMap.put(allWorkOrderList[i].ParentWorkOrderLineItemId, new List<WorkOrderLineItem>{allWorkOrderList[i]});
                }
            }
            
            set<id> passedWOLIToSend = new set<id>();
            for (id parentWOliID : passedWOLIIdMap.Values()){                
                if (!allWOLIParentMap.ContainsKey(parentWOliID)){
                    if(!passedWOLIToSend.Contains(parentWOliID))
                    {                        
                        passedWOLIToSend.add(parentWOliID);
                        woliToUpDateList.add(new WorkOrderLineItem(Id =parentWOliID, Status= 'Passed'));
                    }
                }
            }                   
        }
        // system.debug('!!! failedWOLIIdMap: ' + failedWOLIIdMap);
        set<id> failedWOLIToSend = new set<id>();
        for (id parentWOliID : failedWOLIIdMap.Values()){
            if(!failedWOLIToSend.Contains(parentWOliID))
            {
                failedWOLIToSend.add(parentWOliID);
                woliToUpDateList.add(new WorkOrderLineItem(Id =parentWOliID, Status= 'Failed'));
            }            
        }
        
        if (woliToUpDateList.size()>0){
            WorkOrderLineItemTriggerHandler.TriggerDisabled = True;
            update woliToUpDateList;
            WorkOrderLineItemTriggerHandler.TriggerDisabled = False;
        }
        
        
    }
    
    public static Map<Id, List<WorkOrderLineItem>> requiredCheckForWoliNotification(Set<Id> workOrderIds){
        
        //store final eligible list of all work orders and related WOLIs
        Map<Id, List<WorkOrderLineItem>> workOrderWithWoliList = new Map<Id, List<WorkOrderLineItem>>();
        
        //store the work order ids where any of the required_pre_finish WOLI is not completed
        Set<Id> notEligibleWorkOrders = new Set<Id>();
        
        //get all related WOLIs
        List<WorkOrderLineItem> preFinishWoliList = [Select Id, LineItemNumber, WorkOrderId, Status, Description, Required_Pre_Finish__c, WorkOrder.Service_Resource__r.Contact__r.Email, WorkOrder.Service_Resource__r.Contact__r.Id, WorkOrder.ParentWorkOrderId, WorkOrder.ParentWorkOrder.WorkOrderNumber, WorkOrder.ParentWorkOrder.Client_Work_Order_Number__c, WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c, WorkOrder.ParentWorkOrder.NBN_Reference_ID__c, WorkOrder.ParentWorkOrder.City from WorkOrderLineItem 
                                                     where WorkOrderId IN:workOrderIds Order By LineItemNumber ASC];
        
        // System.debug('DildarLog:@WorkOrderLineItemTriggerHelper.requiredCheckForWoliNotification- preFinishWoliList - ' + preFinishWoliList);
        
        if(workOrderIds.size()>0){
            for(Id wId: workOrderIds){
                List<WorkOrderLineItem> matchedWoliList = new List<WorkOrderLineItem>();
                for(WorkOrderLineItem preFinishWoli: preFinishWoliList){
                    if(preFinishWoli.WorkOrderId == wId){
                        matchedWoliList.add(preFinishWoli);
                        processedWoliIds.add(preFinishWoli.Id);
                    }
                }
                if(matchedWoliList.size()>0)
                    workOrderWithWoliList.put(wId, matchedWoliList);
            }
        }
        // System.debug('DildarLog:@WorkOrderLineItemTriggerHelper.requiredCheckForWoliNotification- workOrderWithWoliList - ' + workOrderWithWoliList);
        
        if(workOrderWithWoliList.size() > 0){
            return workOrderWithWoliList;
        }
        return null;
    }
    
    public static Map<Id, List<String>> generateDocumentDistributionLinks(Map<Id, List<WorkOrderLineItem>> workOrderWithWoliList){
        List<Id> contentDocumentIds = new List<Id>();
        Map<Id, String> contentDocAttachmentLinkMap = new Map<Id, String>();
        Map<Id, List<String>> woAttachmentLinkMap = new Map<Id, List<String>>();
        Map<Id, List<String>> woliAttachmentLinkMap = new Map<Id, List<String>>();
        Map<Id,Id> contentDocEntityIdMap = new Map<Id,Id>();
        
        Map<Id,Id> woliWOMap = new Map<Id,Id>();
        Map<Id,WorkOrderLineItem> woliMap = new Map<Id,WorkOrderLineItem>();
        
        for(Id wId: workOrderWithWoliList.keySet()){
            for(WorkOrderLineItem woli: workOrderWithWoliList.get(wId)){
                woliMap.put(woli.Id, woli);
                woliWOMap.put(woli.Id, wId);
            }
        }
        
        for(contentDocumentLink CDLink : [SELECT LinkedEntityid, ContentDocumentid FROM contentDocumentLink WHERE LinkedEntityid IN:woliWOMap.keySet()]){
            contentDocEntityIdMap.put(CDLink.ContentDocumentid,CDLink.LinkedEntityId);
        }
        
        List<ContentDistribution> contentDList = new List<ContentDistribution>();
        
        for ( ContentVersion cversion : [SELECT title, 
                                         PathOnClient, FileType,
                                         versiondata, ContentDocumentId
                                         FROM contentversion 
                                         WHERE ContentDocumentId IN :contentDocEntityIdMap.keySet()]){
                                             
                                             String woliDescription = woliMap.get(contentDocEntityIdMap.get(cversion.ContentDocumentId)).Description;
                                             ContentDistribution contentDRecord = new ContentDistribution();
                                             contentDRecord.Name = woliDescription+' - '+cversion.Title+'.'+cversion.FileType;
                                             contentDRecord.ContentVersionId = cversion.id;
                                             contentDRecord.PreferencesAllowViewInBrowser= true;
                                             contentDRecord.PreferencesLinkLatestVersion=true;
                                             contentDRecord.PreferencesNotifyOnVisit=false;
                                             contentDRecord.PreferencesPasswordRequired=false;
                                             contentDRecord.PreferencesAllowOriginalDownload= true;
                                             contentDList.add(contentDRecord);
                                         }
        
        if(contentDList.size() > 0){
            try{
                insert contentDList;
            }catch(DMLException dmlExp){
                // System.debug('DildarLog:@WorkOrderLineItemTriggerHelper.generateDocumentDistributionLinks- dmlExp - ' + dmlExp);
            }
        }
        
        for(ContentDistribution contentDRecord: [Select Id, 
                                                 DistributionPublicUrl,
                                                 ContentDocumentId,
                                                 Name
                                                 FROM ContentDistribution
                                                 WHERE Id IN:contentDList ORDER BY Name ASC]){
                                                     
                                                     String row = '<tr><td>'+contentDRecord.Name+'</td><td><a href="'+contentDRecord.DistributionPublicUrl+'">View/Download File</a></td></tr>';
                                                     
                                                     if(woAttachmentLinkMap.containsKey(woliWOMap.get(contentDocEntityIdMap.get(contentDRecord.ContentDocumentId)))){
                                                         woAttachmentLinkMap.get(woliWOMap.get(contentDocEntityIdMap.get(contentDRecord.ContentDocumentId))).add(row);
                                                     }else{
                                                         woAttachmentLinkMap.put(woliWOMap.get(contentDocEntityIdMap.get(contentDRecord.ContentDocumentId)), new List<String>{row});
                                                     }
                                                 }
        
        // System.debug('DildarLog:@WorkOrderLineItemTriggerHelper.contentDocAttachmentLinkMap- woAttachmentLinkMap - ' + woAttachmentLinkMap);
        
        if(woAttachmentLinkMap.size()>0){
            return woAttachmentLinkMap;
        }        
        return null;
    }
    
    public static void sendEmail(Map<Id, List<WorkOrderLineItem>> eligibleWorkOrderWithWoliList, Map<Id, List<String>> woAttachmentLinkMap, Map<Id, List<Messaging.Emailfileattachment>> woAttachmentMap){
        
        //get the relevant settings from custom meta data type object
        Map<String, String> fslGeneralSettingsMap = New Map<String, String>();
        for(FSL_General_Field_Value_Setting__mdt setting: [SELECT MasterLabel, QualifiedApiName, Value__c FROM FSL_General_Field_Value_Setting__mdt]){
            fslGeneralSettingsMap.put(setting.QualifiedApiName, setting.Value__c);
        }
        
        EmailTemplate woliEmailTemplate =  [select id, name, Body, HtmlValue, DeveloperName, Subject from EmailTemplate where developerName =:fslGeneralSettingsMap.get('Woli_Email_Notification_Template')];
        
        List<WorkOrderLineItemHistory> WOLIH = [Select Id, OldValue, NewValue, WorkOrderLineItemId, CreatedDate From WorkOrderLineItemHistory
                                                Where Field = 'Status' AND
                                                WorkOrderLineItemId IN:processedWoliIds];
        Map<Id, WorkOrderLineItemHistory> woliHistoryMap = new Map<Id, WorkOrderLineItemHistory>();
        if(WOLIH.size() > 0){
            for(WorkOrderLineItemHistory history:WOLIH){
                if(history.OldValue != 'Completed' && history.NewValue == 'Completed')
                    woliHistoryMap.put(history.WorkOrderLineItemId, history);
            }
        }
        try{
            for(Id wId: eligibleWorkOrderWithWoliList.keySet()){
                String rows = '';
                if(woAttachmentLinkMap.get(wId).size() > 0){
                    for(String url: woAttachmentLinkMap.get(wId)){
                        rows+=Url;
                    }
                }
                String processStartDate ='';
                if(!test.isRunningTest()){
                    
                    processStartDate = String.valueOf(woliHistoryMap.get(eligibleWorkOrderWithWoliList.get(wId)[0].Id).CreatedDate);                
                }
                
                String processEndDate = String.valueOf(System.now());                
                String technicianEmail = eligibleWorkOrderWithWoliList.get(wId)[0].WorkOrder.Service_Resource__r.Contact__r.Email;
                String NBNParentWOId = eligibleWorkOrderWithWoliList.get(wId)[0].WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c;
                String NBNWORefId = eligibleWorkOrderWithWoliList.get(wId)[0].WorkOrder.ParentWorkOrder.NBN_Reference_ID__c;
                String NBNClientWONumber = eligibleWorkOrderWithWoliList.get(wId)[0].WorkOrder.ParentWorkOrder.Client_Work_Order_Number__c;
                String Suburb = eligibleWorkOrderWithWoliList.get(wId)[0].WorkOrder.ParentWorkOrder.City;
                
                if(technicianEmail != null && technicianEmail != ''){
                    EmailUtility sendEmailUtil = new EmailUtility(new List<String>{technicianEmail});
                    
                    String technicianContactId = eligibleWorkOrderWithWoliList.get(wId)[0].WorkOrder.Service_Resource__r.Contact__r.Id;
                    String tableBody = Label.Woli_Email_Body_Contents+'<br><Table border="1"><th>File Name</th><th>File Link</th>'+rows+'</Table><br>'+Label.Woli_Email_Footer_Contents;
                    String templateBody = woliEmailTemplate.body;
                    String templateSubject = woliEmailTemplate.Subject;
                    
                    NBNParentWOId = NBNParentWOId != null?NBNParentWOId:'';
                    NBNWORefId = NBNWORefId != null?NBNWORefId:'';
                    NBNClientWONumber = NBNClientWONumber != null?NBNClientWONumber:'';
                    Suburb = Suburb != null?Suburb:'';
                    
                    //Setup email body with dynamic field values
                    templateBody = templateBody.replace('{!WorkOrder.Description}', tableBody);
                    templateBody = templateBody.replace('{!StartDateTime}', processStartDate);
                    templateBody = templateBody.replace('{!FinishDateTime}', processEndDate);
                    templateBody = templateBody.replace('{!NBNParentWOId}', NBNParentWOId);
                    templateBody = templateBody.replace('{!NBNRefId}', NBNWORefId);
                    templateBody = templateBody.replace('{!NBNChildWOId}', NBNClientWONumber);
                    
                    //Setup email subjectline with dynamic field values
                    templateSubject = templateSubject.replace('{!WorkOrder.ParentWorkOrder}',NBNClientWONumber);
                    templateSubject = templateSubject.replace('{!WorkOrder.City}',Suburb);
                    templateSubject = templateSubject.replace('{!WorkOrder.NBN_Reference_ID__c}',NBNWORefId);
                    templateSubject = templateSubject.replace('{!WorkOrder.NBN_Parent_Work_Order_Id__c}',NBNParentWOId);                                       
                    
                    sendEmailUtil.senderDisplayName(fslGeneralSettingsMap.get('Woli_Email_Sender_Name')).subject(templateSubject).htmlBody(templateBody).templateId(woliEmailTemplate.id).saveAsActivity(true).targetObjectId(technicianContactId).whatId(wId).sendEmail();
                }
            }
        }catch(Exception emailEx){
            // System.debug('Exception: '+ emailEx.getMessage()+emailEx.getStackTraceString());
        }
    }

    //#FSL3-416 - David A (IBM) - 01/Sep/20 - Create WOARS for Woli Import for AusGrid and BRS
    public static void createWOARforImportWOLI(List<WorkOrderLineItem> lstWoli){
        
        
        Map<String, Map<String, List<WorkOrderLineItem>>> mapEquipmentTypeMaintenanceRoutineWoli = new Map<String, Map<String, List<WorkOrderLineItem>>>();
        Map<String, List<Work_Order_Automation_Rule__c>> mapWoarsByEquipmentType = new Map<String, List<Work_Order_Automation_Rule__c>>();
        List<WorkOrderLineItem> lstInputChildWolis = new List<WorkOrderLineItem>();

        //get all routines and ET from the imported Wolis
        for(WorkOrderLineItem woli : lstWoli){
            if (!mapEquipmentTypeMaintenanceRoutineWoli.containsKey(woli.Equipment_Type__c)){
                mapEquipmentTypeMaintenanceRoutineWoli.put(woli.Equipment_Type__c, new map<String, List<WorkOrderLineItem>>());
            }
            if (mapEquipmentTypeMaintenanceRoutineWoli.containsKey(woli.Equipment_Type__c) && !mapEquipmentTypeMaintenanceRoutineWoli.get(woli.Equipment_Type__c).containsKey(woli.Maintenance_Routine__c)){
                mapEquipmentTypeMaintenanceRoutineWoli.get(woli.Equipment_Type__c).put(woli.Maintenance_Routine__c, new List<WorkOrderLineItem>());
            }
            
            mapEquipmentTypeMaintenanceRoutineWoli.get(woli.Equipment_Type__c).get(woli.Maintenance_Routine__c).add(woli);
        }

        //get WOARS by Equipment Type
        for(Work_Order_Automation_Rule__c woar : [SELECT Action__c, Type__c, Maintenance_Routine_Global__c, Account__c, Primary_Access_Technology__c, 
                                                    Equipment_Type__c, Equipment_Type__r.Name, Readings__c, Prompt_Number__c, Sort_Order__c, Work_Action_Type__c, 
                                                    Service_Territory__c, Account__r.RecordType.DeveloperName, Account__r.Type, Level__c, Work_Type__c
                                                FROM Work_Order_Automation_Rule__c 
                                                WHERE RecordType.DeveloperName = 'APS_WOLI_Automation' 
                                                AND Active__c = true
                                                AND Equipment_Type__r.Name != NULL
                                                ORDER BY Sort_Order__c ASC]) {            
            if(!mapWoarsByEquipmentType.containsKey(woar.Equipment_Type__r.Name)) {
                mapWoarsByEquipmentType.put(woar.Equipment_Type__r.Name, new List<Work_Order_Automation_Rule__c>());
            }
            mapWoarsByEquipmentType.get(woar.Equipment_Type__r.Name).add(woar);
        }

        //Match ET and Maintenance Routine, then create a child WOLI with WOAR rules
        for(String woliET : mapEquipmentTypeMaintenanceRoutineWoli.keySet()){
            //matched ET
            if (mapWoarsByEquipmentType.containsKey(woliET)){
                //loop through woars by ET
                for(Work_Order_Automation_Rule__c woar : mapWoarsByEquipmentType.get(woliET)){
                    //loop through Wolis by MaintenanceRoutine
                    for(WorkOrderLineItem parentWoli : mapEquipmentTypeMaintenanceRoutineWoli.get(woliET).get(woar.Maintenance_Routine_Global__c)){
                        if (APS_TaskCreationHelper.matchMaintenanceRoutines(parentWoli.Maintenance_Routine__c, woar.Maintenance_Routine_Global__c, 'equipment')){
                            WorkOrderLineItem childWoli = APS_TaskCreationHelper.createTaskWorkOrderLineItem(
                                                                new WorkOrder(Id = parentWoli.WorkOrderId), //workorder
                                                                parentWoli, //parentWoli
                                                                woar, //woar
                                                                'equipment' //type
                                                            );
                            lstInputChildWolis.add(childWoli);
                        }
                    }
                }
            }
        }

        //insert WOLI
        if (lstInputChildWolis.size() > 0){
            try{
                Database.insert(lstInputChildWolis);
            }catch(Exception e){
                // System.debug('*-* Exception on APS_TaskCreationHelper.createWOARforImportWOLI:' + e.getMessage());
            }
        }
        
    }
}