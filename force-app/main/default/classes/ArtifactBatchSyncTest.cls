@istest(seeAllData = true)
public class ArtifactBatchSyncTest {

    testMethod private static void testArtifactBatchSync() {
       
        test.startTest();
        
        database.executeBatch(new ArtifactBatchSync());
         
        test.stopTest(); 

    }
    
}