/**
 * @author          Sunila.M
 * @date            20/Oct/2019   
 * @description     Trigger Handler added fo rthe Work Order trigger
 *
 * Change Date    Modified by         Description  
 * 29/Jun/2020    Bohao Chen @IBM     JIRA FSL3-11 create BSA task and site task work order line items
  **/
public class DraftWorkOrderTriggerHandler implements ITriggerHandler{

    public static Boolean TriggerDisabled = false;

        /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    public void BeforeInsert(List<SObject> workOrderList) {}
    public void AfterInsert(Map<Id, SObject> newItems) {}
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
        
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        // send email when draft work order status changed from 'Generation In Progress' to 'Generation Completed'
        List<Draft_Work_Order__c> draftWorkOrders = new List<Draft_Work_Order__c>();

        for(Id dwoId : newItems.keySet()) {
            Draft_Work_Order__c oldDwo = (Draft_Work_Order__c)oldItems.get(dwoId);
            Draft_Work_Order__c newDwo = (Draft_Work_Order__c)newItems.get(dwoId);
            if(oldDwo.Status__c == 'Generation In Progress' && newDwo.Status__c == 'Generation Completed') {
                draftWorkOrders.add(newDwo);
            }
        }
        System.debug('@draftWorkOrders: ' + draftWorkOrders);
        
        if(!draftWorkOrders.isEmpty()) {
            APS_WorkOrderGeneratorHelper.sendWorkOrderGenerationSummaryEmails(draftWorkOrders);
        }
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}
}