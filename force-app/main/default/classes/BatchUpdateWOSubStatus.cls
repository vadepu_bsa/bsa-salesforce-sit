global class BatchUpdateWOSubStatus Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
    global String strQuery;
    public List<String> batchException = new List<String>();
    global set<Id> workOrderIdSet = New Set<Id>();
    //Initialize the query string
    global BatchUpdateWOSubStatus(){
        String queryFields = 'Work_Order_Id__c,'+
            'Reason_Code__c,'+
            'Data_Validation_Outcome__c';
        
        strQuery = 'SELECT '+queryFields+' from Simplex_Activity_Staging__c Where IsProcessed__c = false AND recordtype.developername =\'SOR_Activity_Validation_Report\' ';
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Simplex_Activity_Staging__c> stagingRecordList){
        boolean isGLCodeError =false;
        boolean isProntoTerritoryError =false;
        boolean isError =false;
        Map<String,Simplex_Activity_Staging__c> mWoToStaging = new Map<String,Simplex_Activity_Staging__c>();
        Map<String,List<Simplex_Activity_Staging__c>> mWoToStagingList = new Map<String,List<Simplex_Activity_Staging__c>>();
        List<WorkOrder> woToBeUpdated = new List<WorkOrder>();
        List<Case> casesToBeInserted = new List<Case>();
        set<Id> validatedWOs = new Set<Id>();
        for(Simplex_Activity_Staging__c objActivity : stagingRecordList){
            if(mWoToStaging.keySet().contains(objActivity.Work_Order_Id__c)){
                if(mWoToStaging.get(objActivity.Work_Order_Id__c).Data_Validation_Outcome__c.equalsIgnoreCase('VALID') 
                   && objActivity.Data_Validation_Outcome__c.equalsIgnoreCase('ERROR')){
                       mWoToStaging.put(objActivity.Work_Order_Id__c,objActivity);
                   }
            }
            else{
                mWoToStaging.put(objActivity.Work_Order_Id__c,objActivity);
            }
            List<Simplex_Activity_Staging__c> lsttemp = new List<Simplex_Activity_Staging__c>();
            if(mWoToStagingList.keyset().contains(objActivity.Work_Order_Id__c)){
                lsttemp = mWoToStagingList.get(objActivity.Work_Order_Id__c);
            }
            lsttemp.add(objActivity);
            mWoToStagingList.put(objActivity.Work_Order_Id__c,lsttemp);
        }
        Id caseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByDeveloperName().get('Dispute').getRecordTypeId();
        List<Group> lstQueue =[select Id from Group where Name = 'Dispute Case Queue' and Type = 'Queue'];
        
        List<WorkOrder> workOrderList =[select id,WorkOrderNumber,Client_Work_Order_Number__c,sub_status__c,GL_Code__c,Pronto_Territory_Id__c,recordtype.developername from workorder where Client_Work_Order_Number__c=:mWoToStaging.keySet()];
         Set<Id> woIds = new Set<Id>();
        Map<String, WorkOrder> woMap = new Map<String, WorkOrder>();
        for(WorkOrder wo: workOrderList){
            woMap.put(wo.Client_Work_Order_Number__c, wo);
            woIds.add(wo.Id);
        }
        UpdateInvoicePaymentSimplexProcess uipsp = new UpdateInvoicePaymentSimplexProcess(woIds);
        for(WorkOrder woObj: workOrderList){
            isGLCodeError =false;
            isProntoTerritoryError =false;
            isGLCodeError = woObj.GL_Code__c!='' && woObj.GL_Code__c!=null ?false:true;
            isProntoTerritoryError = woObj.Pronto_Territory_Id__c!='' && woObj.Pronto_Territory_Id__c!=null ?false:true;
            
            Simplex_Activity_Staging__c objActivity = mWoToStaging.get(woObj.Client_Work_Order_Number__c);
            
            if(objActivity!=null && objActivity.Data_Validation_Outcome__c.equalsIgnoreCase('VALID')){
                isError = false;
            }
            else if(objActivity!=null && objActivity.Data_Validation_Outcome__c.equalsIgnoreCase('ERROR')){
                isError =true;
            }
            else{
                isError =true;
            }
            System.debug('SAYALI: Debug '+woObj.id+','+isError+','+isGLCodeError+','+isProntoTerritoryError);
            if(!isError && !isGLCodeError && !isProntoTerritoryError){
                System.debug('SAYALI: Debug woObj- '+woObj.Id+' '+woObj.RecordType.developername);
                if(woObj.RecordType.developername =='Parent_CUI_Work_Order'){
                    woObj.Sub_Status__c = 'Ready For Review';
                }
                
                if(woObj.RecordType.developername =='Simplex_CUI_Work_Order' ){
                    woObj.Sub_Status__c = 'Pending Approval';
                    validatedWOs.add(woObj.Id);
                }
                
                
            }
            else {
                woObj.Sub_Status__c = 'In Dispute';
                Case caseObj = new Case();
                caseObj.Work_Order__c = woObj.Id;
                String description =(String)objActivity.Reason_Code__c;
                
                if(isGLCodeError){
                    description =description+'\nGL Code is missing on work order.';
                }
                if(isProntoTerritoryError){
                    description =description+'\nPronto Territory Id is missing on work order.';
                }
                caseObj.Description = description;
                caseObj.Subject ='Dispute on WorkOrder : '+woObj.Client_Work_Order_Number__c;
                caseObj.recordTypeId = caseRecordTypeId;
                if(lstQueue!=null && lstQueue.size()>0){
                    caseObj.OwnerId =  lstQueue[0].Id;
                }
                casesToBeInserted.add(caseObj);
            }
            woToBeUpdated.add(woObj);
            
        }
        List<Payment__c> lstPaymentsToBeUpdated = new List<Payment__c>();
        List<Invoice__c> lstInvoicesToBeUpdated = new List<Invoice__c>();
        List<Line_Item__c> lstLIToBeUpdated = new List<Line_Item__c>();
        System.debug('Sayali Log - validatedWOs-'+validatedWOs);
        if(validatedWOs!=null && validatedWOs.size()>0){
            List<Payment__c> lstPayments =[select id,Sync_Status__c,Status__c from Payment__c where Work_Order__c in:validatedWOs AND Status__c='Pending' AND Sync_Status__c = null ];
            for(Payment__c paymentObj :lstPayments){
                paymentObj.Sync_Status__c = 'Ready For Sync';
                lstPaymentsToBeUpdated.add(paymentObj);
            }
            List<Invoice__c> lstInvoices =[select id,Sync_Status__c,Status__c from Invoice__c where Work_Order__c in:validatedWOs AND Status__c='Pending' AND Sync_Status__c = null ];
            for(Invoice__c invObj :lstInvoices){
                invObj.Sync_Status__c = 'Ready For Sync';
                lstInvoicesToBeUpdated.add(invObj);
            }
            List<Line_Item__c> lstLineItems =[Select Id,Status__c from Line_Item__c where Status__c='Pending' And Work_Order__c in:validatedWOs];
            for(Line_Item__c liObj :lstLineItems){
                liObj.Status__c = 'Committed';
                lstLIToBeUpdated.add(liObj);
            }
            System.debug('Sayali Log - lstLIToBeUpdated-'+lstLIToBeUpdated);
            
        }
        
        if(lstLIToBeUpdated!=null && lstLIToBeUpdated.size()>0){
            Update lstLIToBeUpdated;
        }
        
        if(woToBeUpdated!=null && woToBeUpdated.size()>0)
        {
            List<Simplex_Activity_Staging__c> activityList = new List<Simplex_Activity_Staging__c>();
            Database.saveResult[] SaveResultList = Database.update(woToBeUpdated,false);
            System.debug('SaveResultList'+SaveResultList);
            for(integer i =0; i<woToBeUpdated.size();i++){
                String msg='';
                WorkOrder woObj = woToBeUpdated[i];
                If(!SaveResultList[i].isSuccess()){
                    msg +='Error: "';        
                    for(Database.Error err: SaveResultList[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    } 
                    List<Simplex_Activity_Staging__c> lstobjWoStaging = mWoToStagingList.get(woObj.Client_Work_Order_Number__c);
                    for(Simplex_Activity_Staging__c objWoStaging: lstobjWoStaging){
                        objWoStaging.Sub_Status_Update_Error__c = msg;
                        ObjWoStaging.IsProcessed__c = true;
                        activityList.add(ObjWoStaging);
                    }
                    
                }
                else{
                    List<Simplex_Activity_Staging__c> lstobjWoStaging = mWoToStagingList.get(woObj.Client_Work_Order_Number__c);
                    for(Simplex_Activity_Staging__c objWoStaging: lstobjWoStaging){
                        ObjWoStaging.IsSubStatusUpdateSuccess__c = true;
                        ObjWoStaging.IsProcessed__c = true;
                        activityList.add(ObjWoStaging);
                    }
                    
                }
                
            } 
            Update activityList;
        }
        /*
        if(lstLIToBeUpdated!=null && lstLIToBeUpdated.size()>0){
            Update lstLIToBeUpdated;
        }
        */
        if(casesToBeInserted!=null && casesToBeInserted.size()>0){
            Insert casesToBeInserted;
        }
        if(lstPaymentsToBeUpdated!=null && lstPaymentsToBeUpdated.size()>0){
            Update lstPaymentsToBeUpdated;
        }
        if(lstInvoicesToBeUpdated!=null && lstInvoicesToBeUpdated.size()>0){
            Update lstInvoicesToBeUpdated;
        }
        
    }
    global void finish(Database.BatchableContext bcMain){
        
    }
}