/**
 * @File Name          : ActionAccountToBSAHandler.cls
 * @Description        : Generic PE's handler. This is instantiated from PlatformEventSubscriptionFactory 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 03-15-2021
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    23/07/2020   IBM               Initial Version
**/

public  class ActionAccountToBSAHandler extends PlatformEventSubscriptionBaseHandler{
    
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private sObjectType mPESobjectType;
    private IDTO mDTOClass;
    private ClientDTO mBaseDTO;
    // Sydney UNU DTO 
    // private SydneyUNIDTO mBaseDTO;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    private String eventActionName;
    private string accountcode;
    private String contractNumber;
    
    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
       
        for(sObject sObj : peToProcessList){
            Action_Account_To_BSA__e event = (Action_Account_To_BSA__e)sObj;
            eventActionName = event.action__c;
            accountcode = event.AccountCode__c;
            contractNumber = event.ContractNumber__c;
            deserializeJSON(event);
            
           // setupDeserializedDataForMapping();
            setupAdditonalSettings(event);
            
        }
        super.process(peSobjectType,peToProcessList, mappedSobjectType);        
    }


    private void setupAdditonalSettings(Action_Account_To_BSA__e event){

        mIdentifierMapping = new Map<string,String>();
        mIdentifierMapping.put('AccountCode__c',event.AccountCode__c);  
        mIdentifierMapping.put('ContractNumber__c',event.ContractNumber__c);  
        mIdentifierMapping.put('Action_Name__c',event.Action__c);  
        //mAdditionalWhereClause = 'PE_Subscription_Object_Action_Mapping__r.Action_Name__c = \''+event.Action__c+'\'';            
        setAdditionalMappingFilters('Action_Name__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);

    }

    private void deserializeJSON(Action_Account_To_BSA__e event){
     
        String jsonString = '';
        if(String.isNotBlank(event.Account__c)){
            String tempString  = ((event.Account__c).removeEnd('}')) + ',"correlationId":"'+event.CorrelationId__c+'"},';
            // System.debug('Account JSON'+tempString);
            jsonString = jsonString + tempString;
        }
       
        if(String.isNotBlank(event.Contact__c)){

            jsonString = jsonString + event.Contact__c+',';
        }
        boolean singlecontact = false;
        if (jsonstring.contains('"contactDetails":{')){
          jsonstring =   jsonstring.replace('"contactDetails":{','"contactDetails":[{');
          singlecontact = true;
        }
        if (singlecontact){
            jsonstring = '{'+jsonString.removeEnd(',')+']}';
        }else {
              jsonstring = '{'+jsonString.removeEnd(',')+'}';
        }
        // System.debug('BuildJson '+ jsonstring);
         for (Integer i = 0; i < jsonstring.length(); i=i+300) {
            Integer iEffectiveEnd = (i+300 > (jsonstring.length()-1) ? jsonstring.length()-1 : i+300);
            System.debug(jsonstring.substring(i,iEffectiveEnd));
          }
        if (event.Action__c == 'CreateClient' || event.Action__c == 'CreateSite'  ){
            mSpecificDTOClassName ='ClientDTO';
        }
        // System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        if(mSpecificDTOClassName.equalsIgnoreCase('ClientDTO')){
            ClientDTO newclientdto = new ClientDTO();
            mBaseDTO = newclientdto.parse(jsonstring);
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
        }
     
                
        // System.debug('Deserilize class'+mBaseDTO);
        // System.debug('Serialized class'+mSerializedData);
       
    }

   
    public override void processBuinessLogic(){
        // System.debug('!!!! Mahmood processBuinessLogic !!!! ');
        List<sObject> finalsObjectList = getMappedObjectList();
        List<sobject> finalListsAccount = new List<sobject>();
        List<Contact> finalListContact = new List<Contact>();
        
        String accExternalField = getMappedExternalIdFromNode('accountDetails',false);
        String contactExternalField = getMappedExternalIdFromNode('contactDetails',false);
        
        
        String locationExternalIdValue = '';
        String accExternalIdValue = '';
        String accCorrelationId = '';
        
        for(sObject obj: finalsObjectList){
          if(String.valueOf(obj.getSObjectType()) == 'Account'){
                finalListsAccount.add(obj);
                accExternalIdValue = (String)((Account)obj).get('CustomerCode__c');
                accCorrelationId = (String)((Account)obj).get('CorrelationId__c');
            }else if(String.valueOf(obj.getSObjectType()) == 'Contact'){
                finalListContact.add((Contact)obj);
            }
        }
        List<Account> finalListAccount = new List<Account>(); 
        if(finalListsAccount.size() > 0){
            
            SObjectField accRefId = getSObjectFieldName('Account', accExternalField);
          
            Set<Id> successAccIdSet = new Set<Id>();
            // system.debug('!!!!Mahmood Before finalListsAccount' + finalListsAccount);
            string accownerid = null;
            string accparentId = null;
            string accServiceTerritory = null;
            string accmarkupKey = null;
            string accpriceBook = null;
        
                for(integer i= 0; i < finalListsAccount.size(); i++){
                  
                      
                    //if (finalListsAccount[i].get('OwnerId') != null && finalListsAccount[i].get('OwnerId')  != ''){
                    //    accownerid =  (String)finalListsAccount[i].get('OwnerId');
                    //  finalListsAccount[i].put('OwnerId',null);
                    //}
                    if ( finalListsAccount[i].get('ParentId') != null && finalListsAccount[i].get('ParentId') != ''){
                        accparentId = (String)finalListsAccount[i].get('ParentId');
                        if (eventActionName =='CreateClient'){
                            finalListsAccount[i].put('GLCode__c',accparentId);
                        }
                        string acccustomercode = (string)finalListsAccount[i].get('CustomerCode__c');
                        if (acccustomercode == accparentId ){
                            accparentId = null;
                        }
                        finalListsAccount[i].put('ParentId',null);
                    }
                    if ( finalListsAccount[i].get('Service_Territory__c') != null && finalListsAccount[i].get('Service_Territory__c') != ''){
                        accServiceTerritory =  (String)finalListsAccount[i].get('Service_Territory__c');
                        finalListsAccount[i].put('Service_Territory__c',null);
                    }
                    if ( finalListsAccount[i].get('Markup_Code__c') != null && finalListsAccount[i].get('Markup_Code__c') != ''){
                        accmarkupKey =  (String)finalListsAccount[i].get('Markup_Code__c');
                        finalListsAccount[i].put('Markup_Code__c',null);
                    } 
                                        
                    if (finalListsAccount[i].get('Price_Book__c') != null && finalListsAccount[i].get('Price_Book__c') != ''){
                        accpriceBook =  (String)finalListsAccount[i].get('Price_Book__c');
                        finalListsAccount[i].put('Price_Book__c',null);
                    }                    

                    if (eventActionName =='CreateSite'){
                        finalListsAccount[i].put('CustomerCode__c',contractNumber);
                        finalListsAccount[i].put('Contract_Number__c',contractNumber);
                        finalListsAccount[i].put('AccountNumber', accountcode );
                    }
                    if (eventActionName =='CreateClient'){
                        finalListsAccount[i].put('AccountNumber', accountcode );
                    }
                        
                    Map<String, Object> acctMap1 = new Map<String, Object>( finalListsAccount[i].getPopulatedFieldsAsMap() );
                    acctMap1.remove('Service_Territory__c');
                    acctMap1.remove('ParentId');
                   // acctMap1.remove('OwnerId');

                   if (accmarkupKey != null && accmarkupKey != ''){
                    acctMap1.remove('Markup_Code__c');
                    } else {
                        Id MUpId = null;
                        finalListsAccount[i].put('Markup_Code__c',MUpId);
                    }

                    if(accpriceBook != null && accpriceBook != ''){   
                        acctMap1.remove('Price_Book__c');
                    }
                    else{                        
                        Id pcBookId = null;
                        finalListsAccount[i].put('Price_Book__c',pcBookId);
                    }
                    Account accObj =  (Account) JSON.deserialize( JSON.serialize( acctMap1 ), Account.class );
                   // if (accownerid != null){
                       
                   // }
                    if (accparentId != null){
                        String parentIdRelationshipName = Account.ParentId.getDescribe().getRelationshipName();
                        Account accountsObject = new Account();
                        accountsObject.CustomerCode__c = accparentId;
                        accObj.putSObject(parentIdRelationshipName, accountsObject);   
                    }
                    if (accServiceTerritory != null){
                        String serviceTerrRelName = Account.Service_Territory__c.getDescribe().getRelationshipName();
                        ServiceTerritory ServiceTerritorysObj =  new ServiceTerritory() ;
                        ServiceTerritorysObj.Pronto_ID__c =  accServiceTerritory;
                        accObj.putSObject(serviceTerrRelName, ServiceTerritorysObj);   
                    }
                    if (accmarkupKey != null && accmarkupKey != ''){
                        String markupcodeRelName = Account.Markup_Code__c.getDescribe().getRelationshipName();
                        Markup_Code__c mcObj =  new Markup_Code__c() ;
                        mcObj.External_ID__c =  accmarkupKey;
                        accObj.putSObject(markupcodeRelName, mcObj);   
                    }                   
                    
                    if(accpriceBook != null && accpriceBook != ''){    
                        String priceBookRelName = Account.Price_Book__c.getDescribe().getRelationshipName();  
                        Pricebook2 pcBookVal = new Pricebook2();                  
                        pcBookVal.Name = accpriceBook;  
                        accObj.putSObject(priceBookRelName, pcBookVal);                      
                    }                                                                            
                    
                    finalListAccount.add(accObj);
                }
            
            // system.debug('!!!!Mahmood After finalListAccount' + finalListAccount);
            Database.UpsertResult[] upsertResult = Database.upsert(finalListAccount, accRefId , false);
            // System.debug('DildarLog: upsertResult Account - ' + upsertResult);    
            successAccIdSet = UtilityClass.readUpsertResults(upsertResult, JSON.serialize(finalListAccount), 'Account', accCorrelationId, eventActionName,accExternalIdValue );
                  
            
            //Save related child records
            if(successAccIdSet != null && successAccIdSet.size() > 0){
                saveMappedContactWithAccount(finalListContact, accRefId, accExternalIdValue, contactExternalField, accCorrelationId);
               
             }
            
        }
    }
   
   
    private void saveMappedContactWithAccount(List<contact> contactList, SObjectField accRefId, String accExternalIdValue, String contactExternalField, String accCorrelationId){
        
        SObjectField contactRefId = getSObjectFieldName('Contact', contactExternalField);
        
        // System.debug('DildarLog: saveMappedContactWithWO - ContactList - '+contactList);
        
        if(contactList != null && contactList.size() > 0){
            for(Contact contactRecord: contactList){
                String parentAccountRelName = Contact.AccountId.getDescribe().getRelationshipName();
                Account accparentObj = new Account();
                if (eventActionName =='CreateClient'){
                    accparentObj.CustomerCode__c = accountcode ;
                } else if (eventActionName =='CreateSite') {
                    accparentObj.CustomerCode__c = contractNumber;
                }
               contactRecord.putSObject(parentAccountRelName, accparentObj);                                
            }
            Database.UpsertResult[] upsertContactResult = Database.upsert(contactList, contactRefId, false);
            UtilityClass.readUpsertResults(upsertContactResult, JSON.serialize(contactList), 'contact', accCorrelationId, eventActionName, accExternalIdValue);
            // System.debug('DildarLog: upsertContactResult - ' + upsertContactResult);             
        }

    }
   
   
     
}