public class WSNBNFileInboundActionDTO  implements IDTOList{
	public String id {get;set;} 
	public RelatedEntity relatedEntity {get;set;} 
	public Metadata metadata {get;set;} 
	public Content content {get;set;} 
	
    public String getSpecificDTOClassName(){
        return '';
    }
    
    public WSNBNFileInboundActionDTO(){}
    
	public WSNBNFileInboundActionDTO(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'id') {
						id = parser.getText();
					} else if (text == 'relatedEntity') {
						relatedEntity = new RelatedEntity(parser);
					} else if (text == 'metadata') {
						metadata = new Metadata(parser);
					} else if (text == 'content') {
						content = new Content(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class RelatedEntity {
		public String id {get;set;} 
		public String type_z {get;set;} 

		public RelatedEntity(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'type_z') {
							type_z = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'RelatedEntity consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Content {
		public String presignedUrl {get;set;} 

		public Content(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'presignedUrl') {
							presignedUrl = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Content consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Metadata {
		public String objectType {get;set;} 
		public String id {get;set;} 
		public String source {get;set;} 
		public String notes {get;set;} 
		public String fileName {get;set;} 
		public String contentType {get;set;} 

		public Metadata(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'objectType') {
							objectType = parser.getText();
						} else if (text == 'id') {
							id = parser.getText();
						} else if (text == 'source') {
							source = parser.getText();
						} else if (text == 'notes') {
							notes = parser.getText();
						} else if (text == 'fileName') {
							fileName = parser.getText();
						} else if (text == 'contentType') {
							contentType = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Metadata consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static List<WSNBNFileInboundActionDTO> parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return arrayOfWSNBNFileInboundActionDTO(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	
    
    private static List<WSNBNFileInboundActionDTO> arrayOfWSNBNFileInboundActionDTO(System.JSONParser p) {
        List<WSNBNFileInboundActionDTO> res = new List<WSNBNFileInboundActionDTO>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new WSNBNFileInboundActionDTO(p));
        }
        return res;
    }

}