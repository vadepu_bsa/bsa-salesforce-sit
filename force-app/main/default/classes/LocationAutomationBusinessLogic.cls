/* Author: Abhijeet Anand
 * Date: 13 September, 2019
 * Apex class that handles all pre/post DML operations on the FSL object 'Location'
 */
 
 public class LocationAutomationBusinessLogic {

    // Method to associate a Location with a Service Resource based on the unique Pronto Service Resource ID post successfull insertion of a Location
    public static void linkServiceResourceToLocation (List<Schema.Location> locationList) {

        Map<String,Schema.Location> resourceLocationMap = new Map<String,Schema.Location>();
        List<ServiceResource> serviceResourceToUpdate = new List<ServiceResource>();

        System.debug('locationList: ' + locationList);

        for(Schema.Location obj : locationList) {
            if(obj.Pronto_Service_Resource_ID__c != null) {
                resourceLocationMap.put(obj.Pronto_Service_Resource_ID__c,obj); // Map to hold the key-value pair of Pronto Service Resource ID & Location sobject
            }
        }

        if(!resourceLocationMap.isEmpty()) {
            // Query the Service Resource with matching Pronto Service Resource IDs
            for(ServiceResource obj : [Select Id, LocationId, Pronto_Service_Resource_ID__c FROM ServiceResource WHERE Pronto_Service_Resource_ID__c IN : resourceLocationMap.keyset()]) {
                if(resourceLocationMap.containskey(obj.Pronto_Service_Resource_ID__c)) {
                    obj.LocationId = resourceLocationMap.get(obj.Pronto_Service_Resource_ID__c).Id; // associate Location with a Service Resource
                    serviceResourceToUpdate.add(obj);
                }
            }

            if(!serviceResourceToUpdate.isEmpty()) {
                update serviceResourceToUpdate; // update the ServiceResource list
            }
        }
        
    }

}