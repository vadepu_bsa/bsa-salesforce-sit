public class WSNBNWOOutboundActionDTR  {
    
    private String actionName;
    private sObject sObjectRecord;
    private String workOrderSpecification;
    private String currentTransactionCorrelationId;
    
    //WO Fields
    private Datetime interaction_datetime;
    private String specificationId;
    private String specificationVersion;
    private String pcr_code;
	private String closure_summary;
	private String pcr_group;
    private String clientWorkOrdrNumber;
    private String type_z;
    private String correlationId;
    private String eot_cause;
    private String eot_reason;
    private Datetime scheduled_start_date;
    private Datetime scheduled_end_date;
    private String delivery_confidence_level;
    private Datetime planned_remediation_date;
    private Boolean IsPCRUpdatedByNBN;
    private String prd_comments;
    
    //WO Contact Fields
    private String contactName;
    private String contactPhone;
    private String contactNotes;
	private String contactId;
    private String contactType;
    private Integer contactSequence;
        
    //Notes Fields
    private String noteSummary;
	private Datetime notes_datetime;
    private String notes_type;
    
    //Task Fields
    private String taskTitle;
    private String taskStatus;
    private String taskCode;
    private String taskId;    
    private Integer taskSequence;
    private List<Custom_Attribute__c> workOrderAttributeList = new List<Custom_Attribute__c>();
    
    //WOLI Fields
    private String documentComplete;
    
    private String Id;
    
    private WSNBNWOOutboundActionDTO.Specification specification;
    private List<WSNBNWOOutboundActionDTO.input_data_bindings> inputBindingsList;
    private WSNBNWOOutboundActionDTO payloadObj;
    
    private WSNBNWOOutboundActionDTO.input_data_bindings inp1;
    
    public WSNBNWOOutboundActionDTR(String actionName, sObject sObjectRecord,String workOrderSpecification){
        
        this.actionName = actionName;
        this.sObjectRecord = sObjectRecord;
        this.workOrderSpecification = workOrderSpecification;
        
        if(workOrderSpecification == 'GeneralWOS'){
            type_z = 'General';            
        }else if(workOrderSpecification == 'GreenfieldPreInstallWOS'){
            type_z = 'GreenfieldPreInstall';            
        }else if(workOrderSpecification == 'RemediationWOS'){
            type_z = 'Remediation';
        }
    }
    
    public void initiateWOActions(){ 
        
        //making process specific to specifications mentioned above.
        if(type_z == null){
            System.debug('ERROR@ Provided specification <'+ workOrderSpecification +'> is not configured to process.');
            return;
        }
        
		generateCommonPayload(); 
        if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_SUSPEND) || actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_ACCEPT) ||  actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_ENROUTE) || actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_ONSITE)){
            generateAcceptEnrouteOnsiteActionBody();            
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_COMPLETE) || actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_INCOMPLETE)){
            generateCompleteInCompleteActionBody();            
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_UPDATE_SCHEDULE)){
            generateUpdateScheduledActionBody();            
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_ADD_CONTACT) || actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_UPDATE_CONTACT)){
            generateAddUpdateContactActionBody();            
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_ADD_NOTES) || actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_UPDATE_NOTES)){
            generateAddUpdateNotesActionBody();
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_UPDATE_CONFIDENCE_LEVEL)){
            generateUpdateConfidenceLevelActionBody();
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_UPDATE_PRD)){
            generateUpdatePRDActionBody();
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_UPDATE_TASK)){
            generateUpdateTaskActionBody();
        }else if(actionName.equalsIgnoreCase(BSA_ConstantsUtility.ACTION_DOC_COMPLETE)){
            generateDocumentCompleteActionBody();
        }        
        System.debug('DildarLog: payloadObj - ' + payloadObj);
        if(payloadObj != null)
        	publishEvents(getHeaderString(), (payloadObj.getPayload()).replace('type_Z','type'), Label.WS_NBN_WO_Outbound_Action_URL, 'post', currentTransactionCorrelationId);
    }
     
    private void generateAcceptEnrouteOnsiteActionBody(){
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);        
    }
    
    private void generateUpdateScheduledActionBody(){   
        
		WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('eot_cause',eot_cause);
        WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('eot_reason',eot_reason);
        WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('scheduled_start_date',getConvertedDatetimeGMT(scheduled_start_date));
        WSNBNWOOutboundActionDTO.input_data_bindings inp5 = new WSNBNWOOutboundActionDTO.input_data_bindings('scheduled_end_date',getConvertedDatetimeGMT(scheduled_end_date));
        WSNBNWOOutboundActionDTO.input_data_bindings inp6 = new WSNBNWOOutboundActionDTO.input_data_bindings('delivery_confidence_level',delivery_confidence_level);
        inputBindingsList.add(inp2);
        inputBindingsList.add(inp3);
        inputBindingsList.add(inp4);
        inputBindingsList.add(inp5);
        inputBindingsList.add(inp6);
 
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);
    }
    
    private void generateUpdatePRDActionBody(){   
        
		WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('eot_cause',eot_cause);
        WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('eot_reason',eot_reason);
        WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('planned_remediation_date',getConvertedDatetimeGMT(planned_remediation_date));
        WSNBNWOOutboundActionDTO.input_data_bindings inp5 = new WSNBNWOOutboundActionDTO.input_data_bindings('delivery_confidence_level',delivery_confidence_level);
        inputBindingsList.add(inp2);
        inputBindingsList.add(inp3);
        inputBindingsList.add(inp4);
        inputBindingsList.add(inp5);
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);
    }
    
    private void generateCompleteInCompleteActionBody(){
        
        System.debug('SAYALI DEBUG:UserInfo.getFirstName()-->'+UserInfo.getFirstName());
        if(UserInfo.getFirstName() != 'Automated' && !IsPCRUpdatedByNBN){
            WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('pcr_code',pcr_code);
            WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('pcr_group',pcr_group);
            WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('closure_summary',closure_summary);
            inputBindingsList.add(inp2);
        	inputBindingsList.add(inp3);
        	inputBindingsList.add(inp4);
        }
        /*else
        {
            WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('pcr_code','');
            WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('pcr_group','');
            WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('closure_summary','');
            inputBindingsList.add(inp2);
        	inputBindingsList.add(inp3);
        	inputBindingsList.add(inp4);
        
        }*/
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);        
    }
    
    private void generateAddUpdateContactActionBody(){
        
       WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('contact_name_'+contactSequence,contactName);
        WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('contact_number_'+contactSequence,contactPhone);
        WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('contact_type_'+contactSequence,contactType);
        inputBindingsList.add(inp2);
        inputBindingsList.add(inp3);
        inputBindingsList.add(inp4);
        if(contactNotes!=null && contactNotes!=''){
        	WSNBNWOOutboundActionDTO.input_data_bindings inp5 = new WSNBNWOOutboundActionDTO.input_data_bindings('contact_notes_'+contactSequence,contactNotes);
            inputBindingsList.add(inp5);
        }
        //WSNBNWOOutboundActionDTO.input_data_bindings inp6 = new WSNBNWOOutboundActionDTO.input_data_bindings('contact_id_'+contactSequence,contactId);
        //inputBindingsList.add(inp6);
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);           
    }
    
    private void generateUpdateTaskActionBody(){
        
        WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('task_id_'+taskSequence,taskId);
        WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('task_code_'+taskSequence,taskCode);
        WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('task_status_'+taskSequence,taskStatus);
        inputBindingsList.add(inp2);
        inputBindingsList.add(inp3);
        inputBindingsList.add(inp4);

        if(workOrderAttributeList.size() > 0 && workOrderAttributeList != null){
            
            System.debug('DildarLog: workOrderAttributeList - ' + workOrderAttributeList);
            
            //Integer count = 1;
            for(Custom_Attribute__c woca: workOrderAttributeList){
                WSNBNWOOutboundActionDTO.input_data_bindings nameValAtt = new WSNBNWOOutboundActionDTO.input_data_bindings(woca.Field_Id__c+'_'+taskSequence,woca.Value__c);                
                inputBindingsList.add(nameValAtt);
                //count++;                
            }
            
        }        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);        
    }
    
    private void generateAddUpdateNotesActionBody(){
        
        WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('notes_type',notes_type);
        WSNBNWOOutboundActionDTO.input_data_bindings inp3 = new WSNBNWOOutboundActionDTO.input_data_bindings('notes_datetime',getConvertedDatetimeGMT(notes_datetime));
        WSNBNWOOutboundActionDTO.input_data_bindings inp4 = new WSNBNWOOutboundActionDTO.input_data_bindings('notes',noteSummary);
        inputBindingsList.add(inp2);
        inputBindingsList.add(inp3);
        inputBindingsList.add(inp4);
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);        
    }
    
    private void generateUpdateConfidenceLevelActionBody(){
        
        WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('delivery_confidence_level',delivery_confidence_level);
        inputBindingsList.add(inp2);
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);        
    }
    
    private void generateDocumentCompleteActionBody(){
        
        WSNBNWOOutboundActionDTO.input_data_bindings inp2 = new WSNBNWOOutboundActionDTO.input_data_bindings('documentation_complete', documentComplete);
        inputBindingsList.add(inp2);
        
        payloadObj = new WSNBNWOOutboundActionDTO(clientWorkOrdrNumber,type_z,specification,actionName,inputBindingsList);        
    }
    
    private void generateCommonPayload(){
        currentTransactionCorrelationId = UtilityClass.getUUID();
        specification = new WSNBNWOOutboundActionDTO.Specification(specificationId,specificationVersion);
        inp1 = new WSNBNWOOutboundActionDTO.input_data_bindings('interaction_datetime',getConvertedDatetimeGMT(System.now()));
        inputBindingsList = new List<WSNBNWOOutboundActionDTO.input_data_bindings>();
        inputBindingsList.add(inp1);
    } 
    public void fetchWOLIRecords(String Id){
        WorkOrderLineItem objLineItem =[Select id,WorkOrder.ParentWorkOrder.NBN_Field_Work_Specified_By_Id__c,WorkOrder.ParentWorkOrder.NBN_Field_Work_Specified_By_Version__c,
                                       WorkOrder.ParentWorkOrder.CorrelationID__c,Documentation_Completed__c,WorkOrder.ParentWorkOrder.Client_Work_Order_Number__c from WorkOrderLineItem where id=: Id];
        specificationId = objLineItem.WorkOrder.ParentWorkOrder.NBN_Field_Work_Specified_By_Id__c;
        specificationVersion = objLineItem.WorkOrder.ParentWorkOrder.NBN_Field_Work_Specified_By_Version__c;
        correlationId = objLineItem.WorkOrder.ParentWorkOrder.CorrelationID__c;
        clientWorkOrdrNumber = objLineItem.WorkOrder.ParentWorkOrder.Client_Work_Order_Number__c;
        if(objLineItem.Documentation_Completed__c){
        	documentComplete = 'true';
        }
        else{
            documentComplete = 'false';
        }
    }
    public void fetchWORecords(String Id){
        WorkOrder wo = [Select Id, eot_cause__c, PRD_Comments__c, planned_remediation_date__c, IsPCRUpdatedByNBN__c, PCR_Group__c, Closure_Summary__c, eot_reason__c, startdate, enddate, delivery_confidence_level__c, CorrelationID__c, NBN_Activity_Start_Date_Time__c, Reason_Code__c, NBN_Field_Work_Specified_By_Id__c, NBN_Field_Work_Specified_By_Version__c, ParentWorkOrder.Client_Work_Order_Number__c, Client_Work_Order_Number__c from workorder where Id =: Id];
        specificationId = wo.NBN_Field_Work_Specified_By_Id__c;
        specificationVersion = wo.NBN_Field_Work_Specified_By_Version__c;
        pcr_code = wo.Reason_Code__c;
        closure_summary = wo.Closure_Summary__c;
        pcr_group = wo.PCR_Group__c;
        correlationId = wo.CorrelationID__c;
        
    	
    	scheduled_start_date = wo.StartDate;
    	scheduled_end_date = wo.EndDate;
    	if(wo.delivery_confidence_level__c!=null && wo.delivery_confidence_level__c!=''){
    		delivery_confidence_level = wo.delivery_confidence_level__c;
        }
        else{
            delivery_confidence_level = 'Green';
        }
        if(wo.eot_reason__c!=null && wo.eot_reason__c!=''){
    		eot_reason = wo.eot_reason__c; 
        }
        else{
            //eot_reason = Label.NBN_EOT_Reason;
            eot_reason = '';
        }
        if(wo.eot_cause__c!=null && wo.eot_cause__c!=''){
    		eot_cause = wo.eot_cause__c;
        }
        else{
            //eot_cause = Label.NBN_EOT_Cause;
            eot_cause = '';
        }
        clientWorkOrdrNumber = wo.Client_Work_Order_Number__c != '' && wo.Client_Work_Order_Number__c != null ? wo.Client_Work_Order_Number__c : wo.ParentWorkOrder.Client_Work_Order_Number__c;
        planned_remediation_date = wo.planned_remediation_date__c;
        IsPCRUpdatedByNBN = wo.IsPCRUpdatedByNBN__c;
        prd_comments = wo.PRD_Comments__c;
    }
    
    public void fetchWOCRecords(String Id){
        Work_Order_Contact__c woc = [Select Id, NBN_Contact_Id__c, Contact_Count__c, Phone__c, Notes__c, External_Contact_Id__c, Type__c, Name__c, Work_Order__r.Client_Work_Order_Number__c, Work_Order__r.CorrelationID__c, Work_Order__r.NBN_Activity_Start_Date_Time__c, Work_Order__r.NBN_Field_Work_Specified_By_Id__c, Work_Order__r.NBN_Field_Work_Specified_By_Version__c from work_order_Contact__c where Id =: Id];
        specificationId = woc.Work_Order__r.NBN_Field_Work_Specified_By_Id__c;
        specificationVersion = woc.Work_Order__r.NBN_Field_Work_Specified_By_Version__c;
        correlationId = woc.Work_Order__r.CorrelationID__c;
        contactName = woc.Name__c;
        clientWorkOrdrNumber = woc.Work_Order__r.Client_Work_Order_Number__c;
        contactPhone = woc.Phone__c;
        if(contactPhone!=null && contactPhone!='' &&  !contactPhone.startsWithIgnoreCase('+')){
            if(contactPhone.startsWithIgnoreCase('0')){
                    contactPhone = contactPhone.removeStart('0');
            }
            contactPhone ='+61'+ contactPhone;
        }
        contactNotes = woc.Notes__c; 
        contactId = woc.NBN_Contact_Id__c != '' && woc.NBN_Contact_Id__c != null ? woc.NBN_Contact_Id__c : woc.Id;
        contactType = woc.Type__c;
        contactSequence = 1;//(Integer)woc.Contact_Count__c;
	}
    
    public void fetchNotesRecords(String Id){
        NBN_Log_Entry__c note = [Select Id, Source__c, Summary__c, NBN_Field_Work_Note_Description__c, Field_Work_Note_Date_Time__c, Field_Work_Note_Type__c, Work_Order__r.ParentWorkOrder.Client_Work_Order_Number__c, Work_Order__r.CorrelationID__c, Work_Order__r.NBN_Activity_Start_Date_Time__c, Work_Order__r.NBN_Field_Work_Specified_By_Id__c, Work_Order__r.NBN_Field_Work_Specified_By_Version__c from NBN_Log_Entry__c where Id =: Id];        
        specificationId = note.Work_Order__r.NBN_Field_Work_Specified_By_Id__c;
        specificationVersion = note.Work_Order__r.NBN_Field_Work_Specified_By_Version__c;
        correlationId = note.Work_Order__r.CorrelationID__c;
        noteSummary = note.Source__c == BSA_ConstantsUtility.NBN_NOTE_SOURCE_UPDATE_FOR_NBN ? note.NBN_Field_Work_Note_Description__c : note.Summary__c;
        clientWorkOrdrNumber = note.Work_Order__r.ParentWorkOrder.Client_Work_Order_Number__c;
        notes_datetime = note.Field_Work_Note_Date_Time__c;
        notes_type = note.Field_Work_Note_Type__c != null && note.Field_Work_Note_Type__c != '' ? note.Field_Work_Note_Type__c : Label.NBN_Unify_Default_Note_Type;
    }
    
    public void fetchTaskRecords(String Id){
        Work_Order_Task__c wot = [Select Id, Task_Count__c, Code__c, Status__c, Name, Work_Order__r.Client_Work_Order_Number__c, Work_Order__r.CorrelationID__c, Work_Order__r.NBN_Field_Work_Specified_By_Id__c, Work_Order__r.NBN_Field_Work_Specified_By_Version__c from Work_Order_Task__c where Id =: Id];
        specificationId = wot.Work_Order__r.NBN_Field_Work_Specified_By_Id__c;
        specificationVersion = wot.Work_Order__r.NBN_Field_Work_Specified_By_Version__c;
        correlationId = wot.Work_Order__r.CorrelationID__c;
        clientWorkOrdrNumber = wot.Work_Order__r.Client_Work_Order_Number__c;
        taskCode = wot.Code__c;
        taskId = wot.Name;
        taskStatus = wot.Status__c;
        taskSequence = (Integer)wot.Task_Count__c;
        System.debug('DildarLog: TaskId - ' + Id);
        for(Custom_Attribute__c woca : [Select Id, Name__c,Field_Id__c, Value__c, Work_Order_Task__c From Custom_Attribute__c Where Work_Order_Task__c =: Id]){            
            workOrderAttributeList.add(woca);
        }
    }        
    
    private String getHeaderString(){
        /*List<String> parameters = new List<String>();		
        parameters.add(UtilityClass.getUUID());        
        parameters.add(correlationId);
        String header = String.format(Label.Get_WO_Outbound_Header, parameters);
        System.debug('DildarLog: Header - ' + header);*/
        
        String header = Label.Get_WO_Outbound_Header;
        header = header.replace('{0}',currentTransactionCorrelationId);
        header = header.replace('{1}', correlationId);
        
        List<String> headerList = header.split(';');
        Map<String, String> headerMap = new Map<String, String>();
        for(String headerItem : headerList){
            headerMap.put(headerItem.split(':')[0], headerItem.split(':')[1]);
        }
        
        if(headerMap != null)
        	return json.serialize(headerMap);
        else
            return '';
    }
    
    public static void publishEvents(String requestHeader ,String requestBody ,String requestUrl, String requestMethod, String currentTransactionCorrelationId){ 
       	Action_B2B_From_BSA__e woOutboundEvent = new Action_B2B_From_BSA__e();
        woOutboundEvent.RequestURL__c = requestUrl;
        woOutboundEvent.RequestHeader__c = requestHeader;
        woOutboundEvent.RequestBody__c = requestBody;
        woOutboundEvent.RequestMethod__c = requestMethod;
        woOutboundEvent.CorrelationId__c = currentTransactionCorrelationId;
        
        System.debug('DildarLog: WSOutboundActionBasePayload- URL: ' + requestUrl + ', Header: ' + requestHeader);
        System.debug('DildarLog: WSOutboundActionBasePayload- Body: ' + requestBody);
        List<sObject> sobj = new List<sObject>{woOutboundEvent};
		ApplicationLogUtility.publishEvents(sobj);
        
    }
    
    private String getConvertedDatetimeGMT(Datetime dtt){
        if(dtt != null){
            Datetime dt = dtt;
            //String formatedDt = dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'\'');
            //String formatedDt = dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');
            String formatedDt = dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            return formatedDt;
        }else{
            return null;
        }        
    }
}