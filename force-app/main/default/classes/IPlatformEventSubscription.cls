/**
 * @File Name          : IPlatformEventSubscription.cls
 * @Description        : Interface for Platform Event Subscription
 * @Author             : Karan Shekhar
 * @Group              : 
 * @Last Modified By   : Karan Shekhar
 * @Last Modified On   : 04/05/2020, 9:20:10 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/05/2020   Karan Shekhar     Initial Version
**/
public interface IPlatformEventSubscription {

	/**
	* @description 
	* @author Karan Shekhar | 04/05/2020 
	* @param peSobjectType:  Sobject Type of Platform Event
	* @param peToProcessList : PE list to process
	* @param mappedSobjectType : sObjectType of Mapped object
	* @param ; 
	* @return void 
	**/
	void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType);
}