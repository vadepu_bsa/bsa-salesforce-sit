global class BatchUpsertSimplexSOR Implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
	global String strQuery;
    public List<String> batchException = new List<String>();
    global Set<Id> successWOSet = new Set<Id>();
    global String simplexWORTN = 'SOR_Activities';
    
    //Initialize the query string
    global BatchUpsertSimplexSOR(){
        String queryFields = 'Work_Order_Id__c,'+
            'Work_Order_Lines_Description__c,'+
            'SOR_Id__c,'+
            'SOR_Quantity__c,'+
            'Task_Id__c,'+
            'Task_Approval_Required__c,'+
            'Task_Classification__c,'+
            'Task_Code__c,'+
            'Task_Comments__c,'+
            'Task_Length__c,'+
            'Task_Quantity__c,'+
            'Task_Status__c,'+
            'Task_Title__c,'+
            'SAM_Id__c';
        strQuery = 'SELECT '+queryFields+' from Simplex_Activity_Staging__c Where RecordType.DeveloperName =:simplexWORTN AND IsProcessed__c = false AND SOR_Id__c !=\'\' ';        
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Simplex_Activity_Staging__c> stagingRecordList){
        Id simplexWORecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Simplex_CUI_Work_Order').getRecordTypeId();

        //Store all success and failure on staging
        Map<String, Simplex_Activity_Staging__c> activityStagingMap = new Map<String, Simplex_Activity_Staging__c>();
        
        Set<String> workOrderClientIdSet = new Set<String>();
        
        //Store all task records
        Map<String, Set<Work_Order_Task__c>> workOrderTaskMap = new Map<String, Set<Work_Order_Task__c>>();
        
        //Store all sor records against workorder
        Map<String, Set<Line_Item__c>> workOrderLineItemMap = new Map<String, Set<Line_Item__c>>();
        
        //Store all tasks uniquely
        Set<Work_Order_Task__c> workOrderTaskSet = new Set<Work_Order_Task__c>();
        
        //Store all SOR Codes uniquely
        Set<String> sorCodeSet = new Set<String>();
        
        //Create sets to store unique data from the staging records
        for(Simplex_Activity_Staging__c stagingRecord:stagingRecordList){
            workOrderClientIdSet.add(stagingRecord.Work_Order_Id__c);
            sorCodeSet.add(stagingRecord.SOR_Id__c);
        }
        
        //System.debug('DildarLog: workOrderClientIdSet - ' + workOrderClientIdSet);
        //System.debug('DildarLog: sorCodeSet - ' + sorCodeSet);
        
        Map<String, PriceBookEntry> SORPriceBookEntryMap = new Map<String, PriceBookEntry>();
        for(PriceBookEntry pbe: [Select Id, Pricebook2Id, BSA_Cost__c, Tech_Cost__c, UnitPrice, ProductCode From PriceBookEntry Where Task_Code_Formula__c != '' AND ProductCode IN: sorCodeSet]){
        //for(PriceBookEntry pbe: [Select Id, Pricebook2Id, BSA_Cost__c, Tech_Cost__c, UnitPrice, ProductCode From PriceBookEntry Where ProductCode IN: sorCodeSet]){
			SORPriceBookEntryMap.put(pbe.ProductCode+'-'+pbe.Pricebook2Id, pbe);
    	}
        
        
        //Prepare client work order number and SF simplex work order map 
        Map<String, WorkOrder> simplexWOMap = new Map<String, WorkOrder>();
        if(Label.UnifySimplexBatchAdjustmentQueryActivation == 'Yes'){
            for(WorkOrder WO:[Select Id, Client_Work_Order_Number__c, Service_Resource__c, WorkTypeId, Pricebook2Id From WorkOrder Where Client_Work_Order_Number__c IN : workOrderClientIdSet AND (RecordType.DeveloperName = 'Simplex_CUI_Work_Order' OR Pricebook2.name Like '%Simplex%')]){
                simplexWOMap.put(wo.Client_Work_Order_Number__c, WO);            
            }            
        }else{
            for(WorkOrder WO:[Select Id, Client_Work_Order_Number__c, Service_Resource__c, WorkTypeId, Pricebook2Id From WorkOrder Where Client_Work_Order_Number__c IN : workOrderClientIdSet AND RecordType.DeveloperName = 'Simplex_CUI_Work_Order']){
                simplexWOMap.put(wo.Client_Work_Order_Number__c, WO);            
            }
		}
       	
        /*Map<String, Invoice__c> woInvoiceMap = new Map<String, Invoice__c>();
        for(Invoice__c invRecord: [Select Id, BSA_Total__c, Invoice_External_Id__c, Work_Order__r.Client_Work_Order_Number__c from Invoice__c where Work_Order__c IN: simplexWOMap.keySet()]){
            woInvoiceMap.put(invRecord.Work_Order__r.Client_Work_Order_Number__c, invRecord);
        }
        
        Map<String, Payment__c> woPaymentMap = new Map<String, Payment__c>();
        for(Payment__c payRecord: [Select Id, Amount__c, Payment_External_Id__c, Work_Order__r.Client_Work_Order_Number__c from Payment__c where Work_Order__c IN: simplexWOMap.keySet()]){
            woPaymentMap.put(payRecord.Work_Order__r.Client_Work_Order_Number__c, payRecord);
        }
        
        System.debug('DildarLog: woInvoiceMap.size - ' + woInvoiceMap);
        System.debug('DildarLog: woPaymentMap.size - ' + woPaymentMap);*/
        //System.debug('DildarLog: simplexWOMap.size - ' + simplexWOMap.size());
        
        for(Simplex_Activity_Staging__c stagingRecord:stagingRecordList){
            
            if(simplexWOMap.get(stagingRecord.Work_Order_Id__c) != null){
                Id simplexWORecordId = (simplexWOMap.get(stagingRecord.Work_Order_Id__c)).Id;
                Work_Order_Task__c newWOTask = new Work_Order_Task__c();
                newWOTask.Code__c = stagingRecord.Task_Code__c;
                newWOTask.Client_Task_Id__c = stagingRecord.Work_Order_Id__c +'-'+stagingRecord.Task_Id__c;
                newWOTask.Title__c = stagingRecord.Task_Title__c;
                newWOTask.Status__c = stagingRecord.Task_Status__c;
                newWOTask.Work_Order__c = simplexWORecordId;
                newWOTask.Work_Order_Task_Id__c = stagingRecord.Task_Id__c;
                newWOTask.Activity_Staging_Ref_Id__c = stagingRecord.Id;
                newWOTask.Approval_Required__c = stagingRecord.Task_Approval_Required__c;
                newWOTask.Description__c = stagingRecord.Task_Comments__c;
                //newWOTask.Quantity__c = Decimal.valueOf((stagingRecord.Task_Quantity__c!=null)?stagingRecord.Task_Quantity__c:'0');
                newWOTask.Quantity__c = Decimal.valueOf(stagingRecord.Task_Quantity__c);
                newWOTask.Task_Classification__c = stagingRecord.Task_Classification__c;
                newWOTask.Task_Length__c = stagingRecord.Task_Length__c;
                newWOTask.isBatch__c = True;
                            
                if(workOrderTaskMap.containsKey(stagingRecord.Work_Order_Id__c)){
                    workOrderTaskMap.get(stagingRecord.Work_Order_Id__c).add(newWOTask);
                    System.debug('if===115');
                }else{
                    workOrderTaskMap.put(stagingRecord.Work_Order_Id__c, new Set<Work_Order_Task__c> {newWOTask});
                    System.debug('else====118');
                }
                Line_Item__c newLI = new Line_Item__c();
                newLI.SOR_Code__c = stagingRecord.SOR_Id__c;
                newLI.Line_Item_External_Id__c = stagingRecord.SOR_Id__c+'-'+simplexWORecordId;
                System.debug('newLI.Line_Item_External_Id__c'+newLI.Line_Item_External_Id__c);
                newLI.Quantity__c = Decimal.valueOf(stagingRecord.SOR_Quantity__c);
                newLI.Work_Order__c = simplexWORecordId;
                newLI.SOR_Description__c = stagingRecord.Work_Order_Lines_Description__c;
                newLI.Main_Work_Order__c = simplexWORecordId;
                newLI.Activity_Staging_Ref_Id__c = stagingRecord.Id;
                newLI.Price_Book__c = (simplexWOMap.get(stagingRecord.Work_Order_Id__c)).Pricebook2Id;
                String sorPriceBookCode = stagingRecord.SOR_Id__c+'-'+(simplexWOMap.get(stagingRecord.Work_Order_Id__c)).Pricebook2Id;
                if(SORPriceBookEntryMap.get(sorPriceBookCode) != null){
                    Decimal bsaUnitRate = (SORPriceBookEntryMap.get(sorPriceBookCode)).BSA_Cost__c != null ? (SORPriceBookEntryMap.get(sorPriceBookCode)).BSA_Cost__c : (SORPriceBookEntryMap.get(sorPriceBookCode)).UnitPrice;
                    Decimal techUnitRate = (SORPriceBookEntryMap.get(sorPriceBookCode)).Tech_Cost__c != null ? (SORPriceBookEntryMap.get(sorPriceBookCode)).Tech_Cost__c : (SORPriceBookEntryMap.get(sorPriceBookCode)).UnitPrice;
					newLI.BSA_Unit_Rate__c = bsaUnitRate;
                    newLI.BSA_Cost__c = bsaUnitRate * Decimal.valueOf(stagingRecord.SOR_Quantity__c);
                    newLI.Technician_Unit_Rate__c = techUnitRate;
                    newLI.Technician_Cost__c = techUnitRate * Decimal.valueOf(stagingRecord.SOR_Quantity__c);
                }
                
                if(workOrderLineItemMap.containsKey(stagingRecord.Work_Order_Id__c)){
                    workOrderLineItemMap.get(stagingRecord.Work_Order_Id__c).add(newLI);
                }else{
                    workOrderLineItemMap.put(stagingRecord.Work_Order_Id__c, new Set<Line_Item__c> {newLI});
                }
            }
        }
        
        //System.debug('DildarLog:  workOrderLineItemMap.size - ' + workOrderLineItemMap.Size());
        //System.debug('DildarLog:  workOrderTaskMap.size - ' + workOrderTaskMap.Size());
        
        //Prepare work order task list for upsert
        List<Work_Order_Task__c> wotList = new List<Work_Order_Task__c>();
        if(workOrderTaskMap.size() > 0){            
            for(String wotKey: workOrderTaskMap.keySet()){
                //System.debug('DildarLog: wotKey - ' + wotKey);
                //System.debug('DildarLog: wot Size - ' + (workOrderTaskMap.get(wotKey)).size());
                wotList.addAll(workOrderTaskMap.get(wotKey));
            }
        }
        //Prepare line item / sor list for upsert
        List<Line_Item__c> liList = new List<Line_Item__c>();
        System.debug('workOrderLineItemMap'+workOrderLineItemMap);
        System.debug('workOrderLineItemMap.size()'+workOrderLineItemMap.size());
        if(workOrderLineItemMap.size() > 0){        
            for(String liKey: workOrderLineItemMap.keySet()){
                System.debug('DildarLog: liKey - ' + liKey);
                System.debug('DildarLog: liKey Size - ' + (workOrderLineItemMap.get(liKey)).size());
                liList.addAll(workOrderLineItemMap.get(liKey));                
            }
        }       
        
        //Upsert task data for all simplex workorders
		Database.UpsertResult[] upsertTaskResult = Database.upsert(wotList, Work_Order_Task__c.Fields.Client_Task_Id__c, false);
        //System.debug('DildarLog: upsertTaskResult - ' + upsertTaskResult);
                
        for(integer i =0; i<wotList.size();i++){
            String msg='';
            Work_Order_Task__c wotObj = wotList[i];
            If(upsertTaskResult[i].isSuccess()){                
                Simplex_Activity_Staging__c objStaging = new Simplex_Activity_Staging__c(Id =wotObj.Activity_Staging_Ref_Id__c);                
                objStaging.IsTaskSuccess__c = true;
                objStaging.IsProcessed__c = true;
                activityStagingMap.put(wotObj.Activity_Staging_Ref_Id__c, objStaging);
            }
            else{
                msg +='Error: "';        
                for(Database.Error err: upsertTaskResult[i].getErrors()){  
                    msg += err.getmessage()+'"\n\n';
                } 
                Simplex_Activity_Staging__c objStaging = new Simplex_Activity_Staging__c(Id =wotObj.Activity_Staging_Ref_Id__c	);
                objStaging.Task_Error_Message__c = msg;
                objStaging.IsProcessed__c = true;
				objStaging.IsTaskSuccess__c = false;
                activityStagingMap.put(wotObj.Activity_Staging_Ref_Id__c, objStaging);              
            }        
     	}
        
        //Upsert lineitem/sor data for all simplex workorders
        Database.UpsertResult[] upsertLIResult = Database.upsert(liList, Line_Item__c.Fields.Line_Item_External_Id__c, false);
        System.debug('DildarLog: upsertLIResult - ' + upsertLIResult);
        System.debug('DildarLog: Line_Item_External_Id__c - ' + Line_Item__c.Fields.Line_Item_External_Id__c);
        
        Set<Id> liSuccessIdSet = new Set<Id>();
        for(integer i =0; i<liList.size();i++){
            System.debug('Zakeer: liList.size()'+liList.size());
            String msg='';
            Line_Item__c liObj = liList[i];
            If(upsertLIResult[i].isSuccess()){
                liSuccessIdSet.add(liObj.Id);
                successWOSet.add(liObj.Work_Order__c);
                System.debug('Zakeer : successWOSet'+successWOSet);
                System.debug('Zakeer : successWOSet.add(liObj.Work_Order__c);'+successWOSet.add(liObj.Work_Order__c));
                
                Simplex_Activity_Staging__c objStaging;
                if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                    objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                    objStaging.IsSORSuccess__c = true;
                    objStaging.IsProcessed__c = true;
                }else{
                    objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                    objStaging.IsSORSuccess__c = true;
                    objStaging.IsProcessed__c = true;                    
                }
                activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);
            }
            else{
                msg +='Error: "';        
                for(Database.Error err: upsertLIResult[i].getErrors()){  
                    msg += err.getmessage()+'"\n\n';
                } 
                Simplex_Activity_Staging__c objStaging;
                if(activityStagingMap.containsKey(liObj.Activity_Staging_Ref_Id__c)){
                    objStaging = activityStagingMap.get(liObj.Activity_Staging_Ref_Id__c);
                    objStaging.IsSORSuccess__c = false;
                    objStaging.IsProcessed__c = true;
                    objStaging.SOR_Error_Message__c = msg;
                }else{
                    objStaging = new Simplex_Activity_Staging__c(Id =liObj.Activity_Staging_Ref_Id__c);                
                    objStaging.IsSORSuccess__c = false;
                    objStaging.IsProcessed__c = true;                    
                    objStaging.SOR_Error_Message__c = msg;
                }
                activityStagingMap.put(liObj.Activity_Staging_Ref_Id__c, objStaging);             
            }        
     	}
        
        /*Map<String, List<Line_Item__c>> newWOLIMap = new Map<String, List<Line_Item__c>>();
        for(Line_Item__c li: [Select Id, BSA_Cost__c, Technician_Cost__c, Work_Order__r.Client_Work_Order_Number__c From Line_Item__c Where Id IN:liSuccessIdSet]){
			if(newWOLIMap.containsKey(li.Work_Order__r.Client_Work_Order_Number__c)){
                newWOLIMap.get(li.Work_Order__r.Client_Work_Order_Number__c).add(li);
            }else{
                newWOLIMap.put(li.Work_Order__r.Client_Work_Order_Number__c, new List<Line_Item__c> {li});
            }
        }
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        List<Payment__c> paymentList = new List<Payment__c>();
        List<Line_Item__c> liListToUpdate = new List<Line_Item__c>();
        if(newWOLIMap.size() > 0){
            for(String newLIWOKey: newWOLIMap.keySet()){
                System.debug('DildarLog: newLIWOKey - ' + newLIWOKey);
                System.debug('DildarLog: newLIWOKey Size - ' + (newWOLIMap.get(newLIWOKey)).size());    
                Double totalBSACost = 0;
                Double totalTechCost = 0;
                Invoice__c inv = new Invoice__c();
                Payment__c pay = new Payment__c();
                for(Line_Item__c li: newWOLIMap.get(newLIWOKey)){
                    if(li.BSA_Cost__c != null)
                        totalBSACost = totalBSACost + li.BSA_Cost__c;
                    
                    if(li.Technician_Cost__c != null)
                        totalTechCost = totalTechCost + li.Technician_Cost__c;
                    li.Payment__r = new Payment__c(Payment_External_Id__c = newLIWOKey);
                    li.Invoice__r = new Invoice__c(Invoice_External_Id__c = newLIWOKey);
                    liListToUpdate.add(li);                    
                }
                inv.Work_Order__r = new WorkOrder(Client_Work_Order_Number__c=newLIWOKey);
                inv.BSA_Total__c = woInvoiceMap.get(newLIWOKey) != null && (woInvoiceMap.get(newLIWOKey)).BSA_Total__c != null ? (woInvoiceMap.get(newLIWOKey)).BSA_Total__c + totalBSACost : totalBSACost;
                inv.Invoice_External_Id__c = newLIWOKey;
                invoiceList.add(inv);
                
                pay.Work_Order__r = new WorkOrder(Client_Work_Order_Number__c=newLIWOKey);
                pay.Amount__c = woPaymentMap.get(newLIWOKey) != null && (woPaymentMap.get(newLIWOKey)).Amount__c != null ? (woPaymentMap.get(newLIWOKey)).Amount__c + totalTechCost : totalTechCost;
                pay.Payment_External_Id__c = newLIWOKey;
                pay.Service_Resource__c = (simplexWOMap.get(newLIWOKey)).Service_Resource__c;
                pay.Status__c = 'Pending';
                paymentList.add(pay);
            }
        }
        System.debug('DildarLog: invoiceList - ' + invoiceList);
        System.debug('DildarLog: paymentList - ' + paymentList);
        
        //Upsert lineitem/sor data for all simplex workorders
        if(invoiceList.size() > 0){
        	Database.UpsertResult[] upsertINVResult = Database.upsert(invoiceList, Invoice__c.Fields.Invoice_External_Id__c, false);
        	System.debug('DildarLog: upsertINVResult - ' + upsertINVResult);
            
            Map<String, String> errorInvStagingMap = new Map<String, String>();
            
            for(integer i =0; i<invoiceList.size();i++){
                String msg='';
                Invoice__c invObj = invoiceList[i];
                If(!upsertINVResult[i].isSuccess()){                
                    msg +='Error: "';        
                    for(Database.Error err: upsertINVResult[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    errorInvStagingMap.put(invObj.Invoice_External_Id__c, msg);
                }             
            }
            
            List<Simplex_Activity_Staging__c> objStagingList = new List<Simplex_Activity_Staging__c>();
            
            for(Simplex_Activity_Staging__c sas: [Select Id, Work_Order_Id__c, Invoice_Error_Message__c From Simplex_Activity_Staging__c Where Work_Order_Id__c IN: errorInvStagingMap.keySet()]){
                Simplex_Activity_Staging__c newSAS = new Simplex_Activity_Staging__c();
                newSAS.Id = sas.Id;
                newSAS.Invoice_Error_Message__c = errorInvStagingMap.get(sas.Work_Order_Id__c);
                objStagingList.add(newSAS);
            }
            if(objStagingList.size() > 0)
            	Update objStagingList;
        }
        
        //Upsert payment data for all simplex workorders
        if(paymentList.size() > 0){
        	Database.UpsertResult[] upsertPAYResult = Database.upsert(paymentList, Payment__c.Fields.Payment_External_Id__c, false);
        	System.debug('DildarLog: upsertPAYResult - ' + upsertPAYResult);
        }
        
        if(liListToUpdate.size() > 0){
            //Upsert lineitem/sor data for all simplex workorders with payment and invoice lookup
            Database.SaveResult[] updateLIResultWithINVPAY = Database.update(liListToUpdate, false);
            System.debug('DildarLog: updateLIResultWithINVPAY - ' + updateLIResultWithINVPAY);
        }
        */
        
        if(activityStagingMap.size() > 0 ){
            Update activityStagingMap.values();
        }
    }
	global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exception list
        if(batchException.size() > 0){
            System.debug('Dildar Debug: Batch - BatchUpsertSimplexSOR - Finish - Exceptions - ' + batchException);
        }else if(Label.UnifySimplexBatchInvoicePaymentActivation == 'Yes'){
            System.debug('DildarLog: successWOSet - ' + successWOSet);
            BatchProcessSimplexInvoiceAndPayment bt = new BatchProcessSimplexInvoiceAndPayment(successWOSet);
        	Database.executeBatch(bt, 25);
        }else{
            System.debug('DildarLog: BatchProcessSimplexInvoiceAndPayment is not activated');
        }
	}
}