@isTest
public class FilesControllerTest {
    @TestSetup
    static void setup(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorgbsa.com');
		Insert u;
        Contact c = new Contact();
         c.FirstName = 'Test Contact';
        c.Phone ='9876543210';
        c.MobilePhone = '1234567898';
        c.LastName = 'Resource';
        c.Email = 'StandrdUser@test.com';
        insert c;
        ServiceResource serviceResourceRecord 
                        = new ServiceResource(Name='ResourceName2'
                                           , ResourceType ='T'
                                           , RelatedRecordId=u.Id
                                           , IsActive=true
                                           ,Contact__c = c.Id
                                           , Pronto_Service_Resource_ID__c ='34567');
        insert serviceResourceRecord;
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
         Account account1 = new Account();
         account1.Name = 'Test Account';
         account1.RecordTypeId = clientRecordTypeId;
         insert account1;
         system.debug('account1'+account1.Id);
         
                 
        Id clientRecordTypeIdForContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Contact contact1 = new Contact();
        contact1.FirstName = 'Test Contact';
        contact1.LastName = 'last name';
        contact1.RecordTypeId = clientRecordTypeIdForContact;
        contact1.MobilePhone = '0987654635';
        contact1.Phone ='1234123412';
        
        contact1.AccountId = account1.Id;
        insert contact1;
        system.debug('contact1'+contact1.Id);
         
         Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();

         Account account2 = new Account();
         account2.Name = 'Test Account Two';
         account2.RecordTypeId = customerRecordTypeId;
         insert account2;
         
         //--Create Work type
         WorkType workType = new WorkType();
         workType.Name = 'CN (Install)';
         Date todayDate = Date.today();
         workType.APS__c = false;
         workType.CUI__c = true;
         workType.EstimatedDuration = 5;
         
         insert workType;
         system.debug('workType'+workType.Id);
         
         //--create Price Book
         Pricebook2 priceBook = new Pricebook2();
         priceBook.Name = 'NSW-CN-Metro-PRICE-BOOK';
         priceBook.IsActive = true;
         insert priceBook;
         
         //--create operating hours
          OperatingHours operatingHours = new OperatingHours();
         operatingHours.Name = 'calender 1';
         insert operatingHours;
         
         //--create service territory
         ServiceTerritory serviceTerritory = new ServiceTerritory();
         serviceTerritory.Name = 'New South Wales';
         serviceTerritory.OperatingHoursId = operatingHours.Id;
         serviceTerritory.IsActive = true;
         insert serviceTerritory;
         
         Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
         System.debug('devRecordTypeId'+devRecordTypeId);
         
         //--create WOAR
         Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
         woar.Account__c = account1.Id;
         woar.RecordTypeId = devRecordTypeId;
         woar.Work_Area__c = 'Metro';
         woar.Work_Type__c = workType.Id;
         woar.Service_Territory__c = serviceTerritory.Id;
         woar.Price_Book__c = priceBook.Id;
         insert woar;
         
         //create case
         Case caseRecord =new Case();
         caseRecord.Work_Area__c = 'Metro';
         caseRecord.Status = 'New';
         //caseRecord.Task_Type__c = 'Hazard';
         caseRecord.Origin = 'Email';
         caseRecord.Work_Type__c = workType.Id;
         caseRecord.AccountId = account1.Id;
         caseRecord.Contactid = contact1.id;
         insert caseRecord;

         WorkOrder workOrder = new WorkOrder();
        workOrder.Service_Resource__c =serviceResourceRecord.Id;
        workOrder.Work_Area__c = 'Metro';
         workOrder.WorkTypeId = workType.Id;
         workOrder.ServiceTerritoryId = serviceTerritory.Id;
         workOrder.CaseId = caseRecord.Id;
         workOrder.city = 'City';
         workOrder.country ='Australia';
         workOrder.state='NSW';
         workOrder.street ='Street1';
         workOrder.PostalCode = '2132';
         workOrder.Client_Work_Order_Number__c  ='number';
        workOrder.NBN_Activity_Start_Date_Time__c = System.now();
         insert workOrder;
        
        WorkOrder cworkOrder = new WorkOrder();
        cworkOrder.Service_Resource__c =serviceResourceRecord.Id;
        cworkOrder.Work_Area__c = 'Metro';
         cworkOrder.WorkTypeId = workType.Id;
        cworkOrder.ParentWorkOrderId = workOrder.id;
         cworkOrder.ServiceTerritoryId = serviceTerritory.Id;
         cworkOrder.CaseId = caseRecord.Id;
         cworkOrder.city = 'City';
         cworkOrder.country ='Australia';
         cworkOrder.state='NSW';
         cworkOrder.street ='Street1';
         cworkOrder.PostalCode = '2132';
         cworkOrder.Client_Work_Order_Number__c  ='number';
        cworkOrder.NBN_Activity_Start_Date_Time__c = System.now();
         insert cworkOrder;
        
         ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;
        
        ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id LIMIT 1];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = cworkOrder.id;
        cdl.ContentDocumentId = contentVersionSelect.ContentDocumentId;
        cdl.shareType = 'V';
        cdl.Visibility = 'AllUsers';
        insert cdl;
        
    }
	@isTest
    public static void fetchFilesTest(){
        List<WorkOrder> lstWo = [select id,recordtype.Developername,Service_Resource__r.Contact__r.Email from WorkOrder limit 1];
        System.debug('lstWo ==+'+lstWo[0].recordtype.Developername);
        
        Map<String,List<ContentDocumentLink>> mContent = FilesController.fetchFiles((String)lstWo[0].Id);
        
    }
}