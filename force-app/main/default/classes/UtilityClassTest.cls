@isTest
public class UtilityClassTest {
    
    
    static testMethod void testUtilityMethods(){
        Account acc = new Account(name='test');
        //insert acc;
        Invoice__c inv1 = new Invoice__c();
        Invoice__c inv2 = new Invoice__c();
        List<Invoice__c> invList = new List<Invoice__c>();
        invList.add(inv1);
        invList.add(inv2);
        DataBase.SaveResult[] results = Database.insert(invList,false);
        
        UtilityClass.setStringToDateFormat('2019-01-01');
        UtilityClass.setStringToDateTimeFormat('2020-01-17T11:43:07+11:00');
        UtilityClass.dynamicUpsert(new List<Account>(),'Id');        
       // UtilityClass.doUpsert(new Account[]{acc});
        UtilityClass.getObjectKeyPrefix('Account');
        set<String> fieldset = new Set<String>();
        fieldset.add('Account');fieldset.add('Contact');fieldset.add('Id');
        UtilityClass.buildFieldsForSOQLQuery(fieldset);
        UtilityClass.setAddress(acc, 'Billing', '1', '2', 'harris', 'street', 'city', 'VIC', '3000');
        UtilityClass.getUUID();
        UtilityClass.picklistValues('Asset', 'Status');
        UtilityClass.fetchFieldTypesforObject('Account');
        //UtilityClass.getGroupIdByName('Test');
        //UtilityClass.transformAndMapFieldValues(fieldType, value)
        UtilityClass.sendSuccessErrorToMulesoft('Test', 'test', true, 'test','2020-01-17T11:43:07+11:00' , 'test');
        UtilityClass.sendSuccessErrorToMulesoft('Test', 'test', false, 'test','2020-01-17T11:43:07+11:00' , 'test');
        UtilityClass.fireOutboundPlatformEvent(null,'WorkOrder');
        UtilityClass.fireOutboundPlatformEvent(null,'TimeSheetEntry');
        UtilityClass.fireOutboundPlatformEvent(null,'Order');
        UtilityClass.fireOutboundPlatformEvent(null,'ProductConsumed');
        UtilityClass.fireOutboundPlatformEvent(null,'Product2');
        UtilityClass.createObjectShare(results[0].getId(),userinfo.getuserId(),'Edit');
        UtilityClass.readSaveResults(results,'test','test','test','test','test');
        List<Invoice__c> invcList = [Select id from Invoice__c];
        Schema.SObjectField f = Invoice__c.Fields.Id;
         DataBase.UpsertResult [] results1 = Database.upsert(invcList,f); 
        UtilityClass.readUpsertResults(results1,'test','test','test','test','test');
        //UtilityClass.getGroupIdByName('Accounts_Payable');
        UtilityClass.transformAndMapFieldValues(Schema.DisplayType.DATETIME,'2020-01-17T11:43:07+11:00');
        UtilityClass.getUUIDForMedia();
        UtilityClass.getUUID();
        //UtilityClass.transformAndMapFieldValues(Schema.DisplayType.DATE,'2020-01-17T11:43:07+11:00');
        //UtilityClass.transformAndMapFieldValues(Schema.DisplayType.DOUBLE,);
    }
}