@isTest
public class ASPR_TriggerFactoryTest {
    
    public static String triggerStatus;
    
    @isTest
    private static void factoryTest(){
        ASPR_TriggerFactory.createHandler(Account.sObjectType);
    }
    
    @isTest
    private static void addClassToHandlerTest(){
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.addClassToHandler('Handler', new List<ASPR_ITrigger>{testHandler});
    }
    
    @isTest
    private static void beforeDeleteTest(){
        Test.startTest();
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.isBeforeTrigger = true;
        ASPR_TriggerFactory.isDeleteTrigger = true;
        ASPR_TriggerFactory.execute(testHandler);
        Test.stopTest();
    }
    
    @isTest
    private static void beforeInsertTest(){
        Test.startTest();
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.isBeforeTrigger = true;
        ASPR_TriggerFactory.isInsertTrigger = true;
        ASPR_TriggerFactory.execute(testHandler);
        Test.stopTest();
    }
    
    @isTest
    private static void beforeUpdateTest(){
        Test.startTest();
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.isBeforeTrigger = true;
        ASPR_TriggerFactory.isUpdateTrigger = true;
        ASPR_TriggerFactory.execute(testHandler);
        Test.stopTest();
    }
    
    @isTest
    private static void afterDeleteTest(){
        Test.startTest();
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.isAfterTrigger = true;
        ASPR_TriggerFactory.isDeleteTrigger = true;
        ASPR_TriggerFactory.execute(testHandler);
        Test.stopTest();
    }
    
    @isTest
    private static void afterInsertTest(){
        Test.startTest();
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.isAfterTrigger = true;
        ASPR_TriggerFactory.isInsertTrigger = true;
        ASPR_TriggerFactory.execute(testHandler);
        Test.stopTest();
    }
    
    @isTest
    private static void afterUpdateTest(){
        Test.startTest();
        Handler testHandler = new Handler();
        ASPR_TriggerFactory.isAfterTrigger = true;
        ASPR_TriggerFactory.isUpdateTrigger = true;
        ASPR_TriggerFactory.execute(testHandler);
        Test.stopTest();
    }
    
    class Handler extends ASPR_AbstractITrigger {
        public void bulkAfter(List<sObject> so) { }
        public void bulkBefore(List<sObject> so) { }
        
        public void beforeInsert(sObject so) { }
        public void beforeUpdate(sObject oldSo, sObject so) { }
        public void beforeDelete(sObject so) { }
        
        public void afterInsert(sObject so) { }
        public void afterUpdate(sObject oldSo, sObject so) { }
        public void afterDelete(sObject so) { }
        
        public void andFinally() { }
    }
}