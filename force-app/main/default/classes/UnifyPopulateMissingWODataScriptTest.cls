@isTest
public class UnifyPopulateMissingWODataScriptTest {
	@testSetup static void setup() {
        List<WorkOrder> woList = new List<WorkOrder>();
        Account acc = NBNTestDataFactory.createTestAccounts();
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);        
    }
    
    static testMethod void validateRunScriptWithoutException(){
		Test.StartTest(); 
			WorkOrder workOrderRecord = [Select Id, planned_remediation_date__c, NBN_Field_Work_Specified_By_Version__c, NBN_Field_Work_Specified_By_Id__c, CorrelationID__c, Client_Work_Order_Number__c from WorkOrder Limit 1];
        	workOrderRecord.planned_remediation_date__c = System.now();
            workOrderRecord.Client_Work_Order_Number__c = 'ERTYU23456TY';
            workOrderRecord.CorrelationID__c = 'ERTYU23456TY';
        	workOrderRecord.NBN_Field_Work_Specified_By_Id__c ='RemediationWOS';
            workOrderRecord.NBN_Field_Work_Specified_By_Version__c = '1.3.1';
        	
			update workOrderRecord;
        	
        	WorkType workTypeRecord = NBNTestDataFactory.createTestWorkType();
        	workTypeRecord.Name = 'NBN Unify Remediation';
        	update workTypeRecord;
			
        	PageReference pageRef = Page.UnifyPopulateMissingWOData; // Add your VF page Name here
			Test.setCurrentPage(pageRef);
			
        	ApexPages.StandardController sc = new ApexPages.StandardController(workOrderRecord);
			UnifyPopulateMissingWODataScript testRunScript = new UnifyPopulateMissingWODataScript(sc);
			testRunScript.unifyWorkOrderPageReference();
		Test.StopTest();
    }
    static testMethod void validateRunScriptWithException(){
		Test.StartTest(); 
			WorkOrder workOrderRecord = [Select Id, planned_remediation_date__c, NBN_Field_Work_Specified_By_Version__c, NBN_Field_Work_Specified_By_Id__c, CorrelationID__c, Client_Work_Order_Number__c from WorkOrder Limit 1];
        	workOrderRecord.planned_remediation_date__c = System.now();
            workOrderRecord.Client_Work_Order_Number__c = 'ERTYU23456TY';
            workOrderRecord.CorrelationID__c = 'ERTYU23456TY';
        	workOrderRecord.NBN_Field_Work_Specified_By_Id__c ='RemediationWOS';
            workOrderRecord.NBN_Field_Work_Specified_By_Version__c = '1.3.1';
        	
			update workOrderRecord;
        	
        	PageReference pageRef = Page.UnifyPopulateMissingWOData; // Add your VF page Name here
			Test.setCurrentPage(pageRef);
			
        	ApexPages.StandardController sc = new ApexPages.StandardController(workOrderRecord);
			UnifyPopulateMissingWODataScript testRunScript = new UnifyPopulateMissingWODataScript(sc);
			testRunScript.unifyWorkOrderPageReference();
		Test.StopTest();
    }
    
}