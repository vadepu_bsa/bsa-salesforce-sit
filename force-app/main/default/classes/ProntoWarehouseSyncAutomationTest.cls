@isTest
public class ProntoWarehouseSyncAutomationTest {
    static testMethod void testValidEvent() {

        // Create a test event instance
        Pronto_Van_Warehouse_Created_Updated__e syncEvent = new Pronto_Van_Warehouse_Created_Updated__e();
        syncEvent.Warehouse_Code__c = 'prontotest';
        syncEvent.Warehouse_Region__c = 'NSW';
        syncEvent.Warehouse_Description__c = 'Testing Warehouse Sync Automation';
        syncEvent.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        syncEvent.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';

        Test.startTest();
        
            // Publish test event
            Database.SaveResult sr = EventBus.publish(syncEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());
        
        // Verify that a location was created/updated by the trigger.
        List<schema.location> locations = [SELECT Id FROM location];
        // Validate that this location was found
        System.assertEquals(1, locations.size());

    }
    static testMethod void testInvalidEvent() {

        // Create a test event instance
        Pronto_Van_Warehouse_Created_Updated__e syncEvent = new Pronto_Van_Warehouse_Created_Updated__e();
        syncEvent.Warehouse_Code__c = 'prontotest';
        syncEvent.Warehouse_Region__c = 'Invalid';
        syncEvent.Warehouse_Description__c = 'Testing Warehouse Sync Automation';
        syncEvent.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        syncEvent.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';

        Test.startTest();
        
            // Publish test event
            Database.SaveResult sr = EventBus.publish(syncEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());
        
        // Verify that a location was NOT created/updated by the trigger.
        List<schema.location> locations = [SELECT Id FROM location];
        // Validate that this location was NOT found
        System.assertEquals(0, locations.size());

    }    
}