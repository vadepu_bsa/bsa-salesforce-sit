global class APS_WorkOrderLineItemFeeder implements Iterator<WorkOrderLineItem>, Iterable<WorkOrderLineItem> {
    List<WorkOrderLineItem> assetWolis;
    
    global Iterator<WorkOrderLineItem> iterator() {
        return this;
    }
    
    global APS_WorkOrderLineItemFeeder(List<WorkOrderLineItem> assetWolis) {
        this.assetWolis = assetWolis;
    }
    
    global WorkOrderLineItem next() {
        return assetWolis.remove(0);
    }
    
    global boolean hasNext() {
        return assetWolis != null && !assetWolis.isEmpty();
    }
}