public class B2BToBSAUpsertQueueable  implements Queueable {
    
    @testVisible
    private SObjectField woRefId;
    private String woCorrelationId;
    private List<WorkOrder> workOrderList;
    private String eventActionName;
    private String woExternalIdValue;
    private integer retryCount;
    
    
    public B2BToBSAUpsertQueueable(SObjectField woRefId,  String woCorrelationId, List<WorkOrder> workOrderList, String eventActionName,String woExternalIdValue, integer retryCount) {
        this.woRefId = woRefId;
        this.woCorrelationId = woCorrelationId;
        this.workOrderList = workOrderList;
        this.eventActionName = eventActionName;
        this.woExternalIdValue = woExternalIdValue;
        this.retryCount = retrycount;
        
    }
    
    public void execute(QueueableContext context) {
        if (workOrderList != null && woRefId != null && woExternalIdValue != null ){
            boolean fireApplicationLog = false;
            
            
            if (eventActionName =='ProntoWorkorder'){
                WorkOrderTriggerHandler.TriggerDisabled = true;
            }
            Database.UpsertResult[] upsertResult = Database.upsert(workOrderList, woRefId , false);
            if (eventActionName =='ProntoWorkorder'){
                WorkOrderTriggerHandler.TriggerDisabled = false;
            }
            for(Database.upsertResult result : upsertResult) {
                
                if(result.isSuccess()) {
                    fireApplicationLog = true;
                }    
                else{
                    string errormessage = '';
                    for(Database.Error err : result.getErrors()) {                    
                        System.debug('DildarLog: ErrorMessage - ' + err.getStatusCode() + ': ' + err.getMessage());
                        errormessage += err.getMessage() + '\n';
                    } 
                    if (!errormessage.contains('unable to obtain exclusive access to this record')){
                        fireApplicationLog = true; 
                    }
                    
                }
            }
            if (retryCount >3){
                fireApplicationLog = true;
            }
            if (fireApplicationLog  ){
                
                UtilityClass.readUpsertResults(upsertResult, JSON.serialize(workOrderList), 'WorkOrder',  woCorrelationId, eventActionName,woExternalIdValue); 			
                
            }else {
                retryCount += 1;
                System.enqueueJob(new B2BToBSAUpsertQueueable(woRefId, woCorrelationId, workorderList,eventActionName, woExternalIdValue,retryCount));
                
            }

            
        }
    }
    
}