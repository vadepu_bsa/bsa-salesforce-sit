/**
* @author          Sunila.M
* @date            12/Feb/2019	
* @description     Test class for ProductConsumedTrigger
*
* Change Date    Modified by         Description  
* 12/Feb/2019		Sunila.M		Created Test class
**/
@isTest
public class ProductConsumedTrigger_Test {
    
    @isTest static void validateProductConsumedTest() {
        
        Test.startTest();
        //create Account
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Account account1 = new Account();
        account1.Name = 'Optus Business';
        account1.RecordTypeId = clientRecordTypeId;
        insert account1;
        system.debug('account1'+account1.Id);
        
        Id clientRecordTypeIdForContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
        Contact contact1 = new Contact();
        contact1.FirstName = 'Test Contact';
        contact1.LastName = 'last name';
        contact1.RecordTypeId = clientRecordTypeIdForContact;
        contact1.MobilePhone = '0987654635';
        contact1.Phone ='1234123412';
        insert contact1;
        system.debug('contact1'+contact1.Id);
        
        Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
        Account account2 = new Account();
        account2.Name = 'Optus Business One';
        account2.RecordTypeId = customerRecordTypeId;
        insert account2;
        
        //--Create Work type
        WorkType workType = new WorkType();
        workType.Name = 'SC (Service Assurance)';
        Date todayDate = Date.today();
        workType.EstimatedDuration = 5;
        workType.CUI__c = true;
        insert workType;
        system.debug('workType'+workType.Id);
        
        //--create Price Book
        Pricebook2 priceBook = new Pricebook2();
        priceBook.Name = 'NSW-CN-Metro-PRICE-BOOK';
        priceBook.IsActive = true;
        insert priceBook;
        
        //--create operating hours
        OperatingHours operatingHours = new OperatingHours();
        operatingHours.Name = 'calender 1';
        insert operatingHours;
        
        //--create service territory
        ServiceTerritory serviceTerritory = new ServiceTerritory();
        serviceTerritory.Name = 'New South Wales';
        serviceTerritory.OperatingHoursId = operatingHours.Id;
        serviceTerritory.IsActive = true;
        insert serviceTerritory;
        
        Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
        System.debug('devRecordTypeId'+devRecordTypeId);
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        PriceBook2 stdPriceBook = new PriceBook2();
        stdPriceBook.Id = pricebookId;
        stdPriceBook.IsActive =true;
        update stdPriceBook;
        //--create WOAR
        Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
        woar.Account__c = account1.Id;
        woar.RecordTypeId = devRecordTypeId;
        woar.Work_Area__c = 'Metro';
        woar.Work_Type__c = workType.Id;
        woar.Service_Territory__c = serviceTerritory.Id;
        woar.Price_Book__c = pricebookId;
        insert woar;
        
        //create case
        Case caseRecord =new Case();
        caseRecord.Work_Area__c = 'Metro';
        caseRecord.Status = 'New';
        //caseRecord.Task_Type__c = 'Hazard';
        caseRecord.Origin = 'Email';
        caseRecord.Work_Type__c = workType.Id;
        caseRecord.AccountId = account1.Id;
        caseRecord.Contactid = contact1.id;
        insert caseRecord;
        
        
        //insert WO
        WorkOrder workOrder = new WorkOrder();
        workOrder.Work_Area__c = 'Metro';
        workOrder.WorkTypeId = workType.Id;
        workOrder.ServiceTerritoryId = serviceTerritory.Id;
        workOrder.CaseId = caseRecord.Id;
        workOrder.city = 'City';
        workOrder.country ='Australia';
        workOrder.state='NSW';
        workOrder.street ='Street1';
        workOrder.PostalCode = '2132';
        workOrder.Client_Work_Order_Number__c  ='number';
        workOrder.Client_Approval_Received__c = true;
        insert workOrder;
        
        Product2 product = new Product2();
        product.ProductCode = 'F1/1.3.4.1';
        product.Name ='F1/1.3.4.1 /product';
        product.Family = 'None';
        insert product;
        
        Product2 product2 = new Product2();
        product2.ProductCode = 'F2/1.3.4.2';
        product2.Name ='F2/1.3.4.2 /product';
        product2.Family = 'None';
        insert product2;        
        
        Product2 product3 = new Product2();
        product3.ProductCode = 'F3/1.3.4.2';
        product3.Name ='F3/1.3.4.3 /product';
        product3.Family = 'None';
        insert product3;  
        
        Product2 product4 = new Product2();
        product4.ProductCode = 'F3/1.3.4.2';
        product4.Name ='F4/1.3.4.4 /product';
        product4.Family = 'None';
        insert product4; 
        
        
        ///-price Book Entry
        PricebookEntry priceBookEntry = new PricebookEntry();
        pricebookEntry.Pricebook2Id = pricebookId;
        pricebookEntry.Product2Id = product.id;
        pricebookEntry.UnitPrice = 0.0;
        pricebookEntry.IsActive = true;
        pricebookEntry.UseStandardPrice = false;
        insert pricebookEntry;
        
        PricebookEntry priceBookEntry2 = new PricebookEntry();
        priceBookEntry2.Pricebook2Id = pricebookId;
        priceBookEntry2.Product2Id = product2.id;
        priceBookEntry2.UnitPrice = 0.0;
        priceBookEntry2.IsActive = true;
        priceBookEntry2.UseStandardPrice = false;
        insert priceBookEntry2;
        
        PricebookEntry priceBookEntry3 = new PricebookEntry();
        priceBookEntry3.Pricebook2Id = pricebookId;
        priceBookEntry3.Product2Id = product3.id;
        priceBookEntry3.UnitPrice = 0.0;
        priceBookEntry3.IsActive = true;
        priceBookEntry3.UseStandardPrice = false;
        insert priceBookEntry3;
        
        //Price_Book_Dependent_Code__mdt customMetaData = new Price_Book_Dependent_Code__mdt();
        //add product Consumed
        ProductConsumed pcRecord = new ProductConsumed();
        pcRecord.PricebookEntryId = pricebookEntry.Id;
        pcRecord.QuantityConsumed = 1;
        pcRecord.WorkOrderId = workOrder.Id;
        insert pcRecord;
        
        ProductConsumed pcRecord4 = new ProductConsumed();
        pcRecord4.PricebookEntryId = pricebookEntry3.Id;
        pcRecord4.QuantityConsumed = 1;
        pcRecord4.WorkOrderId = workOrder.Id;
        insert pcRecord4;
        
        try{
            update pcRecord4;
            
            ProductConsumed pcRecord2 = new ProductConsumed();
            pcRecord2.PricebookEntryId = pricebookEntry.Id;
            pcRecord2.QuantityConsumed = 1;
            pcRecord2.WorkOrderId = workOrder.Id;
            insert pcRecord2;
            
            ProductConsumed pcRecord3 = new ProductConsumed();
            pcRecord3.PricebookEntryId = pricebookEntry2.Id;
            pcRecord3.QuantityConsumed = 1;
            pcRecord3.WorkOrderId = workOrder.Id;
            insert pcRecord3;
            
           
            priceBookEntry3.Needs_Approval__c = true;
            update priceBookEntry3;
            
            workOrder.Client_Approval_Received__c = false;
            update workOrder;
            
            ProductConsumed pcRecord5 = new ProductConsumed();
            pcRecord5.PricebookEntryId = priceBookEntry3.Id;
            pcRecord5.QuantityConsumed = 1;
            pcRecord5.WorkOrderId = workOrder.Id;
            insert pcRecord5;
            
        }catch(Exception ex){
            system.debug('ex'+ex.getMessage());
        }  
        
        //insert WO
       
        WorkOrder workOrder2 = new WorkOrder();
        workOrder2.Work_Area__c = 'Metro';
        workOrder2.WorkTypeId = workType.Id;
        workOrder2.ServiceTerritoryId = serviceTerritory.Id;
        workOrder2.CaseId = caseRecord.Id;
        workOrder2.city = 'City';
        workOrder2.country ='Australia';
        workOrder2.state='NSW';
        workOrder2.street ='Street1';
        workOrder2.PostalCode = '2132';
        workOrder2.Client_Work_Order_Number__c  ='number';
        workOrder2.Client_Approval_Received__c = false;
        workorder2.Client_Work_Order_Number__c = 'Testing';
        insert workOrder2;
        
        try{
            priceBookEntry3.Needs_Approval__c = true;
            update priceBookEntry3;
                        
            ProductConsumed pcRecord5 = new ProductConsumed();
            pcRecord5.PricebookEntryId = priceBookEntry3.Id;
            pcRecord5.QuantityConsumed = 1;
            pcRecord5.WorkOrderId = workOrder2.Id;
            insert pcRecord5;
            
            pcRecord5.QuantityConsumed = 2;
            update  pcRecord5;
            workOrder2.Client_Approval_Received__c = false;
            //update workOrder2;
            
            insert pcRecord5;
            
        }catch(Exception ex){
            system.debug('ex'+ex.getMessage());
        }  

       
        //ProductConsumed pcRecord4 = new ProductConsumed();
        //pcRecord4.PricebookEntryId = pricebookEntry3.Id;
        //pcRecord4.QuantityConsumed = 1;
        //pcRecord4.WorkOrderId = workOrder.Id;
        //insert pcRecord4;
        
               
        Test.stopTest();
    }
    
    
     @isTest static void APSProductConsumed_Test() {
        
        Test.startTest();
       List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = true;
        lstWorkType[1].CUI__c = false;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;
         
        Id pricebookId = Test.getStandardPricebookId();
        
        PriceBook2 stdPriceBook = new PriceBook2();
        stdPriceBook.Id = pricebookId;
        stdPriceBook.IsActive =true;
        update stdPriceBook;
       

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        //lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;

        Id contactClientRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactClientRTypeId, 1);
        insert lstContact;

        Id contactSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Site');
        List<Contact> lstContact2 = APS_TestDataFactory.createContact(lstAccount[2].Id, contactSiteRTypeId, 1);
        lstContact2[0].Service_Report_Recipient__c = true;
        insert lstContact2;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 5);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 2);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;

        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill Limit 10];
        system.debug('!!!!! skillList = ' + skillList);

        Id nonAPSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'APS_Work_Order');
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 1);
        lstWorkOrder[0].RecordTypeId = nonAPSrecordType;
        lstWorkOrder[0].WorkTypeId = lstWorkType[1].Id;
        lstWorkOrder[0].Skill_Group__c = skillList[0].DeveloperName;
        insert lstWorkOrder;
        
        Product2 product = new Product2();
        product.ProductCode = 'F1/1.3.4.1';
        product.Name ='F1/1.3.4.1 /product';
        product.Family = 'None';
        insert product;
        
        Product2 product2 = new Product2();
        product2.ProductCode = 'F2/1.3.4.2';
        product2.Name ='F2/1.3.4.2 /product';
        product2.Family = 'None';
        insert product2;        
        
        Product2 product3 = new Product2();
        product3.ProductCode = 'F3/1.3.4.2';
        product3.Name ='F3/1.3.4.3 /product';
        product3.Family = 'None';
        insert product3;  
        
        Product2 product4 = new Product2();
        product4.ProductCode = 'F3/1.3.4.2';
        product4.Name ='F4/1.3.4.4 /product';
        product4.Family = 'None';
        insert product4; 
        
        
        ///-price Book Entry
        List<PricebookEntry> standardpriceList = new List<PriceBookEntry>();
        PricebookEntry priceBookEntry = new PricebookEntry();
        pricebookEntry.Pricebook2Id = pricebookId;
        pricebookEntry.Product2Id = product.id;
        pricebookEntry.UnitPrice = 0.0;
        pricebookEntry.IsActive = true;
        pricebookEntry.UseStandardPrice = false;
        standardpriceList.add(pricebookEntry);
 
        
        PricebookEntry priceBookEntry2 = new PricebookEntry();
        priceBookEntry2.Pricebook2Id = pricebookId;
        priceBookEntry2.Product2Id = product2.id;
        priceBookEntry2.UnitPrice = 0.0;
        priceBookEntry2.IsActive = true;
        priceBookEntry2.UseStandardPrice = false;
         standardpriceList.add(priceBookEntry2); 
        
        PricebookEntry priceBookEntry3 = new PricebookEntry();
        priceBookEntry3.Pricebook2Id = pricebookId;
        priceBookEntry3.Product2Id = product3.id;
        priceBookEntry3.UnitPrice = 0.0;
        priceBookEntry3.IsActive = true;
        priceBookEntry3.UseStandardPrice = false;
        standardpriceList.add(priceBookEntry3);
        
        insert standardPriceList;
         
        List<PricebookEntry> custompriceList = new List<PriceBookEntry>();
        PricebookEntry priceBookEntry4 = new PricebookEntry();
        priceBookEntry4.Pricebook2Id =  lstPriceBook[0].Id;
        priceBookEntry4.Product2Id = product.id;
        priceBookEntry4.UnitPrice = 0.0;
        priceBookEntry4.IsActive = true;
        priceBookEntry4.UseStandardPrice = false;
        custompriceList.add(pricebookEntry4);
 
        
        PricebookEntry priceBookEntry5 = new PricebookEntry();
        priceBookEntry5.Pricebook2Id =  lstPriceBook[0].Id;
        priceBookEntry5.Product2Id = product2.id;
        priceBookEntry5.UnitPrice = 0.0;
        priceBookEntry5.IsActive = true;
        priceBookEntry5.UseStandardPrice = false;
         custompriceList.add(priceBookEntry5); 
        
        PricebookEntry priceBookEntry6 = new PricebookEntry();
        priceBookEntry6.Pricebook2Id =  lstPriceBook[0].Id;
        priceBookEntry6.Product2Id = product3.id;
        priceBookEntry6.UnitPrice = 0.0;
        priceBookEntry6.IsActive = true;
        priceBookEntry6.UseStandardPrice = false;
        custompriceList.add(priceBookEntry6);
        
        insert custompriceList;
        //Price_Book_Dependent_Code__mdt customMetaData = new Price_Book_Dependent_Code__mdt();
        //add product Consumed
        ProductConsumed pcRecord = new ProductConsumed();
        pcRecord.PricebookEntryId = pricebookEntry4.Id;
        pcRecord.QuantityConsumed = 1;
        pcRecord.WorkOrderId =  lstWorkOrder[0].Id;
        insert pcRecord;
        pcRecord.QuantityConsumed = 3;
        update pcRecord;
        
        ProductConsumed pcRecord4 = new ProductConsumed();
        pcRecord4.PricebookEntryId = pricebookEntry6.Id;
        pcRecord4.QuantityConsumed = 1;
        pcRecord4.WorkOrderId =  lstWorkOrder[0].Id;
        insert pcRecord4;
         
        delete pcRecord4;
        
      
               
        Test.stopTest();
    }
}