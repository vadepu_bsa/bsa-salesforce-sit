/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-02-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class ActionInvoiceToBSAHandler extends PlatformEventSubscriptionBaseHandler{
    
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private String eventActionName;
    private String eventCorrelationId;
    private String eventInvoiceId;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    //private IDTO mDTOClass;
    private InvoiceDTO mBaseDTO;

    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
        //system.debug('Ananti_peSobjectType: ' + peSobjectType);
        //system.debug('Ananti_peToProcessList: ' + peToProcessList);
        //system.debug('Ananti_mappedSobjectType: ' + mappedSobjectType);

        for(sObject sObj : peToProcessList){
            Action_Invoice_To_BSA__e event = (Action_Invoice_To_BSA__e)sObj;
            eventActionName = event.Action__c;
            eventCorrelationId = event.CorrelationId__c;
            //eventInvoiceId = event.InvoiceStatus__c;            
            deserializeJSON(event);                       
            setupAdditonalSettings(event);            
        }        
        super.process(peSobjectType,peToProcessList, mappedSobjectType);            
    }

    private void setupAdditonalSettings(Action_Invoice_To_BSA__e event){

        mIdentifierMapping = new Map<string,String>();      
        mIdentifierMapping.put('Action_Name__c',event.Action__c);
        mIdentifierMapping.put('Correlation_Id__c',event.CorrelationId__c);
        //mIdentifierMapping.put('Invoice_Id__c',event.InvoiceId__c);
        //mAdditionalWhereClause = 'PE_Subscription_Object_Action_Mapping__r.Action_Name__c = \''+event.Action__c+'\'';            
        setAdditionalMappingFilters('Action_Name__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);

    }

    private void deserializeJSON(Action_Invoice_To_BSA__e event){
     
        String jsonString = '';
        if(String.isNotBlank(event.InvoiceStatus__c)){
            String tempString  = ((event.InvoiceStatus__c).removeEnd('}')) + ',"correlationId":"'+event.CorrelationId__c+'"},';
            // System.debug('Invoice JSON'+tempString);
            jsonString = jsonString + tempString;
        }
               
        jsonstring = '{'+jsonString.removeEnd(',')+'}';      
        System.debug('BuildJson'+ jsonString);

        mSpecificDTOClassName ='InvoiceDTO';        

        // System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        if(mSpecificDTOClassName.equalsIgnoreCase('InvoiceDTO')){
            InvoiceDTO newInvoiceDTO = new InvoiceDTO();
            mBaseDTO = newInvoiceDTO.parse(jsonstring.unescapeEcmaScript());
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
        }            

        system.debug('Ananti_mSerializedData: ' + mSerializedData);
    }
    
    public override void processBuinessLogic(){
        List<sObject> finalsObjectList = getMappedObjectList();
        List<Invoice__c> invoiceList = new list<Invoice__c>(); //workorderList
        List<sobject> finalListsInvoice = new List<sobject>(); //sObjectwoList
        //String invExternalField = getMappedExternalIdFromNode('invoiceStatusDetails',false);//woExternalField
        String invExternalIdValue = ''; //woExternalIdValue
        //String invCorrelationId = ''; //woCorrelationId

        system.debug('Ananti_finalsObjectList: ' + finalsObjectList);
        //system.debug('Ananti_invExternalField: ' + invExternalField);

        
        for(sObject obj: finalsObjectList){
            if(String.valueOf(obj.getSObjectType()) == 'Invoice__c'){
                finalListsInvoice.add(obj);
                invExternalIdValue = (String)((Invoice__c)obj).get('Id');                
            }
        }

        system.debug('Ananti_invExternalIdValue: ' + invExternalIdValue);
        system.debug('Ananti_finalListsInvoice: ' + finalListsInvoice);

        if(finalListsInvoice.size() > 0){
            //SObjectField invRefId = getSObjectFieldName('Invoice__c', invExternalField);
            //system.debug('Ananti_invRefId: ' + invRefId);
            Set<Id> successInvIdSet = new Set<Id>();
            if(eventActionName =='InvoiceStatus'){
                //set<string> InvoiceIdSet = new Set<string>();
                Map<string,String> InvoiceUpdateMap = new Map<String,String>();
                /*
                for(integer i= 0; i < finalListsInvoice.size(); i++){
                    string Invoiceid =  (String)finalListsInvoice[i].get('Id');                    
                    InvoiceIdSet.add(Invoiceid);
                }*/

                List<Invoice__c> fetchInvoiceList = [SELECT Id, Invoice_Number__c, Status__c FROM Invoice__c WHERE id =: invExternalIdValue ];

                system.debug('Ananti_fetchInvoiceList: ' + fetchInvoiceList);

                for (integer i = 0; i <fetchInvoiceList.size() ; i++){
                    InvoiceUpdateMap.put(fetchInvoiceList[i].Id, fetchInvoiceList[i].Invoice_Number__c);                    
                }
                system.debug('Ananti_InvoiceUpdateMap: ' + InvoiceUpdateMap);

                for(integer i= 0; i < finalListsInvoice.size(); i++){
                    finalListsInvoice[i].put('Id', invExternalIdValue);              
                    invoiceList.add((Invoice__c)finalListsInvoice[i]);
                }
                system.debug('Ananti_After update_finalListsInvoice: ' + finalListsInvoice);
                system.debug('Ananti_invoiceList: ' + invoiceList);
            }

            if(invoiceList != null && invoiceList.size() > 0)
            {
                Database.UpsertResult[] upsertResult = Database.upsert(invoiceList, false);
                successInvIdSet = UtilityClass.readUpsertResults(upsertResult, JSON.serialize(invoiceList), 'Invoice__c', eventCorrelationId, eventActionName,invExternalIdValue );

                system.debug('Ananti_upsertResult: ' + upsertResult);
                system.debug('Ananti_successInvIdSet: ' + successInvIdSet);
            }
        }      
        
    }
    
}