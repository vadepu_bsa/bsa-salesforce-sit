@isTest
public class NBNLogEntryTriggerTest {
     @testsetup
    public static void setup(){
        Account acc = NBNTestDataFactory.createTestAccounts();
         WorkType workType =  NBNTestDataFactory.createTestWorkType();
        Pricebook2 priceBook = NBNTestDataFactory.createTestPriceBook();
        OperatingHours operatingHrs = NBNTestDataFactory.createTestOperatingHours();
        ServiceTerritory servTerr = NBNTestDataFactory.createTestServiceTerritory(operatingHrs.id);
        ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
        
        NBNTestDataFactory.createTestWOARS(acc.id,workType.id,servTerr.id,priceBook.id,serviceContract.Id);
        WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
    }
    @isTest
    public static void insertLogsOnParentWO(){
        WorkOrder workorder = [select id from workorder where parentWorkOrderId=null limit 1 ];
        WorkOrder woChild = [select id,Service_Resource__c from WorkOrder where parentWorkOrderId=:workorder.Id LIMIT 1];
		ServiceResource serviceRes = NBNTestDataFactory.createTestResource();
        NBNNAWorkOrderServiceImplementation.isWOUpdated =false;
		woChild.Service_Resource__c = serviceRes.Id;
        update woChild;
        Test.startTest();
        NBN_Log_Entry__c objLog =NBNTestDataFactory.createNBNLogEntry(workorder.Id);
        NBNLogEntryTriggerHandler.TriggerDisabled = false;
        
        objLog.Summary__c ='Test Update';
        update objLog;
        Test.stopTest();
        NBN_Log_Entry__c objLog1 =[select id,Summary__c from NBN_Log_Entry__c LIMIT 1];
        System.assert(objLog1!=null);
        
    }
    public Testmethod static void insertLogsOnChildWO(){
        WorkOrder workorder = [select id from workorder where parentWorkOrderId=null limit 1 ];
        WorkOrder woChild = [select id,Service_Resource__c from WorkOrder where parentWorkOrderId=:workorder.Id LIMIT 1];
		ServiceResource serviceRes = NBNTestDataFactory.createTestResource();
        NBNNAWorkOrderServiceImplementation.isWOUpdated =false;
		woChild.Service_Resource__c = serviceRes.Id;
        update woChild;
        Test.startTest();
        
        NBN_Log_Entry__c objLog = NBNTestDataFactory.createNBNLogEntry(woChild.Id);
        NBNLogEntryTriggerHandler.TriggerDisabled = false;
        objLog.Summary__c ='Test Update';
        update objLog;
        Test.stopTest();
        NBN_Log_Entry__c objLog1 =[select id,Summary__c from NBN_Log_Entry__c LIMIT 1];
        System.assert(objLog1!=null);
        
    }

}