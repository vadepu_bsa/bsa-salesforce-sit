/**
 * @File Name          : ProntoProductSerialize.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 11/08/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    17/08/2020         Tom Henson             Initial Version
**/
public class ProntoProductSerialize {
    
    public static void generateProntoProductPlatformEvent (set<id> productIdSet){
        
        List<ProductDetails> finalProductList = productSerialize(productIdSet);
        
        List<Action_Product_From_BSA__e> InsertplatformEventList = generatePlatformEvents(finalProductList);
        string logs =  JSON.serializePretty(InsertplatformEventList); 
        // System.Debug('!!! Full Output');  
        for (Integer i = 0; i < logs.length(); i=i+300) {
            Integer iEffectiveEnd = (i+300 > (logs.length()-1) ? logs.length()-1 : i+300);
            // System.debug(logs.substring(i,iEffectiveEnd));
          }
        if (InsertplatformEventList.size() > 0 ) {
            List<Database.SaveResult> results = EventBus.publish(InsertplatformEventList);
        }
    }
    
    public static List<ProductDetails>  productSerialize(set<id> productIdSet){
        
        List<Product2> productObj = [SELECT Client_ID__c, Client_Name__c, Client_Name__r.CustomerCode__c,Correlation_ID__c,Description,DisplayUrl,Event_TimeStamp__c,ExternalDataSourceId,ExternalId,Family,
                                        Is_Serialized__c,Name,Pack_Quantity__c,ProductCode,Product_Code__c,Product_Expiry__c,Product_Group__c,QuantityUnitOfMeasure,StockKeepingUnit,Type__c 
                 
                                        FROM Product2
                                        WHERE Id =: productIdSet]; 
        List<ProductDetails> productDetailsList = new List<ProductDetails>();
        for (integer i = 0; i < productObj.size(); i++){
            ProductDetails pObj = new ProductDetails();
          
            pObj.Client_ID = (productObj[i].Client_ID__c != null) ? productObj[i].Client_ID__c : null;
            pObj.Client_Name = (productObj[i].Client_Name__c != null && productObj[i].Client_Name__r.CustomerCode__c != null) ? productObj[i].Client_Name__r.CustomerCode__c : null;
            pObj.Correlation_ID = (productObj[i].Correlation_ID__c != null) ? productObj[i].Correlation_ID__c : null;
            pObj.Description = (productObj[i].Description != null) ? productObj[i].Description : null;
            pObj.DisplayUrl = (productObj[i].DisplayUrl != null) ? productObj[i].DisplayUrl : null;
            pObj.Event_TimeStamp = (productObj[i].Event_TimeStamp__c != null) ? productObj[i].Event_TimeStamp__c : null;
            pObj.ExternalDataSourceId = (productObj[i].ExternalDataSourceId != null) ? productObj[i].ExternalDataSourceId : null;
            pObj.ExternalId = (productObj[i].ExternalId != null) ? productObj[i].ExternalId : null;
            pObj.Family = (productObj[i].Family != null) ? productObj[i].Family : null;
            pObj.Is_Serialized = (productObj[i].Is_Serialized__c != null) ? String.valueOf(productObj[i].Is_Serialized__c) : null;
            pObj.Name = (productObj[i].Name != null) ? productObj[i].Name : null;
            pObj.Pack_Quantity = (productObj[i].Pack_Quantity__c != null) ? String.valueOf(productObj[i].Pack_Quantity__c) : null;
            pObj.ProductCode = (productObj[i].ProductCode != null) ? productObj[i].ProductCode : null;
            pObj.Product_Code = (productObj[i].Product_Code__c != null) ? productObj[i].Product_Code__c : null;
            pObj.Product_Expiry = (productObj[i].Product_Expiry__c != null) ? String.valueOf(productObj[i].Product_Expiry__c) : null;
            pObj.Product_Group = (productObj[i].Product_Group__c != null) ? productObj[i].Product_Group__c : null;
            pObj.QuantityUnitOfMeasure = (productObj[i].QuantityUnitOfMeasure != null) ? productObj[i].QuantityUnitOfMeasure : null;
            pObj.StockKeepingUnit = (productObj[i].StockKeepingUnit != null) ? productObj[i].StockKeepingUnit : null;
            pObj.ProductType = (productObj[i].Type__c != null) ? productObj[i].Type__c : null;
            
            productDetailsList.add(pObj);
            // system.debug('JSON Serialize ' + JSON.serialize(pObj,false));
        }
        // system.debug('JSON Serialize ALL ' + JSON.serialize(productDetailsList,false));
        return productDetailsList;
    }
    
    Public Static List<Action_Product_From_BSA__e> generatePlatformEvents(List<ProductDetails> finalProductList){
        List<Action_Product_From_BSA__e> returnlist = new List<Action_Product_From_BSA__e>();
 
         for (ProductDetails p : finalProductList){
             Action_Product_From_BSA__e eventObj = new Action_Product_From_BSA__e();
 
             eventObj.Action__c = 'Generate_APS_SORCodes';
             eventObj.CorrelationId__c  = UtilityClass.getUUID();
             eventObj.Product__c = '"productDetails":' + JSON.serialize(p,false);
             eventObj.EventTimeStamp__c = String.ValueOf(System.DateTime.Now());
             returnlist.add(eventObj);
         }
         return returnlist;
     }

    Public Class ProductDetails{
        public String Client_ID {get;set;}
        public String Client_Name {get;set;}
        public String Correlation_ID {get;set;}
        public String Description {get;set;}
        public String DisplayUrl {get;set;}
        public String Event_TimeStamp {get;set;}
        public String ExternalDataSourceId {get;set;}
        public String ExternalId {get;set;}
        public String Family {get;set;}
        public String Is_Serialized {get;set;}
        public String Name {get;set;}
        public String Pack_Quantity {get;set;}
        public String ProductCode {get;set;}
        public String Product_Code {get;set;}
        public String Product_Expiry {get;set;}
        public String Product_Group {get;set;}
        public String QuantityUnitOfMeasure {get;set;}
        public String StockKeepingUnit {get;set;}
        public String ProductType {get;set;}                              

        public ProductDetails(){
            Client_ID = null;
            Client_Name = null;
            Correlation_ID = null;
            Description = null;
            DisplayUrl = null;
            Event_TimeStamp = null;
            ExternalDataSourceId = null;
            ExternalId = null;
            Family = null;
            Is_Serialized = null;
            Name = null;
            Pack_Quantity = null;
            ProductCode = null;
            Product_Code = null;
            Product_Expiry = null;
            Product_Group = null;
            QuantityUnitOfMeasure = null;
            StockKeepingUnit = null;  
            ProductType = null;      
        }
    }
      
}