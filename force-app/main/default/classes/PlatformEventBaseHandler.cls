/**
* @author          Karan Shekhar
* @date            20/Jan/2020	
* @description    
* Change Date    Modified by         Description  
* 20/Jan/2020    Karan Shekhar		Base handler to support Publishing of Platform Events
**/

public virtual class PlatformEventBaseHandler implements IPlatformEvent{
    
    private Boolean mPublishPE; 
    private Boolean mSObjectListToProcess; 
    private List<sObject> mPlatformEventsToPublish;
    
    public PlatformEventBaseHandler(SObjectType sObjectRecord, List<sObject> sObjectList, Boolean shouldPublishPE){

        mPublishPE = shouldPublishPE;
        mPlatformEventsToPublish = sObjectList;
    } 
            
    public virtual void processPlatformEvent(){
        
        mapPlatformEvent(mPlatformEventsToPublish);
        publishEvents(); 
        
    }
               
    public void mapPlatformEvent(List<sObject> recordsToMaps){ 
        mPlatformEventsToPublish = new List<sObject>();
        for(sObject sobj : recordsToMaps){
             
            sObject event = mapPlatformEventFields(sobj);
            mPlatformEventsToPublish.add(event);
        }          
    } 
                    
    public void publishEvents(){ 
        System.debug('Events to publish'+mPlatformEventsToPublish);
        System.debug('Should publish Event?' +mPublishPE);
        if(!mPlatformEventsToPublish.isEmpty() && mPublishPE){
            ApplicationLogUtility.publishEvents(mPlatformEventsToPublish);            
        }
        
    }
    
    public virtual sObject mapPlatformEventFields(sObject recordTomap){
        
        return null;
    }

}