@isTest
public class ProntoTimeSheetSerializeTest {
    @TestSetup
    static void makeData(){
        Profile stdProfile = [SELECT Id, Name FROM Profile WHERE Name = 'Standard User'];
        User u1 = new User( email='test.user@gmail.com',
                            UserName='testTimeSheetSerialize.user@gmail.com', 
                            Alias = 'GDS',
                            TimeZoneSidKey='America/New_York',
                            EmailEncodingKey='ISO-8859-1',
                            LocaleSidKey='en_US', 
                            LanguageLocaleKey='en_US',
                            PortalRole = 'Manager',
                            FirstName = 'Test',
                            ProfileId = stdProfile.Id,
                            LastName = 'User');
         insert u1;
        
        ServiceResource sr = new ServiceResource();
        sr.RelatedRecordId = u1.Id;
        sr.Name ='Test';
        sr.Pronto_Service_Resource_ID__c = '12345';

        insert sr;

        TimeSheet ts = new TimeSheet();
        ts.ServiceResourceId = sr.Id;
        ts.StartDate = Date.today();
        ts.EndDate = Date.today().addDays(1);
        insert ts;

        TimeSheetEntry tse = new TimeSheetEntry();
        tse.TimeSheetId = ts.Id;
        tse.Status = 'Submitted';
        insert tse;

    }
    @isTest
    static void generateProntoTimeSheetPlatformEventTest() {
        TimeSheetEntry t = [SELECT Id From TimeSheetEntry LIMIT 1];
        Set<Id> idSet = new Set<Id>();
        idSet.add(t.Id);
        ProntoTimeSheetSerialize.generateProntoTimeSheetPlatformEvent(idSet);
    }
}