@isTest
private class APS_ConvertQuoteToWorkOrderCtrTest {

    static final List<String> maintenanceRoutines = new String[]{'Monthly', 'Quarterly', 'Bi-Monthly', 'Quarterly', 'Six-Monthly', 'Yearly', 'Bi-Annual', '5 Yearly'};

    static String startDueDateStr {
        get{
            if(String.isBlank(startDueDateStr)) {
                Date startDueDate = Date.today();
                startDueDateStr = startDueDate.year() + '-' + startDueDate.month() + '-' + startDueDate.day();
            }
            return startDueDateStr;
        }
        set;
    }
            
    static String endDueDateStr {
        get{
            if(String.isBlank(endDueDateStr)) {
                Date endDueDate = Date.today().addMonths(1);
                endDueDateStr = endDueDate.year() + '-' + endDueDate.month() + '-' + endDueDate.day();
            }
            return endDueDateStr;
        }
        set;
    }

    @TestSetup
    static void setup(){
        APS_TestDataFactory.testDataSetup();
        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;


        List<Product2> products = new List<Product2>();
        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'GenericDefect',
                'Name' => 'Generic Defect',
                'Type__c' => 'Part',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'GenericLabourInternal',
                'Name' => 'Generic Labour Internal',
                'Type__c' => 'Labour',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'GenericPartSOR',
                'Name' => 'Generic Part',
                'Type__c' => 'Part',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        products.addAll((List<Product2>)TestRecordCreator.createSObjects('Product2', 1,
            new Map<String, object>{
                'ProductCode' => 'APSF',
                'Name' => 'APS Filter',
                'Type__c' => 'Part',
                'Family' => 'None',
                'Product_Group__c' => 'Part',
                'IsActive' => true
        }, null));

        insert products;

        String pricebookId = Test.getStandardPricebookId();

        List<PricebookEntry> pbes = APS_TestDataFactory.createPricebookEntries(products, pricebookId);
        insert pbes;

        List<Account> sites = [SELECT Id FROM Account WHERE Name = 'test site'];
    
        List<Asset> equipments = [SELECT Id FROM Asset WHERE Name = 'test equipment'];
        // create defects
        List<Defect__c> defects = new List<Defect__c>();

        defects.addAll((List<Defect__c>)TestRecordCreator.createSObjects('Defect__c', 1,
            new Map<String, object>{
                'Site__c' => sites[0].Id,
                'Defect_Details__c' => 'Circuit no longer burned',
                'Defect_Status__c' => 'Open',
                'Subject__c' => 'Circuit has burned out',
                'Asset__c' => equipments[0].Id,
                'SOR_Code__c' => 'APSF'
            }, null));

        defects.addAll((List<Defect__c>)TestRecordCreator.createSObjects('Defect__c', 1,
        new Map<String, object>{
            'Site__c' => sites[0].Id,
            'Defect_Details__c' => 'Circuit no longer burned',
            'Defect_Status__c' => 'Open',
            'Subject__c' => 'Circuit has burned out',
            'Asset__c' => equipments[0].Id,
            'SOR_Code__c' => ''
        }, null));

        insert defects;

        sites = [SELECT Id FROM Account WHERE Name = 'test site'];

        for(Account site : sites) {
            site.Price_Book__c = Test.getStandardPricebookId();
        }

        update sites;

        List<WorkType> wts = new List<WorkType>();

        WorkType wt = new WorkType();
        wt.Name = 'Defect Rectification';
        wt.EstimatedDuration = 2;
        wt.DurationType = 'Hours';
        wt.APS__c = true;
        wts.add(wt);

        wt = new WorkType();
        wt.Name = 'Quote';
        wt.EstimatedDuration = 2;
        wt.DurationType = 'Hours';
        wt.APS__c = true;
        wts.add(wt);
        insert wts;

    }   
    @IsTest
    static void generateDefectWorkOrdersTest() {

        String defectIdsStr = '';
        for(Defect__c def : [SELECT Id FROM Defect__c]) {
            defectIdsStr += def.Id + ',';
        }
        
        defectIdsStr = defectIdsStr.removeEnd(',');

       String quoteId = APS_DefectController.createDefectQuote(defectIdsStr);
       quoteId=quoteId.split(',')[1];
        system.debug('Printing QuoteId'+quoteId);
        //Order defectQuote = [SELECT Status FROM Order WHERE Id =: quoteId LIMIT 1];
        Order defectQuote = [SELECT Status FROM Order WHERE OrderNumber =: quoteId LIMIT 1];
        defectQuote.Status = 'In Approval Process';
        update defectQuote;

        defectQuote.Status = 'Submitted';
        update defectQuote;

        defectQuote.Status = 'Accepted';
        update defectQuote;
        

        Test.startTest();
        //APS_ConvertQuoteToWorkOrderCtr.generateWorkOrders(quoteId, 'Defect_Quote');
        APS_ConvertQuoteToWorkOrderCtr.generateWorkOrders(defectQuote.Id, 'Defect_Quote');
        Test.stopTest();
    }

    @IsTest
    static void generateNonDefectWorkOrdersTest() {

        Account sites = [SELECT Id FROM Account WHERE Name = 'test site'];

        String defectIdsStr = '';
        for(Defect__c def : [SELECT Id FROM Defect__c]) {
            defectIdsStr += def.Id + ',';
        }
        
        defectIdsStr = defectIdsStr.removeEnd(',');

        Order nonDefectQuote = new Order();
        nonDefectQuote.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Non_Defect_Quote').getRecordTypeId(); 
        nonDefectQuote.AccountId = sites.Id;
        nonDefectQuote.EffectiveDate = Date.today();
        nonDefectQuote.Status = 'Draft';
        insert nonDefectQuote;

        String pricebookId = Test.getStandardPricebookId();


        Product2 p = [SELECT Id FROM Product2 WHERE ProductCode = 'APSF'];
        PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Pricebook2Id =: pricebookId AND Product2Id =: p.Id];

        List<OrderItem> ois = new List<OrderItem>();

        OrderItem oi = new OrderItem();
        oi.OrderId = nonDefectQuote.Id;
        oi.Product2Id = p.Id;
        oi.PricebookEntryId = pbe.Id;
        oi.UnitPrice = 1;
        ois.add(oi);

        insert ois;

        nonDefectQuote.Status = 'In Approval Process';
        update nonDefectQuote;

        nonDefectQuote.Status = 'Submitted';
        update nonDefectQuote;

        nonDefectQuote.Status = 'Accepted';
        update nonDefectQuote;
        

        Test.startTest();
        APS_ConvertQuoteToWorkOrderCtr.generateWorkOrders(nonDefectQuote.Id, 'Non_Defect_Quote');
        Test.stopTest();
    }
}