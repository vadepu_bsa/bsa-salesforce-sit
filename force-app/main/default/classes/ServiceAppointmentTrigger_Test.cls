/**
* @author          Sunila.M
* @date            23/April/2019 
* @description     
*
* Change Date       Modified by         Description  
* 23/April/2019     Sunila.M            Test class for Service Appointment trigger
* 08/July/2020      David.A (IBM)       Test class for Service Appointment Trigger Helper (After Update)
**/

@isTest
public class ServiceAppointmentTrigger_Test {


    @TestSetup
    static void setup(){
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = false;
        lstWorkType[1].CUI__c = true;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        //lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;

        Id contactRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactRTypeId, 1);
        insert lstContact;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 5);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 5);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 3);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        lstWOAR[2].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;
    }

    @isTest 
    static void testDelete() {
        
        Set<Id> setWoIds = new Set<Id>();
        List<Account> lstAccount = [SELECT Id FROM Account ];
        List<WorkType> lstWorkType = [SELECT Id FROM WorkType ];
        List<ServiceTerritory> lstServiceTerritory = [SELECT Id FROM ServiceTerritory ];
        List<ServiceContract> lstServiceContract = [SELECT Id FROM ServiceContract ];
        List<ServiceResource> lstServiceResource = [SELECT Id FROM ServiceResource ];
        List<Pricebook2> lstPriceBook = [SELECT Id FROM Pricebook2 ];
        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill LIMIT 10];
        Case objCase = [SELECT Id FROM Case ][0];
        FSL__Scheduling_Policy__c fslSchedule = [SELECT Id FROM FSL__Scheduling_Policy__c ];
        List<MaintenancePlan> lstMaintenancePlan  = [SELECT Id FROM MaintenancePlan ];
        List<MaintenanceAsset> lstMaintenanceAsset = [SELECT Id FROM MaintenanceAsset ];
        List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Required_Skill__c> lstRequiredSkill = [SELECT Id FROM Required_Skill__c ];
        List<ServiceTerritoryMember> lstServiceTerritoryMember  = [SELECT Id FROM ServiceTerritoryMember ];

        
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 6);
        lstWorkOrder[1].ParentWorkOrderId = lstWorkOrder[0].Id;
        insert lstWorkOrder;
        
        Test.startTest();

        Id SARecordTypeId = APS_TestDataFactory.getRecordTypeId('ServiceAppointment', 'APS');
        List<ServiceAppointment> lstServiceAppointment = APS_TestDataFactory.createServiceAppointment(lstWorkOrder[0].Id, lstServiceTerritory[0].Id, SARecordTypeId, 1);
        lstServiceAppointment[0].Status = 'New';
        lstServiceAppointment[0].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[0].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        System.debug('***- SA -- ' + lstServiceAppointment);
        insert lstServiceAppointment;  
            
          Entitlement entObj = new Entitlement();
          entObj.Name = 'Test Entitlement';
          entobj.AccountId = lstAccount[2].Id;
          insert entobj;
         lstWorkOrder[0].EntitlementId = entobj.Id;
         update lstWorkOrder;

        AssignedResource assResource = new AssignedResource();
        assResource.ServiceResourceId = lstServiceResource[0].Id;
        assResource.ServiceAppointmentId = lstServiceAppointment[0].Id;
        insert assResource;

        Id woliRT = APS_TestDataFactory.getRecordTypeId('WorkOrderLineItem', 'Asset_Action');
        List<WorkOrderLineItem> lstWoli = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id, lstWorkOrder[0].Id, lstMaintenancePlan[0].Id, 3);
        lstWoli[0].RecordTypeId = woliRT;
        lstWoli[0].Service_Appointment__c = lstServiceAppointment[0].Id;
        lstWoli[1].RecordTypeId = woliRT;
        lstWoli[2].RecordTypeId = woliRT;
        lstWoli[1].AssetId = lstAssetChildren[1].Id;
        lstWoli[2].AssetId = lstAssetChildren[2].Id;
        insert lstWoli;

            delete lstServiceAppointment;
        Test.stopTest();
        
    }

    //IBM - David Azzi - 08-July-2020
    @isTest 
    static void validateUpdateWorkOrderStatus() {
        
        Set<Id> setWoIds = new Set<Id>();
        List<Account> lstAccount = [SELECT Id FROM Account ];
        List<WorkType> lstWorkType = [SELECT Id FROM WorkType ];
        List<ServiceTerritory> lstServiceTerritory = [SELECT Id FROM ServiceTerritory ];
        List<ServiceContract> lstServiceContract = [SELECT Id FROM ServiceContract ];
        List<ServiceResource> lstServiceResource = [SELECT Id FROM ServiceResource ];
        List<Pricebook2> lstPriceBook = [SELECT Id FROM Pricebook2 ];
        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill LIMIT 10];
        Case objCase = [SELECT Id FROM Case ][0];
        FSL__Scheduling_Policy__c fslSchedule = [SELECT Id FROM FSL__Scheduling_Policy__c ];
        List<MaintenancePlan> lstMaintenancePlan  = [SELECT Id FROM MaintenancePlan ];
        List<MaintenanceAsset> lstMaintenanceAsset = [SELECT Id FROM MaintenanceAsset ];
        List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Required_Skill__c> lstRequiredSkill = [SELECT Id FROM Required_Skill__c ];
        List<ServiceTerritoryMember> lstServiceTerritoryMember  = [SELECT Id FROM ServiceTerritoryMember ];

        
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 6);
        lstWorkOrder[1].ParentWorkOrderId = lstWorkOrder[0].Id;
        insert lstWorkOrder;
        
         Entitlement entObj = new Entitlement();
        entObj.Name = 'Test Entitlement';
        entobj.AccountId = lstAccount[2].Id;
        insert entobj;
         lstWorkOrder[0].EntitlementId = entobj.Id;
         update lstWorkOrder;
        
        Id woliRT = APS_TestDataFactory.getRecordTypeId('WorkOrderLineItem', 'Asset_Action');
        List<WorkOrderLineItem> lstWoli = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id, lstWorkOrder[0].Id, lstMaintenancePlan[0].Id, 3);
        lstWoli[0].RecordTypeId = woliRT;
        lstWoli[1].RecordTypeId = woliRT;
        lstWoli[2].RecordTypeId = woliRT;
        lstWoli[1].AssetId = lstAssetChildren[1].Id;
        lstWoli[2].AssetId = lstAssetChildren[2].Id;
        insert lstWoli;

        Id SARecordTypeId = APS_TestDataFactory.getRecordTypeId('ServiceAppointment', 'APS');
        List<ServiceAppointment> lstServiceAppointment = APS_TestDataFactory.createServiceAppointment(lstWorkOrder[0].Id, lstServiceTerritory[0].Id, SARecordTypeId, 4);
        lstServiceAppointment[0].Status = 'New';
        lstServiceAppointment[0].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[0].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        lstServiceAppointment[1].Status = 'Completed';
        lstServiceAppointment[1].Completion_Status__c = 'No Further Action Required';
        lstServiceAppointment[1].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[1].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        lstServiceAppointment[1].ParentRecordId = lstWorkOrder[1].Id;
        lstServiceAppointment[2].Status = 'Completed';
        lstServiceAppointment[2].ParentRecordId = lstWorkOrder[2].Id;
        lstServiceAppointment[2].Completion_Status__c = 'Multi-Day Job';
        lstServiceAppointment[2].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[2].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        lstServiceAppointment[3].ParentRecordId = lstWorkOrder[3].Id;
        lstServiceAppointment[3].Status = 'Completed';
        lstServiceAppointment[3].Completion_Status__c = 'No Access';
        lstServiceAppointment[3].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[3].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        
        Test.startTest();
            System.debug('***- SA -- ' + lstServiceAppointment);
            insert lstServiceAppointment;  

            AssignedResource assResource = new AssignedResource();
            assResource.ServiceResourceId = lstServiceResource[0].Id;
            assResource.ServiceAppointmentId = lstServiceAppointment[0].Id;
            insert assResource;

            ServiceAppointmentTriggerHelper.updateWorkOrderStatus(lstServiceAppointment);

            //lstServiceAppointment[0].Status = 'Completed';
            //update lstServiceAppointment;
            System.assertEquals(lstServiceAppointment[0].RecordTypeId, SARecordTypeId);
        Test.stopTest();
    }

    //IBM - David Azzi - 08-July-2020
    @isTest 
    static void validateScheduledSLAsEnhancement() {
        
        Set<Id> setWoIds = new Set<Id>();
        List<Account> lstAccount = [SELECT Id FROM Account ];
        List<WorkType> lstWorkType = [SELECT Id FROM WorkType ];
        List<ServiceTerritory> lstServiceTerritory = [SELECT Id FROM ServiceTerritory ];
        List<ServiceContract> lstServiceContract = [SELECT Id FROM ServiceContract ];
        List<ServiceResource> lstServiceResource = [SELECT Id FROM ServiceResource ];
        List<Pricebook2> lstPriceBook = [SELECT Id FROM Pricebook2 ];
        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill LIMIT 10];
        Case objCase = [SELECT Id FROM Case ][0];
        FSL__Scheduling_Policy__c fslSchedule = [SELECT Id FROM FSL__Scheduling_Policy__c ];
        List<MaintenancePlan> lstMaintenancePlan  = [SELECT Id FROM MaintenancePlan ];
        List<MaintenanceAsset> lstMaintenanceAsset = [SELECT Id FROM MaintenanceAsset ];
        List<Asset> lstAssetParents = [SELECT Id FROM Asset WHERE ParentId = '' ];
        List<Asset> lstAssetChildren = [SELECT Id FROM Asset WHERE ParentId != '' ];
        List<Required_Skill__c> lstRequiredSkill = [SELECT Id FROM Required_Skill__c ];
        List<ServiceTerritoryMember> lstServiceTerritoryMember  = [SELECT Id FROM ServiceTerritoryMember ];

        
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 6);
        lstWorkOrder[1].ParentWorkOrderId = lstWorkOrder[0].Id;
        insert lstWorkOrder;
        
        Entitlement entObj = new Entitlement();
        entObj.Name = 'Test Entitlement';
        entobj.AccountId = lstAccount[2].Id;
        insert entobj;
         lstWorkOrder[0].EntitlementId = entobj.Id;
         update lstWorkOrder;

        
        Id woliRT = APS_TestDataFactory.getRecordTypeId('WorkOrderLineItem', 'Asset_Action');
        List<WorkOrderLineItem> lstWoli = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id, lstWorkOrder[0].Id, lstMaintenancePlan[0].Id, 3);
        lstWoli[0].RecordTypeId = woliRT;
        lstWoli[1].RecordTypeId = woliRT;
        lstWoli[2].RecordTypeId = woliRT;
        lstWoli[1].AssetId = lstAssetChildren[1].Id;
        lstWoli[2].AssetId = lstAssetChildren[2].Id;
        insert lstWoli;

        Id SARecordTypeId = APS_TestDataFactory.getRecordTypeId('ServiceAppointment', 'APS');
        List<ServiceAppointment> lstServiceAppointment = APS_TestDataFactory.createServiceAppointment(lstWorkOrder[0].Id, lstServiceTerritory[0].Id, SARecordTypeId, 4);
        lstServiceAppointment[0].Status = 'New';
        lstServiceAppointment[0].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[0].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        lstServiceAppointment[1].Status = 'Completed';
        lstServiceAppointment[1].Completion_Status__c = 'No Further Action Required';
        lstServiceAppointment[1].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[1].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        lstServiceAppointment[2].Status = 'Completed';
        lstServiceAppointment[2].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[2].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        lstServiceAppointment[3].Status = 'Completed';
        lstServiceAppointment[3].Completion_Status__c = 'No Access';
        lstServiceAppointment[3].Service_Resource__c = lstServiceResource[0].Id;
        lstServiceAppointment[3].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        
        //Create Entity Milestone
        //Create business hours
        //Create SLAProcess
        //Create Milestone Type

        Test.startTest();
            System.debug('***- SA -- ' + lstServiceAppointment);
            insert lstServiceAppointment;  

            AssignedResource assResource = new AssignedResource();
            assResource.ServiceResourceId = lstServiceResource[0].Id;
            assResource.ServiceAppointmentId = lstServiceAppointment[0].Id;
            insert assResource;

            //lstServiceAppointment[0].Status = 'Completed';
            //update lstServiceAppointment;
            System.assertEquals(lstServiceAppointment[0].RecordTypeId, SARecordTypeId);
        Test.stopTest();
        
    }
/*
    @isTest 
    static void validateWorkOrderSACreation() {
         
         Test.startTest();
         //create Account
         Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
         Account account1 = new Account();
         account1.Name = 'Test Account';
         account1.RecordTypeId = clientRecordTypeId;
         insert account1;
         system.debug('account1'+account1.Id);
         
         Id clientcontactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
         Contact con1 = new Contact();
         con1.FirstName = 'FirstOne';
         con1.LastName = 'LastOne';
         con1.MobilePhone = '1234567890';
        con1.Phone = '1234567890';
         con1.RecordTypeId = clientcontactRecordTypeId;
         insert con1;
         system.debug('Contact1'+con1.Id);
        
         Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();

         Account account2 = new Account();
         account2.Name = 'Test Account Two';
         account2.RecordTypeId = customerRecordTypeId;
         insert account2;
         
         //--Create Work type
         WorkType workType = new WorkType();
         workType.Name = 'CN (Install)';
         Date todayDate = Date.today();
         workType.EstimatedDuration = 5;
         
         insert workType;
         system.debug('workType'+workType.Id);
         
         //--create Price Book
         Pricebook2 priceBook = new Pricebook2();
         priceBook.Name = 'NSW-CN-Metro-PRICE-BOOK';
         priceBook.IsActive = true;
         insert priceBook;
         
         //--create operating hours
          OperatingHours operatingHours = new OperatingHours();
         operatingHours.Name = 'calender 1';
         insert operatingHours;
         
         //--create service territory
         ServiceTerritory serviceTerritory = new ServiceTerritory();
         serviceTerritory.Name = 'New South Wales';
         serviceTerritory.OperatingHoursId = operatingHours.Id;
         serviceTerritory.IsActive = true;
         insert serviceTerritory;
         
         Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
         System.debug('devRecordTypeId'+devRecordTypeId);
         
         //--create WOAR
         Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
         woar.Account__c = account1.Id;
         woar.RecordTypeId = devRecordTypeId;
         woar.Work_Area__c = 'Metro';
         woar.Work_Type__c = workType.Id;
         woar.Service_Territory__c = serviceTerritory.Id;
         woar.Price_Book__c = priceBook.Id;
         insert woar;
         
         //create case
         Case caseRecord =new Case();
         caseRecord.Work_Area__c = 'Metro';
         caseRecord.Status = 'New';
         //caseRecord.Task_Type__c = 'Hazard';
         caseRecord.Origin = 'Email';
         caseRecord.Work_Type__c = workType.Id;
         caseRecord.AccountId = account1.Id;
         caseRecord.ContactId = con1.Id;
         insert caseRecord;
       
         
         //insert WO
         WorkOrder workOrder = new WorkOrder();
         workOrder.Work_Area__c = 'Metro';
         workOrder.WorkTypeId = workType.Id;
         workOrder.ServiceTerritoryId = serviceTerritory.Id;
         workOrder.CaseId = caseRecord.Id;
         workOrder.city = 'City';
         workOrder.country ='Australia';
         workOrder.state='NSW';
         workOrder.street ='Street1';
         workOrder.PostalCode = '2132';
         workOrder.Client_Work_Order_Number__c  ='number';
         insert workOrder;
         
         //create SA
         ServiceAppointment sa1 = new ServiceAppointment();
         sa1.ParentRecordId = workOrder.Id;
         sa1.EarliestStartTime = todayDate.addDays(3);
         sa1.DueDate = todayDate.addDays(3);
         insert sa1;
         try{
             //create SA for same work order
             ServiceAppointment sa2 = new ServiceAppointment();
             sa2.ParentRecordId = workOrder.Id;
             sa2.EarliestStartTime = todayDate.addDays(3);
             sa2.DueDate = todayDate.addDays(3);
             insert sa2;
         }catch(Exception ex){
             String errorMessage = ex.getMessage();
             String existingSANumber = [SELECT AppointmentNumber FROM ServiceAppointment WHERE id =: sa1.Id ].AppointmentNumber;
             System.assert(errorMessage.contains(existingSANumber));
             
         }
         Test.stopTest();
    }
    */
}