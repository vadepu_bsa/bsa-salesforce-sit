public class WSNBNWOOutboundActionDTO {
    
        
    public String id {get;set;}  
    public String type_Z {get;set;} // in json: type
    public Specification specification {get;set;}  
    public String action {get;set;} 
    public List<Input_data_bindings> input_data_bindings {get;set;}     
    
    public WSNBNWOOutboundActionDTO(String id,String type_Z ,Specification specification,String action,List<Input_data_bindings> input_data_bindings){
        
        this.id = id;
        this.type_Z = type_Z;
        this.specification = specification;
        this.action = action;
        this.input_data_bindings = input_data_bindings;
    }


    public String getPayload(){ 
        
        system.debug('Payload'+json.serialize(this));  
        return json.serialize(this);               
    }
            
    public class Input_data_bindings {
        public String name {get;set;} 
        public String value {get;set;} 
        
        public Input_data_bindings(String name,String value){
            this.name= name;
            this.value= value;
            
        }
        
    }
    
    public class Specification {
        public String id {get;set;} 
        public String version {get;set;} 
        public Specification(String id,String version){
            this.id= id;
            this.version= version;
            
        }
        
    }
    
    
}