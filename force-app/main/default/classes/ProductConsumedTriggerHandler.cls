/**
* @author          Sunila.M
* @date            11/April/2019 
* @description     
*
* Change Date        Modified by         Description  
* 1/April/2019       Sunila.M           Created the Product Consumed trigger Handler 
* 10/May/2021        Ben Lee            Added condition to only update when there is a Pronto ID
**/
public class ProductConsumedTriggerHandler implements ITriggerHandler{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void BeforeInsert(List<SObject> newProductConsumed) 
    { 
        //--validate the product consumed records added for a work order
       map<Id,Id> workorderIdmap  = new map<id,id>();
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       List<ProductConsumed> APSRecordList = new List<ProductConsumed>();
       List<ProductConsumed> nonAPSRecordList = new List<ProductConsumed>();
        
       for(SObject obj : newProductConsumed) {
           ProductConsumed pcObj = (ProductConsumed)obj;
            if (pcObj.WorkOrderId != null ){
                workorderIdmap.put(pcObj.WorkOrderId,null);
            } 
       }
       List<WorkOrder> workorderList = [SELECT ID, recordtypeId FROM WorkOrder WHERE Id =: workorderIdmap.keyset()];
        for (integer i = 0; i< workorderList.size(); i++){
            workorderIdmap.put(workorderList[i].Id, workorderList[i].RecordTypeId);
        }
        for(SObject obj : newProductConsumed) {
           ProductConsumed pcObj = (ProductConsumed)obj;
            if (pcObj.WorkOrderId != null && workorderIdmap.ContainsKey(pcObj.WorkOrderId) && workorderIdmap.get(pcObj.WorkOrderId) == APSRecordTypeId  ){
                APSRecordList.add(pcObj);
            } else {
                nonAPSRecordList.add(pcObj);
            }
        }
        if (nonAPSRecordList.size()>0){
            ProductConsumedValidationHandler.validatePriceBookCode((List<ProductConsumed>)nonAPSRecordList);
            ProductConsumedValidationHandler.populateCorrelationID((List<ProductConsumed>)nonAPSRecordList);
        }
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
       List<ProductConsumed> productConsumedList = new List<ProductConsumed>();
       productConsumedList.addAll((List<ProductConsumed>)newItems.values());
          //--validate the product consumed records added for a work order
       map<Id,Id> workorderIdmap  = new map<id,id>();
      Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       List<ProductConsumed> APSRecordList = new List<ProductConsumed>();
       List<ProductConsumed> nonAPSRecordList = new List<ProductConsumed>();
        
       for(ProductConsumed pcObj : productConsumedList) {
           if (pcObj.WorkOrderId != null &&  pcObj.WorkOrder_RecordType__c == 'APS_Work_Order'){
                APSRecordList.add(pcObj);
            } else {
                nonAPSRecordList.add(pcObj);
            }
       }
        if (nonAPSRecordList.size() > 0){
            //--validate the product consumed records on update
            ProductConsumedValidationHandler.validatePriceBookCode((List<ProductConsumed>)nonAPSRecordList);
            // ProductConsumedValidationHandler.populateCorrelationID((List<ProductConsumed>)productConsumedList);
        }

    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       List<ProductConsumed> APSRecordList = new List<ProductConsumed>();
       List<ProductConsumed> nonAPSRecordList = new List<ProductConsumed>();
       Map<Id, string> productconsumedAPSIddMap = new map<id,string>();
       for(sobject obj : oldItems.values()) {
           ProductConsumed pcObj = (ProductConsumed)obj;
            if (pcObj.WorkOrderId != null &&  pcObj.WorkOrder_RecordType__c == 'APS_Work_Order'){
                ProductConsumed GetData = [SELECT Id, WorkOrder.Pronto_WO_ID__c FROM ProductConsumed WHERE Id =: pcObj.Id LIMIT 1];
                if(GetData != null && GetData.WorkOrder.Pronto_WO_ID__c != null){
                    APSRecordList.add(pcObj);
                     productconsumedAPSIddMap.put(pcObj.id, 'Delete');
                }
            } else {
                nonAPSRecordList.add(pcObj);
            }
       }
        if (nonAPSRecordList.size()>0){
            ProductConsumedValidationHandler.publishVanStockConsumptionPlatformEvent(nonAPSRecordList);
        }
        if (APSRecordList.size() > 0){
            ProntoProductConsumedSerialize.generateProntoProductPlatformEvent(productconsumedAPSIddMap.Keyset(),productconsumedAPSIddMap);
       }
    }  
    
    public void AfterInsert(Map<Id, SObject> newItems) {
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       List<ProductConsumed> APSRecordList = new List<ProductConsumed>();
       List<ProductConsumed> nonAPSRecordList = new List<ProductConsumed>();
       //Map<Id, string> productconsumedAPSIddMap = new map<id,string>();
        
       for(sobject obj : newItems.values()) {
            ProductConsumed pcObj = (ProductConsumed)obj;
            if (pcObj.WorkOrderId != null &&  pcObj.WorkOrder_RecordType__c == 'APS_Work_Order'){
                APSRecordList.add(pcObj);
                //productconsumedAPSIddMap.put(pcObj.id, 'Insert');
            } else {
                nonAPSRecordList.add(pcObj);
            }
       }
       //if (APSRecordList.size() > 0){
        //    ProntoProductConsumedSerialize.generateProntoProductPlatformEvent(productconsumedAPSIddMap.Keyset(),productconsumedAPSIddMap);
       //}
        
    } 

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       List<ProductConsumed> APSRecordList = new List<ProductConsumed>();
       List<ProductConsumed> nonAPSRecordList = new List<ProductConsumed>();
       Map<Id,string> productconsumedAPSIddMap = new map<id,string>();
        
       for(sobject obj : newItems.values()) {
           ProductConsumed pcObj = (ProductConsumed)obj;
           if (pcObj.WorkOrderId != null &&  pcObj.WorkOrder_RecordType__c == 'APS_Work_Order'){
                ProductConsumed GetData = [SELECT Id, WorkOrder.Pronto_WO_ID__c FROM ProductConsumed WHERE Id =: pcObj.Id LIMIT 1];
                if(GetData != null && GetData.WorkOrder.Pronto_WO_ID__c != null){
                    APSRecordList.add(pcObj);
                    productconsumedAPSIddMap.put(pcObj.id, 'Update');
                } else {
                    nonAPSRecordList.add(pcObj);
                }
           }

           if (APSRecordList.size() > 0){
               ProntoProductConsumedSerialize.generateProntoProductPlatformEvent(productconsumedAPSIddMap.Keyset(),productconsumedAPSIddMap);
           }
        
        }
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
}