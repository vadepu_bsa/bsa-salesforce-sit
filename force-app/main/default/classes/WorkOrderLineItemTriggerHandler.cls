/**
 * @author          Dildar Hussain
 * @date            26/Nov/2019 
 * 
 * Change Date    Modified by         Description  
 * 29/Jun/2020    Bohao Chen @IBM     JIRA FSL3-11 create equipment task work order line items
 * 
 **/
public without sharing class WorkOrderLineItemTriggerHandler implements ITriggerHandler{

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
     
        List<WorkOrderLineItem> woliList = new List<WorkOrderLineItem>();
        woliList.addAll((List<WorkOrderLineItem>)newItems.values());

        //get all the completed WOLIs to send email notification to the related clients        
        WorkOrderLineItemTriggerHelper.sendWoliEmailNotification(woliList,oldItems);     
          //Mahmood : Clone WOLI When Woli Status Changed to No Access. FSL3-144
        WorkOrderLineItemTriggerHelper.cloneWOLINoAccess(woliList,oldItems);
        WorkOrderLineItemTriggerHelper.updateParentWoliStatus(woliList,oldItems);
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void BeforeInsert(List<SObject> newItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {} public void AfterUndelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        System.debug('@WorkOrderLineItemTriggerHandler afterinsert');
        
        
        //#FSL3-416 - David A (IBM) - 01/Sep/20 - Create WOARS for Woli Import for AusGrid and BRS
        List<WorkOrderLineItem> lstImportWoli = new List<WorkOrderLineItem>();
        List<WorkOrderLineItem> lstNonImportWoli = new List<WorkOrderLineItem>();
        for(SObject sobjWoli : newItems.values()){
            WorkOrderLineItem woli = (WorkOrderLineItem)sobjWoli;
            if (woli.MenuWoliImport__c){
                lstImportWoli.add(woli);
            } else if(woli.Insert_by_WOG__c == false) {
                lstNonImportWoli.add(woli);
            }
        }

        //#FSL3-416
        if (lstImportWoli.size() > 0){
            WorkOrderLineItemTriggerHelper.createWOARforImportWOLI(lstImportWoli);
        } 

        if (lstNonImportWoli.size() > 0){
            // Bohao Chen @IBM 2020-06-29 @FSL3-11
            // Bohao Chen @IBM 2020-07-14 @FSL3-266
            // replace sync methods to async methods to avoid CPU time limit
            List<WorkOrderLineItem> equipmentTaskWolis = APS_TaskCreationHelper.generateEquipmentTasks(lstNonImportWoli);
            if(!equipmentTaskWolis.isEmpty()) {
                System.debug('*** Calling APS_TaskCreationDispatcher with tasktype - Equipment Task');
                List<WorkOrderLineItem> successRecord=new List<WorkOrderLineItem>();
                List<Id> failedRecords=new List<Id>();
                String message;
                System.enqueueJob(new APS_TaskCreationDispatcher('Equipment Task', equipmentTaskWolis, lstNonImportWoli, lstNonImportWoli,successRecord,failedRecords,message));
            }
        }
    }
}