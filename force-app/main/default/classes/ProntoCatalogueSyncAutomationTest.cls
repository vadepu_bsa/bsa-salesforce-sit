@isTest
public class ProntoCatalogueSyncAutomationTest {
    static testMethod void testValidEvent() {

        // Create a test event instance
        Pronto_Catalogue_Product_Created_Updated__e syncEvent = new Pronto_Catalogue_Product_Created_Updated__e();
        syncEvent.Product_Name__c = 'Test Product Name';
        syncEvent.Product_Code__c = 'Test Product Code';
        syncEvent.Client_ID__c = 'ABCDXYZ';
        syncEvent.Pack_Quantity__c = 2;
        syncEvent.Product_Expiry__c = '2020-11-14';
        syncEvent.Quantity_Unit_Of_Measure__c = 'Each';
        syncEvent.Product_Group__c = '';
        syncEvent.Correlation_ID__c = '88a57e4e-b22b-4683';
        syncEvent.Event_TimeStamp__c = '2018-02-03T00:35:11.000Z';

        Test.startTest();
        
            // Publish test event
            Database.SaveResult sr = EventBus.publish(syncEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());
        
        // Verify that a Product was created/updated by the trigger.
        List<Product2> products = [SELECT Id FROM Product2];
        // Validate that this Product was found
        System.assertEquals(1, products.size());

    }

    static testMethod void testInvalidEvent() {
        
        // Create a test event instance with invalid data.
        // Publishing the evnt without a Product Name
        // Publishing with a missing required field should fail.
        Pronto_Catalogue_Product_Created_Updated__e syncEvent = new Pronto_Catalogue_Product_Created_Updated__e();
        syncEvent.Product_Code__c = 'Test Invalid Product Code';
        syncEvent.Client_ID__c = 'ABCDPQRS';
        syncEvent.Pack_Quantity__c = 2;
        syncEvent.Product_Expiry__c = '2020-10-14';
        syncEvent.Quantity_Unit_Of_Measure__c = 'Each';
        syncEvent.Product_Group__c = '';
        syncEvent.Correlation_ID__c = '88a57e4e-b22b-78965';
        syncEvent.Event_TimeStamp__c = '2019-03-04T00:35:11.000Z';
        
        Test.startTest();
        
        // Publish test event
        Database.SaveResult sr = EventBus.publish(syncEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify that a Product was NOT created/updated by the trigger.
        List<Product2> products = [SELECT Id FROM Product2];
        // Validate that this product was not found
        System.assertEquals(0, products.size());
    }

}