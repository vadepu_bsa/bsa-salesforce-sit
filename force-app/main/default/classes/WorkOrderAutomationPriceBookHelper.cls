/**
* @author          Sunila.M
* @date            08/Feb/2019 
* @description     This will query the WOAR record based on the below query criteria and find the Price Book for the WO.
*                  WOAR.Account = WorkOrder.Case.Account
WOAR.WorkType = WorkOrder.WorkType
WOAR.WorkArea = WorkOrder.WorkArea
WOAR.ServiceTerritory = WorkOrder.ServiceTerritory
WOAR.RecordType = "Price Book Automation"
*
* Change Date    Modified by         Description  
* 08/Feb/2019      Sunila.M        Trigger handler for WO before create/update
* 07/July/2020	Sindhu Madhusudan	Added CUI Record Type Id for WOLI creation
**/
public class WorkOrderAutomationPriceBookHelper {
    
    public static Boolean workOrderUpdated = false;
    
    public static void syncSAWithParentWorkOrder(Map<Id,SObject> newWorkOrderMap,Map<Id,SObject> oldWorkOrderMap){
        
        Set<Id> SAWOIDSet= new Set<Id>();
        Map<id,String> parentWOIdToCurrentStatusMap = new Map<Id,String>();
        List<ServiceAppointment> serviceAppointmentListToUpdate  =  new List<ServiceAppointment>();
        List<WorkOrder> workOrderListToUpdate  = new List<WorkOrder>();
        
        try{
            
            for(Id sobj : newWorkOrderMap.keyset()){
                
                WorkOrder woObj = (WorkOrder)newWorkOrderMap.get(sobj);                    
                if(woObj.ParentWorkOrderId == null && String.isNotBlank(woObj.Status) && woObj.Status != ((WorkOrder)oldWorkOrderMap.get(woObj.id)).Status && BSA_ConstantsUtility.ALLOWED_STATUS_SET.contains(woObj.Status)){
                    // parentWOIDSet.add(woObj.Id);
                    parentWOIdToCurrentStatusMap.put(woObj.Id,woObj.Status);
                    
                }
                
            }
            if(parentWOIdToCurrentStatusMap.size() >0){
                
                for(WorkOrder woObj: [SELECT Id,Status,ParentWorkOrderId,(SELECT Id,Status,Work_order__r.Id,Work_order__r.ParentWorkOrder.Status,Work_order__r.ParentWorkOrderId FROM ServiceAppointments) FROM Workorder WHERE ParentWorkOrderId IN : parentWOIdToCurrentStatusMap.keyset() ]){
                    if(woObj.ServiceAppointments != null && woObj.ServiceAppointments.size()>0){
                        for(ServiceAppointment servAppObj : woObj.ServiceAppointments){
                            if(!BSA_ConstantsUtility.NOT_ALLOWED_STATUS_SET.contains(servAppObj.status)){                            
                                servAppObj.Status = parentWOIdToCurrentStatusMap.get(servAppObj.Work_order__r.ParentWorkOrderId);
                                serviceAppointmentListToUpdate.add(servAppObj);
                            }
                            
                        }
                    }
                    else{
                        
                        if(!BSA_ConstantsUtility.NOT_ALLOWED_STATUS_SET.contains(woObj.status)){
                            
                            woObj.status = parentWOIdToCurrentStatusMap.get(woObj.ParentWorkOrderId);
                            workOrderListToUpdate.add(woObj);
                        }
                    }
                    
                }
            }
            
            System.debug('serviceAppointmentListToUpdate'+serviceAppointmentListToUpdate);
            System.debug('workOrderListToUpdate'+workOrderListToUpdate);
            
            if(!serviceAppointmentListToUpdate.isEmpty()){
                
                UPDATE serviceAppointmentListToUpdate;
            }
            
            if(!workOrderListToUpdate.isEmpty()){
                
                UPDATE workOrderListToUpdate;
            }
        } catch(Exception excp) {
            
            System.debug('Exception occured while syncing Service appointment with WO status'+excp.getMessage()+excp.getStackTraceString());
        }
        
        
    }
    
    
    public static void createNbnWorkOrders(Map<Id,SObject> newWorkOrderMap) {
        
        List<ItemInvolvesResource> itemResourceWrapper = new List<ItemInvolvesResource>();
        List<ActivityInstructions> activityWrapper = new  List<ActivityInstructions>();
        List<WorkOrder> childWorkOrderToInsert = new List<WorkOrder>();
        List<Item_Involves_Resource__c> itemResourceToUpsert = new List<Item_Involves_Resource__c>();
        List<Activity_Instructions__c> activityInstructionToUpsert = new List<Activity_Instructions__c>();
        Id hfcRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.HFC_CHILD).getRecordTypeId();
        Id fttnRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(BSA_ConstantsUtility.FTTN_CHILD).getRecordTypeId();
        
        
        
        for(SObject obj : newWorkOrderMap.values()) {
            WorkOrder woObj = (WorkOrder)obj;
            String msg = ''; //String to write a successful/failure message
            String logLevel = ''; // String to capture Log Level(Info/Warning) on the Application Log object
            String jsonPayload = ''; //String to build a JSON payload
            List<WorkOrder> genericList = new List<WorkOrder>();
            genericList.add(woObj);
            // Build a JSON string
            jsonPayload = JSON.serialize(genericList);
            System.debug('WorkOrder with Id ' + woObj.Id +' was created ');
            msg = 'WorkOrder with Id ' + woObj.Id +' was created ';
            logLevel = 'Info';
            
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            ApplicationLogUtility.addException(woObj.Id, jsonPayload, msg, logLevel, 'WorkOrder');
            for(integer i = 0; i < 2; i++) {
                WorkOrder childWo  = woObj.clone(false, false, false, false);
                childWo.ParentWorkOrderId = woObj.Id;
                childWo.NBN_Reference_ID__c = '';
                childWo.NBN_Activity_Status_Info__c = '';
                childWo.Client_Work_Order_Number__c = '';
                childWo.NBN_Parent_Work_Order_Id__c = '';
                childWo.NBN_NA_JSON_Payload__c = '';
                childWo.Status = 'New';
                if(woObj.Primary_Access_Technology__c == BSA_ConstantsUtility.HFC_TECH) {
                    childWo.RecordTypeId = hfcRecordTypeId;
                }
                else if(woObj.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTN_TECH) {
                    childWo.RecordTypeId = fttnRecordTypeId;
                }
                childWorkOrderToInsert.add(childWo);
            }
            
            if(woObj.Item_Involves_Resource_Info__c != null) {
                itemResourceWrapper = (List<ItemInvolvesResource>)System.JSON.deserialize(woObj.Item_Involves_Resource_Info__c, List<ItemInvolvesResource>.class);
                System.debug(' itemResourceWrapper>>>>> '+itemResourceWrapper);
            }
            
            if(!itemResourceWrapper.isEmpty()) {
                for(ItemInvolvesResource wrapObj : itemResourceWrapper) {
                    Item_Involves_Resource__c item = new Item_Involves_Resource__c();
                    item.GeographicDatum__c = wrapObj.GeographicDatum;
                    item.Id__c = wrapObj.Id;
                    item.Type__c = wrapObj.Type;
                    item.SubType__c = wrapObj.SubType;
                    item.Resource_Geolocation__latitude__s = wrapObj.Latitude;
                    item.Resource_Geolocation__longitude__s = wrapObj.Longitude;
                    item.Work_Order__c = woObj.Id;
                    
                    itemResourceToUpsert.add(item);
                }
            }
            
            if(woObj.NBN_Activity_Instructions__c != null) {
                activityWrapper = (List<ActivityInstructions>)System.JSON.deserialize(woObj.NBN_Activity_Instructions__c, List<ActivityInstructions>.class);
                System.debug(' activityWrapper>>>>> '+activityWrapper);
            }
            
            if(!activityWrapper.isEmpty()) {
                for(ActivityInstructions wrapObj : activityWrapper) {
                    Activity_Instructions__c activity = new Activity_Instructions__c();
                    activity.Id__c = wrapObj.Id;
                    activity.Type__c = wrapObj.Type;
                    activity.Value__c = wrapObj.Value;
                    activity.External_Key__c = wrapObj.Id+'-'+woObj.Id;
                    activity.Work_Order__c = woObj.Id;
                    
                    activityInstructionToUpsert.add(activity);
                }
            }
            
        }
        
        //Insert the application log entry in the SF database
        ApplicationLogUtility.saveExceptionLog();
        
        system.debug('childWorkOrderToInsert --> '+childWorkOrderToInsert);
        
        if(!childWorkOrderToInsert.isEmpty()) {
            insert childWorkOrderToInsert;
        }
        
        system.debug('itemResourceToUpsert --> '+itemResourceToUpsert);
        
        if(!itemResourceToUpsert.isEmpty()) {
            upsert itemResourceToUpsert Id__c;
        }
        
        system.debug('activityInstructionToUpsert --> '+activityInstructionToUpsert);
        
        if(!activityInstructionToUpsert.isEmpty()) {
            upsert activityInstructionToUpsert External_Key__c;
        }
        
    }
    
    public static void createWolisForNbnchildWo(Map<Id,SObject> newWorkOrderMap) {
        
        //get the WOAR record type Id
        Id woliAutomationRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('WOLI Automation').getRecordTypeId();
        
        //holds the WorkOrder.WorkType
        Set<Id> workTypeIDs = new Set<Id>();
        
        //holds the WorkOrder.ServiceContract.Account
        Set<Id> accountIDs = new Set<Id>();
        
        //holds the child workorders
        List<WorkOrder> childWoList = new List<WorkOrder>();
        
        //holds the WorkOrderLineItems to create
        List<WorkOrderLineItem> woliToCreateList = new List<WorkOrderLineItem>();
        
        //get the WOLI CUI record type Id
        Id woliCUIRecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByName().get('CUI').getRecordTypeId();
        
        //holds the parent workorders IDs
        Set<Id> parentWoIDs = new Set<Id>();
        
        //holds the parent NBN workorders to update
        List<WorkOrder> parentWoList = new List<WorkOrder>();
        
        //holds the Action NBN Accept To NBN Platform Event
        List<sobject> platformEventsToPublish = new List<sobject>();
        
        Boolean isWOARExists = false, hasError = false, woUpdated;
        
        for(WorkOrder woObj : [SELECT Id, WorkTypeId, ServiceContract.AccountId, ParentWorkOrderId FROM WorkOrder WHERE Id IN : newWorkOrderMap.values()]) {
            if(woObj.WorkTypeId != null) {
                workTypeIDs.add(woObj.WorkTypeId);
            }
            if(woObj.ServiceContract.AccountId != null) {
                accountIDs.add(woObj.ServiceContract.AccountId);
            }
            childWoList.add(woObj);
            parentWoIDs.add(woObj.ParentWorkOrderId);
        }
        
        System.debug('childWoList --> '+childWoList);
        
        //--query the Work Order Automation Rule record to get the WOLI Type
        for(Work_Order_Automation_Rule__c eachWOAR : [SELECT Id, Work_Type__c, Account__c, Type__c, Action_Type__c, Action__c, Required_Pre_Finish__c, Required_Pre_Notification__c FROM Work_Order_Automation_Rule__c WHERE 
                                                      RecordTypeId =: woliAutomationRecordTypeId 
                                                      AND Work_Type__c IN : workTypeIDs 
                                                      AND Account__c IN : accountIDs
                                                      AND Action_Type__c = 'Create WOLI' ORDER BY Sequence__c ASC] ){
                                                          
                                                          isWOARExists = true; 
                                                          for(WorkOrder woObj : childWoList) {
                                                              
                                                              if(woObj.ServiceContract.AccountId == eachWOAR.Account__c && woObj.WorkTypeId == eachWOAR.Work_Type__c) {
                                                                  WorkOrderLineItem woli = new WorkOrderLineItem();
                                                                  woli.Type__c = eachWOAR.Type__c;
                                                                  woli.Description = eachWOAR.Action__c;
                                                                  woli.WOrkOrderId = woObj.Id;
                                                                  woli.WorkTypeId = eachWOAR.Work_Type__c;
                                                                  woli.Required_Pre_Finish__c = eachWOAR.Required_Pre_Finish__c;
                                                                  woli.Required_Pre_Notification__c = eachWOAR.Required_Pre_Notification__c;
                                                                  woli.RecordTypeId = woliCUIRecordTypeId;
                                                                  woliToCreateList.add(woli);
                                                              }
                                                              
                                                          }
                                                          
                                                      }
        
        if(!woliToCreateList.isEmpty()) {
            try{
                insert woliToCreateList;
            }
            catch(Exception e) {
                hasError = true;
                System.debug('Exception is -->'+e);
            }
        }
        
        //Changing the NBN Parent Work Order status to Acknowledged and publish a PE to NBN for Acknowledgement
        if(hasError == false) {
            if(!parentWoIDs.isEmpty()) {
                for(WorkOrder obj: [SELECT Id, NBN_Activity_State_Id__c, NBN_Activity_Instantiated_By__c, NBN_Field_Work_Specified_By_Version__c, NBN_Parent_Work_Order_Id__c, Primary_Access_Technology__c,
                                    NBN_Field_Work_Specified_By_Id__c, Classification__c, Based_Revision_Number__c, Client_Work_Order_Number__c, NBN_Activity_Start_Date_Time__c, NBN_Field_Work_Specified_By_Type__c,
                                    CorrelationID__c, LastModifiedDate, NBN_Action__c, NBN_Field_Work_Specified_By_Category__c
                                    FROM WorkOrder WHERE Id IN : parentWoIDs]) {
                                        obj.NBN_Activity_State_Id__c = 'ACKNOWLEDGED:Step_SDP_Tech_On_Site';
                                        Action_NBN_Accept_To_NBN__e event = new Action_NBN_Accept_To_NBN__e();
                                        event.NBN_Field_Work_Specified_By_Version__c = obj.NBN_Field_Work_Specified_By_Version__c;
                                        event.NBN_Field_Work_ID__c = obj.NBN_Parent_Work_Order_Id__c;
                                        event.NBN_Related_ID__c = obj.NBN_Parent_Work_Order_Id__c;
                                        event.NBN_Field_Work_Specified_By_Id__c = obj.NBN_Field_Work_Specified_By_Id__c;
                                        event.NBN_Activity_Instantiated_By__c = obj.NBN_Activity_Instantiated_By__c;
                                        event.NBN_Activity_Status_Info_ID__c = obj.Client_Work_Order_Number__c;
                                        event.NBN_Related_Sub_Id__c = obj.Id;
                                        event.NBN_Occurs_Within_Activity_ID__c = obj.Client_Work_Order_Number__c;
                                        event.CorrelationID__c = UtilityClass.getUUID();
                                        //event.NBN_Activity_State_Date_Time__c = obj.NBN_Activity_Start_Date_Time__c;
                                        event.NBN_Activity_State_Date_Time__c = DateTime.valueOfGMT(String.valueOf(obj.NBN_Activity_Start_Date_Time__c));
                                        event.NBN_Field_Work_Specified_By_Type__c = obj.NBN_Field_Work_Specified_By_Type__c;
                                        event.NBN_Action_Date__c = obj.LastModifiedDate;
                                        event.Action__c = obj.NBN_Action__c;
                                        event.NBN_Field_Work_Specified_By_Category__c = obj.NBN_Field_Work_Specified_By_Category__c;
                                        event.Revision_Number__c = obj.Based_Revision_Number__c;
                                        /*event.NBN_Activity_Change_State_Id__c = 'ACKNOWLEDGED';
event.NBN_Activity_Change_Step_Id__c = 'Step_SDP_Tech_On_Site';
event.NBN_Activity_Change_Action_Id__c = 'Action_Accept_Activity';*/
                                        parentWoList.add(obj);
                                        platformEventsToPublish.add(event);
                                    }
            }
        }
        
        if(!parentWoList.isEmpty()) {
            try{
                workOrderUpdated = true;
                update parentWoList;
                woUpdated = true;
            }
            
            catch(Exception e) {
                System.debug('Exception is -->'+e);
                woUpdated = false;
            }
        }
        
        if(woUpdated == true) {
            String jsonPayload = ''; //String to build a JSON payload
            //Pass the sobject list to JSON generator apex class to build a JSON string
            jsonPayload = JSON.serializePretty(parentWoList);
            
            String msg = ''; //String to write a successful/failure message
            String logLevel = ''; // String to capture Log Level(Info/Warning) on the Application Log object
            
            for(WorkOrder obj : parentWoList) {
                msg = 'Action_NBN_Accept_ToNBN : ACKNOWLEDGED';
                logLevel = 'Info';
                //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
                ApplicationLogUtility.addException(obj.Id, jsonPayload, msg, logLevel, 'WorkOrder');
            }
            
            //Insert the application log entry in the SF database
            ApplicationLogUtility.saveExceptionLog();
            
            if(!platformEventsToPublish.isEmpty()) {
                System.debug('Acknowledgement Event To Publish -->'+platformEventsToPublish);
                ApplicationLogUtility.publishEvents(platformEventsToPublish);
            }
        }
        
    }
    
    public static void getWOARPriceBook(Boolean isInsertScenario , List<WorkOrder> workOrderList, Map<Id, SObject> oldWorkOrderMap){
        
        //--holds the WO.workArea
        List<String> workAreaList = new List<String>();
        
        //--holds the WO to be processed
        List<WorkOrder> workOrdersToBeProcessed = new List<WorkOrder>();
        
        //--holds the WO.serviceTerritory
        List<Id> serviceTerritoryList = new List<Id>();
        
        //--holds the WO.Case Account
        List<Id> WOCaseIdList = new List<Id>();
        
        //--holds the WO.workType
        List<Id> workTypeList = new List<Id>();
        
        //--holds the WO Case as key and case.Account as value
        Map<Id, Id> WOCaseIdAccountMap = new Map<Id, Id>();
        
        //--holds the WO ServiceContract as key and ServiceContract.Account as value
        Map<Id, Id> WOServiceCOntractIdAccountMap = new Map<Id, Id>();
        
        //--holds the WO.ServiceContract
        List<Id> WOServiceContractIdList = new List<Id>();
        
        //--iterate the SA record list and check WO Price Book fields
        for(WorkOrder eachWorkOrder : workOrderList) {
            
            if(!isInsertScenario) {
                
                // Get Old WO
                WorkOrder oldWO = (WorkOrder) oldWorkOrderMap.get(eachWorkOrder.Id);
                
                // Check if the Price Book Deciding Criteria has changed
                if(eachWorkOrder.Work_Area__c == oldWO.Work_Area__c 
                   && eachWorkOrder.WorkTypeId == oldWO.WorkTypeId 
                   && eachWorkOrder.ServiceTerritoryId ==  oldWO.ServiceTerritoryId){
                       continue;
                   }
            } 
            workOrdersToBeProcessed.add(eachWorkOrder);
            workAreaList.add(eachWorkOrder.Work_Area__c);
            serviceTerritoryList.add(eachWorkOrder.ServiceTerritoryId);
            
            if(eachWorkOrder.ServiceContractId != null) {
                WOServiceContractIdList.add(eachWorkOrder.ServiceContractId);
            }
            
            if(eachWorkOrder.CaseId != null) {
                WOCaseIdList.add(eachWorkOrder.CaseId);
            }
            
            workTypeList.add(eachWorkOrder.WorkTypeId);
        }
        
        // if no WO found to update for PB, then return
        if (workOrdersToBeProcessed.size() == 0) {
            return;
        }
        
        //
        if(!serviceTerritoryList.isEmpty()) {
            for(ServiceTerritory obj : [SELECT Id, ParentTerritoryId FROM ServiceTerritory WHERE Id IN : serviceTerritoryList AND ParentTerritoryId != null]) {
                serviceTerritoryList.add(obj.ParentTerritoryId);
            }
        }
        
        //--get the Work order ServiceContract Account and build ServiceContract id Account Id map
        List<Id> serviceContractAccountId = new List<Id>();
        if(!WOServiceContractIdList.isEmpty()) {
            for(ServiceContract eachSC : [SELECT Id, AccountId FROM ServiceContract WHERE Id IN : WOServiceContractIdList]){
                serviceContractAccountId.add(eachSC.AccountId);
                WOServiceContractIdAccountMap.put(eachSC.Id, eachSC.AccountId);
            }
        }
        
        
        //--get the Work order Case Account and build case id Account Id map
        List<Id> caseAccountId = new List<Id>();
        if(!WOCaseIdList.isEmpty()) {
            for(Case eachCase : [SELECT Id, AccountId FROM Case where Id IN : WOCaseIdList]){
                caseAccountId.add(eachCase.AccountId);
                WOCaseIdAccountMap.put(eachCase.Id, eachCase.AccountId);
            }
        }
        
        //get the WOAR record type Id
        Id priceBookAutomationRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
        
        //--holds to check WOAR record exists
        Boolean isWOARExists = false;
        
        List<String> updatedWorkOrderDetails = new List<String>();        
        
        //--query the Work Order Automation Rule record to get the Price Book
        for(Work_Order_Automation_Rule__c eachWOAR : [SELECT Id, Price_Book__c, Work_Area__c, Service_Territory__c, Work_Type__c, Account__c FROM Work_Order_Automation_Rule__c WHERE RecordTypeId =:priceBookAutomationRecordTypeId AND /*Work_Area__c IN:workAreaList AND*/ Service_Territory__c IN: serviceTerritoryList AND Work_Type__c IN:workTypeList AND (Account__c  IN : caseAccountId OR Account__c IN: serviceContractAccountId) AND Price_Book__r.isActive = true] ){
            
            isWOARExists = true; 
            
            for(WorkOrder eachWorkOrder : workOrdersToBeProcessed){
                //--associate the Price book if exists else add validation error message
                if(/*eachWorkOrder.Work_Area__c == eachWOAR.Work_Area__c &&*/ eachWorkOrder.WorkTypeId == eachWOAR.Work_Type__c && (eachWorkOrder.ServiceTerritoryId ==  eachWOAR.Service_Territory__c || eachWorkOrder.Parent_Service_Territory_Id__c ==  eachWOAR.Service_Territory__c) && (WOCaseIdAccountMap.get(eachWorkOrder.CaseId) == eachWOAR.Account__c || WOServiceContractIdAccountMap.get(eachWorkOrder.ServiceContractId) == eachWOAR.Account__c)) {
                    eachWorkOrder.Pricebook2Id = eachWOAR.Price_Book__c;
                    updatedWorkOrderDetails.add(eachWorkOrder.Work_Area__c+'#'+eachWorkOrder.WorkTypeId+'#'+eachWorkOrder.ServiceTerritoryId+'#'+WOCaseIdAccountMap.get(eachWorkOrder.CaseId));
                }
                else if(eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.NHUR || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.CSLL || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTP || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTC || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTB || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTN_TECHONE || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.HFC_TECH || eachWorkOrder.Primary_Access_Technology__c.contains(BSA_ConstantsUtility.FIBRE_TECH)) {
                    eachWorkOrder.Pricebook2Id = System.Label.UnAllocated_PriceBook_Id;
                }
            }
        }
        
        if(isWOARExists == false) {
            for(WorkOrder eachWorkOrder : workOrdersToBeProcessed) {
                eachWorkOrder.Pricebook2Id = System.Label.UnAllocated_PriceBook_Id;
            }
        }
        
        //if not WOAR record found iterate through WO records and add the validation error message
        for(WorkOrder eachWorkOrder : workOrdersToBeProcessed){
            system.debug('updatedWorkOrderDetails'+updatedWorkOrderDetails);
            if(String.isNotBlank(eachWorkOrder.Primary_Access_Technology__c) && (eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.NHUR || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.CSLL || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTP || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTC || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTB || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.FTTN_TECHONE || eachWorkOrder.Primary_Access_Technology__c == BSA_ConstantsUtility.HFC_TECH || eachWorkOrder.Primary_Access_Technology__c.contains(BSA_ConstantsUtility.FIBRE_TECH)) ) continue;
            if(!updatedWorkOrderDetails.contains(eachWorkOrder.Work_Area__c+'#'+eachWorkOrder.WorkTypeId+'#'+eachWorkOrder.ServiceTerritoryId+'#'+WOCaseIdAccountMap.get(eachWorkOrder.CaseId))) {
                eachWorkOrder.addError('No matching Price Book found as per the given criteria. Please raise it to your team lead.');
            }
        }
    }    
    
    public class ItemInvolvesResource {
        public String Id;
        public String Type;
        public String SubType;
        public String GeographicDatum;
        public Double Latitude;
        public Double Longitude;
    }
    
    public class ItemInvolvesResourceList {
        public List<Item_Involves_Resource__c> ItemInvolvesResource;
    }
    
    
    public class ActivityInstructions {
        public String Id;
        public String Type;
        public String Value;
    }
    
    public class ActivityInstructionsList {
        public List<Activity_Instructions__c> ActivityInstructions;
    }
    
    
}