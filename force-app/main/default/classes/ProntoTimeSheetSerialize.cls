/**
 * @File Name          : ProntoTimeSheetSerialize.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 11/08/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/08/2020         Tom Henson             Initial Version
**/
public class ProntoTimeSheetSerialize {

    public static void generateProntoTimeSheetPlatformEvent (set<id> timeSheetEntryIdSet){
        set<id> timeSheetIdSet = new set<id>();
        for (TimeSheetEntry tseObj : [SELECT Id, TimeSheetId from TimeSheetEntry WHERE ID =:timeSheetEntryIdSet]){
            if (tseObj.TimeSheetId != null){
                timeSheetIdSet.add(tseObj.TimeSheetId);
            }
        }
        List<TimeSheetDetails> finalTimeSheetList = timeSheetSerialize(timeSheetIdSet);
        List<TimesheetEntryDetails> finalEntryList = timeSheetEntrySerialize(timeSheetEntryIdSet);
        
        List<Action_Timesheet_From_BSA__e> InsertplatformEventList = generatePlatformEvents(finalTimeSheetList,finalEntryList);
        string logs =  JSON.serializePretty(InsertplatformEventList); 
        // System.Debug('!!! Full Output');  
        for (Integer i = 0; i < logs.length(); i=i+300) {
        	Integer iEffectiveEnd = (i+300 > (logs.length()-1) ? logs.length()-1 : i+300);
        	// System.debug(logs.substring(i,iEffectiveEnd));
   		  }
        if (InsertplatformEventList.size() > 0 ) {
            List<Database.SaveResult> results = EventBus.publish(InsertplatformEventList);
        }
    } 
    
    public static List<TimeSheetDetails>  timeSheetSerialize(set<id> timeSheetIdset){
        
        List<TimeSheet> timeSheetObj = [SELECT Status,StartDate,EndDate,TimeSheetEntryCount,TimeSheetNumber, ServiceResource.Employee_Code__c, Time_Sheet_Type__c,
                                        ServiceResource.RelatedRecord.Name, ServiceResource.RelatedRecord.Email, ServiceResource.RelatedRecord.Phone
                                        FROM TimeSheet 
                                        WHERE Id =: timeSheetIdset]; 
        List<TimeSheetDetails> timeSheetDetailsList = new List<TimeSheetDetails>();
        for (integer i = 0; i < timeSheetObj.size(); i++){
            TimeSheetDetails tObj = new TimeSheetDetails();
            
            tobj.Id = (timeSheetObj[i].Id != null) ? timeSheetObj[i].Id  : null;
            tObj.ServiceResourceId = (timeSheetObj[i].ServiceResource.Employee_Code__c != null) ? timeSheetObj[i].ServiceResource.Employee_Code__c  : null;
            tObj.ServiceResourceName = (timeSheetObj[i].ServiceResource.RelatedRecord.Name != null) ? timeSheetObj[i].ServiceResource.RelatedRecord.Name  : null;
        	tObj.ServiceResourceEmail =  (timeSheetObj[i].ServiceResource.RelatedRecord.Email != null) ? timeSheetObj[i].ServiceResource.RelatedRecord.Email : null;
       		tObj.ServiceResourcePhone = (timeSheetObj[i].ServiceResource.RelatedRecord.Phone != null) ? timeSheetObj[i].ServiceResource.RelatedRecord.Phone  : null;
            tObj.Status = (timeSheetObj[i].Status != null) ? timeSheetObj[i].Status  : null;
            tObj.StartDate = (timeSheetObj[i].StartDate != null) ? string.valueof( timeSheetObj[i].StartDate)  : null;
            tObj.EndDate  = (timeSheetObj[i].EndDate != null) ? string.valueof(timeSheetObj[i].EndDate ) : null;
            tObj.TimeSheetEntryCount = (timeSheetObj[i].TimeSheetEntryCount != null) ? string.valueof(timeSheetObj[i].TimeSheetEntryCount)  : null;
            tObj.TimeSheetNumber = (timeSheetObj[i].TimeSheetNumber != null) ? timeSheetObj[i].TimeSheetNumber  : null;
            tObj.TimesheetType = (timeSheetObj[i].Time_Sheet_Type__c != null) ? timeSheetObj[i].Time_Sheet_Type__c  : null;
            
            timeSheetDetailsList.add(tObj);
            system.debug('JSON Serialize ' + JSON.serialize(tObj,false));
        }
        system.debug('JSON Serialize ALL ' + JSON.serialize(timeSheetDetailsList,false));
        return timeSheetDetailsList;
    }

    public static List<TimeSheetEntryDetails>  timeSheetEntrySerialize(set<id> timeSheetEntryIdSet){
        
        List<TimeSheetEntry> timeSheetEntryObj = [SELECT Description,DurationInMinutes,Employee_Number__c,EndTime,Labour_Type__c,Latitude__c,Longitude__c,
                                            Minutes_Worked__c,Planned_Duration__c,Resource_Absence_Type__c,StartTime,Status,Subject,TimeSheetEntryNumber,
                                            TimeSheetId,Type, WorkOrder.Client_Work_Order_Number__c,WorkOrderLineItemId,WO_Duration_Type__c,WO_Duration__c
                                            , Duration_APS__c, WorkOrder.WorkOrderNumber
                                            FROM TimeSheetEntry 
                                            WHERE ID =: timeSheetEntryIdSet and Status ='Submitted']; 
        List<TimeSheetEntryDetails> timeSheetEntryDetailList = new List<TimeSheetEntryDetails>();
        for (integer i = 0; i < timeSheetEntryObj.size(); i++){
            TimeSheetEntryDetails tObj = new TimeSheetEntryDetails();
            tObj.Description = (timeSheetEntryObj[i].Description != null) ? timeSheetEntryObj[i].Description : null;
            tObj.DurationInMinutes = (timeSheetEntryObj[i].DurationInMinutes != null) ? string.valueof(timeSheetEntryObj[i].DurationInMinutes) : null;
            tObj.Employee_Number = (timeSheetEntryObj[i].Employee_Number__c != null) ? string.valueof(timeSheetEntryObj[i].Employee_Number__c) : null;
            tObj.EndTime = (timeSheetEntryObj[i].EndTime != null) ? string.valueof(timeSheetEntryObj[i].EndTime) : null;
            tObj.Labour_Type = (timeSheetEntryObj[i].Labour_Type__c != null) ? string.valueof(timeSheetEntryObj[i].Labour_Type__c) : null;
            tObj.Latitude = (timeSheetEntryObj[i].Latitude__c != null) ? timeSheetEntryObj[i].Latitude__c : null;
            tObj.Longitude = (timeSheetEntryObj[i].Longitude__c != null) ? timeSheetEntryObj[i].Longitude__c : null;
            tObj.Minutes_Worked = (timeSheetEntryObj[i].Minutes_Worked__c != null) ? string.valueof(timeSheetEntryObj[i].Minutes_Worked__c) : null;
            tObj.Planned_Duration = (timeSheetEntryObj[i].Planned_Duration__c != null) ? string.valueof(timeSheetEntryObj[i].Planned_Duration__c) : null;
            tObj.Resource_Absence_Type= (timeSheetEntryObj[i].Resource_Absence_Type__c != null) ? timeSheetEntryObj[i].Resource_Absence_Type__c : null;
            tObj.StartTime = (timeSheetEntryObj[i].StartTime != null) ? string.valueof(timeSheetEntryObj[i].StartTime) : null;
            tObj.Status = (timeSheetEntryObj[i].Status != null) ? timeSheetEntryObj[i].Status : null;
            tObj.Subject = (timeSheetEntryObj[i].Subject != null) ? timeSheetEntryObj[i].Subject : null;
            tObj.TimeSheetEntryNumber = (timeSheetEntryObj[i].TimeSheetEntryNumber != null) ? string.valueof(timeSheetEntryObj[i].TimeSheetEntryNumber) : null;
            tObj.TimeSheetId = (timeSheetEntryObj[i].TimeSheetId != null) ? timeSheetEntryObj[i].TimeSheetId : null;
            tObj.Type = (timeSheetEntryObj[i].Type != null) ? timeSheetEntryObj[i].Type : null;
            tObj.WorkOrderId = (timeSheetEntryObj[i].WorkOrder.WorkOrderNumber != null) ? timeSheetEntryObj[i].WorkOrder.WorkOrderNumber : null;
            tObj.WorkOrderLineItemId = (timeSheetEntryObj[i].WorkOrderLineItemId != null) ? timeSheetEntryObj[i].WorkOrderLineItemId : null;
            tObj.WO_Duration_Type = (timeSheetEntryObj[i].WO_Duration_Type__c != null) ? timeSheetEntryObj[i].WO_Duration_Type__c : null;
            if (timeSheetEntryObj[i].Duration_APS__c != null){ 
              decimal DurationInMinutes = timeSheetEntryObj[i].Duration_APS__c;
              Decimal tsDurationValue =   DurationInMinutes/60;
              tsDurationValue = tsDurationValue.setScale(2);
              tObj.WO_Duration = string.valueof(tsDurationValue)  ;
            }
          
            
            timeSheetEntryDetailList.add(tObj);
            system.debug('JSON Serialize TSE ' + JSON.serialize(tObj,false));
        }
        system.debug('JSON Serialize ALL TSE ' + JSON.serialize(timeSheetEntryDetailList,false));
        return timeSheetEntryDetailList;
    }
    
    Public Static List<Action_Timesheet_From_BSA__e> generatePlatformEvents(List<TimeSheetDetails> finalTimeSheetList, List<TimeSheetEntryDetails> finalTimeSheetEntryDetailList){
       List<Action_Timesheet_From_BSA__e> returnlist = new List<Action_Timesheet_From_BSA__e>();

        for (integer i = 0 ; i < finalTimeSheetEntryDetailList.size(); i++){
            Action_Timesheet_From_BSA__e eventObj = new Action_Timesheet_From_BSA__e();
            
            TimeSheetDetails relatedTimesheet = new TimeSheetDetails();
            for (TimeSheetDetails tse : finalTimeSheetList){
                if(tse.Id == finalTimeSheetEntryDetailList[i].TimesheetId){
                    relatedTimesheet = tse ;
                }
            }

            eventObj.Action__c = 'Generate_TimeSheet';
            eventObj.CorrelationId__c  = UtilityClass.getUUID();
            eventObj.TimeSheet__c = '"timeSheetDetails":' + JSON.serialize(relatedTimesheet,false);
            eventObj.TimeSheetEntry__c = '"timeSheetEntryDetails":[' + JSON.serialize(finalTimeSheetEntryDetailList[i],false)+']' ;
            eventObj.EventTimeStamp__c = String.ValueOf(System.DateTime.Now());
            returnlist.add(eventObj);
        }
        return returnlist;
    }
    Public Class TimeSheetDetails{
        public String Id {get; set;}
        public String ServiceResourceId {get; set;}
        public String ServiceResourceName {get; set;}
        public String ServiceResourceEmail {get; set;}
        public String ServiceResourcePhone {get; set;}
        public String Status {get; set;}
        public String StartDate {get; set;}
        public String EndDate {get; set;}
        public String TimeSheetEntryCount {get; set;}
        public String TimeSheetNumber {get; set;}
        public string TimesheetType {get;set;}
        
        public TimeSheetDetails(){
            ServiceResourceId = null;
            ServiceResourceName = null;
        	ServiceResourceEmail =  null;
       		ServiceResourcePhone = null;
            Status = null;
            StartDate = null;
            EndDate = null;
            TimeSheetEntryCount = null;
            TimeSheetNumber = null;
            TimesheetType = null;
        }
    }
    Public Class TimeSheetEntryDetails{
        public String Description {get;set;}
        public String DurationInMinutes {get;set;}
        public String Employee_Number {get;set;}
        public String EndTime {get;set;}
        public String Labour_Type {get;set;}
        public String LastModifiedById {get;set;}
        public String LastModifiedDate {get;set;}
        public String LastReferencedDate {get;set;}
        public String LastViewedDate {get;set;}
        public String Latitude {get;set;}
        public String Longitude {get;set;}
        public String Minutes_Worked {get;set;}
        public String Planned_Duration {get;set;}
        public String Resource_Absence_Type {get;set;}
        public String StartTime {get;set;}
        public String Status {get;set;}
        public String Subject {get;set;}
        public String TimeSheetEntryNumber {get;set;}
        public String TimeSheetId {get;set;}
        public String Type {get;set;}
        public String WorkOrderId {get;set;}
        public String WorkOrderLineItemId {get;set;}
        public String WO_Duration_Type {get;set;}
        public String WO_Duration {get;set;}
        
        public TimeSheetEntryDetails(){

            Description = null;
            DurationInMinutes = null;
            Employee_Number = null;
            EndTime = null;
            Labour_Type = null;
            LastModifiedById = null;
            LastModifiedDate = null;
            LastReferencedDate = null;
            LastViewedDate = null;
            Latitude = null;
            Longitude = null;
            Minutes_Worked = null;
            Planned_Duration = null;
            Resource_Absence_Type= null;
            StartTime = null;
            Status = null;
            Subject = null;
            TimeSheetEntryNumber = null;
            TimeSheetId = null;
            Type = null;
            WorkOrderId = null;
            WorkOrderLineItemId = null;
            WO_Duration_Type = null;
            WO_Duration = null;   
        }
    }
}