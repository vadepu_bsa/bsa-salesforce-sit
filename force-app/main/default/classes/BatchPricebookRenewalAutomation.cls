global class BatchPricebookRenewalAutomation Implements Database.Batchable<sObject>, Database.Stateful{
    global String strQuery;
    public List<String> batchException = new List<String>();
    
    //Initialize the query string
    global BatchPricebookRenewalAutomation(){
        strQuery = 'SELECT Id, Price_Book__c, Price_Book__r.Name FROM Work_Order_Automation_Rule__c WHERE RecordType.DeveloperName=\'Price_Book_Automation\' AND Price_Book__c !=\'\' AND Price_Book__r.Start_Date_to_Apply__c != NEXT_YEAR';
    }
    
    //Return query result
    global Database.QueryLocator start(Database.BatchableContext bcMain){
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bcMain, List<Work_Order_Automation_Rule__c> listBatchRecords){
        
        Set<String> uniquePricebookSet = New Set<String>();
        List<Work_Order_Automation_Rule__c> listBatchRecordToUpdate = New List<Work_Order_Automation_Rule__c>();
        MAP<String, Pricebook2> priceBook2Map = New MAP<String, Pricebook2>();
        
        //Get unique product names across all the WOARs
        for(Work_Order_Automation_Rule__c WOAR: listBatchRecords){
            uniquePricebookSet.add(WOAR.Price_Book__r.Name);
        }
        
        System.debug('DildarLog: Batch - BatchPricebookRenewalAutomation - uniquePricebookSet - ' + uniquePricebookSet);
        
        //Get new pricebook created/configured for next year with matchig name as current list
        List<Pricebook2> priceBook2List = [SELECT Id, Name, Start_Date_to_Apply__c FROM Pricebook2 WHERE Name IN:uniquePricebookSet AND Start_Date_to_Apply__c = NEXT_YEAR];
        
        for(Pricebook2 pricebook2Record: priceBook2List){
            priceBook2Map.put(pricebook2Record.Name, pricebook2Record);
        }
        
        //Prepare a list of updated WOAR with new pricebook
        for(Work_Order_Automation_Rule__c WOAR: listBatchRecords){
            if(priceBook2Map.get(WOAR.Price_Book__r.Name) != Null){
                WOAR.Price_Book__c = priceBook2Map.get(WOAR.Price_Book__r.Name).Id;
                listBatchRecordToUpdate.add(WOAR);
            }
        }        
        System.debug('DildarLog: Batch - BatchPricebookRenewalAutomation - listBatchRecordToUpdate - ' + listBatchRecordToUpdate.size());
        try{
            if(listBatchRecordToUpdate.size() > 0){
            	Database.update(listBatchRecordToUpdate);
        	}
        }catch(Exception batchEx){
            batchException.add(batchEx.getMessage());
            System.debug('DildarLog: Batch - BatchPricebookRenewalAutomation - Execute - Exception - ' + batchEx.getMessage());
        }
		System.debug('DildarLog: Batch - BatchPricebookRenewalAutomation - priceBook2List - ' + priceBook2List);                
    }
    
    global void finish(Database.BatchableContext bcMain){
        //TODO - add email logic to send exceltion list        
        if(batchException.size() > 0){
            System.debug('DildarLog: Batch - BatchPricebookRenewalAutomation - Finish - Exceptions - ' + batchException);
        }        
    }
}