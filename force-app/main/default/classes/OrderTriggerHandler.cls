/**
 * @File Name          : OrderTrigggerHandler.cls
 * @Description        :
 * @Author             : IBM
 * @Group              :
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 25/08/2020
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    25/08/2020         Tom Henson             Initial Version
**/
public class OrderTriggerHandler implements ITriggerHandler {

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;

    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }

    public void BeforeInsert(List<SObject> newItems){
    }


    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        OrderTriggerHelper.validateStatus(newItems,oldItems); 
    } public void BeforeDelete(Map<Id, SObject> oldItems){} public void AfterInsert(Map<Id, SObject> newItems){}public void AfterDelete(Map<Id, SObject> oldItems){}public void AfterUndelete(Map<Id, SObject> oldItems){}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        List<Order> purchaseOrderList = new List<Order>();

        OrderTriggerHelper.updateAcceptedOrderLineItems(newItems, oldItems);
        orderTriggerHelper.updateDefectStatusOnOrderSubmit(newItems,oldItems);
        orderTriggerHelper.updateDefectFieldOnOrderCancelled(newItems,OldItems);
        Id POrecordType = Schema.Sobjecttype.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        for (Id key : newItems.keySet()) {
            Order newPo = (Order)newItems.get(key);
            if (newPo.RecordTypeId == POrecordType ){
                  purchaseOrderList.add(newPo);
            }

        }
        if (purchaseOrderList.size() > 0){
            OrderTriggerHelper.sentToPronto(purchaseOrderList,oldItems);
        }

    }
}