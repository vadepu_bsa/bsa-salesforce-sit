@isTest
public class BSA_ShiftTriggerTest {
    
    @isTest static void createShift() {
        //--create operating hours
        OperatingHours operatingHours = new OperatingHours();
        operatingHours.Name = 'Test H';
        operatingHours.TimeZone = 'America/New_York';
        insert operatingHours;
        
        //--create service territory 
        ServiceTerritory serviceTerritory = new ServiceTerritory();
        serviceTerritory.Name = 'Test serviceTerritory';
        serviceTerritory.OperatingHoursId = operatingHours.Id; 
        serviceTerritory.IsActive = true;
        insert serviceTerritory;
        
        Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        system.debug('profileId'+profileDetails.Id);
        
        User u1 = new User( email='te082034st.user@gmail.com',
                           profileid = profileDetails.Id, 
                           UserName='tes98322523t.user@gmail.com', 
                           Alias = 'GDS',
                           TimeZoneSidKey='America/New_York',
                           EmailEncodingKey='ISO-8859-1',
                           LocaleSidKey='en_US', 
                           LanguageLocaleKey='en_US',
                           FirstName = 'Test',
                           LastName = 'User');
        insert u1;
        
        ServiceResource serviceResource = new ServiceResource();
        serviceResource.Name = 'Test user';
        serviceResource.RelatedRecordId = u1.Id;
        serviceResource.ResourceType = 'T'; 
        serviceResource.IsActive = true;
        serviceResource.Pronto_Service_Resource_ID__c = '123456789098765432';
        insert serviceResource;
        
        Test.startTest();
        Date testDate = Date.today().addDays(4);
        Shift shift = new Shift();
        shift.StartTime = DateTime.newInstance(testDate, Time.newInstance(9, 0, 0, 0));
        shift.EndTime = DateTime.newInstance(testDate, Time.newInstance(15, 0, 0, 0));
        shift.Status = 'Tentative';
        shift.TimeSlotType = 'Normal';
        shift.ServiceResourceId = serviceResource.Id;
        shift.ServiceTerritoryId = serviceTerritory.Id;
        
        insert shift;
        
        shift.StartTime = DateTime.newInstance(Date.today().addDays(5), Time.newInstance(9, 0, 0, 0));
        shift.EndTime = DateTime.newInstance(Date.today().addDays(5), Time.newInstance(15, 0, 0, 0));
        update shift;
        Test.stopTest();
    }
}