// @FSL3-492
public class APS_DefectController {
    
    public static final String defaultProductSorCode = 'GenericDefect';
    public static final String defaultLabourSorCode = 'GenericLabourInternal';
    public static final String defaultPartSorCode = 'GenericPartSOR';
    public String defectIds {get;set;}
    public String SerrMsg = '';
    public boolean errorMsg = false;
    
    public APS_DefectController(ApexPages.StandardSetController controller) {

        List<Defect__c> defects = (List<Defect__c>)controller.getSelected();
        System.debug('@APS_DefectController --> getSelected SIZE : ' + controller.getSelected().size()); 
        System.debug('@APS_DefectController --> defects SIZE : ' + defects.size()); 
 
        defectIds='';
        for(Defect__c def : defects) {
            
            defectIds += def.Id + ',';     //build list of ids string concatenated with comma 
            //System.debug('@APS_DefectController --> defectIds: ' + defectIds); 
        }
        system.debug('Printing Defect Ids::'+defectIds);
        System.debug('@APS_DefectController --> defects SIZE : ' + defects.size()); 
        
    }
    
    

    public  PageReference redirectToLC() {
       String returnUrl = '/lightning/cmp/c__APS_CreateDefectQuote?c__listofDefects=' + defectIds;
       PageReference pgReturnPage = new PageReference(returnUrl);
       System.debug('@redirectToLC --> pgReturnPage: ' + pgReturnPage);
       return null;
    }

    @AuraEnabled
    public static String createDefectQuote(String defectIdsStr) {

        System.debug('@createDefectQuote --> total of defectIdsStr: ' + defectIdsStr);

        String quoteId;

        List<String> defectIds = new List<String>();
        if(String.isNotBlank(defectIdsStr)) {
            if(defectIdsStr.contains(',')) {
                defectIds = defectIdsStr.split(',');
            }
            else {
                defectIds.add(defectIdsStr);
            }
        }

        System.debug('@defectIds: ' + defectIds);

        if(!defectIds.isEmpty()) {

            List<Defect__c> defects = [SELECT Asset__c, Site__c, Site__r.Price_Book__c, Name, SOR_Code__c, Subject__c, Defect_Details__c, Asset_Name__c,Last_Order_Number__c,
                                            Site__r.Parent.Price_Book__c, Site__r.Service_Territory__r.Price_Book__c ,  Site__r.Service_Territory__c,Defect_Status__c
                                       FROM Defect__c 
                                       WHERE Id IN: defectIds];

             System.debug('@defects: ' + defects.size());

            // get site account id from defects (all defects here should belong to the same site)        
            String siteId;

            for(Defect__c def : defects) {
                if(String.isBlank(siteId)) {
                    siteId = def.Site__c;
                }
                else {
                    // check if all defects belong to the same site
                    if(siteId != def.Site__c) {
                        throw new applicationException('Defects need to belong to the same site to create a defect quote!');
                    }else if(def.Defect_Status__c != 'Open' && def.Defect_Status__c != 'Rejected'){
                        throw new applicationException('Defects status needs to be either Open or Rejected,Please check selected Defect/Defects !');
                    }
                }
            }

            // check if the price book exist on site, customer or service territory
            if(String.isBlank(defects[0].Site__r.Price_Book__c) && String.isBlank(defects[0].Site__r.Parent.Price_Book__c) && 
                ( defects[0].Site__r.Service_Territory__c != null && defects[0].Site__r.Service_Territory__r.Price_Book__c == null)) {
                    throw new applicationException('Price book doesn\'t exist on the site, customer or service territory');
            }

            // create an order with record type "Defect_Quote"
            // The selected records from the list view should be added as Quote Line Items (Order Products)
            //  Order Products need to be linked to the Defect, the Asset from the Defect (lookups)
            // There is a SOR code (Product) for each defect which needs to be set on the Order Product
            // When no SOR is found use a default SOR Code (generic)
            // For every Defect Line, create a dummy Order Product for 1 part and 1 labour
            Order quote = new Order();
            quote.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Defect_Quote').getRecordTypeId(); 
            quote.AccountId = siteId;
            quote.EffectiveDate = Date.today();
            quote.Status = 'Draft';
            // quote.Pricebook2Id = pricebookId;

            Savepoint sp = Database.setSavepoint();

            try {
                System.debug('@quote: ' + quote);
                Insert quote;

                quote = [SELECT Pricebook2Id,name,OrderNumber FROM Order WHERE Id =: quote.Id LIMIT 1];

                String pricebookId = quote.Pricebook2Id;

                // get "Generic Labour Internal"
                // get "Generic Part - SOR"
                Id labourProductId;
                PricebookEntry labourProductPricebookEntry;
                Id partProductId;
                PricebookEntry partProductPricebookEntry;
                

                for(Product2 p : [SELECT Id, ProductCode, 
                                    (SELECT Id, UnitPrice FROM PriceBookEntries 
                                    WHERE Pricebook2Id =: pricebookId) 
                                  FROM Product2
                                  WHERE (ProductCode =: defaultLabourSorCode OR Productcode =: defaultPartSorCode)
                                  AND IsActive = true]) {
                    if(p.ProductCode == defaultLabourSorCode) {
                        labourProductId = p.Id;
                        labourProductPricebookEntry = p.PricebookEntries[0];
                        // labourProductPricebookEntryId = p.PricebookEntries[0].Id;
                    }
                    else if(p.ProductCode == defaultPartSorCode) {
                        partProductId = p.Id;
                        partProductPricebookEntry = p.PricebookEntries[0];
                        // partProductPricebookEntryId = p.PricebookEntries[0].Id;
                    }                                    
                }

                // System.debug('@labourProductId: ' + labourProductId);
                // System.debug('@partProductId: ' + partProductId);


                List<DefectOrderItemWrapper> orderLineItemWrappers = new List<DefectOrderItemWrapper>();

                Map<String, Product2> productBySorCode = new Map<String, Product2>();

                // get SOR codes to find matching products
                for(Defect__c def : defects) {
                    if(String.isNotBlank(def.SOR_Code__c)) {
                        productBySorCode.put(def.SOR_Code__c, null);
                    }
                }

                productBySorCode.put(defaultProductSorCode, null); // default product SOR code

                // find matching products based on SOR codes
                if(!productBySorCode.isEmpty()) {
                    for(Product2 p : [SELECT ProductCode, Id, 
                                        (SELECT Id, UnitPrice FROM PriceBookEntries 
                                        WHERE Pricebook2Id =: pricebookId)
                                      FROM Product2 
                                      WHERE ProductCode IN: productBySorCode.keySet() 
                                      AND IsActive = true]) {
                        productBySorCode.put(p.ProductCode, p);
                    }
                } 

                // create order line items for selected defects
                for(Defect__c def : defects) {
                    
                                        
                    Product2 product;
                    if(String.isNotBlank(def.SOR_Code__c)) {
                        product = productBySorCode.get(def.SOR_Code__c);
                    }
                    else {
                        product = productBySorCode.get(defaultProductSorCode);
                    }

                    if(product != null) {
                        OrderItem oli = new OrderItem();
                        oli.OrderId = quote.Id;
                        oli.Defect__c = def.Id;
                        oli.Asset__c = def.Asset__c;

                        if (def.Subject__c != null && def.Subject__c.containsIgnoreCase(def.Asset_Name__c)){ 
                            System.debug('Asset_Name__c = ' + def.Asset_Name__c);
                           oli.Description = def.Defect_Details__c;
                        }else {
                           oli.Description = def.Subject__c;
                        }

                        if(product.PricebookEntries[0] != null) {
                            PricebookEntry pbe = product.PricebookEntries[0];
                            oli.PricebookEntryId = pbe.Id;
                            oli.Product2Id = product.Id;
                            oli.UnitPrice = pbe.UnitPrice;
                            oli.Quantity = 1;
                        }

                        OrderItem oliLabour = new OrderItem();
                        oliLabour.OrderId = quote.Id;
                        oliLabour.Product2Id = labourProductId;                        
                        oliLabour.PricebookEntryId = labourProductPricebookEntry.Id;
                        oliLabour.UnitPrice = labourProductPricebookEntry.UnitPrice;
                        oliLabour.Quantity = 1;
                        oliLabour.Defect__c = def.Id;
                      //  oliLabour.Description =    oli.Description;

                        OrderItem oliPart = new OrderItem();
                        oliPart.OrderId = quote.Id;
                        oliPart.Product2Id = partProductId;
                        oliPart.PricebookEntryId = partProductPricebookEntry.Id;
                        oliPart.UnitPrice = partProductPricebookEntry.UnitPrice;
                        oliPart.Quantity = 1;
                        oliPart.Defect__c = def.Id;
                       // oliPart.Description =    oli.Description;

                        orderLineItemWrappers.add(new DefectOrderItemWrapper(oli, oliLabour, oliPart));
                    }
                    else {
                        Database.rollback(sp);
                        throw new applicationException('No product found for product code: ' + def.SOR_Code__c);
                    }
                }

                if(!orderLineItemWrappers.isEmpty()) {

                    List<OrderItem> defectItems = new List<OrderItem>();
                    // create defect order line item first
                    for(DefectOrderItemWrapper oliw : orderLineItemWrappers) {
                        defectItems.add(oliw.defectOrderItem);
                    }

                    System.debug('@defectItems: ' + defectItems);
                    Insert defectItems;

                    List<OrderItem> labourAndPartItems = new List<OrderItem>();

                    // create labour and part order line items when the id of defect order line item is available
                    for(DefectOrderItemWrapper oliw : orderLineItemWrappers) {
                        // System.debug('@defect order line item id: ' + oliw.defectOrderItem.Id);

                        // link part and labour order line item to the defect order line item
                        oliw.part.Order_Product__c = oliw.defectOrderItem.Id;
                        // System.debug('@oliw.part: ' + oliw.part);
                        labourAndPartItems.add(oliw.part);

                        oliw.labour.Order_Product__c = oliw.defectOrderItem.Id;
                        // System.debug('@oliw.labour: ' + oliw.labour);
                        labourAndPartItems.add(oliw.labour);
                    }
                    // System.debug('@labourAndPartItems: ' + labourAndPartItems);
                    Insert labourAndPartItems;
                }
                //FR-339 Start
                for(Defect__c def:defects){
                    def.Last_Order_Number__c=quote.id;
                }                
                update defects;
                //FR-339 End

                quoteId = quote.id+','+quote.OrderNumber;
            }
            catch(Exception ex) {
                Database.rollback(sp);
                System.debug('@createDefectQuote error: ' + ex.getMessage());
                throw new AuraHandledException(ex.getMessage());
            }
        }
        return quoteId;
    }

    class DefectOrderItemWrapper {

        OrderItem defectOrderItem;
        OrderItem part;
        OrderItem labour;

        public DefectOrderItemWrapper(OrderItem defectOrderItem, OrderItem part, OrderItem labour) {
            this.defectOrderItem = defectOrderItem;
            this.part = part;
            this.labour = labour;
        }
    }

    public class applicationException extends Exception {}
}