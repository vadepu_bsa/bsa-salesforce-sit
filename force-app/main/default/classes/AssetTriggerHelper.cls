/**
* @author          IBM - David A
* @date            17/Aug/2020 
* @description     
*
* Change Date       Modified by         Description  
* 25/Aug/2020       David A (IBM)       Added UpdateEGQuantity for #FSL3-37
**/
public class AssetTriggerHelper {

    //David Azzi - IBM - #FSL3-37
    public static void UpdateEGQuantity(Map<Id, SObject> mapAssetItems){

        List<Asset> lstAssets = new List<Asset>();
        Set<Id> setAssetParentId = new Set<Id>();
        Map<Id, Asset> mapIdParentAsset = new Map<Id, Asset>();
        Id equipmentGroupId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('Equipment_Group').getRecordTypeId();

        for(SObject sObj : mapAssetItems.values()){
            Asset objAsset = (Asset)sObj;
            if (String.isNotBlank(objAsset.ParentId)){
                setAssetParentId.add(objAsset.ParentId);
            } else {
                setAssetParentId.add(objAsset.Id);
            }
        }

        //Get Asset with and Without ParentId (if there is no child, it will update to 0)
        List<Asset> lstAllAssets = [ SELECT Id, ParentId, Status, Parent.Account.Include_New_Assets_in_PM_Routine__c  
                                    FROM Asset
                                    WHERE 
                                            (   
                                                Id IN: setAssetParentId
                                                AND Serialised__c = true
                                                AND RecordTypeId =: equipmentGroupId  
                                                AND ParentId = ''
                                            )
                                            OR
                                            (
                                                Parent.Serialised__c = true
                                                AND ParentId IN: setAssetParentId
                                                AND Parent.RecordTypeId =: equipmentGroupId
                                                AND (   Status = 'Active'
                                                        OR  (   Parent.Account.Include_New_Assets_in_PM_Routine__c = true 
                                                                AND Status = 'New'
                                                            )
                                                    )
                                            )
                                    ];

                    
        for(Asset objAsset : lstAllAssets){
            String objAssetId = (String)objAsset.ParentId;    
            if (String.isBlank(objAssetId)){
                objAssetId = objAsset.Id;
            } 

            if (!mapIdParentAsset.containsKey(objAssetId)){
                objAsset.Quantity = -1;
                mapIdParentAsset.put(objAssetId, objAsset);
            }
            
            //overwrite child objAsset with parent Item when parent is found
            if (String.isBlank(objAsset.ParentId)){
                objAsset.Quantity = mapIdParentAsset.get(objAssetId).Quantity;
                mapIdParentAsset.put(objAssetId, objAsset);
            }

            mapIdParentAsset.get(objAssetId).Quantity += 1;
        }

        //Match Quantity in items to update to new items, if don't match - update that item
        Set<Id> tmpAddedList = new Set<Id>();
        for(SObject sObj : mapAssetItems.values()){
            Asset objAsset = (Asset)sObj;
            String assetId = objAsset.Id;
            if (String.isNotBlank(objAsset.ParentId)){
                assetId = objAsset.ParentId;
            }
            if (mapIdParentAsset.containsKey(assetId)){
                if ((objAsset.Quantity != mapIdParentAsset.get(assetId).Quantity) && !tmpAddedList.contains(assetId)){
                    lstAssets.add(mapIdParentAsset.get(assetId));
                    tmpAddedList.add(assetId);
                }
            }
        }
        

        if (lstAssets.size() > 0){
            try{
                AssetTriggerHandler.TriggerDisabled = true;
                Database.update(lstAssets);
            }catch(Exception e){
                System.debug('*-* Exception on AssetTriggerHelper.UpdateEGQuantity:' + e.getMessage());
            }
        }
        
    }

}