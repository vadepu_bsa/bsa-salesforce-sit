/**
* @author          Sunila.M
* @date            23/April/2019 
* @description     
*
* Change Date   	 Modified by         Description  
* 23/April/2019   	 Sunila.M        	Created Trigger to restrict more than one Service Appointment for same Work Order
**/
public class ServiceAppointmentValidationHelper {

    public static void validateServiceAppointment(List<ServiceAppointment> serviceAppointmentList){
        
        List<Id> workOrderIdList = new List<Id>();
        for(ServiceAppointment eachServiceAppointment : (List<ServiceAppointment>)serviceAppointmentList){
            workOrderIdList.add(eachServiceAppointment.ParentRecordId);
        }
        
        Map<Id, String> workOrderIdSAMap = new Map<Id, String>();
        //--query Service Appointment with same Parent Id
        for(ServiceAppointment eachExistingSA : [SELECT Id, ParentRecordId, AppointmentNumber FROM ServiceAppointment WHERE ParentRecordId IN: workOrderIdList]){
            workOrderIdSAMap.put(eachExistingSA.ParentRecordId, eachExistingSA.AppointmentNumber);                       
        }     
        
        Map<Id, WorkOrder> workOrderIdMap = new Map<Id, WorkOrder>();
        for(WorkOrder eachWO : [SELECT Id, Service_Contract_Number__c FROM WorkOrder WHERE Id IN: workOrderIdList AND Service_Contract_Number__c = '']){
            workOrderIdMap.put(eachWO.Id, eachWO);                       
        }
        
        System.debug('DildarLog: workOrderIdMap - ' + workOrderIdMap);
        
        for(ServiceAppointment eachServiceAppointment : (List<ServiceAppointment>)serviceAppointmentList){
            if(workOrderIdSAMap != null && workOrderIdSAMap.containsKey(eachServiceAppointment.ParentRecordId) && workOrderIdMap.size() > 0){
                eachServiceAppointment.addError('Another Service Appointment '+workOrderIdSAMap.get(eachServiceAppointment.ParentRecordId)+' already available for this work order!');
            }
        }        
    }
}