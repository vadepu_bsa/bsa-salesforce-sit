//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = true)
public class ActionOrderToBSAHandlerTest {

    @isTest
    static void updateOrderTest() {

        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        acc.Status__c = 'Active';
        insert acc;

        Order o = new Order();
        o.AccountId = acc.Id;
        o.Status = 'draft';
        o.Name='Test12345';
        o.OrderReferenceNumber = '804405';
        o.Pronto_Status__c = '00';
        o.RecordTypeId = TestRecordCreator.getRecordTypeByDeveloperName('Order', 'Purchase_Order');
        o.EffectiveDate = Date.today();   
        insert o;
        
        Order order = [SELECT OrderNumber From Order WHERE Name='Test12345' LIMIT 1];
        System.Debug('!OrderNumber '+order.OrderNumber);

        Action_Order_To_BSA__e event = new Action_Order_To_BSA__e();
        event.Action__c='UpdatePurchaseOrder';
        event.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event.CorrelationId__c='ID000000034937';
        event.OrderNumber__c=String.valueOf(order.OrderNumber);
        System.Debug('!EventOrderNumber '+event.OrderNumber__c);

        event.OrderHeader__c='"purchaseOrderDetails":{"ProntoOrderNumber":"804405","OrderStatus":"01","dummy":""}';
        event.OrderItemEntry__c='"orderItemDetails":[{"OrderItemNumber":"0000000011","ProntoOrderItemNumber":"1"},{"OrderItemNumber":"0000000012","ProntoOrderItemNumber":"3"}]';
        event.WorkOrderNumber__c='00036314';

        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
        Test.stopTest();

        Order order2 = [SELECT Pronto_Status__c From Order WHERE Name='Test12345' LIMIT 1];
        System.assertEquals('01',order2.Pronto_Status__c);
      
    }
}