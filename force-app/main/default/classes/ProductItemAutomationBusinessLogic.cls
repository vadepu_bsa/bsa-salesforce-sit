/* Author: Abhijeet Anand
 * Date: 26 September, 2019
 * Apex class that handles all pre/post DML operations on the standard FSL object 'ProductItem'
 */
 
 public class ProductItemAutomationBusinessLogic {

    // Method to grant Read/Write of Product Item to the Service Resources of the Location; JIRA: FSL-996
    public static void shareProductItemWithServiceResource (List<ProductItem> productItemList, Map<Id,SObject> oldMap) {

        List<SObject> productToShareList = new List<SObject>(); //List to insert ProductItem share records
        Id groupId = UtilityClass.getGroupIdByName('Operations'); //Get Salesforce Id of the 'Operations' public Group
        Map<Id,List<ProductItem>> locationItemMap = new Map<Id,List<ProductItem>>(); //Map to store ProductItems per Location
        List<ProductItemShare> productShareToDelete = new List<ProductItemShare>(); //List to delete ProductItem share records for old Service Resources
        Set<Id> oldLocationIDs = new Set<Id>(); //Set to get old Location IDs
        Set<Id> oldResourceIDs = new Set<Id>(); //Set to get old Service Resource IDs
        Set<Id> productItemIDs = new Set<Id>();

        for(ProductItem thisElem : productItemList) {
            if(oldMap != null) {
                ProductItem pi = (ProductItem)oldMap.get(thisElem.Id);
                oldLocationIDs.add(pi.LocationId);
                productItemIDs.add(pi.Id);
                if(thisElem.LocationId != null && pi.LocationId != thisElem.LocationId) {
                    locationItemMap = populateLocationMap(thisElem);
                }
            }
            else if(thisElem.LocationId != null) {
                locationItemMap = populateLocationMap(thisElem);
                //Share ProductItems created by Pronto_Van_Warehouse_Replenished_Updated__e platform event with the Public Group 'Operations' giving Read/Write access
                // Using apex sharing because records created by platform events are owned by the user named 'Automated Process' and this user cannot be included in the Sharing Rules scheme
                System.debug('thisElem.Correlation_ID__c -->'+thisElem.Correlation_ID__c);
                System.debug('groupId -->'+groupId);
                if(/*thisElem.Correlation_ID__c != null && */ groupId != null) {
                    SObject prdShare = UtilityClass.createObjectShare(thisElem.Id, groupId, 'Edit');
                    productToShareList.add(prdShare);  
                }
            }
        }

        System.debug('oldLocationIDs -->'+oldLocationIDs);

        //Query the Service Resource based on the Location
        if(!locationItemMap.isEmpty()) {
            for(ServiceResource obj : [SELECT Id, RelatedRecordId, LocationId FROM ServiceResource
                                       WHERE (LocationId IN : locationItemMap.keyset() OR LocationId IN : oldLocationIDs)]) {
                
                if(locationItemMap.containsKey(obj.LocationId)) {
                    for(ProductItem prd : locationItemMap.get(obj.LocationId)) {
                        // make sure we do not create a share for the record owner.
                        if (prd.OwnerId == obj.RelatedRecordId) continue;

                        SObject prdShare = UtilityClass.createObjectShare(prd.Id, obj.RelatedRecordId, 'Edit'); //Give Read/Write access to the Service Resource
                        productToShareList.add(prdShare);    
                    }
                }   

                else if(oldLocationIDs.contains(obj.LocationId)) {
                    oldResourceIDs.add(obj.RelatedRecordId);
                }

            }
        }

        System.debug('productToShareList -->'+productToShareList);
        System.debug('oldResourceIDs -->'+oldResourceIDs);

        if(!productToShareList.isEmpty()) {
            insert productToShareList; // insert ProductItem share records
        }

        productShareToDelete = [SELECT Id FROM ProductItemShare WHERE ParentId IN : productItemIDs AND userOrGroupId IN : oldResourceIDs];

        System.debug('productShareToDelete -->'+productShareToDelete);

        if(!productShareToDelete.isEmpty()) {
            delete productShareToDelete; // delete ProductItem share records when Lcoation of the Product Item has changed. This removes access to the Product Item for the resources of the old location.
        }

    }

    private static Map<Id,List<ProductItem>> populateLocationMap(ProductItem thisElem) {

        Map<Id,List<ProductItem>> mapToReturn = new Map<Id,List<ProductItem>>();

        if(mapToReturn.containsKey(thisElem.LocationId)) {
                List<ProductItem> productList = mapToReturn.get(thisElem.LocationId);
                productList.add(thisElem);
                mapToReturn.put(thisElem.LocationId,productList);
        } else {
            mapToReturn.put(thisElem.LocationId, new List<ProductItem> { thisElem });
        }

        return mapToReturn;

    }
    
}