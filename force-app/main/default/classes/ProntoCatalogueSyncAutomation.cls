/* Author: Abhijeet Anand
 * Date: 09 September, 2019
 * Handler class containing the business logic around the automation of Pronto_Catalogue_Product Platform Event
 */
 public class ProntoCatalogueSyncAutomation {

    public static void syncProntoProductsWithSF(List<Pronto_Catalogue_Product_Created_Updated__e> prdCatalogueList) {

        // List to hold all Products from Pronto to be upserted.
        List<Product2> productToUpsert = new List<Product2>();
        Set<String> clientId = new Set<String>();
        Map<String,Account> accountMap = new Map<String,Account>();
        
        // Iterate through each notification.
        for(Pronto_Catalogue_Product_Created_Updated__e event : prdCatalogueList) {
            System.debug('Product Code: ' + event.Product_Code__c);
            // Upsert Product to sync SF with Pronto.
            Product2 prd = new Product2();
            prd.Name = event.Product_Name__c == null?'':event.Product_Name__c;
            prd.Product_Code__c = event.Product_Code__c;
            prd.ProductCode = event.Product_Code__c;
            prd.Client_ID__c = event.Client_ID__c;
            prd.Pack_Quantity__c = event.Pack_Quantity__c;

            if(event.Product_Expiry__c == null) {
                prd.Product_Expiry__c = null;
            }
            else{
                prd.Product_Expiry__c = UtilityClass.setStringToDateFormat(event.Product_Expiry__c);
            }
            
            prd.QuantityUnitOfMeasure = event.Quantity_Unit_Of_Measure__c;
            prd.Family = event.Product_Group__c == null?'':event.Product_Group__c;
            prd.Product_Group__c= 'Part';
            prd.Correlation_ID__c = event.Correlation_ID__c;
            prd.Event_TimeStamp__c = event.Event_TimeStamp__c;
            prd.Is_Serialized__c = event.Is_Serialized__c == 'Y'?true:false;
            productToUpsert.add(prd);
        }
        
        // Upsert all Products in the list.
        Database.UpsertResult [] results = Database.upsert(productToUpsert, Product2.Product_Code__c, false);

        String jsonPayload = ''; //String to build a JSON payload
        //Pass the sobject list to JSON generator apex class to build a JSON string
        //jsonPayload = JSONGeneratorUtility.generateJSON(productToUpsert);
        jsonPayload = JSON.serialize(productToUpsert);
        
        for(Database.upsertResult result : results) {
            
            
            String msg = ''; //String to write a successful/failure message
            String logLevel = ''; // String to capture Log Level(Info/Warning) on the Application Log object
            Boolean outcome = result.isSuccess();
        
            System.debug('Outcome :'+outcome);

            if(result.isSuccess()) {
                
                logLevel = 'Info';
                if(result.isCreated()) { //successfull insert
                    System.debug('Product with Id ' + result.getId() +' was created ');
                    msg = 'Product with Id ' + result.getId() +' was created ';
                }
                else { //successfull update
                    System.debug('Product with Id ' + result.getId() +' was updated ');
                    msg = 'Product with Id ' + result.getId() +' was updated ';
                }
            }
            else if(!result.isSuccess()) { // failure
                System.debug('Inside failure');
                // Operation failed, so get all errors
                logLevel = 'Error';
                for(Database.Error err : result.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
            }
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, 'Product2');

        }
        
        //Insert the application log entry in the SF database
        ApplicationLogUtility.saveExceptionLog();

    }

}