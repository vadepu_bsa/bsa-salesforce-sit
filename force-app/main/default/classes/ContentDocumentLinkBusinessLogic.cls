/* Author: Abhijeet Anand
* Date: 08 November, 2019
* Apex class that handles all pre/post DML operations on the standard 'ContentDocumentLink' object
*/
public class ContentDocumentLinkBusinessLogic {
    
    @future(callout=true)
    public static void uploadFile(Map<Id,Id> documentEntityMap) {
        
        try{
            List<ContentVersion> contentVersionList = [SELECT Id, ContentDocumentId, Title, FileType, VersionData FROM ContentVersion WHERE ContentDocumentId IN : documentEntityMap.keyset()];
            List<WorkOrderLineItem> woliList = [SELECT Id, WorkOrder.Id, WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c, WorkOrder.NBN_Activity_Start_Date_Time__c FROM WorkOrderLineItem WHERE Id =: documentEntityMap.get(contentVersionList[0].ContentDocumentId)];
            
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();      
            gen.writeStringField('NBNWorkOrderID', woliList[0].WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c);
            gen.writeDateTimeField('NBNActivityStartDate', woliList[0].WorkOrder.NBN_Activity_Start_Date_Time__c);
            gen.writeStringField('SFOrgID',UserInfo.getOrganizationId());
            gen.writeStringField('FileType',contentVersionList[0].FileType.toLowerCase());
            gen.writeStringField('Title',contentVersionList[0].Title);
            gen.writeStringField('FileBody',EncodingUtil.base64Encode(contentVersionList[0].VersionData));
            gen.writeEndObject();    
            String jsonS = gen.getAsString();
            System.debug('jsonMaterials'+jsonS);
            
            String addr = System.Label.Artifacts_Upload_Url;
            //String authHeader = 'Username:test';
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint(addr);
            req.setMethod('POST');
            req.setHeader('Content-Type','application/JSON');
            req.setBody(jsonS);
            
            Http http = new Http();
            HttpResponse response = http.send(req);
            system.debug(response);
            
            Map<String,Object> responseMap;
            
            String jsonResponse = response.getBody();
            // Added By Karan starts here: Added a check to deseralize only when we get a success
            if(jsonResponse.contains('statusCode')){
                responseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonResponse);
            }
            //Added by Karan ends here.
            
            System.debug('responseMap --> '+responseMap);
            String msg = '', filePath = '';
            
            Application_Log__c alObj = new Application_Log__c();
            alObj.Work_Order_Line_Item__c = woliList[0].Id;
            
            system.debug('status code --> '+responseMap.get('statusCode'));
            system.debug('status --> '+responseMap.get('status'));
            
            if(responseMap.get('statusCode') == '200' && responseMap.get('status') == 'OK') {
                alObj.Log_Level__c = 'Info';
                msg = 'File uploaded to Gdrive successfully';
                filePath = (String)responseMap.get('filePath');
            }
            else if(responseMap.get('statusCode') != '200' && responseMap.get('status') != 'OK') {
                alObj.Log_Level__c = 'Error';
                msg = 'File upload to Gdrive unsuccessful';
            }
            
            alObj.Message__c = (String)responseMap.get('statusCode') + ' ' + (String)responseMap.get('status') + '\n' + msg;
            
            insert alObj; // Create an Application Log Entry
            
            if(alObj.Log_Level__c == 'Info') {
                WorkOrder wo = new WorkOrder(Id = woliList[0].WorkOrder.Id);
                system.debug('wo -->'+wo);
                if(wo != null) {
                    wo.Gdrive_Artifact_Path__c = filePath.replaceAll('\"','');
                    system.debug('Path --> '+wo.Gdrive_Artifact_Path__c);
                    update wo;
                }
            }
            
        } catch(Exception excp){
            
            System.debug('Exception occured'+excp.getMessage()+excp.getStackTraceString());
        }
        
        
    }
}