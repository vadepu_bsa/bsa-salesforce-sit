global class APS_WorkOrderCreationBatch implements Database.batchable<WorkOrder>, Database.Stateful {
    
    String guid;
    List<WorkOrder> workOrders;    
    Map<Id, List<WorkOrderLineItem>> workOrderLineItemsByWoId;
    Map<WorkOrder, String> errorMsgByWorkOrder; // error messages when inserting asset wolis
    String actionType;
    String csvURL; 
    Id draftWorkOrderId;
    Set<WorkOrder> successWo;
    
    global APS_WorkOrderCreationBatch(List<WorkOrder> workOrders, Id draftWorkOrderId, String guid, String actionType) {
        this.workOrders = workOrders;
        this.draftWorkOrderId = draftWorkOrderId; 
        this.workOrderLineItemsByWoId = new Map<Id, List<WorkOrderLineItem>>();
        this.errorMsgByWorkOrder = new Map<WorkOrder, String>();
        this.actionType = actionType;
        this.guid = guid;
        this.successWo=new Set<WorkOrder>();
        // update draft work orders status to 'in progress' to lock the draft work order so that no one can generate work orders using the same GUID
        if(this.actionType == 'approveWorkOrders') {
            update new Draft_Work_Order__c(Id = this.draftWorkOrderId, Status__c = 'Generation In Progress');
        }
    }
    
    global Iterable<WorkOrder> start(Database.BatchableContext bc) {
        APS_WorkOrderFeeder woFeeder = new APS_WorkOrderFeeder(workOrders);
        return woFeeder;
    }
    
    global void execute(Database.BatchableContext bc, List<WorkOrder> workOrders) {
        
        if(this.actionType == 'approveWorkOrders') {
            List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrderWrappers = APS_WorkOrderGeneratorHelper.generateWorkOrderLineItems(workOrders);
            for(APS_WorkOrderGeneratorCtr.WorkOrderWrapper woWrapper : workOrderWrappers) {
                woWrapper.workOrder.Line_Item_Count__c = woWrapper.workOrderLineItems.size();
                woWrapper.workOrder.Number_of_Remaining_Assets__c= woWrapper.workOrderLineItems.size();
            }
            
            // insert work orders
            Database.SaveResult[] srList = Database.insert(workOrders, false);
            
            List<WorkOrder> successWorkOrders = new List<WorkOrder>();
            
            Integer srListSize = srList.size();
            for(Integer i = 0; i < srListSize; i++) {
                Database.SaveResult sr = srList[i];
                
                if(sr.isSuccess()) {
                    successWorkOrders.add(workOrders[i]);
                    successWo.add(workOrders[i]);
                }
                else {
                    String errorMessage = '';
                    // Operation failed, so get all errors
                    for(Database.Error err : sr.getErrors()) {
                        errorMessage += err.getStatusCode() + ': ' + err.getMessage() + '. ';
                    }
                    errorMsgByWorkOrder.put(workOrders[i], errorMessage);
                }
            }
            
            // only pass success insert work orders here
            for(APS_WorkOrderGeneratorCtr.WorkOrderWrapper woWrapper : workOrderWrappers) {
                if(woWrapper.workOrder.Id != null) {
                    Id woId = woWrapper.workOrder.Id;
                    
                    if(!workOrderLineItemsByWoId.containsKey(woId)) {
                        workOrderLineItemsByWoId.put(woId, new List<WorkOrderLineItem>());
                    }
                    
                    for(WorkOrderLineItem woli : woWrapper.workOrderLineItems) {
                        woli.WorkOrderId = woId;
                        workOrderLineItemsByWoId.get(woId).add(woli);
                    }                    
                }
            }
            // System.debug('@APS_WorkOrderCreationBatch workOrderLineItemsByWoId: ' + workOrderLineItemsByWoId);
        }
        else if(this.actionType == 'exportToCsv') {
            // pass all draft work orders here
            List<APS_WorkOrderGeneratorCtr.WorkOrderWrapper> workOrderWrappers = APS_WorkOrderGeneratorHelper.generateWorkOrderLineItems(workOrders);
            csvURL = APS_WorkOrderGeneratorHelper.generateCSVFile(workOrderWrappers, draftWorkOrderId, bc.getJobID());
            // System.debug('@csvURL in execute: ' + csvURL);
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        // System.debug('@csvURL: ' + csvURL);
        
        // run work order line item batch to process work order line items and insert them into the system
        /* if(!workOrderLineItemsByWoId.isEmpty()) {

// doing this is to make sure all wolis with the same parent id are grouped together to try to avoid racing condition
List<WorkOrderLineItem> workOrderLineItems = new List<WorkOrderLineItem>();

for(List<WorkOrderLineItem> wolis : workOrderLineItemsByWoId.values()) {
workOrderLineItems.addAll(wolis);
}

APS_WorkOrderLineItemCreationBatch woliCreationBatch = new APS_WorkOrderLineItemCreationBatch(workOrderLineItems, guid);
ID batchProcessId = Database.executeBatch(woliCreationBatch, APS_WorkOrderGeneratorHelper.findBatchSize('APS_WorkOrderLineItemCreationBatch'));
}*/
        
        String message = '';
        
        if(!errorMsgByWorkOrder.isEmpty()) {
            List<WorkOrder> toBeDeleteWorkOrders = new List<WorkOrder>();
            
            // log and send email when there are any errors when processing work orders            
            for(WorkOrder wo : errorMsgByWorkOrder.keySet()) {
                toBeDeleteWorkOrders.add(wo);
                message += 'Work Order Details: Account Id '+wo.AccountId+', Description: '+wo.Description+', Preferred Engineer: '+wo.Preferred_Engineer__c+', Site Name: '+wo.Site_Name__c+' has following error(s): ' + errorMsgByWorkOrder.get(wo) + '\n';
            }
            
            APS_WorkOrderGeneratorHelper.updateNumOfFailedUnsavedWorkOrders(draftWorkOrderId, null, toBeDeleteWorkOrders, message);
            system.debug('Printing Draft WorkOrderId::'+draftWorkOrderId);
            Draft_Work_Order__c df=[select id,All_Wolis_Inserted__c,Name from Draft_Work_Order__c where id=:draftWorkOrderId];
            system.debug('Printing Draft WorkOrderId::'+df);
            if(df != null){
                message += 'View Draft Work Order for details : '+df.Name+ '\n';
            }
            system.debug('Printing Draft Success Size::'+successWo.size());
            if(successWo.size() == 0){
                df.All_Wolis_Inserted__c=true;
                update df;                
            }
            APS_WorkOrderGeneratorHelper.QuickEmail('Work Order Generation Summary', message, UserInfo.getUserEmail());
        }
        
        if(String.isNotBlank(csvURL)) {
            message += 'You can review the work order by downloading the csv file from ' + csvURL 
                + ' or you can go back to the work order review page by clicking the link ' + URL.getSalesforceBaseUrl().toExternalForm() + '/lightning/n/Work_Order_Generator?c__guid=' + this.guid;
            APS_WorkOrderGeneratorHelper.QuickEmail('Work Order Generation Summary', message, UserInfo.getUserEmail());
        }
        
        APS_WorkOrderGeneratorHelper.updateNumOfSuccessWorkOrders(successWo);
        
        // run work order line item batch to process work order line items and insert them into the system
        if(!workOrderLineItemsByWoId.isEmpty()) {
            // doing this is to make sure all wolis with the same parent id are grouped together to try to avoid racing condition
            List<WorkOrderLineItem> workOrderLineItems = new List<WorkOrderLineItem>();
            
            for(List<WorkOrderLineItem> wolis : workOrderLineItemsByWoId.values()) {
                workOrderLineItems.addAll(wolis);
            }
            
            APS_WorkOrderLineItemCreationBatch woliCreationBatch = new APS_WorkOrderLineItemCreationBatch(workOrderLineItems, guid,message,draftWorkOrderId);
            ID batchProcessId = Database.executeBatch(woliCreationBatch, APS_WorkOrderGeneratorHelper.findBatchSize('APS_WorkOrderLineItemCreationBatch'));
        }
    }
}