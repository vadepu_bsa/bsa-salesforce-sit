global class APS_TaskCreationScheduler implements Schedulable {

    global String taskType;
    global List<WorkOrderLineItem> woliTasks;
    global List<SObject> triggerRecords;
    global List<SObject> originalTriggerRecords;

    global APS_TaskCreationScheduler(String taskType, List<WorkOrderLineItem> woliTasks, List<SObject> triggerRecords, List<SObject> originalTriggerRecords) {
        this.taskType = taskType;
        this.woliTasks = woliTasks;
        this.triggerRecords = triggerRecords;
        this.originalTriggerRecords = originalTriggerRecords;
    }

    global void execute(SchedulableContext SC) {
        List<WorkOrderLineItem> successRecords=new List<WorkOrderLineItem>();
        List<Id> failedRecords=new List<Id>();
        String message='';
        System.enqueueJob(new APS_TaskCreationDispatcher(taskType, woliTasks, triggerRecords, originalTriggerRecords,successRecords,failedRecords,message));
    }
 }