/**
* @author          David Azzi (IBM)
* @date            06/Aug/2020
* @description     Trigger Helper added for the Order Item trigger
*/

public with sharing class OrderItemTriggerHelper {
    public OrderItemTriggerHelper() {
        
    }
    
    //David Azzi - 06/Aug/2020 - #FSL3-86 #FSL3-363
    public static void populateOrderCharges(List<SObject> newItems){
        System.debug('*-* populateOrderCharges start' );
        
        List<OrderItem> lstOrderItemToUpdate = new List<OrderItem>();
        
        Set<Id> setOrderIds = new Set<Id>();
        Set<Id> setAllAccountIds = new Set<Id>();
        Set<Id> setAccountIds = new Set<Id>();
        set<Id> setParentAccountIds = new Set<id>();
        
        Set<Id> setServiceTerritoryIds = new Set<Id>();
        Map<Id, OrderItem> mapOrderIdItems = new Map<Id, OrderItem>();
        
        Map<Id, Order> mapOrder = new Map<Id, Order>();
        Map<Id, Id> mapOrderAccount = new Map<Id, Id>();
        Map<String, List<Material_Markup__c>> mapMarkupCode = new Map<String, List<Material_Markup__c>>();
        Map<Id, List<Labour_Rate__c>> mapAccountLabour = new Map<Id, List<Labour_Rate__c>>();
        Map<Id, List<Labour_Rate__c>> mapCustomerLabour = new Map<Id, List<Labour_Rate__c>>();
        Map<Id, List<Labour_Rate__c>> mapServiceTerritoryLabour = new Map<Id, List<Labour_Rate__c>>();

        String OrderStatus = null;
        
        //Get Map of All Account Ids based on OrderItems from all OrderIds
        for(SObject oi : newItems){
            OrderItem objOI = (OrderItem) oi;
            setOrderIds.add(objOI.OrderId);
        }
        List<Order> lstOrder = [SELECT Id, AccountId,Account.ParentId, Account.Markup_Code__c, Account.Service_Territory__c,Status FROM Order WHERE Id IN: setOrderIds];
        System.debug('*-* populateOrderCharges ==> Get  Orders size : '  + lstOrder.size());

        //Id NonDefectRecordType = Schema.Sobjecttype.Order.getRecordTypeInfosByName().get('Non_Defect_Quote').getRecordTypeId();
        //Id DefectRecordType = Schema.Sobjecttype.Order.getRecordTypeInfosByName().get('Defect_Quote').getRecordTypeId();

        for(Order objOrder : lstOrder){

            System.debug('*-* populateOrderCharges ==> objOrder Status : ' + objOrder.Status);
            OrderStatus = objOrder.Status;
            //OrderRecordType = objOrder.Order_RecordType__c;

            mapOrder.put(objOrder.Id, objOrder);
            if (objOrder.AccountId != null){
                setAccountIds.add(objOrder.AccountId);
                setAllAccountIds.add(objOrder.AccountId);
                if (objOrder.Account.ParentId != null){
                    setAllAccountIds.add(objOrder.Account.ParentId);
                    setParentAccountIds.add(objOrder.Account.ParentId );
                }
                if (objOrder.Account.Service_Territory__c != null){
                    setServiceTerritoryIds.add(objOrder.Account.Service_Territory__c);
                }
            }
            
        }
        System.debug('*-* populateOrderCharges ==> OrderStatus Status : ' + OrderStatus);
        // FR-192 Eliana ;  recalculation of Markup  and Labour Price should not happen when quote status = Submitted or Accepted   
        if (OrderStatus != 'Submitted' && OrderStatus != 'Accepted') {

            System.debug('*-* populateOrderCharges ==> Get lstLabourRate  ');
            //Get a map of Labour based on AccountIds
            List<Labour_Rate__c> lstLabourRate = [SELECT Account__c, Account__r.ParentId, After_Hours_Minimum_Rates__c,After_Hours_Minimum_Time_Blk__c,
                                                  After_Hours_Normal_Rate__c,After_Hours_Normal_Time_Blk__c,Id,Minimum_Rates__c,
                                                  Minimum_Time_Blk__c,Name,Normal_Rate__c,Normal_Time_Blk__c, Technician_Category__c,
                                                  Work_Type__c, Service_Territory__c, Charge_Type__C
                                                  FROM    Labour_Rate__c
                                                  WHERE   Account__c IN: setAllAccountIds 
                                                  OR Service_Territory__c IN: setServiceTerritoryIds
                                                 ];
            
            for (integer i = 0; i < lstLabourRate.size(); i++){
                if (!mapAccountLabour.containsKey(lstLabourRate[i].Account__c) && setAccountIds.contains(lstLabourRate[i].Account__c) ){
                    mapAccountLabour.put(lstLabourRate[i].Account__c, new List<Labour_Rate__c>{lstLabourRate[i]});
                } else if ( mapAccountLabour.containsKey(lstLabourRate[i].Account__c) && setAccountIds.contains(lstLabourRate[i].Account__c)  ){
                    mapAccountLabour.get(lstLabourRate[i].Account__c).add(lstLabourRate[i]); //Account
                    
                }
                if (!mapCustomerLabour.containsKey(lstLabourRate[i].Account__c)  && setParentAccountIds.contains(lstLabourRate[i].Account__c)){
                    mapCustomerLabour.put(lstLabourRate[i].Account__c, new List<Labour_Rate__c>{lstLabourRate[i]});
                }else if ( mapCustomerLabour.containsKey(lstLabourRate[i].Account__c) && setParentAccountIds.contains(lstLabourRate[i].Account__c) ){
                    mapCustomerLabour.get(lstLabourRate[i].Account__c).add(lstLabourRate[i]); //Customer 
                }
                if (!mapServiceTerritoryLabour.containsKey(lstLabourRate[i].Service_Territory__c)  && setServiceTerritoryIds.contains(lstLabourRate[i].Service_Territory__c)){
                    mapServiceTerritoryLabour.put(lstLabourRate[i].Service_Territory__c, new List<Labour_Rate__c>{lstLabourRate[i]});
                } else if ( mapServiceTerritoryLabour.containsKey(lstLabourRate[i].Service_Territory__c) && setServiceTerritoryIds.contains(lstLabourRate[i].Service_Territory__c)  ){
                    mapServiceTerritoryLabour.get(lstLabourRate[i].Service_Territory__c).add(lstLabourRate[i]); //Service Territory
                }
            }
            System.debug('*-* populateOrderCharges ==> Get  lstMaterialRate  ');
            //Get a map of Material Markup
            List<Material_Markup__c> lstMaterialRate = [SELECT Markup_Code__r.Name, Markup_Code__r.Code__c, Code__c, End_Value__c, Markup__c, Start_Value__c
                                                        FROM Material_Markup__c ];
            for (integer i = 0; i <lstMaterialRate.size(); i++) {
                if (!mapMarkupCode.containsKey(lstMaterialRate[i].Markup_Code__r.Id)){
                    mapMarkupCode.put(lstMaterialRate[i].Markup_Code__r.Id, new List<Material_Markup__c>());
                }
                mapMarkupCode.get(lstMaterialRate[i].Markup_Code__r.Id).add(lstMaterialRate[i]);
            }
            
            System.debug('*-* populateOrderCharges ==> loop through all changes ');
            //loop through all changes
            for(SObject oi : newItems){
                OrderItem objOrderItem = (OrderItem) oi;
                //#FSL3-519 - David A (IBM) - Default Quantity to 1 if null
                objOrderItem.Quantity = (objOrderItem.Quantity != null) ? Double.valueOf(objOrderItem.Quantity) : 1;
                System.debug('*-* populateOrderCharges ==> loop through all changes Quantity ' + objOrderItem.Quantity );
    
                if (mapOrder.containsKey(objOrderItem.OrderId)){
                    //Material Markup
                    System.debug('*-* populateOrderCharges ==> loop through all changes -- Material Markup .. mapMarkupCode.size() ' + mapMarkupCode.size() );
    
                    if (mapMarkupCode.size() > 0){
                        Double markupPercent = 5; //default standard markup price - Check this with Mahmood
                        Double oiQuantity = objOrderItem.Quantity;
                        //Double oiQuantity = (objOrderItem.Quantity != null) ? Double.valueOf(objOrderItem.Quantity) : 1;
                        Double oiUnitPrice = (objOrderItem.UnitPrice != null) ? Double.valueOf(objOrderItem.UnitPrice) : 1;
                        String markupCode = mapOrder.get(objOrderItem.OrderId).Account.Markup_Code__c;
                        
                        System.debug('*-* populateOrderCharges ==> loop through all changes -- Material Markup .. markupCode  ' + markupCode); 
    
                        if(markupCode != null && mapMarkupCode.containsKey(markupCode)) {
                            for (Material_Markup__c mm : mapMarkupCode.get(markupCode)){
    
                                //if Total Price is equal to or between Start and End Value
                                if ( (oiQuantity * oiUnitPrice) >= mm.Start_Value__c &&  (oiQuantity * oiUnitPrice) <= mm.End_Value__c){
                                    markupPercent = mm.Markup__c;    //overwrite default
    
                                    System.debug('*-* populateOrderCharges ==> loop through all changes -- Material Markup .. oiQuantity  ' + oiQuantity + ' oiUnitPrice ' + oiUnitPrice + ' mm.Start_Value__c ' + mm.Start_Value__c + ' mm.End_Value__c ' + mm.End_Value__c  + ' markupPercent ' + markupPercent);
                                    objOrderItem.Markup_Price__c = (oiQuantity * oiUnitPrice) * (1 + (markupPercent/100));
                                    System.debug('*-* populateOrderCharges ==> loop through all changes --1  Markup_Price__c ' + objOrderItem.Markup_Price__c );
                                }
                            }
                        } else if (markupCode == null) {
                            objOrderItem.Markup_Price__c = null;
                            System.debug('*-* populateOrderCharges ==> loop through all changes --2  Markup_Price__c ' + objOrderItem.Markup_Price__c );
                        }
                            
                    }
                    System.debug('*-* populateOrderCharges ==> loop through all changes -- Labour Rate');
                    //Labour Rate
                    if (lstLabourRate.size() > 0){
                        List<Labour_Rate__c> lstLabourObj = new List<Labour_Rate__c>();
                        Id serviceTerritoryId = null;
                        Id accId = null;
                        Id customerId = null;
                        
                        if (mapOrder.get(objOrderItem.OrderId).AccountId != null){
                            accId = mapOrder.get(objOrderItem.OrderId).AccountId;
                        }
                        
                        if (mapOrder.get(objOrderItem.OrderId).Account.ParentId != null){
                            customerId = mapOrder.get(objOrderItem.OrderId).Account.ParentId;
                        }
                        if (mapOrder.get(objOrderItem.OrderId).Account.Service_Territory__c != null){
                            serviceTerritoryId = mapOrder.get(objOrderItem.OrderId).Account.Service_Territory__c;
                        }
                        boolean found = false;
                        if (mapAccountLabour.containsKey(accId)){
                             system.debug('!!! site');
                            lstLabourObj = mapAccountLabour.get(accId);
                           found = calculation(objOrderItem,lstLabourObj);
                        }//check customer if no acc
                        if (mapCustomerLabour.containsKey(customerId) && !found){
                            system.debug('!!! customer');
                            lstLabourObj = mapCustomerLabour.get(customerId);
                             found = calculation(objOrderItem,lstLabourObj);
                        }//check service terr if no customer
                        if (mapServiceTerritoryLabour.containsKey(serviceTerritoryId)  && !found){
                            system.debug('!!! st');
                            lstLabourObj = mapServiceTerritoryLabour.get(serviceTerritoryId);
                             found = calculation(objOrderItem,lstLabourObj);
                        }
                        if (!found){
                            system.debug('!!! Labour_Total__c changed' );
                            objOrderItem.Labour_Total__c = 0;
                        }
                        
                  
                    }
                }
            }
        }

        
        //moved to before trigger
        /* if(lstOrderItemToUpdate.size() > 0){
try{

//Database.update(lstOrderItemToUpdate);
}catch(Exception e){
System.debug('*-* Exception on OrderItemTriggerHelper.PopulateOrderCharges:' + e.getMessage());
}
}*/
        
        System.debug('*-* populateOrderCharges stop');
    }
    
    
    //IBM - 04/Sep/2020 -
    public static void sendOrderLineItemEvent(List<SObject> newItems, string Operation){
        List<OrderItem> orderItemList = new List<Orderitem>();
        map<Id,string> orderItemMap = new map<Id,String>();
        set<id> orderIdSet = new set<Id>();
        Id recordType = Schema.Sobjecttype.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        for(SObject oi : newItems){
            OrderItem objOI = (OrderItem) oi;
            if (objOI.Order_RecordType__c == recordType && objOI.Order_Status__c != 'Draft' && objOI.Order_Status__c != 'In Approval Process'){
                orderItemMap.put(objOI.id,Operation);
                orderIdSet.add(objOI.OrderId);
                orderItemList.add(objOI);
            }
        }
        if (orderIdSet.size()>0){
            System.debug('*-* sendOrderLineItemEvent generateProntoPurchaseOrderPlatformEvent');
            ProntoPurchaseOrderSerialize.generateProntoPurchaseOrderPlatformEvent(orderIdSet,orderItemMap);
        }
        
    }
    
    private static boolean calculation(OrderItem objOrderItem, List<Labour_Rate__c> lstLabourObj){
        boolean found = false;                              
        System.debug('*-* populateOrderCharges calculation ');
        for (Integer i = 0; i <lstLabourObj.size(); i++){
             system.debug('!!!! lstLabourObj : ' + lstLabourObj[i]);
             system.debug('!!!! objOrderItem : ' + objOrderItem);
            //Get the Normal Time / Rate (result is in minutes)
            if ((objOrderItem.Technician_Category__c == lstLabourObj[i].Technician_Category__c)){
                found = true;
                if (objOrderItem.Work_Type__c ==  lstLabourObj[i].Charge_Type__c ){
                    objOrderItem.Labour_Total__c = orderItemCalculation(objOrderItem,lstLabourObj[i]);   
                } else {
                    objOrderItem.Labour_Total__c = orderItemCalculation(objOrderItem,lstLabourObj[i]);
                }
                system.debug('!!!! 1 Labour_Total__c : ' + objOrderItem.Labour_Total__c);
            }
        }
        if (!found){
            for (Integer i = 0; i <lstLabourObj.size(); i++){
                If ( lstLabourObj[i].Technician_Category__c == '~*'){
                    found = true;
                    objOrderItem.Labour_Total__c = orderItemCalculation(objOrderItem,lstLabourObj[i]);
                    system.debug('!!!! 2 Labour_Total__c : ' + objOrderItem.Labour_Total__c);
                }
                
            }
        }
        
        return  found;
    }
    public static void updateDefect(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
         Map<Id,Id> defectVsOrderId=new Map<Id,Id>();
        for(sObject obj:newItems.values()){
            OrderItem newOrdItem=(OrderItem)obj;
            OrderItem oldOtem=(OrderItem)oldItems.get(newOrdItem.id);
            if(newOrdItem.Defect__c != oldOtem.Defect__c){
                defectVsOrderId.put(newOrdItem.Defect__c,newOrdItem.OrderId);
            }
        }
        if(defectVsOrderId.size() != 0){
            List<Defect__c> defectList=[select id,Last_Order_Number__c from Defect__c where id IN: defectVsOrderId.keySet()];
            for(Defect__c d:defectList){
                d.Last_Order_Number__c=defectVsOrderId.get(d.Id);
            }
            update defectList;
        }
    }
    
    private static double orderItemCalculation(OrderItem objOrderItem, Labour_Rate__c objLabourRate ){
        Double total = 0;
        Double minutesWorked = (objOrderItem.No_of_Hours__c != null) ? objOrderItem.No_of_Hours__c  * 60 : 0;
        Double costPerMinute = 0;
        Double chargeoutFee = (objOrderItem.Charge_out_fee__c == null) ? 0 : objOrderItem.Charge_out_fee__c;

        System.debug('*-* populateOrderCharges orderItemCalculation ==>  : Charge_Type__c : ' + objOrderItem.Charge_Type__c );
        
        if (objOrderItem.Charge_Type__c == 'Normal' && objLabourRate.Normal_Time_Blk__c != null && objLabourRate.Normal_Time_Blk__c != 0){ 
            costPerMinute = objLabourRate.Normal_Rate__c / objLabourRate.Normal_Time_Blk__c;
        } else if (objOrderItem.Charge_Type__c == 'Normal' && (objLabourRate.Normal_Time_Blk__c == null || objLabourRate.Normal_Time_Blk__c == 0)){ 
            costPerMinute = 0;
        }else if (objOrderItem.Charge_Type__c == 'After Hours' && objLabourRate.After_Hours_Normal_Time_Blk__c != null && objLabourRate.After_Hours_Normal_Time_Blk__c != 0){ 
            costPerMinute = objLabourRate.After_Hours_Normal_Rate__c / objLabourRate.After_Hours_Normal_Time_Blk__c;
        }else if (objOrderItem.Charge_Type__c == 'After Hours' && (objLabourRate.After_Hours_Normal_Time_Blk__c == null || objLabourRate.After_Hours_Normal_Time_Blk__c == 0)){ 
            costPerMinute = 0;
        }
        total = (minutesWorked * costPerMinute) + chargeoutFee;    

        System.debug('*-* populateOrderCharges orderItemCalculation ==> total : ' + total);
        return total;
    }
    
    
}