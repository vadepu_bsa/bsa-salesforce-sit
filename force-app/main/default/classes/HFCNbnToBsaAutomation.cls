/* Author: Abhijeet Anand
* Date: 21 October, 2019
* Handler class containing the business logic around the automation for post insert of HFC_Action_Assign_From_NBN_To_BSA Platform Event
*/

public class HFCNbnToBsaAutomation {
    
    //Map to get WorkType Id based on primary technology
    public static Map<String,Id> workTypeMap = new Map<String,Id>();
    //Map to get the corresponding WO status depending on the incoming NBN Status values
    public static Map<String,NBN_NA_Status__mdt> statusMap = new Map<String,NBN_NA_Status__mdt>();
    //Map to get the corresponding WO record type name depending on the incoming NBN Primary Access Technology values
    public static Map<String,NBN_Primary_Access_Technology__mdt> primaryTechnologyMap = new Map<String,NBN_Primary_Access_Technology__mdt>();
    //Map to get the corresponding Priority Description values from NBN Priority values
    public static Map<String,NBN_NA_Priority__mdt> priorityMap = new Map<String,NBN_NA_Priority__mdt>();
    //List to create Log Entries coming from NBN
    public static List<NBN_Log_Entry__c> logEntryToCreate = new List<NBN_Log_Entry__c>();
    //Instantiate Log Entry variable 
    public static NBN_Log_Entry__c logEntry;
    //WorkOrder Map
    public static Map<Id,WorkOrder> workOrderMap = new Map<Id,WorkOrder>();
    // List to hold all Work Orders from NBN to be upserted.
    public static List<WorkOrder> workOrderToUpsert = new List<WorkOrder>();
    
    public static void syncWorkOrdersWithSF(List<Action_From_NBN_To_BSA__e> hfcFromNBNList) {
        
        //System.debug('hfcFromNBNList-->'+hfcFromNBNList);
        
        //Set to store SAM IDs
        Set<String> samIDs = new Set<String>();
        //Set to store Primary Technology
        Set<String> workTypes = new Set<String>();
        //Map to get ServiceTerritory Id based on SAM ID
        Map<String,Id> territoryMap = new Map<String,Id>();
        //Map to hold WorkArea values on WorkOrder object
        Map<String,String> workAreaMap = UtilityClass.picklistValues('WorkOrder','Work_Area__c');
        
        for(NBN_NA_Status__mdt obj : [SELECT DeveloperName, FSL_Work_Order_Status__c, NBN_State_Value__c FROM NBN_NA_Status__mdt]) {
            statusMap.put(obj.DeveloperName,obj);
        }
        
        //System.debug('statusMap --> '+statusMap);
        
        for(NBN_Primary_Access_Technology__mdt obj : [SELECT Label, NBN_Record_Type__c FROM NBN_Primary_Access_Technology__mdt]) {
            primaryTechnologyMap.put(obj.Label,obj);
        }
        
        for(NBN_NA_Priority__mdt obj : [SELECT Label, Priority_Description__c FROM NBN_NA_Priority__mdt]) {
            priorityMap.put(obj.Label,obj);
        }
        
        //System.debug('primaryTechnologyMap --> '+primaryTechnologyMap);
        
        // Iterate through each notification.
        for(Action_From_NBN_To_BSA__e event : hfcFromNBNList) {
            if(event.SAM_ID__c != null) { //Get SAM IDs and add to a set
                samIDs.add(event.SAM_ID__c);
            }
            if(event.Primary_Access_Technology__c != null) {
                workTypes.add(event.Primary_Access_Technology__c); // Get Primary Technology and add to a set
            }
        }
        
        system.debug('samIDs'+samIDs);
        system.debug('workTypes'+workTypes);
        
        for(Service_Territory_Mappings__c obj : [SELECT Id, Active__c, SAM_Id__c,Service_Territory__c FROM Service_Territory_Mappings__c WHERE SAM_Id__c IN : samIDs AND Active__c = True]) {
            System.debug('ST mapping'+obj);
            territoryMap.put(obj.SAM_Id__c,obj.Service_Territory__c);
             
        }
        System.debug('territoryMap'+territoryMap);
        
        
        //Get Work Type Id based on the primary technology
        for(WOrkType obj : [SELECT Id, Name, Primary_Access_Technology__c FROM WorkType WHERE Primary_Access_Technology__c IN : workTypes]) {
            workTypeMap.put(obj.Primary_Access_Technology__c,obj.Id);
        }
        
        // Iterate through each notification & create a Work Order
        for(Action_From_NBN_To_BSA__e event : hfcFromNBNList) {  
            
            //WorkOrder woObj = new WorkOrder();
            List<WorkOrder> woList = new List<WorkOrder>();
            if(event.Update_Sub_Type__c == 'Action_NBN_Cancel' && event.Activity_Status_Info__c == null) {
                Workorder woObj = new WorkOrder();
                woObj.Status = 'Canceled';
                woObj.Client_Work_Order_Number__c = event.NBN_Field_Work_Activity_Id__c;
                woObj.Action_Date__c = String.isNotBlank(event.Action_Date__c) ? UtilityClass.setStringToDateTimeFormat(event.Action_Date__c) : null ;
                woObj.Reason_Code__c = event.Reason_Code__c;
                woObj.NBN_Activity_State_Id__c = event.NBN_Activity_State_Id__c;
                woObj.NBN_Field_Work_Specified_By_Id__c = event.Work_Order_Specification__c;
                woObj.NBN_Field_Work_Specified_By_Version__c = event.NBN_Field_Work_Specified_By_Version__c;
                woObj.NBN_Field_Work_Specified_By_Category__c = event.NBN_Field_Work_Specified_By_Category__c;
                woObj.NBN_Field_Work_Specified_By_Type__c = event.NBN_Field_Work_Specified_By_Type__c;
                woObj.CorrelationID__c = event.Correlation_ID__c;
                workOrderToUpsert.add(woObj);
            }
            else{
                woList = setWorkOrderNumber(event.Activity_Status_Info__c);
            }

            if(event.Update_Sub_Type__c == 'LOG-ENTRY') {
                if(event.NBN_Log_Entry_Info__c != null) {
                    logEntryToCreate = createLogEntries(event,event.NBN_Log_Entry_Info__c);
                }
            }
            
            if(woList.size() > 0 ) {
                
                for(Workorder woObj : woList ){
                    woObj.NBN_Activity_Status_Info__c = event.Activity_Status_Info__c;
                    woObj.Update_Type__c = event.Update_Type__c == null?'NBN Action':event.Update_Type__c;
                    woObj.Update_Sub_Type__c = event.Update_Sub_Type__c == null?'Action_NBN_Assign':event.Update_Sub_Type__c;
                    woObj.CorrelationID__c = event.Correlation_ID__c;
                    woObj.NBN_Location_ID__c = event.NBN_Location_ID__c == null?'':event.NBN_Location_ID__c;
                    if(event.NBN_NA_JSON_Payload__c != null) {
                        woObj.NBN_NA_JSON_Payload__c = event.NBN_NA_JSON_Payload__c;
                    }
                    
                    if(event.Update_Sub_Type__c == 'Action_NBN_Reschedule') {
                        if(event.NBN_Appointment_End_Date_Time__c != null && event.NBN_Appointment_Start_Date_Time__c != null) {
                            woObj.EndDate = UtilityClass.setStringToDateTimeFormat(event.NBN_Appointment_End_Date_Time__c);
                            woObj.StartDate = UtilityClass.setStringToDateTimeFormat(event.NBN_Appointment_Start_Date_Time__c);
                        }
                    }
                    else if(event.Update_Sub_Type__c == 'Action_NBN_Cancel') {
                        woObj.Status = 'Canceled';
                        woObj.Client_Work_Order_Number__c = event.NBN_Field_Work_Activity_Id__c;
                        woObj.Action_Date__c = String.isNotBlank(event.Action_Date__c) ? UtilityClass.setStringToDateTimeFormat(event.Action_Date__c) : null ;
                        woObj.Reason_Code__c = event.Reason_Code__c;
                        woObj.NBN_Activity_State_Id__c = event.NBN_Activity_State_Id__c;
                    }
                    /*else if(event.Update_Sub_Type__c == 'LOG-ENTRY') {
                        logEntry = new NBN_Log_Entry__c();
                        logEntry.NBN_Field_Work_Instructions__c = event.NBN_Field_Work_Instructions__c == null?'':event.NBN_Field_Work_Instructions__c;
                        logEntry.NBN_Field_Work_Note_Description__c = event.NBN_Field_Work_Note_Description__c == null?'':event.NBN_Field_Work_Note_Description__c;
                        logEntry.Action_Type__c = 'Log Entry'; 
                        logEntry.Log_Entry_Direction__c = 'FromNBNToBSA';
                        logEntry.Field_Work_Note_ID__c = event.Field_Work_Note_ID__c;
                        logEntry.Field_Work_Note_Type__c = event.Field_Work_Note_Type__c;
                        logEntry.Field_Work_Note_Originator__c = event.Field_Work_Note_Originator__c;
                        logEntryToCreate.add(logEntry);
                    }*/
                    else if(event.Update_Sub_Type__c == 'JEOPARDY-ALERT') {
                        woObj.NBN_Jeopardy_Reason__c = event.Jeopardy_Reason__c;
                        woObj.NBN_Jeopardy_Description__c = event.Jeopardy_Description__c;
                    }
                    
                    if(woObj.Status == 'Completed') {
                        woObj.Date_Completed__c = System.now();
                    }
                    
                    if(event.NBN_Activity_Instructions__c != null) {
                        woObj.NBN_Activity_Instructions__c = event.NBN_Activity_Instructions__c;
                    }
                    
                    if(event.Item_Involves_Resource_Info__c != null) {
                        woObj.Item_Involves_Resource_Info__c = event.Item_Involves_Resource_Info__c;
                    }
                    
                    if(event.NBN_Current_Field_Work_Order_Status__c != null) {
                        woObj.NBN_Current_Field_Work_Order_Status__c = event.NBN_Current_Field_Work_Order_Status__c;
                    }
                    
                    if(event.NBN_Field_Work_Instructions__c != null) {
                        woObj.NBN_Field_Work_Instructions__c = event.NBN_Field_Work_Instructions__c;                
                    }
                    
                    woObj.Nearest_Address_Notes__c = event.Nearest_Address_Notes__c == null?'':event.Nearest_Address_Notes__c;
                    woObj.NBN_Field_Work_Specified_By_Id__c = event.Work_Order_Specification__c;
                    woObj.NBN_Field_Work_Specified_By_Version__c = event.NBN_Field_Work_Specified_By_Version__c;
                    woObj.NBN_Field_Work_Specified_By_Category__c = event.NBN_Field_Work_Specified_By_Category__c;
                    woObj.NBN_Field_Work_Specified_By_Type__c = event.NBN_Field_Work_Specified_By_Type__c;
                    woObj.NBN_Action__c = event.Action__c;
                    woObj.NBN_Parent_Work_Order_Id__c = event.NBN_Parent_Work_Order_Id__c;
                    system.debug('event.Primary_Access_Technology__c'+event.Primary_Access_Technology__c);
                    woObj.Primary_Access_Technology__c = event.Primary_Access_Technology__c;
                    woObj.NBN_Reference_ID__c = event.NBN_Reference_ID__c;
                    
                    if(event.NBN_Field_Work_Note_Description__c != null) {
                        woObj.NBN_Field_Work_Note_Description__c = event.NBN_Field_Work_Note_Description__c;
                    }
                    
                    if(woObj.Status == 'New' && (woObj.NBN_Activity_State_Id__c.contains('APPR'))) {
                        
                        woObj.OwnerId = System.Label.NBN_NA_Operations;
                        
                        if(primaryTechnologyMap.containskey(event.Primary_Access_Technology__c)) {
                            //get the corresponding record type Id
                            woObj.RecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(primaryTechnologyMap.get(event.Primary_Access_Technology__c).NBN_Record_Type__c).getRecordTypeId();
                        }
                        
                        if(event.SAM_ID__c != null) {
                            woObj.SAM_ID__c = event.SAM_ID__c;
                        }
                        else{
                            woObj.ServiceTerritoryId = System.Label.UnAllocated_Service_Territory_Id; //UnAllocated territory when no SAM ID is coming from NBN
                        }
                        
                        if(!territoryMap.containsKey(event.SAM_ID__c)){
                            woObj.ServiceTerritoryId = System.Label.UnAllocated_Service_Territory_Id; //UnAllocated territory when the SAM ID coming from NBN is not present in SF
                        }
                        else {
                            woObj.ServiceTerritoryId = territoryMap.get(event.SAM_ID__c);
                        }
                        
                        woObj.ServiceContractId = System.Label.NBN_Service_Contract_Id;
                        woObj.Classification__c = event.Classification__c;
                        
                        if(workTypeMap.containsKey(event.Primary_Access_Technology__c)) {
                            woObj.WorkTypeId = workTypeMap.get(event.Primary_Access_Technology__c);
                        }
                        
                        woObj.NBN_Primary_Contact_Name__c = event.Contact_Name__c;  

                        if(workAreaMap.containsKey(event.Region__c)) {
                            woObj.Work_Area__c = event.Region__c;
                        }      
                        else {
                            woObj.Work_Area__c = BSA_ConstantsUtility.WORKAREA_URBAN;
                        }            
                        
                        woObj.Business_Unit__c = event.Business_Unit__c;
                        woObj.Priority = event.Priority__c;
                        
                        if(priorityMap.containskey(event.Priority__c)) {
                            woObj.Priority_Description__c = priorityMap.get(event.Priority__c).Priority_Description__c;
                        }
                        
                        woObj.ADA__c = event.ADA__c;
                        woObj.FSA__c = event.FSA__c;
                        
                        if(event.Finish_No_Later_Than__c != null) {
                            woObj.EndDate = UtilityClass.setStringToDateTimeFormat(event.Finish_No_Later_Than__c);
                        }
                        if(event.Start_No_Earlier_Than__c != null) {
                            woObj.StartDate = UtilityClass.setStringToDateTimeFormat(event.Start_No_Earlier_Than__c);
                        }
                        
                        //woObj.Fault_Details__c = event.Fault_Details__c;
                        woObj.Based_Revision_Number__c = event.Based_Revision_Number__c;
                        woObj.WorkOrder_Geolocation__latitude__s = event.Latitude__c;
                        woObj.WorkOrder_Geolocation__longitude__s = event.Longitude__c;
                        woObj.Access_Seeker_ID__c = event.Access_Seeker_ID__c;
                        UtilityClass.setAddress(woObj, '', event.Lot_Number__c, event.Street_Number__c, event.Street_Name__c, event.Street_Type_Code__c, event.Locality_Name__c, event.State_Territory_Code__c, event.Post_Code__c);
                        
                    }
                    workOrderToUpsert.add(woObj);
                }
            }
            
        }
        
        System.debug('workOrderToUpsert-->'+workOrderToUpsert);
        
        // Upsert all Work Orders in the list.
        Database.UpsertResult [] results = Database.upsert(workOrderToUpsert, WorkOrder.Client_Work_Order_Number__c, false);
        
        String jsonPayload = ''; //String to build a JSON payload
        //Pass the sobject list to JSON generator apex class to build a JSON string
        jsonPayload = JSON.serialize(workOrderToUpsert);
        //jsonPayload = JSONGeneratorUtility.generateJSON(workOrderToUpsert);
        Set<Id> workOrderIDs = new Set<Id>();
        String logLevel; // String to capture Log Level(Info/Warning) on the Application Log object
        
        for(Database.upsertResult result : results) {
            
            String msg = ''; //String to write a successful/failure message
            logLevel = ''; 
            Boolean outcome = result.isSuccess();
            
            System.debug('Outcome :'+outcome);
            
            if(result.isSuccess()) {
                if(result.isCreated()) { //successfull insert
                    System.debug('WorkOrder with Id ' + result.getId() +' was created ');
                    msg = 'WorkOrder with Id ' + result.getId() +' was created ';
                }
                else { //successfull update
                    logLevel = 'Info';
                    System.debug('WorkOrder with Id ' + result.getId() +' was updated ');
                    msg = 'WorkOrder with Id ' + result.getId() +' was updated ';
                }
                
                if(!logEntryToCreate.isEmpty()){
                    for(NBN_Log_Entry__c le : logEntryToCreate) {
                        le.Work_Order__c = result.getId();
                    }
                }
                
            }
            if(!result.isSuccess()) { // failure
                System.debug('Inside failure');
                // Operation failed, so get all errors
                logLevel = 'Error';
                for(Database.Error err : result.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    msg += err.getMessage() + '\n';
                }
            }
            //Pass the result and JSON payload with a success/failure message to create an application log entry in SF
            if(logEntryToCreate.isEmpty()) {
                if(logLevel == 'Error' || logLevel == 'Info') {
                    ApplicationLogUtility.addException(result.getId(), jsonPayload, msg, logLevel, 'WorkOrder');
                }
            }
            
        }
        
        //Insert the application log entry in the SF database
        if(logEntryToCreate.isEmpty()) {
            if(logLevel != '') {
                ApplicationLogUtility.saveExceptionLog();
            }
        }
        
        System.debug('logEntryToCreate :'+logEntryToCreate);
        
        //Insert the Log Entry records
        if(!logEntryToCreate.isEmpty()) {
            insert logEntryToCreate;
        }
        
    }
    
    //Method to get the Client Work Order Number & Work Order Status from the NBN JSON payload
    private static List<Workorder> setWorkOrderNumber(String activityInfo) {
        
        List<Workorder> woList = new List<Workorder>();
        for(String obj : activityInfo.split('\\^')) {
            system.debug(obj);
            Workorder woObj = new WorkOrder();
            for(String objOne : obj.split(',')) {
                system.debug(objOne);
                if(objOne.startsWith('WO')) {
                    woObj.Client_Work_Order_Number__c = objOne;
                    system.debug('Client_Work_Order_Number__c '+woObj.Client_Work_Order_Number__c);
                }
                else if(objOne.contains('Step_') || objOne.contains('_Accept_')) {
                    String temp = objOne.substring(objOne.indexOf(':')+1);
                    system.debug('temp --> '+ temp);
                    woObj.NBN_Activity_State_Id__c = objOne;
                    if(statusMap.containskey(temp)) {
                        woObj.Status = statusMap.get(temp).FSL_Work_Order_Status__c;
                    }
                }
                else if(objOne.contains('-') && objOne.contains(':') && objOne.contains('T')) {
                    woObj.NBN_Activity_Start_Date_Time__c = UtilityClass.setStringToDateTimeFormat(objOne);
                }
                else{
                    woObj.NBN_Activity_Instantiated_By__c = objOne;
                }
            }
            woList.add(woObj);
        }
        System.debug('Multiple Activities' +woList);
        return woList;
        
    }

    //Method to create Log Entries
    private static List<NBN_Log_Entry__c> createLogEntries(Action_From_NBN_To_BSA__e peObj, String logEntryInfo) {
        
        List<NBN_Log_Entry__c> logEntryList = new List<NBN_Log_Entry__c>();
        
        for(String obj : logEntryInfo.split('\\<=>')) {
            system.debug('long string split '+obj);
            NBN_Log_Entry__c logEntry = new NBN_Log_Entry__c();
            for(String objOne : obj.split('\\^')) {
                logEntry.NBN_Field_Work_Instructions__c = peObj.NBN_Field_Work_Instructions__c == null?'':peObj.NBN_Field_Work_Instructions__c;
                logEntry.Action_Type__c = 'Log Entry'; 
                logEntry.Log_Entry_Direction__c = 'FromNBNToBSA';
                if(objOne.isNumeric()) {
                    logEntry.Field_Work_Note_ID__c = objOne;
                }
                else if(objOne == 'WFM Note' || objOne == 'TECHNICIAN NOTE') {
                    logEntry.Field_Work_Note_Type__c = objOne;
                }
                else if(objOne.contains('-') && objOne.contains(':') && objOne.contains('T') && objOne.contains('+')) {
                    logEntry.Field_Work_Note_Date_Time__c = UtilityClass.setStringToDateTimeFormat(objOne);
                }
                else if(objOne == 'SDP' || objOne == 'NBN') {
                    logEntry.Field_Work_Note_Originator__c = objOne;
                }
                else {
                    logEntry.NBN_Field_Work_Note_Description__c = objOne;
                }
            }
            logEntryList.add(logEntry);
            System.debug( 'Log Entry' +logEntryList);
        }
        
        System.debug('Multiple Log Entries' +logEntryList);
        return logEntryList;
        
    }
    
    public static List<WorkOrder> getWorkOrderList () {

        return workOrderToUpsert;
    }
    
}