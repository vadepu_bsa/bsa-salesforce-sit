/* Author: Abhijeet Anand
 * Date: 05 September, 2019
 * Utility class to generate JSON string
 */
 public class JSONGeneratorUtility {
    
    //Method to receive a generic sobject List, iterate through the collection variable and generate & return a JSON string
    public static String generateJSON(List<sobject> listToProcess) {

        // Create a JSONGenerator object.
        // Pass true to the constructor for pretty print formatting.
        JSONGenerator gen = JSON.createGenerator(true);
        System.debug('listToProcess--> ' + listToProcess);
        String objectName = String.valueof(listToProcess.getSObjectType()); //Returns the object type of this generic sobject List
        System.debug('objectName--> ' + objectName);
        
        for(sobject record : listToProcess) {     
            Map<String,Object> fieldsToValue = record.getPopulatedFieldsAsMap(); //Returns a map of populated field name and its corresponding value
            // Write data to the JSON string.
            gen.writeStartObject();

                for(String fieldName : fieldsToValue.keySet()) {
                    System.debug('field name is ' + fieldName + ', value is ' + fieldsToValue.get(fieldName));

                    String fieldLabel = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel(); // Get the field label
                    Schema.DisplayType fieldType = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe().getType(); // Get the field data type
                    
                    if(String.valueOf(fieldType) == 'STRING') {
                        gen.writeStringField(fieldLabel, (String)fieldsToValue.get(fieldName));
                    }
                    if(String.valueOf(fieldType) == 'REFERENCE') {
                        if((String)fieldsToValue.get(fieldName) != null) {
                            gen.writeStringField(fieldLabel, (String)fieldsToValue.get(fieldName));
                        }
                        else {
                            gen.writeNullField(fieldLabel);
                        }
                    }
                    else if(String.valueOf(fieldType) == 'PICKLIST') {
                        gen.writeStringField(fieldLabel, (String)fieldsToValue.get(fieldName));
                    }
                    else if(String.valueOf(fieldType) == 'DOUBLE') {
                        gen.writeNumberField(fieldLabel, (Decimal)fieldsToValue.get(fieldName));
                    }
                    else if(String.valueOf(fieldType) == 'INTEGER') {
                        gen.writeNumberField(fieldLabel, (Integer)fieldsToValue.get(fieldName));
                    }
                    else if(String.valueOf(fieldType) == 'DATE') {
                        if((Date)fieldsToValue.get(fieldName) != null) {
                            gen.writeDateField(fieldLabel, (Date)fieldsToValue.get(fieldName));
                        }
                        else{
                            gen.writeNullField(fieldLabel);
                        }
                    }
                }

            gen.writeEndObject();
        }
        
        String jsonData = gen.getAsString(); //Returns the generated JSON content
        System.debug('jsonData-> ' + jsonData);
        
        return jsonData;
        
    }
    
}