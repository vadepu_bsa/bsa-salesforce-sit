@isTest
private class APS_TaskCreationTest {

    private static List<WorkOrder> workOrders {
        get {
            if(workOrders == null) {
                workOrders = [SELECT AccountId, Maintenance_Plan__c, ServiceTerritoryId, Account.ParentId, Maintenance_Asset_Ids__c, 
                                    Account.Trade__c, JSA_Type__c, WorkTypeId, WorkType.Name 
                            FROM WorkOrder];
            }
            return workOrders;
        }
        set;
    }


    @TestSetup
    static void setup(){
        APS_TestDataFactory.testDataSetup();

                // List<Account> customers = [SELECT Id FROM Account WHERE Name = 'test account'];
        List<WorkType> workTypes = [SELECT Id FROM WorKType WHERE Name = 'Preventative Maintenance - HVAC'];
        List<Account> sites = [SELECT Id FROM Account WHERE Name = 'test site'];
        List<MaintenancePlan> maintenancePlans = [SELECT Id FROM MaintenancePlan WHERE MaintenancePlanTitle = 'Monthly'];
        List<ServiceTerritory> serviceTerritories = [SELECT Id FROM ServiceTerritory WHERE Name = 'Fire NSW'];
        // MaintenancePlan mp = [SELECT Id FROM MaintenancePlan LIMIT 1];
        APS_TaskCreationDispatcher.doChainJob = false;
        List<WorkOrder> workOrders = new List<WorkOrder>();

        Entitlement entitlement = [SELECT Id FROM Entitlement WHERE Name = 'Non Default Entitlement'];

        workOrders.addAll((List<WorkOrder>)TestRecordCreator.createSObjects('WorkOrder', 1,
            new Map<String, object>{
                'AccountId' => sites[0].Id,
                'Status' => 'New',
                'WorkTypeId' => workTypes[0].Id,
                'SuggestedMaintenanceDate' => Date.today(),
                'MaintenancePlanId' => maintenancePlans[0].Id,
                'ServiceTerritoryId' => serviceTerritories[0].Id,
                'RecordTypeId' => TestRecordCreator.getRecordTypeByDeveloperName('WorkOrder', 'APS_Work_Order'),
                'Maintenance_Plan__c' => 'Monthly',
                'EntitlementId' => entitlement.Id
            }, null));
        insert workOrders;
    }

    @IsTest
    static void createSiteTasksTest() {
        
        Test.startTest();
        List<WorkOrderLineItem> siteTaskWolis = APS_TaskCreationHelper.generateSiteTasks(workOrders);
        if(!siteTaskWolis.isEmpty()) {
            System.enqueueJob(new APS_TaskCreationDispatcher('Site Task', siteTaskWolis, workOrders, workOrders,new List<WorkOrderLineItem>{},new List<Id>{},''));
        }
        Test.stopTest();
    }

    @IsTest
    static void createBSATasksTest() {

        Test.startTest();        
        List<WorkOrderLineItem> bsaTaskWolis = APS_TaskCreationHelper.generateBsaTasks(workOrders);
        if(!bsaTaskWolis.isEmpty()) {
            System.enqueueJob(new APS_TaskCreationDispatcher('BSA Task', bsaTaskWolis, workOrders, workOrders,new List<WorkOrderLineItem>{},new List<Id>{},''));
        }
        Test.stopTest();
    }

    @IsTest
    static void createJSATasksTest() {

        Test.startTest();
        List<WorkOrderLineItem> TaskWolis = APS_TaskCreationHelper.generateJsaTasks(workOrders); // TODO: CHECK JSA TASK SIZE?
        System.debug('@TaskWolis' + TaskWolis);
    
        if(!TaskWolis.isEmpty()) {
            System.enqueueJob(new APS_TaskCreationDispatcher('JSA Task', TaskWolis, workOrders, workOrders,new List<WorkOrderLineItem>{},new List<Id>{},''));
        }
        Test.stopTest();
    }

    @IsTest
    static void createTerritoryTasksTest() {
        
        Test.startTest();
        List<WorkOrderLineItem> TaskWolis = APS_TaskCreationHelper.generateTerritoryTask(workOrders);
        if(!TaskWolis.isEmpty()) {
            System.enqueueJob(new APS_TaskCreationDispatcher('Territory Task', TaskWolis, workOrders, workOrders,new List<WorkOrderLineItem>{},new List<Id>{},''));
        }
        Test.stopTest();
    }

    @IsTest
    static void createCustomerTasksTest() {
     
        Test.startTest();
        List<WorkOrderLineItem> TaskWolis = APS_TaskCreationHelper.generateCustomerTasks(workOrders);
        Set<Id> setIds=new Set<Id>();
        setIds.add(workOrders.get(0).id);
        if(!TaskWolis.isEmpty()) {
            System.enqueueJob(new APS_TaskCreationDispatcher('Customer Task', TaskWolis, workOrders, workOrders,new List<WorkOrderLineItem>{},new List<Id>{},''));
            System.enqueueJob(new APS_TaskDeletionDispatcher(setIds,TaskWolis,''));

        }
        Test.stopTest();
    }

    @IsTest
    static void createEquipmentOnlyTasksTest() {

        List<Asset> assets = [SELECT Id FROM Asset WHERE Name = 'test equipment'];
        
        Test.startTest();
        List<WorkOrderLineItem> workOrderLineItems = 
        (List<WorkOrderLineItem>)TestRecordCreator.createSObjects('WorkOrderLineItem', 1,
            new Map<String, object>{
                'AssetId' => assets[0].Id,
                'WorkOrderId' => workOrders[0].Id, 
                'Maintenance_Routine__c' => 'Monthly',
                'RecordTypeId' => TestRecordCreator.getRecordTypeByDeveloperName('WorkOrderLineItem', 'Asset_Action')
            }, null);
        insert workOrderLineItems;

        Test.stopTest();
    }

    @IsTest
    static void cloneBsaAndSiteTasksTest() {

        ServiceResource sr = [SELECT Id FROM ServiceResource LIMIT 1];

        List<ServiceTerritory> serviceTerritories = [SELECT Id FROM ServiceTerritory WHERE Name = 'Fire NSW'];
        
        List<WorkOrderLineItem> siteTaskWolis = APS_TaskCreationHelper.generateSiteTasks(workOrders);
        insert siteTaskWolis;

        Id SARecordTypeId = APS_TestDataFactory.getRecordTypeId('ServiceAppointment', 'APS');

        // create a first SA
        FSL__Scheduling_Policy__c fslSchedule = [SELECT Id FROM FSL__Scheduling_Policy__c];
        
        List<ServiceAppointment> sa = APS_TestDataFactory.createServiceAppointment(workOrders[0].Id, serviceTerritories[0].Id, SARecordTypeId, 1);
        sa[0].Status = 'New';
        sa[0].FSL__Scheduling_Policy_Used__c= fslSchedule.Id;
        insert sa;

        Test.startTest();
        // clone the first SA
        List<ServiceAppointment> secondSa = sa.deepClone();
        insert secondSa;

        AssignedResource ar = new AssignedResource();
        ar.ServiceResourceId = sr.Id;
        ar.ServiceAppointmentId = secondSa[0].Id;
        insert ar;

        secondSa[0].Service_Resource__c = sr.Id;
        secondSa[0].Status = 'Scheduled';
        update secondSa;

        Test.stopTest();
    }

    @IsTest
    static void updateDraftWorkOrderTest1() {

        Draft_Work_Order__c draftWorkOrder = new Draft_Work_Order__c(
            User__c = UserInfo.getUserId(),
            Selection_Details__c = 'abc',
            Reference_Id__c = '1234',
            Total_Num_Of_Work_Orders__c = 1
        );

        insert draftWorkOrder;

        for(WorkOrder wo : workOrders) {
            wo.Draft_Work_Order__c = draftWorkOrder.Id;
        }

        update workOrders;

        Test.startTest();
        // test update failed work order
        List<WorkOrder> failedWorkOrders = workOrders;
        APS_WorkOrderGeneratorHelper.updateNumOfFailedWorkOrders(draftWorkOrder.Id, draftWorkOrder.Reference_Id__c, failedWorkOrders, 'failed message');
        Test.stopTest();
    }

    @IsTest
    static void updateDraftWorkOrderTest2() {

        Draft_Work_Order__c draftWorkOrder = new Draft_Work_Order__c(
            User__c = UserInfo.getUserId(),
            Selection_Details__c = 'abc',
            Reference_Id__c = '1234',
            Total_Num_Of_Work_Orders__c = 1
        );

        insert draftWorkOrder;

        Test.startTest();

        for(WorkOrder wo : workOrders) {
            wo.Draft_Work_Order__c = draftWorkOrder.Id;
        }

        update workOrders;
        
        // test update failed work order
        List<WorkOrder> failedWorkOrders = workOrders;
        APS_WorkOrderGeneratorHelper.updateNumOfFailedWorkOrders(failedWorkOrders, 'failed message');
        Test.stopTest();
    }

    @IsTest
    static void updateNextSuggestedMaintenanceDateTest() {

        List<Asset> equipmentGroups = [SELECT Id, Technician__c, Skill_Group__c, AccountId, 
                            (SELECT Id, MaintenancePlanId, AssetId, MaintenancePlan.Technician__c 
                            FROM MaintenanceAssets)
                           FROM Asset WHERE RecordType.DeveloperName = 'Equipment_Group'];

        Map<Id, List<MaintenanceAsset>> maintenanceAssetsByEgId = new Map<Id, List<MaintenanceAsset>>();

        // Map<String, Set<String>> mpIdsByEgIds = new Map<String, Set<String>>();
        Map<String, List<String>> mpIdsByEgIds = new Map<String, List<String>>();

        for(Asset eg : equipmentGroups) {
            for(MaintenanceAsset ma : eg.MaintenanceAssets) {
                if(!mpIdsByEgIds.containsKey(eg.Id)) {
                    mpIdsByEgIds.put(eg.Id, new List<String>());    
                }
                mpIdsByEgIds.get(eg.Id).add(ma.MaintenancePlanId);
            }
        }

        Draft_Work_Order__c draftWorkOrder = new Draft_Work_Order__c(
            User__c = UserInfo.getUserId(),
            Selection_Details__c = 'abc',
            Reference_Id__c = '1234',
            Total_Num_Of_Work_Orders__c = 1
        );

        insert draftWorkOrder;

        Test.startTest();
        for(WorkOrder wo : workOrders) {
            wo.Draft_Work_Order__c = draftWorkOrder.Id;
            wo.EGs_wt_MPs__c = JSON.serialize(mpIdsByEgIds);
            //wo.Site_Tasks_Generated__c = true;
            wo.Number_of_Remaining_Assets__c = 0;
        }
        
        update workOrders;
        Test.stopTest();
    }
    
    

}