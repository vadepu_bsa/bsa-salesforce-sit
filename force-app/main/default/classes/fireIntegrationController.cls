public class fireIntegrationController {
    @auraEnabled 
    public static void execute(string recId, string ObjName){
        UtilityClass.fireOutboundPlatformEvent(recId,ObjName);
    }
}