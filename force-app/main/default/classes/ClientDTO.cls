/**
 * @File Name          : ClientDTO.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 03-15-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    16/05/2020  Mahmood Zubair      Initial Version
**/

public class ClientDTO  implements IDTO{
    public AccountDetails accountDetails {get;set;} 
    public List<ContactDetails> contactDetails {get;set;} 
    public ClientDTO(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'accountDetails') {
						accountDetails = new AccountDetails(parser);
					} else if (text == 'contactDetails') {
						contactDetails = arrayOfContactDetails(parser);
                    } else {
						System.debug(LoggingLevel.WARN, 'ClientDTO consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
        
    private static List<ContactDetails> arrayOfContactDetails(System.JSONParser p) {
        List<ContactDetails> res = new List<ContactDetails>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new ContactDetails(p));
        }
        return res;
    }

	
	public class ContactDetails {
		public String externalContactId {get;set;} 
		public String type_Z {get;set;} // in json: type
		public String name {get;set;} 
		public String phoneNumber {get;set;} 
		public String afterHoursPhoneNumber {get;set;} 
		public String role {get;set;} 
		public String email {get;set;} 
		public String notes {get;set;} 
		public String privacyStatementIssued {get;set;} 
		public String privacyStatementIssueDate {get;set;} 
		public String availableTimes {get;set;} 

		public ContactDetails(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'contactID') {
							externalContactId = parser.getText();
						} else if (text == 'contactType') {
							type_Z = parser.getText();
						} else if (text == 'contactName') {
							name = parser.getText();
						} else if (text == 'phoneNumber') {
							phoneNumber = parser.getText();
						} else if (text == 'afterHoursPhoneNumber') {
							afterHoursPhoneNumber = parser.getText();
						} else if (text == 'role') {
							role = parser.getText();
						} else if (text == 'email') {
							email = parser.getText();
						} else if (text == 'notes') {
							notes = parser.getText();
						} else if (text == 'privacyStatementIssued') {
							privacyStatementIssued = parser.getText();
						} else if (text == 'privacyStatementIssueDate') {
							privacyStatementIssueDate = parser.getText();
						} else if (text == 'availableTimes') {
							availableTimes = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'ContactDetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	
	
	public class AccountDetails {
	   	public String accountcode {get;set;} 
		public String billto {get;set;} 
        public string businesscode {get;set;}
		public String contractbaseanniversarydate {get;set;} 
		public String contractbillingcycle {get;set;} 
		public String contractbranchcode {get;set;} 
		public String contractchargeflag2 {get;set;} 
		public String contractchargeflag4 {get;set;} 
		public String contractcontactphone {get;set;} 
		public String contractenddate {get;set;} 
		public String contractinvoicedfromdate {get;set;} 
		public String contractinvoicedtodate {get;set;} 
		public String contractinvoicenumber {get;set;} 
		public String contractmiscfield {get;set;} 
		public String contractno {get;set;} 
		public String contractonholdreasoncode {get;set;} 
		public String contractperiod {get;set;} 
		public String contractreference {get;set;} 
		public String contractrepcode {get;set;} 
		public String contractreviewdate {get;set;} 
		public String contractsitedesc {get;set;} 
        public String contractsitedesc1 {get;set;} 
        public String contractsitedesc2 {get;set;} 
        public String contractsitedesc3 {get;set;} 
		public String contractstartdate {get;set;} 
		public String contractstatus {get;set;} 
		public String contracttfrflag {get;set;} 
        public String correlationId {get;set;}
        public string cycleamount {get;set;}
        public String debstatus {get;set;} 
		public String drcusttype {get;set;} 
		public String drindustrycode {get;set;} 
        public String drmarketingflag {get;set;} 
		public String naaddress6 {get;set;} 
		public String naaddress7 {get;set;} 
		public String nacompany {get;set;} 
		public String nacountry {get;set;} 
		public String nafaxno {get;set;} 
        public String email {get;set;} 
        public String buildingname {get;set;} 
        public String abn {get;set;} 
        public String aacn {get;set;} 
        public String naphone {get;set;}  
	    public String nastreet {get;set;} 
		public String nasuburb {get;set;} 
		public String nasuburbstate {get;set;} 
		public String postcode {get;set;} 
        public String repcode {get;set;} 
        public String shortname {get;set;} 
        public String territory {get;set;} 
        public String warehouse {get;set;} 
        public string drcompanymask {get;set;}
        public string drreportingstate {get;set;}
        public string drcreditlimitamount {get;set;}
        public string cedmarkupkey {get;set;}
        public string clas {get;set;}
        public string hbld {get;set;}
        public string hcmp {get;set;}
        public string hlse {get;set;}
        public string hltp {get;set;}
        public string unit {get;set;}
		public string ndqt {get;set;}
		public string uoad {get;set;}
		public string bsig {get;set;}
		public string nacc {get;set;}
		public string ssra {get;set;}
		public string emsr {get;set;}
		public string pcbk {get;set;}
		public string trde {get;set;}
        
		public boolean ndqt_Available {get;set;}
		
		public boolean nacc_Available {get;set;}
		public boolean pcbk_Available {get;set;}
		public boolean trde_Available {get;set;}

		public AccountDetails(JSONParser parser) {
			ndqt_Available = false;

			nacc_Available = false;
			pcbk_Available = false;
			trde_Available = false;
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'accountcode') {
							accountcode = parser.getText();
						} else if (text == 'bill-to') {
							billto = parser.getText();
						}else if (text == 'business-code') {
							businesscode = parser.getText();
                        } else if (text == 'contract-base-anniversary-date') {
							contractbaseanniversarydate = parser.getText();
						} else if (text == 'contract-billing-cycle') {
							contractbillingcycle = parser.getText();
						} else if (text == 'contract-branch-code') {
							contractbranchcode = parser.getText();
						} else if (text == 'contract-charge-flag[2]') {
							contractchargeflag2 = parser.getText();
						} else if (text == 'contract-charge-flag[4]') {
							contractchargeflag4 = parser.getText();
						} else if (text == 'contract-contact-phone') {
							contractcontactphone = parser.getText();
						} else if (text == 'contract-invoiced-from-date') {
							contractinvoicedfromdate = parser.getText();
						} else if (text == 'contract-invoiced-to-date') {
							contractinvoicedtodate = parser.getText();
						} else if (text == 'contract-invoice-number') {
							contractinvoicenumber = parser.getText();
						} else if (text == 'contract-misc-field') {
							contractmiscfield = parser.getText();
						} else if (text == 'contract-no') {
							contractno = parser.getText();
						} else if (text == 'contract-on-hold-reason-code') {
							contractonholdreasoncode = parser.getText();
						} else if (text == 'contract-period') {
							contractperiod = parser.getText();
						} else if (text == 'contract-reference') {
							contractreference = parser.getText();
						} else if (text == 'contract-rep-code') {
							contractrepcode = parser.getText();
						} else if (text == 'contract-review-date') {
							contractreviewdate = parser.getText();
						} else if (text == 'contract-site-desc[1]') {
							contractsitedesc1 = parser.getText();
						} else if (text == 'contract-site-desc[2]') {
							contractsitedesc2 = parser.getText();
						} else if (text == 'contract-site-desc[3]') {
							contractsitedesc3 = parser.getText();
						} else if (text == 'contract-start-date') {
							contractstartdate = parser.getText();
						} else if (text == 'contract-end-date') {
							contractenddate = parser.getText();
						} else if (text == 'contract-status') {
							contractstatus = parser.getText();
						} else if (text == 'contract-tfr-flag') {
							contracttfrflag = parser.getText();
						}else if (text == 'correlationId') {
							correlationId = parser.getText();
						}else if (text == 'cycle-amount') {
							cycleamount = parser.getText();
						} else if (text == 'deb-status') {
							debstatus = parser.getText();
						} else if (text == 'dr-cust-type') {
							drcusttype = parser.getText();
                        } else if (text == 'dr-industry-code') {
							drindustrycode = parser.getText();
						}else if (text == 'dr-marketing-flag') {
							drmarketingflag = parser.getText();
						} else if (text == 'na-address-6') {
							naaddress6 = parser.getText();
						} else if (text == 'na-address-7') {
							naaddress7 = parser.getText();
						} else if (text == 'na-company') {
							nacompany = parser.getText();
						} else if (text == 'na-country') {
							nacountry = parser.getText();
						} else if (text == 'na-fax-no') {
							nafaxno = parser.getText();
						} else if (text == 'email') {
							email = parser.getText();
						} else if (text == 'building-name') {
							buildingname = parser.getText();
                        } else if (text == 'abn') {
							abn = parser.getText();
                        } else if (text == 'aacn') {
							aacn = parser.getText();
						} else if (text == 'na-phone') {
							naphone = parser.getText();
						} else if (text == 'na-street') {
							nastreet = parser.getText();
						} else if (text == 'na-suburb') {
							nasuburb = parser.getText();
                        } else if (text == 'na-suburb_state') {
							nasuburbstate = parser.getText();
						} else if (text == 'post-code') {
							postcode = parser.getText();
						} else if (text == 'rep-code') {
							repcode = parser.getText();
						 } else if (text == 'shortname') {
							shortname = parser.getText();
						} else if (text == 'territory') {
							territory = parser.getText();
						} else if (text == 'warehouse') {
							warehouse = parser.getText();
                        }else if (text == 'dr-company-mask') {
							drcompanymask = parser.getText();
                        }else if (text == 'dr-user-only-alpha4-2') {
							drreportingstate = parser.getText();
                        }else if (text == 'dr-credit-limit-amount') {
							drcreditlimitamount = parser.getText();
                        } else if (text == 'ced-markup-key'){
                            cedmarkupkey = parser.getText();
                        } else if (text == 'attributeCLAS') {
							clas = parser.getText();
						} else if (text == 'attributeHBLD') {
							hbld = parser.getText();
                        }else if (text == 'attributeHCMP') {
							hcmp = parser.getText();
                        }else if (text == 'attributeHLSE') {
							hlse = parser.getText();
                        }else if (text == 'attributeHLTP') {
							hltp = parser.getText();
                        } else if (text == 'attributeUNIT'){
                            unit = parser.getText();
                        } else if (text == 'attributeNDQT'){
							ndqt_Available = true;
							ndqt = parser.getText();
                        } else if (text == 'attributeUOAD'){
							uoad = '***Check***';
                        } else if (text == 'attributeBSIG'){
							bsig = '***Check***';
                        } else if (text == 'attributeSSRA'){
							ssra = '***Check***';
                        } else if (text == 'attributeEMSR'){
							emsr = '***Check***';
                        } else if (text == 'attributeNACC'){
							nacc_Available = true;
							nacc = parser.getText();
                        } else if (text == 'attributePCBK'){
							pcbk_Available = true;
							pcbk = parser.getText();
                        } else if (text == 'attributeTRDE'){
							trde_Available = true;
							trde = parser.getText();
                        } else {
							System.debug(LoggingLevel.WARN, 'accountdetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
				 contractsitedesc =   (contractsitedesc1 != null) ? contractsitedesc1 : '';
                 contractsitedesc =   (contractsitedesc2 != null) ? contractsitedesc+contractsitedesc2: contractsitedesc;
				 contractsitedesc =   (contractsitedesc3 != null) ? contractsitedesc+contractsitedesc3: contractsitedesc;				         
			}
			uoad = (uoad == '***Check***')? 'true': 'false';
			ndqt = (ndqt_Available == true)? ndqt: '';

			bsig = (bsig == '***Check***')? 'true': 'false';
			ssra = (ssra == '***Check***')? 'true': 'false';
			emsr = (emsr == '***Check***')? 'true': 'false';
			nacc = (nacc_Available == true)? nacc: '';
			pcbk = (pcbk_Available == true)? pcbk: '';
			trde = (trde_Available == true)? trde: '';
		}
	}
	
	
    
    public  ClientDTO() {
	}
	
	public  ClientDTO parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new ClientDTO(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

  
    public String getSpecificDTOClassName(){

        return 'ClientDTO';
    }


}