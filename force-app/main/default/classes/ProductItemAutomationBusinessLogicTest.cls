@isTest
public class ProductItemAutomationBusinessLogicTest {
    static testMethod void testValidSharingOfProductItemObj(){
        
        Test.startTest();
        
        // Create a test user record
        Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Field Technician' LIMIT 1];
        User userRec = new User( email='test.user@gmail.com',
                            profileid = profileDetails.Id, 
                            UserName='test.ProductItemAutomationBusinessLogicTest.user1@gmail.com', 
                            Alias = 'GDS',
                            TimeZoneSidKey='America/New_York',
                            EmailEncodingKey='ISO-8859-1',
                            LocaleSidKey='en_US', 
                            LanguageLocaleKey='en_US',
                            PortalRole = 'Manager',
                            FirstName = 'Test',
                            LastName = 'User');
         insert userRec;
        
        // Create a test location record
        Schema.Location locationRecord = new Schema.Location();
        locationRecord.name = 'Testing Location';
        locationRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        locationRecord.Description = 'Testing Location';
        locationRecord.Van_Id__c = 'test0007';
        locationRecord.LocationType = 'Warehouse';
        locationRecord.IsMobile = true;
        locationRecord.IsInventoryLocation = true;
        locationRecord.Territory__c = 'NSW';
        locationRecord.Correlation_ID__c = '88a57e4e-b22b-4683';
        locationRecord.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        List<Schema.Location> locationList = new List<Schema.Location>();
        locationList.add(locationRecord);
        insert locationRecord;

        ApplicationLogUtility.addException(locationRecord.id, JSON.serialize(locationList), 'Testing', 'Success', 'Location');
        
        // Create a 2nd test Location record
        Schema.Location locationRecord2 = new Schema.Location();
        locationRecord2.name = 'Test 2nd Location';
        locationRecord2.Pronto_Service_Resource_ID__c = 'srid-b22b-78965';
        locationRecord2.Description = 'Test 2nd Location';
        locationRecord2.Van_Id__c = 'test1987';
        locationRecord2.LocationType = 'Warehouse';
        locationRecord2.IsMobile = true;
        locationRecord2.IsInventoryLocation = true;
        locationRecord2.Territory__c = 'NSW';
        locationRecord2.Correlation_ID__c = '88a57e4e-4683-7e4e';
        locationRecord2.Event_TimeStamp__c = '2019-03-03T00:35:11.000Z';
        
        insert locationRecord2;
        
        // Create a test Service Resource record
        ServiceResource serviceResourceRecord = new ServiceResource();
        serviceResourceRecord.Name = 'Test user';
        serviceResourceRecord.User_Id__c = userRec.Id;
        serviceResourceRecord.RelatedRecordId = userRec.Id;
        serviceResourceRecord.IsActive = true;
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        serviceResourceRecord.LocationId = locationRecord.id;
        
        insert serviceResourceRecord;
        
        // Create a test product record
        Product2 productRecord = new Product2();
        productRecord.Name = 'Test Product Name';
        productRecord.Product_Code__c = 'TestProductCode';
        productRecord.Client_ID__c = 'cl007';                
        
        insert productRecord;
        
        ApplicationLogUtility.addException(productRecord.id, JSON.serialize(new List<Product2>{productRecord}), 'Testing', 'Success', 'Product2');
        
        // Create a test productitem record
        productitem productItemRecord = new productitem();
        productItemRecord.Correlation_ID__c = locationRecord.Correlation_ID__c;
        productItemRecord.Product2Id = productRecord.Id;
        productItemRecord.QuantityOnHand = 10;
        productItemRecord.LocationId = locationRecord.id;
        productItemRecord.QuantityUnitOfMeasure = 'Each';
            
        insert productItemRecord;
        
        ApplicationLogUtility.addException(productItemRecord.id, JSON.serialize(new List<ProductItem>{productItemRecord}), 'Testing', 'Success', 'ProductItem');
        
        update productItemRecord;
        
        //Create a ProductTransfer record
        ProductTransfer ptr = new ProductTransfer();
        ptr.QuantityUnitOfMeasure = 'Each';
        ptr.Status = 'Ready For Pickup';
        ptr.QuantitySent = 2;
        ptr.SourceProductItemId = productItemRecord.Id;
        ptr.SourceLocationId = locationRecord.Id;
        ptr.DestinationLocationId = locationRecord2.Id;
        ptr.QuantityReceived = 2;
        
        insert ptr;
        
        ProductItemTransaction pti = new ProductItemTransaction();
        pti.ProductItemId = productItemRecord.Id;
        //pti.RelatedRecordId = ptr.Id;
        pti.TransactionType = 'Transferred';
        pti.Quantity = 2;
        
        insert pti;
        
        ptr.Status = 'Completed';
        update ptr;
        
        Id groupId = UtilityClass.getGroupIdByName('Operations');
        
        // Query job sharing records.
        List<productitemShare> objShrs = [SELECT Id, UserOrGroupId, AccessLevel, 
         RowCause FROM productitemShare WHERE ParentId = :productItemRecord.Id AND UserOrGroupId= :groupId];
        
        System.debug('DildarLog: - ' + objShrs);
        
        Test.stopTest();
                
        // Validate that the sharing object record gets created.
        System.assertEquals(1, objShrs.size());
    }
}