public class CustomAttributeTriggerHandler implements ITriggerHandler {
    public static Boolean TriggerDisabled = false;
    public static Map<Id,WorkOrder> workOrderMapForMapping = new Map<Id,WorkOrder>();
    public static Boolean taskGenerationInProgress = false;
    /*
    // ADDED FOR CUI CLAIMS DEPLOYMENT
    public static Boolean caExists = false;
    public static Boolean woExists = false;
    public static Boolean wotExists = false;
    public static Boolean dublicateCAExists = false;

    
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }
    
    public void BeforeInsert(List<SObject> workOrderList) { 
    }
    public void AfterInsert(Map<Id, SObject> newItems) { 
        set<Id> parentWoIds = new set<Id>();
        Set<String> wotasksIds = new Set<String>();
        Set<String> childWotasksIds = new Set<String>();
        List<Custom_Attribute__c> newCustomAttributeList = new List<Custom_Attribute__c>();
        List<Custom_Attribute__c> lstCAToInsert = new List<Custom_Attribute__c>();
        Map<Id,Custom_Attribute__c> mcaMap = new Map<Id,Custom_Attribute__c>();
        Map<Id,Custom_Attribute__c> mcaWOTaskMap = new Map<Id,Custom_Attribute__c>();
        for(SObject obj : newItems.values()) {
            Custom_Attribute__c caObj = (Custom_Attribute__c)obj;
            newCustomAttributeList.add(caObj);
            //Rohan Fr-279 Added if condition to stop creating duplicate record if Custom Attribute is created from FLow (Unify - Create/Update Custom Attribute)
            if(caObj.FromCode__c != false)
            {
                if(caObj.Work_Order__c!=null && (caObj.Parent_WO_Id__c== null || caObj.Parent_WO_Id__c=='')){
            	parentWoIds.add(caObj.Work_Order__c);
                mcaMap.put(caObj.Id,caObj);
                }
                else if(caObj.Work_Order__c==null && caObj.Work_Order_Task__c!=null){
                    wotasksIds.add(caObj.ExternalId_WOTask__c+'%');
                    mcaWOTaskMap.put(caObj.Id,caObj);
                }
            }

        }
        if(wotasksIds!=null && wotasksIds.size()>0){
            lstCAToInsert = CustomAttributeTriggerHelper.CopyCAfromWoTask(wotasksIds,mcaWOTaskMap,lstCAToInsert);
        }
        
        if(parentWoIds!=null && parentWoIds.size()>0){
            lstCAToInsert = CustomAttributeTriggerHelper.CopyCAfromWO(parentWoIds,mcaMap,lstCAToInsert);
        }
        System.debug('lstCAToInsert:'+lstCAToInsert);
        if(lstCAToInsert!=null && lstCAToInsert.size()>0){
            Insert lstCAToInsert;
        }
        
        
        //copy pcr_group and specification attributes
        CustomAttributeTriggerHelper.copyAttributesOnParent(newCustomAttributeList);
        
    }
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        List<Custom_Attribute__c> updatedCustomAttributeList = new List<Custom_Attribute__c>();
        List<Custom_Attribute__c> UpdateParentTasksFromChild = new List<Custom_Attribute__c>();
        List<Custom_Attribute__c> UpdateChildTasksFromParent = new List<Custom_Attribute__c>();
        Map<Id,List<Custom_Attribute__c>> mIdtoWotask = new Map<Id,List<Custom_Attribute__c>>();
        set<Id> parentWoIds = new set<Id>();
        set<Id> parentWotaskIds = new set<Id>();
        System.debug('TriggerDisabled -->'+TriggerDisabled);
        if(TriggerDisabled) return;
        for(SObject obj : newItems.values()) {
            Custom_Attribute__c woTaskObj = (Custom_Attribute__c)obj;
            updatedCustomAttributeList.add(woTaskObj);
            Custom_Attribute__c oldWoTaskObj = (Custom_Attribute__c)oldItems.get(woTaskObj.Id);
            if(woTaskObj.CA_External_Id__c.contains(':')){
                String extId = woTaskObj.CA_External_Id__c;
                List<String> extIds = extId.split(':');
                /*Custom_Attribute__c objWotask = woTaskObj.clone(false, false, false, false); 
                objWotask.CA_External_Id__c = extIds[0];
                if(woTaskObj.Work_Order__c!=null){
                	objWotask.Work_Order__c = woTaskObj.Parent_WO_Id__c;
                }
                else{
                    objWotask.Work_Order_Task__c = woTaskObj.Parent_WO_Id__c;
                }*/
                Custom_Attribute__c objWotask = new Custom_Attribute__c(); 
                objWotask.CA_External_Id__c = extIds[0];
                objWotask.Value__c = woTaskObj.Value__c;
                objWotask.FromCode__c = True;
                UpdateParentTasksFromChild.add(objWotask);
            }
            else if(woTaskObj.Work_Order__c!=null){
                parentWoIds.add(woTaskObj.Work_Order__c);
                List<Custom_Attribute__c> newTaskList;
                if(mIdtoWotask.keyset().contains(woTaskObj.Work_Order__c)){
                    newTaskList = mIdtoWotask.get(woTaskObj.Work_Order__c);
                }
                else{
                    newTaskList = new List<Custom_Attribute__c>();
                }
                newTaskList.add(woTaskObj);
                mIdtoWotask.put(woTaskObj.Work_Order__c,newTaskList);
            }
            else if(woTaskObj.Work_Order_task__c!=null){
                parentWoIds.add(woTaskObj.Work_Order_Task__r.Work_order__c);
                List<Custom_Attribute__c> newTaskList;
                if(mIdtoWotask.keyset().contains(woTaskObj.Work_Order_Task__r.Work_order__c)){
                    newTaskList = mIdtoWotask.get(woTaskObj.Work_Order_Task__r.Work_order__c);
                }
                else{
                    newTaskList = new List<Custom_Attribute__c>();
                }
                newTaskList.add(woTaskObj);
                mIdtoWotask.put(woTaskObj.Work_Order_Task__r.Work_order__c,newTaskList);
            }
        }
        system.debug('UpdateParentTasksFromChild=='+UpdateParentTasksFromChild);
        if(UpdateParentTasksFromChild!=null && UpdateParentTasksFromChild.size()>0){
            TriggerDisabled =true;
            Upsert UpdateParentTasksFromChild CA_External_Id__c;
            
        }
        /*if(parentWoIds!=null & parentWoIds.size()>0)
        {
            List<WorkOrder> lstChildWos = [select id,ParentWorkOrderId from workorder where ParentWorkOrderId in:parentWoIds and Service_Resource__c!=''];
            for(WorkOrder objWO: lstChildWos){
                
                for(Custom_Attribute__c objTask: mIdtoWotask.get(objWO.ParentWorkOrderId)){
                    Custom_Attribute__c objWotask = objTask.clone(false, false, false, false); 
                    objWotask.CA_External_Id__c = objTask.CA_External_Id__c+':'+objWO.Id;
                    objWotask.Work_Order__c =objWO.Id;
                    UpdateChildTasksFromParent.add(objWotask);
                }
            }
        }
        if(parentWoTaskIds!=null & parentWoTaskIds.size()>0)
        {
            List<WorkOrder> lstChildWos = [select id,ParentWorkOrderId from workorder where ParentWorkOrderId in:parentWoIds and Service_Resource__c!=''];
            for(WorkOrder objWO: lstChildWos){
                
                for(Custom_Attribute__c objTask: mIdtoWotask.get(objWO.ParentWorkOrderId)){
                    Custom_Attribute__c objWotask = objTask.clone(false, false, false, false); 
                    objWotask.CA_External_Id__c = objTask.CA_External_Id__c+':'+objWO.Id;
                    objWotask.Work_Order__c =objWO.Id;
                    UpdateChildTasksFromParent.add(objWotask);
                }
            }
        }
        if(UpdateChildTasksFromParent!=null && UpdateChildTasksFromParent.size()>0){
            TriggerDisabled =true;
            System.debug('TriggerDisabled inSide If--'+TriggerDisabled);
        	//upsert UpdateChildTasksFromParent CA_External_Id__c;
            
        }*/
        
        //copy pcr_group and specification attributes
        CustomAttributeTriggerHelper.copyAttributesOnParent(updatedCustomAttributeList);
        
    }
    public void BeforeDelete(Map<Id, SObject> oldItems) {}  public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}

}