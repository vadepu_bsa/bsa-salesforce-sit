global class CustomIterable implements Iterator<ContentDocumentLink> { 

   List<ContentDocumentLink> cdls {get; set;} 
   Integer i {get; set;} 

   public CustomIterable() { 
       cdls =  [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN (SELECT Id FROM WorkOrderLineItem WHERE WorkOrder.ParentWorkOrder.Createdby.Name = 'Automated Process' AND WorkOrder.Gdrive_Artifact_Path__c = '') LIMIT 2]; 
       i = 0; 
   }   

   global boolean hasNext() { 
       if(i >= cdls.size()) {
           return false; 
       } else {
           return true; 
       }
   }    

   global ContentDocumentLink next() { 
       if(i == 5){return null;} 
       i++; 
       return cdls[i-1]; 
   } 
   
}