/**
* @author          Dildar Hussain
* @date            06/March/2020
* 
**/
public class InvoiceTriggerHelper {
    
    public static void recalculateFieldsForFinancialReport(List<Invoice__c> newInvoiceList, Map<id, SObject> oldInvoiceMap){
        Set<Id> workOrderIdSet = new Set<Id>();
        //get the workorders to calculate Actual Revenue - Approved / Invoiced / Sent To Client / Pending Approval with/without invoice no. and sales order number populated in associated Invoice record
        Set<Id> pendingWorkOrderList = New Set<Id>();
        //get the workorders to calculate Actual Accrued Revenue - Pending Approval with no invoice number in associated Invoice
        Map<Id,WorkOrder> pendingApprovalWorkOrderMap = New Map<Id,WorkOrder>();
        //get the workorders to calculate Actual Revenue - Approved / Invoiced / Sent To Client / Pending Approval with invoice no. and sales order number populated in associated Invoice record
        Map<Id,WorkOrder> approvedWorkOrderMap = New Map<Id,WorkOrder>();
        
        for(Invoice__c newInvoice: newInvoiceList){
            Invoice__c oldInvoice = (Invoice__c)oldInvoiceMap.get(newInvoice.Id);
            if((newInvoice.Invoice_Number__c != oldInvoice.Invoice_Number__c) || (newInvoice.BSA_Total__c != oldInvoice.BSA_Total__c)){
                workOrderIdSet.add(newInvoice.Work_Order__c);
            }
        }
                
        for(WorkOrder workOrderRecord: [Select Id, Est_Accrued_Revenue__c, Actual_Revenue__c, Actual_Accrued_Revenue__c, Previous_Actual_Revenue__c, Previous_Est_Accrued_Revenue__c,Previous_Actual_Accrued_Revenue__c, ParentWorkOrderId, Sub_Status__c from WorkOrder Where Id IN:workOrderIdSet]){
            if(workOrderRecord.ParentWorkOrderId == null && !BSA_ConstantsUtility.EST_ACCRUED_REVENUE_SUB_STATUS_NOT.Contains(workOrderRecord.Sub_Status__c)){
                pendingWorkOrderList.add(workOrderRecord.Id);
            }else if(workOrderRecord.ParentWorkOrderId == null && BSA_ConstantsUtility.ACTUAL_REVENUE_SUB_STATUS.Contains(workOrderRecord.Sub_Status__c)){
            	If(workOrderRecord.Sub_Status__c == 'Pending Approval')
                    pendingApprovalWorkOrderMap.put(workOrderRecord.id,workOrderRecord);
                else
                    approvedWorkOrderMap.put(workOrderRecord.Id, workOrderRecord);
           }
        }
        if(pendingWorkOrderList.size() > 0){
            WorkOrderTriggerHelper.populateEstAccruedRevenue(pendingWorkOrderList);
        }if(pendingApprovalWorkOrderMap.size() > 0){
            WorkOrderTriggerHelper.populateActualAccruedRevenue(pendingApprovalWorkOrderMap);
        }if(approvedWorkOrderMap.size() > 0){
            WorkOrderTriggerHelper.populateActualRevenue(approvedWorkOrderMap);
        }
    }
}