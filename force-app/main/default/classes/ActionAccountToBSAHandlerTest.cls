/**
 * @description       : 
 * @author            : Karan Shekhar
 * @group             : 
 * @last modified on  : 03-15-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   12-14-2020   Karan Shekhar   Initial Version
**/
//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = false)
public class ActionAccountToBSAHandlerTest {
   @testSetup static void setup() {
        
        OperatingHours ohObj = new OperatingHours();
        ohObj.Name = 'Testing';
        ohObj.TimeZone = 'Australia/Sydney';
        insert ohObj;
        ServiceTerritory stObj = new ServiceTerritory();
        stObj.Name = 'Testting';
        stObj.Pronto_ID__c = 'TFSS';
        stObj.IsActive = True;
        stObj.Description = 'Testing';
        stObj.APS__c = true;
        stObj.OperatingHoursId = ohObj.Id;
        insert stObj;
       
            Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
       Account acc = new Account();
       acc.CustomerCode__c = 'NSWTF001';
       acc.Name = 'Testing';
       acc.RecordTypeid = AccountClientRTypeId;
       insert acc;
    }
    
    @isTest
    static void InsertClientTest() {
      
        Action_Account_To_BSA__e event = new Action_Account_To_BSA__e();
        event.Action__c='CreateClient';
        event.EventTimeStamp__c='2020-08-25T07:19:51Z';
        event.AccountCode__c='NSWTF002';
        event.ContractNumber__c='';
        event.CorrelationId__c='9eb99c5f-fdeb-6edc-aa4c-a08d33434234325345234334';
       // System.Debug('!EventOrderNumber '+event.OrderNumber__c);
        event.Account__c ='"accountDetails":{"rep-code":"DFO","na-phone":"0457 594 864","shortname":"HOUSING NSW","na-fax-no":"02 8753 8406","cycle-amount":"100","building-name":"test002", "aacn":"123456789","bill-to":"NSWTF002","accountcode":"NSWTF002","warehouse":"TFSS","dr-cust-type":"TF","dr-industry-code":"ENDU","na-street":"ASHFIELD NSW","na-suburb":"","na-suburb-state":"","na-suburb_state":"test","post-code":"1800","na-country":"","na-company":"LOCKED BAG 4001","na-address-6":"","na-address-7":"","deb-status":"Active/Normal","business-code":"BUF6","abn":"45 754 121 940","dr-credit-limit-amount":"20000000","dr-company-mask":"","email":"Mike.Mahendran@facs.nsw.gov.au","dr-user-only-alpha4-2":"NSW","dr-marketing-flag":"04","dr-part-shipment-allowed":"Y","dr-order-priority":"5","dr-price-disc-by-bill-to":"Y","dr-billing-control-code":"","dr-mail-control-code":"","dr-industry-sub-group":"BS"}';
        event.Contact__c ='"contactDetails":[{"contactID": "R5345234534534", "contactType": "NOC Contact","afterHoursPhoneNumber": "12345678912", "role": "test", "email": "test242email@gmail.com", "privacyStatementIssued": "true","invalid": "","contactName": "NOC Restore", "phoneNumber": "1300 626 677", "notes":"2020-05-13 15:30:31 - System (Notes) call noc before change" }]';

        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
        Test.stopTest();

       // Order order2 = [SELECT Pronto_Status__c From Order WHERE Name='Test12345' LIMIT 1];
        //System.assertEquals('01',order2.Pronto_Status__c);
    }
    
    
      @isTest
    static void InsertSiteTest() {
      
 
        Action_Account_To_BSA__e event2 = new Action_Account_To_BSA__e();
        event2.Action__c = 'CreateSite';
        event2.AccountCode__c ='NSWTF001';
        event2.ContractNumber__c ='45-049';
        event2.EventTimeStamp__c ='2020-09-16T05:09:58Z';
        event2.CorrelationId__c ='ID000000000025';
        event2.Account__c = '"accountDetails":{"accountcode":"NSWTF001","contract-site-desc[1]":"B05507 - Block A - Unit 1-2 an","contract-site-desc[2]":"d 5-6 and Common Room 368 Macq","contract-site-desc[3]":"uarie St Dubbo","territory":"","contract-rep-code":"","contract-branch-code":"TFSS","contract-start-date":"2010-11-01","contract-tfr-flag":"PM","contract-period":"52","contract-no":"45-049","contract-end-date":"2015-02-28","contract-review-date":"","na-name":"","contract-status":"Active","contract-on-hold-reason-code":"","contract-contact-phone":"","na-street":"","na-suburb":"","na-postcode":"","na-country":"","contract-charge-flag[2]":"Advance","contract-reference":"B05507","contract-billing-cycle":"N","contract-invoice-number":"0","contract-invoiced-from-date":"2010-11-01","contract-invoiced-to-date":"2010-11-01","contract-misc-field":"GE01","contract-charge-flag[4]":"N","contract-base-anniversary-date":"","xca-code":"","xca-description":"","attributeCLAS":"test","attributeHBLD":"test2","attributeHCMP":"test3","attributeHLSE":"test4","attributeHLTP":"Test5","attributeUNIT":"test6"}';        
        event2.Contact__c = '"contactDetails":null';
        test.startTest();
        Database.SaveResult sr2 = EventBus.publish(event2);
        Test.stopTest();

       // Order order2 = [SELECT Pronto_Status__c From Order WHERE Name='Test12345' LIMIT 1];
        //System.assertEquals('01',order2.Pronto_Status__c);
    }

    /*
    @isTest
    static void UpsertSite1Test() {
      
        System.debug('Ananti Test UpsertSite1Test Start');
        
        Action_Account_To_BSA__e event2 = new Action_Account_To_BSA__e();
        event2.Action__c = 'CreateSite';
        event2.AccountCode__c ='NSWTF001';
        event2.ContractNumber__c ='45-049';
        event2.EventTimeStamp__c ='2020-09-16T05:09:58Z';
        event2.CorrelationId__c ='ID000000000025';
        event2.Account__c = '"accountDetails":{"accountcode":"NSWTF001","attributePCBK":"APS Housing NSW","contract-site-desc[1]":"B05507 - Block A - Unit 1-2 an","contract-site-desc[2]":"d 5-6 and Common Room 368 Macq","contract-site-desc[3]":"uarie St Dubbo","territory":"","contract-rep-code":"","contract-branch-code":"TFSS","contract-start-date":"2010-11-01","contract-tfr-flag":"PM","contract-period":"52","contract-no":"45-049","contract-end-date":"2015-02-28","contract-review-date":"","na-name":"","contract-status":"Active","contract-on-hold-reason-code":"","na-street":"","na-suburb":"","na-postcode":"","na-country":"","contract-charge-flag[2]":"Advance","contract-reference":"B05507","contract-billing-cycle":"N","contract-invoice-number":"0","contract-invoiced-from-date":"2010-11-01","contract-invoiced-to-date":"2010-11-01","contract-misc-field":"GE01","contract-charge-flag[4]":"N","contract-base-anniversary-date":"","xca-code":"","xca-description":"","attributeCLAS":"test","attributeHBLD":"test2","attributeHCMP":"test3","attributeHLSE":"test4","attributeHLTP":"Test5","attributeUNIT":"test6"}';        
        event2.Contact__c = '"contactDetails":null';
        test.startTest();
        Database.SaveResult sr2 = EventBus.publish(event2);                                        
        Test.stopTest();
        
        System.debug('Ananti Test UpsertSite1Test Finished');

       // Order order2 = [SELECT Pronto_Status__c From Order WHERE Name='Test12345' LIMIT 1];
        //System.assertEquals('01',order2.Pronto_Status__c);
    }
    
    @isTest
    static void UpsertSite2Test() {
       
        System.debug('Ananti Test UpsertSite2Test Start');
        
        Action_Account_To_BSA__e event3 = new Action_Account_To_BSA__e();
        event3.Action__c = 'CreateSite';
        event3.AccountCode__c ='NSWTF001';
        event3.ContractNumber__c ='45-049';
        event3.EventTimeStamp__c ='2020-09-16T05:09:58Z';
        event3.CorrelationId__c ='ID000000000025';
        //event3.Account__c = '"accountDetails":{"accountcode":"NSWTF001","attributePCBK":"APS Housing NSW","contract-site-desc[1]":"B05507 - Block A - Unit 1-2 an","contract-site-desc[2]":"d 5-6 and Common Room 368 Macq","contract-site-desc[3]":"uarie St Dubbo","territory":"","contract-rep-code":"","contract-branch-code":"TFSS","contract-start-date":"2010-11-01","contract-tfr-flag":"PM","contract-period":"52","contract-no":"45-049","contract-end-date":"2015-02-28","contract-review-date":"","na-name":"","contract-status":"Active","contract-on-hold-reason-code":"","contract-contact-phone":"","na-street":"","na-suburb":"","na-postcode":"","na-country":"","contract-charge-flag[2]":"Advance","contract-reference":"B05507","contract-billing-cycle":"N","contract-invoice-number":"0","contract-invoiced-from-date":"2010-11-01","contract-invoiced-to-date":"2010-11-01","contract-misc-field":"GE01","contract-charge-flag[4]":"N","contract-base-anniversary-date":"","xca-code":"","xca-description":"","attributeCLAS":"test","attributeHBLD":"test2","attributeHCMP":"test3","attributeHLSE":"test4","attributeHLTP":"Test5","attributeUNIT":"test6"}';        
        event3.Account__c = '"accountDetails":{"accountcode":"NSWTF001","contract-site-desc[1]":"B05507 - Block A - Unit 1-2 an","contract-site-desc[2]":"d 5-6 and Common Room 368 Macq","contract-site-desc[3]":"uarie St Dubbo","territory":"","contract-rep-code":"","contract-branch-code":"TFSS","contract-start-date":"2010-11-01","contract-tfr-flag":"PM","contract-period":"52","contract-no":"45-049","contract-end-date":"2015-02-28","contract-review-date":"","na-name":"","contract-status":"Active","contract-on-hold-reason-code":"","contract-contact-phone":"","na-street":"","na-suburb":"","na-postcode":"","na-country":"","contract-charge-flag[2]":"Advance","contract-reference":"B05507","contract-billing-cycle":"N","contract-invoice-number":"0","contract-invoiced-from-date":"2010-11-01","contract-invoiced-to-date":"2010-11-01","contract-misc-field":"GE01","contract-charge-flag[4]":"N","contract-base-anniversary-date":"","xca-code":"","xca-description":"","attributeCLAS":"test","attributeHBLD":"test2","attributeHCMP":"test3","attributeHLSE":"test4","attributeHLTP":"Test5","attributeUNIT":"test7_1"}';        
        event3.Contact__c = '"contactDetails":null'; 
        
        test.startTest();                  
        Database.SaveResult sr3 = EventBus.publish(event3);        
        Test.stopTest();
        
        System.debug('Ananti Test UpsertSite2Test Finished');

       // Order order2 = [SELECT Pronto_Status__c From Order WHERE Name='Test12345' LIMIT 1];
        //System.assertEquals('01',order2.Pronto_Status__c);
    }
    */
}