global class APS_WorkOrderFeeder implements Iterator<WorkOrder>, Iterable<WorkOrder> {
    List<WorkOrder> workOrders;
    
    global Iterator<WorkOrder> iterator() {
        return this;
    }
    
    global APS_WorkOrderFeeder(List<WorkOrder> workOrders) {
        this.workOrders = workOrders;
    }
    
    global WorkOrder next() {
        return workOrders.remove(0);
    }
    
    global boolean hasNext() {
        return workOrders != null && !workOrders.isEmpty();
    }
}