/**
 * @author          David Azzi (IBM)
 * @date            18/Aug/2020
 * @description     Batch processing to remove all Inactive or Decommissioned Assets from WO / WOLI
 */

global class DeleteWoliAssetsBatch implements Database.Batchable<sObject> {

    global String query;
    // global List<WorkOrderLineItem> lstWOLIs = new List<WorkOrderLineItem>();   

    public DeleteWoliAssetsBatch() {

        query = 'SELECT Id, WorkOrderId, SuggestedMaintenanceDate, AssetId, WorkOrder.SuggestedMaintenanceDate, WorkOrder.StartDate, WorkOrder.Status, WorkOrder.Maintenance_Date__c, WorkOrder.AssetId, Asset.Status, Asset.Reactivate_Date__c, Asset.Parent.RecordTypeId '
                + 'FROM WorkOrderLineItem '
                + 'WHERE (Asset.Status = \'Inactive\' OR Asset.Status = \'Decommissioned\') '
                + 'AND (WorkOrder.Status = \'New\' OR WorkOrder.Status = \'In Progress\') '
                + 'AND RecordType.DeveloperName = \'Asset_Action\' '
                + 'ORDER BY WorkOrderId';
    }

    global Database.QueryLocator start (Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    

    global void execute(Database.BatchableContext BC,  List<sObject> records){
        System.debug('*- assetHelper Executed: ');
        DeleteWoliAssetBatchHelper.updateAssetStatus((List<WorkOrderLineItem>)records);
    }

    global void finish(Database.BatchableContext BC) {
        
    }
}