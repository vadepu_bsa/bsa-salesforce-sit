@isTest
private class APS_AbortTaskCreationScheduledJobsTest {
    @IsTest
    static void schedulerTest() {
        // running a job 30 seconds from now only once
        Datetime schDateTime = Datetime.now().addSeconds(30);
        String hour = String.valueOf(schDateTime.hour());
        String min = String.valueOf(schDateTime.minute()); 
        String ss = String.valueOf(schDateTime.second());

        //parse to cron expression
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';

        APS_AbortTaskCreationScheduledJobs s = new APS_AbortTaskCreationScheduledJobs(); 
        Test.startTest();
        String sch = System.schedule('Job Started At ' + String.valueOf(Math.random() * 10000) + '-' + UserInfo.getUserId(), nextFireTime, s);
        Test.stopTest();
        System.assert(String.isNotBlank(sch));
    }

}