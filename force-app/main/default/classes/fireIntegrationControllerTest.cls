/**
* @author          DIldar Hussain
* @date            10/Dec/2019 
* @description     
*
**/
@isTest
public class fireIntegrationControllerTest {
    
     @testSetup static void setup() {
         List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = true;
        lstWorkType[1].CUI__c = false;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;
        Test.StartTest();

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        //lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[0].Access_Attempts__c = 2;
        lstAccount[2].Access_Attempts__c = 2;
        lstAccount[3].Access_Attempts__c = 2;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;

        Id contactClientRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactClientRTypeId, 1);
        insert lstContact;

        Id contactSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Site');
        List<Contact> lstContact2 = APS_TestDataFactory.createContact(lstAccount[2].Id, contactSiteRTypeId, 1);
        lstContact2[0].Service_Report_Recipient__c = true;
        insert lstContact2;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 5);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 2);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;

        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill Limit 10];
        system.debug('!!!!! skillList = ' + skillList);

        Id APSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'APS_Work_Order');
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 1);
        lstWorkOrder[0].RecordTypeId = APSrecordType;
        lstWorkOrder[0].WorkTypeId = lstWorkType[1].Id;
        lstWorkOrder[0].Skill_Group__c = skillList[0].DeveloperName;
        insert lstWorkOrder;
         
        Product2 product = new Product2();
        product.ProductCode = 'F1/1.3.4.1';
        product.Name ='F1/1.3.4.1 /product';
        product.Family = 'None';
        insert product;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PriceBook2 stdPriceBook = new PriceBook2();
        stdPriceBook.Id = pricebookId;
        stdPriceBook.IsActive =true;
        update stdPriceBook;
       
           ///-price Book Entry
        List<PricebookEntry> standardpriceList = new List<PriceBookEntry>();
        PricebookEntry priceBookEntry = new PricebookEntry();
        pricebookEntry.Pricebook2Id = pricebookId;
        pricebookEntry.Product2Id = product.id;
        pricebookEntry.UnitPrice = 0.0;
        pricebookEntry.IsActive = true;
        pricebookEntry.UseStandardPrice = false;
        standardpriceList.add(pricebookEntry);
         
        insert standardPriceList;
         
        List<PricebookEntry> custompriceList = new List<PriceBookEntry>();
        PricebookEntry priceBookEntry4 = new PricebookEntry();
        priceBookEntry4.Pricebook2Id =  lstPriceBook[0].Id;
        priceBookEntry4.Product2Id = product.id;
        priceBookEntry4.UnitPrice = 0.0;
        priceBookEntry4.IsActive = true;
        priceBookEntry4.UseStandardPrice = false;
        custompriceList.add(pricebookEntry4);
        
        PriceBook2  PurchasingPriceBook = [SELECT Id fROM PriceBook2 WHERE Name = 'Purchasing' LIMIT 1];
        
        PricebookEntry priceBookEntry5 = new PricebookEntry();
        priceBookEntry5.Pricebook2Id =  PurchasingPriceBook.Id;
        priceBookEntry5.Product2Id = product.id;
        priceBookEntry5.UnitPrice = 0.0;
        priceBookEntry5.IsActive = true;
        priceBookEntry5.UseStandardPrice = false;
        custompriceList.add(pricebookEntry5);
        
        insert custompriceList;
        //Price_Book_Dependent_Code__mdt customMetaData = new Price_Book_Dependent_Code__mdt();
        //add product Consumed
        ProductConsumed pcRecord = new ProductConsumed();
        pcRecord.PricebookEntryId = pricebookEntry4.Id;
        pcRecord.QuantityConsumed = 1;
        pcRecord.WorkOrderId =  lstWorkOrder[0].Id;
        insert pcRecord;
        
        Order o = new Order();
        o.AccountId = lstAccount[2].Id;
        o.Work_Order__c = lstWorkOrder[0].Id;
        o.Status = 'draft';
        o.Name='Test12345';
        o.OrderReferenceNumber = '804405';
        o.Pronto_Status__c = '00';
        o.RecordTypeId = TestRecordCreator.getRecordTypeByDeveloperName('Order', 'Purchase_Order');
        o.EffectiveDate = Date.today();   
        o.Pricebook2Id = PurchasingPriceBook.Id;
        insert o;
         
        OrderItem oi = new OrderItem();
        oi.OrderId = o.Id;
        oi.PriceBookEntryId = pricebookEntry5.Id;
        oi.UnitPrice = 0.0;
        //insert oi;
 
        
    }
    
        @IsTest
    static void fireIntegrationControllerTest() {
        
       List<WorkOrder> WorkOrderLst = [SELECT Id FROM WorkOrder];
       List<ProductConsumed> ProductConsumedLst = [SELECT Id FROM ProductConsumed];
       List<Product2> ProductLst = [SELECT Id FROM Product2];
       List<Order> OrderLst = [SELECT Id FROM Order]; 
       
        TEst.StartTest();
        
       fireIntegrationController.execute(WorkOrderLst[0].Id, 'WorkOrder');
       fireIntegrationController.execute(ProductConsumedLst[0].Id, 'ProductConsumed');
       fireIntegrationController.execute(ProductLst[0].Id, 'Product2');
       fireIntegrationController.execute(OrderLst[0].Id, 'Order');
       TEst.StopTest();
    }
    
}