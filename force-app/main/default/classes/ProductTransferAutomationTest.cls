@isTest
public class ProductTransferAutomationTest {
    static testMethod void testProductTransfer() {
        
        // Create a test platform event instance
        Pronto_Van_Stock_Transfer_Update__e syncEvent = new Pronto_Van_Stock_Transfer_Update__e();
        syncEvent.Van_ID_Source__c = 'van2909';
        syncEvent.CorrelationID__c = '88a57e4e-b22b-2909';
        syncEvent.Platform_Event_Name__c = 'Pronto_Van_Stock_Transfer_Update';
        syncEvent.Serial_Number__c = 'sl2909';
        syncEvent.Van_ID_Destination__c = 'van2812';        
        syncEvent.Source_Product_Code__c = 'prod2909';
        syncEvent.Product_Transfer_Number__c = 'pt1234';
        syncEvent.Quantity_Received__c = 2;
        syncEvent.Is_Received__c = 'Yes';

        Test.startTest();

            // Publish test event
            Database.SaveResult sr = EventBus.publish(syncEvent);
            
        Test.stopTest();
                
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());

    }
}