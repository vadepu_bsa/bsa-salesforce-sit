/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-07-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-05-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = false)
public class ActionInvoiceToBSAHandlerTest {
    
    @testSetup static void setup() {
        Date todayDate = Date.today();
        OperatingHours ohObj = new OperatingHours();
        ohObj.Name = 'Testing';
        ohObj.TimeZone = 'Australia/Sydney';
        insert ohObj;
       
        ServiceTerritory stObj = new ServiceTerritory();
        stObj.Name = 'Testting';
        stObj.Pronto_ID__c = 'TFSS';
        stObj.IsActive = True;
        stObj.Description = 'Testing';
        stObj.APS__c = true;
        stObj.OperatingHoursId = ohObj.Id;
        insert stObj;
       
       Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site').getRecordTypeId();
       Account acc = new Account();
       acc.Name ='Anderson Stuart Building';
       acc.RecordTypeId = accRecordTypeId ;
       acc.Customer_Reference__c = 'F13';
       acc.Status__c = 'Active';
       insert acc;
       
       WorkType wtObj = new WorkType();
       wtObj.Name ='Do  & Charge';
       wtObj.APS__c = true;
       wtObj.Description = 'testing';
       wtObj.EstimatedDuration = 4;    
       insert wtObj;
       
       Asset assetObj = New Asset();
      	assetObj.Name = 'teting';
       assetobj.Status = 'Active';
       assetobj.accountId = acc.Id;
       insert assetobj;
       
       Id APSRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('APS_Work_Order').getRecordTypeId();
       WorkOrder wObj = new WorkOrder();
       wObj.Client_Work_Order_Number__c = '00044087';
    
       wObj.AccountId = acc.Id;
       wObj.WorkTypeId = wtObj.id;
       Wobj.Description = 'Testing';
       wObj.RecordTypeId = APSRecordTypeId;
       wObj.AssetId = assetObj.Id;
       insert Wobj;

        //insert Invoice records
        Invoice__c invoice1= new Invoice__c();
        invoice1.Work_Order__c = wObj.Id;
        invoice1.WorkType__c = wtObj.Id;
        invoice1.Account__c = acc.Id;
        invoice1.Invoice_Date__c = todaydate.addDays(1);
        invoice1.Invoice_Number__c = '908401';        
        insert invoice1;
        
        //insert Client Invoice
        /*
        Client_Invoice__c clientInvoice1 = new Client_Invoice__c();
        clientInvoice1.Client__c = acc.Id;
        clientInvoice1.WorkType__c = wtObj.Id;
        clientInvoice1.StartDate__c = todaydate;
        clientInvoice1.EndDate__c = todaydate.addDays(10);
        insert clientInvoice1;
        */

    }

    @isTest
    static void updateInvoiceStatus_Test() {
              
        //list<WorkOrder> workorderList = [SELECT id, WorkorderNumber FROM WorkOrder LIMIT 1];
        List<Invoice__c> invoiceList = [SELECT id, Invoice_Number__c, Status__c FROM Invoice__c LIMIT 1];
        system.debug('Ananti_Test_BeforeUpdate: ' + invoiceList);
        Action_Invoice_To_BSA__e event = new Action_Invoice_To_BSA__e();
       	event.Action__c='InvoiceStatus';		
		event.EventTimeStamp__c='2021-07-05T10:10:40Z';
		event.CorrelationId__c='ID000000000027';
        
        if(invoiceList != null && invoiceList.size() > 0)
        {
           event.InvoiceStatus__c  = '"invoiceStatusDetails":{"invoiceId":"'+invoiceList[0].Id+'","invoiceStatus":"NAPR","invoiceStatusDesc":"Invoice not received by client"}';
        }
        
        Test.startTest();
        // Publish test event
        Database.SaveResult sr = EventBus.publish(event);
      
        Test.stopTest();

        List<Invoice__c> invoiceList2 = [SELECT id, Invoice_Number__c, Status__c FROM Invoice__c LIMIT 1];
        system.debug('Ananti_Test_AfterUpdate: ' + invoiceList2);
    }

}