/**
* @File Name          : BSA_ConstantsUtility.cls
* @Description        : 
* @Author             : Karan Shekhar
* @Group              : 
* @Last Modified By   : Karan Shekhar
* @Last Modified On   : 03/06/2020, 10:36:50 am
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    04/05/2020   Karan Shekhar     Initial Version
**/
public class BSA_ConstantsUtility {
    
    public static final String WO_PARENT = 'Parent CUI Work Order';
    public static final String WO_CHILD = 'Child CUI Work Order';
    public static final String WOLI_CUI = 'CUI';
    public static final String FTTB = 'FTTB';
    public static final String FTTC = 'FTTC';
    public static final String FTTP = 'FTTP';
    public static final String CSLL = 'CSLL';
    public static final String NHUR = 'NHUR';
    public static final String HFC_TECH = 'HFC';
    public static final String FIBRE_TECH = 'Fibre';
    public static final String FTTN_TECH = 'Fibre To The Node';
    public static final String FTTN_TECHONE = 'FTTN';
    public static final String FTTN_CHILD = 'FTTN Child';
    public static final String HFC_CHILD = 'HFC Child';
    public static final String SUBMIT_NOTES = 'Submit Notes';
    public static final String LOG_ENTRY = 'Log Entry';
    public static final String SOURCE_API = 'API';  
    public static final String SUCCESS_EVENT_NAME = 'SuccessEvent';
    public static final String FAILURE_EVENT_NAME = 'FailEvent';
    public static final String ACTION_B2B_SETUP_WORK_ORDER_EVENT = 'SetupWorkOrder';
    public static final String ACTION_B2B_FROM_BSA_EVENT = 'Action_B2B_From_BSA__e';
    public static final String PRODUCT_GROUP_PART = 'Part';
    public static final String WORKAREA_URBAN = 'Urban';
    public static final String NBN_NOTE_SOURCE_UPDATE_FOR_NBN = 'LATEST UPDATE FOR NBN';
    public  static final Set<String> ALLOWED_STATUS_SET = new Set<String>{'Canceled','Cannot Complete'};
        public  static final Set<String> NOT_ALLOWED_STATUS_SET = new Set<String>{'In Progress','Completed','Cannot Complete'};
    public  static final Set<String> SA_STATUS_DIFFERS_FROM_WO = new Set<String>{'Enroute','En-Route'};
        public static final String SA_ENROUTE_STATUS ='En-Route';
    public static final String SA_INCOMPLETE_STATUS_APINAME ='Cannot Complete';
    public static final String SA_INCOMPLETE_STATUS ='Incomplete';  
                    
            
            //Platform Event Names 
            public static final String PE_VAN_STOCK_CONSUMPTION = 'SF.FSL.VAN.STOCK.CONSUMPTION';
    
    
    //Generic PE Constants
    public static final String ACTION_SETUPWORKORDER = 'SetupWorkOrder';
    
    //Financial Report
    public static final Set<String> ACTUAL_REVENUE_SUB_STATUS = new Set<String>{'Sent To Client','Pending Approval','Approved'};
        public static final Set<String> EST_ACCRUED_REVENUE_SUB_STATUS_NOT = new Set<String>{'Invoiced','Sent To Client','Pending Approval','Approved'};
            
            //B2B Platform Event Custom Metadata constants
            public static final String FLOW_IMPL_SETTING_TYPE_NAME = 'ImplementationFlow';
    public static final String CLASS_IMPL_SETTING_TYPE_NAME = 'ImplementationClass';
    public static final String MAPPING_SETTING_TYPE_NAME = 'Mapping';
    
    
    
    //B2B NBN Services Constants  for WO Actions from BSA
    public static final String ACTION_ACCEPT = 'accept';
    public static final String ACTION_ENROUTE = 'enroute';
    public static final String ACTION_ONSITE = 'onsite';
    public static final String ACTION_SUSPEND = 'suspend';
    public static final String ACTION_DOC_COMPLETE ='documentation_complete';
    public static final String ACTION_UPDATE_SCHEDULE = 'update_schedule';
    public static final String ACTION_UPDATE_PRD = 'update_prd';
    public static final String ACTION_ADD_CONTACT = 'add_contact';
    public static final String ACTION_UPDATE_CONTACT = 'update_contact';
    public static final String ACTION_ADD_NOTES = 'add_notes';
    public static final String ACTION_UPDATE_NOTES = 'update_notes';
    public static final String ACTION_ADD_TASK = 'add_task';
    public static final String ACTION_UPDATE_TASK = 'update_task';
    public static final String ACTION_UPDATE_CONFIDENCE_LEVEL = 'update_confidence_level';
    public static final String ACTION_COMPLETE = 'complete';
    public static final String ACTION_INCOMPLETE = 'incomplete';
    
    //B2B NBN Services Constants for WO specification
    public static final String GENERAL_WOS = 'GeneralWOS'; 
    public static final Set<String> SA_STATUS_SET = new Set<String>{'Enroute','Onsite','Cancelled','Complete WO','Incomplete', 'Canceled', 'Complete', 'Completed','Suspend'};
        
	//media api constants
	public static final Map<String,String> FILE_TYPE_MIME_MAP = new Map<String,String>{'jpg'=>'image','jpeg'=>'image','png'=>'image','gif'=>'image','bmp'=>'image','JPG'=>'image','JPEG'=>'image','PNG'=>'image','GIF'=>'image','BMP'=>'image','pdf'=>'application','PDF'=>'application'};
}