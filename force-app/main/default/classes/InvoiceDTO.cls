/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-07-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-02-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class InvoiceDTO implements IDTO{    
    public InvoiceStatusDetails invoiceStatusDetails {get;set;} 
    public InvoiceDTO(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'invoiceStatusDetails') {
						invoiceStatusDetails = new InvoiceStatusDetails(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'InvoiceDTO consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
    }

    public InvoiceDTO(){}

    public  InvoiceDTO parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new InvoiceDTO(parser);
	}

    public String getSpecificDTOClassName(){

        return 'InvoiceDTO';
    }

    public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
    

    public class InvoiceStatusDetails{
        public String invoiceId {get;set;}
        public String invoiceStatusDesc {get;set;}

        public InvoiceStatusDetails(JSONParser parser){            
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'invoiceId') {
							invoiceId = parser.getText();
						} else if (text == 'invoiceStatusDesc') {
							invoiceStatusDesc = parser.getText();
						}else {
							System.debug(LoggingLevel.WARN, 'InvoiceDetails consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}				 			         
			}			
        }
    }
}