public class BSA_ShiftTriggerHandler extends ASPR_AbstractITrigger {
    
    public void bulkBefore(List<sObject> so) { 
        if(Trigger.IsInsert) {
            Set<Id> sTerrIdSet = new Set<Id>();
            Set<Id> sResourceIdSet = new Set<Id>();
            List<ServiceTerritoryMember> sTerrMemberList = new List<ServiceTerritoryMember>();
            Map<Id, Datetime> shiftIdToTZStartDateMap = new Map<Id, Datetime>();
            Map<Id, Datetime> shiftIdToTZEndDateMap = new Map<Id, Datetime>();
            Map<Id, String> shiftIdToTZIdMap = new Map<Id, String>();
            Map<Id, Map<DateTime, ServiceTerritoryMember>> resIdToSTMMap;
            
            //fill sTerrIdSet sResourceIdSet
            fillSetCollection(so, sTerrIdSet, sResourceIdSet);
            
            Map<Id, ServiceTerritory> sTerrMap = fillSTerrMap(sTerrIdSet);
            //fill shiftIdToTZStartDateMap and shiftIdToTZEndDateMap
            convertDateTime(so, sTerrMap, shiftIdToTZStartDateMap, shiftIdToTZEndDateMap, shiftIdToTZIdMap);
            
            //existSTerrMemberList for update Operation Hours field
            List<ServiceTerritoryMember> existSTerrMemberList = existSTerrMemberList(sTerrIdSet, sResourceIdSet); 
            resIdToSTMMap = fillResIdToSTMMap(so, existSTerrMemberList, shiftIdToTZEndDateMap, shiftIdToTZIdMap);
            
            for(Shift sh_i: (List<Shift>) so) {
                if(!isRelevantShift(sh_i)){
                    continue;
                }
                ServiceTerritoryMember sTerMember = findSTM(sh_i, resIdToSTMMap, shiftIdToTZIdMap.get(sh_i.Id));
                if(sTerMember == null) {
                    
                    //create Service Territory Members 
                    sTerMember = createSTerritoryMember(sh_i, shiftIdToTZIdMap.get(sh_i.Id));
                    sTerrMemberList.add(sTerMember);
                    
                    if(!resIdToSTMMap.containsKey(sh_i.ServiceResourceId)) {
                        resIdToSTMMap.put(sh_i.ServiceResourceId, new Map<DateTime, ServiceTerritoryMember>());
                    } 
                    DateTime firstWeekDay = firstWeekDay(sh_i.StartTime, shiftIdToTZIdMap.get(sh_i.Id));
                    Date fWeekDay = firstWeekDay.Date();
                    
                    resIdToSTMMap.get(sh_i.ServiceResourceId).put(firstWeekDay, sTerMember);
                } 
            }
            if(!sTerrMemberList.isEmpty()) {
                insert sTerrMemberList;
            }
        }
    }
    
    public void bulkAfter(List<sObject> so) {
        if(Trigger.IsInsert) {
            Set<Id> sTerrIdSet = new Set<Id>();
            Set<Id> sResourceIdSet = new Set<Id>();
            List<TimeSlot> timeSlotList = new List<TimeSlot>();
            Map<Id, ServiceTerritoryMember> sTerrMemberToUpdateMap = new Map<Id, ServiceTerritoryMember>();
            Map<Id, Datetime> shiftIdToTZStartDateMap = new Map<Id, Datetime>();
            Map<Id, Datetime> shiftIdToTZEndDateMap = new Map<Id, Datetime>();
            Map<Id, OperatingHours> stmIdToOpHoursMap = new Map<Id, OperatingHours>();
            Map<Id, Id> shiftIdToSTMIdMap = new Map<Id, Id>();
            Map<Id, String> shiftIdToTZIdMap = new Map<Id, String>();
            Map<Id, Map<DateTime, ServiceTerritoryMember>> resIdToSTMMap;
            
            //fill sTerrIdSet sResourceIdSet
            fillSetCollection(so, sTerrIdSet, sResourceIdSet);
            
            //fill maps
            Map<Id, ServiceTerritory> sTerrMap = fillSTerrMap(sTerrIdSet);
            Map<Id, ServiceResource> sResourceMap = fillSResourceMap(sResourceIdSet); 
            
            //fill shiftIdToTZStartDateMap and shiftIdToTZEndDateMap
            convertDateTime(so, sTerrMap, shiftIdToTZStartDateMap, shiftIdToTZEndDateMap, shiftIdToTZIdMap);
            
            //existSTerrMemberList for update Operation Hours field
            List<ServiceTerritoryMember> existSTerrMemberList = existSTerrMemberList(sTerrIdSet, sResourceIdSet); 
            resIdToSTMMap = fillResIdToSTMMap(so, existSTerrMemberList, shiftIdToTZEndDateMap, shiftIdToTZIdMap);
            
            //create operation Hours
            List<OperatingHours> newOperationHoursList = new  List<OperatingHours>();
            for(Shift sh_i: (List<Shift>) so) {
                if(!isRelevantShift(sh_i)){
                    continue;
                } 
                
                ServiceTerritoryMember sTerrMember = findSTM(sh_i, resIdToSTMMap, shiftIdToTZIdMap.get(sh_i.Id));
                shiftIdToSTMIdMap.put(sh_i.Id, sTerrMember.Id);
                if(String.isEmpty(sTerrMember.OperatingHoursId) && !stmIdToOpHoursMap.containsKey(sTerrMember.Id)) {
                    OperatingHours opHours = createOperationHours(sh_i, sTerrMap, sResourceMap, shiftIdToTZIdMap.get(sh_i.Id));                 
                    stmIdToOpHoursMap.put(sTerrMember.Id, opHours);
                    newOperationHoursList.add(opHours);
                }else{
                    if(sTerrMember.OperatingHoursId != null){
                        stmIdToOpHoursMap.put(sTerrMember.Id, new OperatingHours(Id = sTerrMember.OperatingHoursId));
                    }
                }
            }
            
            if(!newOperationHoursList.isEmpty()) {
                insert newOperationHoursList;  
            }
            
            for(Shift sh_i: (List<Shift>) so) {
                if(!isRelevantShift(sh_i)){
                    continue;
                }
                //create Time Slots
                timeSlotList.addAll(shiftTimeSlotList(sh_i, shiftIdToTZStartDateMap, shiftIdToTZEndDateMap, stmIdToOpHoursMap, shiftIdToSTMIdMap, shiftIdToTZIdMap));
                ServiceTerritoryMember sTerrMember = findSTM(sh_i, resIdToSTMMap, shiftIdToTZIdMap.get(sh_i.Id));
                
                if(!sTerrMemberToUpdateMap.containsKey(sTerrMember.id)) {
                    sTerrMember.OperatingHoursId = stmIdToOpHoursMap.get(shiftIdToSTMIdMap.get(sh_i.Id)).Id;
                    sTerrMemberToUpdateMap.put(sTerrMember.Id, sTerrMember);
                }
            } 
            if(!timeSlotList.isEmpty()) {
                //insert timeSlotList;
                Database.insert(timeSlotList, false);
            }
            if(!sTerrMemberToUpdateMap.isEmpty()) {
                update sTerrMemberToUpdateMap.values();
            }
        }
        if(Trigger.IsUpdate) {
            BSA_ShiftTriggerHelper.updateRelatedTimeSlots(Trigger.oldMap, Trigger.new);
        }
        
    }
    
    private List<TimeSlot> shiftTimeSlotList(Shift sh, Map<Id, Datetime> shiftIdToTZStartDateMap, Map<Id, Datetime> shiftIdToTZEndDateMap, Map<Id, OperatingHours> stmIdToOpHoursMap, Map<Id, Id> shiftIdToSTMIdMap, Map<Id, String> shiftIdToTZIdMap) {
        List<TimeSlot> shiftTimeSlotList = new List<TimeSlot>();
        DateTime currDateTime = shiftIdToTZStartDateMap.get(sh.Id);
        DateTime endDateTime = shiftIdToTZEndDateMap.get(sh.Id);
        Integer dayNumb = currDateTime.Date().daysBetween(endDateTime.Date());
        Id opHoursId = stmIdToOpHoursMap.get(shiftIdToSTMIdMap.get(sh.Id)).Id; 
        String tzOpHours = shiftIdToTZIdMap.get(sh.Id);
        
        Timezone userTz = UserInfo.getTimeZone();
        Timezone tz = Timezone.getTimeZone(tzOpHours);            
        Integer startDateOffset = tz.getOffset(sh.StartTime);
        Integer userStartDateOffset = userTz.getOffset(sh.StartTime);
        for(Integer i=0; i<=dayNumb; i++) {
            TimeSlot ts = new TimeSlot();
            ts.DayOfWeek = currDateTime.addDays(i).format('EEEE');
            ts.Type = 'Normal';
            Datetime dt = shiftIdToTZStartDateMap.get(sh.Id);
            ts.StartTime = shiftIdToTZStartDateMap.get(sh.Id).Time().addSeconds(+(userStartDateOffset-startDateOffset) / 1000);
            ts.EndTime = shiftIdToTZEndDateMap.get(sh.Id).Time().addSeconds(+(userStartDateOffset-startDateOffset) / 1000);

            ts.OperatingHoursId = opHoursId;
            
            shiftTimeSlotList.add(ts);
        }
        return shiftTimeSlotList;
    }
    
    private void convertDateTime(List<sObject> so, Map<Id, ServiceTerritory> sTerrMap, Map<Id, Datetime> shiftIdToTZStartDateMap, Map<Id, Datetime> shiftIdToTZEndDateMap, Map<Id, Id> shiftIdToTZIdMap) {
        for (Shift sh_i: (List<Shift>) so) {
            if (!isRelevantShift(sh_i)) {
                continue;
            }
            String tzOpHours = sTerrMap.get(sh_i.ServiceTerritoryId).OperatingHours.TimeZone; 
            
            Timezone userTz = UserInfo.getTimeZone();
            Timezone tz = Timezone.getTimeZone(tzOpHours);            
            Integer startDateOffset = tz.getOffset(sh_i.StartTime);
            Integer userStartDateOffset = userTz.getOffset(sh_i.StartTime);
            shiftIdToTZStartDateMap.put(sh_i.Id, sh_i.StartTime.addSeconds(-(userStartDateOffset-startDateOffset) / 1000));
            shiftIdToTZEndDateMap.put(sh_i.Id, sh_i.EndTime.addSeconds(-(userStartDateOffset-startDateOffset) / 1000));
            shiftIdToTZIdMap.put(sh_i.Id, tzOpHours);
            System.debug('shiftIdToTZStartDateMap'+shiftIdToTZStartDateMap);
            System.debug('shiftIdToTZEndDateMap'+shiftIdToTZEndDateMap);
            System.debug('shiftIdToTZIdMap'+shiftIdToTZIdMap);
        }
    }
    
    private String dateToString(Date currDate) {
        String strDate = String.valueOf(currDate.year());
        strDate += '-' + String.valueOf(currDate.month());
        strDate += '-' + String.valueOf(currDate.day());
        return strDate;
    }
    
    private OperatingHours createOperationHours(Shift sh, Map<Id, ServiceTerritory> sTerrMap, Map<Id, ServiceResource> sResourceMap, String tzId) {
        String nameOpHours = sResourceMap.get(sh.ServiceResourceId).Name;
        DateTime firstWeekDay = firstWeekDay(sh.StartTime, tzId);
        
        nameOpHours = nameOpHours.replaceAll(' ', '_');
        
        nameOpHours += '_' + firstWeekDay.format('yyyy-MM-dd', tzId);
        nameOpHours += '_' + firstWeekDay.addDays(6).format('yyyy-MM-dd', tzId);
        
        OperatingHours opHours = new OperatingHours();
        opHours.Name = nameOpHours;
        opHours.TimeZone = sTerrMap.get(sh.ServiceTerritoryId).OperatingHours.TimeZone;
        
        return opHours;
    }
    
    private ServiceTerritoryMember findSTM(Shift sh, Map<Id, Map<DateTime, ServiceTerritoryMember>> resIdToSTMMap, String tzId) {
        DateTime firstWeekDay = firstWeekDay(sh.StartTime, tzId);        
        System.debug('firstWeekDay ' + firstWeekDay);
        if(resIdToSTMMap.containsKey(sh.ServiceResourceId) 
           && resIdToSTMMap.get(sh.ServiceResourceId).containsKey(firstWeekDay)) {
               return resIdToSTMMap.get(sh.ServiceResourceId).get(firstWeekDay);
           }
        return null;
    }
    
    private Map<Id, Map<DateTime, ServiceTerritoryMember>> fillResIdToSTMMap(List<sObject> so, List<ServiceTerritoryMember> existSTerrMemberList, Map<Id, Datetime> shiftIdToTZEndDateMap, Map<Id, String> shiftIdToTZIdMap) {
        Map<Id, Map<DateTime, ServiceTerritoryMember>> resIdToSTMMap = new Map<Id, Map<DateTime, ServiceTerritoryMember>>();
        Map<Id, List<ServiceTerritoryMember>> resIdToExSTMMap = new Map<Id, List<ServiceTerritoryMember>>();
        
        for(ServiceTerritoryMember stm_i: existSTerrMemberList) {
            if(!resIdToExSTMMap.containsKey(stm_i.ServiceResourceId)) {
                resIdToExSTMMap.put(stm_i.ServiceResourceId, new List<ServiceTerritoryMember>());
            }
            resIdToExSTMMap.get(stm_i.ServiceResourceId).add(stm_i);
        }
        
        for(Shift sh_i: (List<Shift>) so) {
            
            if(resIdToExSTMMap.containsKey(sh_i.ServiceResourceId)) {
                DateTime firstWeekDay = firstWeekDay(sh_i.StartTime, shiftIdToTZIdMap.get(sh_i.Id));
                DateTime startDT = firstWeekDay;
                DateTime endDT = firstWeekDay(startDT.addDays(7), shiftIdToTZIdMap.get(sh_i.Id)).addMinutes(-1);
                               
                for(ServiceTerritoryMember stm_i: resIdToExSTMMap.get(sh_i.ServiceResourceId)) {
                    
                    if (stm_i.ServiceTerritoryId == sh_i.ServiceTerritoryId 
                        && stm_i.ServiceResourceId == sh_i.ServiceResourceId
                        && stm_i.EffectiveStartDate == startDT 
                        && stm_i.EffectiveEndDate == endDT) { 
                            if(!resIdToSTMMap.containsKey(sh_i.ServiceResourceId)) {
                                resIdToSTMMap.put(sh_i.ServiceResourceId, new Map<DateTime, ServiceTerritoryMember>()) ;
                            } 
                            resIdToSTMMap.get(sh_i.ServiceResourceId).put(startDT, stm_i);
                            
                        }
                }
            }  
        }
        System.debug('resIdToSTMMap'+resIdToSTMMap);    
        return resIdToSTMMap;
    }
    
    private DateTime firstWeekDay(Datetime shStartTime, String tzId) {
        Date firstDayoftheFirstWeek = shStartTime.DateGMT().toStartOfWeek();
        System.debug(Datetime.newInstanceGmt(firstDayoftheFirstWeek, Time.newInstance(0, 0, 0, 0)).formatGmt('E'));
        if (Datetime.newInstanceGmt(firstDayoftheFirstWeek, Time.newInstance(0, 0, 0, 0)).formatGmt('E') == 'Sun')
            firstDayoftheFirstWeek = firstDayoftheFirstWeek.addDays(1); 
        
        DateTime firstWeekDayTime = Datetime.newInstance(firstDayoftheFirstWeek, Time.newInstance(0, 0, 0, 0));
        
        Timezone tz = Timezone.getTimeZone(tzId);
        Timezone userTz = UserInfo.getTimeZone();
        Integer startDateOffset = tz.getOffset(firstWeekDayTime);
        Integer userStartDateOffset = userTz.getOffset(firstWeekDayTime);
        
        firstWeekDayTime = firstWeekDayTime.addSeconds(-(startDateOffset-userStartDateOffset) / 1000);
        
        return firstWeekDayTime;
    }
    
    private ServiceTerritoryMember createSTerritoryMember(Shift sh, String tzId) {
        DateTime firstWeekDay = firstWeekDay(sh.StartTime, tzId);
        
        DateTime startDT = firstWeekDay;
        
        
        DateTime endDT = firstWeekDay(startDT.addDays(7), tzId).addMinutes(-1); 
        
        ServiceTerritoryMember sTerMember = new ServiceTerritoryMember();
        sTerMember.ServiceTerritoryId    = sh.ServiceTerritoryId;
        sTerMember.ServiceResourceId    = sh.ServiceResourceId;
        sTerMember.TerritoryType      = 'P';
        sTerMember.EffectiveStartDate    = startDT;
        sTerMember.EffectiveEndDate    = endDT;
        System.debug('sTerMember '+sTerMember);
        return sTerMember;
    }
    
    private Map<Id, ServiceResource> fillSResourceMap(Set<Id> sResourceIdSet) {
        return new Map<Id, ServiceResource>([SELECT Id, Name
                                             FROM ServiceResource
                                             WHERE Id IN: sResourceIdSet]);
    }
    
    private Map<Id, ServiceTerritory> fillSTerrMap(Set<Id> sTerrIdSet) {
        return new Map<Id, ServiceTerritory>([SELECT Id, Name, OperatingHours.TimeZone
                                              FROM ServiceTerritory
                                              WHERE Id IN: sTerrIdSet]);
    }
    
    private List<ServiceTerritoryMember> existSTerrMemberList(Set<Id> sTerrIdSet, Set<Id> sResourceIdSet) {
        return [SELECT Id, ServiceTerritoryId, ServiceResourceId, EffectiveStartDate, EffectiveEndDate, OperatingHoursId
                FROM ServiceTerritoryMember
                WHERE ServiceTerritoryId IN: sTerrIdSet
                AND ServiceResourceId IN: sResourceIdSet
               ];
    }
    
    private Boolean isRelevantShift(Shift sh) {
        if(String.isBlank(sh.ServiceTerritoryId) || String.isBlank(sh.ServiceResourceId)) {
            return false;
        } else {
            return true;
        } 
    } 
    
    private void fillSetCollection(List<sObject> so, Set<Id> sTerrIdSet, Set<Id> sResourceIdSet) {
        for(Shift sh_i: (List<Shift>) so) {
            if(!isRelevantShift(sh_i)){
                System.debug('Shift ' + sh_i.Id + ' with missing fields');
            } else {
                sTerrIdSet.add(sh_i.ServiceTerritoryId);
                sResourceIdSet.add(sh_i.ServiceResourceId);
            }
        }
    }
    
    // not used
    public void beforeInsert(sObject so) { }
    public void beforeUpdate(sObject oldSo, sObject so) { }
    public void beforeDelete(sObject so) { }
    
    public void afterInsert(sObject so) { }
    public void afterUpdate(sObject oldSo, sObject so) { }
    public void afterDelete(sObject so) { }
    
    public void andFinally() { }
}