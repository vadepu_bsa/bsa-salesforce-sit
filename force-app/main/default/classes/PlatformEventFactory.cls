/**
* @author          Karan Shekhar
* @date            20/Jan/2020	
* @description    
* Change Date    Modified by         Description  
* 20/Jan/2020    Karan Shekhar		Factory for Invoking relevant Platform Event Handlers. Below is PE to handler Mapping
									Pronto_Van_Stock_Consumption_Update__e --> ProntoVanStockConsumptionUpdatePEHandler
**/

public class PlatformEventFactory {
	
    private sObjectType mSObjectName ;
    private List<sObject> mSObjectList ;
    private Boolean mshouldPublishPE;  
    
    //Private constructor to avoid accidental initialization of Factory
    @testvisible
    private PlatformEventFactory(){}
    
    public PlatformEventFactory(sObject sObjectRecord, List<sObject> sObjectList, Boolean shouldPublishPE){
        
        mSObjectName = sObjectRecord.Id.getsobjecttype();
        mSObjectList = sObjectList;
        mshouldPublishPE = shouldPublishPE;
    }
    
    public IPlatformEvent gethandlerName(){ 
        
        if(mSObjectName == ProductConsumed.SObjectType){            
             return new ProntoVanStockConsumptionUpdatePEHandler(mSObjectName,mSObjectList,mshouldPublishPE); 
        }
        return null;
    }
    
}