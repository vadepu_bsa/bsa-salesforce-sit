//SeeAllData is used because of a process builder that assigns a priceBookId to Order with a hardcoded ID
@isTest(SeeAllData = true)
public class ProntoProductConsumedSerializeTest {
    @isTest
    public static void generateProntoProductPlatformEvent() {
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[0].APS__c = true;
        lstWorkType[1].APS__c = True;
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(1);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountClientRTypeId, 2);
        lstAccount[0].Include_New_Assets_in_PM_Routine__c = true;
        lstAccount[1].RecordTypeId = AccountSiteRTypeId;
        lstAccount[1].Include_New_Assets_in_PM_Routine__c = true;
        insert lstAccount;

        Id contactRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactRTypeId, 1);
        insert lstContact;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[1].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[1].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[0].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[1].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 1);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        Id woarRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[1].Id, woarRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 1);
        insert lstWOAR;

        List<Skill> skillList = [SELECT Id,Description,DeveloperName, MasterLabel FROM Skill Limit 10];
        // system.debug('!!!!! skillList = ' + skillList);

        Order o = new Order();
        o.AccountId = lstAccount[0].Id;
        o.Status = 'draft';
        o.Name='Test12345';
        o.OrderReferenceNumber = '804405';
        o.Pronto_Status__c = '00';
        o.RecordTypeId = TestRecordCreator.getRecordTypeByDeveloperName('Order', 'Purchase_Order');
        o.EffectiveDate = Date.today();   
        o.PriceBook2Id  = lstPriceBook[0].Id;
        insert o;
        
        // Insert Product
        Product2 product = new Product2();
        product.ProductCode = 'F1/1.3.4.1';
        product.Name ='F1/1.3.4.1 /product';
        product.Family = 'None';
        insert product;

        Order insertedOrder = [SELECT Pricebook2Id FROM Order WHERE Id = :o.Id LIMIT 1];

        Id pricebookId = Test.getStandardPricebookId();
        // Insert PricebookEntry
        PricebookEntry priceBookEntry = new PricebookEntry();
        pricebookEntry.Pricebook2Id = pricebookId;
        pricebookEntry.Product2Id = product.id;
        pricebookEntry.UnitPrice = 0.0;
        pricebookEntry.IsActive = true;
        insert pricebookEntry;

        // Insert PricebookEntry
        PricebookEntry priceBookEntry2 = new PricebookEntry();
        pricebookEntry2.Pricebook2Id = insertedOrder.Pricebook2Id;
        pricebookEntry2.Product2Id = product.id;
        pricebookEntry2.UnitPrice = 0.0;
        pricebookEntry2.IsActive = true;
        pricebookEntry2.UseStandardPrice = false;
        insert pricebookEntry2;

        Id nonAPSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'Work_Order');
        Id APSrecordType = APS_TestDataFactory.getRecordTypeId('WorkOrder', 'APS_Work_Order');
        List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[1].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, 
                                                                           lstServiceContract[0].Id,  insertedOrder.Pricebook2Id, objCase.Id, lstAssetParents[1].Id,
                                                                           lstMaintenancePlan[0].Id,null, 3);
        lstWorkOrder[0].RecordTypeId = APSrecordType;
        lstWorkOrder[1].RecordTypeId = APSrecordType;
        lstWorkOrder[2].RecordTypeId = APSrecordType;
        lstWorkOrder[0].Pricebook2Id = insertedOrder.Pricebook2Id;
        lstWorkOrder[1].Pricebook2Id = insertedOrder.Pricebook2Id;
        lstWorkOrder[2].Pricebook2Id = insertedOrder.Pricebook2Id;
        lstWorkOrder[0].WorkTypeId = lstWorkType[1].Id;
        lstWorkOrder[0].Skill_Group__c = skillList[0].DeveloperName;
        lstWorkOrder[1].Skill_Group__c = skillList[1].DeveloperName;      
        lstWorkOrder[2].ParentWorkOrderId = lstWorkOrder[1].Id;
        lstWorkOrder[2].Skill_Group__c = skillList[2].DeveloperName;      
        insert lstWorkOrder;

        

        ProductConsumed pc = new ProductConsumed();
        pc.PriceBookEntryId = priceBookEntry2.Id;
        pc.WorkOrderID = lstWorkOrder[0].Id;
        pc.QuantityConsumed = 1;
        system.debug('Mahmood priceBookEntry2 = ' +priceBookEntry2.Pricebook2Id);
        system.debug('Mahmood WO PriceBook = ' +lstWorkOrder[0].Pricebook2Id);
        insert pc;
        
        ProductConsumed p = [SELECT Id FROM ProductConsumed LIMIT 1];
        System.debug('!ProductConsumed' +p);
        Set<Id> idSet = new Set<Id>();
        idSet.add(p.Id);
        Map<Id,String> orderItemOperationMap = new Map<Id,String>();
        Test.StartTest();
        ProntoProductConsumedSerialize.generateProntoProductPlatformEvent(idSet,orderItemOperationMap);
        Test.StopTest();
    }
}