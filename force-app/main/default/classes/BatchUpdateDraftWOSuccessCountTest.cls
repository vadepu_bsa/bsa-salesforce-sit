@isTest
public class BatchUpdateDraftWOSuccessCountTest {
    static testMethod void testDWOBatch(){   
        Test.startTest();
        APS_TestDataFactory.testDataSetup();
        WorkType workTypeRecord = APS_TestDataFactory.createWorkType(1)[0];
        insert workTypeRecord;
        System.debug('DildarLog: TEST - workTypeRecord - ' + workTypeRecord.Id);
        WorkOrder wo = APS_TestDataFactory.createWorkOrder(null, null, workTypeRecord.Id, null, null, null, null, null, null, null, 1)[0];
        wo.Number_of_Remaining_Assets__c = 0;
        wo.Site_Tasks_Generated__c = true;
        
        Draft_Work_Order__c dwo = new Draft_Work_Order__c();
        dwo.Reference_Id__c = 'dwo12345';
        dwo.Total_Num_Of_Work_Orders__c = 1;
        dwo.Num_Of_Failed_Work_Orders__c = 0;
        dwo.Num_of_Success_Work_Orders_v2__c = 0;
        dwo.Status__c = 'Generation In Progress';

		insert dwo;
        
        MaintenancePlan mp = [Select Id from MaintenancePlan Limit 1][0];
        Asset ast = [Select AccountId, Id from Asset Limit 1][0];
       
        wo.Draft_Work_Order__c = dwo.Id;
        wo.Maintenance_Plan__c = '{"02i5P000002esbvQAA":["1MP5P00000004yxWAA"]}';
        wo.Maintenance_Asset_Ids__c = '1MP5P00000004yxWAA';
        wo.SuggestedMaintenanceDate = System.TODAY();
        wo.MaintenancePlanId = mp.Id;
        wo.AssetId = ast.Id;
        wo.AccountId = ast.AccountId;
        insert wo;
                
        BatchUpdateDraftWOSuccessCount obj = new BatchUpdateDraftWOSuccessCount();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
    }
}