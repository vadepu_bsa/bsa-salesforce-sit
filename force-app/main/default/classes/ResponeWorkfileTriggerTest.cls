@isTest
public class ResponeWorkfileTriggerTest {
    
    @isTest Static void createWorkfileRecordTest(){
        
        BSA_TestDataFactory.createSeedDataForTesting();
        BSA_TestDataFactory.createTestResponseDetailsRecords();    
        
    	Question_Response_WorkFile__c Qrw = new Question_Response_WorkFile__c();
      	Qrw.Response_Header__c = 'Test Workfile Record';
    
        Test.startTest(); 
        
        Database.SaveResult result = Database.insert(Qrw, False);
        
        Test.stopTest();
        
        System.assert(result.isSuccess());
    }
    

}