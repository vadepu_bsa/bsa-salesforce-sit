/**
 * @File Name          : ActionLabourRateToBSAHandler.cls
 * @Description        : 
 * @Author             : IBM
 * @Group              : 
 * @Last Modified By   : Tom Henson
 * @Last Modified On   : 03/08/2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    31/07/2020         Tom Henson             Initial Version
**/
public class ActionLabourRateToBSAHandler extends PlatformEventSubscriptionBaseHandler{
    
    private Map<string,String> mIdentifierMapping;
    private String mAdditionalWhereClause;
    private LabourRateDTO mBaseDTO;
    private String mSpecificDTOClassName ;
    private Map<string,Object> mSerializedData;
    private String eventActionName;
    private String correlationId;
    private String accountcode;
    
    public override void process(sObjectType peSobjectType,List<sObject> peToProcessList,sObjectType mappedSobjectType){
       
        for(sObject sObj : peToProcessList){
            Action_LabourRate_to_BSA__e event = (Action_LabourRate_to_BSA__e)sObj;
            eventActionName = event.action__c;
            correlationId = event.correlationId__c;
            deserializeJSON(event);
            accountCode = event.AccountCode__c;
            setupAdditonalSettings(event);   
        }
        super.process(peSobjectType,peToProcessList, mappedSobjectType);       
    }

    private void deserializeJSON(Action_LabourRate_to_BSA__e event){
     
        String jsonString = '';
        if(String.isNotBlank(event.LabourRates__c)){
            String tempString  = (event.LabourRates__c);
            // System.debug('Labour Rate JSON'+tempString);
            jsonString = jsonString + tempString;
        }
        boolean singleLabourRate = false;
        if (jsonstring.contains('"labourdetails":{')){
          jsonstring =   jsonstring.replace('"labourdetails":{','"labourdetails":[{');
          singleLabourRate = true;
        }
        if (singleLabourRate){
            jsonstring = '{'+jsonString.removeEnd(',')+']}';
        }else {
              jsonstring = '{'+jsonString.removeEnd(',')+'}';
        }
        // System.debug('BuildJson'+ jsonstring);

        mSpecificDTOClassName ='LabourRateDTO';
     
        // System.debug('Class Name'+mSpecificDTOClassName);
        // I hate apex for not giving an option to dynamically type case interfaces & classes using string. That is why had to type cast statically. 
        if(mSpecificDTOClassName.equalsIgnoreCase('LabourRateDTO')){
            LabourRateDTO newLabourRateDTO = new LabourRateDTO();
            mBaseDTO = newLabourRateDTO.parse(jsonstring);
            mSerializedData = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(mBaseDTO));
        }
                
        // System.debug('Deserilize class'+mBaseDTO);
        // System.debug('Serialized class'+mSerializedData);
    }

	public override void processBuinessLogic(){
        // System.debug('!!!!Tom processBuinessLogic !!!! ');
        List<sObject> finalsObjectList = getMappedObjectList();
        // System.debug('!!!Final sObjectList '+ finalsObjectList);
        List<Labour_Rate__c> finalLabourRateList = new List<Labour_Rate__c>();
        String accountId = null;
        String serviceTerritory = null;
        Set<Id> successIdSet = new Set<Id>();
        String labourRateExternalField = getMappedExternalIdFromNode('labourRateDetails',false);
 
        if(finalsObjectList.size() > 0){
           	
            for(integer i= 0; i < finalsObjectList.size(); i++){
                if (finalsObjectList[i].get('Account__c') != null && finalsObjectList[i].get('Account__c')  != ''){
                    accountId =  (String)finalsObjectList[i].get('Account__c');
                    accountId =  accountId.trim();
                    finalsObjectList[i].put('Account__c',null);
                }
                 if ( finalsObjectList[i].get('Service_Territory__c') != null && finalsObjectList[i].get('Service_Territory__c') != ''){
                    serviceTerritory =  (String)finalsObjectList[i].get('Service_Territory__c');
                    serviceTerritory = serviceTerritory.trim();
                    finalsObjectList[i].put('Service_Territory__c',null);
                }
                
                finalsObjectList[i].put('CorrelationId__c',correlationId);    
                Map<String, Object> labourRateMap = new Map<String, Object>(finalsObjectList[i].getPopulatedFieldsAsMap());
                labourRateMap.remove('Service_Territory__c');
                labourRateMap.remove('Account__c');
                Labour_Rate__c labourRateObj =  (Labour_Rate__c) JSON.deserialize( JSON.serialize( labourRateMap ), Labour_Rate__c.class );
                if (accountId != null){
                    String accountIdRelationshipName = Labour_Rate__c.Account__c.getDescribe().getRelationshipName();
                    Account accSObj = new Account();
                    accSObj.CustomerCode__c =accountId;
                    labourRateObj.putSObject(accountIdRelationshipName, accSObj);   
                }
                if (serviceTerritory != null){
                    String serviceTerrRelName = Labour_Rate__c.Service_Territory__c.getDescribe().getRelationshipName();
                    ServiceTerritory ServiceTerritorysObj =  new ServiceTerritory() ;
                    ServiceTerritorysObj.Pronto_ID__c =  serviceTerritory;
                    labourRateObj.putSObject(serviceTerrRelName, ServiceTerritorysObj);   
                }
                finalLabourRateList.add(labourRateObj);
            }
            
           SObjectField labourRateRefId = getSObjectFieldName('Labour_Rate__c', labourRateExternalField);

            Database.UpsertResult[] upsertResult = Database.upsert(finalLabourRateList, labourRateRefId , false);
            // System.debug('!!!!Tom Log: upsertResult Labour Rate - ' + upsertResult);   
            successIdSet = UtilityClass.readUpsertResults(upsertResult, JSON.serialize(finalLabourRateList), 'LabourRate', correlationId, eventActionName, accountCode); 			
        }
    }

    private void setupAdditonalSettings(Action_LabourRate_To_BSA__e event){

        mIdentifierMapping = new Map<string,String>(); 
        mIdentifierMapping.put('Action_Name__c',event.Action__c);  

        setAdditionalMappingFilters('Action_Name__c',mIdentifierMapping,mAdditionalWhereClause);
        setFieldAndPayloadMappingRecords(mSerializedData);
    }
}