@isTest
public class LocationAutomationBusinessLogicTest {
    static testMethod void testValidateLocationLinkwithSr(){
        
        // Create a test location record
        Schema.Location locationRecord = new Schema.Location();
        locationRecord.name = 'Testing Location Automation';
        locationRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        locationRecord.Description = 'Testing Location Automation';
        locationRecord.Van_Id__c = 'test0007';
        locationRecord.LocationType = 'Warehouse';
        locationRecord.IsMobile = true;
        locationRecord.IsInventoryLocation = true;
        locationRecord.Territory__c = 'NSW';
		locationRecord.Correlation_ID__c = '88a57e4e-b22b-4683';
		locationRecord.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        Test.startTest();
        
        // Create a test user record
       	Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Field Technician' LIMIT 1];
        User userRec = new User( email='test2121.user@gmail.com',
                            profileid = profileDetails.Id, 
                            UserName='test112212.user@gmail.com', 
                            Alias = 'GDS',
                            TimeZoneSidKey='America/New_York',
                            EmailEncodingKey='ISO-8859-1',
                            LocaleSidKey='en_US', 
                            LanguageLocaleKey='en_US',
                            PortalRole = 'Manager',
                            FirstName = 'Test',
                            LastName = 'User');
         insert userRec;
        
        // Create a test Service Resource record
        ServiceResource serviceResourceRecord = new ServiceResource();
        serviceResourceRecord.Name = 'Test user';
        serviceResourceRecord.User_Id__c = userRec.Id;
        serviceResourceRecord.RelatedRecordId = userRec.Id;
        serviceResourceRecord.IsActive = true;
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        
        insert serviceResourceRecord;
        
        Database.SaveResult srLocation = database.insert(locationRecord);
        
        Test.stopTest();
                
        // Verify SaveResult value
        System.assertEquals(true, srLocation.isSuccess());
        
        // Verify that a service resource with matching pronto service resource id found.
        List<ServiceResource> serviceResources = [SELECT Id FROM ServiceResource where Pronto_Service_Resource_ID__c=:locationRecord.Pronto_Service_Resource_ID__c limit 1];
        // Validate that the service resource found.
        System.assertEquals(1, serviceResources.size());
    }
    
    static testMethod void testInvalidLocationLinkwithSr(){
        
        // Create a test location record
        Schema.Location locationRecord = new Schema.Location();
        locationRecord.name = 'Testing Location Automation';
        locationRecord.Pronto_Service_Resource_ID__c = 'invalid-srid-88a57e4e-b22b-78965';
        locationRecord.Description = 'Testing Location Automation';
        locationRecord.Van_Id__c = 'test0007';
        locationRecord.LocationType = 'Warehouse';
        locationRecord.IsMobile = true;
        locationRecord.IsInventoryLocation = true;
        locationRecord.Territory__c = 'NSW';
		locationRecord.Correlation_ID__c = '88a57e4e-b22b-4683';
		locationRecord.Event_TimeStamp__c = '2019-02-03T00:35:11.000Z';
        
        Test.startTest();
        
        // Create a test user record
       	Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Field Technician' LIMIT 1];
        User userRec = new User( email='tes21212t.user@gmail.com',
                            profileid = profileDetails.Id, 
                            UserName='tes21212t.user@gmail.com', 
                            Alias = 'GDS',
                            TimeZoneSidKey='America/New_York',
                            EmailEncodingKey='ISO-8859-1',
                            LocaleSidKey='en_US', 
                            LanguageLocaleKey='en_US',
                            PortalRole = 'Manager',
                            FirstName = 'Test',
                            LastName = 'User');
         insert userRec;
        
        // Create a test Service Resource record
        ServiceResource serviceResourceRecord = new ServiceResource();
        serviceResourceRecord.Name = 'Test user';
        serviceResourceRecord.User_Id__c = userRec.Id;
        serviceResourceRecord.RelatedRecordId = userRec.Id;
        serviceResourceRecord.IsActive = true;
        serviceResourceRecord.Pronto_Service_Resource_ID__c = 'srid-88a57e4e-b22b-78965';
        
        insert serviceResourceRecord;
        
        Database.SaveResult srLocation = database.insert(locationRecord);
        
        Test.stopTest();
                
        // Verify SaveResult value
        System.assertEquals(true, srLocation.isSuccess());
        
        // Verify that a service resource with matching pronto service resource id NOT found.
        List<ServiceResource> serviceResources = [SELECT Id FROM ServiceResource where Pronto_Service_Resource_ID__c=:locationRecord.Pronto_Service_Resource_ID__c limit 1];
        // Validate that the service resource NOT found.
        System.assertEquals(0, serviceResources.size());
    }
}