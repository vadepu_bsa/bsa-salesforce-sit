@isTest
public class ContentDocumentLinkShareHandlerTest {
    @testSetup static void setup() {
        BSA_TestDataFactory.createSeedDataForTesting(); 
    }
    
    static testMethod void testPositiveScenarios(){   
        Test.startTest();
       	Workorder wo =[SELECT Id from Workorder limit 1];
        //WorkorderLineitem woLI =[SELECT Id from WorkorderLineitem limit 1];
        Test.setMock(HttpCalloutMock.class, new FileUploadFutureMock());
       	BSA_TestDataFactory.createTestContentDocuments(wo.id);
        //BSA_TestDataFactory.createTestContentDocuments(woLI.id);
        Test.stopTest();
        
    }
    
    static testMethod void testAccountAttachment(){   
        Test.startTest();
		Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
       	//Account acc =[SELECT Id from Account where RecordTypeId =: devRecordTypeId limit 1];
       	Account acc =[SELECT Id from Account  limit 1];
        acc.RecordTypeId = devRecordTypeId;
        acc.Pronto_Supplier_Code__c = '1234';
        update acc;
        //WorkorderLineitem woLI =[SELECT Id from WorkorderLineitem limit 1];
        Test.setMock(HttpCalloutMock.class, new FileUploadFutureMock());
       	BSA_TestDataFactory.createTestContentDocuments(acc.id);
        //BSA_TestDataFactory.createTestContentDocuments(woLI.id);
        Test.stopTest();   
    }
}