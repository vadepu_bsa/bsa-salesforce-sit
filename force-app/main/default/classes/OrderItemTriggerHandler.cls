/**
 * @author          David Azzi (IBM)
 * @date            06/Aug/2020
 * @description     Trigger Handler added for the Order Item trigger
 */

public class OrderItemTriggerHandler implements ITriggerHandler {
    public OrderItemTriggerHandler() {

    }

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;

    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        System.debug('*- BeforeInsert Order Item:');
        OrderItemTriggerHelper.populateOrderCharges(newItems);
    }
    
    public void AfterInsert(Map<Id, SObject> newItems){
        System.debug('*- AfterInsert Order Item:');
        List<SObject> lstOrderItem = new List<SObject>();
        for (SObject oi : newItems.values()){
            lstOrderItem.add(oi);
        }
        OrderItemTriggerHelper.sendOrderLineItemEvent(lstOrderItem,'Insert');
        
    }    
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
        System.debug('*- BeforeUpdate Order Item:');
        List<SObject> lstOrderItem = new List<SObject>();
        for (SObject oi : newItems.values()){
            lstOrderItem.add(oi);
        }
        OrderItemTriggerHelper.populateOrderCharges(lstOrderItem);
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        System.debug('*- AfterUpdate Order Item:');
        List<SObject> lstOrderItem = new List<SObject>();
        for (SObject oi : newItems.values()){
            lstOrderItem.add(oi);
        }
        OrderItemTriggerHelper.sendOrderLineItemEvent(lstOrderItem,'Update');
        OrderItemTriggerHelper.updateDefect(newItems,oldItems);
        
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
        System.debug('*- BeforeDelete Order Item:');
        List<SObject> lstOrderItem = new List<SObject>();
        for (SObject oi : oldItems.values()){
            lstOrderItem.add(oi);
        }
        OrderItemTriggerHelper.sendOrderLineItemEvent(lstOrderItem,'Delete');
        
        
    }  public void AfterDelete(Map<Id, SObject> oldItems) {}     public void AfterUndelete(Map<Id, SObject> oldItems) {}
}