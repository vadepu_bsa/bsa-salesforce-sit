global class ArtifactBatchSync implements Database.Batchable<ContentDocumentLink>, Database.Stateful, Database.AllowsCallouts {

    global Map<Id,WorkOrderLineItem> woliMap;
    global Map<Id,String> filepathWoliMap;
    global String SFOrgID = UserInfo.getOrganizationId();
    global List<Application_Log__c> applicationLogList;
 
    global ArtifactBatchSync() {
        applicationLogList = new List<Application_Log__c> ();
        filepathWoliMap = new Map<Id,String>();
        //if(Test.isRunningTest()) {
            woliMap = new Map<Id,WorkOrderLineItem>([SELECT Id, WorkOrderId, WorkOrder.Gdrive_Artifact_Path__c, WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c, WorkOrder.NBN_Activity_Start_Date_Time__c FROM WorkOrderLineItem WHERE WorkOrder.ParentWorkOrder.Createdby.Name = 'Automated Process' AND WorkOrder.Gdrive_Artifact_Path__c = '' LIMIT 2]);
        //}
        //else{
            woliMap = new Map<Id,WorkOrderLineItem>([SELECT Id, WorkOrderId, WorkOrder.Gdrive_Artifact_Path__c, WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c, WorkOrder.NBN_Activity_Start_Date_Time__c FROM WorkOrderLineItem WHERE WorkOrder.ParentWorkOrder.Createdby.Name = 'Automated Process' AND WorkOrder.Gdrive_Artifact_Path__c = '']);
        //}
        
    }
     
    global Iterable<ContentDocumentLink> start(Database.batchableContext info) { 
         return new CustomIterableExample(); 
    } 

    global void execute(Database.BatchableContext BC, List<ContentDocumentLink> records) {         
          
        Map<Id,Id> documentEntityMap = new Map<Id,Id> ();   
        
        for(ContentDocumentLink obj : records){
            documentEntityMap.put(obj.ContentDocumentId,obj.LinkedEntityId);
        }

        for(ContentVersion cv : [SELECT Id, ContentDocumentId, Title, FileType, VersionData FROM ContentVersion WHERE ContentDocumentId IN : documentEntityMap.keyset()]) {
            try {                  
                JSONGenerator gen = JSON.createGenerator(true);    
                gen.writeStartObject();      
                gen.writeStringField('NBNWorkOrderID', woliMap.get(documentEntityMap.get(cv.ContentDocumentId)).WorkOrder.ParentWorkOrder.NBN_Parent_Work_Order_Id__c);
                gen.writeDateTimeField('NBNActivityStartDate', woliMap.get(documentEntityMap.get(cv.ContentDocumentId)).WorkOrder.NBN_Activity_Start_Date_Time__c);
                gen.writeStringField('SFOrgID',SFOrgID);
                gen.writeStringField('FileType',cv.FileType.toLowerCase());
                gen.writeStringField('Title',cv.Title);
                gen.writeStringField('FileBody',EncodingUtil.base64Encode(cv.VersionData));
                gen.writeEndObject();
                  
                String jsonS = gen.getAsString();
                String addr = System.Label.Artifacts_Upload_Url;
                
                HttpRequest req = new HttpRequest();
                HttpResponse response = new HttpResponse();
                Http http = new Http();
                
                req.setEndpoint(addr);
                req.setMethod('POST');
                req.setHeader('Content-Type','application/JSON');
                req.setBody(jsonS);
                
                if (!Test.isRunningTest()) {      
                    response = http.send(req);
                    system.debug('response:'+response);
                }
                else {
                    response.setHeader('Content-Type', 'application/json');
                    response.setBody('{"Response": {"statusCode":200, "status":"OK", "filePath":"SYDCLARION01"}}');
                }
                
                Map<String,Object> responseMap;
            
                String jsonResponse = response.getBody();
                
                if(jsonResponse.contains('statusCode')){
                    responseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonResponse);
                }
                
                System.debug('responseMap --> '+responseMap);
                String msg = '', filePath = '';
                
                Application_Log__c alObj = new Application_Log__c();
                System.debug('WOLI ID-->'+documentEntityMap.get(cv.ContentDocumentId));
                alObj.Work_Order_Line_Item__c = documentEntityMap.get(cv.ContentDocumentId);
                
                system.debug('status code --> '+responseMap.get('statusCode'));
                system.debug('status --> '+responseMap.get('status'));
                
                if(responseMap.get('statusCode') == '200' && responseMap.get('status') == 'OK') {
                    alObj.Log_Level__c = 'Info';
                    msg = 'File uploaded to Gdrive successfully';
                    filePath = (String)responseMap.get('filePath');
                    filePath.replaceAll('\"','');
                    filepathWoliMap.put(documentEntityMap.get(cv.ContentDocumentId),filePath);
                }
                else if(responseMap.get('statusCode') != '200' && responseMap.get('status') != 'OK') {
                    alObj.Log_Level__c = 'Error';
                    msg = 'File upload to Gdrive unsuccessful';
                }
                
                alObj.Message__c = (String)responseMap.get('statusCode') + ' ' + (String)responseMap.get('status') + '\n' + msg;
                System.debug('alObj-->'+alObj);
                applicationLogList.add(alObj);
                        
            }
            
            catch (Exception e) {         
                System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber());           
            }
        }
    }   

    global void finish(Database.BatchableContext BC) { 
    
        // Create Application Log Entry
        try {
            System.debug('applicationLogList-->'+applicationLogList);
            if(!applicationLogList.isEmpty()) {
                insert applicationLogList; 
            }
        }
        catch(Exception e){
            System.debug('Error:' + e.getMessage() + 'LN:' + e.getLineNumber()); 
        }
            
        Map<Id,WorkOrder> workOrderMap = new Map<Id,WorkOrder> ();  

        system.debug('filepathWoliMap --> '+filepathWoliMap);
        
        for(Application_Log__c alObj : applicationLogList) {    
            if(alObj.Log_Level__c == 'Info') {
                Id woId = woliMap.get(alObj.Work_Order_Line_Item__c).WorkOrderId;
                System.debug('woId:'+woId);
                WorkOrder wo = new WorkOrder(Id = woId);
                if(filepathWoliMap.containsKey(alObj.Work_Order_Line_Item__c)) {
                    wo.Gdrive_Artifact_Path__c = filepathWoliMap.get(alObj.Work_Order_Line_Item__c).replaceAll('\"','');
                    workOrderMap.put(woId, wo);
                }
                system.debug('Path --> '+wo.Gdrive_Artifact_Path__c);
            }
        }       

        try {
            if(!workOrderMap.values().isEmpty()) {
                update workOrderMap.values();
            }
        }
        catch (Exception ex) {
            system.debug('Exception is:'+ex);
        }   
        
        List<ContentDocumentLink> contentList = [SELECT Id, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN (SELECT Id FROM WorkOrderLineItem WHERE WorkOrder.ParentWorkOrder.Createdby.Name = 'Automated Process' AND WorkOrder.Gdrive_Artifact_Path__c = '')];
        
        if(contentList.size() > 0 && !Test.isRunningTest()) {
            database.executeBatch(new ArtifactBatchSync());
        }
        
    }
}