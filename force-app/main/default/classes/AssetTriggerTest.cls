/**
* @author          David.A
* @date            08/September/2020
* @description     
*
* Change Date       Modified by         Description  
* 08/Sep/2020       David.A (IBM)       TestUpdateEGQuantity on AssetTriggerHelper
**/

@isTest
public with sharing class AssetTriggerTest {

    @TestSetup
    static void setup(){

        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = false;
        lstWorkType[1].CUI__c = true;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

        List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;

        Id contactRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactRTypeId, 1);
        insert lstContact;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 5);
        insert lstUser;

        List<ServiceResource> lstServiceResource = new List<ServiceResource>();
        for (User usr : lstUser){
            lstServiceResource.add(APS_TestDataFactory.createServiceResource(usr.Id));
        }
        insert lstServiceResource;

        List<ServiceTerritoryMember> lstServiceTerritoryMember = APS_TestDataFactory.createServiceTerritoryMember(lstServiceResource[0].Id, lstServiceTerritory[0].Id, lstServiceResource.size() );
        for(Integer i = 0; i < lstServiceResource.size(); i++){
            lstServiceTerritoryMember[i].ServiceResourceId = lstServiceResource[i].Id;
        }
        insert lstServiceTerritoryMember;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[1].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        lstAssetParents[1].AccountId = lstAccount[1].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[1].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 5);
        lstAssetChildren[1].ParentId = lstAssetParents[1].Id;
        lstAssetChildren[1].Status = 'Active';
        lstAssetChildren[2].ParentId = lstAssetParents[1].Id;
        lstAssetChildren[2].Status = 'New';
        lstAssetChildren[3].ParentId = lstAssetParents[1].Id;
        lstAssetChildren[3].Status = 'Decommissioned';
        lstAssetChildren[4].Status = 'Inactive';
        lstAssetChildren[4].Reactivate_Date__c = System.today().addDays(3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[2].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 5);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        FSL__Scheduling_Policy__c fslScheduling = APS_TestDataFactory.createAPSCustomerFSLSchedulingPolicy();
        insert fslScheduling;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 3);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        lstWOAR[2].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;

      /*  List<WorkOrder> lstWorkOrder = APS_TestDataFactory.createWorkOrder(lstAccount[2].Id, null, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstServiceContract[0].Id, lstPriceBook[0].Id, objCase.Id, null, lstMaintenancePlan[0].Id, lstMaintenanceAsset[0].Id, 6);
        lstWorkOrder[0].ParentWorkOrderId = null;
        lstWorkOrder[0].AccountId = lstAccount[1].Id;
        lstWorkOrder[1].AccountId = lstAccount[1].Id;
        lstWorkOrder[1].ParentWorkOrderId = lstWorkOrder[1].Id;
        insert lstWorkOrder;

        Id woliRT = APS_TestDataFactory.getRecordTypeId('WorkOrderLineItem', 'Asset_Action');
        List<WorkOrderLineItem> lstWoli = APS_TestDataFactory.createWorkOrderLineItem(lstAssetChildren[0].Id, lstWorkOrder[0].Id, lstMaintenancePlan[0].Id, 3);
        lstWoli[0].RecordTypeId = woliRT;
        lstWoli[1].RecordTypeId = woliRT;
        lstWoli[2].RecordTypeId = woliRT;
        lstWoli[1].AssetId = lstAssetChildren[1].Id;
        lstWoli[2].AssetId = lstAssetChildren[2].Id;
        insert lstWoli;*/
    }


    
    @isTest
    public static void testUpdate(){
        
        List<Asset> lstAssetUpdate = [SELECT Id FROM Asset WHERE Status = 'Active' LIMIT 1];

        Test.startTest();
            lstAssetUpdate[0].Status = 'Decommissioned';
            update lstAssetUpdate;
        Test.stopTest();
    }

        
    @isTest
    public static void testInsert(){

        List<Equipment_Type__c> lstEquipmentType = [SELECT Id FROM Equipment_Type__c LIMIT 1];
        List<Account> lstAccount = [SELECT Id FROM Account Limit 1];

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[0].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 1);

        Test.startTest();
            insert lstAssetParents;
            AssetTriggerHandler.TriggerDisabled = true;
            AssetTriggerHandler ath = new AssetTriggerHandler();
            Boolean isDisabled = ath.isDisabled();
        Test.stopTest();
    }

    @isTest
    public static void testDelete(){
        
        List<Asset> lstAssetUpdate = [SELECT Id FROM Asset ];

        Test.startTest();
            delete lstAssetUpdate;
        Test.stopTest();
    }

}