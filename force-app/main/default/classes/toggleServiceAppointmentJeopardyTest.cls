@IsTest(SeeAllData = true)
public with sharing class toggleServiceAppointmentJeopardyTest {

	@IsTest
    static void toggleServiceAppointmentJeopardyAction() {
        toggleServiceAppointmentJeopardy actionTest = new toggleServiceAppointmentJeopardy();
        Datetime ganttStartDate = datetime.newInstance(2019, 02, 01);
        Datetime ganttEndDate = datetime.newInstance(2019, 02, 01);
        Map<String, Object> additionalParameters = new Map<String, Object>();
        List<Id> serviceAppointmentsIds = new List<Id>();
        System.assertEquals('Service Appointments successfully processed: ', 
                            actionTest.action(serviceAppointmentsIds, 
                                              ganttStartDate, ganttEndDate, additionalParameters));
    }
}