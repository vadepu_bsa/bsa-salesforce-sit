global class ScheduleBatchWOStatusAutomation implements Schedulable{
 
    global void execute(SchedulableContext SC) {    
		//Run once per scheduling
        Database.executeBatch(new BatchWOStatusUpdateAutomation());
        
        //Schedule batch to run on defined interval
       // ScheduleBatchWOStatusAutomation.start();
	}
}