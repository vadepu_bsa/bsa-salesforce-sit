@isTest
private class APS_FrequencyPicklistCtrTest {
    @IsTest
    static void getMaintenanceRoutinePicklistTest() {
        List<APS_FrequencyPicklistCtr.SelectOptionObj> mpOptions = APS_FrequencyPicklistCtr.getPicklistOptions('MaintenanceAsset', 'Maintenance_Routine_Global__c');
        System.assert(mpOptions.size() > 0);
    }
}