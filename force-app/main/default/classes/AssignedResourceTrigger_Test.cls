/**
* @author          Sunila.M
* @date            18/May/2019	
* @description     Test class for AssignedResourceTrigger
*
* Change Date    Modified by         Description  
* 18/May/2019		Sunila.M		Created Test class
**/
@isTest
public class AssignedResourceTrigger_Test {
    
    @TestSetup
    static void setup(){
        
        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Account accObj = new Account();
        AccObj.Name ='Default Account';
        accObj.RecordTypeid = AccountClientRTypeId;
        insert accObj;
        
        
        Entitlement entObj = new Entitlement();
        entObj.Name = 'PM - Default Entitlement';
        entobj.AccountId = accObj.Id;
        // entobj.BusinessHoursId = lstOperatingHours[0].Id;
        insert entobj;
    }
    
    @isTest static void deleteWOShareRecord() {
        
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'APS_Business_System_Admin' Limit 1];
        
        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' And IsActive = True Limit 1];
        
        adminUser.UserRoleId = userRole.Id;
        update adminUser;
        
        System.runAs(adminUser){
            
            Test.startTest();
            //create Account
            Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId();
            Account account1 = new Account();
            account1.Name = 'Test Account';
            account1.RecordTypeId = clientRecordTypeId;
            insert account1;
            system.debug('account1'+account1.Id);
            
            Id contactClientRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Client').getRecordTypeId();
            
            Contact conObj = New Contact();
            conObj.LastName = 'Testing';
            conObj.Phone = '1234567812';
            conObj.MobilePhone  ='987654321987';
            conObj.AccountId = account1.Id;
            conObj.RecordTypeId = contactClientRecordTypeId;
            insert conObj;
            
            Id customerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
            
            Account account2 = new Account();
            account2.Name = 'Test Account Two';
            account2.RecordTypeId = customerRecordTypeId;
            insert account2;
            
            //--Create Work type
            WorkType workType = new WorkType();
            workType.Name = 'CN (Install)';
            workType.APS__c = false;
            workType.CUI__c = true;
            Date todayDate = Date.today();
            workType.EstimatedDuration = 5;
            
            insert workType;
            system.debug('workType'+workType.Id);
            
            //--create Price Book
            Pricebook2 priceBook = new Pricebook2();
            priceBook.Name = 'NSW-CN-Metro-PRICE-BOOK';
            priceBook.IsActive = true;
            insert priceBook;
            
            //--create operating hours
            OperatingHours operatingHours = new OperatingHours();
            operatingHours.Name = 'calender 1';
            insert operatingHours;
            
            //--create service territory
            ServiceTerritory serviceTerritory = new ServiceTerritory();
            serviceTerritory.Name = 'New South Wales';
            serviceTerritory.OperatingHoursId = operatingHours.Id;
            serviceTerritory.IsActive = true;
            insert serviceTerritory;
            
            Id devRecordTypeId = Schema.SObjectType.Work_Order_Automation_Rule__c.getRecordTypeInfosByName().get('Price Book Automation').getRecordTypeId();
            System.debug('devRecordTypeId'+devRecordTypeId);
            
            //--create WOAR
            Work_Order_Automation_Rule__c woar = new Work_Order_Automation_Rule__c();
            woar.Account__c = account1.Id;
            woar.RecordTypeId = devRecordTypeId;
            woar.Work_Area__c = 'Metro';
            woar.Work_Type__c = workType.Id;
            woar.Service_Territory__c = serviceTerritory.Id;
            woar.Price_Book__c = priceBook.Id;
            //insert woar;
            
            //create case
            Case caseRecord =new Case();
            caseRecord.Work_Area__c = 'Metro';
            caseRecord.Status = 'New';
            //caseRecord.Task_Type__c = 'Hazard';
            caseRecord.Origin = 'Email';
            caseRecord.Work_Type__c = workType.Id;
            caseRecord.AccountId = account1.Id;
            caseRecord.ContactId = conObj.Id;
            //insert caseRecord;
            
            
            //insert WO
            Account acc = NBNTestDataFactory.createTestAccounts();
            ServiceContract serviceContract = NBNTestDataFactory.createTestServiceContract(acc.Id);
            WorkOrder workorder = NBNTestDataFactory.createTestParentWorkOrders(acc.id,serviceContract.ContractNumber);
            
            //System.assertEquals([SELECT Pricebook2Id FROM WorkOrder WHERE id=:workOrder.Id].Pricebook2Id, priceBook.Id);
            
            Date todayValue = Date.today();
            //Create SA
            ServiceAppointment sa1 = new ServiceAppointment();
            sa1.ParentRecordId = workOrder.Id;
            sa1.EarliestStartTime = todayValue.addDays(3);
            sa1.DueDate = todayValue.addDays(5);
            sa1.SchedStartTime = todayValue.addDays(3);
            sa1.SchedEndTime = todayValue.addDays(9);
            insert sa1;
            
            Entitlement entObj = new Entitlement();
            entObj.Name = 'Test Entitlement';
            entobj.AccountId = account1.Id;
            //insert entobj;
            //workOrder.EntitlementId = entobj.Id;
            update workOrder;
            //create community user
            Id technicianRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
            Account portalAccount = new Account(name = 'portalAccount', recordTypeId = technicianRecordTypeId);
            insert portalAccount;
            Id technicianContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
            Contact portalContact = new contact(LastName = 'portalContact', AccountId = portalAccount.Id, recordTypeId = technicianContactRecordTypeId);
            portalContact.Phone ='1234123412';
            portalContact.MobilePhone ='1234123412';
            insert portalContact;            
            
            
            Profile profileDetails = [SELECT Id FROM Profile WHERE Name = 'BSA Prime Community Plus User' LIMIT 1];
            system.debug('profileId'+profileDetails.Id);
            UserRole roledetails = [Select Id, name, PortalRole from userRole where PortalRole  = 'Manager' Limit 1];
            User u1 = new User( email='test.user@gmail.com',
                               profileid = profileDetails.Id, 
                               UserName='test.u43434ser@gmail.com', 
                               Alias = 'GDS',
                               TimeZoneSidKey='America/New_York',
                               EmailEncodingKey='ISO-8859-1',
                               LocaleSidKey='en_US', 
                               LanguageLocaleKey='en_US',
                               ContactId = portalContact.Id,
                               
                               FirstName = 'Test',
                               LastName = 'User');
            insert u1;
            
            ServiceResource sr1 = new ServiceResource();
            sr1.Name = 'Test user';
            sr1.Account__c = portalAccount.Id;
            sr1.AccountId = portalAccount.Id;
            sr1.Contact__c = portalContact.id;
            sr1.User_Id__c = u1.Id;
            sr1.RelatedRecordId = u1.Id;
            sr1.IsActive = true;
            sr1.Pronto_Service_Resource_ID__c = '12345';
            //sr1.IsOptimizationCapable =true;
            insert sr1;
            
            OperatingHours opHours = new OperatingHours();
            opHours.Name = 'Base Cal';
            insert opHours;
            
            ServiceTerritory territory = new ServiceTerritory();
            territory.Name = 'New South Wales1';
            territory.IsActive =true;
            territory.OperatingHoursId = opHours.Id;
            insert territory;
            
            ServiceTerritoryMember member = new ServiceTerritoryMember();
            member.ServiceTerritoryId =territory.Id;
            member.ServiceResourceId = sr1.Id;
            member.EffectiveStartDate = todayValue.addDays(-1);
            member.EffectiveEndDate = todayValue.addDays(30);
            insert member;
            
            AssignedResource resource1 = new AssignedResource();
            resource1.ServiceAppointmentId = sa1.Id;
            resource1.ServiceResourceId = sr1.Id;
            insert resource1;
            
            //create second community user
            Account portalAccount2 = new Account(name = 'community Account', recordTypeId = technicianRecordTypeId);
            insert portalAccount2;
            
            Contact portalContact2 = new contact(LastName = 'Tech two Contact', AccountId = portalAccount2.Id, recordTypeId = technicianContactRecordTypeId);
            portalContact2.Phone ='1234123412';
            portalContact2.MobilePhone ='1234123412';
            insert portalContact2;  
            
            User u2 = new User( email='test2.user@gmail.com',
                               profileid = profileDetails.Id, 
                               UserName='test2.user@gmail.com', 
                               Alias = 'GDS1',
                               TimeZoneSidKey='America/New_York',
                               EmailEncodingKey='ISO-8859-1',
                               LocaleSidKey='en_US', 
                               LanguageLocaleKey='en_US',
                               ContactId = portalContact2.Id,
                               PortalRole = 'Manager',
                               FirstName = 'Test',
                               LastName = 'User 2');
            insert u2;
            
            ServiceResource sr2 = new ServiceResource();
            sr2.Name = 'Test user 2';
            sr2.Account__c = portalAccount2.Id;
            sr2.AccountId = portalAccount2.Id;
            sr2.Contact__c = portalContact2.id;
            sr2.User_Id__c = u2.Id;
            sr2.RelatedRecordId = u2.Id;
            sr2.IsActive = true;
            sr2.Pronto_Service_Resource_ID__c = '2323';
            //sr1.IsOptimizationCapable =true;
            insert sr2;
            
            ServiceTerritoryMember member2 = new ServiceTerritoryMember();
            member2.ServiceTerritoryId =territory.Id;
            member2.ServiceResourceId = sr2.Id;
            member2.EffectiveStartDate = todayValue.addDays(-1);
            member2.EffectiveEndDate = todayValue.addDays(30);
            insert member2;
            
            WorkOrderShare shareWO = new WorkOrderShare();
            shareWO.ParentId = workOrder.Id;
            shareWO.AccessLevel = 'Read';
            shareWO.UserOrGroupId = u1.id;
            insert shareWO;
            
            resource1.ServiceResourceId = sr2.Id;
            update resource1;
            try{
                WorkOrderShare sharedWO = [SELECT Id FROM WorkOrderShare WHERE ParentId =: shareWO.ParentId AND UserOrGroupId =: shareWO.UserOrGroupId LIMIT 1];
            }
            catch(Exception ex){
                system.debug('ex'+ex.getMessage());
            }
            
            delete resource1;
            
            Test.stopTest();
        }
    }
}