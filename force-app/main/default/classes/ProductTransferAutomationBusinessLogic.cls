/* Author: Abhijeet Anand
 * Date: 09 October, 2019
 * Apex class that handles all pre/post DML operations on the standard FSL object 'ProductTransfer'
 */
 
public class ProductTransferAutomationBusinessLogic {

    // Method to popuplate CorrelationID
    public static void populateCorrelationID (List<SObject> newList) {
    
        for(SObject obj : newList) {
            ProductTransfer pTrn = (ProductTransfer)obj;
            pTrn.CorrelationID__c = UtilityClass.getUUID();
        }
    
    } 
    
    // Method to populate the neccessary fields on ProductItem created as a result of ProductTransfer
    public static void populateProductItem (List<ProductTransfer> productTransferList, Map<Id,SObject> oldMap) {
    
        //Set<Id> newItemIDs = new Set<Id>();
        List<ProductItem> prdItemToUpdate = new List<ProductItem>();
        Map<Id,Id> pTransferMap = new Map<Id,Id>();
        Set<Id> sourceItemIDs = new Set<Id>();
        Map<Id,ProductItem> sourcePrdItemMap = new Map<Id,ProductItem>();
        Map<Id,Id> newItemMap = new Map<Id,Id>();
        
        for(ProductTransfer obj : productTransferList) {
            System.debug('New Status-->'+obj.Status);
            System.debug('Old Map-->'+oldMap);
            if(oldMap != null) {
                ProductTransfer pTrn = (ProductTransfer)oldMap.get(Obj.Id);
                System.debug('Old Status-->'+pTrn.Status);
                if(obj.Status == 'Completed' && pTrn.Status != obj.Status) {
                    pTransferMap.put(obj.Id,obj.SourceProductItemId);
                    sourceItemIDs.add(obj.SourceProductItemId);
                }
            }
        }

        System.debug( 'sourceItemIDs' +sourceItemIDs);
        System.debug( 'pTransferMap' +pTransferMap);
        
        if(!sourceItemIDs.isEmpty()) {
            for(ProductItem obj : [SELECT Id, Product_Code__c, QuantityOnHand, Pronto_Serial_Number__c, Is_Serialized__c, LocationId, SerialNumber,
                               DeSerialized_External_Key__c FROM ProductItem
                               WHERE Id IN : sourceItemIDs]) {
                                   
                sourcePrdItemMap.put(obj.Id,obj);                      
            
            }   
        }

        System.debug( 'sourcePrdItemMap' +sourcePrdItemMap);
        
        if(!pTransferMap.isEmpty()) {
            for(ProductItemTransaction obj : [SELECT Id, ProductItemId, RelatedRecordId FROM ProductItemTransaction WHERE RelatedRecordId IN : pTransferMap.keyset() 
                                          AND TransactionType = 'Transferred' AND Quantity > 0]) {
                newItemMap.put(obj.ProductItemId,obj.RelatedRecordId);
            }
        }
        
        System.debug( 'newItemMap' +newItemMap);
        
        if(!newItemMap.isEmpty()) {
            for(ProductItem obj : [SELECT Id, Product2Id, Product_Code__c, QuantityOnHand, Pronto_Serial_Number__c, Is_Serialized__c, LocationId, SerialNumber,
                                DeSerialized_External_Key__c FROM ProductItem
                                WHERE Id IN : newItemMap.keyset()]) {
        
                obj.DeSerialized_External_Key__c = String.valueOf(obj.Product2Id)+'-'+String.valueOf(obj.LocationId);obj.SerialNumber = sourcePrdItemMap.get(pTransferMap.get(newItemMap.get(obj.Id))).SerialNumber;obj.Is_Serialized__c = sourcePrdItemMap.get(pTransferMap.get(newItemMap.get(obj.Id))).Is_Serialized__c;obj.Product_Code__c = sourcePrdItemMap.get(pTransferMap.get(newItemMap.get(obj.Id))).Product_Code__c;obj.Pronto_Serial_Number__c = sourcePrdItemMap.get(pTransferMap.get(newItemMap.get(obj.Id))).Pronto_Serial_Number__c;prdItemToUpdate.add(obj);
                
            }
        }

        System.debug( 'prdItemToUpdate' +prdItemToUpdate);
        
        if(!prdItemToUpdate.isEmpty()) {
            update prdItemToUpdate;
        }
            
    }
 
}