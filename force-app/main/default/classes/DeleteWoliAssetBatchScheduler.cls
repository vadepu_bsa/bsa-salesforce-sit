global class DeleteWoliAssetBatchScheduler implements Schedulable{

    public static String sched = '0 0 3 * * ?';  //Every Day at 3am 

    global static String DeleteWoliAssetBatchScheduler() {
        return System.schedule('Inactive or Decomissioned Asset removal from WOLI', sched, new DeleteWoliAssetBatchScheduler());
    }

    global void execute(SchedulableContext sc) {
        Id idBatchProcessing = Database.executeBatch(new DeleteWoliAssetsBatch(), 200);           
    }
}