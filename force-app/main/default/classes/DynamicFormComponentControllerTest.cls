/**
 * @author          
 * @date            
 * @description     Test class for DynamicFormComponentController
 *
 * Change Date    Modified by           Description  

 **/
@isTest
public class DynamicFormComponentControllerTest {
    
     @TestSetup
    static void setup(){
        
    
        List<WorkType> lstWorkType = APS_TestDataFactory.createWorkType(2);
        lstWorkType[1].APS__c = false;
        lstWorkType[1].CUI__c = true;
        lstWorkType[0].Name = 'After Hour Call';
        lstWorkType[1].Name = 'After Hour Call';
        insert lstWorkType;
        Id AccountClientRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Client');
        Account accObj = new Account();
        AccObj.Name ='Default Account';
        accObj.RecordTypeid = AccountClientRTypeId;
        insert accObj;
        
         List<OperatingHours> lstOperatingHours = APS_TestDataFactory.createOperatingHours(1);
        insert lstOperatingHours;
        
        Entitlement entObj = new Entitlement();
        entObj.Name = 'PM - Default Entitlement';
        entobj.AccountId = accObj.Id;
       // entobj.BusinessHoursId = lstOperatingHours[0].Id;
        insert entobj;
        

        List<ServiceContract> lstServiceContract = APS_TestDataFactory.createServiceContract(1);
        insert lstServiceContract;

        List<PriceBook2> lstPriceBook = APS_TestDataFactory.createPricebook(2);
        insert lstPriceBook;

        List<Equipment_Type__c> lstEquipmentType = APS_TestDataFactory.createEquipmentType(1);
        insert lstEquipmentType;

       

        List<ServiceTerritory> lstServiceTerritory = APS_TestDataFactory.createServiceTerritory(lstOperatingHours[0].Id, 1);
        lstServiceTerritory[0].Price_Book__c = lstPricebook[0].Id;
        insert lstServiceTerritory;

     
        Id AccountSiteRTypeId = APS_TestDataFactory.getRecordTypeId('Account', 'Site');
        List<Account> lstAccount = APS_TestDataFactory.createAccount(null, lstServiceTerritory[0].Id, AccountSiteRTypeId, 5);
        lstAccount[0].RecordTypeId = AccountClientRTypeId;
        lstAccount[0].Price_Book__c = lstPricebook[0].Id;
        lstAccount[1].ParentId = lstAccount[0].Id;
        lstAccount[1].Price_Book__c = lstPricebook[0].Id;
        lstAccount[2].RecordTypeId = AccountClientRTypeId;
        lstAccount[3].ParentId = lstAccount[2].Id;
        lstAccount[3].Price_Book__c = lstPricebook[0].Id;
        insert lstAccount;

        Id contactRTypeId = APS_TestDataFactory.getRecordTypeId('Contact', 'Client');
        List<Contact> lstContact = APS_TestDataFactory.createContact(lstAccount[0].Id, contactRTypeId, 1);
        insert lstContact;

        List<User> lstUser = APS_TestDataFactory.createUser(null, 1);
        insert lstUser;

        Id assetEGRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Equipment_Group');
        List<Asset> lstAssetParents = APS_TestDataFactory.createAsset(lstAccount[2].Id, null, 'Active' , assetEGRTypeId, lstEquipmentType[0].Id, 2);
        lstAssetParents[0].AccountId = lstAccount[0].Id;
        insert lstAssetParents;

        Id assetRT = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset_Fire');
        List<Asset> lstAssetChildren = APS_TestDataFactory.createAsset(lstAccount[2].Id, lstAssetParents[1].Id, 'Active' , assetRT, lstEquipmentType[0].Id, 3);
        insert lstAssetChildren;

        Id caseRTypeId = APS_TestDataFactory.getRecordTypeId('Case', 'Client');
        Case objCase = APS_TestDataFactory.createCase(lstWorkType[0].Id, lstAccount[2].Id, lstContact[0].Id, caseRTypeId);
        insert objCase;

        List<MaintenancePlan> lstMaintenancePlan = APS_TestDataFactory.createMaintenancePlan(lstAccount[2].Id, lstContact[0].Id, lstWorkType[0].Id, lstServiceContract[0].Id, 5);
        insert lstMaintenancePlan;
        
        List<MaintenanceAsset> lstMaintenanceAsset = APS_TestDataFactory.createMaintenanceAsset(lstAssetParents[1].Id, lstMaintenancePlan[0].Id, lstWorkType[0].Id , 1);
        insert lstMaintenanceAsset;

        ServiceResource saResource = APS_TestDataFactory.createServiceResource(lstUser[0].Id);
        insert saResource;

        Id woarWoliRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'APS_WOLI_Automation');
        Id woarPbRT = APS_TestDataFactory.getRecordTypeId('Work_Order_Automation_Rule__c', 'Price_Book_Automation');
        List<Work_Order_Automation_Rule__c> lstWOAR = APS_TestDataFactory.createWorkOrderAutomationRule(lstAccount[0].Id, woarWoliRT, lstWorkType[0].Id, lstServiceTerritory[0].Id, lstPriceBook[0].Id, 3);
        lstWOAR[1].RecordTypeId = woarPbRT;
        lstWOAR[1].Price_Book__c = lstPriceBook[1].Id;
        lstWOAR[2].Price_Book__c = lstPriceBook[1].Id;
        insert lstWOAR;

    }

    @isTest
    static void getFieldsToDisplay_Test(){
        Id AssetFireRTypeId = APS_TestDataFactory.getRecordTypeId('Asset', 'Asset_Fire');
        Asset astObj = [SELECT id FROM Asset WHERE RecordTypeid =: AssetFireRTypeId LIMIT 1];
        Account accObj = [SELECT Id FROM Account Limit 1];
        Test.Starttest();
        DynamicFormComponentController.getFieldsToDisplay(astObj.Id, 'Asset');
        DynamicFormComponentController.getFieldsToDisplay(accObj.Id, 'Account');
        Test.StopTest();
    
    }
    
     @isTest
    static void getIconNameStr_Test(){
        Test.StartTest();
        DynamicFormComponentController.getIconNameStr('Asset');
         DynamicFormComponentController.getIconNameStr('Account');
         DynamicFormComponentController.getIconNameStr('Equipment_Type__c');
        Test.Stoptest();
        
    
    }


}