/**
* @author          Sunila.M
* @date            09/April/2019 
* @description     
*
* Change Date        Modified by         Description  
* 09/April/2019      Sunila.M           Created the Client Invoice trigger Handler to restrict more than one Client Invoice record
**/
public class ClientInvoiceTriggerHandler implements ITriggerHandler{
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
    Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return false;
    }
    
    public void BeforeInsert(List<SObject> clientInvoiceRecords) 
    {        
        List<Id> clientInvoiceWorkTypeId = new List<Id>();
        List<Id> clientInvoiceClientId = new List<Id>();

        for (Client_Invoice__c eachClientInvoice : (List<Client_Invoice__c>) clientInvoiceRecords)
        {         
            clientInvoiceWorkTypeId.add(eachClientInvoice.WorkType__c);
            clientInvoiceClientId.add(eachClientInvoice.Client__c);                
        }

        //--query existing Client Invoice records to check already CI is available in same ddate range
        //for(Client_Invoice__c eachClientInvoice : [SELECT Id, Client__c, WorkType__c, StartDate__c, EndDate__c FROM Client_Invoice__c  WHERE WorkType__c IN: clientInvoiceWorkTypeId AND StartDate__c IN: clientInvoiceStartDate AND EndDate__c IN: clientInvoiceEndDate AND Client__c IN: clientInvoiceClientId])
        for(Client_Invoice__c eachClientInvoice : [SELECT Id, Client__c, WorkType__c, StartDate__c, EndDate__c,Status__c FROM Client_Invoice__c  WHERE WorkType__c IN: clientInvoiceWorkTypeId AND Client__c IN: clientInvoiceClientId AND Status__c = 'Pending'])
        {
            for (Client_Invoice__c eachCI : (List<Client_Invoice__c>) clientInvoiceRecords)
            {
                system.debug('enddate'+eachCI.EndDate__c+ '--'+eachClientInvoice.EndDate__c);
                //system.debug('startdate'+eachCI.StartDate__c +'---'+ eachClientInvoice.StartDate__c);
                                
                if(eachCI.Status__c == 'Pending' && eachCI.Client__c == eachClientInvoice.Client__c && eachCI.EndDate__c <= eachClientInvoice.EndDate__c && eachCI.StartDate__c >= eachClientInvoice.StartDate__c && eachCI.WorkType__c == eachClientInvoice.WorkType__c){
                    
                    //if any record exists in the same range do not create, throw an error
                    eachCI.addError('You cannot add more than once Client Invoice for the given criteria!');
                    break;
                }
            }
        }
    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void BeforeDelete(Map<Id, SObject> oldItems) {} public void AfterInsert(Map<Id, SObject> newItems) {}  public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {} public void AfterDelete(Map<Id, SObject> oldItems) {}    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}